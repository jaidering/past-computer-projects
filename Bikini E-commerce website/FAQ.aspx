﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bi4Mi.master" AutoEventWireup="true" CodeFile="FAQ.aspx.cs" Inherits="FAQ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        FAQ</p>
    <p>
        <i>Are there any fixed sizes?</i><br />
        Size charts are available for every design for customers to get their perfect 
        fit.</p>
    <p>
        <i>How would the bikini be sent to me?<br />
        </i>Bikini would be sent via postage. Depending on your location postage timings 
        would vary. A minimum of 2 days and a maximum of 1 week is required.</p>
                            <p>
                                <i>What are the available payment methods?</i><br />
                                Available payment methods would be through VISA, online payment methods such as 
                                pay-pal and also through bank transfers. Please have a look at the order form 
                                for further information.</p>
</asp:Content>

