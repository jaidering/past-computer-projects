﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Navigation.ascx.cs" Inherits="User_Controls_Navigation" %>
<asp:HyperLink ID="HyperLink1" runat="server" CssClass="style6" 
    style="font-family: Calibri; font-weight: bold; color: #808080" 
    NavigateUrl="~/Default.aspx">::Home::</asp:HyperLink>
                    &nbsp;&nbsp;<asp:HyperLink ID="HyperLink2" runat="server" 
    CssClass="style6" 
    style="font-family: Calibri; font-weight: bold; color: #808080" 
    NavigateUrl="~/OurBikinis.aspx">::Our Bikinis::</asp:HyperLink>
                    &nbsp;&nbsp;<asp:HyperLink ID="HyperLink3" runat="server" 
    CssClass="style6" 
    style="font-family: Calibri; font-weight: bold; color: #808080" NavigateUrl="~/Promotions.aspx">::Promotions::</asp:HyperLink>
                    &nbsp;&nbsp;<asp:HyperLink ID="HyperLink4" runat="server"
    CssClass="style6" 
    style="font-family: Calibri; font-weight: bold; color: #808080" NavigateUrl="~/Events.aspx">::Events &amp; Activities::</asp:HyperLink>
                    &nbsp;
                    <asp:HyperLink ID="HyperLink5" runat="server" 
    CssClass="style6" 
    style="font-family: Calibri; font-weight: bold; color: #808080" NavigateUrl="~/Members.aspx">::Members::</asp:HyperLink>
                    &nbsp;
                    <asp:HyperLink ID="HyperLink6" runat="server" 
    CssClass="style6" 
    style="font-family: Calibri; font-weight: bold; color: #808080" NavigateUrl="~/FAQ.aspx">::FAQ::</asp:HyperLink>
                    &nbsp;
                    <asp:HyperLink ID="HyperLink7" runat="server" 
    CssClass="style6" 
    style="font-family: Calibri; font-weight: bold; color: #808080" NavigateUrl="~/ContactUs.aspx">::Contact Us::</asp:HyperLink>