﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bi4Mi.master" AutoEventWireup="true" CodeFile="Events.aspx.cs" Inherits="Events" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style6
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <span class="style6">Upcoming Events</span></p>
    <p>
        30 March 2009- Victorias Fashion Runway
        <br />
        Here in the Victorias Fashion Runway, we would be displaying our latest range of 
        bikinis. Hosted by the very famous Victoria herself, this show is going to make 
        you&nbsp; go &quot;WOW&quot;! And in the finale, Victoria would be the showstopper.<br />
        Come on down on the 30th.
        <br />
        <br />
        Venue: Singapore National Stadium,$25/-</p>
    <p>
        4 April 2009 - Sentosa Getaway<br />
        Beware!! We would be hunting you down and those caught wearing our bikini, WATCH 
        OUT! One of you lucky wearers would be given a $100 voucher plus freebies(make 
        up items, accessories,etc). Not to worry. Those who do not win would still get 
        to enjoy our Beach Party.<br />
        So come on down on the 4th.<br />
        <br />
        Venue: Sentosa Beach, Free.</p>
</asp:Content>

