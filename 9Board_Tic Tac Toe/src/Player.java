public enum Player {
		X("X"), O("O"), NONE(" ");
		
		private String stringRep;
		private Player opponent;
		
		static {
			X.opponent = O;
			O.opponent = X;
			NONE.opponent = NONE;
		}
		
		private Player(String stringRep) {
			this.stringRep = stringRep;
		}
		
		public Player opponent() {
			return opponent;
		}
		
		@Override
		public String toString() {
			return stringRep;
		}
	}