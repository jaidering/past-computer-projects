
public class SmallBoard {
	private Player board[][];
	
	public SmallBoard() {
		board = new Player[3][3];
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				board[row][col] = Player.NONE;
			}
		}
	}
	
	private Player getPos(int pos) {
		int col = (pos - 1) % 3;
		int row = (pos - 1) / 3;
		
		return board[row][col];
	}
	private void setPos(int pos, Player player) {
		int col = (pos - 1) % 3;
		int row = (pos - 1) / 3;
		
		board[row][col] = player;
	}
	
	public int getNumMoves() {
		int numMoves = 0;
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				if (board[row][col] != Player.NONE) {
					numMoves++;
				}
			}
		}
		
		return numMoves;
	}
	
	public Player getPlayerAt(int pos) {
		return getPos(pos);
	}
	
	public SmallBoard copy() {
		SmallBoard newBoard = new SmallBoard();
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				newBoard.board[row][col] = this.board[row][col];
			}
		}
		
		return newBoard;
	}
	
	public boolean isValidMove(int pos) {
		return getPos(pos) == Player.NONE;
	}
	
	public SmallBoard makeMove(int pos, Player player) {
		if (getPos(pos) == Player.NONE) {
			SmallBoard newBoard = this.copy();
			newBoard.setPos(pos, player);
			
			return newBoard;
		} else {
			return null;
		}
	}
	
	public int maxJoined(Player player) {
		int maxJoined = 0;
		int num;
		
		// Check rows
		for (int row = 0; row < 3; row++) {
			num = 0;
			for (int col = 0; col < 3; col++) {
				if (board[row][col] == player) {
					num++;
				} else if (board[row][col] == player.opponent()) {
					num = 0;
					break;
				}
			}
			if (num > maxJoined) {
				maxJoined = num;
			}
		}
		
		// Check columns
		for (int col = 0; col < 3; col++) {
			num = 0;
			for (int row = 0; row < 3; row++) {
				if (board[row][col] == player) {
					num++;
				} else if (board[row][col] == player.opponent()) {
					num = 0;
					break;
				}
			}
			if (num > maxJoined) {
				maxJoined = num;
			}
		}
		
		// Check diagonal 1
		num = 0;
		for (int i = 0; i < 3; i++) {
			if (board[i][i] == player) {
				num++;
			} else if (board[i][i] == player.opponent()) {
				num = 0;
				break;
			}
		}
		if (num > maxJoined) {
			maxJoined = num;
		}
		
		// Check diagonal 2
		num = 0;
		for (int i = 0; i < 3; i++) {
			if (board[i][2 - i] == player) {
				num++;
			} else if (board[i][2 - i] == player.opponent()) {
				num = 0;
				break;
			}
		}
		if (num > maxJoined) {
			maxJoined = num;
		}
		
		return maxJoined;
	}
	
	public boolean canWin(Player player) {
		int maxJoined = maxJoined(player);
		
		if (maxJoined == 2) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isFull() {
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				if (board[row][col] == Player.NONE){
					return false;
				}
			}
		}
		return true;
	}
	public boolean isWon() {
		return isWon(Player.X) || isWon(Player.O);
	}
	public boolean isWon(Player player) {
		int maxJoined = maxJoined(player);
		
		if (maxJoined == 3) {
			return true;
		} else {
			return false;
		}
	}
	
	public String toString(int row, int lastMove) {
		int lastRow = -1;
		int lastCol = -1;
		if (lastMove != 0) {
			lastCol = (lastMove - 1) % 3;
			lastRow = (lastMove - 1) / 3;
		}
		
		String stringRep = "";
		for (int col = 0; col < 3; col++) {
			if (row == lastRow && col == lastCol) {
				stringRep += board[row][col].toString().toLowerCase();
			} else {
				stringRep += board[row][col];
			}
			if (col < 2) {
				stringRep += " ";
			}
		}
		
		return stringRep;
	}
	@Override
	public String toString() {
		String stringRep = "";
		for (int row = 0; row < 3; row++) {
			stringRep += this.toString(row, 0) + "\n";
		}
		
		return stringRep;
	}
}
