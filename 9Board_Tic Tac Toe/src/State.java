import java.util.ArrayList;
import java.util.List;


public class State {
	private SmallBoard[][] boards;
	private int activeBoard;
	
	private int lastBoard;
	private int lastMove;

	public State() {
		boards = new SmallBoard[3][3];
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				boards[row][col] = new SmallBoard();
			}
		}
		activeBoard = 0;
	}
	
	private SmallBoard getBoard(int board) {
		int col = (board - 1) % 3;
		int row = (board - 1) / 3;

		return boards[row][col];
	}
	private void setBoard(int board, SmallBoard newBoard) {
		int col = (board - 1) % 3;
		int row = (board - 1) / 3;

		boards[row][col] = newBoard;
	}
	
	public int getActiveBoard() {
		return activeBoard;
	}
	public int getNumMoves() {
		int numMoves = 0;
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				numMoves += boards[row][col].getNumMoves();
			}
		}
		
		return numMoves;
	}
	public List<Integer> validMoves() {
		List<Integer> valid = new ArrayList<Integer>();
		SmallBoard board = getBoard(activeBoard);
		for (int i = 1; i <= 9; i++) {
			if (board.isValidMove(i)) {
				valid.add(i);
			}
		}
		return valid;
	}
	public Player getPlayerAt(int board, int pos) {
		return getBoard(board).getPlayerAt(pos);
	}

	public State copy() {
		State newState = new State();
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				newState.boards[row][col] = this.boards[row][col];
			}
		}

		return newState;
	}
	
	public State makeMove(int pos, Player player) {
		return makeMove(activeBoard, pos, player);
	}
	public State makeMove(int board, int pos, Player player) {
		SmallBoard newBoard = getBoard(board).makeMove(pos, player);
		if (newBoard == null) {
			return null;
		} else {
			State newState = this.copy();
			newState.setBoard(board, newBoard);
			newState.activeBoard = pos;
			newState.lastBoard = board;
			newState.lastMove = pos;
			return newState;
		}
	}
	
	public int maxJoined(int board, Player player) {
		return getBoard(board).maxJoined(player);
	}
	public boolean canWin(int board, Player player) {
		return getBoard(board).canWin(player);
	}
	public boolean isWon() {
		return isWon(Player.X) || isWon(Player.O);
	}
	public boolean isWon(Player player) {
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				if (boards[row][col].isWon(player)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		String stringRep = "";
		for (int row = 0; row < 3; row++) {
			for (int smallRow = 0; smallRow < 3; smallRow++) {
				for (int col = 0; col < 3; col++) {
					if (boards[row][col] == getBoard(lastBoard)) {
						stringRep += boards[row][col].toString(smallRow, lastMove);
					} else {
						stringRep += boards[row][col].toString(smallRow, 0);
					}
					if (col < 2) {
						stringRep += " | ";
					}
				}
				stringRep += "\n";
			}
			if (row < 2) {
				stringRep += "------+-------+------\n";
			}
		}

		return stringRep;
	}
}
