import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class Agent {
	private static boolean DEBUGGING = false;
	private static final int INFINITY = 100000000;
	private static final int MAX_SCORE = 1000;
	private static Player me = null;

	public static void main(String[] args) throws Exception {
		/*
		 * Set up network things
		 */
		String serverName = "localhost";
		int serverPort = 31415;
		if (args.length >= 2 && args[0].equals("-p")) {
			serverPort = Integer.parseInt(args[1]);
		}
		if (args.length >= 3 && args[2].equals("-d")) {
			DEBUGGING = true;
		}
		InetAddress serverIPAddress = InetAddress.getByName(serverName);
		Socket clientSocket = new Socket(serverIPAddress, serverPort);
		BufferedReader inFromServer = new BufferedReader(
						new InputStreamReader(clientSocket.getInputStream()));
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());

		/*
		 * Initialise our state and other useful things
		 */
		State myState = new State();
		Random rand = new Random(System.nanoTime());
		int BASE_DEPTH = 6;

		boolean weWon = true;
		String reason = "";

		/*
		 * Communicate with server
		 */
		while (true) {
//			System.out.println("Waiting for input from server on port " + serverPort);
			String message;
			message = inFromServer.readLine();
//			System.out.println(message);

			if (message.equals("init.")) {
				// Whatever been there done that
			} else if (message.equals("start(o).")) {
				me = Player.O;
				if (DEBUGGING) {
					System.out.println("I am " + me);
				}

				outToServer.writeBytes("");
			} else if (message.equals("start(x).")) {
				me = Player.X;
				if (DEBUGGING) {
					System.out.println("I am " + me);
				}

				outToServer.writeBytes("");
			} else if (message.matches("^.*second_move.*$")) {
				int board = Character.getNumericValue(message.charAt(12));
				int pos = Character.getNumericValue(message.charAt(14));
//				System.out.println("The enemy moved on board " + board + " at location " + pos);

				myState = myState.makeMove(board, pos, me.opponent());

				int myMove = bestMove(myState, BASE_DEPTH, rand);
				myState = myState.makeMove(myState.getActiveBoard(), myMove, me);
				if (DEBUGGING) {
					System.out.println(myState);
				}
				outToServer.writeBytes(myMove + "\n");
			} else if (message.matches("^.*third_move.*$")) {
				int board = Character.getNumericValue(message.charAt(11));
				int myFirstMove = Character.getNumericValue(message.charAt(13));
				int pos = Character.getNumericValue(message.charAt(15));

//				System.out.println("I moved on board " + board + " at location " + myFirstMove);
				myState = myState.makeMove(board, myFirstMove, me);


//				System.out.println("The enemy moved on board " + myState.getActiveBoard() + " at location " + pos);
				myState = myState.makeMove(myState.getActiveBoard(), pos, me.opponent());

				int myMove = bestMove(myState, BASE_DEPTH, rand);
				myState = myState.makeMove(myState.getActiveBoard(), myMove, me);
				if (DEBUGGING) {
					System.out.println(myState);
				}
				outToServer.writeBytes(myMove + "\n");
			} else if (message.matches("^.*next_move.*$")) {
				int pos = Character.getNumericValue(message.charAt(10));
//				System.out.println("The enemy last moved at location " + pos);

				myState = myState.makeMove(myState.getActiveBoard(), pos, me.opponent());
				
				if (DEBUGGING) {
					System.out.println("\nThinking about my move...");
				}
				int myMove = bestMove(myState, BASE_DEPTH, rand);
				myState = myState.makeMove(myState.getActiveBoard(), myMove, me);
				if (DEBUGGING) {
					System.out.println(myState);
				}
				outToServer.writeBytes(myMove + "\n");
			} else if (message.matches("^.*last_move.*$")) {
				int pos = Character.getNumericValue(message.charAt(10));

				myState = myState.makeMove(myState.getActiveBoard(), pos, me.opponent());
			} else if (message.matches("^.*loss.*$")) {
				weWon = false;
				reason = message;
			} else if (message.matches("^.*win.*$")) {
				weWon = true;
				reason = message;
			} else if (message.equals("end.")) {
				break;
			} else {
				// We didn't get anything understandable from the server
				System.out.println("No idea what server just said: " + message);
				outToServer.writeBytes("");
			}
		}
		clientSocket.close();

		/*
		 * Game has ended
		 */
		if (weWon) {
			System.out.println("Win");
		} else {
			System.out.println("Lose");
		}
//		System.out.println(reason);

	}
	
	private static int bestMove(State curState, int depth, Random rand) {
		int[] scores = new int[10];
		for (int i = 0; i < 10; i++) {
			scores[i] = -INFINITY;
		}
		List<Integer> validMoves = curState.validMoves();
		
		int bestScore = -INFINITY;
		for (int move : validMoves) {
			State newState = curState.makeMove(curState.getActiveBoard(), move, me);
			scores[move] = bestScore(newState, false, depth, -INFINITY, INFINITY);
			
			if (scores[move] > bestScore) {
				bestScore = scores[move];
			}
			if (scores[move] == MAX_SCORE) {
				break;
			}
		}
		
		List<Integer> movesToChoose = new ArrayList<Integer>();
		for (int move : validMoves) {
			if (scores[move] == bestScore) {
				movesToChoose.add(move);
			}
		}
		Collections.shuffle(movesToChoose, rand);
		
		if (DEBUGGING) {
			System.out.println("Best score: " + bestScore);
		}
		return movesToChoose.get(0);
	}

	private static int bestScore(State curState, boolean isMe, int depth, int alpha, int beta) {
		if (curState.isWon(me)) {
			return MAX_SCORE;
		} else if (curState.isWon(me.opponent())) {
			return -MAX_SCORE;
		}
		// Heuristic
		if (depth == 0) {
			int myScore = 0;
			int opScore = 0;
			for (int i = 1; i <= 9; i++) {
				if (curState.canWin(i, me)) {
					myScore += 10;
				} else {
					myScore += curState.maxJoined(i, me);
				}
				if (curState.canWin(i, me.opponent())) {
					opScore += 10;
					myScore += curState.maxJoined(i, me.opponent());
				}
			}
			return myScore - opScore;
		}
		
		if (isMe) {
			for (int move : curState.validMoves()) {
				int curScore = 0;

				State newState = curState.makeMove(curState.getActiveBoard(), move, me);
				curScore = bestScore(newState, false, depth - 1, alpha, beta);

				if (curScore > alpha) {
					alpha = curScore;
				}
				if (beta <= alpha || alpha == MAX_SCORE) {
					break;
				}
			}
			return alpha;
		} else {
			for (int move : curState.validMoves()) {
				int curScore = 0;

				State newState = curState.makeMove(curState.getActiveBoard(), move, me.opponent());
				curScore = bestScore(newState, true, depth - 1, alpha, beta);

				if (curScore < beta) {
					beta = curScore;
				}
				if (beta <= alpha || beta == -MAX_SCORE) {
					break;
				}
			}
			return beta;
		}
	}
}
