;; The first three lines of this file were inserted by DrScheme. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname ps03) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;
; CS1101S --- Programming Methodology (Scheme)
; 
; PS03: Solutions to Homework Exercises

; Note that answers are commented out to allow the Tutors to 
; run your code easily while grading your problem set.

;===========
'(--------------------- Exercise 1 ---------------------)
;===========

;;; Your solution

(define (RSA-decrypt intlist private-key)
    (intlist->string (map (lambda (int) (RSA-transform int private-key))
         intlist)))

; This simple scheme is inadequate for a secure encryption because if a third person knows 
; the recipient's private code he/she can easily decrypt the encrypted code. There is no extra so called seal in
; this encryption.


;===========
'(--------------------- Exercise 2 ---------------------)
;===========

;;; Your solution

(define (RSA-unconvert-list intlist private-key)
    (let ((n (key-modulus private-key)))
      (define (unconvert lst sum)
        (if (null? lst)
            '()
            (let ((x (RSA-transform (car lst) private-key)))
              (cons (modulo (+ x sum) n) (unconvert (cdr lst) (car lst))))))
      (unconvert intlist 0)))

;;; Check your procedure by evaluating

(define test-public-key1 (key-pair-public test-key-pair1))
(define result1 (RSA-encrypt "This is a test message." test-public-key1))
(define test-private-key1 (key-pair-private test-key-pair1))

(RSA-unconvert-list result1 test-private-key1)
;;; Result: (242906196 69006496 213717089 229128819 205322725 67875559)

;;; Now evaluate

(RSA-decrypt result1 test-private-key1)
;;; Result: "This is a test message. "


;;; Test messages

(RSA-encrypt "Schemers are COOL!!" test-public-key2)
;(258033250 350513703 391502396 17014986 46441517)

(RSA-decrypt (list 258033250 350513703 391502396 17014986 46441517) test-private-key2)
;"Schemers are COOL!! "

(RSA-encrypt "Schemers are COOL!!" test-public-key1)
;(206226983 93051428 221734146 227199486 321271579)

(RSA-decrypt (list 206226983 93051428 221734146 227199486 321271579) test-private-key1)
;"Schemers are COOL!! "

(RSA-encrypt "Scheme is fun!!!" test-public-key2)
;(258033250 388763729 183033977 390885022)

(RSA-decrypt (list 258033250 388763729 183033977 390885022) test-private-key2)
;"Scheme is fun!!!"


;===========
'(--------------------- Exercise 3 ---------------------)
;===========

;;; Your solution


(define (encrypt-and-sign msg sender-private-key recipient-public-key)
  (make-signed-message
   (RSA-transform (compress (RSA-encrypt msg recipient-public-key)) sender-private-key)
   (RSA-encrypt msg recipient-public-key)))

;; data structure
(define (make-signed-message signature message)
  (cons signature message))
(define (get-signature signed-message)
  (car signed-message))
(define (get-message signed-message)
  (cdr signed-message))


;;; Test your procedure

(define result2 
  (encrypt-and-sign "Test message from user 1 to user 2"
                    test-private-key1
                    test-public-key2))

;;; Result's message part is
;;; (296342791 45501589 263575681 219298391 4609203 331301818 178930017 242685109 1345058)

(define (authenticate-and-decrypt enc-msg sender-public-key recipient-private-key)
  (if (equal? (list (compress (get-message signed-message))) (RSA-unconvert-list (list (get-signature signed-message)) sender-public-key))
      (RSA-decrypt (get-message signed-message) recipient-private-key)
      "Signature not authentic!!"))

;; testing

(authenticate-and-decrypt result2 test-public-key1 test-private-key2)
;"Test message from user 1 to user 2  "

;; Demo

(encrypt-and-sign "testing message" jai-private-key Caleb-public-key)
;(14349546 44788281 351119943 375343765 411608031)

(authenticate-and-decrypt '(14349546 44788281 351119943 375343765 411608031) jai-public-key Caleb-private-key)
;"testing message "

(authenticate-and-decrypt '(14349546 44788281 351119943 375343765 411608031) Ben-public-key Caleb-private-key)
;"Signature not authentic!!"



;===========
'(--------------------- Exercise 4 ---------------------)
;===========

(authenticate-and-decrypt (cons received-mystery-signature received-mystery-message) Ben-public-key Caleb-private-key)
;"Signature not authentic!!"

(authenticate-and-decrypt (cons received-mystery-signature received-mystery-message) Hongyang-public-key Caleb-private-key)
;"Signature not authentic!!"

(authenticate-and-decrypt (cons received-mystery-signature received-mystery-message) ZiHan-public-key Caleb-private-key)
;"Signature not authentic!!"

(authenticate-and-decrypt (cons received-mystery-signature received-mystery-message) Chris-public-key Caleb-private-key)
;"Signature not authentic!!"

(authenticate-and-decrypt (cons received-mystery-signature received-mystery-message) DucHiep-public-key Caleb-private-key)
;"I have solid evidence that you have been illegally manufacturing state-controlled EMP weapons and selling them on the black market. "

; The sender is Duc Hiep and the message is "I have solid evidence that you have been illegally manufacturing state-controlled EMP weapons and 
; selling them on the black market. "



;===========
'(--------------------- Exercise 5 ---------------------)
;===========

;;; Your solution

(define (solve-ax+by=1 a b)
      (cond ((= b 1) (cons 0 1))
            ((not (= (gcd a b) 1)) "no solution")
            ((or (= a 0) (= b 0)) "no solution")
            ((< a b) (solve-ax+by=1 b a))
            ((= (modulo a b) 0) "no solution")
            (else (cons (cdr (solve-ax+by=1 b (modulo a b))) (/ (- 1 (* a (cdr (solve-ax+by=1 b (modulo a b))))) b)))))


;;; Test your procedure

(solve-ax+by=1 233987973 41111687)
;(-11827825 . 67318298)

; jai's keys
(define jai-public-key (make-key 325709621 15720961))
(define jai-private-key (make-key 325709621 106853377))

; Demo
(RSA-encrypt "HELLO!!" jai-public-key)
;(55353391 47681951)

(RSA-decrypt '(55353391 47681951) jai-private-key)
;"HELLO!! "

;===========
'(--------------------- Exercise 6 ---------------------)
;===========

;;; Your solution

(define (crack-rsa public-key)
    (cons
     (car public-key)
     (invert-modulo (cdr public-key) 
                    (* (- (smallest-divisor (car public-key)) 1) (- (/ (car public-key) (smallest-divisor (car public-key))) 1)))))

; Test

(crack-rsa test-public-key1)
;(385517963 . 137332327)    
test-private-key1
;(385517963 . 137332327)
 
(crack-rsa test-public-key2)
;(432208237 . 401849313)     
test-private-key2
;(432208237 . 401849313)

;===========
'(--------------------- Exercise 7 ---------------------)
;===========

;;; Your solution

(crack-rsa DucHiep-public-key)
;(733058129 . 556977335)
(define DucHiep-private-key (make-key 733058129 556977335))

(encrypt-and-sign "Keeping my mouth shut will cost you ONE BILLION DOLLARS." DucHiep-private-key Caleb-public-key)
;(140268548 389860351 595611930 463601025 640084954 396884014 609009825 612443255 318288414 481267883 410385437 187964896 11456875 257577355 225798736)

(authenticate-and-decrypt '(140268548 389860351 595611930 463601025 640084954 396884014 609009825 612443255 318288414 481267883 410385437 187964896 11456875 257577355 225798736) DucHiep-public-key Caleb-private-key)
;"Keeping my mouth shut will cost you ONE BILLION DOLLARS."


;===========
'(--------------------- Exercise 8 ---------------------)
;===========

;;; Your explanation here
; It would be better to sign first then encrypt because if we encrypt then sign, it is easier for a third person to change the signature 
; (which is created using the sender's private key) and use his/her own private key to sign the message and claim authorship for the message.



;===========
'(--------------------- Exercise 9 ---------------------)
;===========

;;; Provide your estimation here

(timed smallest-divisor 780450379) ;; 9 digits
;time:16
;25057

(timed smallest-divisor 1839232353883) ;; 13 digits
;time:640
;1338637

(timed smallest-divisor 1811692732421591) ;; 16 digits
;time:17629
;38249647

;; for 50 digits it will take years.
;; for 100 digits it will take years.

