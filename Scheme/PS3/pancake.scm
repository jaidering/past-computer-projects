;; CS1101S Pancake problem

;; Data structure for pancake

(define (make-pancake size)
  (define (build-pancake n pancake)
    (if (= n 0)
        pancake
        (build-pancake (- n 1)
                       (cons n pancake))))
  (build-pancake size '()))

(define (pancake-size pancake)
  (define (length pancake counter)
    (if (null? pancake)
        counter
        (length (cdr pancake) (+ counter 1))))
  (length pancake 0))

(define (get-pancake pancake)
  pancake)

(define (copy-pancake pancake)
  (if (= (pancake-size pancake) 0)
      '()
      (cons (car pancake) (copy-pancake (cdr pancake)))))

(define (top-slice pancake)
  (car pancake))

(define (rest-of-pancake pancake)
  (cdr pancake))

;; Operations
(define (show-pancake pancake) ; print the pancake
  (define (print-space n)
    (if (= n 0)
        #t
        (begin (display "  ")
               (print-space (- n 1)))))
  (define (print-pancake n)
    (if (= n 0)
        #t
        (begin (display "####")
               (print-pancake (- n 1)))))
  (define (print-slice size max)
    (let ((space (- max size)))
      (print-space space)
      (print-pancake size)
      (print-space space)
      (newline)))
  (define (show-pancake-inner pancake size) 
    (if (empty? pancake)
        #t
        (begin (print-slice (top-slice pancake) size)
               (show-pancake-inner (rest-of-pancake pancake) size))))
  (show-pancake-inner pancake (pancake-size pancake))) 

(define (flip-pancake pancake level)  ; flips a pancake at specified level
  (define (flip-iter current done counter)
    (if (= counter level)
        (append done current)
        (flip-iter (cdr current)
                   (cons (car current) done)
                   (+ 1 counter))))
  (if (or (< level 1)
          (> level (pancake-size pancake)))
      "Illegal move"
      (flip-iter pancake '() 0)))

(define (check-pancake pancake) ; returns #t if pancake is sorted
  (define (check-iter pancake counter)
    (cond ((null? pancake) #t)
          ((not (= counter (car pancake))) #f)
          (else (check-iter (cdr pancake) (+ counter 1)))))
  (check-iter pancake 1))

(define (random-flip pancake) ; randomly makes one flip 
  (let ((flip (+ (random (pancake-size pancake)) 1)))
    (flip-pancake pancake flip)))

(define (random-flips pancake n) ; makes n random flips 
  (if (= n 0)
      pancake
      (random-flips (random-flip pancake) (- n 1))))

(define (check-flips pancake list-of-flips)
  (let ((number-of-flips (length list-of-flips)))
    (define (helper pancake list-of-flips)
      (if (null? list-of-flips)
          (if (check-pancake pancake)
              number-of-flips
              #f)
          (helper (flip-pancake pancake (car list-of-flips)) (cdr list-of-flips))))
    (helper pancake list-of-flips)))

(define p1 (make-pancake 8))
(show-pancake p1)
(check-pancake p1)
(define p2 (random-flips p1 10))
(show-pancake p2)
(check-pancake p2)

(define p3 (flip-pancake (flip-pancake p1 2) 3))
(check-flips p3 '(2))
(check-flips p3 '(2 3))
(check-flips p3 '(3 2))