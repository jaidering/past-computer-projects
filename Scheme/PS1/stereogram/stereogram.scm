;; The first three lines of this file were inserted by DrScheme. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname stereogram) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;;; Scheme Source Code for CS1101S Lecture 2
;;; Procedural Abstraction

; Setup graphics

(require (lib "graphics.ss" "graphics"))
(define viewport-size 600) ; This is the height of the viewport.
(open-graphics)

; Note that we have removed argument vp from clear, draw, and stereogram method
; since we now only have 1 viewport.
(define vp (open-viewport "Quilting ViewPort" (* 4/3 viewport-size) viewport-size))
(define (clear) ((clear-viewport vp)))

					
; Vector arithmetic

(define (add-vect p1 p2)
  (make-posn (+ (posn-x p1) (posn-x p2))
             (+ (posn-y p1) (posn-y p2))))

(define (scale-vect r p)
  (make-posn (* r (posn-x p))
             (* r (posn-y p))))


; A graphic object (painter) is drawn in a frame 
; made up of an origin and 2 vectors

(define (make-frame p0 p1 p2 z1 z2)
  (list p0 p1 p2 z1 z2))

(define (frame-orig f) (car f))
(define (frame-x f) (cadr f))
(define (frame-y f) (caddr f))
(define (frame-z1 f) (cadddr f))
(define (frame-z2 f) (car (cddddr f)))


; Show a painter in the unit frame

(define unit-frame 
  (make-frame (make-posn (* 1/6 viewport-size) 0)
              (make-posn viewport-size 0)
              (make-posn 0 viewport-size)
              0
              1))

(define (show painter)
  (painter vp unit-frame))

					
; Translate a point from the unit frame into a new frame

(define (transform-posn frame)
  (lambda (posn)
    (add-vect (frame-orig frame)
              (add-vect (scale-vect (/ (posn-x posn) viewport-size)
                                    (frame-x frame))
                        (scale-vect (/ (posn-y posn) viewport-size)
                                    (frame-y frame))))))

; Zero element of painting

(define (null-painter frame) 'nothing-to-do)

					
; Some useful predefined painters (page 16 in "Concrete Abstractions")

(define (rcross-bb vp frame)
  (let ((p1 (list (make-posn 0 0)
                  (make-posn (/ viewport-size 4) (/ viewport-size 4))
                  (make-posn (/ (* 3 viewport-size) 4) (/ viewport-size 4))
                  (make-posn (/ (* 3 viewport-size) 4) (/ (* 3 viewport-size) 4))
                  (make-posn viewport-size viewport-size)
                  (make-posn viewport-size 0)))
        (p2 (list (make-posn (/ viewport-size 4) (/ viewport-size 4))
                  (make-posn (/ viewport-size 4) (/ (* 3 viewport-size) 4))
                  (make-posn (/ (* 3 viewport-size) 4) (/ (* 3 viewport-size) 4)))))
    ((draw-solid-polygon vp) (map (transform-posn frame) p1)
                             (make-posn 0 0)
                             (make-rgb (frame-z1 frame) (frame-z1 frame) (frame-z1 frame)))
    ((draw-solid-polygon vp) (map (transform-posn frame) p2)
                             (make-posn 0 0)
                             (make-rgb (frame-z1 frame) (frame-z1 frame) (frame-z1 frame)))))

(define (sail-bb vp frame)
  (let ((p (list (make-posn (/ viewport-size 2) 0)
                 (make-posn (/ viewport-size 2) viewport-size)
                 (make-posn viewport-size viewport-size))))
    ((draw-solid-polygon vp) (map (transform-posn frame) p)
                             (make-posn 0 0)
                             (make-rgb (frame-z1 frame) (frame-z1 frame) (frame-z1 frame)))))

(define (corner-bb vp frame)
  (let ((p (list (make-posn (/ viewport-size 2) 0)
                 (make-posn viewport-size 0)
                 (make-posn viewport-size 
                            (/ viewport-size 2)))))
    ((draw-solid-polygon vp) (map (transform-posn frame) p) 
                             (make-posn 0 0)
                             (make-rgb (frame-z1 frame) (frame-z1 frame) (frame-z1 frame)))))

(define (nova-bb vp frame)
  (let ((p (list (make-posn (/ viewport-size 2) 0)
                 (make-posn (/ viewport-size 4) (/ viewport-size 2))
                 (make-posn viewport-size (/ viewport-size 2))
                 (make-posn (/ viewport-size 2) (/ viewport-size 4)))))
    ((draw-solid-polygon vp) (map (transform-posn frame) p)
                             (make-posn 0 0)
                             (make-rgb (frame-z1 frame) (frame-z1 frame) (frame-z1 frame)))))

(define (heart-bb vp frame)
  (let* ((k (/ (sqrt 2) 2))
         (1-k/1+3k (/ (- 1 k) (+ 1 (* 3 k))))
         (1-k/1+k (/ (- 1 k) (+ 1 k)))
         (1+k/1+3k (/ (+ 1 k) (+ 1 (* 3 k))))
         (p (list (make-posn (/ viewport-size 2) (* 1-k/1+3k viewport-size))
                  (make-posn (* 1-k/1+k (/ viewport-size 2))
                             (* 1+k/1+3k viewport-size))
                  (make-posn (/ viewport-size 2) viewport-size)
                  (make-posn (- viewport-size (* 1-k/1+k (/ viewport-size 2)))
                             (* 1+k/1+3k viewport-size)))))
    ; Draw a kite (bottom half of the heart).
    ((draw-solid-polygon vp) (map (transform-posn frame) p) 
                             (make-posn 0 0)
                             (make-rgb (frame-z1 frame) (frame-z1 frame) (frame-z1 frame)))
    ; Draw the top of the heart.
    (let ((heart-circle
           (stack-frac (/ 2 (+ 1 (* 3 k)))
                       (quarter-turn-right (stack-frac (/ k (+ 1 k)) blank-bb circle-bb))
                       blank-bb)))
      (heart-circle vp frame)
      ((flip-horiz heart-circle) vp frame))))

; center-and-fill will center and scale a 2x2 image to fill the entire viewport.
; This is used by circle-bb, spiral-bb, and ribbon-bb.
(define center (make-posn (/ viewport-size 2) (/ viewport-size 2)))
(define (center-and-fill x)
  (add-vect center (scale-vect (/ viewport-size 2) x)))
  
(define (circle-bb vp frame)
  ; make-circle will return a list of points (a lot of points) that approx a circle.
  (define (make-circle)
    (define (helper angle poly)
      (if (>= angle (* 2 pi))
          poly
          (cons (make-posn (cos angle) (sin angle))
                (helper (+ angle (/ 1 viewport-size)) poly))))
    (helper 0 ()))

  ; We approximate a circle by drawing polygon with A LOT of vertices.
  ((draw-solid-polygon vp) (map (transform-posn frame) (map center-and-fill (make-circle)))
                           (make-posn 0 0)
                           (make-rgb (frame-z1 frame) (frame-z1 frame) (frame-z1 frame))))

(define (spiral-bb vp frame)
  (let* ((theta-max 30)
         (thickness (/ -1 theta-max)))
    
    (define (make-spiral)
      (define (helper angle offset poly)
        (if (>= angle theta-max)
            poly
            (cons (make-posn (* (+ offset (/ angle theta-max)) (cos angle))
                             (* (+ offset (/ angle theta-max)) (sin angle)))
                  (helper (+ angle 0.02) offset poly))))
      (helper 0 0 (reverse (helper 0 thickness '()))))
    
    ((draw-solid-polygon vp) (map (transform-posn frame) 
                                  (map center-and-fill (make-spiral)))
                             (make-posn 0 0)
                             (make-rgb (frame-z1 frame) (frame-z1 frame) (frame-z1 frame)))))

(define (ribbon-bb vp frame)
  (let* ((theta-max 30)
         (thickness (/ -1 theta-max)))
    
    (define (make-ribbon angle poly)
      (if (>= angle theta-max)
          (close-ribbon theta-max poly)
          (cons (make-posn (* (/ angle theta-max) (cos angle))
                           (* (/ angle theta-max) (sin angle)))
                (make-ribbon (+ angle 0.02) poly))))
    
    (define (close-ribbon angle poly)
      (if (<= angle 0)
          poly
          (cons (make-posn (+ (abs (* (cos angle) thickness))
                              (* (/ angle theta-max) (cos angle)))
                           (+ (abs (* (sin angle) thickness))
                              (* (/ angle theta-max) (sin angle))))
                (close-ribbon (- angle 0.02) poly))))
    
    ((draw-solid-polygon vp) (map (transform-posn frame)
                                  (map center-and-fill (make-ribbon 0 ())))
                             (make-posn 0 0)
                             (make-rgb (frame-z1 frame) (frame-z1 frame) (frame-z1 frame)))))

(define (black-bb vp frame)
 (let ((p (list (make-posn 0 0)
                (make-posn viewport-size 0)
                (make-posn viewport-size viewport-size)
                (make-posn 0 viewport-size))))
  ((draw-solid-polygon vp) (map (transform-posn frame) p)
                           (make-posn 0 0)
                           (make-rgb (frame-z1 frame) (frame-z1 frame) (frame-z1 frame)))))

; (void) prevents PLT scheme from displaying anything.
; An alternative is to use (when #f ()).
(define (blank-bb vp frame) (void))
                           

; Custom painter maker

(define (image->painter filename)
  (let ((converter (open-pixmap "Quilting ViewPort" viewport-size viewport-size))
        (tolerance 0.14))
    
    (define (outer-loop y)
      (define (inner-loop x y)
        (if (< x viewport-size)
            (let* ((color ((get-color-pixel converter) (make-posn x y)))
                   (r (rgb-red color))
                   (g (rgb-green color))
                   (b (rgb-blue color))
                   (mono (/ (+ r g b) 3)))
              (cons mono (inner-loop (+ x 1) y)))
            ()))
      (if (< y viewport-size)
          (cons (inner-loop 0 y) (outer-loop (+ y 1)))
          ()))
    
    ((draw-pixmap converter) filename (make-posn 0 0))
    
    (let ((data (eval (cons 'vector (map (lambda (x) (eval (cons 'vector x))) (outer-loop 0))))))
      (define (blend x y)
        (let* ((top (min (- (vector-length data) 1) (inexact->exact (ceiling y))))
               (bottom (max 0 (inexact->exact (floor y))))
               (left (max 0 (inexact->exact (floor x))))
               (right (min (- (vector-length (vector-ref data 0)) 1)
                           (inexact->exact (ceiling x))))
               (tr (+ (- top y) (- right x)))
               (br (+ (- y bottom) (- right x)))
               (bl (+ (- y bottom) (- x left)))
               (tl (+ (- top y) (- x left)))
               (total (+ tr br bl tl)))
          (if (zero? total)
              (vector-ref (vector-ref data (inexact->exact (round y)))
                          (inexact->exact (round x)))
              (/ (+ (* tr (vector-ref (vector-ref data top) right))
                    (* br (vector-ref (vector-ref data bottom) right))
                    (* bl (vector-ref (vector-ref data bottom) left))
                    (* tl (vector-ref (vector-ref data top) left)))
                 total))))
      
      (lambda (vp frame)
        
        (define (out-loop y)
          (define (in-loop x)
            (let ((color (+ (frame-z1 frame) 
                            (* (- (frame-z2 frame) (frame-z1 frame)) (blend x y)
                               ;(vector-ref (vector-ref data (inexact->exact (round y)))
                               ;(inexact->exact (round x)))
                                                ))))
              (if (> (- 1 tolerance) (/ color (frame-z2 frame)))
                  ((draw-pixel vp) (apply (transform-posn frame) (list (make-posn x y))) 
                                   (make-rgb color color color)))
              (if (< x (- viewport-size 1))
                  (in-loop (+ x (/ viewport-size (posn-x (frame-x frame))))))))
          (in-loop 0)
          (if (< y (- viewport-size 1))
              (out-loop (+ y (/ viewport-size (posn-y (frame-y frame)))))))
        
        (out-loop 0)))))


; This procedure creates a painter from a depth function f(x,y) where 0<=x,y<=600. The
; depth function returns a depth value, a real number in the range [0,1] where 0 represents
; shallowest depth and 1 deepest. The depth function will be sampled at integer intervals,
; users may use this fact to aid them in writing the depth function.
(define (function->painter depth-fun)
  (define (integer-round f) (inexact->exact (round f)))
  (let ((integer-sequence (build-list viewport-size values)))
    (lambda (vp frame)
      (define (draw-at x y)
        (let* ((depth-at-xy (depth-fun x y))
               (color (+ (frame-z1 frame) (* (- (frame-z2 frame) (frame-z1 frame)) depth-at-xy))))
          ((draw-pixel vp) ((transform-posn frame) (make-posn x y))
                           (make-rgb color color color))))
      (for-each (lambda (y)
                  (for-each (lambda (x) (draw-at x y)) integer-sequence))
                integer-sequence))))


; Frame transformation factory.
(define (process-frame op frame)
  (let* ((p0 (frame-orig frame))
         (p1 (frame-x frame))
         (p2 (frame-y frame))
         (z1 (frame-z1 frame))
         (z2 (frame-z2 frame)))
    (cond ((eq? op 'bottom-frac)
           (lambda (frac)
             (make-frame (add-vect p0 (scale-vect (- 1 frac) p2))
                         p1
                         (scale-vect frac p2)
                         z1
                         z2)))
          ((eq? op 'top-frac)
           (lambda (frac)
             (make-frame p0
                         p1
                         (scale-vect frac p2)
                         z1
                         z2)))
          ((eq? op 'left)
           (make-frame p0
                       (scale-vect (/ 1 2) p1)
                       p2
                       z1
                       z2))
          ((eq? op 'right)
           (make-frame (add-vect p0 (scale-vect (/ 1 2) p1))
                       (scale-vect (/ 1 2) p1)
                       p2
                       z1
                       z2))
          ((eq? op 'flip-horiz)
           (make-frame (add-vect p0 p1)
                       (scale-vect -1 p1)
                       p2
                       z1
                       z2))
          ((eq? op 'flip-vert)
           (make-frame (add-vect p0 p2)
                       p1
                       (scale-vect -1 p2)
                       z1
                       z2))
          ((eq? op 'reduce2)
           (make-frame (add-vect p0 (add-vect (scale-vect -0.4 p1)
                                              (scale-vect 0.125 p2)))
                       (scale-vect 0.7071 p1)
                       (scale-vect 0.7071 p2)
                       z1
                       z2))
          ((eq? op 'rotate)
           (lambda (rad)
             (let ((cos-theta (cos rad))
                   (sin-theta (sin rad)))
               (define (rotate-posn p)
                 (make-posn (+ (* cos-theta (posn-x p))
                               (* sin-theta (posn-y p)))
                            (- (* cos-theta (posn-y p))
                               (* sin-theta (posn-x p)))))
               (let* ((half-gradient (scale-vect 1/2 (add-vect p1 p2)))
                      (center (add-vect (add-vect p0 half-gradient)
                                        (rotate-posn (scale-vect -1 half-gradient)))))
                 (make-frame center
                             (rotate-posn p1)
                             (rotate-posn p2)
                             z1
                             z2)))))
          ((eq? op 'rotate90)
           (make-frame (add-vect p0 p1)
                       p2
                       (scale-vect -1 p1)
                       z1
                       z2))
          ((eq? op 'deep-frac)
           (lambda (frac)
             (make-frame p0
                         p1
                         p2
                         (+ z1 (* (- z2 z1) frac))
                         z2)))
          ((eq? op 'shallow-frac)
           (lambda (frac)
             (make-frame p0
                         p1
                         p2
                         z1
                         (+ z1 (* (- z2 z1) frac)))))
          ((eq? op 'scale-independent)
           (lambda (ratio-x ratio-y)
             (let* ((gradient (add-vect p1 p2))
                    (scaled-gradient (make-posn (* (/ (- 1 ratio-x) 2) (posn-x gradient))
                                                (* (/ (- 1 ratio-y) 2) (posn-y gradient))))
                    (center (add-vect p0 scaled-gradient)))
               (make-frame center
                           (scale-vect ratio-x p1)
                           (scale-vect ratio-y p2)
                           z1
                           z2))))
          ((eq? op 'translate)
           (lambda (x y)
             (make-frame (add-vect p0 (make-posn x y))
                         p1
                         p2
                         z1
                         z2))))))

; Basic painter combinators

(define (stack painter1 painter2)
  (stack-frac 1/2 painter1 painter2))

(define (stack-frac frac painter1 painter2)
  (lambda (vp frame)
    (let* ((uf ((process-frame 'top-frac frame) frac))
           (lf ((process-frame 'bottom-frac frame) (- 1 frac))))
      (painter1 vp uf)
      (painter2 vp lf))))

(define (rotate rad painter)
  (lambda (vp frame)
    (painter vp ((process-frame 'rotate frame) rad))))

(define (eighth-turn-left painter)
  (rotate (/ pi 4) painter))

(define (quarter-turn-right painter)
  (lambda (vp frame)
    (painter vp (process-frame 'rotate90 frame))))

(define (flip-horiz painter)
  (lambda (vp frame)
    (painter vp (process-frame 'flip-horiz frame))))

(define (flip-vert painter)
  (lambda (vp frame)
    (painter vp (process-frame 'flip-vert frame))))

(define (overlay painter1 painter2)
  (overlay-frac 1/2 painter1 painter2))

(define (overlay-frac frac painter1 painter2)
  (lambda (vp frame)
    (if (or (< 1 frac) (< frac 0))
        (error "overlay-frac: invalid fraction")
        (let* ((df ((process-frame 'deep-frac frame) frac))
               (sf ((process-frame 'shallow-frac frame) frac)))
          (painter2 vp df)
          (painter1 vp sf)))))

(define (scale-independent ratio-x ratio-y painter)
  (lambda (vp frame)
    (painter vp ((process-frame 'scale-independent frame) ratio-x ratio-y))))

(define (scale ratio painter)
  (scale-independent ratio ratio painter))

; Translate the painter. Note that positive x means translate right,
; positive y means translate down.
(define (translate x y painter)
  (lambda (vp frame)
    (painter vp ((process-frame 'translate frame) x y))))


; Painter combinations defined in lecture 2
(define (turn-upside-down painter)
  (quarter-turn-right (quarter-turn-right painter)))

(define (quarter-turn-left painter)
  (turn-upside-down (quarter-turn-right painter)))

(define (beside painter1 painter2) 
  (quarter-turn-right 
   (stack (quarter-turn-left painter2) 
          (quarter-turn-left painter1))))

(define (make-cross painter)
  (stack (beside (quarter-turn-right painter)
                 (turn-upside-down painter))
         (beside painter
                 (quarter-turn-left painter))))

(define (repeat-pattern n pat pic)
  (if (= n 0)
      pic
      (pat (repeat-pattern (- n 1) pat pic))))

(define (stackn n painter)
  (if (= n 1)
      painter
      (stack-frac (/ 1 n) painter (stackn (- n 1) painter))))

; Stereogram generation function
(define (stereogram painter)
  (let* ((viewport-size 600)
         (unit-frame (make-frame (make-posn 0 0)
                                 (make-posn viewport-size 0)
                                 (make-posn 0 viewport-size)
                                 0
                                 1))
         (E 300) ; distance between eyes, 300 pixels
         (D 600) ; distance between eyes and image plane, 600 pixels
         (DELTA 40) ; stereo seperation
         (MAX_X (round (* 4/3 viewport-size))) ; stereogram viewport to be 3 times as wide to 
         (MAX_Y viewport-size)
         (MAX_Z 0)
         (CENTRE (round (/ MAX_X 2)))
         (stereo vp)
         (depth (open-pixmap "Depthmap Viewport" viewport-size viewport-size)))
    
    (define (get-depth x y) ; translates greyscale depthmap into numerical depth data.
                            ; white = -500 (furthest), black = -400 (nearest)
      (if (and (>= x (* 1/6 viewport-size)) (< x (- MAX_X (* 1/6 viewport-size))))
          (+ -400 (* -100 (rgb-red ((get-color-pixel depth) (make-posn (- x (* 1/6 viewport-size)) y)))))
          -500))
    
    (define (row-loop y) ; Generate stereogram row by row
      (let ((link-left (vector-fill! (make-vector MAX_X) #f))
            (link-right (vector-fill! (make-vector MAX_X) #f))
            (colours (vector-fill! (make-vector MAX_X) #f)))

        (define (column-loop x) ; Runs through a row
          (let* ((z (get-depth x y)) ; Obtain depth
                 (s (+ (* z (/ E (- z D))) DELTA)) ; Determine distance between intersection
                                                   ; of lines of sight on image plane
                 (left (- x (round (/ s 2)))) ; Determine coordinates of intersection of
                                              ; line of sight from left eye
                 (right (+ left (round s)))) ; Determine coordinates of intersection of
                                             ; line of sight from right eye
            (if (and (> left 0) (< right MAX_X)) ; Proceed if within bounds
                (if (and (or (not (vector-ref link-right left)) ; check if new contraint needs to be updated
                             (< s (vector-ref link-right left))) ; update only if no constraint exist or
                         (or (not (vector-ref link-left right)) ; if new constraint is of a smaller separation
                             (< s (vector-ref link-left right))))
                    (begin
                      (vector-set! link-right left (round s)) ; update right linkage
                      (vector-set! link-left right (round s))))) ; update left linkage
            (if (< (+ x 1) MAX_X) 
                (column-loop (+ x 1)))))
        
        (define (confirm x) ; compare right and left linkage vectors to eliminate overwritten constraints
          (let* ((s (vector-ref link-left x))
                 (s (if (not s)
                        +inf.0
                        x))
                 (d (if (< 0 (- x s))
                        (vector-ref link-right (- x s))
                        +inf.0)))
            (if (or (= s +inf.0) (> s d))
                (vector-set! link-left x 0))
            (if (< (+ x 1) MAX_X)
                (confirm (+ x 1)))))
        
        (define (paint x) ; paint the stereogram viewport
          (let* ((s (vector-ref link-left x))
                 (colour (vector-ref colours (- x s)))
                 (colour (if (not colour)
                             (make-rgb (/ (random 10) 9) (/ (random 10) 9) (/ (random 10) 9))
                             colour)))
;            (if (not (= s 0))
; if constraint linkage exists copy pixel, else create new random pixel
;                ((draw-pixel stereo) (make-posn x y)
; ((get-color-pixel stereo) (make-posn (- x s) y)))
;                ((draw-pixel stereo) (make-posn x y)
; (make-rgb (/ (random 10) 9) (/ (random 10) 9) (/ (random 10) 9)))) ; 1000 colours
            ((draw-pixel stereo) (make-posn x y) colour)
            (vector-set! colours x colour)
            (if (< (+ x 1) MAX_X)
                (paint (+ x 1)))))
        
        (column-loop 0)
        (confirm 0)
        (paint 0)
        
        (if (< (+ y 1) MAX_Y)
            (row-loop (+ y 1)))))
    
    (painter depth unit-frame)
    (row-loop 0)
    (display "done")
    ((save-pixmap stereo)
     (string-append "Stereogram" 
                    (number->string (remainder (current-seconds) 10000))
                    ".bmp") 'bmp) ; save stereogram
    (close-viewport depth)))

; Save File

(define (save-as-bmp name)
  ((save-pixmap vp) name 'bmp))

(define (save-as-xpm name)
  ((save-pixmap vp) name 'xpm))

; Some examples

;(stereogram (overlay (scale 0.8 heart-bb) circle-bb))
;(show (overlay-frac 0
;                        (overlay-frac 0 spiral-bb (quarter-turn-right spiral-bb))
;                        (quarter-turn-right
;                        (quarter-turn-right(overlay-frac 0 spiral-bb 
;                                                          (quarter-turn-right spiral-bb))))))


; (define pic (image->painter "cs1101s.jpg"))
; (define pic2 (overlay pic heart-bb))
; (show pic2)
; (stereogram pic2)

(define (square x) (* x x))

; Sample depth map.
; Note that radius1 should be < than radius2. Can you see why?
; How would you modify this such that the requirement is no longer necessary?
(define (create-conc-circle-zf radius1 depth1 radius2 depth2)
  (let ((a1^2 (square radius1))
        (a2^2 (square radius2)))
    (lambda (x y)
      (define (helper x y)
        (let ((x^2+y^2 (+ (square x) (square y))))
          (cond ((< x^2+y^2 a1^2) depth1)
                ((< x^2+y^2 a2^2) depth2)
                (else 1))))
      (helper (- x 300) (- y 300)))))
