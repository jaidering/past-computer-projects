;; The first three lines of this file were inserted by DrScheme. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname ps02) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;
; CS1101S --- Programming Methodology (Scheme)
; 
; PS02: Solutions to Homework Exercises
;
;
;===========
; Exercise 1
;===========

; ((draw-points-squeezed-on-window g1 200) unit-circle)
; ((draw-points-squeezed-on-window g2 200) alternative-unit-circle)
; the spacing between the points increases in the alternative-unit-circle 
; clockwise from the top
; the spacing in the unit-circle is constant


;===========
; Exercise 2
;===========

(define (reflect-through-y-axis curve)
  (lambda (t)
    (let ((ct (curve t)))
      (make-point
       (- (x-of ct))
       (y-of ct)))))


;===========
; Exercise 3
;===========

(define (connect-ends curve1 curve2)
    (connect-rigidly curve1
                     ((translate
                      (- (x-of (curve1 1)) (x-of (curve2 0)))
                      (- (y-of (curve1 1)) (y-of (curve2 0)))) curve2)))

; define vertical-line
(define (vertical-line point length)
           (lambda (t)
           (make-point (x-of point) 
                       (+ (y-of point) (* length t)))))


;test curves
(define curve1 (vertical-line (make-point 0.5 0.5) 0.4))
(define curve2 ((scale-x-y 0.5 0.5) unit-circle))

;===========
; Exercise 4
;===========

(define (show-points-gosper window level number-of-points initial-curve)
    ((draw-points-on window number-of-points)
     ((squeeze-rectangular-portion -.5 1.5 -.5 1.5)
      ((repeated gosperize level) initial-curve))))

(define (arc t)
  (make-point (sin (* pi t))
              (cos (* pi t))))

;===========
; Exercise 5
;===========

(define (param-gosperize theta)
    (lambda (curve)
      (put-in-standard-position
       (connect-ends
        ((rotate-around-origin theta) curve)
        ((rotate-around-origin (- theta)) curve)))))

; connect-ends code is from exercise 3


;===========
; Exercise 6
;===========

; Comparison of times show that param-gosper2 is the slowest:
; Level: 10
;   gosper-curve         <cpu time: 0 real time: 0>
;   param-gosper         <cpu time: 0 real time: 0>
;   your-param-gosper    <cpu time: 0 real time: 0>
;
; Level: 100
;   gosper-curve         <cpu time: 0 real time: 0>
;   param-gosper         <cpu time: 0 real time: 0>
;   your-param-gosper    <cpu time: 442 real time: 442>
;
; Level: 1000
;   gosper-curve         <cpu time: 15 real time: 16>
;   param-gosper         <cpu time: 16 real time: 16>
;   your-param-gosper    <cpu time: 42479 real time: 42630>
;
; More time is taken for param-gosper2 to compute the selected points and it
; increases as the level increases.
; There is no speed advantage in customized procedures.
; Instead they may be slower like param-gosper2 is the slowest among 
; gosper-curve, param-gosper and param-gosper2.


;===========
; Exercise 7
;===========

; Caleb's defination is correct.
;
; For caleb-rotate, the curve is computed twice (function by function) for every
; value of t as compared to rotate-around-origin. The number of functions computed
; form a binary tree and hence caleb-rotate has exponential time growth.
;
; Part of caleb-rotate:
; (lambda (t)
;     (let ((x (x-of (curve t)))   
;           (y (y-of (curve t))))
;
; Part of rotate-of-origin:
; (lambda (t)
;     (let ((ct (curve t))) 
;          (let ((x (x-of ct))
;                (y (y-of ct)))




;===========
; Exercise 8
;===========

; Fill in this table:
;
;                   level      rotate      caleb-rotate
;                     1         <3>            <4> 
;                     2         <5>            <10> 
;                     3         <7>            <22> 
;                     4         <9>            <46> 
;                     5         <11>           <94> 
;
; Evidence of exponential growth in caleb-rotate.


;===========
; Exercise 9
;===========

(define (dragonize n curve)
    (if (= n 0)
        curve
        (let ((c (dragonize (- n 1) curve)))
          (put-in-standard-position
           (connect-ends
            ((rotate-around-origin (- (/ pi 2))) (revert c))
            c)))))
