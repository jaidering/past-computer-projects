;; The first three lines of this file were inserted by DrScheme. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname lib) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  This is the file lib.scm
;;;;  Contains utility and clock functions for the OOP game
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Simple object system with inheritance

(define (ask object message . args)  ;; See your Scheme manual to explain `.'
  (let ((method (get-method object message)))
    (if (method? method)
	(apply method (cons object args))
	(error "No method" message (cadr method)))))

(define (get-method object message)
  (object message))

(define (no-method name)
  (list 'no-method name))

(define (method? x)
  (not (no-method? x)))

(define (no-method? x)
  (if (pair? x)      
      (eq? (car x) 'no-method)
      #f))

;;; -------------------------------------------------------------------

;;; symbol-append is available in MIT-GNU Scheme but not in DrScheme
(define (symbol-append sym1 sym2)
  (string->symbol (string-append
                   (symbol->string sym1)
                   (symbol->string sym2))))

;;; --------------------------------------------------------------------------

;;; Clock routines
(define *clock-list* '())
(define *the-time* 0)

(define (initialize-clock-list)
  (set! *clock-list* '())
  'initialized)

(define (add-to-clock-list person)
  (set! *clock-list* (cons person *clock-list*))
  'added)

(define (remove-from-clock-list person)
  (set! *clock-list* (delq person *clock-list*))  ;; DELQ defined below
  'removed)

(define (clock)
  (newline)
  (display "---Tick---")
  (set! *the-time* (+ *the-time* 1))
  (for-each (lambda (person) (ask person 'move))
	    *clock-list*)
  'tick-tock)

(define (current-time)
  *the-time*)

(define (run-clock n)
  (cond ((zero? n) 'done)
	(else (clock)
	      (run-clock (- n 1)))))

;;; --------------------------------------------------------------------------

;;; Miscellaneous procedures
(define (is-a object property)
  (let ((method (get-method object property)))
    (if (method? method)
	(ask object property)
	#f)))

(define (change-place mobile-object new-place)	; Since this bridges the gap
  (let ((old-place (ask mobile-object 'place))) ; between MOBILE-OBJECT and
    (ask mobile-object 'set-place new-place)	; PLACE, it is best it not
    (ask old-place 'del-thing mobile-object))	; be internal to either one.
  (ask new-place 'add-thing mobile-object)
  'place-changed)

(define (other-people-at-place person place)
  (filter (lambda (object)
	    (if (not (eq? object person))
		(is-a object 'person?)
		#f))
          (ask place 'things)))

(define (greet-people person people)
  (if (not (null? people))
      (ask person 'say
	   (cons "Hi"
		 (map (lambda (p) (ask p 'name))
                      people)))
      'sure-is-lonely-in-here))

(define (display-message list-of-stuff)
  (newline)
  (for-each (lambda (s) (display s) (display " "))
	    list-of-stuff)
  'message-displayed)

(define (random-neighbor place)
  (pick-random (ask place 'neighbors)))


(define (pick-random lst)
  (if (null? lst)
      #f      
      (list-ref lst (random (length lst)))))  ;; See manual for LIST-REF

(define (delq item lst)
  (cond ((null? lst) '())
	((eq? item (car lst)) (delq item (cdr lst)))
	(else (cons (car lst) (delq item (cdr lst))))))

;;; -------------------------------------------------------------------

;;; -------------------------------------------------------------------

;;; show-thing needs to be ported over to DrScheme

;(define (show thing)
;  (define (global-environment? frame)
;    (environment->package frame))
;  (define (pp-binding name value width)
;    (let ((value* (with-string-output-port
;		   (lambda (port)
;		     (if (pair? value)
;			 (pretty-print value port #F (+ width 2))
;			 (display value port))))))
;      (newline)
;      (display name)
;      (display ": ")
;      (display (make-string (- width (string-length name)) #\Space))
;      (if (pair? value)
;	  (display (substring value* (+ width 2) (string-length value*)))
;	  (display value*))))
;  (define (show-frame frame)
;    (if (global-environment? frame)
;	(display "\nGlobal Environment")
;	(let* ((bindings (environment-bindings frame))
;	       (parent   (environment-parent frame))
;	       (names    (cons "Parent frame"
;			       (map symbol->string (map car bindings))))
;	       (values   (cons (if (global-environment? parent)
;				   'global-environment
;				   parent)
;			       (map cadr bindings)))
;	       (width    (reduce max 0 (map string-length names))))
;	  (for-each (lambda (n v) (pp-binding n v width))
;                    names values))))
;  (define (show-procedure proc)
;    (fluid-let ((*unparser-list-depth-limit* 4)
;		(*unparser-list-breadth-limit* 4))
;      (newline)
;      (display "Frame:")
;      (newline)
;      (display "  ")
;      (if (global-environment? (procedure-environment proc))
;	  (display "Global Environment")
;	  (display (procedure-environment proc)))
;      (newline)
;      (display "Body:")
;      (newline)
;      (pretty-print (procedure-lambda proc) (current-output-port) #T 2)))
;  
;  (define (print-nicely thing)
;    (newline)
;    (display thing)
;    (cond ((equal? #f thing)
;	   'uninteresting)
;	  ((environment? thing)
;	   (show-frame thing))
;	  ((compound-procedure? thing)
;	   (show-procedure thing))
;	  (else 'uninteresting)))
;  
;  (print-nicely
;   (or (if (integer? thing)
;	   (object-unhash thing)
;	   thing)
;       thing)))
