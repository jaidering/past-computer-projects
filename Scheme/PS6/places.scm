;; The first three lines of this file were inserted by DrScheme. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname places) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  This is the file places.scm
;;;;  Defines the places in the game world
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;
;;  Code for adventure game
;; 
;;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++



(initialize-clock-list)

;; Here we define the places in our world...

;;------------------------------------------
(define central-library (make-place 'central-library))
(define forum (make-place 'forum))
(define lt15 (make-place 'lt15))
(define arts-canteen (make-place 'arts-canteen))
(define com1-open-area (make-place 'com1-open-area))
(define sr1 (make-place 'sr1))
(define com1-classrooms (make-place 'com1-classrooms))
(define office-of-the-president (make-place 'office-of-the-president))
(define caleb-laboratory (make-place 'caleb-laboratory))
(define com1-lift-lobby-top (make-place 'com1-lift-lobby-top))
(define com1-lift-lobby-bottom (make-place 'com1-lift-lobby-bottom))
(define com1-ladies (make-place 'com1-ladies))
(define com1-gents (make-place 'com1-gents))
(define secret-portal (make-place 'secret-portal))
(define ben-office (make-place 'ben-office))
(define technical-services (make-place 'technical-services))
(define com1-research-labs (make-place 'com1-research-labs))
(define com1-staircase-top (make-place 'com1-staircase-top))
(define com1-student-area (make-place 'com1-student-area))
(define enchanted-garden (make-place 'enchanted-garden))
(define data-comm-lab2 (make-place 'data-comm-lab2))
(define com1-staircase-bottom (make-place 'com1-staircase-bottom))
(define secret-oneway-portal (make-place 'secret-portal))



;; One-way paths connect individual places in the world.

;;------------------------------------------------------

(define (can-go from direction to)
  (ask from 'add-neighbor direction to))

(define (can-go-both-ways from direction reverse-direction to)
  (can-go from direction to)
  (can-go to reverse-direction from))


(can-go-both-ways forum 'up 'down central-library)
(can-go-both-ways office-of-the-president 'north 'south central-library)
(can-go-both-ways lt15 'north 'south forum)
(can-go-both-ways arts-canteen 'east 'west lt15)
(can-go-both-ways lt15 'up 'down office-of-the-president)
(can-go-both-ways com1-open-area 'north 'south lt15)
(can-go-both-ways com1-open-area 'east 'west sr1)
(can-go-both-ways com1-classrooms 'north 'south com1-open-area)
(can-go-both-ways caleb-laboratory 'north 'south com1-classrooms)
(can-go-both-ways com1-lift-lobby-top 'east 'west com1-open-area)
(can-go-both-ways com1-lift-lobby-bottom 'up 'down com1-lift-lobby-top)
(can-go-both-ways com1-ladies 'north 'south com1-lift-lobby-bottom)
(can-go-both-ways com1-gents 'north 'south com1-ladies)
(can-go-both-ways secret-portal 'up 'down com1-gents)
(can-go secret-portal 'west ben-office)
(can-go-both-ways ben-office 'east 'west secret-oneway-portal)
(can-go secret-oneway-portal 'down com1-staircase-bottom)
(can-go-both-ways com1-lift-lobby-bottom 'east 'west technical-services)
(can-go-both-ways com1-research-labs 'north 'south technical-services)
(can-go-both-ways com1-staircase-top 'north 'south com1-research-labs)
(can-go-both-ways com1-student-area 'up 'down technical-services)
(can-go-both-ways com1-student-area 'east 'west enchanted-garden)
(can-go-both-ways data-comm-lab2 'north 'south com1-student-area)
(can-go-both-ways com1-staircase-bottom 'north 'south data-comm-lab2)
(can-go-both-ways com1-staircase-bottom 'up 'down com1-staircase-top)