;; The first three lines of this file were inserted by DrScheme. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname classes) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  This is the file classes.scm
;;;;  Contains definition of various classes used by
;;;;  game.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Persons, places, and things will all be kinds of named objects
(define (make-named-object name)
  (lambda (message) 
    (case message
      ((name) (lambda (self) name))
      (else (no-method name)))))

;;; Persons and things are mobile since their places can change
(define (make-mobile-object name location)
  (let ((named-obj (make-named-object name)))
    (lambda (message)
      (case message
        ((place) (lambda (self) location))
        ((install)
         (lambda (self)
           (ask location 'add-thing self)))
        ;; Following method should never be called by the user...
        ;;  it is a system-internal method.
        ;; See CHANGE-PLACE instead
        ((set-place)
         (lambda (self new-place)
           (set! location new-place)
           'place-set))
        (else (get-method named-obj message))))))

(define (make&install-mobile-object name place)
  (let ((mobile-obj (make-mobile-object name place)))
    (ask mobile-obj 'install) mobile-obj))

;;; A thing is something that can be owned
(define (make-thing name birthplace)
  (let ((owner 'nobody)
	(mobile-obj (make-mobile-object name birthplace)))
    (lambda (message)
      (case message
        ((owner) (lambda (self) owner))
        ((ownable?) (lambda (self) true))
        ((owned?) (lambda (self) (not (eq? owner 'nobody))))
        ;; Following method should never be called by the user...
        ;;  it is a system-internal method.
        ;; See TAKE and LOSE instead.
        ((set-owner)
         (lambda (self new-owner)
           (set! owner new-owner)
           'owner-set))
        (else (get-method mobile-obj message))))))

(define (make&install-thing name birthplace)	
  (let ((thing (make-thing name birthplace)))
    (ask thing 'install)
    thing))

;;; Implementation of places
(define (make-place name)
  (let ((neighbor-map '())		
	(things       '())
	(named-obj (make-named-object name)))
    (lambda (message)
      (case message
        ((things) (lambda (self) things))
        ((neighbors)
         (lambda (self) (map cdr neighbor-map)))
        ((exits)
         (lambda (self) (map car neighbor-map)))
        ((neighbor-towards)
         (lambda (self direction)
           (let ((places (assq direction neighbor-map)))
             (if places
                 (cdr places)
                 #f))))
        ((add-neighbor)
         (lambda (self direction new-neighbor)
           (cond ((assq direction neighbor-map)
                  (display-message (list "Direction already assigned"
                                         direction name))
                  #f)
                 (else (set! neighbor-map
                            (cons (cons direction new-neighbor) neighbor-map))
                      #t))))
        ((accept-person?)
         (lambda (self person)
           #t))
        ;; Following two methods should never be called by the user...
        ;;  they are system-internal methods. See CHANGE-PLACE instead.
        ((add-thing)
         (lambda (self new-thing)
           (cond ((memq new-thing things)
                  (display-message (list (ask new-thing 'name)
                                         "is already at" name))
                  #f)
                 (else (set! things (cons new-thing things))
                       #t))))
        ((del-thing)
         (lambda (self thing)
           (cond ((not (memq thing things))
                  (display-message (list (ask thing 'name)
                                         "is not at" name))
                  #f)
                 (else (set! things (delq thing things))	;; DELQ defined
                       #t))))                                   ;; below
        (else (get-method named-obj message))))))

;;; ----------------------------------------------------------------------------
;;; Implementation of people


(define (make-person name birthplace threshold)
  (let ((possessions '())
	(mobile-obj  (make-mobile-object name birthplace)))
    (lambda (message)
      (case message
        ((person?) (lambda (self) true))
        ((possessions) (lambda (self) possessions))
        ((say)
         (lambda (self list-of-stuff)
           (display-message
            (append (list "At" (ask (ask self 'place) 'name)
                          ":"  name "says --")
                    (if (null? list-of-stuff)
                        '("Oh, nevermind.")
                        list-of-stuff)))
           'said))
        ((have-fit)
         (lambda (self)
           (ask self 'say '("Yaaaah! I am upset!"))
           'I-feel-better-now))
        ((look-around)
         (lambda (self)
           (let ((other-things
                  (map (lambda (thing) (ask thing 'name))
                       (delq self                       ;; DELQ
                             (ask (ask self 'place)     ;; defined
                                  'things)))))          ;; below
             (ask self 'say (cons "I see" (if (null? other-things)
                                              '("nothing")
                                              other-things)))
             other-things)))           
        ((take)
         (lambda (self thing)
           (cond ((symbol? thing) ; referencing object by name 
                  (let ((obj (find-object thing (ask (ask self 'place) 'things))))
                    (if (null? obj)
                        #f
                        (ask self 'take obj))))
                 ((memq thing possessions)
                  (ask self 'say
                       (list "I already have" (ask thing 'name)))
                  
                  #t)
                 ((and (let ((things-at-place (ask (ask self 'place) 'things)))
                         (memq thing things-at-place))
                       (is-a thing 'ownable?))
                  (if (not (ask thing 'owned?))
                      (begin
                        (ask thing 'set-owner self)
                        (set! possessions (cons thing possessions))
                        (ask self 'say
                             (list "I take" (ask thing 'name))))
                      
                      'unowned)
                  #t)
                 (else
                  (display thing)
                  (display-message
                   (list "You cannot take" (ask thing 'name)))
                  #f))))
        ((lose)
         (lambda (self thing)
           (cond ((symbol? thing) ; referencing object by name 
                  (let ((obj (find-object thing (ask (ask self 'place) 'things))))
                    (if (null? obj)
                        #f
                        (ask self 'lose obj))))
                 ((eq? self (ask thing 'owner))
                  (set! possessions (delq thing possessions)) ;; DELQ
                  (ask thing 'set-owner 'nobody)              ;; defined
                  (ask self 'say                              ;; below
                       (list "I lose" (ask thing 'name)))
                  #t)
                 (else
                  (display-message (list name "does not own"
                                         (ask thing 'name)))
                 #f))))
        ((move)
         (lambda (self)
           (cond ((= (random threshold) 0)
                  (ask self 'act)
                  #t))))
        ((act)
         (lambda (self)
           (let ((new-place (random-neighbor (ask self 'place))))
             (if new-place
                 (ask self 'move-to new-place)
                 #f))))		; All dressed up and no place to go
        ((move-to)
         (lambda (self new-place)
           (let ((old-place (ask self 'place)))
             (cond ((eq? new-place old-place)
                    (display-message (list name "is already at"
                                           (ask new-place 'name)))
                    #f)
                   ((ask new-place 'accept-person? self)
                    (change-place self new-place)
                    (for-each (lambda (p) (change-place p new-place))
                              possessions)
                    (display-message
                     (list name "moves from" (ask old-place 'name)
                           "to"         (ask new-place 'name)))
                    (greet-people self (other-people-at-place self new-place))
                    #t)
                   (else
                    (display-message (list name "can't move to"
                                           (ask new-place 'name))))))))
        ((go)
         (lambda (self direction)
           (let ((old-place (ask self 'place)))
             (let ((new-place (ask old-place 'neighbor-towards direction)))
               (cond (new-place
                      (ask self 'move-to new-place))
                     (else
                      (display-message (list "You cannot go" direction
                                             "from" (ask old-place 'name)))
                      #f))))))
        ((install)
         (lambda (self)
           (add-to-clock-list self)
           ((get-method mobile-obj 'install) self)))
        (else (get-method mobile-obj message))))))

(define (make&install-person name birthplace threshold)
  (let ((person (make-person name birthplace threshold)))
    (ask person 'install)
    person))

;;; A troll is a kind of person (but not a kind person!)
;; However we don't need the troll in this problem set!
;(define (make-troll name birthplace threshold)
;  (let ((person (make-person name birthplace threshold)))
;    (lambda (message)
;      (case message
;        ((act)
;         (lambda (self)
;           (let ((others (other-people-at-place self (ask self 'place))))
;             (if (not (null? others))
;                 (ask self 'eat-person (pick-random others))
;                 ((get-method person 'act) self)))))
;        ((eat-person)
;         (lambda (self person)
;           (ask self 'say
;                (list "Growl.... I'm going to eat you,"
;                      (ask person 'name)))
;           (go-to-heaven person)
;           (ask self 'say
;                (list "Chomp chomp." (ask person 'name)
;                      "tastes yummy!"))
;           '*burp*))
;        (else (get-method person message))))))
;
;(define (make&install-troll name birthplace threshold)
;  (let ((troll  (make-troll name birthplace threshold)))
;    (ask troll 'install)
;    troll))

(define (go-to-heaven person)
  (for-each (lambda (item) (ask person 'lose item))
	    (ask person 'possessions))
  (ask person 'say
       '("
                   Dulce et decorum est 
                   pro computatore mori!"
	 ))
  (ask person 'move-to heaven)
  (remove-from-clock-list person)
  'game-over-for-you-dude)

(define (association-procedure proc select)
  (define (helper lst)
    (cond ((null? lst) '())
          ((proc (car lst)) (select (car lst)))
          (else (helper (cdr lst)))))
  (lambda (list)
    (helper list)))

(define find-object
  (lambda (name objlist)
    ((association-procedure
      (lambda (obj) (equal? (ask obj 'name) name))
      (lambda (obj) obj)) objlist)))

(define heaven (make-place 'heaven))		; The point of no return

