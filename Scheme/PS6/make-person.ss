;; The first three lines of this file were inserted by DrScheme. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname make-person) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;;;---------------------------------------------
;;; make-person-modified

(define (make-person name birthplace threshold)
  (let ((possessions '())
	(mobile-obj  (make-mobile-object name birthplace)))
    (lambda (message)
      (case message
        ((person?) (lambda (self) true))
        ((possessions) (lambda (self) possessions))
        ((say)
         (lambda (self list-of-stuff)
           (display-message
            (append (list "At" (ask (ask self 'place) 'name)
                          ":"  name "says --")
                    (if (null? list-of-stuff)
                        '("Oh, nevermind.")
                        list-of-stuff)))
           'said))
;       ((have-fit)
;        (lambda (self)
;          (ask self 'say '("Yaaaah! I am upset!"))
;          'I-feel-better-now))
        ((look-around)
         (lambda (self)
           (let ((other-things
                  (map (lambda (thing) (ask thing 'name))
                       (delq self                       ;; DELQ
                             (ask (ask self 'place)     ;; defined
                                  'things)))))          ;; below
             (ask self 'say (cons "I see" (if (null? other-things)
                                              '("nothing")
                                              other-things)))
             other-things)))           
        ((take)
         (lambda (self thing)
           (cond ((symbol? thing) ; referencing object by name 
                  (let ((obj (find-object thing (ask (ask self 'place) 'things))))
                    (if (null? obj)
                        #f
                        (ask self 'take obj))))
                 ((memq thing possessions)
                  (ask self 'say
                       (list "I already have" (ask thing 'name)))
                  
                  #t)
                 ((and (let ((things-at-place (ask (ask self 'place) 'things)))
                         (memq thing things-at-place))
                       (is-a thing 'ownable?))
                  (if (not (ask thing 'owned?))
                      (begin
                        (ask thing 'set-owner self)
                        (set! possessions (cons thing possessions))
                        (ask self 'say
                             (list "I take" (ask thing 'name))))
                      
                      'unowned)
                  #t)
                 (else
                  (display thing)
                  (display-message
                   (list "You cannot take" (ask thing 'name)))
                  #f))))
        ((lose)
         (lambda (self thing)
           (cond ((symbol? thing) ; referencing object by name 
                  (let ((obj (find-object thing (ask (ask self 'place) 'things))))
                    (if (null? obj)
                        #f
                        (ask self 'lose obj))))
                 ((eq? self (ask thing 'owner))
                  (set! possessions (delq thing possessions)) ;; DELQ
                  (ask thing 'set-owner 'nobody)              ;; defined
                  (ask self 'say                              ;; below
                       (list "I lose" (ask thing 'name)))
                  #t)
                 (else
                  (display-message (list name "does not own"
                                         (ask thing 'name)))
                 #f))))

        ((move-to)
         (lambda (self new-place)
           (let ((old-place (ask self 'place)))
             (cond ((eq? new-place old-place)
                    (display-message (list name "is already at"
                                           (ask new-place 'name)))
                    #f)
                   ((ask new-place 'accept-person? self)
                    (change-place self new-place)
                    (for-each (lambda (p) (change-place p new-place))
                              possessions)
                    (display-message
                     (list name "moves from" (ask old-place 'name)
                           "to"         (ask new-place 'name)))
                    (greet-people self (other-people-at-place self new-place))
                    #t)
                   (else
                    (display-message (list name "can't move to"
                                           (ask new-place 'name))))))))
        ((go)
         (lambda (self direction)
           (let ((old-place (ask self 'place)))
             (let ((new-place (ask old-place 'neighbor-towards direction)))
               (cond (new-place
                      (ask self 'move-to new-place))
                     (else
                      (display-message (list "You cannot go" direction
                                             "from" (ask old-place 'name)))
                      #f))))))))))

;;;----------------------------------------------------
;;; make-generic-npc

(define (make-generic-npc name birthplace threshold possessions)
  (let ((person (make-person name birthplace threshold)))
    (lambda (message)
      (case message
        ((npc?) (lambda (self) true))
        ((move)
         (lambda (self)
           (cond ((= (random threshold) 0)
                  (ask self 'act)
                  #t))))
        ((act)
         (lambda (self)
           (let ((new-place (random-neighbor (ask self 'place))))
             (if new-place
                 (ask self 'move-to new-place)
                 #f))))		; All dressed up and no place to go
        ((install)
         (lambda (self)
           (add-to-clock-list self)
           ((get-method person 'install) self)))
        (else (get-method person message))))))
