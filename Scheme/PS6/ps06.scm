;; The first three lines of this file were inserted by DrScheme. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname ps06) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
(load "ps6.scm")




;;;
;;; Exercise 1
;;;  - Warm Up
;;;



; (a) duc hiep is more restless
; (b) it occurs 1 in 6 times (the threshold)
; (c) it is random



;;;
;;; Exercise 2
;;;  - Now we're getting started
;;;



(define (make-person name birthplace)
  (let ((possessions '())
	(mobile-obj  (make-mobile-object name birthplace)))
    (lambda (message)
      (case message
        ((person?) (lambda (self) true))
        ((possessions) (lambda (self) possessions))
        ((say)
         (lambda (self list-of-stuff)
           (display-message
            (append (list "At" (ask (ask self 'place) 'name)
                          ":"  name "says --")
                    (if (null? list-of-stuff)
                        '("Oh, nevermind.")
                        list-of-stuff)))
           'said))
        ((look-around)
         (lambda (self)
           (let ((other-things
                  (map (lambda (thing) (ask thing 'name))
                       (delq self                       ;; DELQ
                             (ask (ask self 'place)     ;; defined
                                  'things)))))          ;; below
             (ask self 'say (cons "I see" (if (null? other-things)
                                              '("nothing")
                                              other-things)))
             other-things)))           
        ((take)
         (lambda (self thing)
           (cond ((symbol? thing) ; referencing object by name 
                  (let ((obj (find-object thing (ask (ask self 'place) 'things))))
                    (if (null? obj)
                        #f
                        (ask self 'take obj))))
                 ((memq thing possessions)
                  (ask self 'say
                       (list "I already have" (ask thing 'name)))
                  
                  #t)
                 ((and (let ((things-at-place (ask (ask self 'place) 'things)))
                         (memq thing things-at-place))
                       (is-a thing 'ownable?))
                  (if (not (ask thing 'owned?))
                      (begin
                        (ask thing 'set-owner self)
                        (set! possessions (cons thing possessions))
                        (ask self 'say
                             (list "I take" (ask thing 'name))))
                      
                      'unowned)
                  #t)
                 (else
                  (display thing)
                  (display-message
                   (list "You cannot take" (ask thing 'name)))
                  #f))))
        ((lose)
         (lambda (self thing)
           (cond ((symbol? thing) ; referencing object by name 
                  (let ((obj (find-object thing (ask (ask self 'place) 'things))))
                    (if (null? obj)
                        #f
                        (ask self 'lose obj))))
                 ((eq? self (ask thing 'owner))
                  (set! possessions (delq thing possessions)) ;; DELQ
                  (ask thing 'set-owner 'nobody)              ;; defined
                  (ask self 'say                              ;; below
                       (list "I lose" (ask thing 'name)))
                  #t)
                 (else
                  (display-message (list name "does not own"
                                         (ask thing 'name)))
                 #f))))

        ((move-to)
         (lambda (self new-place)
           (let ((old-place (ask self 'place)))
             (cond ((eq? new-place old-place)
                    (display-message (list name "is already at"
                                           (ask new-place 'name)))
                    #f)
                   ((ask new-place 'accept-person? self)
                    (change-place self new-place)
                    (for-each (lambda (p) (change-place p new-place))
                              possessions)
                    (display-message
                     (list name "moves from" (ask old-place 'name)
                           "to"         (ask new-place 'name)))
                    (greet-people self (other-people-at-place self new-place))
                    #t)
                   (else
                    (display-message (list name "can't move to"
                                           (ask new-place 'name))))))))
        ((go)
         (lambda (self direction)
           (let ((old-place (ask self 'place)))
             (let ((new-place (ask old-place 'neighbor-towards direction)))
               (cond (new-place
                      (ask self 'move-to new-place))
                     (else
                      (display-message (list "You cannot go" direction
                                             "from" (ask old-place 'name)))
                      #f))))))
         (else (get-method mobile-obj message))))))


(define (make-generic-npc name birthplace threshold)
  (let ((person (make-person name birthplace))
        (possessions '()))
    (lambda (message)
      (case message
        ((npc?) (lambda (self) true))
        ((move)
         (lambda (self)
           (cond ((= (random threshold) 0)
                  (ask self 'act)
                  #t))))
        ((act)
         (lambda (self)
           (let ((new-place (random-neighbor (ask self 'place))))
             (if new-place
                 (ask self 'move-to new-place)
                 #f))))		; All dressed up and no place to go
        ((install)
         (lambda (self)
           (add-to-clock-list self)
           ((get-method person 'install) self)))
        (else (get-method person message))))))


;;; revised version

(define (make&install-person name birthplace)
  (let ((person (make-person name birthplace)))
    (ask person 'install)
    person))


(define (make&install-npc name birthplace threshold)
  (let ((npc (make-generic-npc name birthplace threshold)))
    (ask npc 'install)
    npc))
         


;;;
;;; Exercise 3
;;;  - Restricted Access
;;;


(define (make-president-office name)
  (let ((place (make-place name)))
    (lambda (message)
      (case message
        ((accept-person?)
         (lambda (self person)
           (let ((possessions (ask person 'possessions)))
             (if (null? (filter (lambda (x) (is-a x 'key-card?)) possessions)) #f #t)))) 
        (else (get-method place message))))))

         
         


;;; Change jon's office to a president office

(define office-to-a-president (make-president-office 'office-to-a-president))


;;;
;;; Exercise 4
;;;  - Jon, the President
;;;



(define (make-jonathan-hay name birthplace threshold)
  (let ((npc (make-generic-npc name birthplace threshold))
        (possessions '()))
  (lambda (message)
      (case message
        ((move)
         (lambda (self)
           (let* ((place (ask self 'place))
                  (my-things (ask self 'possessions))
                  (place-things (ask place 'things))
                  (place-things-key-card-only (filter (lambda (x) (is-a x 'key-card?)) place-things))
                  (key-cards-not-owned (filter (lambda (x) (not (ask x 'owned?))) place-things-key-card-only)))
;             (display-message (list (map (lambda (x) (ask x 'name)) key-cards-not-owned)))
             (cond ((not (null? key-cards-not-owned))
                    (ask self 'say (list "Oh! There is a key-card lying on the floor."))
                    (for-each (lambda (x) (ask self 'take x)) key-cards-not-owned)
                    #t)
                    (else (= (random threshold) 0)
                          (ask self 'act)
                          #t)))))
                    (else (get-method npc message))))))

    
        
(define (make&install-jon name birthplace threshold)
  (let ((jon (make-jonathan-hay name birthplace threshold)))
    (ask jon 'install)
    jon))


;;; Make jon a jon-object

(define jon (make&install-jon 'jon office-of-the-president 2))


;;; Transcript of what happens with a few (clock) ticks
;
;---Tick---
;caleb moves from caleb-laboratory to com1-classrooms 
;At com1-classrooms : caleb says -- Hi duc-hiep 
;duc-hiep moves from com1-classrooms to com1-open-area 
;At office-of-the-president : jon says -- Oh! There is a key-card lying on the floor. 
;At office-of-the-president : jon says -- I take jon-card tick-tock
;
;---Tick---
; 
;jon moves from office-of-the-president to lt15 tick-tock
;
;---Tick---
;duc-hiep moves from com1-open-area to sr1  
;jon moves from lt15 to com1-open-area tick-tock


;;;
;;; Exercise 5
;;;  - Hide-and-Seek  
;;;







;;; Backup card definition   

(define backup-card
  (make&install-key-card 'backup-card (if (= (random 2) 0) enchanted-garden ben-office) 'BACKUP))  

  

;;; Definitions to take control of Caleb
(define caleb (make&install-person 'caleb caleb-laboratory))   

(define you caleb)



;;;
;;; Exercise 6
;;;  - Booby Trap  
;;;

(define alarm #f)

(define (make-security-bot name birthplace threshold)
    (let ((npc (make-generic-npc name birthplace threshold)))
        (lambda (message)
          (case message
            ((act)
             (lambda (self)
               (let* ((place (ask self 'place))
                      (others (other-people-at-place self place))
                      (find-possessions (map (lambda (person) (ask person 'possessions)) others))
                      (find-key-card (filter (lambda (lst) 
                                               (not (null? (filter (lambda (x) (is-a x 'key-card?)) lst))))
                                             find-possessions))) ; for each bag kill the owner of the 1st item of the bag 
                 (if (null? find-key-card)
                     ((get-method npc message) self)
                     (for-each (lambda (lst)
                                 (if (not (eq? (ask (car lst) 'owner) jon))
                                     (ask self 'zap-person (ask (car lst) 'owner))) find-key-card))))))
            
            
            ((zap-person)
             (lambda (self person)
               (for-each (lambda (item) (ask person 'lose item))
                         (ask person 'possessions))
               (ask person 'say
                    '("
                   Dulce et decorum est 
                   pro computatore mori!"
                      ))
               (ask person 'move-to heaven)
               (remove-from-clock-list person)
               'game-over-for-you-dude))
            (else (get-method npc message))))))
  
(define (make&install-security-bot name birthplace threshold)
  (let ((security-bot  (make-security-bot name birthplace threshold)))
    (ask security-bot 'install)
    security-bot))


(define (make-key-card name birthplace serial)
  (let ((id serial)
        (thing (make-thing name birthplace)))
    (lambda (message)
      (case message
        ((set-owner) (lambda (self new-owner)
                       (if (and (not (eq? new-owner jon)) ; to check if the new owner is jon
                                (not alarm))
                           (begin (make&install-security-bot 'security-bot lt15 1)
                                  (set! alarm #t)
                                  display-message (list "INTRUDER! INTRUDER!")))
                       ((get-method thing 'set-owner) self new-owner)))
        ((key-card?) (lambda (self) true))
        ((card-serial) (lambda (self) id))
        (else (get-method thing message))))))  



(define master-card
  (make&install-key-card 'jon-card office-of-the-president 'MASTER)) 

(define backup-card
  (make&install-key-card 'backup-card (pick-random (list enchanted-garden
                                                        ben-office)) 'BACKUP)) 










;;;
;;; Exercise 7
;;;  - Threat Level: Code Rainbow
;;;

   
 
(define (make-security-bot name birthplace threshold)
    (let ((npc (make-generic-npc name birthplace threshold))
          (counter 0))
        (lambda (message)
          (case message
            ((security-bot?) (lambda (self) true))
            ((act)
             (lambda (self)
               (let* ((place (ask self 'place))
                      (others (other-people-at-place self place))
                      (find-possessions (map (lambda (person) (ask person 'possessions)) others))
                      (find-key-card (filter (lambda (lst) 
                                               (not (null? (filter (lambda (x) (is-a x 'key-card?)) lst))))
                                             find-possessions))
                      (num-bots (filter (lambda (x) (is-a x 'security-bot?)) others)))
                  (if (or (< (length num-bots) 3)   ;; to see the number of bots and whether they sld clone according to the probability
                         (= (random 5) 0))
                     (if (= counter 5)             ;; cloning part
                         (begin (set! counter 0)
                                (make&install-security-bot name (ask self 'place) 1))
                         (set! counter (+ counter 1))))
                   (if (null? find-key-card)  ;; find the person who has the key-card and zap him if it's not jon
                     ((get-method npc message) self)
                     (for-each (lambda (lst)
                                 (if (not (eq? (ask (car lst) 'owner) jon))
                                     (ask self 'zap-person (ask (car lst) 'owner))) find-key-card))))))
            ((zap-person)          ;;zapping the thief
             (lambda (self person)
               (for-each (lambda (item) (ask person 'lose item))
                         (ask person 'possessions))
               (ask person 'say
                    '("
                   Dulce et decorum est 
                   pro computatore mori!"
                      ))
               (ask person 'move-to heaven)
               (remove-from-clock-list person)
               'game-over-for-you-dude))
            (else (get-method npc message))))))








;;;
;;; Exercise 8
;;;  - The Intelligent Bots  
;;;

(define (make-security-bot name birthplace threshold)
  (let ((npc (make-generic-npc name birthplace threshold))
        (counter 0)
        (place-bank (list office-of-the-president)))
    (lambda (message)
      (case message
        ((security-bot?) (lambda (self) true))  ;; to test if it's a bot
        ((act)
         (lambda (self)
           (let* ((place (ask self 'place))
                  (others (other-people-at-place self place))
                  (find-possessions (map (lambda (person) (ask person 'possessions)) others))
                  (find-key-card (filter (lambda (lst) 
                                           (not (null? (filter (lambda (x) (is-a x 'key-card?)) lst))))
                                         find-possessions))
                  (num-bots (filter (lambda (x) (is-a x 'security-bot?)) others))
                  (neighbors (ask place 'neighbors))
                  (not-visited-place (filter (lambda (x) (not (memq x place-bank))) neighbors))
                  (new-place (pick-random not-visited-place)))
             (if (or (< (length num-bots) 3) 
                     (= (random 5) 0))
                 (if (= counter 5)
                     (begin (set! counter 0)
                            (make&install-security-bot name (ask self 'place) 1))
                     (set! counter (+ counter 1))))
             (if (null? find-key-card)
                 (begin (set! not-visited-place (cons new-place place-bank)) ;; the place that the bot goes to is registered to the list of the places it has been to
                        (ask self 'move-to new-place))
                 (for-each (lambda (lst)
                             (if (not (eq? (ask (car lst) 'owner) jon))
                                 (ask self 'zap-person (ask (car lst) 'owner))) find-key-card))))))
        ((zap-person)
         (lambda (self person)
           (for-each (lambda (item) (ask person 'lose item))
                     (ask person 'possessions))
           (ask person 'say
                '("
                   Dulce et decorum est 
                   pro computatore mori!"
                  ))
           (ask person 'move-to heaven)
           (remove-from-clock-list person)
           'game-over-for-you-dude))
        (else (get-method npc message))))))







;;;
;;; Exercise 9a
;;;  - Now you see Caleb, now you don't
;;;



; Be sure to include comments explaining your choice of implementation
; as well as advantages/disadvantages and assumptions you have made.

(define (convert direction)
  (case direction
    ((#\n) 'north)
    ((#\s) 'south)
    ((#\e) 'east)
    ((#\w) 'west)
    ((#\u) 'up)
    ((#\d) 'down)))

(define square (lambda (x) (* x x)))

(define (make-sprinter name birthplace)
  (let ((person (make-person name birthplace))
        (cooldown 0))
    (lambda (message)
      (case message
        ((move)
            (lambda (self)
              (set! cooldown (- cooldown 1)))) ;; the counter for its cool down and until it's zero it will not sprint again
        ((sprint)  
         (lambda (self direction)
           (let ((dir (string->list (symbol->string direction))))
             (if (= cooldown 0)  ;; if the counter is zero it'll be able to sprint
                 (if (< (length dir) 4) ;; max direction for sprinting is 3
                     (begin (for-each (lambda (x) (ask self 'go x)) (map convert dir))
                            (set! cooldown (square (- 1 (length dir)))))
                     (display-message (list "invalid move")))))))
        ((install)
         (lambda (self)
           (add-to-clock-list self)
           ((get-method person 'install) self)))
        (else (get-method person message))))))
        
(define (make&install-sprinter name birthplace)
  (let ((sprinter (make-sprinter name birthplace)))
    (ask sprinter 'install)
    sprinter))



(define caleb (make&install-sprinter 'caleb caleb-laboratory))
(define you caleb)


  

;;;
;;; Exercise 9b
;;;  - Now you see Caleb, now you can't
;;;



; Be sure to include comments explaining your choice of implementation
; as well as advantages/disadvantages and assumptions you have made.

  

; your answer here

  

  

;;;
;;; Exercise 10 (Optional)
;;;  - Polymorphism in Scheme
;;;

  

(define (make-person ; arguments, if any, are not provided

         )

  (lambda (message)

      (case message

        ((say)

         ; your answer here

         )

        (else ; complete the else block

         ))))