;; The first three lines of this file were inserted by DrScheme. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname |trial answers|) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;;; Exe 1

(define move-forward-10cm
  '((setpower a 7)
    (setpower c 7)
    (forward a)
    (forward c)
    (sleep ___)
    (stop a)
    (stop c)
    ))


;; get speed of motors with sample of 3 secs
(define move-forward-3cm
  '((setpower a 7)
    (setpower c 7)
    (forward a)
    (forward c)
    (sleep 3000)
    (stop a)
    (stop c)
    ))

;;; find distance then calculate speed
;; calculating speed
(define cal-speed
  (lambda (d t) (/ d t)))

;;; find time taken to move 10cm
;; calculating time taken
(define cal-time
  (lambda (d v) (* d v)))

(tojava move-forward-10cm "move-forward-10cm")



;;; Exe 2

(define rotate-counter-clockwise-90deg
  '((setpower a 7)
    (setpower c 7)
    (forward a)
    (forward c)
    (sleep ___)
    (stop a)
    (stop c)
    ))

;; find speed of rotating with 1 motor

;; find distance it is suppose to turn -> quarter of circle with radius from centre of robot to tip
(define quarter-circum
  (let ((pi (/ 22 7)))
    (lambda (r) (* pi (square r)))))

(define square (lambda (x) (* x x)))

;; find time taken to turn this distance -> for the sleep command

;;;;; Exe 3

; (a)
(define (move-forward l)
  (let ((t (cal-time l speed)))
    '((setpower a 7)
      (setpower c 7)
      (forward a)
      (forward c)
      (sleep t)
      (stop a)
      (stop c))))

; (b)
(define (move-forward l)
  (let ((t (cal-time l speed)))
    '((setpower a 7)
      (setpower c 7)
      (backward a)
      (backward c)
      (sleep t)
      (stop a)
      (stop c)
      )))

;;;;; Exe 4

;; calculating the angle
(define cal-arc
  (lambda (r a) (* r a)))

(define rotate 
  (lambda (a) 
    (let ((t (cal-time (cal-arc r a) v))) ;; v is the speed of 1 motor
      '((setpower a 7)
        (forward a)
        (sleep t)
        (stop a)
        ))))

;;;;; Exe 5

(define pen-up
  (lambda (time)

;;; the "time" is the time it stays up 
;;; is the time required??
;;; is time as an arg for pen-down required??

