;; The first three lines of this file were inserted by DrScheme. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname ps04) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;
; CS1101S --- Programming Methodology (Scheme)
; 
; PS04: Solutions to Homework Exercises
;
; Note that answers are commented out to allow the Tutors to 
; run your code easily while grading your problem set.

;;;
;;; Exercise 1
;;;

;;; Your solution here


;c2+7i 
(create-complex (create-number 2) (create-number 7))
;{complex . ({number . 2} . {number . 7})}
(create-complex (create-number 3) (create-number 1))
;{complex . ({number . 3} . {number . 1})}

; Sample ASCII box and pointer for your convenience
;            +---+---+    +---+---+            +---+---+
;     csq -> | | | --+--->| | | --+----------->| | | --+--> -12
;            +-|-+---+    +-|-+---+            +-|-+---+
;              |            |                    |
;              v            |                    v
;           complex         v                  number
;                         +---+---+    
;                         | | | --+--> -35
;                         +-|-+---+
;                           |
;                           v
;                         number
;;;
;;; Exercise 3
;;;

;;; Code to test (equ? (sub c-3+10i (mul c1+2i c1+3i)) (add c1+2i c1+3i)) here
(define c-3+10i (create-complex (create-number -3) (create-number 10)))
(define c1+2i (create-complex (create-number 1) (create-number 2)))
(define c1+3i (create-complex (create-number 1) (create-number 3)))

(equ? (sub c-3+10i (mul c1+2i c1+3i)) (add c1+2i c1+3i))
; #t

;;;
;;; Exercises 2, 4, 5, 6a
;;; See below for an example (exercise 2) of clearly marking the answer
;;; to each exercise. Do the same for exercises 4, 5, and 6a
;;;

(define (install-complex-package)
  (define (make-com r i) (cons r i))
  (define (real x) (car x))
  (define (imag x) (cdr x))
  (define (add-com x y)
    (make-com (add (real x) (real y))
              (add (imag x) (imag y))))
  (define (sub-com x y)
    (make-com (sub (real x) (real y))
              (sub (imag x) (imag y))))
  (define (mul-com x y) 
    (make-com (sub (mul (real x) (real y)) 
                   (mul (imag x) (imag y)))
              (add (mul (real x) (imag y))
                   (mul (real y) (imag x)))))
  (define (div-com x y)  
    (let ((com-conj (complex-conjugate y)))
      (let ((x-times-com-conj (mul-com x com-conj))
            (y-times-com-conj (mul-com y com-conj)))
        (make-com (div (real x-times-com-conj) (real y-times-com-conj))
                  (div (imag x-times-com-conj) (real y-times-com-conj))))))
  (define (complex-conjugate x)
    (make-com (real x) 
	      (negate (imag x))))
  (define (tag x) (attach-tag 'complex x))
  (define (make-complex r i) (tag (make-com r i)))
  (define (add-complex x y) (tag (add-com x y)))
  (define (sub-complex x y) (tag (sub-com x y)))
  (define (mul-complex x y) (tag (mul-com x y)))
  (define (div-complex x y) (tag (div-com x y)))
  
;;; Exercise 2
  
  (define (negate-complex x) (make-com (negate (real x)) (negate (imag x))))
  ; Type signature: RepCom -> RepCom
  
  (define (=zero-complex? x) (=zero? (and (real x) (imag x))))
  ; Type signature: RepCom -> Sch-Bool
  
  (define (=complex? x y) (=zero-complex? (sub-com x y)))
  ; Type signature: (RepCom, RepCom) -> Sch-Bool
  
;;; End of exercise 2
  
; procedures to coerce RepNums to RepComs
  
;;; Exercise 4
  (define (repnum->repcom n)           
    (make-com (create-number n) (create-number 0)))
;;; End of exercise 4
  
  (define (CCmethod->NCmethod method)
    (lambda (num com)
      (method
       (repnum->repcom num) 
       com)))
  
;;; Exercise 5
  (define (CCmethod->CNmethod method)   
    (lambda (com num)
      (method
       com
       (repnum->repcom num))))
;;; End of exercise 5
  
;<------------------------ Place PART 1 (exe 7) here
;<------------------------ Place PART 2 (exe 7) here  
;<------------------------ Place PART 3 (exe 7) here
  
  
  (put 'make 'complex make-complex)
  (put 'negate '(complex) negate-complex)      ; exe 2
  (put '=zero? '(complex) =zero-complex?)      ; exe 2
  (put 'equ? '(complex complex) =complex?)     ; exe 2
  (put 'add '(complex complex) add-complex)
  (put 'sub '(complex complex) sub-complex)
  (put 'mul '(complex complex) mul-complex)  
  (put 'div '(complex complex) div-complex)
  

;;; Exercise 6a
  (put 'add '(number complex) (CCmethod->NCmethod add-complex))
  (put 'add '(complex number) (CCmethod->CNmethod add-complex))
  (put 'sub '(number complex) (CCmethod->NCmethod sub-complex))
  (put 'sub '(complex number) (CCmethod->CNmethod sub-complex))
  (put 'mul '(number complex) (CCmethod->NCmethod mul-complex))
  (put 'mul '(complex number) (CCmethod->CNmethod mul-complex))
  (put 'div '(number complex) (CCmethod->NCmethod div-complex))
  (put 'div '(complex number) (CCmethod->CNmethod div-complex))
  (put 'equ? '(number complex) (CCmethod->NCmethod =complex?))
  (put 'equ? '(complex number) (CCmethod->CNmethod =complex?))
;;;End of exercise 6a  
  
;<------------------------ Place PART 4 (exe 7) here
  'done)

;;;
;;; Exercise 6b
;;;

; Installation
(install-complex-package)

; Testing
(define n3 (create-number 3))
(define c3+0i (create-complex (create-number 3) (create-number 0)))
(define c2+7i (create-complex (create-number 2) (create-number 7)))
(equ? n3 c3+0i)
; #t
(equ? (sub (add n3 c2+7i) c2+7i) n3)
; #t

;;;
;;; Exercise 7
;;;
;;; Add these into complex package
;;; see above complex package for the location of the parts of this exercise

(define (reprat->repcom n)                                 ;part 1 (exe7)
    (make-com (attach-tag 'rational n)(create-number 0)))
  (define (CCmethod->RCmethod method)                      ;part 2 (exe7)
    (lambda (rat com)
      (method
       (reprat->repcom rat) 
       com)))
  (define (CCmethod->CRmethod method)                      ;part 3 (exe7)
    (lambda (com rat)
      (method
       com
       (reprat->repcom rat))))
(put 'add '(rational complex) (CCmethod->RCmethod add-complex))   ; part 4 (exe7)
(put 'add '(complex rational) (CCmethod->CRmethod add-complex))
(put 'sub '(rational complex) (CCmethod->RCmethod sub-complex))
(put 'sub '(complex rational) (CCmethod->CRmethod sub-complex))
(put 'mul '(rational complex) (CCmethod->RCmethod mul-complex))
(put 'mul '(complex rational) (CCmethod->CRmethod mul-complex))
(put 'div '(rational complex) (CCmethod->RCmethod div-complex))
(put 'div '(complex rational) (CCmethod->CRmethod div-complex))
(put 'equ? '(rational complex) (CCmethod->RCmethod =complex?))
(put 'equ? '(complex rational) (CCmethod->CRmethod =complex?))

;;Testing
(define r7/1 (create-rational (create-number 7) (create-number 1)))
(define c7/1+i0/1 (create-complex (create-rational (create-number 7) (create-number 1)) (create-rational (create-number 0) (create-number 1))))
(equ? c7/1+i0/1 r7/1)
; #t

;;;
;;; Exercise 8
;;;

(define (make-com r i)    ;replace make-com in complex package with this
  (cond ((and (test-complex? r) (test-complex? i)) (contents (add r (mul i c0+i1))))
          ((test-complex? r) (contents (add r (mul i c0+i1))))
          ((test-complex? i) (contents (add r (mul i c0+i1))))
          (else (cons r i))))
(define (test-complex? x) (equal? 'complex (type-tag x)))  ;add this in

(define c0+i1 (create-complex (create-number 0) (create-number 1)))

;Testing
(create-complex c1+i2 c3+i4)
; {complex . ({number . -3} . {number . 5})}
(create-complex (create-rational (create-number 2) (create-number 4)) c3+i4)
; {complex . ({rational . ({number . -14} . {number . 4})} . {number . 3})}

;;; 'grats! You are done!