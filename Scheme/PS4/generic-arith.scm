;; The first three lines of this file were inserted by DrScheme. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname generic-arith) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;  This is the file generic-arith.scm
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define nil '())
(require scheme/mpair)

;;; The bottom level typing system

(define attach-tag mcons)

(define (type-tag datum)
  (if (mpair? datum)
      (mcar datum)
      (error "Bad typed datum -- TYPE-TAG" datum)))

(define (contents datum)
  (if (mpair? datum)
      (mcdr datum)
      (error "Bad typed datum -- CONTENTS" datum)))

;;; The apply-generic mechanism.  
;;;  Note that we don't deal with coercion here.

(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
          (apply proc (map contents args))
          (error "No method for the given types -- APPLY-GENERIC"
		 (mlist op type-tags))))))

;;; Code for creating the table, you don't need to worry about this.
(define (make-table)
  (let ((local-table (mlist '*table*)))
    
    (define (lookup key-1 key-2)
      (let ((subtable (massoc key-1 (mcdr local-table))))
        (if subtable
            (let ((record (massoc key-2 (mcdr subtable))))
              (if record
                  (mcdr record)
                  false))
            false)))
    
    (define (insert! key-1 key-2 value)
      (let ((subtable (massoc key-1 (mcdr local-table))))
        (if subtable
            (let ((record (massoc key-2 (mcdr subtable))))
              (if record
                  (set-mcdr! record value)
                      (set-mcdr! subtable
                                 (mcons (mcons key-2 value)
                                        (mcdr subtable)))))
            (set-mcdr! local-table
                       (mcons (mlist key-1
                                     (mcons key-2 value))
                              (mcdr local-table)))))
      'ok)
    
    (define (dispatch m)
      (cond ((eq? m 'lookup-proc) lookup)
            ((eq? m 'insert-proc!) insert!)
            (else (error "Unknown operation -- TABLE" m))))
    
    dispatch))

(define operation-table (make-table))
(define get (operation-table 'lookup-proc))
(define put (operation-table 'insert-proc!))

;;; GENERIC ARITHMETIC OPERATIONS

;;; Generic-Num = ({number} X RepNum) U ({rational} X RepRat) U 
;;;               ({complex} X RepCom)

; add,sub,mul,div: (Generic-Num, Generic-Num) -> Generic-Num
(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))

; negate: Generic-Num -> Generic-Num
(define (negate x) (apply-generic 'negate x))

; =zero?: Generic-Num -> Sch-Bool
(define (=zero? x) (apply-generic '=zero? x))

; equ?: (Generic-Num, Generic-Num) -> Sch-Bool
(define (equ? x y) (apply-generic 'equ? x y))

;;; a sample compound generic operation
(define (square x) (mul x x))


;;; generic ordinary number package
(define (install-number-package)
  (define (tag x)
    (attach-tag 'number x))
  (define (make-number x) (tag x))
  (define (negate x) (tag (- x)))
  (define (zero? x) (= x 0))
  (define (add x y) (tag (+ x y)))
  (define (sub x y) (tag (- x y)))
  (define (mul x y) (tag (* x y)))
  (define (div x y) (tag (/ x y)))
  (put 'make 'number make-number)
  (put 'negate '(number) negate)
  (put '=zero? '(number) zero?)
  (put 'add '(number number) add)
  (put 'sub '(number number) sub)
  (put 'mul '(number number) mul)
  (put 'div '(number number) div)
  'done)

;;; Generic Ordinary Number Package User Interface

;;; A convenient external procedure for building a generic
;;; ordinary number from Scheme numbers.

;;; Sch-Num --> ({number} X RepNum)
(define (create-number x) 
  ((get 'make 'number) x))


;;; generic rational number package
(define (install-rational-package)
  (define (make-rat n d) (cons n d))
  (define (numer x) (car x))
  (define (denom x) (cdr x))
  (define (add-rat x y)
    (make-rat (add (mul (numer x) (denom y))
                   (mul (denom x) (numer y)))
              (mul (denom x) (denom y))))
  (define (sub-rat x y)
    (make-rat (sub (mul (numer x) (denom y))
                   (mul (denom x) (numer y)))
              (mul (denom x) (denom y))))
  (define (mul-rat x y)
    (make-rat (mul (numer x) (numer y))
              (mul (denom x) (denom y))))
  (define (div-rat x y)
    (make-rat (mul (numer x) (denom y))
              (mul (denom x) (numer y))))
  (define (tag x) (attach-tag 'rational x))
  (define (make-rational n d) (tag (make-rat n d)))
  (define (add-rational x y) (tag (add-rat x y)))
  (define (sub-rational x y) (tag (sub-rat x y)))
  (define (mul-rational x y) (tag (mul-rat x y)))
  (define (div-rational x y) (tag (div-rat x y)))
  (define (negate-rational x) (tag (negate-rat x)))
  (define (=zero-rational? x) (=zero? (numer x)))
  (define (=rational? x y) (=zero-rational? (sub-rat x y)))
  ; procedures to do coercion
  (define (repnum->reprat n)
    (make-rat (create-number n) (create-number 1)))
  (define (RRmethod->NRmethod method)
    (lambda (num rat)
      (method
       (repnum->reprat num)
       rat)))
  (define (RRmethod->RNmethod method)
    (lambda (rat num)
      (method
       rat
       (repnum->reprat num))))
  (put 'make 'rational make-rational)
  (put 'add '(rational rational) add-rational)
  (put 'sub '(rational rational) sub-rational)
  (put 'mul '(rational rational) mul-rational)  
  (put 'div '(rational rational) div-rational)
  (put 'negate '(rational) negate-rational)
  (put '=zero? '(rational) =zero-rational?)
  (put 'equ? '(rational rational) =rational?)
  (put 'add '(number rational) (RRmethod->NRmethod add-rational))
  (put 'add '(rational number) (RRmethod->RNmethod add-rational))
  (put 'sub '(number rational) (RRmethod->NRmethod sub-rational))
  (put 'sub '(rational number) (RRmethod->RNmethod sub-rational))
  (put 'mul '(number rational) (RRmethod->NRmethod mul-rational))
  (put 'mul '(rational number) (RRmethod->RNmethod mul-rational))
  (put 'div '(number rational) (RRmethod->NRmethod div-rational))
  (put 'div '(rational number) (RRmethod->RNmethod div-rational))
  (put 'equ? '(number rational) (RRmethod->NRmethod =rational?))
  (put 'equ? '(rational number) (RRmethod->RNmethod =rational?))
  'done)

;;; Generic Rational Number Package User Interface

;;; A convenient procedure for building a generic rational
;;; number from generic numbers.

;;;    (GN, GN) --> ({rational} X RepRat)
(define  (create-rational n d)
  ((get 'make 'rational) n d))


;;; generic complex number package in rectangular form (a+bi)
(define (install-complex-package)
  (define (make-com r i) 
   (cond ((and (test-complex? r) (test-complex? i)) (contents (add r (mul i c0+i1))))
          ((test-complex? r) (contents (add r (mul i c0+i1))))
          ((test-complex? i) (contents (add r (mul i c0+i1))))
          (else (cons r i))))
  (define (test-complex? x) (equal? 'complex (type-tag x)))
  (define (real x) (car x))
  (define (imag x) (cdr x))
  (define (add-com x y)
    (make-com (add (real x) (real y))
              (add (imag x) (imag y))))
  (define (sub-com x y)
    (make-com (sub (real x) (real y))
              (sub (imag x) (imag y))))
  (define (mul-com x y) 
    (make-com (sub (mul (real x) (real y)) 
                   (mul (imag x) (imag y)))
              (add (mul (real x) (imag y))
                   (mul (real y) (imag x)))))
  (define (div-com x y)  
    (let ((com-conj (complex-conjugate y)))
      (let ((x-times-com-conj (mul-com x com-conj))
            (y-times-com-conj (mul-com y com-conj)))
        (make-com (div (real x-times-com-conj) (real y-times-com-conj))
                  (div (imag x-times-com-conj) (real y-times-com-conj))))))
  (define (complex-conjugate x)
    (make-com (real x) 
	      (negate (imag x))))
  (define (tag x) (attach-tag 'complex x))
  (define (make-complex r i) (tag (make-com r i)))
  (define (add-complex x y) (tag (add-com x y)))
  (define (sub-complex x y) (tag (sub-com x y)))
  (define (mul-complex x y) (tag (mul-com x y)))
  (define (div-complex x y) (tag (div-com x y)))
  (define (negate-complex x) (make-com (negate (real x)) (negate (imag x))))
  (define (=zero-complex? x) (=zero? (and (real x) (imag x))))
  (define (=complex? x y) (=zero-complex? (sub-com x y)))
  ; procedures to coerce RepNums to RepComs
  (define (repnum->repcom n)           
    (make-com (create-number n) (create-number 0)))
  (define (CCmethod->NCmethod method)
    (lambda (num com)
      (method
       (repnum->repcom num) 
       com)))
  (define (CCmethod->CNmethod method)   
    (lambda (com num)
      (method
       com
       (repnum->repcom num))))
  ;;reprat->repcom
  (define (reprat->repcom n)
    (make-com (attach-tag 'rational n)(create-number 0)))
  (define (CCmethod->RCmethod method)
    (lambda (rat com)
      (method
       (reprat->repcom rat) 
       com)))
  (define (CCmethod->CRmethod method)   
    (lambda (com rat)
      (method
       com
       (reprat->repcom rat))))
  (put 'make 'complex make-complex)
  (put 'add '(complex complex) add-complex)
  (put 'sub '(complex complex) sub-complex)
  (put 'mul '(complex complex) mul-complex)  
  (put 'div '(complex complex) div-complex)
  (put 'negate '(complex) negate-complex)      
  (put '=zero? '(complex) =zero-complex?)     
  (put 'equ? '(complex complex) =complex?)  
  (put 'add '(number complex) (CCmethod->NCmethod add-complex))
  (put 'add '(complex number) (CCmethod->CNmethod add-complex))
  (put 'sub '(number complex) (CCmethod->NCmethod sub-complex))
  (put 'sub '(complex number) (CCmethod->CNmethod sub-complex))
  (put 'mul '(number complex) (CCmethod->NCmethod mul-complex))
  (put 'mul '(complex number) (CCmethod->CNmethod mul-complex))
  (put 'div '(number complex) (CCmethod->NCmethod div-complex))
  (put 'div '(complex number) (CCmethod->CNmethod div-complex))
  (put 'equ? '(number complex) (CCmethod->NCmethod =complex?))
  (put 'equ? '(complex number) (CCmethod->CNmethod =complex?))
;;JAI'S ADDITIONAL STUFF
  (put 'add '(rational complex) (CCmethod->RCmethod add-complex))
  (put 'add '(complex rational) (CCmethod->CRmethod add-complex))
  (put 'sub '(rational complex) (CCmethod->RCmethod sub-complex))
  (put 'sub '(complex rational) (CCmethod->CRmethod sub-complex))
  (put 'mul '(rational complex) (CCmethod->RCmethod mul-complex))
  (put 'mul '(complex rational) (CCmethod->CRmethod mul-complex))
  (put 'div '(rational complex) (CCmethod->RCmethod div-complex))
  (put 'div '(complex rational) (CCmethod->CRmethod div-complex))
  (put 'equ? '(rational complex) (CCmethod->RCmethod =complex?))
  (put 'equ? '(complex rational) (CCmethod->CRmethod =complex?))
  'done)

;;; Generic Complex Number Package User Interface

;;; A convenient procedure for building a generic complex
;;; number from generic numbers.

;;;    (GN, GN) --> ({complex} X RepRat)
(define (create-complex r i)
  ((get 'make 'complex) r i))



(install-complex-package)
(install-rational-package)
(install-number-package)
(define c1+i2 (create-complex (create-number 1) (create-number 2)))
(define c3+i4 (create-complex (create-number 3) (create-number 4)))
(define c0+i1 (create-complex (create-number 0) (create-number 1)))
