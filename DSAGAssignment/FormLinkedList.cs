using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace DSAGAssignment
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class frmLinkedList : System.Windows.Forms.Form
    {
        private System.Windows.Forms.TreeView tvLinkedList;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        private Label lblLinkedListSize;
        private Label lblLinkedListSizeValue;
        private Label lblName;
        private TextBox tbxName;
        private TextBox tbxInsertPosition;

        private Button btnAddRear;
        private Button btnAddFront;
        private Button btnInsertAtPosition;

        private Button btnDeleteFront;
        private Button btnDeleteRear;

        private Node head;
        private Button btnDeleteAtPosition;
        private TextBox tbxDeletePosition;
        private TextBox tbxType;
        private Label lblType;
        private int linkedListSize;
        private Label lblLevel;
        private TextBox tbxLevel;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private Button btnAddUnique;
        private Button btnDisplayByType;
        private Button btnDeleteByType;

        //Keep a copy of linked list object
        private LinkedList list;

        public frmLinkedList()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            head = null;
            linkedListSize = 0;
            lblLinkedListSizeValue.Text = linkedListSize.ToString();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvLinkedList = new System.Windows.Forms.TreeView();
            this.lblLinkedListSize = new System.Windows.Forms.Label();
            this.lblLinkedListSizeValue = new System.Windows.Forms.Label();
            this.tbxInsertPosition = new System.Windows.Forms.TextBox();
            this.btnAddRear = new System.Windows.Forms.Button();
            this.btnAddFront = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.tbxName = new System.Windows.Forms.TextBox();
            this.btnInsertAtPosition = new System.Windows.Forms.Button();
            this.btnDeleteFront = new System.Windows.Forms.Button();
            this.btnDeleteRear = new System.Windows.Forms.Button();
            this.btnDeleteAtPosition = new System.Windows.Forms.Button();
            this.tbxDeletePosition = new System.Windows.Forms.TextBox();
            this.tbxType = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.lblLevel = new System.Windows.Forms.Label();
            this.tbxLevel = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnAddUnique = new System.Windows.Forms.Button();
            this.btnDisplayByType = new System.Windows.Forms.Button();
            this.btnDeleteByType = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvLinkedList
            // 
            this.tvLinkedList.Location = new System.Drawing.Point(6, 21);
            this.tvLinkedList.Name = "tvLinkedList";
            this.tvLinkedList.Size = new System.Drawing.Size(526, 187);
            this.tvLinkedList.TabIndex = 0;
            // 
            // lblLinkedListSize
            // 
            this.lblLinkedListSize.AutoSize = true;
            this.lblLinkedListSize.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkedListSize.Location = new System.Drawing.Point(182, 217);
            this.lblLinkedListSize.Name = "lblLinkedListSize";
            this.lblLinkedListSize.Size = new System.Drawing.Size(127, 16);
            this.lblLinkedListSize.TabIndex = 4;
            this.lblLinkedListSize.Text = "Size of Linked List : ";
            this.lblLinkedListSize.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLinkedListSizeValue
            // 
            this.lblLinkedListSizeValue.AutoSize = true;
            this.lblLinkedListSizeValue.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkedListSizeValue.Location = new System.Drawing.Point(314, 219);
            this.lblLinkedListSizeValue.Name = "lblLinkedListSizeValue";
            this.lblLinkedListSizeValue.Size = new System.Drawing.Size(12, 16);
            this.lblLinkedListSizeValue.TabIndex = 5;
            this.lblLinkedListSizeValue.Text = " ";
            // 
            // tbxInsertPosition
            // 
            this.tbxInsertPosition.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxInsertPosition.Location = new System.Drawing.Point(438, 77);
            this.tbxInsertPosition.Name = "tbxInsertPosition";
            this.tbxInsertPosition.Size = new System.Drawing.Size(30, 22);
            this.tbxInsertPosition.TabIndex = 10;
            // 
            // btnAddRear
            // 
            this.btnAddRear.Location = new System.Drawing.Point(185, 76);
            this.btnAddRear.Name = "btnAddRear";
            this.btnAddRear.Size = new System.Drawing.Size(100, 25);
            this.btnAddRear.TabIndex = 13;
            this.btnAddRear.Text = "Insert (rear)";
            this.btnAddRear.UseVisualStyleBackColor = true;
            this.btnAddRear.Click += new System.EventHandler(this.btnAddRear_Click);
            // 
            // btnAddFront
            // 
            this.btnAddFront.Location = new System.Drawing.Point(79, 76);
            this.btnAddFront.Name = "btnAddFront";
            this.btnAddFront.Size = new System.Drawing.Size(100, 25);
            this.btnAddFront.TabIndex = 12;
            this.btnAddFront.Text = "Insert (front)";
            this.btnAddFront.UseVisualStyleBackColor = true;
            this.btnAddFront.Click += new System.EventHandler(this.btnAddFront_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(261, 24);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(50, 16);
            this.lblName.TabIndex = 19;
            this.lblName.Text = "Name :";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbxName
            // 
            this.tbxName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxName.Location = new System.Drawing.Point(317, 21);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(193, 22);
            this.tbxName.TabIndex = 2;
            // 
            // btnInsertAtPosition
            // 
            this.btnInsertAtPosition.Location = new System.Drawing.Point(302, 76);
            this.btnInsertAtPosition.Name = "btnInsertAtPosition";
            this.btnInsertAtPosition.Size = new System.Drawing.Size(130, 25);
            this.btnInsertAtPosition.TabIndex = 20;
            this.btnInsertAtPosition.Text = "Insert at position :";
            this.btnInsertAtPosition.UseVisualStyleBackColor = true;
            this.btnInsertAtPosition.Click += new System.EventHandler(this.btnInsertAtPosition_Click);
            // 
            // btnDeleteFront
            // 
            this.btnDeleteFront.Location = new System.Drawing.Point(79, 19);
            this.btnDeleteFront.Name = "btnDeleteFront";
            this.btnDeleteFront.Size = new System.Drawing.Size(100, 25);
            this.btnDeleteFront.TabIndex = 21;
            this.btnDeleteFront.Text = "Delete (front)";
            this.btnDeleteFront.UseVisualStyleBackColor = true;
            this.btnDeleteFront.Click += new System.EventHandler(this.btnDeleteFront_Click);
            // 
            // btnDeleteRear
            // 
            this.btnDeleteRear.Location = new System.Drawing.Point(185, 19);
            this.btnDeleteRear.Name = "btnDeleteRear";
            this.btnDeleteRear.Size = new System.Drawing.Size(100, 25);
            this.btnDeleteRear.TabIndex = 22;
            this.btnDeleteRear.Text = "Delete (rear)";
            this.btnDeleteRear.UseVisualStyleBackColor = true;
            this.btnDeleteRear.Click += new System.EventHandler(this.btnDeleteRear_Click);
            // 
            // btnDeleteAtPosition
            // 
            this.btnDeleteAtPosition.Location = new System.Drawing.Point(302, 19);
            this.btnDeleteAtPosition.Name = "btnDeleteAtPosition";
            this.btnDeleteAtPosition.Size = new System.Drawing.Size(130, 25);
            this.btnDeleteAtPosition.TabIndex = 27;
            this.btnDeleteAtPosition.Text = "Delete at position :";
            this.btnDeleteAtPosition.UseVisualStyleBackColor = true;
            this.btnDeleteAtPosition.Click += new System.EventHandler(this.btnDeleteAtPosition_Click);
            // 
            // tbxDeletePosition
            // 
            this.tbxDeletePosition.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxDeletePosition.Location = new System.Drawing.Point(439, 20);
            this.tbxDeletePosition.Name = "tbxDeletePosition";
            this.tbxDeletePosition.Size = new System.Drawing.Size(30, 22);
            this.tbxDeletePosition.TabIndex = 26;
            // 
            // tbxType
            // 
            this.tbxType.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxType.Location = new System.Drawing.Point(79, 21);
            this.tbxType.Name = "tbxType";
            this.tbxType.Size = new System.Drawing.Size(176, 22);
            this.tbxType.TabIndex = 1;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblType.Location = new System.Drawing.Point(19, 24);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(44, 16);
            this.lblType.TabIndex = 2;
            this.lblType.Text = "Type :";
            this.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel.Location = new System.Drawing.Point(19, 51);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(45, 16);
            this.lblLevel.TabIndex = 31;
            this.lblLevel.Text = "Level :";
            this.lblLevel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbxLevel
            // 
            this.tbxLevel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxLevel.Location = new System.Drawing.Point(79, 48);
            this.tbxLevel.Name = "tbxLevel";
            this.tbxLevel.Size = new System.Drawing.Size(66, 22);
            this.tbxLevel.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblLinkedListSizeValue);
            this.groupBox1.Controls.Add(this.tvLinkedList);
            this.groupBox1.Controls.Add(this.lblLinkedListSize);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(538, 244);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "View of Game Character Entities";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbxName);
            this.groupBox2.Controls.Add(this.lblName);
            this.groupBox2.Controls.Add(this.lblLevel);
            this.groupBox2.Controls.Add(this.btnAddRear);
            this.groupBox2.Controls.Add(this.tbxLevel);
            this.groupBox2.Controls.Add(this.tbxType);
            this.groupBox2.Controls.Add(this.lblType);
            this.groupBox2.Controls.Add(this.btnAddFront);
            this.groupBox2.Controls.Add(this.btnInsertAtPosition);
            this.groupBox2.Controls.Add(this.tbxInsertPosition);
            this.groupBox2.Location = new System.Drawing.Point(12, 262);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(538, 110);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Insert Entity";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnDeleteFront);
            this.groupBox3.Controls.Add(this.btnDeleteRear);
            this.groupBox3.Controls.Add(this.btnDeleteAtPosition);
            this.groupBox3.Controls.Add(this.tbxDeletePosition);
            this.groupBox3.Location = new System.Drawing.Point(12, 378);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(538, 54);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Delete Entity";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnDeleteByType);
            this.groupBox4.Controls.Add(this.btnDisplayByType);
            this.groupBox4.Controls.Add(this.btnAddUnique);
            this.groupBox4.Location = new System.Drawing.Point(12, 438);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(538, 86);
            this.groupBox4.TabIndex = 33;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Additional Functions";
            // 
            // btnAddUnique
            // 
            this.btnAddUnique.Font = new System.Drawing.Font("Arial", 9.75F);
            this.btnAddUnique.Location = new System.Drawing.Point(32, 36);
            this.btnAddUnique.Name = "btnAddUnique";
            this.btnAddUnique.Size = new System.Drawing.Size(147, 25);
            this.btnAddUnique.TabIndex = 24;
            this.btnAddUnique.Text = "Add Unique Entity";
            this.btnAddUnique.UseVisualStyleBackColor = true;
            this.btnAddUnique.Click += new System.EventHandler(this.btnAddUnique_Click);
            // 
            // btnDisplayByType
            // 
            this.btnDisplayByType.Location = new System.Drawing.Point(194, 36);
            this.btnDisplayByType.Name = "btnDisplayByType";
            this.btnDisplayByType.Size = new System.Drawing.Size(147, 25);
            this.btnDisplayByType.TabIndex = 25;
            this.btnDisplayByType.Text = "Display By Type";
            this.btnDisplayByType.UseVisualStyleBackColor = true;
            this.btnDisplayByType.Click += new System.EventHandler(this.btnDisplayByType_Click);
            // 
            // btnDeleteByType
            // 
            this.btnDeleteByType.Location = new System.Drawing.Point(367, 36);
            this.btnDeleteByType.Name = "btnDeleteByType";
            this.btnDeleteByType.Size = new System.Drawing.Size(143, 25);
            this.btnDeleteByType.TabIndex = 26;
            this.btnDeleteByType.Text = "Delete By Type";
            this.btnDeleteByType.UseVisualStyleBackColor = true;
            this.btnDeleteByType.Click += new System.EventHandler(this.btnDeleteByType_Click);
            // 
            // frmLinkedList
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.ClientSize = new System.Drawing.Size(562, 533);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmLinkedList";
            this.Text = "Game Character Display List";
            this.Load += new System.EventHandler(this.frmLinkedList_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new frmLinkedList());
        }

        // ################################################################################

        private void Visualize(Node h)
        {
            this.tvLinkedList.Nodes.Clear();
            if (h == null) return;
            Node n = h;
            TreeNode firstNode = null, tn = null;
            while (n != null)
            {
                if (firstNode == null)
                {
                    tn = new TreeNode(n.Data.ToString());
                    firstNode = tn;
                }
                else
                {
                    tn.Nodes.Add(new TreeNode(n.Data.ToString()));
                    tn = tn.Nodes[0];
                }
                n = n.Link;
            }
            firstNode.ExpandAll();
            this.tvLinkedList.Nodes.Add(firstNode);

            clearAllFields();
        }

        // ################################################################################

        private void frmLinkedList_Load(object sender, EventArgs e)
        {
            list = new LinkedList();
            lblLinkedListSizeValue.Text = list.Size.ToString();
        }


        // ################################################################################


        private Entity constructEntityFromUI()
        {
            Entity n = new Entity(this.tbxType.Text, this.tbxName.Text, Convert.ToInt32(tbxLevel.Text));
            
            return n;
        }

        private bool isFirstNode(Node node)
        { return (node == head); }

        private bool isLastNode(Node node)
        { return (node.Link == null); }

        private void clearAllFields()
        {
            tbxType.Clear();
            tbxName.Clear();
            tbxLevel.Clear();
            tbxInsertPosition.Clear();
            tbxDeletePosition.Clear();
        }   // end clearAllFields

        // ################################################################################

        private void btnAddFront_Click(object sender, EventArgs e)
        {
            Entity n = new Entity(this.tbxType.Text, this.tbxName.Text, Convert.ToInt32(this.tbxLevel.Text));// convert so int cause from textbox its in string type
            list.AddHead(n);//Add from the value of textbox .... all the objects
            this.Visualize(list.Head); // Visualize display linked list in big big box
            lblLinkedListSizeValue.Text = list.Size.ToString();
        }   // end btnAddFront_Click () ...

        // ################################################################################

        private void btnAddRear_Click(object sender, EventArgs e)
        {
            Entity n = new Entity(this.tbxType.Text, this.tbxName.Text, Convert.ToInt32(this.tbxLevel.Text));// convert so int cause from textbox its in string type
            list.AddTail(n);//Add from the value of textbox .... all the objects
            this.Visualize(list.Head); // Visualize display linked list in big big box
            lblLinkedListSizeValue.Text = list.Size.ToString();
        }   // end btnAddRear_Click () ...

        // ################################################################################

        private void btnInsertAtPosition_Click(object sender, EventArgs e)
        {
            Entity n = new Entity(this.tbxType.Text, this.tbxName.Text, Convert.ToInt32(this.tbxLevel.Text));
            list.AddAt(n, Convert.ToInt32(this.tbxInsertPosition.Text));
            this.Visualize(list.Head);
            lblLinkedListSizeValue.Text = list.Size.ToString();
        }   // end btnInsertAtPosition_Click () ...

        // ################################################################################

        private void btnDeleteFront_Click(object sender, EventArgs e)
        {
            list.DeleteHead();
            this.Visualize(list.Head);
            lblLinkedListSizeValue.Text = list.Size.ToString();
    
        }   // end btnDeleteFront_Click () ...

        // ################################################################################

        private void btnDeleteRear_Click(object sender, EventArgs e)
        {
            list.DeleteTail();
            this.Visualize(list.Head);
            lblLinkedListSizeValue.Text = list.Size.ToString();

        }   // end btnDeleteRear_Click () ...

        // ################################################################################

        private void btnDeleteAtPosition_Click(object sender, EventArgs e)
        {
            list.RemoveAt(Convert.ToInt32(tbxDeletePosition.Text));
            this.Visualize(this.list.Head);
            this.lblLinkedListSizeValue.Text = this.list.Size.ToString();
        }   // end btnDeleteAtPosition_Click () ...

        // ################################################################################

        private void btnAddUnique_Click(object sender, EventArgs e)
        {
            bool check = list.AddUnique(this.tbxName.Text, this.tbxType.Text);
            if(check == true)
            {
                MessageBox.Show("Character already exist!");
            }
            else
            {
            Entity n = new Entity(this.tbxType.Text, this.tbxName.Text, Convert.ToInt32(this.tbxLevel.Text));// convert so int cause from textbox its in string type
            list.AddTail(n);//Add from the value of textbox .... all the objects
            this.Visualize(list.Head); // Visualize display linked list in big big box
            lblLinkedListSizeValue.Text = list.Size.ToString();
            
            }

            this.clearAllFields();

        }

        private void btnDisplayByType_Click(object sender, EventArgs e)
        {
            MessageBox.Show(list.Display(this.tbxType.Text));
        }

        private void btnDeleteByType_Click(object sender, EventArgs e)
        {
            list.DeleteByType(this.tbxType.Text);
            this.Visualize(list.Head);
            lblLinkedListSizeValue.Text = list.Size.ToString();
        }

    }
    
} 

	



 