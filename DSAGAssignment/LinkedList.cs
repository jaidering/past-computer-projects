﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSAGAssignment
{
    class LinkedList
    {
        private Node head;
        private Node tail;
        private int size;
        private Node cursor;
        private Node prev;

        public LinkedList()
        {
            head = null;
            tail = null;
            size = 0;
            prev = null;
        }


        public Node Head
        {
            get
            {
                return head;
            }
        }

        public Node Tail
        {
            get {
                return tail;
            }
        }

        public int Size
        {
            get
            {
                return size;
            }
        }

        public void AddAt(object data, int num)
        {
            Node newNode = new Node(data, null);


            if (head == null)
            {
                head = newNode;
            }
            else
                if (num == 1) 
                {
                    newNode.Link = head;
                    head = newNode;
                }
                else
                {
                    cursor = head;
                    
                    for (int i = 1; i < num - 1; i++)
                    {
                        cursor = cursor.Link;
                    }
                    if (cursor == null)
                        throw new ArgumentOutOfRangeException();

                    newNode.Link = cursor.Link;
                    cursor.Link = newNode;
                    cursor = tail;
                }
            size++;
        }


        public void AddHead(object obj)
        {
            Node n = new Node(obj, head); 
            if (head == null)
            {
                tail = n;
            }
            head = n;
            size++;

        }

        public void DeleteHead() 
        {
            if (head != null)
            {
                head = head.Link;
                size --;
            }

            if(head == null) 
            {
                tail = null;
            }
        }

        public void AddTail(object obj) 
        {
            if(tail!= null)
            {
                Node n = new Node(obj, null); 
                tail.Link = n;
                tail = n;
                size++;
            }

        }

        public bool AddUnique(string uniqueType, string uniqueName)
        {
            for (cursor = head; cursor != null; cursor = cursor.Link)
            {
                Entity ent = (Entity)cursor.Data;
                string name = ent.Name;
                string type = ent.Type;
            
                if (type == uniqueType && name == uniqueName)
                {
                    return true;
                    
                }
            }
            return false;
           
        }

        public string Display (string type)
        {
            string s = "";
            for (cursor = head; cursor != null; cursor = cursor.Link)
            {
                Entity ent = (Entity)cursor.Data;

                if (type == ent.Type)
                {
                    s = s + "\n" + ent.ToString();
                }
            }
            return s;
        }

        public void DeleteByType(string dType)
        {
            prev = head;
            for (cursor = head; cursor != null; cursor = cursor.Link)
            {
                Entity ent = (Entity)cursor.Data;
                string type = ent.Type;
                //compare to see which type is to be deleted.
                if (type.ToUpper() == dType.ToUpper())
                {
                    //if first node, call delete head method
                    if (cursor == head)
                    {
                        DeleteHead();
                    }
                    else
                        //if last node, call delete tail method
                        if (cursor == tail)
                        {
                            DeleteTail();
                        }
                        else
                        {
                            //link the node before the to be deleted node to the node after the to be deleted node.
                            prev.Link = cursor.Link;
                            size--;
                        }
                }
            }
        }

        public void RemoveAt(int position)
        {
            if (position == 1)
            {	// removing the first node
                head = head.Link;
            }
            else
            {
                Node cursor = head;
                //move cursor to the position to remove Node
                for (int i = 1; i < position - 1; i++)
                {
                    cursor = cursor.Link;
                }
                if (cursor == null)
                {
                    throw new ArgumentOutOfRangeException();
                }

                Node removedNode = cursor.Link;
                cursor.Link = removedNode.Link;
                removedNode.Link = null;
            }
            size--;

        }


        public void DeleteTail()
        {
            Node cursor = head; //For traversing
            Node previous = null;

            if (head == null)
            {
                return; //return does not mean return null!!!
            }

            while (cursor.Link != null) //curosr.Link refers to the object it is pointin gto
            {
                previous = cursor;//keeping a copy of previous object
                cursor = cursor.Link;//point to next object
            }
            if (previous != null && cursor != head) //only if 1 element left
            {
                previous.Link = null;
                tail = previous;
            }
            else
            {
                head = null;
                tail = null;
                
            }
            
            size--;
        }
    } // class
} // namespace
