﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSAGAssignment
{
    class Entity
    {
        //Attributes
        private string type;
        private string name;
        private int level;

        //Constructor
        public Entity(string type, string name, int level)
        {
            this.type = type;
            this.name = name;
            this.level = level;
        }

        //Get/Set Properties
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Level
        {
            get { return level; }
            set { level = value; }
        }

        //ToString Method
        public override string ToString()
        {
            return "Type: " + type + ",Name: " + name + ",Level: " + level;
        }


    }
}
