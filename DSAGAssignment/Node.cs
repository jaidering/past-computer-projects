using System;

namespace DSAGAssignment
{
    public class Node
    {
        object data;
        Node link;

        public Node(object d, Node l)
        {
            data = d;
            link = l;
        }

        public object Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public Node Link
        {
            get
            {
                return link;
            }
            set
            {
                link = value;
            }
        }
    }
}
