using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Collections;

namespace BusinessLogic_Demo
{
    public partial class FrmDeleteCD : Form
    {
        private AllDataClass data;

// #####################################################################

        public FrmDeleteCD()
        {
            InitializeComponent();
        }

        public FrmDeleteCD(AllDataClass adc)
        {
            InitializeComponent();
            data = adc;
        }

        private void btnSearchCD_Click(object sender, EventArgs e)
        {
            //search CD info
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.data.mainWindowForm.Show();
        }

        private void btnDetail_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.data.formDeleteCDDetail.Show();
        }



    }   // end class FrmDeleteCD ...
}