using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Collections;

namespace BusinessLogic_Demo
{
    public partial class FrmModifyCD : Form
    {
        private AllDataClass data;

// #####################################################################

        public FrmModifyCD()
        {
            InitializeComponent();
        }


        public FrmModifyCD(AllDataClass adc)
        {
            InitializeComponent();
            data = adc;
        }

        private void btnSearchCD_Click(object sender, EventArgs e)
        {
            //search for a CD
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.data.mainWindowForm.Show();
        }

        private void btnDetail_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.data.formModifyCDDetail.Show();
        }



    }   // end class FrmModifyCD ...
}