using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BusinessLogic_Demo
{
    public partial class FrmModifyCDDetail : Form
    {
        private AllDataClass data;

// #####################################################################

        public FrmModifyCDDetail()
        {
            InitializeComponent();
        }


        public FrmModifyCDDetail(AllDataClass adc)
        {
            InitializeComponent();
            data = adc;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.data.formModifyCD.Show();
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            //modify CD info
        }


    
    }   // end class FrmModifyCDDetail ...
}



