using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BusinessLogic_Demo
{
    public partial class FrmSearchCDDetail : Form
    {
        private AllDataClass data;
        private Form prevForm;

        // #####################################################################

        public FrmSearchCDDetail()
        {
            InitializeComponent();
        }


        public FrmSearchCDDetail(AllDataClass adc, Form _prevForm)
        {
            InitializeComponent();
            data = adc;
            prevForm = _prevForm;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            prevForm.Show();
        }

        private void FrmSearchCDDetail_Load(object sender, EventArgs e)
        {
            MusicCD mcd = data.CDDetail;

            tbxTitle.Text = mcd.Title;
            tbxComposer.Text = mcd.Composer;
            tbxEdition.Text = mcd.Edition.ToString();
        }


    }   // end class FrmCDDetail ...
}