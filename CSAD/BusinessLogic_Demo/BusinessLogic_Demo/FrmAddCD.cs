using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Collections;

namespace BusinessLogic_Demo
{
    public partial class FrmAddCD : Form
    {
        private AllDataClass data;

// #####################################################################
        
        public FrmAddCD()
        {
            InitializeComponent();
        }


        public FrmAddCD(AllDataClass adc)
        {
            InitializeComponent();

            data = adc;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.data.mainWindowForm.Show();
        }

        private void btnAddCD_Click(object sender, EventArgs e)
        {
            try
            {
                // retrieve CD info from User Interface ...
                string title = tbxTitle.Text;
                string composer = tbxComposer.Text;
                int edition = Convert.ToInt32(tbxEdition.Text);

                // if no exists CD with same info ...
                if (DoesCDExist(title, composer, edition) == false)
                {
                    // Create a new 'data' class to store the CD info ...
                    MusicCD mcd = new MusicCD(title, composer, edition);

                    // Add the class to the ArrayList ...
                    data.CDList.Add(mcd);

                    MessageBox.Show("CD Info has been added successfully!");
                }
                else
                {
                    MessageBox.Show("CD is not added.\n CD with same info already exists!");

                    // reset the values of the TextBoxes to empty strings ...
                    ResetUIControls();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Add CD Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
            }
        }

        private bool DoesCDExist(string title, string composer, int edition)
        {
            ArrayList cdList = data.CDList;

            foreach (MusicCD mcd in cdList)
            {
                if (mcd.Title.Equals(title) &&
                    mcd.Composer.Equals(composer) &&
                    (mcd.Edition == edition)
                    )
                    return (true);
            }

            return (false);
        }

        public void ResetUIControls()
        {
            // reset the values of the TextBoxes to empty strings ...
            tbxTitle.Text = "";
            tbxComposer.Text = "";
            tbxEdition.Text = "";
        }

    }   // end class FrmAddCD ...
}