namespace BusinessLogic_Demo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.productToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewCDInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyCDInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCDInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchCDInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(292, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // productToolStripMenuItem
            // 
            this.productToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewCDInfoToolStripMenuItem,
            this.modifyCDInfoToolStripMenuItem,
            this.deleteCDInfoToolStripMenuItem,
            this.searchCDInfoToolStripMenuItem});
            this.productToolStripMenuItem.Name = "productToolStripMenuItem";
            this.productToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.productToolStripMenuItem.Text = "Product";
            // 
            // addNewCDInfoToolStripMenuItem
            // 
            this.addNewCDInfoToolStripMenuItem.Name = "addNewCDInfoToolStripMenuItem";
            this.addNewCDInfoToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.addNewCDInfoToolStripMenuItem.Text = "Add New CD Info";
            this.addNewCDInfoToolStripMenuItem.Click += new System.EventHandler(this.addNewCDInfoToolStripMenuItem_Click);
            // 
            // modifyCDInfoToolStripMenuItem
            // 
            this.modifyCDInfoToolStripMenuItem.Name = "modifyCDInfoToolStripMenuItem";
            this.modifyCDInfoToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.modifyCDInfoToolStripMenuItem.Text = "Modify CD Info";
            this.modifyCDInfoToolStripMenuItem.Click += new System.EventHandler(this.modifyCDInfoToolStripMenuItem_Click);
            // 
            // deleteCDInfoToolStripMenuItem
            // 
            this.deleteCDInfoToolStripMenuItem.Name = "deleteCDInfoToolStripMenuItem";
            this.deleteCDInfoToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.deleteCDInfoToolStripMenuItem.Text = "Delete CD Info";
            this.deleteCDInfoToolStripMenuItem.Click += new System.EventHandler(this.deleteCDInfoToolStripMenuItem_Click);
            // 
            // searchCDInfoToolStripMenuItem
            // 
            this.searchCDInfoToolStripMenuItem.Name = "searchCDInfoToolStripMenuItem";
            this.searchCDInfoToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.searchCDInfoToolStripMenuItem.Text = "Search CD Info";
            this.searchCDInfoToolStripMenuItem.Click += new System.EventHandler(this.searchCDInfoToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Music CDs ";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem productToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewCDInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyCDInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteCDInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchCDInfoToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}

