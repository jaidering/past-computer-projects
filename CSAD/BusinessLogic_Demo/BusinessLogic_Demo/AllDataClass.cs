using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
using System.Collections;

namespace BusinessLogic_Demo
{
    public class AllDataClass
    {
        public Form mainWindowForm;

        public FrmAddCD formAddCD;
        public FrmDeleteCD formDeleteCD;
        public FrmDeleteCDDetail formDeleteCDDetail;
        public FrmModifyCD formModifyCD;
        public FrmModifyCDDetail formModifyCDDetail;
        public FrmSearchCD formSearchCD;

        // Detail forms to be instantiated upon click corresponding "Detail" button.
        // Check what is new the Contructor of Detail Form, e.g. FrmSearchCDDetail.cs.
        // public FrmSearchCDDetail formSearchCDDetail;

        // Data structure to hold CD info
        private ArrayList cdList;
        private MusicCD cdDetail;


        //constructor
        public AllDataClass(Form _mainWindowForm)
        {
            mainWindowForm = _mainWindowForm;
            
            formAddCD          = new FrmAddCD(this);

            formDeleteCD       = new FrmDeleteCD(this);
            formDeleteCDDetail = new FrmDeleteCDDetail(this);
            
            formModifyCD       = new FrmModifyCD(this);
            formModifyCDDetail = new FrmModifyCDDetail(this);
            
            formSearchCD       = new FrmSearchCD(this);
            //formSearchCDDetail = new FrmSearchCDDetail(this);


            cdList = new ArrayList();
        }

        // Properties to access the CD list and detail info of a particular CD
        public ArrayList CDList
        {
            get { return cdList; }
            set { cdList = value; }
        }

        public MusicCD CDDetail
        {
            get { return cdDetail; }
            set { cdDetail = value; }
        }

    }   // end MyDataClass ...
}
