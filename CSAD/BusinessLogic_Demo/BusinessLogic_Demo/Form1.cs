using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BusinessLogic_Demo
{
    public partial class Form1 : Form
    {
        private AllDataClass allData;

// #####################################################################

        public Form1()
        {
            InitializeComponent();

            allData = new AllDataClass(this);
        }

        private void addNewCDInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.allData.formAddCD.ResetUIControls();
            this.allData.formAddCD.Show();
        }

        private void modifyCDInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.allData.formModifyCD.Show();
        }

        private void deleteCDInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.allData.formDeleteCD.Show();
        }

        private void searchCDInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.allData.formSearchCD.ResetUIControls();
            this.allData.formSearchCD.Show();
        }




// #####################################################################
            
    }
}