namespace BusinessLogic_Demo
{
    partial class FrmAddCD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblComposer = new System.Windows.Forms.Label();
            this.lblEdition = new System.Windows.Forms.Label();
            this.tbxTitle = new System.Windows.Forms.TextBox();
            this.tbxComposer = new System.Windows.Forms.TextBox();
            this.tbxEdition = new System.Windows.Forms.TextBox();
            this.btnAddCD = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(12, 41);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(40, 19);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Title";
            // 
            // lblComposer
            // 
            this.lblComposer.AutoSize = true;
            this.lblComposer.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComposer.Location = new System.Drawing.Point(12, 81);
            this.lblComposer.Name = "lblComposer";
            this.lblComposer.Size = new System.Drawing.Size(81, 19);
            this.lblComposer.TabIndex = 1;
            this.lblComposer.Text = "Composer";
            // 
            // lblEdition
            // 
            this.lblEdition.AutoSize = true;
            this.lblEdition.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEdition.Location = new System.Drawing.Point(12, 119);
            this.lblEdition.Name = "lblEdition";
            this.lblEdition.Size = new System.Drawing.Size(58, 19);
            this.lblEdition.TabIndex = 2;
            this.lblEdition.Text = "Edition";
            // 
            // tbxTitle
            // 
            this.tbxTitle.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxTitle.Location = new System.Drawing.Point(130, 38);
            this.tbxTitle.Name = "tbxTitle";
            this.tbxTitle.Size = new System.Drawing.Size(150, 27);
            this.tbxTitle.TabIndex = 3;
            // 
            // tbxComposer
            // 
            this.tbxComposer.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxComposer.Location = new System.Drawing.Point(130, 78);
            this.tbxComposer.Name = "tbxComposer";
            this.tbxComposer.Size = new System.Drawing.Size(150, 27);
            this.tbxComposer.TabIndex = 4;
            // 
            // tbxEdition
            // 
            this.tbxEdition.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxEdition.Location = new System.Drawing.Point(130, 116);
            this.tbxEdition.Name = "tbxEdition";
            this.tbxEdition.Size = new System.Drawing.Size(150, 27);
            this.tbxEdition.TabIndex = 5;
            // 
            // btnAddCD
            // 
            this.btnAddCD.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCD.Location = new System.Drawing.Point(16, 180);
            this.btnAddCD.Name = "btnAddCD";
            this.btnAddCD.Size = new System.Drawing.Size(98, 29);
            this.btnAddCD.TabIndex = 6;
            this.btnAddCD.Text = "Add CD Info";
            this.btnAddCD.UseVisualStyleBackColor = true;
            this.btnAddCD.Click += new System.EventHandler(this.btnAddCD_Click);
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(182, 180);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(98, 29);
            this.btnBack.TabIndex = 7;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // FrmAddCD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.ControlBox = false;
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnAddCD);
            this.Controls.Add(this.tbxEdition);
            this.Controls.Add(this.tbxComposer);
            this.Controls.Add(this.tbxTitle);
            this.Controls.Add(this.lblEdition);
            this.Controls.Add(this.lblComposer);
            this.Controls.Add(this.lblTitle);
            this.Name = "FrmAddCD";
            this.Text = "FrmAddCD";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblComposer;
        private System.Windows.Forms.Label lblEdition;
        private System.Windows.Forms.TextBox tbxTitle;
        private System.Windows.Forms.TextBox tbxComposer;
        private System.Windows.Forms.TextBox tbxEdition;
        private System.Windows.Forms.Button btnAddCD;
        private System.Windows.Forms.Button btnBack;
    }
}