using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BusinessLogic_Demo
{
    public partial class FrmDeleteCDDetail : Form
    {
        private AllDataClass data;

// #####################################################################

        public FrmDeleteCDDetail()
        {
            InitializeComponent();
        }

        public FrmDeleteCDDetail(AllDataClass adc)
        {
            InitializeComponent();
            data = adc;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.data.formDeleteCD.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //delete a CD entry
        }


    }   // end class FrmDeleteCDDetail ...
}

