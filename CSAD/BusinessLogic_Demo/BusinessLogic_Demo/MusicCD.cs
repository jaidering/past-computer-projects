using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic_Demo
{
    public class MusicCD
    {
        private string title, composer;
        private int edition;

        public MusicCD(string _title, string _composer, int _edition)
        {
            title = _title;
            composer = _composer;
            edition = _edition;
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string Composer
        {
            get { return composer; }
            set { composer = value; }
        }

        public int Edition
        {
            get { return edition; }
            set { edition = value; }
        }

        public override string ToString()
        {
            string s = "Title : " + title + ", ";
            s += "Composer : " + composer + ", ";
            s += "Edition : " + edition;

            return (s);
        } 

    }
}
