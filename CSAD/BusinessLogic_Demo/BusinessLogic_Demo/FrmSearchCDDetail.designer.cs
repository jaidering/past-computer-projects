namespace BusinessLogic_Demo
{
    partial class FrmSearchCDDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBack = new System.Windows.Forms.Button();
            this.tbxEdition = new System.Windows.Forms.TextBox();
            this.tbxComposer = new System.Windows.Forms.TextBox();
            this.tbxTitle = new System.Windows.Forms.TextBox();
            this.lblEdition = new System.Windows.Forms.Label();
            this.lblComposer = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(91, 190);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(98, 29);
            this.btnBack.TabIndex = 15;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // tbxEdition
            // 
            this.tbxEdition.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxEdition.Location = new System.Drawing.Point(130, 126);
            this.tbxEdition.Name = "tbxEdition";
            this.tbxEdition.Size = new System.Drawing.Size(150, 27);
            this.tbxEdition.TabIndex = 13;
            // 
            // tbxComposer
            // 
            this.tbxComposer.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxComposer.Location = new System.Drawing.Point(130, 88);
            this.tbxComposer.Name = "tbxComposer";
            this.tbxComposer.Size = new System.Drawing.Size(150, 27);
            this.tbxComposer.TabIndex = 12;
            // 
            // tbxTitle
            // 
            this.tbxTitle.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxTitle.Location = new System.Drawing.Point(130, 48);
            this.tbxTitle.Name = "tbxTitle";
            this.tbxTitle.Size = new System.Drawing.Size(150, 27);
            this.tbxTitle.TabIndex = 11;
            // 
            // lblEdition
            // 
            this.lblEdition.AutoSize = true;
            this.lblEdition.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEdition.Location = new System.Drawing.Point(12, 129);
            this.lblEdition.Name = "lblEdition";
            this.lblEdition.Size = new System.Drawing.Size(58, 19);
            this.lblEdition.TabIndex = 10;
            this.lblEdition.Text = "Edition";
            // 
            // lblComposer
            // 
            this.lblComposer.AutoSize = true;
            this.lblComposer.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComposer.Location = new System.Drawing.Point(12, 91);
            this.lblComposer.Name = "lblComposer";
            this.lblComposer.Size = new System.Drawing.Size(81, 19);
            this.lblComposer.TabIndex = 9;
            this.lblComposer.Text = "Composer";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(12, 51);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(40, 19);
            this.lblTitle.TabIndex = 8;
            this.lblTitle.Text = "Title";
            // 
            // FrmSearchCDDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.ControlBox = false;
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.tbxEdition);
            this.Controls.Add(this.tbxComposer);
            this.Controls.Add(this.tbxTitle);
            this.Controls.Add(this.lblEdition);
            this.Controls.Add(this.lblComposer);
            this.Controls.Add(this.lblTitle);
            this.Name = "FrmSearchCDDetail";
            this.Text = "FrmSearchCDDetail";
            this.Load += new System.EventHandler(this.FrmSearchCDDetail_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.TextBox tbxEdition;
        private System.Windows.Forms.TextBox tbxComposer;
        private System.Windows.Forms.TextBox tbxTitle;
        private System.Windows.Forms.Label lblEdition;
        private System.Windows.Forms.Label lblComposer;
        private System.Windows.Forms.Label lblTitle;
    }
}