namespace BusinessLogic_Demo
{
    partial class FrmSearchCD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBack = new System.Windows.Forms.Button();
            this.btnSearchCD = new System.Windows.Forms.Button();
            this.tbxEdition = new System.Windows.Forms.TextBox();
            this.tbxComposer = new System.Windows.Forms.TextBox();
            this.tbxTitle = new System.Windows.Forms.TextBox();
            this.lblEdition = new System.Windows.Forms.Label();
            this.lblComposer = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lstResult = new System.Windows.Forms.ListBox();
            this.btnDetail = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btnBack.Location = new System.Drawing.Point(180, 341);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(98, 29);
            this.btnBack.TabIndex = 15;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnSearchCD
            // 
            this.btnSearchCD.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchCD.Location = new System.Drawing.Point(12, 140);
            this.btnSearchCD.Name = "btnSearchCD";
            this.btnSearchCD.Size = new System.Drawing.Size(140, 29);
            this.btnSearchCD.TabIndex = 14;
            this.btnSearchCD.Text = "Search CD Info";
            this.btnSearchCD.UseVisualStyleBackColor = true;
            this.btnSearchCD.Click += new System.EventHandler(this.btnSearchCD_Click);
            // 
            // tbxEdition
            // 
            this.tbxEdition.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxEdition.Location = new System.Drawing.Point(129, 92);
            this.tbxEdition.Name = "tbxEdition";
            this.tbxEdition.Size = new System.Drawing.Size(150, 27);
            this.tbxEdition.TabIndex = 13;
            // 
            // tbxComposer
            // 
            this.tbxComposer.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxComposer.Location = new System.Drawing.Point(129, 54);
            this.tbxComposer.Name = "tbxComposer";
            this.tbxComposer.Size = new System.Drawing.Size(150, 27);
            this.tbxComposer.TabIndex = 12;
            // 
            // tbxTitle
            // 
            this.tbxTitle.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxTitle.Location = new System.Drawing.Point(129, 14);
            this.tbxTitle.Name = "tbxTitle";
            this.tbxTitle.Size = new System.Drawing.Size(150, 27);
            this.tbxTitle.TabIndex = 11;
            // 
            // lblEdition
            // 
            this.lblEdition.AutoSize = true;
            this.lblEdition.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEdition.Location = new System.Drawing.Point(11, 95);
            this.lblEdition.Name = "lblEdition";
            this.lblEdition.Size = new System.Drawing.Size(58, 19);
            this.lblEdition.TabIndex = 10;
            this.lblEdition.Text = "Edition";
            // 
            // lblComposer
            // 
            this.lblComposer.AutoSize = true;
            this.lblComposer.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComposer.Location = new System.Drawing.Point(11, 57);
            this.lblComposer.Name = "lblComposer";
            this.lblComposer.Size = new System.Drawing.Size(81, 19);
            this.lblComposer.TabIndex = 9;
            this.lblComposer.Text = "Composer";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(11, 17);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(40, 19);
            this.lblTitle.TabIndex = 8;
            this.lblTitle.Text = "Title";
            // 
            // lstResult
            // 
            this.lstResult.FormattingEnabled = true;
            this.lstResult.Location = new System.Drawing.Point(15, 189);
            this.lstResult.Name = "lstResult";
            this.lstResult.Size = new System.Drawing.Size(263, 134);
            this.lstResult.TabIndex = 16;
            // 
            // btnDetail
            // 
            this.btnDetail.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDetail.Location = new System.Drawing.Point(15, 341);
            this.btnDetail.Name = "btnDetail";
            this.btnDetail.Size = new System.Drawing.Size(107, 29);
            this.btnDetail.TabIndex = 17;
            this.btnDetail.Text = "View Detail";
            this.btnDetail.UseVisualStyleBackColor = true;
            this.btnDetail.Click += new System.EventHandler(this.btnDetail_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(180, 140);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(100, 29);
            this.btnReset.TabIndex = 18;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // FrmSearchCD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 386);
            this.ControlBox = false;
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnDetail);
            this.Controls.Add(this.lstResult);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnSearchCD);
            this.Controls.Add(this.tbxEdition);
            this.Controls.Add(this.tbxComposer);
            this.Controls.Add(this.tbxTitle);
            this.Controls.Add(this.lblEdition);
            this.Controls.Add(this.lblComposer);
            this.Controls.Add(this.lblTitle);
            this.Name = "FrmSearchCD";
            this.Text = "FrmSearchCD";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnSearchCD;
        private System.Windows.Forms.TextBox tbxEdition;
        private System.Windows.Forms.TextBox tbxComposer;
        private System.Windows.Forms.TextBox tbxTitle;
        private System.Windows.Forms.Label lblEdition;
        private System.Windows.Forms.Label lblComposer;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ListBox lstResult;
        private System.Windows.Forms.Button btnDetail;
        private System.Windows.Forms.Button btnReset;
    }
}