using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace BusinessLogic_Demo
{
    public partial class FrmSearchCD : Form
    {
        private AllDataClass data;

        public FrmSearchCD()
        {
            InitializeComponent();
        }


        public FrmSearchCD(AllDataClass adc)
        {
            InitializeComponent();
            data = adc;
        }

        private void btnSearchCD_Click(object sender, EventArgs e)
        {
            try
            {
                // retrieve CD info from User Interface ...
                string title = tbxTitle.Text;
                string composer = tbxComposer.Text;
                int edition = Convert.ToInt32(tbxEdition.Text);

                // get the list of CD info which matches search criteria ...
                ArrayList matchingCDList = RetrieveMatchingCDInfo(title, composer, edition);

                // if no CD Info matches the criteria, inform the user!
                if (matchingCDList.Count <= 0)
                {
                    MessageBox.Show("No CD Info matches your search criteria. Pls try again!");
                    return;
                }
                lstResult.DataSource = matchingCDList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Add CD Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
            }
                
        }

        private void btnDetail_Click(object sender, EventArgs e)
        {

            // if no selection is made on ListBox 'lstResult', 
            // display some error message ...
            if (lstResult.SelectedIndex < 0)
                MessageBox.Show("Please select a row to view!");
            else
            {
                MusicCD mcd = (MusicCD)lstResult.SelectedItem;
                data.CDDetail = mcd;

                FrmSearchCDDetail formSearchCDDetail = new FrmSearchCDDetail(data, this);

                this.Hide();
                formSearchCDDetail.Show();
            }            
            
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.data.mainWindowForm.Show();
        }

        public ArrayList RetrieveMatchingCDInfo(string title, string composer, int edition)
        {
            ArrayList cdList = data.CDList;
            ArrayList matchingCDList = new ArrayList();

            // loop through the cdList to search for CD Info that matches :
            // the title OR the composer OR the edition
            // Note : Depending on how you want to search to work, the
            //        below if conditions may not be suitable !!
            //        (i.e. if you want only CDs that matches ALL conditions
            //              EXACTLY, then below if statement needs to change!
            foreach (MusicCD mcd in cdList)
            {
                if (mcd.Title.Contains(title) ||
                    mcd.Composer.Contains(composer) ||
                    (mcd.Edition == edition)
                    )
                    matchingCDList.Add(mcd);
            }

            return (matchingCDList);
        }

        public void ResetUIControls()
        {
            // reset the values of the TextBoxes to empty strings ...
            tbxTitle.Text = "";
            tbxComposer.Text = "";
            tbxEdition.Text = "";

            lstResult.DataSource = null;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetUIControls();
        }
 
    }   // end class FrmSearchCD ...
}
