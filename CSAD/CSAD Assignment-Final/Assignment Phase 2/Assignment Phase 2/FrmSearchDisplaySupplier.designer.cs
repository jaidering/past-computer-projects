﻿namespace Assignment_Phase_2
{
    partial class FrmSearchDisplaySupplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearchDisplaySupplier));
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnSearchToDisplaySupplier_Cancel = new System.Windows.Forms.Button();
            this.btnSearchToDisplaySupplier_Search = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rdbtnSearchToDisplaySupplier_City = new System.Windows.Forms.RadioButton();
            this.rdbtnSearchToDisplaySuplier_ContactName = new System.Windows.Forms.RadioButton();
            this.rdbtnSearchToDisplaySupplier_CompanyName = new System.Windows.Forms.RadioButton();
            this.tbxSearchToDisplaySupplier_Input = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(186, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 46);
            this.label1.TabIndex = 57;
            this.label1.Text = "  Search to Display\r\nSupplier Information";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(22, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(93, 77);
            this.pictureBox1.TabIndex = 56;
            this.pictureBox1.TabStop = false;
            // 
            // btnSearchToDisplaySupplier_Cancel
            // 
            this.btnSearchToDisplaySupplier_Cancel.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchToDisplaySupplier_Cancel.Location = new System.Drawing.Point(307, 226);
            this.btnSearchToDisplaySupplier_Cancel.Name = "btnSearchToDisplaySupplier_Cancel";
            this.btnSearchToDisplaySupplier_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btnSearchToDisplaySupplier_Cancel.TabIndex = 55;
            this.btnSearchToDisplaySupplier_Cancel.Text = "Cancel";
            this.btnSearchToDisplaySupplier_Cancel.UseVisualStyleBackColor = true;
            // 
            // btnSearchToDisplaySupplier_Search
            // 
            this.btnSearchToDisplaySupplier_Search.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchToDisplaySupplier_Search.Location = new System.Drawing.Point(216, 226);
            this.btnSearchToDisplaySupplier_Search.Name = "btnSearchToDisplaySupplier_Search";
            this.btnSearchToDisplaySupplier_Search.Size = new System.Drawing.Size(75, 23);
            this.btnSearchToDisplaySupplier_Search.TabIndex = 54;
            this.btnSearchToDisplaySupplier_Search.Text = "Search";
            this.btnSearchToDisplaySupplier_Search.UseVisualStyleBackColor = true;
            this.btnSearchToDisplaySupplier_Search.Click += new System.EventHandler(this.btnSearchToDisplaySupplier_Search_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(228, 15);
            this.label2.TabIndex = 53;
            this.label2.Text = "Please enter fill in the supplier information";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.rdbtnSearchToDisplaySupplier_City);
            this.groupBox1.Controls.Add(this.rdbtnSearchToDisplaySuplier_ContactName);
            this.groupBox1.Controls.Add(this.rdbtnSearchToDisplaySupplier_CompanyName);
            this.groupBox1.Controls.Add(this.tbxSearchToDisplaySupplier_Input);
            this.groupBox1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(22, 136);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(360, 74);
            this.groupBox1.TabIndex = 52;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search for Supplier";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 15);
            this.label3.TabIndex = 52;
            this.label3.Text = "Supplier Information:";
            // 
            // rdbtnSearchToDisplaySupplier_City
            // 
            this.rdbtnSearchToDisplaySupplier_City.AutoSize = true;
            this.rdbtnSearchToDisplaySupplier_City.Location = new System.Drawing.Point(287, 49);
            this.rdbtnSearchToDisplaySupplier_City.Name = "rdbtnSearchToDisplaySupplier_City";
            this.rdbtnSearchToDisplaySupplier_City.Size = new System.Drawing.Size(46, 19);
            this.rdbtnSearchToDisplaySupplier_City.TabIndex = 6;
            this.rdbtnSearchToDisplaySupplier_City.TabStop = true;
            this.rdbtnSearchToDisplaySupplier_City.Text = "City";
            this.rdbtnSearchToDisplaySupplier_City.UseVisualStyleBackColor = true;
            // 
            // rdbtnSearchToDisplaySuplier_ContactName
            // 
            this.rdbtnSearchToDisplaySuplier_ContactName.AutoSize = true;
            this.rdbtnSearchToDisplaySuplier_ContactName.Location = new System.Drawing.Point(161, 49);
            this.rdbtnSearchToDisplaySuplier_ContactName.Name = "rdbtnSearchToDisplaySuplier_ContactName";
            this.rdbtnSearchToDisplaySuplier_ContactName.Size = new System.Drawing.Size(96, 19);
            this.rdbtnSearchToDisplaySuplier_ContactName.TabIndex = 5;
            this.rdbtnSearchToDisplaySuplier_ContactName.TabStop = true;
            this.rdbtnSearchToDisplaySuplier_ContactName.Text = "Contact Name";
            this.rdbtnSearchToDisplaySuplier_ContactName.UseVisualStyleBackColor = true;
            // 
            // rdbtnSearchToDisplaySupplier_CompanyName
            // 
            this.rdbtnSearchToDisplaySupplier_CompanyName.AutoSize = true;
            this.rdbtnSearchToDisplaySupplier_CompanyName.Location = new System.Drawing.Point(29, 49);
            this.rdbtnSearchToDisplaySupplier_CompanyName.Name = "rdbtnSearchToDisplaySupplier_CompanyName";
            this.rdbtnSearchToDisplaySupplier_CompanyName.Size = new System.Drawing.Size(102, 19);
            this.rdbtnSearchToDisplaySupplier_CompanyName.TabIndex = 4;
            this.rdbtnSearchToDisplaySupplier_CompanyName.TabStop = true;
            this.rdbtnSearchToDisplaySupplier_CompanyName.Text = "Company Name";
            this.rdbtnSearchToDisplaySupplier_CompanyName.UseVisualStyleBackColor = true;
            // 
            // tbxSearchToDisplaySupplier_Input
            // 
            this.tbxSearchToDisplaySupplier_Input.Location = new System.Drawing.Point(161, 23);
            this.tbxSearchToDisplaySupplier_Input.Name = "tbxSearchToDisplaySupplier_Input";
            this.tbxSearchToDisplaySupplier_Input.Size = new System.Drawing.Size(172, 23);
            this.tbxSearchToDisplaySupplier_Input.TabIndex = 3;
            // 
            // FrmSearchDisplaySupplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 264);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnSearchToDisplaySupplier_Cancel);
            this.Controls.Add(this.btnSearchToDisplaySupplier_Search);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmSearchDisplaySupplier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmSearchDisplaySupplier";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnSearchToDisplaySupplier_Cancel;
        private System.Windows.Forms.Button btnSearchToDisplaySupplier_Search;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rdbtnSearchToDisplaySupplier_City;
        private System.Windows.Forms.RadioButton rdbtnSearchToDisplaySuplier_ContactName;
        private System.Windows.Forms.RadioButton rdbtnSearchToDisplaySupplier_CompanyName;
        private System.Windows.Forms.TextBox tbxSearchToDisplaySupplier_Input;

    }
}