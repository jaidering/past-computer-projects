﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_2
{
    public partial class FrmSearchDisplayProduct : Form
    {
        private AllDataClass allData;
        private bool cancel = false;

        public FrmSearchDisplayProduct(AllDataClass adc)
        {
            InitializeComponent();
            allData = adc;
            for (int i = 0; i < allData.categoryList.Count; i++)
            {
                if (allData.categoryList[i] != null)
                {
                    Category c = (Category)allData.categoryList[i];
                    cbxSearchDisplayProductCategory.Items.Add(c.Name);
                }
            }
        }

        public bool Cancel
        {
            get
            {
                return cancel;
            }
            set
            {
                cancel = value;
            }
        }

        private void btnSearchDisplayProduct_Click(object sender, EventArgs e)
        {   //Product Name only
            allData.storeTempProductList.Clear();
            if(tbxSearchDisplayProductProductNameOnly.Text.Trim() != "" && 
                tbxSearchDisplayProductProductName.Text.Trim() == "" && tbxSearchDisplayProductModelNo.Text.Trim() == "" &&
                cbxSearchDisplayProductCategory.Text.Trim() == "" && tbxSearchDisplayProductMSRPX.Text.Trim() == "" && tbxSearchDisplayProductMSRPY.Text.Trim() == "")
            {
                if (searchForExistingProductToDisplay(tbxSearchDisplayProductProductNameOnly.Text.Trim()) == true)
                {
                    allData.foundSearch = true;
                    MessageBox.Show("Product Name matches! Proceed to select product to display","Product Found");
                    cancel = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Product not found!","Product Not Found");
                }
            }
            //Category and X<= MSRP <= Y
            else if (tbxSearchDisplayProductProductNameOnly.Text.Trim() == "" &&
                tbxSearchDisplayProductProductName.Text.Trim() == "" && tbxSearchDisplayProductModelNo.Text.Trim() == "" &&
                cbxSearchDisplayProductCategory.Text.Trim() != "" && tbxSearchDisplayProductMSRPX.Text.Trim() != "" && tbxSearchDisplayProductMSRPY.Text.Trim() != "")
            {
       
                    try
                    {
                        double x = Convert.ToDouble(tbxSearchDisplayProductMSRPX.Text.Trim());
                        double y = Convert.ToDouble(tbxSearchDisplayProductMSRPY.Text.Trim());

                        if (x <= y)
                        {
                            if (searchForExistingProductToDisplay(cbxSearchDisplayProductCategory.Text.Trim(), x, y) == true)
                            {
                                allData.foundSearch = true;
                                MessageBox.Show("Product Name matches! Proceed to select product to display", "Product Found");
                                cancel = true;
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("Product not found!", "Product Not Found");
                            }
                        }

                        else
                        {
                            if (searchForExistingProductToDisplay(cbxSearchDisplayProductCategory.Text.Trim(), y, x) == true)
                            {
                                allData.foundSearch = true;
                                MessageBox.Show("Product Name matches! Proceed to select product to display", "Product Found");
                                cancel = true;
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("Product not found!", "Product Not Found");
                            }
                        }
                    }

                    catch
                    {
                        MessageBox.Show("Please key in number with or without decimal place for MSRP Range", "Error In Number Format");
                    }
                }

            
            //Product Name and Model Number
            else if (tbxSearchDisplayProductProductNameOnly.Text.Trim() == "" &&
                tbxSearchDisplayProductProductName.Text.Trim() != "" && tbxSearchDisplayProductModelNo.Text.Trim() != "" &&
                cbxSearchDisplayProductCategory.Text.Trim() == "" && tbxSearchDisplayProductMSRPX.Text.Trim() == "" && tbxSearchDisplayProductMSRPY.Text.Trim() == "")
            {
                if (tbxSearchDisplayProductModelNo.Text.Length == 8)
                {
                    if (searchForExistingProductToDisplay(tbxSearchDisplayProductProductName.Text.Trim(), tbxSearchDisplayProductModelNo.Text.Trim()) == true)
                    {
                        allData.foundSearch = true;
                        MessageBox.Show("Product Name matches! Proceed to select product to display","Product Found");
                        cancel = true;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Product not found!","Error In Searching");
                    }
                }

                else
                {
                    MessageBox.Show("Model number must be of 8 characters", "Error In Searching");
                }
            }

            else
            {
                MessageBox.Show("Please use ONLY 1 of the 3 combination to search for exisitng product.","Error In Searching");
            }
        }

        public bool searchForExistingProductToDisplay(string searchNameOfProductOnly)
        {
            bool checkIfProductExist = false;

            for (int i = 0; i < allData.productList.Count; i++)
            {
                if (allData.productList[i] != null)
                {
                    Product productInArray = (Product)allData.productList[i];
                    if (productInArray.Name == searchNameOfProductOnly)
                    {
                        checkIfProductExist = true;
                        allData.storeTempProductList.Add(productInArray);
                        //allData.storeTempProductListForDisplay.Add(productInArray);
                    }

                }

            }

            return checkIfProductExist;
        }

        public bool searchForExistingProductToDisplay(string category, double x, double y)
        {
            bool checkIfProductExist = false;

            for (int i = 0; i < allData.productList.Count; i++)
            {
                if (allData.productList[i] != null)
                {
                    Product productInArray = (Product)allData.productList[i];
                    if (productInArray.Category == category && productInArray.MSRP >= x && productInArray.MSRP <= y)
                    {
                        checkIfProductExist = true;
                        allData.storeTempProductList.Add(productInArray);
                        //allData.storeTempProductListForDisplay.Add(productInArray);
                    }
                }
            }

            return checkIfProductExist;
        }

        public bool searchForExistingProductToDisplay(string searchNameOfProduct, string modelNumber)
        {
            bool checkIfProductExist = false;

            for (int i = 0; i < allData.productList.Count; i++)
            {
                if (allData.productList[i] != null)
                {
                    Product productInArray = (Product)allData.productList[i];
                    if (searchNameOfProduct == productInArray.Name && modelNumber == productInArray.ModelNumber)
                    {
                        checkIfProductExist = true;
                        allData.storeTempProductList.Add(productInArray);
                        //allData.storeTempProductListForDisplay.Add(productInArray);
                    }
                }
            }

            return checkIfProductExist;
        }

        private void btnCancelSearchDisplayProduct_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
