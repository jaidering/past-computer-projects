﻿namespace Assignment_Phase_2
{
    partial class FrmSearchDisplayProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearchDisplayProduct));
            this.panelSearchDisplayProductName = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCancelSearchDisplayProduct = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tbxSearchDisplayProductMSRPY = new System.Windows.Forms.TextBox();
            this.tbxSearchDisplayProductMSRPX = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbxSearchDisplayProductProductName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxSearchDisplayProductCategory = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxSearchDisplayProductModelNo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbxSearchDisplayProductProductNameOnly = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSearchDisplayProduct = new System.Windows.Forms.Button();
            this.panelSearchDisplayProductName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSearchDisplayProductName
            // 
            this.panelSearchDisplayProductName.Controls.Add(this.label15);
            this.panelSearchDisplayProductName.Controls.Add(this.label3);
            this.panelSearchDisplayProductName.Controls.Add(this.pictureBox1);
            this.panelSearchDisplayProductName.Controls.Add(this.btnCancelSearchDisplayProduct);
            this.panelSearchDisplayProductName.Controls.Add(this.groupBox1);
            this.panelSearchDisplayProductName.Controls.Add(this.btnSearchDisplayProduct);
            this.panelSearchDisplayProductName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSearchDisplayProductName.Location = new System.Drawing.Point(0, 0);
            this.panelSearchDisplayProductName.Name = "panelSearchDisplayProductName";
            this.panelSearchDisplayProductName.Size = new System.Drawing.Size(464, 289);
            this.panelSearchDisplayProductName.TabIndex = 50;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(20, 90);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(314, 15);
            this.label15.TabIndex = 55;
            this.label15.Text = "Please use only ONE of the following search mode - 1, 2 or 3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(147, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(305, 23);
            this.label3.TabIndex = 53;
            this.label3.Text = "Search && Display Product Information";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(23, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(93, 77);
            this.pictureBox1.TabIndex = 52;
            this.pictureBox1.TabStop = false;
            // 
            // btnCancelSearchDisplayProduct
            // 
            this.btnCancelSearchDisplayProduct.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelSearchDisplayProduct.Location = new System.Drawing.Point(377, 260);
            this.btnCancelSearchDisplayProduct.Name = "btnCancelSearchDisplayProduct";
            this.btnCancelSearchDisplayProduct.Size = new System.Drawing.Size(75, 23);
            this.btnCancelSearchDisplayProduct.TabIndex = 51;
            this.btnCancelSearchDisplayProduct.Text = "Cancel";
            this.btnCancelSearchDisplayProduct.UseVisualStyleBackColor = true;
            this.btnCancelSearchDisplayProduct.Click += new System.EventHandler(this.btnCancelSearchDisplayProduct_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.tbxSearchDisplayProductMSRPY);
            this.groupBox1.Controls.Add(this.tbxSearchDisplayProductMSRPX);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbxSearchDisplayProductProductName);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbxSearchDisplayProductCategory);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbxSearchDisplayProductModelNo);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbxSearchDisplayProductProductNameOnly);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(11, 108);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(441, 148);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search for Stock";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(236, 85);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 15);
            this.label14.TabIndex = 68;
            this.label14.Text = "( Inclusive )";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(366, 69);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 15);
            this.label13.TabIndex = 67;
            this.label13.Text = "to";
            // 
            // tbxSearchDisplayProductMSRPY
            // 
            this.tbxSearchDisplayProductMSRPY.Location = new System.Drawing.Point(385, 66);
            this.tbxSearchDisplayProductMSRPY.Name = "tbxSearchDisplayProductMSRPY";
            this.tbxSearchDisplayProductMSRPY.Size = new System.Drawing.Size(50, 23);
            this.tbxSearchDisplayProductMSRPY.TabIndex = 66;
            // 
            // tbxSearchDisplayProductMSRPX
            // 
            this.tbxSearchDisplayProductMSRPX.Location = new System.Drawing.Point(313, 66);
            this.tbxSearchDisplayProductMSRPX.Name = "tbxSearchDisplayProductMSRPX";
            this.tbxSearchDisplayProductMSRPX.Size = new System.Drawing.Size(50, 23);
            this.tbxSearchDisplayProductMSRPX.TabIndex = 65;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(236, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 15);
            this.label11.TabIndex = 64;
            this.label11.Text = "MSRP Range:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 115);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 15);
            this.label12.TabIndex = 63;
            this.label12.Text = "3.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 15);
            this.label10.TabIndex = 62;
            this.label10.Text = "2.";
            // 
            // tbxSearchDisplayProductProductName
            // 
            this.tbxSearchDisplayProductProductName.Location = new System.Drawing.Point(111, 110);
            this.tbxSearchDisplayProductProductName.Name = "tbxSearchDisplayProductProductName";
            this.tbxSearchDisplayProductProductName.Size = new System.Drawing.Size(119, 23);
            this.tbxSearchDisplayProductProductName.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 15);
            this.label8.TabIndex = 56;
            this.label8.Text = "1.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(153, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 15);
            this.label4.TabIndex = 61;
            this.label4.Text = "OR";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(153, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 15);
            this.label2.TabIndex = 60;
            this.label2.Text = "OR";
            // 
            // cbxSearchDisplayProductCategory
            // 
            this.cbxSearchDisplayProductCategory.FormattingEnabled = true;
            this.cbxSearchDisplayProductCategory.Location = new System.Drawing.Point(111, 66);
            this.cbxSearchDisplayProductCategory.Name = "cbxSearchDisplayProductCategory";
            this.cbxSearchDisplayProductCategory.Size = new System.Drawing.Size(119, 23);
            this.cbxSearchDisplayProductCategory.TabIndex = 56;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 15);
            this.label1.TabIndex = 59;
            this.label1.Text = "Product Name:";
            // 
            // tbxSearchDisplayProductModelNo
            // 
            this.tbxSearchDisplayProductModelNo.Location = new System.Drawing.Point(313, 110);
            this.tbxSearchDisplayProductModelNo.Name = "tbxSearchDisplayProductModelNo";
            this.tbxSearchDisplayProductModelNo.Size = new System.Drawing.Size(122, 23);
            this.tbxSearchDisplayProductModelNo.TabIndex = 58;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(24, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 15);
            this.label9.TabIndex = 55;
            this.label9.Text = "Category:";
            // 
            // tbxSearchDisplayProductProductNameOnly
            // 
            this.tbxSearchDisplayProductProductNameOnly.Location = new System.Drawing.Point(111, 22);
            this.tbxSearchDisplayProductProductNameOnly.Name = "tbxSearchDisplayProductProductNameOnly";
            this.tbxSearchDisplayProductProductNameOnly.Size = new System.Drawing.Size(119, 23);
            this.tbxSearchDisplayProductProductNameOnly.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Product Name:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(236, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 15);
            this.label6.TabIndex = 2;
            this.label6.Text = "Model No:";
            // 
            // btnSearchDisplayProduct
            // 
            this.btnSearchDisplayProduct.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchDisplayProduct.Location = new System.Drawing.Point(287, 260);
            this.btnSearchDisplayProduct.Name = "btnSearchDisplayProduct";
            this.btnSearchDisplayProduct.Size = new System.Drawing.Size(75, 23);
            this.btnSearchDisplayProduct.TabIndex = 50;
            this.btnSearchDisplayProduct.Text = "Search";
            this.btnSearchDisplayProduct.UseVisualStyleBackColor = true;
            this.btnSearchDisplayProduct.Click += new System.EventHandler(this.btnSearchDisplayProduct_Click);
            // 
            // FrmSearchDisplayProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 289);
            this.Controls.Add(this.panelSearchDisplayProductName);
            this.Name = "FrmSearchDisplayProduct";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Display Existing Product Information";
            this.panelSearchDisplayProductName.ResumeLayout(false);
            this.panelSearchDisplayProductName.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSearchDisplayProductName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnCancelSearchDisplayProduct;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbxSearchDisplayProductProductNameOnly;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbxSearchDisplayProductProductName;
        private System.Windows.Forms.Button btnSearchDisplayProduct;
        private System.Windows.Forms.TextBox tbxSearchDisplayProductModelNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxSearchDisplayProductCategory;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbxSearchDisplayProductMSRPX;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbxSearchDisplayProductMSRPY;
        private System.Windows.Forms.Label label15;
    }
}