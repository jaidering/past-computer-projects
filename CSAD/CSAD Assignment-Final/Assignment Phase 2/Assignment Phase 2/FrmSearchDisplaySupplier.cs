﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_2
{
    public partial class FrmSearchDisplaySupplier : Form
    {
        private AllDataClass allData;
        private bool cancel = false;
        public FrmSearchDisplaySupplier(AllDataClass adc)
        {
            InitializeComponent();
            allData = adc;
        }

        public bool Cancel
        {
            get
            {
                return cancel;
            }
            set
            {
                cancel = value;
            }
        }

        private void btnSearchToDisplaySupplier_Search_Click(object sender, EventArgs e)
        {
            allData.frmMainStaff.UpdateListBox();
            allData.tempSupplierList.Clear();
            if (rdbtnSearchToDisplaySuplier_ContactName.Checked == true)
            {
                if (searchForExistingCompany_conName(tbxSearchToDisplaySupplier_Input.Text.Trim()) == true)
                {
                    allData.findSearch = true;
                    MessageBox.Show("Contact Name matches! Proceed to select company to display");
                    cancel = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Contact not found!");
                }
            }
            else if (rdbtnSearchToDisplaySupplier_CompanyName.Checked == true)
            {
                if(searchForExistingCompany_comName(tbxSearchToDisplaySupplier_Input.Text.Trim()) == true)
                {
                    allData.findSearch = true;
                    MessageBox.Show("Company Name matches! Proceed to select company to display");
                    cancel = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Company not found!");
                }
                
            }
            else if(rdbtnSearchToDisplaySupplier_City.Checked == true)
            {
                if(searchForExistingCompany_city(tbxSearchToDisplaySupplier_Input.Text.Trim())== true)
                {
                    allData.findSearch = true;
                    MessageBox.Show("City matches! Proceed to select company to display");
                    cancel = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("City not found!");
                }
                
            }
        }

        //companyName
        public bool searchForExistingCompany_comName(string companyName)
        {
            bool checkIfCompanyExists = false;

            for (int i = 0; i < allData.inventoryList.Count; i++)
            {
                if (allData.inventoryList[i] != null)
                {
                    Supplier supplierInArray = (Supplier)allData.inventoryList[i];
                    if (supplierInArray.CompanyName == companyName)
                    {
                        checkIfCompanyExists = true;
                        allData.tempSupplierList.Add(supplierInArray);
                    }
                }

            }
            return checkIfCompanyExists;

        }

        //contactName
        public bool searchForExistingCompany_conName(string contactName)
        {
            bool checkIfCompanyExists = false;

            for (int i = 0; i < allData.inventoryList.Count; i++)
            {
                if (allData.inventoryList[i] != null)
                {
                    Supplier supplierInArray = (Supplier)allData.inventoryList[i];
                    if (supplierInArray.ContactName == contactName)
                    {
                        checkIfCompanyExists = true;
                        allData.tempSupplierList.Add(supplierInArray);
                    }
                }

            }
            return checkIfCompanyExists;

        }

        //city
        public bool searchForExistingCompany_city(string city)
        {
            bool checkIfCompanyExists = false;

            for (int i = 0; i < allData.inventoryList.Count; i++)
            {
                if (allData.inventoryList[i] != null)
                {
                    Supplier supplierInArray = (Supplier)allData.inventoryList[i];
                    if (supplierInArray.City == city)
                    {
                        checkIfCompanyExists = true;
                        allData.tempSupplierList.Add(supplierInArray);
                    }
                }

            }
            return checkIfCompanyExists;

        }

    }
}