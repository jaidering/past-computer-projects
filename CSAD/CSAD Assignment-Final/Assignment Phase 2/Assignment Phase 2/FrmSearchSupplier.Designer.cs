﻿namespace Assignment_Phase_2
{
    partial class FrmSearchSupplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearchSupplier));
            this.pnlSearchCompanyName = new System.Windows.Forms.Panel();
            this.lblSearchScreen = new System.Windows.Forms.Label();
            this.picBoxLogoSearchFrm = new System.Windows.Forms.PictureBox();
            this.btnSearchCompany_Cancel = new System.Windows.Forms.Button();
            this.btnSearchCompany_Search = new System.Windows.Forms.Button();
            this.lblSearchandDispInfo = new System.Windows.Forms.Label();
            this.grpBxSearchSupplier = new System.Windows.Forms.GroupBox();
            this.tbxSearchCompanyName = new System.Windows.Forms.TextBox();
            this.lblSearchStaffName = new System.Windows.Forms.Label();
            this.pnlSearchCompanyName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoSearchFrm)).BeginInit();
            this.grpBxSearchSupplier.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSearchCompanyName
            // 
            this.pnlSearchCompanyName.Controls.Add(this.lblSearchScreen);
            this.pnlSearchCompanyName.Controls.Add(this.picBoxLogoSearchFrm);
            this.pnlSearchCompanyName.Controls.Add(this.btnSearchCompany_Cancel);
            this.pnlSearchCompanyName.Controls.Add(this.btnSearchCompany_Search);
            this.pnlSearchCompanyName.Controls.Add(this.lblSearchandDispInfo);
            this.pnlSearchCompanyName.Controls.Add(this.grpBxSearchSupplier);
            this.pnlSearchCompanyName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchCompanyName.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchCompanyName.Name = "pnlSearchCompanyName";
            this.pnlSearchCompanyName.Size = new System.Drawing.Size(384, 214);
            this.pnlSearchCompanyName.TabIndex = 46;
            // 
            // lblSearchScreen
            // 
            this.lblSearchScreen.AutoSize = true;
            this.lblSearchScreen.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchScreen.Location = new System.Drawing.Point(158, 33);
            this.lblSearchScreen.Name = "lblSearchScreen";
            this.lblSearchScreen.Size = new System.Drawing.Size(164, 23);
            this.lblSearchScreen.TabIndex = 45;
            this.lblSearchScreen.Text = "Search for Supplier";
            // 
            // picBoxLogoSearchFrm
            // 
            this.picBoxLogoSearchFrm.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoSearchFrm.Image")));
            this.picBoxLogoSearchFrm.Location = new System.Drawing.Point(23, 8);
            this.picBoxLogoSearchFrm.Name = "picBoxLogoSearchFrm";
            this.picBoxLogoSearchFrm.Size = new System.Drawing.Size(93, 77);
            this.picBoxLogoSearchFrm.TabIndex = 44;
            this.picBoxLogoSearchFrm.TabStop = false;
            // 
            // btnSearchCompany_Cancel
            // 
            this.btnSearchCompany_Cancel.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchCompany_Cancel.Location = new System.Drawing.Point(296, 187);
            this.btnSearchCompany_Cancel.Name = "btnSearchCompany_Cancel";
            this.btnSearchCompany_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btnSearchCompany_Cancel.TabIndex = 7;
            this.btnSearchCompany_Cancel.Text = "Cancel";
            this.btnSearchCompany_Cancel.UseVisualStyleBackColor = true;
            this.btnSearchCompany_Cancel.Click += new System.EventHandler(this.btnSearchCompany_Cancel_Click);
            // 
            // btnSearchCompany_Search
            // 
            this.btnSearchCompany_Search.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchCompany_Search.Location = new System.Drawing.Point(197, 187);
            this.btnSearchCompany_Search.Name = "btnSearchCompany_Search";
            this.btnSearchCompany_Search.Size = new System.Drawing.Size(75, 23);
            this.btnSearchCompany_Search.TabIndex = 6;
            this.btnSearchCompany_Search.Text = "Search";
            this.btnSearchCompany_Search.UseVisualStyleBackColor = true;
            this.btnSearchCompany_Search.Click += new System.EventHandler(this.btnSearchCompany_Search_Click);
            // 
            // lblSearchandDispInfo
            // 
            this.lblSearchandDispInfo.AutoSize = true;
            this.lblSearchandDispInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchandDispInfo.Location = new System.Drawing.Point(8, 90);
            this.lblSearchandDispInfo.Name = "lblSearchandDispInfo";
            this.lblSearchandDispInfo.Size = new System.Drawing.Size(226, 15);
            this.lblSearchandDispInfo.TabIndex = 5;
            this.lblSearchandDispInfo.Text = "Please enter the supplier\'s company name...";
            // 
            // grpBxSearchSupplier
            // 
            this.grpBxSearchSupplier.Controls.Add(this.tbxSearchCompanyName);
            this.grpBxSearchSupplier.Controls.Add(this.lblSearchStaffName);
            this.grpBxSearchSupplier.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBxSearchSupplier.Location = new System.Drawing.Point(9, 108);
            this.grpBxSearchSupplier.Name = "grpBxSearchSupplier";
            this.grpBxSearchSupplier.Size = new System.Drawing.Size(360, 74);
            this.grpBxSearchSupplier.TabIndex = 4;
            this.grpBxSearchSupplier.TabStop = false;
            this.grpBxSearchSupplier.Text = "Search for Supplier";
            // 
            // tbxSearchCompanyName
            // 
            this.tbxSearchCompanyName.Location = new System.Drawing.Point(112, 31);
            this.tbxSearchCompanyName.Name = "tbxSearchCompanyName";
            this.tbxSearchCompanyName.Size = new System.Drawing.Size(201, 23);
            this.tbxSearchCompanyName.TabIndex = 3;
            // 
            // lblSearchStaffName
            // 
            this.lblSearchStaffName.AutoSize = true;
            this.lblSearchStaffName.Location = new System.Drawing.Point(25, 34);
            this.lblSearchStaffName.Name = "lblSearchStaffName";
            this.lblSearchStaffName.Size = new System.Drawing.Size(87, 15);
            this.lblSearchStaffName.TabIndex = 2;
            this.lblSearchStaffName.Text = "Company Name:";
            // 
            // FrmSearchSupplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 214);
            this.Controls.Add(this.pnlSearchCompanyName);
            this.Name = "FrmSearchSupplier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search Supplier";
            this.pnlSearchCompanyName.ResumeLayout(false);
            this.pnlSearchCompanyName.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoSearchFrm)).EndInit();
            this.grpBxSearchSupplier.ResumeLayout(false);
            this.grpBxSearchSupplier.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearchCompanyName;
        private System.Windows.Forms.Label lblSearchScreen;
        private System.Windows.Forms.PictureBox picBoxLogoSearchFrm;
        private System.Windows.Forms.Button btnSearchCompany_Cancel;
        private System.Windows.Forms.Button btnSearchCompany_Search;
        private System.Windows.Forms.Label lblSearchandDispInfo;
        private System.Windows.Forms.GroupBox grpBxSearchSupplier;
        private System.Windows.Forms.TextBox tbxSearchCompanyName;
        private System.Windows.Forms.Label lblSearchStaffName;

    }
}