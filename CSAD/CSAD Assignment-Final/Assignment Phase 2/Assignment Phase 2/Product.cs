﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Drawing;

namespace Assignment_Phase_2
{
    public class Product
    {
        //Attributes
        private string name;
        private string modelNumber;
        private string descrption;
        private double msrp;
        private double retailPrice;
        private string category;
        private Bitmap image1;
        private Bitmap image2;

        //Constructor
        public Product(string name, string descrption, string modelNumber, double msrp, double retailPrice, string category)
        {
            this.name = name;
            this.descrption = descrption;
            this.modelNumber = modelNumber;
            this.msrp = msrp;
            this.retailPrice = retailPrice;
            this.category = category;
        }

        //Methods
        public string Name
        {
            get { return name; }
        }

        public string ModelNumber
        {
            get { return modelNumber; }
        }

        public string Descrption
        {
            get { return descrption; }
            set { descrption = value; }
        }

        public double MSRP
        {
            get { return msrp; }
            set { msrp = value; }
        }

        public double RetailPrice
        {
            get { return retailPrice; }
            set { retailPrice = value; }
        }

        public string Category
        {
            get { return category; }
            set { category = value; }
        }

        public Bitmap Image1
        {
            get { return image1; }
            set { image1 = value; }
        }

        public Bitmap Image2
        {
            get { return image2; }
            set { image2 = value; }
        }
    }
}
