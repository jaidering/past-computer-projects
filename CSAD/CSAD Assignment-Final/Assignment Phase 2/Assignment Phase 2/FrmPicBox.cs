﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_2
{
    public partial class FrmPictureBox : Form
    {
        private Bitmap image;
        public FrmPictureBox(Bitmap image)
        {
            InitializeComponent();
            this.image = image;
            picbxBigImage.Image = image;
            picbxBigImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage; 
        }

        private void btnClosePicBx_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
