﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_2
{
    public partial class FrmSearchOrder : Form
    {
        private AllDataClass allData;
        private int n;
        private bool stopSearchDialogue = false;

        public FrmSearchOrder(AllDataClass adc, int n)
        {
            InitializeComponent();
            allData = adc;
            this.n = n;
            if (n == 4)
            {
                pnlSearchDispOrder.Visible = true;
                pnlSearchOrder.Visible = false;
            }
            else
            {
                pnlSearchDispOrder.Visible = false;
                pnlSearchOrder.Visible = true;
            }
        }

        private void btnSearchDispOrder_Click(object sender, EventArgs e)
        {
            if (tbxSearchByOrderMode.Text != "")
            {
                if (comboBxSearchOrderMode.Text == "Search by Order Reference Number only")
                {
                    if (searchForOrderByRefNum(Convert.ToInt32(tbxSearchByOrderMode.Text.Trim())) == true)
                    {
                        allData.foundSearchEntry = true;
                        MessageBox.Show("Existing order information is found. Proceed to view order information.", "View Order Information");
                        stopSearchDialogue = true;
                        this.Close();
                    }
                    else
                    {
                        allData.foundSearchEntry = false;
                        MessageBox.Show("Existing order information is not found to display.", "Error!");
                    }

                }

                if (comboBxSearchOrderMode.Text == "Search by Customer Name only")
                {
                    if (searchForOrderByCustName(tbxSearchByOrderMode.Text.Trim()) == true)
                    {
                        allData.foundSearchEntry = true;
                        MessageBox.Show("Existing order information is found. Proceed to view staff information.", "View Order Information");
                        stopSearchDialogue = true;
                        this.Close();
                    }
                    else
                    {
                        allData.foundSearchEntry = false;
                        MessageBox.Show("Existing order information is not found to display.", "Error!");
                    }
                }
            }
            else
            {
                MessageBox.Show("Please enter a search term to search for existing order information", "Error!");
            }
        }

        private void btnCancelSearchDispOrder_Click(object sender, EventArgs e)
        {
            this.Close();
            stopSearchDialogue = false;
            allData.frmMainStaff.Show();
        }

        private void btnSearchOrder_Click(object sender, EventArgs e)
        {
            if (tbxSearchOrderRefNum.Text.Trim() == "")
            {
                MessageBox.Show("Please enter an Order Reference Number to search!", "Error!");
            }
            else
            {
                if (n == 1)//add order
                {
                    if (searchForOrder() == true)
                    {
                        allData.foundSearchEntry = true;
                        MessageBox.Show("Unable to add order information. The Order already exists!", "Error!");
                    }
                    else
                    {
                        allData.foundSearchEntry = false;
                        MessageBox.Show("Order information is not found. Please proceed to add an order.", "Add Order");
                        stopSearchDialogue = true;
                        this.Close();
                    }
                }
                else if (n == 2)//modify order
                {
                    if (searchForOrder() == true)
                    {
                        allData.foundSearchEntry = true;
                        MessageBox.Show("Order information is found! Please proceed to modify an order.", "Modify Order Information");
                        allData.orderRef = Convert.ToInt32(tbxSearchOrderRefNum.Text);
                        stopSearchDialogue = true;
                        this.Close();
                    }
                    else
                    {
                        allData.foundSearchEntry = false;
                        MessageBox.Show("Order Information is not found to modify.", "Error!");
                    }
                }
                else if (n == 3)//delete order
                {
                    if (searchForOrder() == true)
                    {
                        allData.foundSearchEntry = true;
                        MessageBox.Show("Order information is found! Please proceed to delete order information.", "Delete Order Information");
                        allData.orderRef = Convert.ToInt32(tbxSearchOrderRefNum.Text);
                        stopSearchDialogue = true;
                        this.Close();
                    }
                    else
                    {
                        allData.foundSearchEntry = false;
                        MessageBox.Show("Order Information is not found to delete.", "Error!");
                    }

                }
            }
        }

        private void btnCancelSearchOrder_Click(object sender, EventArgs e)
        {
            this.Close();
            stopSearchDialogue = false;
            allData.frmMainStaff.Show();
        }

        public bool searchForOrder()
        {
            bool searchOrder = false;

            for (int i = 0; i < allData.orderList.Count; i++)
            {
                if (allData.orderList[i] != null)
                {
                    Order orderArray = (Order)allData.orderList[i];
                    if (orderArray.OrderRefNum == Convert.ToInt32(tbxSearchOrderRefNum.Text.Trim()))
                    {
                        searchOrder = true;
                        allData.storeTempOrder = (Order)allData.orderList[i];
                        break;
                    }
                    else
                    {
                        searchOrder = false;
                    }
                }
            }
            return searchOrder;
        }

        public bool StopSearchDialog
        {
            get
            {
                return stopSearchDialogue;
            }
            set
            {
                stopSearchDialogue = value;
            }
        }

        public bool searchForOrderByRefNum(int n)
        {
            bool searchOrder = false;

            for (int i = 0; i < allData.orderList.Count; i++)
            {
                if (allData.orderList[i] != null)
                {
                    Order orderArray = (Order)allData.orderList[i];
                    if (orderArray.OrderRefNum == n)
                    {
                        searchOrder = true;
                        allData.storeTempOrder = (Order)allData.orderList[i];
                        break;
                    }
                    else
                    {
                        searchOrder = false;
                    }
                }
            }
            return searchOrder;
        }

        public bool searchForOrderByCustName(string custName)
        {
            bool searchOrder = false;

            for (int i = 0; i < allData.orderList.Count; i++)
            {
                if (allData.orderList[i] != null)
                {
                    Order orderArray = (Order)allData.orderList[i];
                    if (orderArray.CustName == custName)
                    {
                        searchOrder = true;
                        allData.storeTempOrder = (Order)allData.orderList[i];
                        break;
                    }
                    else
                    {
                        searchOrder = false;
                    }
                }
            }
            return searchOrder;
        }
    }
}
