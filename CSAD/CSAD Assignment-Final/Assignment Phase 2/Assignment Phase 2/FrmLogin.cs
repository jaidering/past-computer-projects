﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_2
{
    public partial class FrmLogin : Form
    {
        private AllDataClass allData;
        private bool validPassword;

        public FrmLogin()
        {
            allData = new AllDataClass(this);
            InitializeComponent();
            validPassword = false;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (tbxNameLogin.Text.Trim() != "" && tbxPasswordLogin.Text.Trim() != "")
            {
                CheckForValidPassword();
                if (validPassword == true)
                {
                    this.validPassword = false;
                    tbxNameLogin.Text = "";
                    tbxPasswordLogin.Text = "";
                    FrmMainWindowStaff f = new FrmMainWindowStaff(allData);
                    //this.Hide();
                    this.Hide();
                    f.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Invalid Password!", "Invalid Password");
                }
            }
            else if (tbxNameLogin.Text.Trim() == "" || tbxPasswordLogin.Text.Trim() == "")
            {
                MessageBox.Show("Please key in your log in details", "Log In Failed");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void CheckForValidPassword()
        {
            if (tbxNameLogin.Text.ToUpper().Trim() == "ADMIN" && tbxPasswordLogin.Text.ToUpper().Trim() == "PASS123")
            {
                allData.defaultAdmin = true;
                this.validPassword = true;

            }
            else
            {
                for (int i = 0; i < allData.staffList.Count; i++)
                {
                    if (allData.staffList[i] != null)
                    {
                        Staff s = (Staff)allData.staffList[i];

                        if (s.Name == tbxNameLogin.Text.Trim() && s.Password == tbxPasswordLogin.Text.Trim())
                        {

                            allData.roleOfStaffLogIn.Add(s.Role1);
                            allData.roleOfStaffLogIn.Add(s.Role2);
                            this.validPassword = true;
                            break;
                        }
                    }
                }
            }
        }
    }
}
