﻿namespace Assignment_Phase_2
{
    partial class FrmSearchStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearchStock));
            this.pnlSearchStock_Display = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnSearchStockEdit_Cancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cboxSearchToEdit_ModelNum = new System.Windows.Forms.ComboBox();
            this.cmboxSearchToEditAdd_CompanyName = new System.Windows.Forms.ComboBox();
            this.cmboxSupplier_ProductName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSearchStockEdit_Search = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlSearchStock_Display.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSearchStock_Display
            // 
            this.pnlSearchStock_Display.Controls.Add(this.label11);
            this.pnlSearchStock_Display.Controls.Add(this.label3);
            this.pnlSearchStock_Display.Controls.Add(this.pictureBox1);
            this.pnlSearchStock_Display.Controls.Add(this.btnSearchStockEdit_Cancel);
            this.pnlSearchStock_Display.Controls.Add(this.groupBox1);
            this.pnlSearchStock_Display.Controls.Add(this.btnSearchStockEdit_Search);
            this.pnlSearchStock_Display.Controls.Add(this.label7);
            this.pnlSearchStock_Display.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchStock_Display.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchStock_Display.Name = "pnlSearchStock_Display";
            this.pnlSearchStock_Display.Size = new System.Drawing.Size(464, 289);
            this.pnlSearchStock_Display.TabIndex = 49;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(176, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(193, 23);
            this.label11.TabIndex = 55;
            this.label11.Text = "(Edit Product Quantity)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(200, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 23);
            this.label3.TabIndex = 53;
            this.label3.Text = "Search for Stock ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(23, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(93, 77);
            this.pictureBox1.TabIndex = 52;
            this.pictureBox1.TabStop = false;
            // 
            // btnSearchStockEdit_Cancel
            // 
            this.btnSearchStockEdit_Cancel.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchStockEdit_Cancel.Location = new System.Drawing.Point(377, 253);
            this.btnSearchStockEdit_Cancel.Name = "btnSearchStockEdit_Cancel";
            this.btnSearchStockEdit_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btnSearchStockEdit_Cancel.TabIndex = 51;
            this.btnSearchStockEdit_Cancel.Text = "Cancel";
            this.btnSearchStockEdit_Cancel.UseVisualStyleBackColor = true;
            this.btnSearchStockEdit_Cancel.Click += new System.EventHandler(this.btnSearchStockEdit_Cancel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cboxSearchToEdit_ModelNum);
            this.groupBox1.Controls.Add(this.cmboxSearchToEditAdd_CompanyName);
            this.groupBox1.Controls.Add(this.cmboxSupplier_ProductName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(11, 108);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(441, 139);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search for Stock";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 15);
            this.label6.TabIndex = 71;
            this.label6.Text = "Model Number :";
            // 
            // cboxSearchToEdit_ModelNum
            // 
            this.cboxSearchToEdit_ModelNum.FormattingEnabled = true;
            this.cboxSearchToEdit_ModelNum.Location = new System.Drawing.Point(193, 68);
            this.cboxSearchToEdit_ModelNum.Name = "cboxSearchToEdit_ModelNum";
            this.cboxSearchToEdit_ModelNum.Size = new System.Drawing.Size(227, 23);
            this.cboxSearchToEdit_ModelNum.TabIndex = 70;
            // 
            // cmboxSearchToEditAdd_CompanyName
            // 
            this.cmboxSearchToEditAdd_CompanyName.FormattingEnabled = true;
            this.cmboxSearchToEditAdd_CompanyName.Location = new System.Drawing.Point(193, 99);
            this.cmboxSearchToEditAdd_CompanyName.Name = "cmboxSearchToEditAdd_CompanyName";
            this.cmboxSearchToEditAdd_CompanyName.Size = new System.Drawing.Size(227, 23);
            this.cmboxSearchToEditAdd_CompanyName.TabIndex = 69;
            // 
            // cmboxSupplier_ProductName
            // 
            this.cmboxSupplier_ProductName.FormattingEnabled = true;
            this.cmboxSupplier_ProductName.Location = new System.Drawing.Point(193, 30);
            this.cmboxSupplier_ProductName.Name = "cmboxSupplier_ProductName";
            this.cmboxSupplier_ProductName.Size = new System.Drawing.Size(227, 23);
            this.cmboxSupplier_ProductName.TabIndex = 68;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(111, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 15);
            this.label4.TabIndex = 67;
            this.label4.Text = "(Supplier)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(18, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 15);
            this.label9.TabIndex = 66;
            this.label9.Text = "Company Name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 15);
            this.label5.TabIndex = 65;
            this.label5.Text = "Product Name :";
            // 
            // btnSearchStockEdit_Search
            // 
            this.btnSearchStockEdit_Search.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchStockEdit_Search.Location = new System.Drawing.Point(278, 253);
            this.btnSearchStockEdit_Search.Name = "btnSearchStockEdit_Search";
            this.btnSearchStockEdit_Search.Size = new System.Drawing.Size(75, 23);
            this.btnSearchStockEdit_Search.TabIndex = 50;
            this.btnSearchStockEdit_Search.Text = "Search";
            this.btnSearchStockEdit_Search.UseVisualStyleBackColor = true;
            this.btnSearchStockEdit_Search.Click += new System.EventHandler(this.btnSearchStockEdit_Search_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(176, 15);
            this.label7.TabIndex = 49;
            this.label7.Text = "Please choose a mode of search...";
            // 
            // FrmSearchStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 289);
            this.Controls.Add(this.pnlSearchStock_Display);
            this.Name = "FrmSearchStock";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmSearchStock";
            this.pnlSearchStock_Display.ResumeLayout(false);
            this.pnlSearchStock_Display.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearchStock_Display;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnSearchStockEdit_Cancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSearchStockEdit_Search;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmboxSearchToEditAdd_CompanyName;
        private System.Windows.Forms.ComboBox cmboxSupplier_ProductName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboxSearchToEdit_ModelNum;
    }
}