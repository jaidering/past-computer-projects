﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_2
{
    public partial class FrmSearchForStockDisplay : Form
    {
        private AllDataClass allData;
        private bool cancel;
        private int numb;
        public FrmSearchForStockDisplay(AllDataClass adc)
        {
            InitializeComponent();
            allData= adc;
            numb = 0;
            //display in combo box
            for (int i = 0; i < allData.stockList.Count; i++)
            {
                if (allData.stockList[i] != null)
                {
                    Stock s = (Stock)allData.stockList[i];
                    cboxSearchStockDisplay_ModelNo.Items.Add(s.ModelNumber);
                    cboxSearchStockDisplay_ProductName.Items.Add(s.ProductName);
                    cboxSearchStockDisplay_SupComName.Items.Add(s.Supplier);
                }
                else
                {
                    MessageBox.Show("There are no stocks in the system. Please add first");
                }
            }
        }

        private void btnSearchStockDispaly_Search_Click(object sender, EventArgs e)
        {
            allData.storeTempStockListForDisplay.Clear();
            if (cboxSearchStockDisplay_ProductName.Text.Trim() != "" && cboxSearchStockDisplay_ModelNo.Text.Trim() != "" && cboxSearchStockDisplay_SupComName.Text.Trim() == "")
            {
                for (int i = 0; i < allData.stockList.Count; i++)
                {
                    if (allData.stockList[i] != null)
                    {
                        Stock s = (Stock)allData.stockList[i];
                        if (cboxSearchStockDisplay_ProductName.Text.Trim() == s.ProductName && cboxSearchStockDisplay_ModelNo.Text.Trim() == s.ModelNumber)
                        {
                            allData.storeTempStockListForDisplay.Add(s);
                            allData.findSearch = true;
                            cancel = true;
                            this.Close();
                        }
                    }
                }
            }
            else if (cboxSearchStockDisplay_ProductName.Text.Trim() == "" && cboxSearchStockDisplay_ModelNo.Text.Trim() == "" && cboxSearchStockDisplay_SupComName.Text.Trim() != "")
            {
                for (int i = 0; i < allData.stockList.Count; i++)
                {
                    if (allData.stockList[i] != null)
                    {
                        Stock s = (Stock)allData.stockList[i];
                        if (cboxSearchStockDisplay_SupComName.Text.Trim() == s.Supplier)
                        {
                            allData.searchTempStock_Display.Add(s);
                            allData.findSearch = true;
                            cancel = true;
                            this.Close();
                        }
                    }
                }
            }
            else if (cboxSearchStockDisplay_ProductName.Text.Trim() != "" && cboxSearchStockDisplay_ModelNo.Text.Trim() != "" && cboxSearchStockDisplay_SupComName.Text.Trim() != "")
            {
                for (int i = 0; i < allData.stockList.Count; i++)
                {
                    if (allData.stockList[i] != null)
                    {
                        Stock s = (Stock)allData.stockList[i];
                        if (cboxSearchStockDisplay_SupComName.Text.Trim() == s.Supplier && cboxSearchStockDisplay_ProductName.Text.Trim() == s.ProductName && cboxSearchStockDisplay_ModelNo.Text.Trim() == s.ModelNumber)
                        {
                            allData.searchTempStock_Display.Add(s);
                            allData.findSearch = false;
                            cancel = true;
                            this.Close();
                        }
                    }
                }
            }
        }


        public bool Cancel
        {
            get
            {
                return cancel;
            }
            set
            {
                cancel = value;
            }
        }

    }
}
