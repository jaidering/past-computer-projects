﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_2
{
    public partial class FrmSearchProduct : Form
    {
        private AllDataClass allData;
        private bool cancel = false;
        private string search;

        public FrmSearchProduct(AllDataClass adc,string search)
        {
            InitializeComponent();
            allData = adc;
            this.search = search;

            if (this.search == "Add Product")
            {
                this.Text = "Add New Product";
            }
            else if (this.search == "Modify Product")
            {
                this.Text = "Modify Existing Product";
            }

            else
            {
                this.Text = "Delete Existing Product";
            }
        }

        public bool Cancel
        {
            get
            {
                return cancel;
            }
            set
            {
                cancel = value;
            }
        }

        private void btnSearchProduct_Click(object sender, EventArgs e)
        {
            if (tbxSearchProductName.Text.Trim() != "" && tbxSearchProductModelNumber.Text.Trim() != "")
            {
                if (tbxSearchProductModelNumber.Text.Length == 8)
                {
                    if (this.search == "Add Product")//Add product
                    {
                        if (searchForExistingProduct() == true)
                        {
                            allData.foundSearch = true;
                            MessageBox.Show("Product already exist! Please try again","Product Already Exist");
                        }
                        else
                        {
                            allData.foundSearch = false;
                            MessageBox.Show("Product not found. Proceed to add product.", "Product Not Found");
                            allData.searchProductName = tbxSearchProductName.Text.Trim();
                            allData.searchProductModelNumber = tbxSearchProductModelNumber.Text.Trim();
                            cancel = true;
                            this.Close();
                        }
                    }

                    else if (this.search == "Modify Product" || this.search == "Delete Product")
                    {
                        if (searchForExistingProduct() == true)
                        {
                            allData.foundSearch = true;
                            MessageBox.Show("Product found. Proceed to modify Product.","Product Found");
                            allData.searchProductName = tbxSearchProductName.Text.Trim();
                            allData.searchProductModelNumber = tbxSearchProductModelNumber.Text.Trim();
                            cancel = true;
                            this.Close();
                        }
                        else
                        {
                            allData.foundSearch = false;
                            MessageBox.Show("Product not found!","Product Not Found");
                        }
                    }
                }

                else
                {
                    MessageBox.Show("Product Model Number must be of 8 characters!","Error In Searching");
                }
            }

            else
            {
                MessageBox.Show("Please key in Product Name and Model Number before searching!","Error In Searching");
            }
        }

        public bool searchForExistingProduct()
        {
            bool checkIfProductExist = false;
            for (int i = 0; i < allData.productList.Count; i++)
            {
                if (allData.productList[i] != null)
                {
                    Product productInArray = (Product)allData.productList[i];
                    if ((productInArray.Name == tbxSearchProductName.Text.Trim() )&& (productInArray.ModelNumber == tbxSearchProductModelNumber.Text.Trim()))
                    {
                        checkIfProductExist = true;
                        allData.storeTempProduct = (Product)allData.productList[i];
                        break;
                    }
                    else
                    {
                        checkIfProductExist = false;
                    }

                }

            }

            return checkIfProductExist;
        }

        private void btnCancelSearchProduct_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
