﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assignment_Phase_2
{
    public class Supplier
    {
        private string companyName;
        private string contactName;
        private string address;
        private string city;
        private string phoneNumber;
        private string faxNumber;
        private string postalNumber;

        public Supplier(string comName, string contactN, string add, string c, string phoneNum, string faxN, string p)
        {
            this.companyName = comName;
            this.contactName = contactN;
            this.address = add;
            this.city = c;
            this.phoneNumber = phoneNum;
            this.faxNumber = faxN;
            this.postalNumber = p;
        }

        public Supplier()
        {

        }

        public string CompanyName
        {
            get { return companyName; }
        }

        public string ContactName
        {
            get { return contactName; }
            set { contactName = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        public string FaxNumber
        {
            get { return faxNumber; }
            set { faxNumber = value; }
        }

        public string PostalCode
        {
            get { return postalNumber; }
            set { postalNumber = value; }
        }
    }
}
