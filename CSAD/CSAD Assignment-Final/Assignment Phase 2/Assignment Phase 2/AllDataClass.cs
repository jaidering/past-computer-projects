﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Collections;

namespace Assignment_Phase_2
{
    public class AllDataClass
    {
        public FrmLogin frmlogin;
        public FrmMainWindowStaff frmMainStaff;
        public FrmSearchStaff frmSearchStaff;
        public FrmSearchProduct frmSearchProduct;
        public FrmSearchOrder frmSearchOrder;
        public FrmSearchCategory frmSearchCategory;
        public FrmSearchSupplier frmSearchSupplier;
        public FrmSearchStock frmSearchStock;
        public ArrayList staffList;
        public ArrayList categoryList;
        public ArrayList productList;
        public ArrayList orderList;
        public ArrayList inventoryList;
        public ArrayList stockList;
        //Weiquan
        public bool foundSearch;
        public string searchCategoryName;
        public string searchProductName;
        public string searchProductModelNumber;
        public Category storeTempCategory;
        public Product storeTempProduct;
        public ArrayList storeTempProductList;
        public ArrayList storeTempProductListForDisplay;

        //Edmund

        public Order storeTempOrder;
        public bool foundSearchEntry;//Rename
        public int orderRef;
        public ArrayList storeTempStaffRole;
        public string searchStaffName;
        public Staff storeTempStaff;
        public ArrayList roleOfStaffLogIn;
        public bool defaultAdmin;

        //Jai
        public bool findSearch;
        public string searchCompanyName;
        public Supplier storeTempSupplier;
        public ArrayList tempSupplierList;
        public ArrayList storeTempSupplierListForDisplay;
        public ArrayList storeTempStockList;
        public ArrayList storeTempStockListForDisplay;
        public ArrayList searchTempStock_Display;
        public Stock storeTempStock;
        public string searchStock_ProductName;
        public string searchStock_ProductQty;
        public string searchStock_CompanyName;
        public string searchStock_ModelNum;
        public FrmSearchForStockDisplay frmSearchStock_Display;
        public string searchStockDisplay_ProductName;
        public string searchStockDisplay_ModelName;
        public string searchStockDisplay_CompName;

        //costructor
        public AllDataClass(FrmLogin _mainWindowForm)
        {
            frmlogin = _mainWindowForm;
            //frmMainStaff = new FrmMainWindowStaff(this);
            //frmSearchStaff = new FrmSearchStaff(this);
            //frmSearchOrder = new FrmSearchOrder(this);
            //frmSearchProduct = new FrmSearchProduct(this);
            //frmSearchCategory = new FrmSearchCategory(this);
            //frmSearchSupplier = new FrmSearchSupplier(this);
            //frmSearchStock = new FrmSearchStock(this);

            staffList = new ArrayList();
            categoryList = new ArrayList();
            productList = new ArrayList();
            orderList = new ArrayList();
            inventoryList = new ArrayList();
            stockList = new ArrayList();
            storeTempProductList = new ArrayList();
            storeTempProductListForDisplay = new ArrayList();
            storeTempStaffRole = new ArrayList();

            //Edmund
            storeTempStaffRole = new ArrayList();
            roleOfStaffLogIn = new ArrayList();
            defaultAdmin = false;
            //Jai
            tempSupplierList = new ArrayList();
            storeTempSupplierListForDisplay = new ArrayList();
            storeTempStockList = new ArrayList();
            storeTempStockListForDisplay = new ArrayList();
        }
    }
}
