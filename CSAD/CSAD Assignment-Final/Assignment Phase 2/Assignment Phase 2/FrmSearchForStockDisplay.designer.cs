﻿namespace Assignment_Phase_2
{
    partial class FrmSearchForStockDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearchForStockDisplay));
            this.label12 = new System.Windows.Forms.Label();
            this.lblSearchToDisplay = new System.Windows.Forms.Label();
            this.picBoxLogoSearchFrm = new System.Windows.Forms.PictureBox();
            this.btnSearchStockDisplay_Cancel = new System.Windows.Forms.Button();
            this.grpBxSearchSupplier = new System.Windows.Forms.GroupBox();
            this.cboxSearchStockDisplay_SupComName = new System.Windows.Forms.ComboBox();
            this.cboxSearchStockDisplay_ModelNo = new System.Windows.Forms.ComboBox();
            this.cboxSearchStockDisplay_ProductName = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblSearchMode = new System.Windows.Forms.Label();
            this.lblSearchStaffName = new System.Windows.Forms.Label();
            this.btnSearchStockDispaly_Search = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoSearchFrm)).BeginInit();
            this.grpBxSearchSupplier.SuspendLayout();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(168, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(217, 23);
            this.label12.TabIndex = 63;
            this.label12.Text = "(Display Product Quantity)";
            // 
            // lblSearchToDisplay
            // 
            this.lblSearchToDisplay.AutoSize = true;
            this.lblSearchToDisplay.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchToDisplay.Location = new System.Drawing.Point(201, 35);
            this.lblSearchToDisplay.Name = "lblSearchToDisplay";
            this.lblSearchToDisplay.Size = new System.Drawing.Size(146, 23);
            this.lblSearchToDisplay.TabIndex = 61;
            this.lblSearchToDisplay.Text = "Search for Stock";
            // 
            // picBoxLogoSearchFrm
            // 
            this.picBoxLogoSearchFrm.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoSearchFrm.Image")));
            this.picBoxLogoSearchFrm.Location = new System.Drawing.Point(24, 10);
            this.picBoxLogoSearchFrm.Name = "picBoxLogoSearchFrm";
            this.picBoxLogoSearchFrm.Size = new System.Drawing.Size(93, 77);
            this.picBoxLogoSearchFrm.TabIndex = 60;
            this.picBoxLogoSearchFrm.TabStop = false;
            // 
            // btnSearchStockDisplay_Cancel
            // 
            this.btnSearchStockDisplay_Cancel.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchStockDisplay_Cancel.Location = new System.Drawing.Point(378, 255);
            this.btnSearchStockDisplay_Cancel.Name = "btnSearchStockDisplay_Cancel";
            this.btnSearchStockDisplay_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btnSearchStockDisplay_Cancel.TabIndex = 59;
            this.btnSearchStockDisplay_Cancel.Text = "Cancel";
            this.btnSearchStockDisplay_Cancel.UseVisualStyleBackColor = true;
            // 
            // grpBxSearchSupplier
            // 
            this.grpBxSearchSupplier.Controls.Add(this.cboxSearchStockDisplay_SupComName);
            this.grpBxSearchSupplier.Controls.Add(this.cboxSearchStockDisplay_ModelNo);
            this.grpBxSearchSupplier.Controls.Add(this.cboxSearchStockDisplay_ProductName);
            this.grpBxSearchSupplier.Controls.Add(this.label10);
            this.grpBxSearchSupplier.Controls.Add(this.label2);
            this.grpBxSearchSupplier.Controls.Add(this.label8);
            this.grpBxSearchSupplier.Controls.Add(this.lblSearchMode);
            this.grpBxSearchSupplier.Controls.Add(this.lblSearchStaffName);
            this.grpBxSearchSupplier.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBxSearchSupplier.Location = new System.Drawing.Point(12, 110);
            this.grpBxSearchSupplier.Name = "grpBxSearchSupplier";
            this.grpBxSearchSupplier.Size = new System.Drawing.Size(441, 139);
            this.grpBxSearchSupplier.TabIndex = 62;
            this.grpBxSearchSupplier.TabStop = false;
            this.grpBxSearchSupplier.Text = "Search for Stock";
            // 
            // cboxSearchStockDisplay_SupComName
            // 
            this.cboxSearchStockDisplay_SupComName.FormattingEnabled = true;
            this.cboxSearchStockDisplay_SupComName.Location = new System.Drawing.Point(160, 105);
            this.cboxSearchStockDisplay_SupComName.Name = "cboxSearchStockDisplay_SupComName";
            this.cboxSearchStockDisplay_SupComName.Size = new System.Drawing.Size(166, 23);
            this.cboxSearchStockDisplay_SupComName.TabIndex = 61;
            // 
            // cboxSearchStockDisplay_ModelNo
            // 
            this.cboxSearchStockDisplay_ModelNo.FormattingEnabled = true;
            this.cboxSearchStockDisplay_ModelNo.Location = new System.Drawing.Point(160, 48);
            this.cboxSearchStockDisplay_ModelNo.Name = "cboxSearchStockDisplay_ModelNo";
            this.cboxSearchStockDisplay_ModelNo.Size = new System.Drawing.Size(166, 23);
            this.cboxSearchStockDisplay_ModelNo.TabIndex = 60;
            // 
            // cboxSearchStockDisplay_ProductName
            // 
            this.cboxSearchStockDisplay_ProductName.FormattingEnabled = true;
            this.cboxSearchStockDisplay_ProductName.Location = new System.Drawing.Point(160, 22);
            this.cboxSearchStockDisplay_ProductName.Name = "cboxSearchStockDisplay_ProductName";
            this.cboxSearchStockDisplay_ProductName.Size = new System.Drawing.Size(166, 23);
            this.cboxSearchStockDisplay_ProductName.TabIndex = 59;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(332, 108);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 15);
            this.label10.TabIndex = 58;
            this.label10.Text = "(Supplier)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "Company Name:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(212, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 15);
            this.label8.TabIndex = 8;
            this.label8.Text = "OR/AND";
            // 
            // lblSearchMode
            // 
            this.lblSearchMode.AutoSize = true;
            this.lblSearchMode.Location = new System.Drawing.Point(66, 25);
            this.lblSearchMode.Name = "lblSearchMode";
            this.lblSearchMode.Size = new System.Drawing.Size(81, 15);
            this.lblSearchMode.TabIndex = 4;
            this.lblSearchMode.Text = "Product Name:";
            // 
            // lblSearchStaffName
            // 
            this.lblSearchStaffName.AutoSize = true;
            this.lblSearchStaffName.Location = new System.Drawing.Point(66, 51);
            this.lblSearchStaffName.Name = "lblSearchStaffName";
            this.lblSearchStaffName.Size = new System.Drawing.Size(59, 15);
            this.lblSearchStaffName.TabIndex = 2;
            this.lblSearchStaffName.Text = "Model No:";
            // 
            // btnSearchStockDispaly_Search
            // 
            this.btnSearchStockDispaly_Search.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchStockDispaly_Search.Location = new System.Drawing.Point(279, 255);
            this.btnSearchStockDispaly_Search.Name = "btnSearchStockDispaly_Search";
            this.btnSearchStockDispaly_Search.Size = new System.Drawing.Size(75, 23);
            this.btnSearchStockDispaly_Search.TabIndex = 58;
            this.btnSearchStockDispaly_Search.Text = "Search";
            this.btnSearchStockDispaly_Search.UseVisualStyleBackColor = true;
            this.btnSearchStockDispaly_Search.Click += new System.EventHandler(this.btnSearchStockDispaly_Search_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 15);
            this.label1.TabIndex = 57;
            this.label1.Text = "Please enter any of the following...";
            // 
            // FrmSearchForStockDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 289);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lblSearchToDisplay);
            this.Controls.Add(this.picBoxLogoSearchFrm);
            this.Controls.Add(this.btnSearchStockDisplay_Cancel);
            this.Controls.Add(this.grpBxSearchSupplier);
            this.Controls.Add(this.btnSearchStockDispaly_Search);
            this.Controls.Add(this.label1);
            this.Name = "FrmSearchForStockDisplay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormSearchForStockDisplay";
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoSearchFrm)).EndInit();
            this.grpBxSearchSupplier.ResumeLayout(false);
            this.grpBxSearchSupplier.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblSearchToDisplay;
        private System.Windows.Forms.PictureBox picBoxLogoSearchFrm;
        private System.Windows.Forms.Button btnSearchStockDisplay_Cancel;
        private System.Windows.Forms.GroupBox grpBxSearchSupplier;
        private System.Windows.Forms.ComboBox cboxSearchStockDisplay_SupComName;
        private System.Windows.Forms.ComboBox cboxSearchStockDisplay_ModelNo;
        private System.Windows.Forms.ComboBox cboxSearchStockDisplay_ProductName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblSearchMode;
        private System.Windows.Forms.Label lblSearchStaffName;
        private System.Windows.Forms.Button btnSearchStockDispaly_Search;
        private System.Windows.Forms.Label label1;
    }
}