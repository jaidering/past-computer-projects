﻿namespace Assignment_Phase_2
{
    partial class FrmPictureBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picbxBigImage = new System.Windows.Forms.PictureBox();
            this.btnClosePicBx = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picbxBigImage)).BeginInit();
            this.SuspendLayout();
            // 
            // picbxBigImage
            // 
            this.picbxBigImage.Location = new System.Drawing.Point(0, 0);
            this.picbxBigImage.Name = "picbxBigImage";
            this.picbxBigImage.Size = new System.Drawing.Size(424, 258);
            this.picbxBigImage.TabIndex = 0;
            this.picbxBigImage.TabStop = false;
            // 
            // btnClosePicBx
            // 
            this.btnClosePicBx.Location = new System.Drawing.Point(337, 264);
            this.btnClosePicBx.Name = "btnClosePicBx";
            this.btnClosePicBx.Size = new System.Drawing.Size(75, 23);
            this.btnClosePicBx.TabIndex = 1;
            this.btnClosePicBx.Text = "OK";
            this.btnClosePicBx.UseVisualStyleBackColor = true;
            this.btnClosePicBx.Click += new System.EventHandler(this.btnClosePicBx_Click);
            // 
            // FrmPictureBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 289);
            this.Controls.Add(this.btnClosePicBx);
            this.Controls.Add(this.picbxBigImage);
            this.Name = "FrmPictureBox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Image - Zoom In ";
            ((System.ComponentModel.ISupportInitialize)(this.picbxBigImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picbxBigImage;
        private System.Windows.Forms.Button btnClosePicBx;
    }
}