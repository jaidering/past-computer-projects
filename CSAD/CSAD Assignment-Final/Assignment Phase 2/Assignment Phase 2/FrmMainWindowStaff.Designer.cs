﻿namespace Assignment_Phase_2
{
    partial class FrmMainWindowStaff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMainWindowStaff));
            this.menuStrpMainWindow = new System.Windows.Forms.MenuStrip();
            this.staffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registerNewStaffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteExistingStaffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchForStaffInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetStaffPasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewCategoryInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyCategoryInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCategoryInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAndDisplayCategoryInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewProductInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyProductInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteExistingProductInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchDisplayProductInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewOrderInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyExistingOrderInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAndDisplayOrderInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.delToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyExistingSupplierInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteExistingSupplierInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAndDisplaySupplierInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editProductQuantityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAndDisplayProductQuantityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.computeExcessShortfallToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlWelcomeScreen = new System.Windows.Forms.Panel();
            this.lblWelcome2 = new System.Windows.Forms.Label();
            this.lblWelcome1 = new System.Windows.Forms.Label();
            this.picBoxLogoMainPnl = new System.Windows.Forms.PictureBox();
            this.lblMainWindowInfo = new System.Windows.Forms.Label();
            this.pnlRegisterStaff = new System.Windows.Forms.Panel();
            this.lblInfoRegisterStaff = new System.Windows.Forms.Label();
            this.lblRegisterStaff = new System.Windows.Forms.Label();
            this.picBoxLogoRegisterStaffPnl = new System.Windows.Forms.PictureBox();
            this.tabCtrRegisterStaffPnl = new System.Windows.Forms.TabControl();
            this.tabStaffPersonalInformation = new System.Windows.Forms.TabPage();
            this.lblProceedToContactInformation = new System.Windows.Forms.Label();
            this.lblFill1 = new System.Windows.Forms.Label();
            this.lblMandatoryFields1 = new System.Windows.Forms.Label();
            this.grpRegisterPersonalParticulars = new System.Windows.Forms.GroupBox();
            this.lblRegisterStaffName = new System.Windows.Forms.Label();
            this.rdBtnFemaleRegister = new System.Windows.Forms.RadioButton();
            this.rdBtnMaleRegister = new System.Windows.Forms.RadioButton();
            this.dtpRegisterDateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.lblRegisterStaffGender = new System.Windows.Forms.Label();
            this.lblRegisterDateOfBirth = new System.Windows.Forms.Label();
            this.tbxRegisterNRIC = new System.Windows.Forms.TextBox();
            this.lblRegisterNRIC = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.tabStaffContactInfo = new System.Windows.Forms.TabPage();
            this.lblProceedToAccountInfo = new System.Windows.Forms.Label();
            this.grpRegisterContactDetails = new System.Windows.Forms.GroupBox();
            this.lblRegisterHpContact = new System.Windows.Forms.Label();
            this.lblRegisterHomeContact = new System.Windows.Forms.Label();
            this.lblRegisterContactNo = new System.Windows.Forms.Label();
            this.tbxRegisterHpNoContact = new System.Windows.Forms.TextBox();
            this.tbxRegisterHomeNo = new System.Windows.Forms.TextBox();
            this.tbxRegisterAddress = new System.Windows.Forms.TextBox();
            this.lblRegisterAddress = new System.Windows.Forms.Label();
            this.lblFill2 = new System.Windows.Forms.Label();
            this.lblMandatoryFields2 = new System.Windows.Forms.Label();
            this.tabStaffAccountInformation = new System.Windows.Forms.TabPage();
            this.btnCancelRegisterStaff = new System.Windows.Forms.Button();
            this.lblMandatoryFields3 = new System.Windows.Forms.Label();
            this.btnRegisterStaff = new System.Windows.Forms.Button();
            this.grpRegisterAccntDetails = new System.Windows.Forms.GroupBox();
            this.chkBoxInventoryManager = new System.Windows.Forms.CheckBox();
            this.chkBoxOrderManager = new System.Windows.Forms.CheckBox();
            this.chkBoxCatalogueManager = new System.Windows.Forms.CheckBox();
            this.chkBoxAdministrator = new System.Windows.Forms.CheckBox();
            this.tbxConfirmRegisterPassword = new System.Windows.Forms.TextBox();
            this.lblCfmRegisteredPassword = new System.Windows.Forms.Label();
            this.tbxRegisterPassword = new System.Windows.Forms.TextBox();
            this.lblRegisterPassword = new System.Windows.Forms.Label();
            this.lblRegisterStaffRole = new System.Windows.Forms.Label();
            this.pnlResetPassword = new System.Windows.Forms.Panel();
            this.lblDispDOBStaffResetRw = new System.Windows.Forms.Label();
            this.lblDispNRICResetPw = new System.Windows.Forms.Label();
            this.lblDispStaffNameResetPw = new System.Windows.Forms.Label();
            this.lblCancelResetPw = new System.Windows.Forms.Button();
            this.btnResetPw = new System.Windows.Forms.Button();
            this.grpResetPW = new System.Windows.Forms.GroupBox();
            this.tbxCfmResetPw = new System.Windows.Forms.TextBox();
            this.tbxResetPw = new System.Windows.Forms.TextBox();
            this.lblCfmNewPw = new System.Windows.Forms.Label();
            this.lblResetPW = new System.Windows.Forms.Label();
            this.lblDOBStaffResetPW = new System.Windows.Forms.Label();
            this.lblNRICStaffResetPW = new System.Windows.Forms.Label();
            this.lblNameOfStaffResetPW = new System.Windows.Forms.Label();
            this.lblResetPassword = new System.Windows.Forms.Label();
            this.picBxLogoPnlResetPassword = new System.Windows.Forms.PictureBox();
            this.pnlAddOrderInformation = new System.Windows.Forms.Panel();
            this.tabCtrOrderInfo = new System.Windows.Forms.TabControl();
            this.tabPgAddOrderCustInfo = new System.Windows.Forms.TabPage();
            this.lblProceedToOrderInfo = new System.Windows.Forms.Label();
            this.grpBoxAddOrderInfo = new System.Windows.Forms.GroupBox();
            this.tbxAddOrderCustDestinationAddress = new System.Windows.Forms.TextBox();
            this.lblAddOrderDestinationAddress = new System.Windows.Forms.Label();
            this.tbxAddOrderNameOfCust = new System.Windows.Forms.TextBox();
            this.lblAddOrderNameOfCust = new System.Windows.Forms.Label();
            this.lblMandatoryFields5 = new System.Windows.Forms.Label();
            this.tabPgAddOrderInfo = new System.Windows.Forms.TabPage();
            this.btnCancelAddOrderInfo = new System.Windows.Forms.Button();
            this.btnAddOrderInfo = new System.Windows.Forms.Button();
            this.grpAddOrderInfo = new System.Windows.Forms.GroupBox();
            this.dtpDateOfAddOrder = new System.Windows.Forms.DateTimePicker();
            this.tbxAddOrderTotalPrice = new System.Windows.Forms.TextBox();
            this.lblAddOrderTotalPrice = new System.Windows.Forms.Label();
            this.lblAddDateOfOrder = new System.Windows.Forms.Label();
            this.tbxAddOrderRefNum = new System.Windows.Forms.TextBox();
            this.lblAddOrderRefNum = new System.Windows.Forms.Label();
            this.lblMandatoryFields6 = new System.Windows.Forms.Label();
            this.lblAddOrderInfo = new System.Windows.Forms.Label();
            this.lblAddOrder = new System.Windows.Forms.Label();
            this.picBxLogoAddOrderPnl = new System.Windows.Forms.PictureBox();
            this.pnlModifyOrderInfo = new System.Windows.Forms.Panel();
            this.lblModifyOrderInfo = new System.Windows.Forms.Label();
            this.btnCancelModifyOrderInformation = new System.Windows.Forms.Button();
            this.btnModifyOrderInformation = new System.Windows.Forms.Button();
            this.lblModifyDisplayTotalPrice = new System.Windows.Forms.Label();
            this.lblModifyDisplayDateOfOrder = new System.Windows.Forms.Label();
            this.grpModifyOrderInfo = new System.Windows.Forms.GroupBox();
            this.dtpModifyOrderDate = new System.Windows.Forms.DateTimePicker();
            this.tbxModifyOrderTotalPrice = new System.Windows.Forms.TextBox();
            this.lblModifyOrderTotalPrice = new System.Windows.Forms.Label();
            this.lblModifyOrderDate = new System.Windows.Forms.Label();
            this.lblModifyDestinationAddress = new System.Windows.Forms.Label();
            this.tbxModifyDestinationAddress = new System.Windows.Forms.TextBox();
            this.lblModifyDispDestinationAddress = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblModifyDispCustomerName = new System.Windows.Forms.Label();
            this.lblModifyDispOrderRefNum = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblModifyOrderInformation = new System.Windows.Forms.Label();
            this.picBxLogoPnlModifyOrderInfo = new System.Windows.Forms.PictureBox();
            this.panelAddNewCategory = new System.Windows.Forms.Panel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancelAddCategory = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblAddCategoryName = new System.Windows.Forms.Label();
            this.tbxAddCategoryDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddCategory = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.panelModifyCategory = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.btnCancelModifyCategory = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblModifyCategoryName = new System.Windows.Forms.Label();
            this.tbxModifyCategoryDescrption = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnModifyCategory = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelDeleteCategory = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.btnCancelDeleteCategory = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblDeleteCategoryName = new System.Windows.Forms.Label();
            this.tbxDeleteCategoryDescrption = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnDeleteCategory = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panelDisplayCategory = new System.Windows.Forms.Panel();
            this.btnDisplayCategory = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblDisplayCategoryName = new System.Windows.Forms.Label();
            this.tbxDisplayCategoryDescrption = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panelAddNewProduct = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.tabControlAddNewProduct = new System.Windows.Forms.TabControl();
            this.tabAddNewProductInfo1 = new System.Windows.Forms.TabPage();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblAddProductModelNumber = new System.Windows.Forms.Label();
            this.cbxAddProductCategory = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tbxAddProductDescription = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.lblAddProductName = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.tabAddNewProductInfo2 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnAddProductEnlargeImage2 = new System.Windows.Forms.Button();
            this.btnAddProductEnlargeImage1 = new System.Windows.Forms.Button();
            this.btnAddProductRemoveImage2 = new System.Windows.Forms.Button();
            this.btnAddProductOpenImage2 = new System.Windows.Forms.Button();
            this.btnAddProductRemoveImage1 = new System.Windows.Forms.Button();
            this.btnAddProductOpenImage1 = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.picbxAddProductImage2 = new System.Windows.Forms.PictureBox();
            this.picbxAddProductImage1 = new System.Windows.Forms.PictureBox();
            this.tbxAddProductRetailPrice = new System.Windows.Forms.TextBox();
            this.tbxAddProductMSRP = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.btnAddProduct = new System.Windows.Forms.Button();
            this.btnCancelAddProduct = new System.Windows.Forms.Button();
            this.label65 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panelModifyProduct = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.tabControlModifyProduct = new System.Windows.Forms.TabControl();
            this.tabModifyProductInfo1 = new System.Windows.Forms.TabPage();
            this.label35 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lblModifyProductModelNumber = new System.Windows.Forms.Label();
            this.cbxModifyProductCategory = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tbxModifyProductDescription = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.lblModifyProductName = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.tabModifyProductInfo2 = new System.Windows.Forms.TabPage();
            this.label42 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btnModifyProductEnlargeImage2 = new System.Windows.Forms.Button();
            this.btnModifyProductEnlargeImage1 = new System.Windows.Forms.Button();
            this.btnModifyProductRemoveImage2 = new System.Windows.Forms.Button();
            this.btnModifyProductOpenImage2 = new System.Windows.Forms.Button();
            this.btnModifyProductRemoveImage1 = new System.Windows.Forms.Button();
            this.btnModifyProductOpenImage1 = new System.Windows.Forms.Button();
            this.label48 = new System.Windows.Forms.Label();
            this.picbxModifyProductImage2 = new System.Windows.Forms.PictureBox();
            this.picbxModifyProductImage1 = new System.Windows.Forms.PictureBox();
            this.tbxModifyProductRetailPrice = new System.Windows.Forms.TextBox();
            this.tbxModifyProductMSRP = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.btnModifyProduct = new System.Windows.Forms.Button();
            this.btnCancelModifyProduct = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panelDeleteProduct = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.btnDeleteProduct = new System.Windows.Forms.Button();
            this.btnCancelDeleteProduct = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.tbxDeleteProductDisplayDescription = new System.Windows.Forms.TextBox();
            this.label132 = new System.Windows.Forms.Label();
            this.lblDeleteProductDisplayCategory = new System.Windows.Forms.Label();
            this.lblDeleteProductDisplayMSRP = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.lblDeleteProductDisplayRetailPrice = new System.Windows.Forms.Label();
            this.lblDeleteProductDisplayModelNumber = new System.Windows.Forms.Label();
            this.lblDeleteProductDisplayName = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.btnDeleteProductEnlargeImage2 = new System.Windows.Forms.Button();
            this.btnDeleteProductEnlargeImage1 = new System.Windows.Forms.Button();
            this.picbxDeleteProductImage2 = new System.Windows.Forms.PictureBox();
            this.picbxDeleteProductImage1 = new System.Windows.Forms.PictureBox();
            this.label52 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.panelDisplayProduct = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.btnDisplayProduct = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbxDisplayProductDisplayDescription = new System.Windows.Forms.TextBox();
            this.label126 = new System.Windows.Forms.Label();
            this.lblDisplayProductDisplayCategory = new System.Windows.Forms.Label();
            this.lblDisplayProductDisplayMSRP = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.lblDisplayProductDisplayRetailPRice = new System.Windows.Forms.Label();
            this.lblDisplayProductDisplayModelNumber = new System.Windows.Forms.Label();
            this.lblDisplayProductDisplayName = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.btnDisplayProductEnlargeImage2 = new System.Windows.Forms.Button();
            this.btnDisplayProductEnlargeImage1 = new System.Windows.Forms.Button();
            this.picbxDisplayProductImage2 = new System.Windows.Forms.PictureBox();
            this.picbxDisplayProductImage1 = new System.Windows.Forms.PictureBox();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlAddStock = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tbxAddStock_CompanyName = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.tbxAddStock_StockQty = new System.Windows.Forms.TextBox();
            this.tbxAddStock_ModelName = new System.Windows.Forms.TextBox();
            this.tbxAddStock_ProductName = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnAddStock_Cancel = new System.Windows.Forms.Button();
            this.btnAddStock_Add = new System.Windows.Forms.Button();
            this.pnlEditStockInfo = new System.Windows.Forms.Panel();
            this.grpEditStockInfo = new System.Windows.Forms.GroupBox();
            this.lblEditStock_DisplayCompanyName = new System.Windows.Forms.Label();
            this.lblEditStock_DisplayModelName = new System.Windows.Forms.Label();
            this.lblEditStock_DisplayProductName = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.tbxEditStock_StockQty = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.lblInfoUpdateStock = new System.Windows.Forms.Label();
            this.lblUpdateStock = new System.Windows.Forms.Label();
            this.picBoxLogoUpdateStock = new System.Windows.Forms.PictureBox();
            this.btnCancelUpdateStock = new System.Windows.Forms.Button();
            this.btnUpdateStock = new System.Windows.Forms.Button();
            this.pnlDisplayComputeExcessSHrtfall = new System.Windows.Forms.Panel();
            this.lblDeleteExistingSupplierInfo = new System.Windows.Forms.Label();
            this.picBxLogoDeleteSupplierPnl = new System.Windows.Forms.PictureBox();
            this.btnDispComputeExcessSHort_OK = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label79 = new System.Windows.Forms.Label();
            this.lblProModelName_ComputeExcessShortFallDisplay = new System.Windows.Forms.Label();
            this.lblTotalOrder_ComputeExcessShortFallDisplay = new System.Windows.Forms.Label();
            this.lblComputeExcessShortFallDisplay = new System.Windows.Forms.Label();
            this.lblTotalQty_ComputeExcessShortFallDisplay = new System.Windows.Forms.Label();
            this.lblProName_ComputeExcessShortFallDisplay = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.pnlComputeExcessShortFall = new System.Windows.Forms.Panel();
            this.label80 = new System.Windows.Forms.Label();
            this.btnCmputeExcessShortOK = new System.Windows.Forms.Button();
            this.grpBoxlIstOfStocks = new System.Windows.Forms.GroupBox();
            this.listBxListOfStock = new System.Windows.Forms.ListBox();
            this.lblComputeExcessSHortFall = new System.Windows.Forms.Label();
            this.picBoxLogoPnlComputeExcessShrt = new System.Windows.Forms.PictureBox();
            this.pnlSearchAndDisplayStock = new System.Windows.Forms.Panel();
            this.btnDispStockInfoOK = new System.Windows.Forms.Button();
            this.grpBoxDispStockInfo = new System.Windows.Forms.GroupBox();
            this.listBxDispStockInfo = new System.Windows.Forms.ListBox();
            this.lblDispExistingStock = new System.Windows.Forms.Label();
            this.picBoxLogoPnlDispStockInfo = new System.Windows.Forms.PictureBox();
            this.pnlAddNewSupplierInfo = new System.Windows.Forms.Panel();
            this.lblInfoRegisterSupplier = new System.Windows.Forms.Label();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPgAddSupplier_SupplierInfo = new System.Windows.Forms.TabPage();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.tbxRegisterCompanyName = new System.Windows.Forms.TextBox();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.tbxRegisterSupplierName = new System.Windows.Forms.TextBox();
            this.lblRegisterSupplierName = new System.Windows.Forms.Label();
            this.tabPgAddSupplier_ContactInfo = new System.Windows.Forms.TabPage();
            this.label83 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.tbxRegisterCityCountry = new System.Windows.Forms.TextBox();
            this.lblRegisterCityCountry = new System.Windows.Forms.Label();
            this.lblRegisterFaxContact = new System.Windows.Forms.Label();
            this.lblRegisterOfficeNum = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.tbxRegisterFaxNum = new System.Windows.Forms.TextBox();
            this.tbxRegisterPhoneOfficeNo = new System.Windows.Forms.TextBox();
            this.tbxRegisterPostalCode = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.tbxRegisterAddress_Supplier = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.btnCancelRegisterSupplier = new System.Windows.Forms.Button();
            this.btnRegisterSupplier = new System.Windows.Forms.Button();
            this.lblRegisterSupplier = new System.Windows.Forms.Label();
            this.picBoxLogoRegisterSupplier = new System.Windows.Forms.PictureBox();
            this.pnlModifySupplierInfo = new System.Windows.Forms.Panel();
            this.tabControl5 = new System.Windows.Forms.TabControl();
            this.tabPgModifySupplier_SupplierInfo = new System.Windows.Forms.TabPage();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.lblModifySupplier_DispCompName = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.tbxModifySupplier_Name = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.tabPgModifySupplier_ContactInfo = new System.Windows.Forms.TabPage();
            this.btnMdifySupplier_Cancel = new System.Windows.Forms.Button();
            this.btnModifySupplier_Update = new System.Windows.Forms.Button();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.tbxModifySupplier_CityCountry = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.tbxModifySupplier_Fax = new System.Windows.Forms.TextBox();
            this.tbxModifySupplier_Phone = new System.Windows.Forms.TextBox();
            this.tbxModifySupplier_PostalCode = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.tbxModifySupplier_Address = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.lblModifySupplierInfo = new System.Windows.Forms.Label();
            this.lblModifySupplierInformation = new System.Windows.Forms.Label();
            this.picBoxLogoModifySupplierInfoPnl = new System.Windows.Forms.PictureBox();
            this.pnlDeleteSupplierInfo = new System.Windows.Forms.Panel();
            this.tabControl6 = new System.Windows.Forms.TabControl();
            this.tabPgDeleteSupplier_SupplierInfo = new System.Windows.Forms.TabPage();
            this.label102 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.lblDeleteSupplier_CompanyName = new System.Windows.Forms.Label();
            this.lblDeleteSupplier_SupplierName = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.tabPgDeleteSupplier_ContactInfo = new System.Windows.Forms.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.lblDeleteSupplier_PostalCode = new System.Windows.Forms.Label();
            this.lblDeleteSupplier_CityCountry = new System.Windows.Forms.Label();
            this.lblDeleteSupplier_FaxNum = new System.Windows.Forms.Label();
            this.lblDeleteSupplier_PhoneNum = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.tbxDeleteSupplier_Address = new System.Windows.Forms.TextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.btnDeleteSupplier = new System.Windows.Forms.Button();
            this.lblCfmDeleteSupplier = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label111 = new System.Windows.Forms.Label();
            this.pnlDisplaySupplierInformation_Details = new System.Windows.Forms.Panel();
            this.tabControl7 = new System.Windows.Forms.TabControl();
            this.tabPgDisplaySupplier_SupplierInfo = new System.Windows.Forms.TabPage();
            this.label112 = new System.Windows.Forms.Label();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.lblDisplaySupplierInformation_Display_CompanyName = new System.Windows.Forms.Label();
            this.lblDisplaySupplierInformation_Display_SupplierName = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.tabPgDisplaySupplier_ContactInfo = new System.Windows.Forms.TabPage();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.lblDisplaySupplierInformation_Display_PostalCode = new System.Windows.Forms.Label();
            this.lblDisplaySupplierInformation_Display_CityCountry = new System.Windows.Forms.Label();
            this.lblDisplaySupplierInformation_Display_FaxNum = new System.Windows.Forms.Label();
            this.lblDisplaySupplierInformation_Display_PhoneNum = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.tbxDisplaySupplierInformation_Display_Address = new System.Windows.Forms.TextBox();
            this.label120 = new System.Windows.Forms.Label();
            this.btnDisplaySupplierInformation_Details_OK = new System.Windows.Forms.Button();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.label121 = new System.Windows.Forms.Label();
            this.pnlDisplaySupplierInfo_ListOfCompanies = new System.Windows.Forms.Panel();
            this.label122 = new System.Windows.Forms.Label();
            this.btnDispSupplierInfoOK = new System.Windows.Forms.Button();
            this.grpBoxDispSupplier = new System.Windows.Forms.GroupBox();
            this.listBxDispSupplierInfo = new System.Windows.Forms.ListBox();
            this.lblDispExistingSupplier = new System.Windows.Forms.Label();
            this.picBoxLogoPnlDispSupplierInfo = new System.Windows.Forms.PictureBox();
            this.panelSelectAProductToDisplay = new System.Windows.Forms.Panel();
            this.btnSelectAProductToDisplay = new System.Windows.Forms.Button();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.btnCancelSelectAProductToDisplay = new System.Windows.Forms.Button();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.listbxSelectAProductToDisplay = new System.Windows.Forms.ListBox();
            this.label145 = new System.Windows.Forms.Label();
            this.pnlModifyStaffInfo = new System.Windows.Forms.Panel();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.lblModifyStaffGender = new System.Windows.Forms.Label();
            this.lblModifyStaffDOB = new System.Windows.Forms.Label();
            this.lblModifyStaffNRIC = new System.Windows.Forms.Label();
            this.lblModifyStaffName = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label56 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.tbxModifyHpNoContact = new System.Windows.Forms.TextBox();
            this.tbxModifyHomeNo = new System.Windows.Forms.TextBox();
            this.tbxModifyAddress = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.btnModifyStaffCancel = new System.Windows.Forms.Button();
            this.label64 = new System.Windows.Forms.Label();
            this.btnModifyStaffInformation = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.chkBoxModifyInventoryManager = new System.Windows.Forms.CheckBox();
            this.chkBoxModifyOrderManager = new System.Windows.Forms.CheckBox();
            this.chkBoxModifyCatalogueManager = new System.Windows.Forms.CheckBox();
            this.chkBoxModifyAdministrator = new System.Windows.Forms.CheckBox();
            this.label68 = new System.Windows.Forms.Label();
            this.lblFill6 = new System.Windows.Forms.Label();
            this.lblModifyStaffInfo = new System.Windows.Forms.Label();
            this.lblModifyStaffInformation = new System.Windows.Forms.Label();
            this.picBoxLogoModifyStaffInfoPnl = new System.Windows.Forms.PictureBox();
            this.pnlDeleteStaffInfo = new System.Windows.Forms.Panel();
            this.lblCfmDeleteStaff = new System.Windows.Forms.Label();
            this.btnCancelDelete = new System.Windows.Forms.Button();
            this.btnDeleteStaff = new System.Windows.Forms.Button();
            this.gpBoxDisplayStaffInfo = new System.Windows.Forms.GroupBox();
            this.lblDispDelStaffAddress = new System.Windows.Forms.Label();
            this.lblDispDelStaffRole = new System.Windows.Forms.Label();
            this.lblDispDelStaffMobile = new System.Windows.Forms.Label();
            this.lblDispDelStaffCintact = new System.Windows.Forms.Label();
            this.lblDispDelStaffGender = new System.Windows.Forms.Label();
            this.lblDispDelStaffDOB = new System.Windows.Forms.Label();
            this.lblDispDelStaffNRIC = new System.Windows.Forms.Label();
            this.lblDispDelStaffName = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.lblDeleteExistingStaffInfo = new System.Windows.Forms.Label();
            this.picBxLogoDeleteStaffPnl = new System.Windows.Forms.PictureBox();
            this.pnlDisplayStaffInfo = new System.Windows.Forms.Panel();
            this.btnDispStaffInfoOK = new System.Windows.Forms.Button();
            this.grpBoxDispStaff = new System.Windows.Forms.GroupBox();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.picBoxLogoPnlDispStaffInfo = new System.Windows.Forms.PictureBox();
            this.lblDispExistingStaff = new System.Windows.Forms.Label();
            this.pnlDeleteOrderInfo = new System.Windows.Forms.Panel();
            this.lblDeleteOrderInfo = new System.Windows.Forms.Label();
            this.btnCancelDeleteOrderInfo = new System.Windows.Forms.Button();
            this.btnDeleteOrderInfo = new System.Windows.Forms.Button();
            this.grpDeleteDispOrderInfo = new System.Windows.Forms.GroupBox();
            this.lblDispDelTotalPrice = new System.Windows.Forms.Label();
            this.lblDispDelOrderDate = new System.Windows.Forms.Label();
            this.lblDispDelOrderDestAdd = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.lblDispDelCustName = new System.Windows.Forms.Label();
            this.lblDispDelORN = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.lblDeleteOrderInformation = new System.Windows.Forms.Label();
            this.picBoxDeleteOrderInformation = new System.Windows.Forms.PictureBox();
            this.pnlSearchDispOrderInfo = new System.Windows.Forms.Panel();
            this.btnOKSearchDispOrderInfo = new System.Windows.Forms.Button();
            this.grpSearchDispOrderInfo = new System.Windows.Forms.GroupBox();
            this.label165 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.lblSearchDispOrderInfo = new System.Windows.Forms.Label();
            this.picBxLogoPnlSearchDispOrderInfo = new System.Windows.Forms.PictureBox();
            this.menuStrpMainWindow.SuspendLayout();
            this.pnlWelcomeScreen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoMainPnl)).BeginInit();
            this.pnlRegisterStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoRegisterStaffPnl)).BeginInit();
            this.tabCtrRegisterStaffPnl.SuspendLayout();
            this.tabStaffPersonalInformation.SuspendLayout();
            this.grpRegisterPersonalParticulars.SuspendLayout();
            this.tabStaffContactInfo.SuspendLayout();
            this.grpRegisterContactDetails.SuspendLayout();
            this.tabStaffAccountInformation.SuspendLayout();
            this.grpRegisterAccntDetails.SuspendLayout();
            this.pnlResetPassword.SuspendLayout();
            this.grpResetPW.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoPnlResetPassword)).BeginInit();
            this.pnlAddOrderInformation.SuspendLayout();
            this.tabCtrOrderInfo.SuspendLayout();
            this.tabPgAddOrderCustInfo.SuspendLayout();
            this.grpBoxAddOrderInfo.SuspendLayout();
            this.tabPgAddOrderInfo.SuspendLayout();
            this.grpAddOrderInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoAddOrderPnl)).BeginInit();
            this.pnlModifyOrderInfo.SuspendLayout();
            this.grpModifyOrderInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoPnlModifyOrderInfo)).BeginInit();
            this.panelAddNewCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panelModifyCategory.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelDeleteCategory.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panelDisplayCategory.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panelAddNewProduct.SuspendLayout();
            this.tabControlAddNewProduct.SuspendLayout();
            this.tabAddNewProductInfo1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabAddNewProductInfo2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbxAddProductImage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbxAddProductImage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panelModifyProduct.SuspendLayout();
            this.tabControlModifyProduct.SuspendLayout();
            this.tabModifyProductInfo1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabModifyProductInfo2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbxModifyProductImage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbxModifyProductImage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panelDeleteProduct.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbxDeleteProductImage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbxDeleteProductImage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.panelDisplayProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbxDisplayProductImage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbxDisplayProductImage1)).BeginInit();
            this.pnlAddStock.SuspendLayout();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.pnlEditStockInfo.SuspendLayout();
            this.grpEditStockInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoUpdateStock)).BeginInit();
            this.pnlDisplayComputeExcessSHrtfall.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoDeleteSupplierPnl)).BeginInit();
            this.groupBox15.SuspendLayout();
            this.pnlComputeExcessShortFall.SuspendLayout();
            this.grpBoxlIstOfStocks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoPnlComputeExcessShrt)).BeginInit();
            this.pnlSearchAndDisplayStock.SuspendLayout();
            this.grpBoxDispStockInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoPnlDispStockInfo)).BeginInit();
            this.pnlAddNewSupplierInfo.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPgAddSupplier_SupplierInfo.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.tabPgAddSupplier_ContactInfo.SuspendLayout();
            this.groupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoRegisterSupplier)).BeginInit();
            this.pnlModifySupplierInfo.SuspendLayout();
            this.tabControl5.SuspendLayout();
            this.tabPgModifySupplier_SupplierInfo.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.tabPgModifySupplier_ContactInfo.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoModifySupplierInfoPnl)).BeginInit();
            this.pnlDeleteSupplierInfo.SuspendLayout();
            this.tabControl6.SuspendLayout();
            this.tabPgDeleteSupplier_SupplierInfo.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.tabPgDeleteSupplier_ContactInfo.SuspendLayout();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.pnlDisplaySupplierInformation_Details.SuspendLayout();
            this.tabControl7.SuspendLayout();
            this.tabPgDisplaySupplier_SupplierInfo.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.tabPgDisplaySupplier_ContactInfo.SuspendLayout();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.pnlDisplaySupplierInfo_ListOfCompanies.SuspendLayout();
            this.grpBoxDispSupplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoPnlDispSupplierInfo)).BeginInit();
            this.panelSelectAProductToDisplay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.groupBox24.SuspendLayout();
            this.pnlModifyStaffInfo.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoModifyStaffInfoPnl)).BeginInit();
            this.pnlDeleteStaffInfo.SuspendLayout();
            this.gpBoxDisplayStaffInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoDeleteStaffPnl)).BeginInit();
            this.pnlDisplayStaffInfo.SuspendLayout();
            this.grpBoxDispStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoPnlDispStaffInfo)).BeginInit();
            this.pnlDeleteOrderInfo.SuspendLayout();
            this.grpDeleteDispOrderInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxDeleteOrderInformation)).BeginInit();
            this.pnlSearchDispOrderInfo.SuspendLayout();
            this.grpSearchDispOrderInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoPnlSearchDispOrderInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrpMainWindow
            // 
            this.menuStrpMainWindow.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.staffToolStripMenuItem,
            this.categoryToolStripMenuItem,
            this.productToolStripMenuItem,
            this.orderToolStripMenuItem,
            this.inventoryToolStripMenuItem,
            this.stockToolStripMenuItem});
            this.menuStrpMainWindow.Location = new System.Drawing.Point(0, 0);
            this.menuStrpMainWindow.Name = "menuStrpMainWindow";
            this.menuStrpMainWindow.Size = new System.Drawing.Size(554, 24);
            this.menuStrpMainWindow.TabIndex = 0;
            this.menuStrpMainWindow.Text = "menuStrip1";
            // 
            // staffToolStripMenuItem
            // 
            this.staffToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logoutToolStripMenuItem,
            this.registerNewStaffToolStripMenuItem,
            this.modifyToolStripMenuItem,
            this.deleteExistingStaffToolStripMenuItem,
            this.searchForStaffInformationToolStripMenuItem,
            this.resetStaffPasswordToolStripMenuItem});
            this.staffToolStripMenuItem.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staffToolStripMenuItem.Name = "staffToolStripMenuItem";
            this.staffToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.staffToolStripMenuItem.Text = "Staff";
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // registerNewStaffToolStripMenuItem
            // 
            this.registerNewStaffToolStripMenuItem.Name = "registerNewStaffToolStripMenuItem";
            this.registerNewStaffToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.registerNewStaffToolStripMenuItem.Text = "Register New Staff";
            this.registerNewStaffToolStripMenuItem.Click += new System.EventHandler(this.registerNewStaffToolStripMenuItem_Click);
            // 
            // modifyToolStripMenuItem
            // 
            this.modifyToolStripMenuItem.Name = "modifyToolStripMenuItem";
            this.modifyToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.modifyToolStripMenuItem.Text = "Modify Existing Staff Information";
            this.modifyToolStripMenuItem.Click += new System.EventHandler(this.modifyToolStripMenuItem_Click);
            // 
            // deleteExistingStaffToolStripMenuItem
            // 
            this.deleteExistingStaffToolStripMenuItem.Name = "deleteExistingStaffToolStripMenuItem";
            this.deleteExistingStaffToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.deleteExistingStaffToolStripMenuItem.Text = "Delete Existing Staff Information";
            this.deleteExistingStaffToolStripMenuItem.Click += new System.EventHandler(this.deleteExistingStaffToolStripMenuItem_Click);
            // 
            // searchForStaffInformationToolStripMenuItem
            // 
            this.searchForStaffInformationToolStripMenuItem.Name = "searchForStaffInformationToolStripMenuItem";
            this.searchForStaffInformationToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.searchForStaffInformationToolStripMenuItem.Text = "Search && Display Staff Information";
            this.searchForStaffInformationToolStripMenuItem.Click += new System.EventHandler(this.searchForStaffInformationToolStripMenuItem_Click);
            // 
            // resetStaffPasswordToolStripMenuItem
            // 
            this.resetStaffPasswordToolStripMenuItem.Name = "resetStaffPasswordToolStripMenuItem";
            this.resetStaffPasswordToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.resetStaffPasswordToolStripMenuItem.Text = "Reset Staff Password";
            // 
            // categoryToolStripMenuItem
            // 
            this.categoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewCategoryInformationToolStripMenuItem,
            this.modifyCategoryInformationToolStripMenuItem,
            this.deleteCategoryInformationToolStripMenuItem,
            this.searchAndDisplayCategoryInformationToolStripMenuItem});
            this.categoryToolStripMenuItem.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.categoryToolStripMenuItem.Name = "categoryToolStripMenuItem";
            this.categoryToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.categoryToolStripMenuItem.Text = "Category";
            // 
            // addNewCategoryInformationToolStripMenuItem
            // 
            this.addNewCategoryInformationToolStripMenuItem.Name = "addNewCategoryInformationToolStripMenuItem";
            this.addNewCategoryInformationToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.addNewCategoryInformationToolStripMenuItem.Text = "Add New Category Information";
            this.addNewCategoryInformationToolStripMenuItem.Click += new System.EventHandler(this.addNewCategoryInformationToolStripMenuItem_Click);
            // 
            // modifyCategoryInformationToolStripMenuItem
            // 
            this.modifyCategoryInformationToolStripMenuItem.Name = "modifyCategoryInformationToolStripMenuItem";
            this.modifyCategoryInformationToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.modifyCategoryInformationToolStripMenuItem.Text = "Modify Existing Category Information";
            this.modifyCategoryInformationToolStripMenuItem.Click += new System.EventHandler(this.modifyCategoryInformationToolStripMenuItem_Click);
            // 
            // deleteCategoryInformationToolStripMenuItem
            // 
            this.deleteCategoryInformationToolStripMenuItem.Name = "deleteCategoryInformationToolStripMenuItem";
            this.deleteCategoryInformationToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.deleteCategoryInformationToolStripMenuItem.Text = "Delete Existing Category Information";
            this.deleteCategoryInformationToolStripMenuItem.Click += new System.EventHandler(this.deleteCategoryInformationToolStripMenuItem_Click);
            // 
            // searchAndDisplayCategoryInformationToolStripMenuItem
            // 
            this.searchAndDisplayCategoryInformationToolStripMenuItem.Name = "searchAndDisplayCategoryInformationToolStripMenuItem";
            this.searchAndDisplayCategoryInformationToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.searchAndDisplayCategoryInformationToolStripMenuItem.Text = "Search && Display Category Information";
            this.searchAndDisplayCategoryInformationToolStripMenuItem.Click += new System.EventHandler(this.searchAndDisplayCategoryInformationToolStripMenuItem_Click);
            // 
            // productToolStripMenuItem
            // 
            this.productToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewProductInformationToolStripMenuItem,
            this.modifyProductInformationToolStripMenuItem,
            this.deleteExistingProductInformationToolStripMenuItem,
            this.searchDisplayProductInformationToolStripMenuItem});
            this.productToolStripMenuItem.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productToolStripMenuItem.Name = "productToolStripMenuItem";
            this.productToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.productToolStripMenuItem.Text = "Product";
            // 
            // addNewProductInformationToolStripMenuItem
            // 
            this.addNewProductInformationToolStripMenuItem.Name = "addNewProductInformationToolStripMenuItem";
            this.addNewProductInformationToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.addNewProductInformationToolStripMenuItem.Text = "Add New Product Information";
            this.addNewProductInformationToolStripMenuItem.Click += new System.EventHandler(this.addNewProductInformationToolStripMenuItem_Click);
            // 
            // modifyProductInformationToolStripMenuItem
            // 
            this.modifyProductInformationToolStripMenuItem.Name = "modifyProductInformationToolStripMenuItem";
            this.modifyProductInformationToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.modifyProductInformationToolStripMenuItem.Text = "Modify Existing Product Information";
            this.modifyProductInformationToolStripMenuItem.Click += new System.EventHandler(this.modifyProductInformationToolStripMenuItem_Click);
            // 
            // deleteExistingProductInformationToolStripMenuItem
            // 
            this.deleteExistingProductInformationToolStripMenuItem.Name = "deleteExistingProductInformationToolStripMenuItem";
            this.deleteExistingProductInformationToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.deleteExistingProductInformationToolStripMenuItem.Text = "Delete Existing Product Information";
            this.deleteExistingProductInformationToolStripMenuItem.Click += new System.EventHandler(this.deleteExistingProductInformationToolStripMenuItem_Click);
            // 
            // searchDisplayProductInformationToolStripMenuItem
            // 
            this.searchDisplayProductInformationToolStripMenuItem.Name = "searchDisplayProductInformationToolStripMenuItem";
            this.searchDisplayProductInformationToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.searchDisplayProductInformationToolStripMenuItem.Text = "Search && Display Product Information";
            this.searchDisplayProductInformationToolStripMenuItem.Click += new System.EventHandler(this.searchDisplayProductInformationToolStripMenuItem_Click);
            // 
            // orderToolStripMenuItem
            // 
            this.orderToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewOrderInformationToolStripMenuItem,
            this.modifyExistingOrderInformationToolStripMenuItem,
            this.searchAndDisplayOrderInformationToolStripMenuItem,
            this.delToolStripMenuItem});
            this.orderToolStripMenuItem.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderToolStripMenuItem.Name = "orderToolStripMenuItem";
            this.orderToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.orderToolStripMenuItem.Text = "Order";
            // 
            // addNewOrderInformationToolStripMenuItem
            // 
            this.addNewOrderInformationToolStripMenuItem.Name = "addNewOrderInformationToolStripMenuItem";
            this.addNewOrderInformationToolStripMenuItem.Size = new System.Drawing.Size(273, 22);
            this.addNewOrderInformationToolStripMenuItem.Text = "Add New Order Information";
            this.addNewOrderInformationToolStripMenuItem.Click += new System.EventHandler(this.addNewOrderInformationToolStripMenuItem_Click);
            // 
            // modifyExistingOrderInformationToolStripMenuItem
            // 
            this.modifyExistingOrderInformationToolStripMenuItem.Name = "modifyExistingOrderInformationToolStripMenuItem";
            this.modifyExistingOrderInformationToolStripMenuItem.Size = new System.Drawing.Size(273, 22);
            this.modifyExistingOrderInformationToolStripMenuItem.Text = "Modify Existing Order Information";
            this.modifyExistingOrderInformationToolStripMenuItem.Click += new System.EventHandler(this.modifyExistingOrderInformationToolStripMenuItem_Click);
            // 
            // searchAndDisplayOrderInformationToolStripMenuItem
            // 
            this.searchAndDisplayOrderInformationToolStripMenuItem.Name = "searchAndDisplayOrderInformationToolStripMenuItem";
            this.searchAndDisplayOrderInformationToolStripMenuItem.Size = new System.Drawing.Size(273, 22);
            this.searchAndDisplayOrderInformationToolStripMenuItem.Text = "Search and Display Order Information";
            this.searchAndDisplayOrderInformationToolStripMenuItem.Click += new System.EventHandler(this.searchAndDisplayOrderInformationToolStripMenuItem_Click);
            // 
            // delToolStripMenuItem
            // 
            this.delToolStripMenuItem.Name = "delToolStripMenuItem";
            this.delToolStripMenuItem.Size = new System.Drawing.Size(273, 22);
            this.delToolStripMenuItem.Text = "Delete Existing Order Information";
            this.delToolStripMenuItem.Click += new System.EventHandler(this.delToolStripMenuItem_Click);
            // 
            // inventoryToolStripMenuItem
            // 
            this.inventoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aToolStripMenuItem,
            this.modifyExistingSupplierInformationToolStripMenuItem,
            this.deleteExistingSupplierInformationToolStripMenuItem,
            this.searchAndDisplaySupplierInformationToolStripMenuItem});
            this.inventoryToolStripMenuItem.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inventoryToolStripMenuItem.Name = "inventoryToolStripMenuItem";
            this.inventoryToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.inventoryToolStripMenuItem.Text = "Inventory";
            // 
            // aToolStripMenuItem
            // 
            this.aToolStripMenuItem.Name = "aToolStripMenuItem";
            this.aToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.aToolStripMenuItem.Text = "Add New Supplier Information";
            this.aToolStripMenuItem.Click += new System.EventHandler(this.aToolStripMenuItem_Click);
            // 
            // modifyExistingSupplierInformationToolStripMenuItem
            // 
            this.modifyExistingSupplierInformationToolStripMenuItem.Name = "modifyExistingSupplierInformationToolStripMenuItem";
            this.modifyExistingSupplierInformationToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.modifyExistingSupplierInformationToolStripMenuItem.Text = "Modify Existing Supplier Information";
            this.modifyExistingSupplierInformationToolStripMenuItem.Click += new System.EventHandler(this.modifyExistingSupplierInformationToolStripMenuItem_Click);
            // 
            // deleteExistingSupplierInformationToolStripMenuItem
            // 
            this.deleteExistingSupplierInformationToolStripMenuItem.Name = "deleteExistingSupplierInformationToolStripMenuItem";
            this.deleteExistingSupplierInformationToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.deleteExistingSupplierInformationToolStripMenuItem.Text = "Delete Existing Supplier Information";
            this.deleteExistingSupplierInformationToolStripMenuItem.Click += new System.EventHandler(this.deleteExistingSupplierInformationToolStripMenuItem_Click);
            // 
            // searchAndDisplaySupplierInformationToolStripMenuItem
            // 
            this.searchAndDisplaySupplierInformationToolStripMenuItem.Name = "searchAndDisplaySupplierInformationToolStripMenuItem";
            this.searchAndDisplaySupplierInformationToolStripMenuItem.Size = new System.Drawing.Size(275, 22);
            this.searchAndDisplaySupplierInformationToolStripMenuItem.Text = "Search && Display Supplier Information";
            this.searchAndDisplaySupplierInformationToolStripMenuItem.Click += new System.EventHandler(this.searchAndDisplaySupplierInformationToolStripMenuItem_Click);
            // 
            // stockToolStripMenuItem
            // 
            this.stockToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editProductQuantityToolStripMenuItem,
            this.searchAndDisplayProductQuantityToolStripMenuItem,
            this.computeExcessShortfallToolStripMenuItem});
            this.stockToolStripMenuItem.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockToolStripMenuItem.Name = "stockToolStripMenuItem";
            this.stockToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.stockToolStripMenuItem.Text = "Stock";
            // 
            // editProductQuantityToolStripMenuItem
            // 
            this.editProductQuantityToolStripMenuItem.Name = "editProductQuantityToolStripMenuItem";
            this.editProductQuantityToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.editProductQuantityToolStripMenuItem.Text = "Edit Product Quantity";
            this.editProductQuantityToolStripMenuItem.Click += new System.EventHandler(this.editProductQuantityToolStripMenuItem_Click);
            // 
            // searchAndDisplayProductQuantityToolStripMenuItem
            // 
            this.searchAndDisplayProductQuantityToolStripMenuItem.Name = "searchAndDisplayProductQuantityToolStripMenuItem";
            this.searchAndDisplayProductQuantityToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.searchAndDisplayProductQuantityToolStripMenuItem.Text = "Search and Display Product Quantity";
            this.searchAndDisplayProductQuantityToolStripMenuItem.Click += new System.EventHandler(this.searchAndDisplayProductQuantityToolStripMenuItem_Click);
            // 
            // computeExcessShortfallToolStripMenuItem
            // 
            this.computeExcessShortfallToolStripMenuItem.Name = "computeExcessShortfallToolStripMenuItem";
            this.computeExcessShortfallToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.computeExcessShortfallToolStripMenuItem.Text = "Compute Excess or Shortfall";
            // 
            // pnlWelcomeScreen
            // 
            this.pnlWelcomeScreen.Controls.Add(this.lblWelcome2);
            this.pnlWelcomeScreen.Controls.Add(this.lblWelcome1);
            this.pnlWelcomeScreen.Controls.Add(this.picBoxLogoMainPnl);
            this.pnlWelcomeScreen.Controls.Add(this.lblMainWindowInfo);
            this.pnlWelcomeScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWelcomeScreen.Location = new System.Drawing.Point(0, 0);
            this.pnlWelcomeScreen.Name = "pnlWelcomeScreen";
            this.pnlWelcomeScreen.Size = new System.Drawing.Size(554, 414);
            this.pnlWelcomeScreen.TabIndex = 2;
            // 
            // lblWelcome2
            // 
            this.lblWelcome2.AutoSize = true;
            this.lblWelcome2.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblWelcome2.Location = new System.Drawing.Point(181, 318);
            this.lblWelcome2.Name = "lblWelcome2";
            this.lblWelcome2.Size = new System.Drawing.Size(195, 27);
            this.lblWelcome2.TabIndex = 14;
            this.lblWelcome2.Text = " E-Business System";
            // 
            // lblWelcome1
            // 
            this.lblWelcome1.AutoSize = true;
            this.lblWelcome1.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblWelcome1.Location = new System.Drawing.Point(79, 52);
            this.lblWelcome1.Name = "lblWelcome1";
            this.lblWelcome1.Size = new System.Drawing.Size(396, 27);
            this.lblWelcome1.TabIndex = 13;
            this.lblWelcome1.Text = "Welcome to the Electronic Gadget Online";
            // 
            // picBoxLogoMainPnl
            // 
            this.picBoxLogoMainPnl.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoMainPnl.Image")));
            this.picBoxLogoMainPnl.Location = new System.Drawing.Point(122, 48);
            this.picBoxLogoMainPnl.Name = "picBoxLogoMainPnl";
            this.picBoxLogoMainPnl.Size = new System.Drawing.Size(309, 284);
            this.picBoxLogoMainPnl.TabIndex = 16;
            this.picBoxLogoMainPnl.TabStop = false;
            // 
            // lblMainWindowInfo
            // 
            this.lblMainWindowInfo.AutoSize = true;
            this.lblMainWindowInfo.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMainWindowInfo.Location = new System.Drawing.Point(4, 393);
            this.lblMainWindowInfo.Name = "lblMainWindowInfo";
            this.lblMainWindowInfo.Size = new System.Drawing.Size(285, 17);
            this.lblMainWindowInfo.TabIndex = 15;
            this.lblMainWindowInfo.Text = "Select an option from the above menu to proceed...";
            // 
            // pnlRegisterStaff
            // 
            this.pnlRegisterStaff.AutoScroll = true;
            this.pnlRegisterStaff.Controls.Add(this.lblInfoRegisterStaff);
            this.pnlRegisterStaff.Controls.Add(this.lblRegisterStaff);
            this.pnlRegisterStaff.Controls.Add(this.picBoxLogoRegisterStaffPnl);
            this.pnlRegisterStaff.Controls.Add(this.tabCtrRegisterStaffPnl);
            this.pnlRegisterStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRegisterStaff.Location = new System.Drawing.Point(0, 0);
            this.pnlRegisterStaff.Name = "pnlRegisterStaff";
            this.pnlRegisterStaff.Size = new System.Drawing.Size(554, 414);
            this.pnlRegisterStaff.TabIndex = 3;
            // 
            // lblInfoRegisterStaff
            // 
            this.lblInfoRegisterStaff.AutoSize = true;
            this.lblInfoRegisterStaff.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoRegisterStaff.Location = new System.Drawing.Point(1, 122);
            this.lblInfoRegisterStaff.Name = "lblInfoRegisterStaff";
            this.lblInfoRegisterStaff.Size = new System.Drawing.Size(199, 17);
            this.lblInfoRegisterStaff.TabIndex = 42;
            this.lblInfoRegisterStaff.Text = "Please fill in the required fields ...";
            // 
            // lblRegisterStaff
            // 
            this.lblRegisterStaff.AutoSize = true;
            this.lblRegisterStaff.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegisterStaff.Location = new System.Drawing.Point(164, 60);
            this.lblRegisterStaff.Name = "lblRegisterStaff";
            this.lblRegisterStaff.Size = new System.Drawing.Size(186, 26);
            this.lblRegisterStaff.TabIndex = 41;
            this.lblRegisterStaff.Text = "Register New Staff";
            // 
            // picBoxLogoRegisterStaffPnl
            // 
            this.picBoxLogoRegisterStaffPnl.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoRegisterStaffPnl.Image")));
            this.picBoxLogoRegisterStaffPnl.Location = new System.Drawing.Point(424, 33);
            this.picBoxLogoRegisterStaffPnl.Name = "picBoxLogoRegisterStaffPnl";
            this.picBoxLogoRegisterStaffPnl.Size = new System.Drawing.Size(100, 81);
            this.picBoxLogoRegisterStaffPnl.TabIndex = 39;
            this.picBoxLogoRegisterStaffPnl.TabStop = false;
            // 
            // tabCtrRegisterStaffPnl
            // 
            this.tabCtrRegisterStaffPnl.Controls.Add(this.tabStaffPersonalInformation);
            this.tabCtrRegisterStaffPnl.Controls.Add(this.tabStaffContactInfo);
            this.tabCtrRegisterStaffPnl.Controls.Add(this.tabStaffAccountInformation);
            this.tabCtrRegisterStaffPnl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabCtrRegisterStaffPnl.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCtrRegisterStaffPnl.Location = new System.Drawing.Point(0, 142);
            this.tabCtrRegisterStaffPnl.Name = "tabCtrRegisterStaffPnl";
            this.tabCtrRegisterStaffPnl.SelectedIndex = 0;
            this.tabCtrRegisterStaffPnl.Size = new System.Drawing.Size(554, 272);
            this.tabCtrRegisterStaffPnl.TabIndex = 40;
            // 
            // tabStaffPersonalInformation
            // 
            this.tabStaffPersonalInformation.AutoScroll = true;
            this.tabStaffPersonalInformation.BackColor = System.Drawing.SystemColors.Control;
            this.tabStaffPersonalInformation.Controls.Add(this.lblProceedToContactInformation);
            this.tabStaffPersonalInformation.Controls.Add(this.lblFill1);
            this.tabStaffPersonalInformation.Controls.Add(this.lblMandatoryFields1);
            this.tabStaffPersonalInformation.Controls.Add(this.grpRegisterPersonalParticulars);
            this.tabStaffPersonalInformation.Location = new System.Drawing.Point(4, 24);
            this.tabStaffPersonalInformation.Name = "tabStaffPersonalInformation";
            this.tabStaffPersonalInformation.Padding = new System.Windows.Forms.Padding(3);
            this.tabStaffPersonalInformation.Size = new System.Drawing.Size(546, 244);
            this.tabStaffPersonalInformation.TabIndex = 0;
            this.tabStaffPersonalInformation.Text = "Personal Information";
            // 
            // lblProceedToContactInformation
            // 
            this.lblProceedToContactInformation.AutoSize = true;
            this.lblProceedToContactInformation.Location = new System.Drawing.Point(12, 224);
            this.lblProceedToContactInformation.Name = "lblProceedToContactInformation";
            this.lblProceedToContactInformation.Size = new System.Drawing.Size(396, 15);
            this.lblProceedToContactInformation.TabIndex = 45;
            this.lblProceedToContactInformation.Text = "Please proceed to fill in required fields in the \'Contact Information\' section";
            // 
            // lblFill1
            // 
            this.lblFill1.AutoSize = true;
            this.lblFill1.Location = new System.Drawing.Point(6, 208);
            this.lblFill1.Name = "lblFill1";
            this.lblFill1.Size = new System.Drawing.Size(0, 15);
            this.lblFill1.TabIndex = 44;
            // 
            // lblMandatoryFields1
            // 
            this.lblMandatoryFields1.AutoSize = true;
            this.lblMandatoryFields1.Location = new System.Drawing.Point(14, 4);
            this.lblMandatoryFields1.Name = "lblMandatoryFields1";
            this.lblMandatoryFields1.Size = new System.Drawing.Size(146, 15);
            this.lblMandatoryFields1.TabIndex = 43;
            this.lblMandatoryFields1.Text = "* Denotes mandatory fields";
            // 
            // grpRegisterPersonalParticulars
            // 
            this.grpRegisterPersonalParticulars.Controls.Add(this.lblRegisterStaffName);
            this.grpRegisterPersonalParticulars.Controls.Add(this.rdBtnFemaleRegister);
            this.grpRegisterPersonalParticulars.Controls.Add(this.rdBtnMaleRegister);
            this.grpRegisterPersonalParticulars.Controls.Add(this.dtpRegisterDateOfBirth);
            this.grpRegisterPersonalParticulars.Controls.Add(this.lblRegisterStaffGender);
            this.grpRegisterPersonalParticulars.Controls.Add(this.lblRegisterDateOfBirth);
            this.grpRegisterPersonalParticulars.Controls.Add(this.tbxRegisterNRIC);
            this.grpRegisterPersonalParticulars.Controls.Add(this.lblRegisterNRIC);
            this.grpRegisterPersonalParticulars.Controls.Add(this.label150);
            this.grpRegisterPersonalParticulars.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpRegisterPersonalParticulars.Location = new System.Drawing.Point(12, 18);
            this.grpRegisterPersonalParticulars.Name = "grpRegisterPersonalParticulars";
            this.grpRegisterPersonalParticulars.Size = new System.Drawing.Size(521, 202);
            this.grpRegisterPersonalParticulars.TabIndex = 42;
            this.grpRegisterPersonalParticulars.TabStop = false;
            this.grpRegisterPersonalParticulars.Text = "Particulars of Registrant";
            // 
            // lblRegisterStaffName
            // 
            this.lblRegisterStaffName.AutoSize = true;
            this.lblRegisterStaffName.Location = new System.Drawing.Point(33, 35);
            this.lblRegisterStaffName.Name = "lblRegisterStaffName";
            this.lblRegisterStaffName.Size = new System.Drawing.Size(181, 15);
            this.lblRegisterStaffName.TabIndex = 17;
            this.lblRegisterStaffName.Text = "- Display Staff Name for adding -";
            // 
            // rdBtnFemaleRegister
            // 
            this.rdBtnFemaleRegister.AutoSize = true;
            this.rdBtnFemaleRegister.Location = new System.Drawing.Point(106, 170);
            this.rdBtnFemaleRegister.Name = "rdBtnFemaleRegister";
            this.rdBtnFemaleRegister.Size = new System.Drawing.Size(60, 19);
            this.rdBtnFemaleRegister.TabIndex = 16;
            this.rdBtnFemaleRegister.TabStop = true;
            this.rdBtnFemaleRegister.Text = "Female";
            this.rdBtnFemaleRegister.UseVisualStyleBackColor = true;
            // 
            // rdBtnMaleRegister
            // 
            this.rdBtnMaleRegister.AutoSize = true;
            this.rdBtnMaleRegister.Location = new System.Drawing.Point(27, 170);
            this.rdBtnMaleRegister.Name = "rdBtnMaleRegister";
            this.rdBtnMaleRegister.Size = new System.Drawing.Size(50, 19);
            this.rdBtnMaleRegister.TabIndex = 15;
            this.rdBtnMaleRegister.TabStop = true;
            this.rdBtnMaleRegister.Text = "Male";
            this.rdBtnMaleRegister.UseVisualStyleBackColor = true;
            // 
            // dtpRegisterDateOfBirth
            // 
            this.dtpRegisterDateOfBirth.Location = new System.Drawing.Point(27, 123);
            this.dtpRegisterDateOfBirth.Name = "dtpRegisterDateOfBirth";
            this.dtpRegisterDateOfBirth.Size = new System.Drawing.Size(209, 23);
            this.dtpRegisterDateOfBirth.TabIndex = 14;
            // 
            // lblRegisterStaffGender
            // 
            this.lblRegisterStaffGender.AutoSize = true;
            this.lblRegisterStaffGender.Location = new System.Drawing.Point(24, 150);
            this.lblRegisterStaffGender.Name = "lblRegisterStaffGender";
            this.lblRegisterStaffGender.Size = new System.Drawing.Size(56, 15);
            this.lblRegisterStaffGender.TabIndex = 12;
            this.lblRegisterStaffGender.Text = "* Gender:";
            // 
            // lblRegisterDateOfBirth
            // 
            this.lblRegisterDateOfBirth.AutoSize = true;
            this.lblRegisterDateOfBirth.Location = new System.Drawing.Point(24, 105);
            this.lblRegisterDateOfBirth.Name = "lblRegisterDateOfBirth";
            this.lblRegisterDateOfBirth.Size = new System.Drawing.Size(90, 15);
            this.lblRegisterDateOfBirth.TabIndex = 10;
            this.lblRegisterDateOfBirth.Text = "* Date Of Birth:";
            // 
            // tbxRegisterNRIC
            // 
            this.tbxRegisterNRIC.Location = new System.Drawing.Point(27, 79);
            this.tbxRegisterNRIC.Name = "tbxRegisterNRIC";
            this.tbxRegisterNRIC.Size = new System.Drawing.Size(209, 23);
            this.tbxRegisterNRIC.TabIndex = 7;
            // 
            // lblRegisterNRIC
            // 
            this.lblRegisterNRIC.AutoSize = true;
            this.lblRegisterNRIC.Location = new System.Drawing.Point(24, 61);
            this.lblRegisterNRIC.Name = "lblRegisterNRIC";
            this.lblRegisterNRIC.Size = new System.Drawing.Size(48, 15);
            this.lblRegisterNRIC.TabIndex = 6;
            this.lblRegisterNRIC.Text = "* NRIC:";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(24, 15);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(96, 15);
            this.label150.TabIndex = 2;
            this.label150.Text = "* Name of Staff:";
            // 
            // tabStaffContactInfo
            // 
            this.tabStaffContactInfo.AutoScroll = true;
            this.tabStaffContactInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tabStaffContactInfo.Controls.Add(this.lblProceedToAccountInfo);
            this.tabStaffContactInfo.Controls.Add(this.grpRegisterContactDetails);
            this.tabStaffContactInfo.Controls.Add(this.lblFill2);
            this.tabStaffContactInfo.Controls.Add(this.lblMandatoryFields2);
            this.tabStaffContactInfo.Location = new System.Drawing.Point(4, 24);
            this.tabStaffContactInfo.Name = "tabStaffContactInfo";
            this.tabStaffContactInfo.Size = new System.Drawing.Size(546, 244);
            this.tabStaffContactInfo.TabIndex = 2;
            this.tabStaffContactInfo.Text = "Contact Information";
            // 
            // lblProceedToAccountInfo
            // 
            this.lblProceedToAccountInfo.AutoSize = true;
            this.lblProceedToAccountInfo.Location = new System.Drawing.Point(12, 224);
            this.lblProceedToAccountInfo.Name = "lblProceedToAccountInfo";
            this.lblProceedToAccountInfo.Size = new System.Drawing.Size(399, 15);
            this.lblProceedToAccountInfo.TabIndex = 47;
            this.lblProceedToAccountInfo.Text = "Please proceed to fill in required fields in the \'Account Information\' section";
            // 
            // grpRegisterContactDetails
            // 
            this.grpRegisterContactDetails.BackColor = System.Drawing.SystemColors.Control;
            this.grpRegisterContactDetails.Controls.Add(this.lblRegisterHpContact);
            this.grpRegisterContactDetails.Controls.Add(this.lblRegisterHomeContact);
            this.grpRegisterContactDetails.Controls.Add(this.lblRegisterContactNo);
            this.grpRegisterContactDetails.Controls.Add(this.tbxRegisterHpNoContact);
            this.grpRegisterContactDetails.Controls.Add(this.tbxRegisterHomeNo);
            this.grpRegisterContactDetails.Controls.Add(this.tbxRegisterAddress);
            this.grpRegisterContactDetails.Controls.Add(this.lblRegisterAddress);
            this.grpRegisterContactDetails.Location = new System.Drawing.Point(12, 18);
            this.grpRegisterContactDetails.Name = "grpRegisterContactDetails";
            this.grpRegisterContactDetails.Size = new System.Drawing.Size(523, 205);
            this.grpRegisterContactDetails.TabIndex = 46;
            this.grpRegisterContactDetails.TabStop = false;
            this.grpRegisterContactDetails.Text = "Contact Information of Registrant";
            // 
            // lblRegisterHpContact
            // 
            this.lblRegisterHpContact.AutoSize = true;
            this.lblRegisterHpContact.Location = new System.Drawing.Point(233, 78);
            this.lblRegisterHpContact.Name = "lblRegisterHpContact";
            this.lblRegisterHpContact.Size = new System.Drawing.Size(71, 15);
            this.lblRegisterHpContact.TabIndex = 50;
            this.lblRegisterHpContact.Text = "(Handphone)";
            // 
            // lblRegisterHomeContact
            // 
            this.lblRegisterHomeContact.AutoSize = true;
            this.lblRegisterHomeContact.Location = new System.Drawing.Point(233, 52);
            this.lblRegisterHomeContact.Name = "lblRegisterHomeContact";
            this.lblRegisterHomeContact.Size = new System.Drawing.Size(43, 15);
            this.lblRegisterHomeContact.TabIndex = 49;
            this.lblRegisterHomeContact.Text = "(Home)";
            // 
            // lblRegisterContactNo
            // 
            this.lblRegisterContactNo.AutoSize = true;
            this.lblRegisterContactNo.Location = new System.Drawing.Point(14, 30);
            this.lblRegisterContactNo.Name = "lblRegisterContactNo";
            this.lblRegisterContactNo.Size = new System.Drawing.Size(106, 15);
            this.lblRegisterContactNo.TabIndex = 48;
            this.lblRegisterContactNo.Text = "* Contact Numbers:";
            // 
            // tbxRegisterHpNoContact
            // 
            this.tbxRegisterHpNoContact.Location = new System.Drawing.Point(18, 75);
            this.tbxRegisterHpNoContact.Name = "tbxRegisterHpNoContact";
            this.tbxRegisterHpNoContact.Size = new System.Drawing.Size(209, 23);
            this.tbxRegisterHpNoContact.TabIndex = 47;
            // 
            // tbxRegisterHomeNo
            // 
            this.tbxRegisterHomeNo.Location = new System.Drawing.Point(18, 49);
            this.tbxRegisterHomeNo.Name = "tbxRegisterHomeNo";
            this.tbxRegisterHomeNo.Size = new System.Drawing.Size(209, 23);
            this.tbxRegisterHomeNo.TabIndex = 46;
            // 
            // tbxRegisterAddress
            // 
            this.tbxRegisterAddress.Location = new System.Drawing.Point(17, 138);
            this.tbxRegisterAddress.Multiline = true;
            this.tbxRegisterAddress.Name = "tbxRegisterAddress";
            this.tbxRegisterAddress.Size = new System.Drawing.Size(209, 48);
            this.tbxRegisterAddress.TabIndex = 42;
            // 
            // lblRegisterAddress
            // 
            this.lblRegisterAddress.AutoSize = true;
            this.lblRegisterAddress.Location = new System.Drawing.Point(15, 120);
            this.lblRegisterAddress.Name = "lblRegisterAddress";
            this.lblRegisterAddress.Size = new System.Drawing.Size(60, 15);
            this.lblRegisterAddress.TabIndex = 37;
            this.lblRegisterAddress.Text = "* Address:";
            // 
            // lblFill2
            // 
            this.lblFill2.AutoSize = true;
            this.lblFill2.Location = new System.Drawing.Point(6, 225);
            this.lblFill2.Name = "lblFill2";
            this.lblFill2.Size = new System.Drawing.Size(0, 15);
            this.lblFill2.TabIndex = 45;
            // 
            // lblMandatoryFields2
            // 
            this.lblMandatoryFields2.AutoSize = true;
            this.lblMandatoryFields2.Location = new System.Drawing.Point(14, 4);
            this.lblMandatoryFields2.Name = "lblMandatoryFields2";
            this.lblMandatoryFields2.Size = new System.Drawing.Size(146, 15);
            this.lblMandatoryFields2.TabIndex = 44;
            this.lblMandatoryFields2.Text = "* Denotes mandatory fields";
            // 
            // tabStaffAccountInformation
            // 
            this.tabStaffAccountInformation.BackColor = System.Drawing.SystemColors.Control;
            this.tabStaffAccountInformation.Controls.Add(this.btnCancelRegisterStaff);
            this.tabStaffAccountInformation.Controls.Add(this.lblMandatoryFields3);
            this.tabStaffAccountInformation.Controls.Add(this.btnRegisterStaff);
            this.tabStaffAccountInformation.Controls.Add(this.grpRegisterAccntDetails);
            this.tabStaffAccountInformation.Location = new System.Drawing.Point(4, 24);
            this.tabStaffAccountInformation.Name = "tabStaffAccountInformation";
            this.tabStaffAccountInformation.Padding = new System.Windows.Forms.Padding(3);
            this.tabStaffAccountInformation.Size = new System.Drawing.Size(546, 244);
            this.tabStaffAccountInformation.TabIndex = 1;
            this.tabStaffAccountInformation.Text = "Account Information";
            // 
            // btnCancelRegisterStaff
            // 
            this.btnCancelRegisterStaff.Location = new System.Drawing.Point(456, 200);
            this.btnCancelRegisterStaff.Name = "btnCancelRegisterStaff";
            this.btnCancelRegisterStaff.Size = new System.Drawing.Size(75, 23);
            this.btnCancelRegisterStaff.TabIndex = 43;
            this.btnCancelRegisterStaff.Text = "Cancel";
            this.btnCancelRegisterStaff.UseVisualStyleBackColor = true;
            this.btnCancelRegisterStaff.Click += new System.EventHandler(this.btnCancelRegisterStaff_Click);
            // 
            // lblMandatoryFields3
            // 
            this.lblMandatoryFields3.AutoSize = true;
            this.lblMandatoryFields3.Location = new System.Drawing.Point(14, 4);
            this.lblMandatoryFields3.Name = "lblMandatoryFields3";
            this.lblMandatoryFields3.Size = new System.Drawing.Size(146, 15);
            this.lblMandatoryFields3.TabIndex = 44;
            this.lblMandatoryFields3.Text = "* Denotes mandatory fields";
            // 
            // btnRegisterStaff
            // 
            this.btnRegisterStaff.Location = new System.Drawing.Point(359, 200);
            this.btnRegisterStaff.Name = "btnRegisterStaff";
            this.btnRegisterStaff.Size = new System.Drawing.Size(75, 23);
            this.btnRegisterStaff.TabIndex = 42;
            this.btnRegisterStaff.Text = "Register";
            this.btnRegisterStaff.UseVisualStyleBackColor = true;
            this.btnRegisterStaff.Click += new System.EventHandler(this.btnRegisterStaff_Click);
            // 
            // grpRegisterAccntDetails
            // 
            this.grpRegisterAccntDetails.Controls.Add(this.chkBoxInventoryManager);
            this.grpRegisterAccntDetails.Controls.Add(this.chkBoxOrderManager);
            this.grpRegisterAccntDetails.Controls.Add(this.chkBoxCatalogueManager);
            this.grpRegisterAccntDetails.Controls.Add(this.chkBoxAdministrator);
            this.grpRegisterAccntDetails.Controls.Add(this.tbxConfirmRegisterPassword);
            this.grpRegisterAccntDetails.Controls.Add(this.lblCfmRegisteredPassword);
            this.grpRegisterAccntDetails.Controls.Add(this.tbxRegisterPassword);
            this.grpRegisterAccntDetails.Controls.Add(this.lblRegisterPassword);
            this.grpRegisterAccntDetails.Controls.Add(this.lblRegisterStaffRole);
            this.grpRegisterAccntDetails.Location = new System.Drawing.Point(12, 18);
            this.grpRegisterAccntDetails.Name = "grpRegisterAccntDetails";
            this.grpRegisterAccntDetails.Size = new System.Drawing.Size(519, 178);
            this.grpRegisterAccntDetails.TabIndex = 42;
            this.grpRegisterAccntDetails.TabStop = false;
            this.grpRegisterAccntDetails.Text = "Account Details";
            // 
            // chkBoxInventoryManager
            // 
            this.chkBoxInventoryManager.AutoSize = true;
            this.chkBoxInventoryManager.Location = new System.Drawing.Point(134, 144);
            this.chkBoxInventoryManager.Name = "chkBoxInventoryManager";
            this.chkBoxInventoryManager.Size = new System.Drawing.Size(125, 19);
            this.chkBoxInventoryManager.TabIndex = 35;
            this.chkBoxInventoryManager.Text = "Inventory Manager";
            this.chkBoxInventoryManager.UseVisualStyleBackColor = true;
            // 
            // chkBoxOrderManager
            // 
            this.chkBoxOrderManager.AutoSize = true;
            this.chkBoxOrderManager.Location = new System.Drawing.Point(10, 144);
            this.chkBoxOrderManager.Name = "chkBoxOrderManager";
            this.chkBoxOrderManager.Size = new System.Drawing.Size(105, 19);
            this.chkBoxOrderManager.TabIndex = 34;
            this.chkBoxOrderManager.Text = "Order Manager";
            this.chkBoxOrderManager.UseVisualStyleBackColor = true;
            // 
            // chkBoxCatalogueManager
            // 
            this.chkBoxCatalogueManager.AutoSize = true;
            this.chkBoxCatalogueManager.Location = new System.Drawing.Point(134, 122);
            this.chkBoxCatalogueManager.Name = "chkBoxCatalogueManager";
            this.chkBoxCatalogueManager.Size = new System.Drawing.Size(124, 19);
            this.chkBoxCatalogueManager.TabIndex = 33;
            this.chkBoxCatalogueManager.Text = "Catalogue Manager";
            this.chkBoxCatalogueManager.UseVisualStyleBackColor = true;
            // 
            // chkBoxAdministrator
            // 
            this.chkBoxAdministrator.AutoSize = true;
            this.chkBoxAdministrator.Location = new System.Drawing.Point(10, 124);
            this.chkBoxAdministrator.Name = "chkBoxAdministrator";
            this.chkBoxAdministrator.Size = new System.Drawing.Size(97, 19);
            this.chkBoxAdministrator.TabIndex = 32;
            this.chkBoxAdministrator.Text = "Administrator";
            this.chkBoxAdministrator.UseVisualStyleBackColor = true;
            // 
            // tbxConfirmRegisterPassword
            // 
            this.tbxConfirmRegisterPassword.Location = new System.Drawing.Point(8, 76);
            this.tbxConfirmRegisterPassword.Name = "tbxConfirmRegisterPassword";
            this.tbxConfirmRegisterPassword.Size = new System.Drawing.Size(209, 23);
            this.tbxConfirmRegisterPassword.TabIndex = 29;
            // 
            // lblCfmRegisteredPassword
            // 
            this.lblCfmRegisteredPassword.AutoSize = true;
            this.lblCfmRegisteredPassword.Location = new System.Drawing.Point(8, 61);
            this.lblCfmRegisteredPassword.Name = "lblCfmRegisteredPassword";
            this.lblCfmRegisteredPassword.Size = new System.Drawing.Size(111, 15);
            this.lblCfmRegisteredPassword.TabIndex = 28;
            this.lblCfmRegisteredPassword.Text = "* Confirm Password:";
            // 
            // tbxRegisterPassword
            // 
            this.tbxRegisterPassword.Location = new System.Drawing.Point(8, 36);
            this.tbxRegisterPassword.Name = "tbxRegisterPassword";
            this.tbxRegisterPassword.Size = new System.Drawing.Size(209, 23);
            this.tbxRegisterPassword.TabIndex = 27;
            // 
            // lblRegisterPassword
            // 
            this.lblRegisterPassword.AutoSize = true;
            this.lblRegisterPassword.Location = new System.Drawing.Point(8, 19);
            this.lblRegisterPassword.Name = "lblRegisterPassword";
            this.lblRegisterPassword.Size = new System.Drawing.Size(66, 15);
            this.lblRegisterPassword.TabIndex = 26;
            this.lblRegisterPassword.Text = "* Password:";
            // 
            // lblRegisterStaffRole
            // 
            this.lblRegisterStaffRole.AutoSize = true;
            this.lblRegisterStaffRole.Location = new System.Drawing.Point(7, 107);
            this.lblRegisterStaffRole.Name = "lblRegisterStaffRole";
            this.lblRegisterStaffRole.Size = new System.Drawing.Size(112, 15);
            this.lblRegisterStaffRole.TabIndex = 23;
            this.lblRegisterStaffRole.Text = "* Role of Registrant:";
            // 
            // pnlResetPassword
            // 
            this.pnlResetPassword.Controls.Add(this.lblDispDOBStaffResetRw);
            this.pnlResetPassword.Controls.Add(this.lblDispNRICResetPw);
            this.pnlResetPassword.Controls.Add(this.lblDispStaffNameResetPw);
            this.pnlResetPassword.Controls.Add(this.lblCancelResetPw);
            this.pnlResetPassword.Controls.Add(this.btnResetPw);
            this.pnlResetPassword.Controls.Add(this.grpResetPW);
            this.pnlResetPassword.Controls.Add(this.lblDOBStaffResetPW);
            this.pnlResetPassword.Controls.Add(this.lblNRICStaffResetPW);
            this.pnlResetPassword.Controls.Add(this.lblNameOfStaffResetPW);
            this.pnlResetPassword.Controls.Add(this.lblResetPassword);
            this.pnlResetPassword.Controls.Add(this.picBxLogoPnlResetPassword);
            this.pnlResetPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlResetPassword.Location = new System.Drawing.Point(0, 0);
            this.pnlResetPassword.Name = "pnlResetPassword";
            this.pnlResetPassword.Size = new System.Drawing.Size(554, 414);
            this.pnlResetPassword.TabIndex = 7;
            // 
            // lblDispDOBStaffResetRw
            // 
            this.lblDispDOBStaffResetRw.AutoSize = true;
            this.lblDispDOBStaffResetRw.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispDOBStaffResetRw.Location = new System.Drawing.Point(250, 184);
            this.lblDispDOBStaffResetRw.Name = "lblDispDOBStaffResetRw";
            this.lblDispDOBStaffResetRw.Size = new System.Drawing.Size(45, 15);
            this.lblDispDOBStaffResetRw.TabIndex = 55;
            this.lblDispDOBStaffResetRw.Text = "- DOB -";
            // 
            // lblDispNRICResetPw
            // 
            this.lblDispNRICResetPw.AutoSize = true;
            this.lblDispNRICResetPw.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispNRICResetPw.Location = new System.Drawing.Point(250, 162);
            this.lblDispNRICResetPw.Name = "lblDispNRICResetPw";
            this.lblDispNRICResetPw.Size = new System.Drawing.Size(50, 15);
            this.lblDispNRICResetPw.TabIndex = 54;
            this.lblDispNRICResetPw.Text = "- NRIC -";
            // 
            // lblDispStaffNameResetPw
            // 
            this.lblDispStaffNameResetPw.AutoSize = true;
            this.lblDispStaffNameResetPw.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispStaffNameResetPw.Location = new System.Drawing.Point(250, 140);
            this.lblDispStaffNameResetPw.Name = "lblDispStaffNameResetPw";
            this.lblDispStaffNameResetPw.Size = new System.Drawing.Size(101, 15);
            this.lblDispStaffNameResetPw.TabIndex = 53;
            this.lblDispStaffNameResetPw.Text = "- Name of Staff - ";
            // 
            // lblCancelResetPw
            // 
            this.lblCancelResetPw.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCancelResetPw.Location = new System.Drawing.Point(449, 362);
            this.lblCancelResetPw.Name = "lblCancelResetPw";
            this.lblCancelResetPw.Size = new System.Drawing.Size(75, 23);
            this.lblCancelResetPw.TabIndex = 52;
            this.lblCancelResetPw.Text = "Cancel";
            this.lblCancelResetPw.UseVisualStyleBackColor = true;
            this.lblCancelResetPw.Click += new System.EventHandler(this.lblCancelResetPw_Click);
            // 
            // btnResetPw
            // 
            this.btnResetPw.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetPw.Location = new System.Drawing.Point(335, 362);
            this.btnResetPw.Name = "btnResetPw";
            this.btnResetPw.Size = new System.Drawing.Size(102, 23);
            this.btnResetPw.TabIndex = 51;
            this.btnResetPw.Text = "Reset Password";
            this.btnResetPw.UseVisualStyleBackColor = true;
            this.btnResetPw.Click += new System.EventHandler(this.btnResetPw_Click);
            // 
            // grpResetPW
            // 
            this.grpResetPW.Controls.Add(this.tbxCfmResetPw);
            this.grpResetPW.Controls.Add(this.tbxResetPw);
            this.grpResetPW.Controls.Add(this.lblCfmNewPw);
            this.grpResetPW.Controls.Add(this.lblResetPW);
            this.grpResetPW.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpResetPW.Location = new System.Drawing.Point(48, 217);
            this.grpResetPW.Name = "grpResetPW";
            this.grpResetPW.Size = new System.Drawing.Size(476, 134);
            this.grpResetPW.TabIndex = 50;
            this.grpResetPW.TabStop = false;
            this.grpResetPW.Text = "Reset Staff Password";
            // 
            // tbxCfmResetPw
            // 
            this.tbxCfmResetPw.Location = new System.Drawing.Point(21, 88);
            this.tbxCfmResetPw.Name = "tbxCfmResetPw";
            this.tbxCfmResetPw.Size = new System.Drawing.Size(171, 23);
            this.tbxCfmResetPw.TabIndex = 3;
            // 
            // tbxResetPw
            // 
            this.tbxResetPw.Location = new System.Drawing.Point(21, 47);
            this.tbxResetPw.Name = "tbxResetPw";
            this.tbxResetPw.Size = new System.Drawing.Size(171, 23);
            this.tbxResetPw.TabIndex = 2;
            // 
            // lblCfmNewPw
            // 
            this.lblCfmNewPw.AutoSize = true;
            this.lblCfmNewPw.Location = new System.Drawing.Point(18, 73);
            this.lblCfmNewPw.Name = "lblCfmNewPw";
            this.lblCfmNewPw.Size = new System.Drawing.Size(128, 15);
            this.lblCfmNewPw.TabIndex = 1;
            this.lblCfmNewPw.Text = "Confirm New Password:";
            // 
            // lblResetPW
            // 
            this.lblResetPW.AutoSize = true;
            this.lblResetPW.Location = new System.Drawing.Point(18, 30);
            this.lblResetPW.Name = "lblResetPW";
            this.lblResetPW.Size = new System.Drawing.Size(146, 15);
            this.lblResetPW.TabIndex = 0;
            this.lblResetPW.Text = "Set New Default Password:";
            // 
            // lblDOBStaffResetPW
            // 
            this.lblDOBStaffResetPW.AutoSize = true;
            this.lblDOBStaffResetPW.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDOBStaffResetPW.Location = new System.Drawing.Point(140, 183);
            this.lblDOBStaffResetPW.Name = "lblDOBStaffResetPW";
            this.lblDOBStaffResetPW.Size = new System.Drawing.Size(103, 16);
            this.lblDOBStaffResetPW.TabIndex = 46;
            this.lblDOBStaffResetPW.Tag = "";
            this.lblDOBStaffResetPW.Text = "Date Of Birth :";
            // 
            // lblNRICStaffResetPW
            // 
            this.lblNRICStaffResetPW.AutoSize = true;
            this.lblNRICStaffResetPW.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNRICStaffResetPW.Location = new System.Drawing.Point(193, 161);
            this.lblNRICStaffResetPW.Name = "lblNRICStaffResetPW";
            this.lblNRICStaffResetPW.Size = new System.Drawing.Size(50, 16);
            this.lblNRICStaffResetPW.TabIndex = 45;
            this.lblNRICStaffResetPW.Tag = "";
            this.lblNRICStaffResetPW.Text = "NRIC :";
            // 
            // lblNameOfStaffResetPW
            // 
            this.lblNameOfStaffResetPW.AutoSize = true;
            this.lblNameOfStaffResetPW.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameOfStaffResetPW.Location = new System.Drawing.Point(137, 139);
            this.lblNameOfStaffResetPW.Name = "lblNameOfStaffResetPW";
            this.lblNameOfStaffResetPW.Size = new System.Drawing.Size(106, 16);
            this.lblNameOfStaffResetPW.TabIndex = 44;
            this.lblNameOfStaffResetPW.Tag = "";
            this.lblNameOfStaffResetPW.Text = "Name of Staff :";
            // 
            // lblResetPassword
            // 
            this.lblResetPassword.AutoSize = true;
            this.lblResetPassword.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResetPassword.Location = new System.Drawing.Point(135, 60);
            this.lblResetPassword.Name = "lblResetPassword";
            this.lblResetPassword.Size = new System.Drawing.Size(206, 26);
            this.lblResetPassword.TabIndex = 43;
            this.lblResetPassword.Text = "Reset Staff Password";
            // 
            // picBxLogoPnlResetPassword
            // 
            this.picBxLogoPnlResetPassword.Image = ((System.Drawing.Image)(resources.GetObject("picBxLogoPnlResetPassword.Image")));
            this.picBxLogoPnlResetPassword.Location = new System.Drawing.Point(424, 33);
            this.picBxLogoPnlResetPassword.Name = "picBxLogoPnlResetPassword";
            this.picBxLogoPnlResetPassword.Size = new System.Drawing.Size(100, 81);
            this.picBxLogoPnlResetPassword.TabIndex = 42;
            this.picBxLogoPnlResetPassword.TabStop = false;
            // 
            // pnlAddOrderInformation
            // 
            this.pnlAddOrderInformation.Controls.Add(this.tabCtrOrderInfo);
            this.pnlAddOrderInformation.Controls.Add(this.lblAddOrderInfo);
            this.pnlAddOrderInformation.Controls.Add(this.lblAddOrder);
            this.pnlAddOrderInformation.Controls.Add(this.picBxLogoAddOrderPnl);
            this.pnlAddOrderInformation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAddOrderInformation.Location = new System.Drawing.Point(0, 0);
            this.pnlAddOrderInformation.Name = "pnlAddOrderInformation";
            this.pnlAddOrderInformation.Size = new System.Drawing.Size(554, 414);
            this.pnlAddOrderInformation.TabIndex = 8;
            // 
            // tabCtrOrderInfo
            // 
            this.tabCtrOrderInfo.Controls.Add(this.tabPgAddOrderCustInfo);
            this.tabCtrOrderInfo.Controls.Add(this.tabPgAddOrderInfo);
            this.tabCtrOrderInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabCtrOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCtrOrderInfo.Location = new System.Drawing.Point(0, 150);
            this.tabCtrOrderInfo.Name = "tabCtrOrderInfo";
            this.tabCtrOrderInfo.SelectedIndex = 0;
            this.tabCtrOrderInfo.Size = new System.Drawing.Size(554, 264);
            this.tabCtrOrderInfo.TabIndex = 47;
            // 
            // tabPgAddOrderCustInfo
            // 
            this.tabPgAddOrderCustInfo.AutoScroll = true;
            this.tabPgAddOrderCustInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tabPgAddOrderCustInfo.Controls.Add(this.lblProceedToOrderInfo);
            this.tabPgAddOrderCustInfo.Controls.Add(this.grpBoxAddOrderInfo);
            this.tabPgAddOrderCustInfo.Controls.Add(this.lblMandatoryFields5);
            this.tabPgAddOrderCustInfo.Location = new System.Drawing.Point(4, 24);
            this.tabPgAddOrderCustInfo.Name = "tabPgAddOrderCustInfo";
            this.tabPgAddOrderCustInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgAddOrderCustInfo.Size = new System.Drawing.Size(546, 236);
            this.tabPgAddOrderCustInfo.TabIndex = 0;
            this.tabPgAddOrderCustInfo.Text = "Customer Information";
            // 
            // lblProceedToOrderInfo
            // 
            this.lblProceedToOrderInfo.AutoSize = true;
            this.lblProceedToOrderInfo.Location = new System.Drawing.Point(15, 209);
            this.lblProceedToOrderInfo.Name = "lblProceedToOrderInfo";
            this.lblProceedToOrderInfo.Size = new System.Drawing.Size(205, 15);
            this.lblProceedToOrderInfo.TabIndex = 48;
            this.lblProceedToOrderInfo.Text = "Please Proceed to Order Information...";
            // 
            // grpBoxAddOrderInfo
            // 
            this.grpBoxAddOrderInfo.Controls.Add(this.tbxAddOrderCustDestinationAddress);
            this.grpBoxAddOrderInfo.Controls.Add(this.lblAddOrderDestinationAddress);
            this.grpBoxAddOrderInfo.Controls.Add(this.tbxAddOrderNameOfCust);
            this.grpBoxAddOrderInfo.Controls.Add(this.lblAddOrderNameOfCust);
            this.grpBoxAddOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxAddOrderInfo.Location = new System.Drawing.Point(15, 26);
            this.grpBoxAddOrderInfo.Name = "grpBoxAddOrderInfo";
            this.grpBoxAddOrderInfo.Size = new System.Drawing.Size(516, 180);
            this.grpBoxAddOrderInfo.TabIndex = 47;
            this.grpBoxAddOrderInfo.TabStop = false;
            this.grpBoxAddOrderInfo.Text = "Customer Information";
            // 
            // tbxAddOrderCustDestinationAddress
            // 
            this.tbxAddOrderCustDestinationAddress.Location = new System.Drawing.Point(16, 77);
            this.tbxAddOrderCustDestinationAddress.Multiline = true;
            this.tbxAddOrderCustDestinationAddress.Name = "tbxAddOrderCustDestinationAddress";
            this.tbxAddOrderCustDestinationAddress.Size = new System.Drawing.Size(209, 48);
            this.tbxAddOrderCustDestinationAddress.TabIndex = 26;
            // 
            // lblAddOrderDestinationAddress
            // 
            this.lblAddOrderDestinationAddress.AutoSize = true;
            this.lblAddOrderDestinationAddress.Location = new System.Drawing.Point(12, 61);
            this.lblAddOrderDestinationAddress.Name = "lblAddOrderDestinationAddress";
            this.lblAddOrderDestinationAddress.Size = new System.Drawing.Size(122, 15);
            this.lblAddOrderDestinationAddress.TabIndex = 17;
            this.lblAddOrderDestinationAddress.Text = "* Destination Address:";
            // 
            // tbxAddOrderNameOfCust
            // 
            this.tbxAddOrderNameOfCust.Location = new System.Drawing.Point(16, 35);
            this.tbxAddOrderNameOfCust.Name = "tbxAddOrderNameOfCust";
            this.tbxAddOrderNameOfCust.Size = new System.Drawing.Size(209, 23);
            this.tbxAddOrderNameOfCust.TabIndex = 7;
            // 
            // lblAddOrderNameOfCust
            // 
            this.lblAddOrderNameOfCust.AutoSize = true;
            this.lblAddOrderNameOfCust.Location = new System.Drawing.Point(12, 19);
            this.lblAddOrderNameOfCust.Name = "lblAddOrderNameOfCust";
            this.lblAddOrderNameOfCust.Size = new System.Drawing.Size(113, 15);
            this.lblAddOrderNameOfCust.TabIndex = 6;
            this.lblAddOrderNameOfCust.Text = "* Name of Customer:";
            // 
            // lblMandatoryFields5
            // 
            this.lblMandatoryFields5.AutoSize = true;
            this.lblMandatoryFields5.Location = new System.Drawing.Point(6, 4);
            this.lblMandatoryFields5.Name = "lblMandatoryFields5";
            this.lblMandatoryFields5.Size = new System.Drawing.Size(146, 15);
            this.lblMandatoryFields5.TabIndex = 45;
            this.lblMandatoryFields5.Text = "* Denotes mandatory fields";
            // 
            // tabPgAddOrderInfo
            // 
            this.tabPgAddOrderInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tabPgAddOrderInfo.Controls.Add(this.btnCancelAddOrderInfo);
            this.tabPgAddOrderInfo.Controls.Add(this.btnAddOrderInfo);
            this.tabPgAddOrderInfo.Controls.Add(this.grpAddOrderInfo);
            this.tabPgAddOrderInfo.Controls.Add(this.lblMandatoryFields6);
            this.tabPgAddOrderInfo.Location = new System.Drawing.Point(4, 24);
            this.tabPgAddOrderInfo.Name = "tabPgAddOrderInfo";
            this.tabPgAddOrderInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgAddOrderInfo.Size = new System.Drawing.Size(546, 236);
            this.tabPgAddOrderInfo.TabIndex = 1;
            this.tabPgAddOrderInfo.Text = "Order Information";
            // 
            // btnCancelAddOrderInfo
            // 
            this.btnCancelAddOrderInfo.Location = new System.Drawing.Point(456, 192);
            this.btnCancelAddOrderInfo.Name = "btnCancelAddOrderInfo";
            this.btnCancelAddOrderInfo.Size = new System.Drawing.Size(75, 23);
            this.btnCancelAddOrderInfo.TabIndex = 20;
            this.btnCancelAddOrderInfo.Text = "Cancel";
            this.btnCancelAddOrderInfo.UseVisualStyleBackColor = true;
            this.btnCancelAddOrderInfo.Click += new System.EventHandler(this.btnCancelAddOrderInfo_Click);
            // 
            // btnAddOrderInfo
            // 
            this.btnAddOrderInfo.Location = new System.Drawing.Point(362, 192);
            this.btnAddOrderInfo.Name = "btnAddOrderInfo";
            this.btnAddOrderInfo.Size = new System.Drawing.Size(75, 23);
            this.btnAddOrderInfo.TabIndex = 19;
            this.btnAddOrderInfo.Text = "Add Order";
            this.btnAddOrderInfo.UseVisualStyleBackColor = true;
            this.btnAddOrderInfo.Click += new System.EventHandler(this.btnAddOrderInfo_Click);
            // 
            // grpAddOrderInfo
            // 
            this.grpAddOrderInfo.Controls.Add(this.dtpDateOfAddOrder);
            this.grpAddOrderInfo.Controls.Add(this.tbxAddOrderTotalPrice);
            this.grpAddOrderInfo.Controls.Add(this.lblAddOrderTotalPrice);
            this.grpAddOrderInfo.Controls.Add(this.lblAddDateOfOrder);
            this.grpAddOrderInfo.Controls.Add(this.tbxAddOrderRefNum);
            this.grpAddOrderInfo.Controls.Add(this.lblAddOrderRefNum);
            this.grpAddOrderInfo.Location = new System.Drawing.Point(12, 22);
            this.grpAddOrderInfo.Name = "grpAddOrderInfo";
            this.grpAddOrderInfo.Size = new System.Drawing.Size(519, 164);
            this.grpAddOrderInfo.TabIndex = 47;
            this.grpAddOrderInfo.TabStop = false;
            this.grpAddOrderInfo.Text = "Order Information";
            // 
            // dtpDateOfAddOrder
            // 
            this.dtpDateOfAddOrder.CustomFormat = "d/M/yyyy";
            this.dtpDateOfAddOrder.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateOfAddOrder.Location = new System.Drawing.Point(15, 81);
            this.dtpDateOfAddOrder.Name = "dtpDateOfAddOrder";
            this.dtpDateOfAddOrder.Size = new System.Drawing.Size(209, 23);
            this.dtpDateOfAddOrder.TabIndex = 19;
            this.dtpDateOfAddOrder.Value = new System.DateTime(2008, 7, 8, 0, 0, 0, 0);
            // 
            // tbxAddOrderTotalPrice
            // 
            this.tbxAddOrderTotalPrice.Location = new System.Drawing.Point(15, 126);
            this.tbxAddOrderTotalPrice.Name = "tbxAddOrderTotalPrice";
            this.tbxAddOrderTotalPrice.Size = new System.Drawing.Size(80, 23);
            this.tbxAddOrderTotalPrice.TabIndex = 18;
            // 
            // lblAddOrderTotalPrice
            // 
            this.lblAddOrderTotalPrice.AutoSize = true;
            this.lblAddOrderTotalPrice.Location = new System.Drawing.Point(12, 108);
            this.lblAddOrderTotalPrice.Name = "lblAddOrderTotalPrice";
            this.lblAddOrderTotalPrice.Size = new System.Drawing.Size(124, 15);
            this.lblAddOrderTotalPrice.TabIndex = 17;
            this.lblAddOrderTotalPrice.Text = "* Total Price of Order:";
            // 
            // lblAddDateOfOrder
            // 
            this.lblAddDateOfOrder.AutoSize = true;
            this.lblAddDateOfOrder.Location = new System.Drawing.Point(11, 65);
            this.lblAddDateOfOrder.Name = "lblAddDateOfOrder";
            this.lblAddDateOfOrder.Size = new System.Drawing.Size(95, 15);
            this.lblAddDateOfOrder.TabIndex = 15;
            this.lblAddDateOfOrder.Text = "* Date Of Order:";
            // 
            // tbxAddOrderRefNum
            // 
            this.tbxAddOrderRefNum.Location = new System.Drawing.Point(15, 39);
            this.tbxAddOrderRefNum.Name = "tbxAddOrderRefNum";
            this.tbxAddOrderRefNum.Size = new System.Drawing.Size(209, 23);
            this.tbxAddOrderRefNum.TabIndex = 7;
            // 
            // lblAddOrderRefNum
            // 
            this.lblAddOrderRefNum.AutoSize = true;
            this.lblAddOrderRefNum.Location = new System.Drawing.Point(11, 23);
            this.lblAddOrderRefNum.Name = "lblAddOrderRefNum";
            this.lblAddOrderRefNum.Size = new System.Drawing.Size(150, 15);
            this.lblAddOrderRefNum.TabIndex = 6;
            this.lblAddOrderRefNum.Text = "* Order Reference Number:";
            // 
            // lblMandatoryFields6
            // 
            this.lblMandatoryFields6.AutoSize = true;
            this.lblMandatoryFields6.Location = new System.Drawing.Point(6, 4);
            this.lblMandatoryFields6.Name = "lblMandatoryFields6";
            this.lblMandatoryFields6.Size = new System.Drawing.Size(146, 15);
            this.lblMandatoryFields6.TabIndex = 46;
            this.lblMandatoryFields6.Text = "* Denotes mandatory fields";
            // 
            // lblAddOrderInfo
            // 
            this.lblAddOrderInfo.AutoSize = true;
            this.lblAddOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddOrderInfo.Location = new System.Drawing.Point(2, 132);
            this.lblAddOrderInfo.Name = "lblAddOrderInfo";
            this.lblAddOrderInfo.Size = new System.Drawing.Size(192, 17);
            this.lblAddOrderInfo.TabIndex = 46;
            this.lblAddOrderInfo.Text = "Please fill in the required fields..";
            // 
            // lblAddOrder
            // 
            this.lblAddOrder.AutoSize = true;
            this.lblAddOrder.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddOrder.Location = new System.Drawing.Point(97, 60);
            this.lblAddOrder.Name = "lblAddOrder";
            this.lblAddOrder.Size = new System.Drawing.Size(264, 26);
            this.lblAddOrder.TabIndex = 45;
            this.lblAddOrder.Text = "Add New Order Information";
            // 
            // picBxLogoAddOrderPnl
            // 
            this.picBxLogoAddOrderPnl.Image = ((System.Drawing.Image)(resources.GetObject("picBxLogoAddOrderPnl.Image")));
            this.picBxLogoAddOrderPnl.Location = new System.Drawing.Point(424, 33);
            this.picBxLogoAddOrderPnl.Name = "picBxLogoAddOrderPnl";
            this.picBxLogoAddOrderPnl.Size = new System.Drawing.Size(100, 81);
            this.picBxLogoAddOrderPnl.TabIndex = 44;
            this.picBxLogoAddOrderPnl.TabStop = false;
            // 
            // pnlModifyOrderInfo
            // 
            this.pnlModifyOrderInfo.AutoScroll = true;
            this.pnlModifyOrderInfo.Controls.Add(this.lblModifyOrderInfo);
            this.pnlModifyOrderInfo.Controls.Add(this.btnCancelModifyOrderInformation);
            this.pnlModifyOrderInfo.Controls.Add(this.btnModifyOrderInformation);
            this.pnlModifyOrderInfo.Controls.Add(this.lblModifyDisplayTotalPrice);
            this.pnlModifyOrderInfo.Controls.Add(this.lblModifyDisplayDateOfOrder);
            this.pnlModifyOrderInfo.Controls.Add(this.grpModifyOrderInfo);
            this.pnlModifyOrderInfo.Controls.Add(this.lblModifyDispDestinationAddress);
            this.pnlModifyOrderInfo.Controls.Add(this.label12);
            this.pnlModifyOrderInfo.Controls.Add(this.label13);
            this.pnlModifyOrderInfo.Controls.Add(this.lblModifyDispCustomerName);
            this.pnlModifyOrderInfo.Controls.Add(this.lblModifyDispOrderRefNum);
            this.pnlModifyOrderInfo.Controls.Add(this.label4);
            this.pnlModifyOrderInfo.Controls.Add(this.label5);
            this.pnlModifyOrderInfo.Controls.Add(this.label6);
            this.pnlModifyOrderInfo.Controls.Add(this.lblModifyOrderInformation);
            this.pnlModifyOrderInfo.Controls.Add(this.picBxLogoPnlModifyOrderInfo);
            this.pnlModifyOrderInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlModifyOrderInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlModifyOrderInfo.Name = "pnlModifyOrderInfo";
            this.pnlModifyOrderInfo.Size = new System.Drawing.Size(554, 414);
            this.pnlModifyOrderInfo.TabIndex = 9;
            // 
            // lblModifyOrderInfo
            // 
            this.lblModifyOrderInfo.AutoSize = true;
            this.lblModifyOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifyOrderInfo.Location = new System.Drawing.Point(16, 207);
            this.lblModifyOrderInfo.Name = "lblModifyOrderInfo";
            this.lblModifyOrderInfo.Size = new System.Drawing.Size(268, 17);
            this.lblModifyOrderInfo.TabIndex = 66;
            this.lblModifyOrderInfo.Text = "Please fill in the respective field(s) to modify..";
            // 
            // btnCancelModifyOrderInformation
            // 
            this.btnCancelModifyOrderInformation.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelModifyOrderInformation.Location = new System.Drawing.Point(448, 377);
            this.btnCancelModifyOrderInformation.Name = "btnCancelModifyOrderInformation";
            this.btnCancelModifyOrderInformation.Size = new System.Drawing.Size(75, 23);
            this.btnCancelModifyOrderInformation.TabIndex = 65;
            this.btnCancelModifyOrderInformation.Text = "Cancel";
            this.btnCancelModifyOrderInformation.UseVisualStyleBackColor = true;
            this.btnCancelModifyOrderInformation.Click += new System.EventHandler(this.btnCancelModifyOrderInformation_Click);
            // 
            // btnModifyOrderInformation
            // 
            this.btnModifyOrderInformation.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyOrderInformation.Location = new System.Drawing.Point(356, 378);
            this.btnModifyOrderInformation.Name = "btnModifyOrderInformation";
            this.btnModifyOrderInformation.Size = new System.Drawing.Size(75, 23);
            this.btnModifyOrderInformation.TabIndex = 64;
            this.btnModifyOrderInformation.Text = "Modify";
            this.btnModifyOrderInformation.UseVisualStyleBackColor = true;
            this.btnModifyOrderInformation.Click += new System.EventHandler(this.btnModifyOrderInformation_Click);
            // 
            // lblModifyDisplayTotalPrice
            // 
            this.lblModifyDisplayTotalPrice.AutoSize = true;
            this.lblModifyDisplayTotalPrice.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifyDisplayTotalPrice.Location = new System.Drawing.Point(258, 171);
            this.lblModifyDisplayTotalPrice.Name = "lblModifyDisplayTotalPrice";
            this.lblModifyDisplayTotalPrice.Size = new System.Drawing.Size(0, 16);
            this.lblModifyDisplayTotalPrice.TabIndex = 62;
            // 
            // lblModifyDisplayDateOfOrder
            // 
            this.lblModifyDisplayDateOfOrder.AutoSize = true;
            this.lblModifyDisplayDateOfOrder.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifyDisplayDateOfOrder.Location = new System.Drawing.Point(258, 154);
            this.lblModifyDisplayDateOfOrder.Name = "lblModifyDisplayDateOfOrder";
            this.lblModifyDisplayDateOfOrder.Size = new System.Drawing.Size(0, 16);
            this.lblModifyDisplayDateOfOrder.TabIndex = 61;
            // 
            // grpModifyOrderInfo
            // 
            this.grpModifyOrderInfo.Controls.Add(this.dtpModifyOrderDate);
            this.grpModifyOrderInfo.Controls.Add(this.tbxModifyOrderTotalPrice);
            this.grpModifyOrderInfo.Controls.Add(this.lblModifyOrderTotalPrice);
            this.grpModifyOrderInfo.Controls.Add(this.lblModifyOrderDate);
            this.grpModifyOrderInfo.Controls.Add(this.lblModifyDestinationAddress);
            this.grpModifyOrderInfo.Controls.Add(this.tbxModifyDestinationAddress);
            this.grpModifyOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpModifyOrderInfo.Location = new System.Drawing.Point(16, 230);
            this.grpModifyOrderInfo.Name = "grpModifyOrderInfo";
            this.grpModifyOrderInfo.Size = new System.Drawing.Size(507, 141);
            this.grpModifyOrderInfo.TabIndex = 47;
            this.grpModifyOrderInfo.TabStop = false;
            this.grpModifyOrderInfo.Text = "Modify Order Information";
            // 
            // dtpModifyOrderDate
            // 
            this.dtpModifyOrderDate.CustomFormat = "d/M/yyyy";
            this.dtpModifyOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpModifyOrderDate.Location = new System.Drawing.Point(22, 39);
            this.dtpModifyOrderDate.Name = "dtpModifyOrderDate";
            this.dtpModifyOrderDate.Size = new System.Drawing.Size(209, 23);
            this.dtpModifyOrderDate.TabIndex = 33;
            this.dtpModifyOrderDate.Value = new System.DateTime(2008, 7, 9, 0, 0, 0, 0);
            // 
            // tbxModifyOrderTotalPrice
            // 
            this.tbxModifyOrderTotalPrice.Location = new System.Drawing.Point(23, 107);
            this.tbxModifyOrderTotalPrice.Name = "tbxModifyOrderTotalPrice";
            this.tbxModifyOrderTotalPrice.Size = new System.Drawing.Size(80, 23);
            this.tbxModifyOrderTotalPrice.TabIndex = 32;
            // 
            // lblModifyOrderTotalPrice
            // 
            this.lblModifyOrderTotalPrice.AutoSize = true;
            this.lblModifyOrderTotalPrice.Location = new System.Drawing.Point(20, 89);
            this.lblModifyOrderTotalPrice.Name = "lblModifyOrderTotalPrice";
            this.lblModifyOrderTotalPrice.Size = new System.Drawing.Size(115, 15);
            this.lblModifyOrderTotalPrice.TabIndex = 31;
            this.lblModifyOrderTotalPrice.Text = "Total Price of Order:";
            // 
            // lblModifyOrderDate
            // 
            this.lblModifyOrderDate.AutoSize = true;
            this.lblModifyOrderDate.Location = new System.Drawing.Point(20, 22);
            this.lblModifyOrderDate.Name = "lblModifyOrderDate";
            this.lblModifyOrderDate.Size = new System.Drawing.Size(86, 15);
            this.lblModifyOrderDate.TabIndex = 10;
            this.lblModifyOrderDate.Text = "Date Of Order:";
            // 
            // lblModifyDestinationAddress
            // 
            this.lblModifyDestinationAddress.AutoSize = true;
            this.lblModifyDestinationAddress.Location = new System.Drawing.Point(273, 19);
            this.lblModifyDestinationAddress.Name = "lblModifyDestinationAddress";
            this.lblModifyDestinationAddress.Size = new System.Drawing.Size(113, 15);
            this.lblModifyDestinationAddress.TabIndex = 17;
            this.lblModifyDestinationAddress.Text = "Destination Address:";
            // 
            // tbxModifyDestinationAddress
            // 
            this.tbxModifyDestinationAddress.Location = new System.Drawing.Point(275, 36);
            this.tbxModifyDestinationAddress.Multiline = true;
            this.tbxModifyDestinationAddress.Name = "tbxModifyDestinationAddress";
            this.tbxModifyDestinationAddress.Size = new System.Drawing.Size(209, 48);
            this.tbxModifyDestinationAddress.TabIndex = 26;
            // 
            // lblModifyDispDestinationAddress
            // 
            this.lblModifyDispDestinationAddress.AutoSize = true;
            this.lblModifyDispDestinationAddress.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifyDispDestinationAddress.Location = new System.Drawing.Point(258, 186);
            this.lblModifyDispDestinationAddress.Name = "lblModifyDispDestinationAddress";
            this.lblModifyDispDestinationAddress.Size = new System.Drawing.Size(0, 16);
            this.lblModifyDispDestinationAddress.TabIndex = 60;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(114, 179);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(136, 16);
            this.label12.TabIndex = 57;
            this.label12.Tag = "";
            this.label12.Text = "Destination Address :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(168, 162);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 16);
            this.label13.TabIndex = 56;
            this.label13.Tag = "";
            this.label13.Text = "Total Price :";
            // 
            // lblModifyDispCustomerName
            // 
            this.lblModifyDispCustomerName.AutoSize = true;
            this.lblModifyDispCustomerName.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifyDispCustomerName.Location = new System.Drawing.Point(258, 139);
            this.lblModifyDispCustomerName.Name = "lblModifyDispCustomerName";
            this.lblModifyDispCustomerName.Size = new System.Drawing.Size(0, 16);
            this.lblModifyDispCustomerName.TabIndex = 54;
            // 
            // lblModifyDispOrderRefNum
            // 
            this.lblModifyDispOrderRefNum.AutoSize = true;
            this.lblModifyDispOrderRefNum.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifyDispOrderRefNum.Location = new System.Drawing.Point(258, 120);
            this.lblModifyDispOrderRefNum.Name = "lblModifyDispOrderRefNum";
            this.lblModifyDispOrderRefNum.Size = new System.Drawing.Size(0, 16);
            this.lblModifyDispOrderRefNum.TabIndex = 53;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(141, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 16);
            this.label4.TabIndex = 52;
            this.label4.Tag = "";
            this.label4.Text = "Date Of Order :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(141, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 16);
            this.label5.TabIndex = 51;
            this.label5.Tag = "";
            this.label5.Text = "Customer Name :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(81, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(169, 16);
            this.label6.TabIndex = 50;
            this.label6.Tag = "";
            this.label6.Text = "Order Reference Number :";
            // 
            // lblModifyOrderInformation
            // 
            this.lblModifyOrderInformation.AutoSize = true;
            this.lblModifyOrderInformation.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifyOrderInformation.Location = new System.Drawing.Point(115, 60);
            this.lblModifyOrderInformation.Name = "lblModifyOrderInformation";
            this.lblModifyOrderInformation.Size = new System.Drawing.Size(246, 26);
            this.lblModifyOrderInformation.TabIndex = 44;
            this.lblModifyOrderInformation.Text = "Modify Order Information";
            // 
            // picBxLogoPnlModifyOrderInfo
            // 
            this.picBxLogoPnlModifyOrderInfo.Image = ((System.Drawing.Image)(resources.GetObject("picBxLogoPnlModifyOrderInfo.Image")));
            this.picBxLogoPnlModifyOrderInfo.Location = new System.Drawing.Point(424, 33);
            this.picBxLogoPnlModifyOrderInfo.Name = "picBxLogoPnlModifyOrderInfo";
            this.picBxLogoPnlModifyOrderInfo.Size = new System.Drawing.Size(100, 81);
            this.picBxLogoPnlModifyOrderInfo.TabIndex = 43;
            this.picBxLogoPnlModifyOrderInfo.TabStop = false;
            // 
            // panelAddNewCategory
            // 
            this.panelAddNewCategory.Controls.Add(this.pictureBox12);
            this.panelAddNewCategory.Controls.Add(this.label1);
            this.panelAddNewCategory.Controls.Add(this.btnCancelAddCategory);
            this.panelAddNewCategory.Controls.Add(this.groupBox1);
            this.panelAddNewCategory.Controls.Add(this.btnAddCategory);
            this.panelAddNewCategory.Controls.Add(this.label7);
            this.panelAddNewCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAddNewCategory.Location = new System.Drawing.Point(0, 0);
            this.panelAddNewCategory.Name = "panelAddNewCategory";
            this.panelAddNewCategory.Size = new System.Drawing.Size(554, 414);
            this.panelAddNewCategory.TabIndex = 12;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(424, 33);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(100, 81);
            this.pictureBox12.TabIndex = 41;
            this.pictureBox12.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Please fill in the Descrption field ...";
            // 
            // btnCancelAddCategory
            // 
            this.btnCancelAddCategory.Location = new System.Drawing.Point(455, 379);
            this.btnCancelAddCategory.Name = "btnCancelAddCategory";
            this.btnCancelAddCategory.Size = new System.Drawing.Size(75, 23);
            this.btnCancelAddCategory.TabIndex = 8;
            this.btnCancelAddCategory.Text = "Cancel";
            this.btnCancelAddCategory.UseVisualStyleBackColor = true;
            this.btnCancelAddCategory.Click += new System.EventHandler(this.btnCancelAddCategory_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblAddCategoryName);
            this.groupBox1.Controls.Add(this.tbxAddCategoryDescription);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(26, 163);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(504, 210);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Category Information";
            // 
            // lblAddCategoryName
            // 
            this.lblAddCategoryName.AutoSize = true;
            this.lblAddCategoryName.Location = new System.Drawing.Point(23, 43);
            this.lblAddCategoryName.Name = "lblAddCategoryName";
            this.lblAddCategoryName.Size = new System.Drawing.Size(155, 15);
            this.lblAddCategoryName.TabIndex = 7;
            this.lblAddCategoryName.Text = "- Display of Category Name -";
            // 
            // tbxAddCategoryDescription
            // 
            this.tbxAddCategoryDescription.Location = new System.Drawing.Point(26, 85);
            this.tbxAddCategoryDescription.Multiline = true;
            this.tbxAddCategoryDescription.Name = "tbxAddCategoryDescription";
            this.tbxAddCategoryDescription.Size = new System.Drawing.Size(311, 107);
            this.tbxAddCategoryDescription.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Category Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Descrption:";
            // 
            // btnAddCategory
            // 
            this.btnAddCategory.Location = new System.Drawing.Point(364, 379);
            this.btnAddCategory.Name = "btnAddCategory";
            this.btnAddCategory.Size = new System.Drawing.Size(75, 23);
            this.btnAddCategory.TabIndex = 7;
            this.btnAddCategory.Text = "Add";
            this.btnAddCategory.UseVisualStyleBackColor = true;
            this.btnAddCategory.Click += new System.EventHandler(this.btnAddCategory_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(95, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(288, 26);
            this.label7.TabIndex = 2;
            this.label7.Text = "Add New Category Information";
            // 
            // panelModifyCategory
            // 
            this.panelModifyCategory.Controls.Add(this.label14);
            this.panelModifyCategory.Controls.Add(this.btnCancelModifyCategory);
            this.panelModifyCategory.Controls.Add(this.groupBox2);
            this.panelModifyCategory.Controls.Add(this.btnModifyCategory);
            this.panelModifyCategory.Controls.Add(this.label10);
            this.panelModifyCategory.Controls.Add(this.pictureBox1);
            this.panelModifyCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelModifyCategory.Location = new System.Drawing.Point(0, 0);
            this.panelModifyCategory.Name = "panelModifyCategory";
            this.panelModifyCategory.Size = new System.Drawing.Size(554, 414);
            this.panelModifyCategory.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(24, 144);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(198, 17);
            this.label14.TabIndex = 11;
            this.label14.Text = "Please edit the Descrption field ...";
            // 
            // btnCancelModifyCategory
            // 
            this.btnCancelModifyCategory.Location = new System.Drawing.Point(455, 379);
            this.btnCancelModifyCategory.Name = "btnCancelModifyCategory";
            this.btnCancelModifyCategory.Size = new System.Drawing.Size(75, 23);
            this.btnCancelModifyCategory.TabIndex = 8;
            this.btnCancelModifyCategory.Text = "Cancel";
            this.btnCancelModifyCategory.UseVisualStyleBackColor = true;
            this.btnCancelModifyCategory.Click += new System.EventHandler(this.btnCancelModifyCategory_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblModifyCategoryName);
            this.groupBox2.Controls.Add(this.tbxModifyCategoryDescrption);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(26, 163);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(504, 210);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Category Information";
            // 
            // lblModifyCategoryName
            // 
            this.lblModifyCategoryName.AutoSize = true;
            this.lblModifyCategoryName.Location = new System.Drawing.Point(23, 43);
            this.lblModifyCategoryName.Name = "lblModifyCategoryName";
            this.lblModifyCategoryName.Size = new System.Drawing.Size(155, 15);
            this.lblModifyCategoryName.TabIndex = 7;
            this.lblModifyCategoryName.Text = "- Display of Category Name -";
            // 
            // tbxModifyCategoryDescrption
            // 
            this.tbxModifyCategoryDescrption.Location = new System.Drawing.Point(26, 85);
            this.tbxModifyCategoryDescrption.Multiline = true;
            this.tbxModifyCategoryDescrption.Name = "tbxModifyCategoryDescrption";
            this.tbxModifyCategoryDescrption.Size = new System.Drawing.Size(311, 107);
            this.tbxModifyCategoryDescrption.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(23, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 15);
            this.label8.TabIndex = 3;
            this.label8.Text = "Category Name:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 15);
            this.label9.TabIndex = 4;
            this.label9.Text = "Descrption:";
            // 
            // btnModifyCategory
            // 
            this.btnModifyCategory.Location = new System.Drawing.Point(364, 379);
            this.btnModifyCategory.Name = "btnModifyCategory";
            this.btnModifyCategory.Size = new System.Drawing.Size(75, 23);
            this.btnModifyCategory.TabIndex = 7;
            this.btnModifyCategory.Text = "Update";
            this.btnModifyCategory.UseVisualStyleBackColor = true;
            this.btnModifyCategory.Click += new System.EventHandler(this.btnModifyCategory_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(107, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(270, 26);
            this.label10.TabIndex = 2;
            this.label10.Text = "Modify Category Information";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(424, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 81);
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            // 
            // panelDeleteCategory
            // 
            this.panelDeleteCategory.Controls.Add(this.label15);
            this.panelDeleteCategory.Controls.Add(this.btnCancelDeleteCategory);
            this.panelDeleteCategory.Controls.Add(this.groupBox3);
            this.panelDeleteCategory.Controls.Add(this.btnDeleteCategory);
            this.panelDeleteCategory.Controls.Add(this.label17);
            this.panelDeleteCategory.Controls.Add(this.pictureBox2);
            this.panelDeleteCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDeleteCategory.Location = new System.Drawing.Point(0, 0);
            this.panelDeleteCategory.Name = "panelDeleteCategory";
            this.panelDeleteCategory.Size = new System.Drawing.Size(554, 414);
            this.panelDeleteCategory.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(24, 144);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(275, 17);
            this.label15.TabIndex = 12;
            this.label15.Text = "Please confirm category information to delete ...";
            // 
            // btnCancelDeleteCategory
            // 
            this.btnCancelDeleteCategory.Location = new System.Drawing.Point(455, 379);
            this.btnCancelDeleteCategory.Name = "btnCancelDeleteCategory";
            this.btnCancelDeleteCategory.Size = new System.Drawing.Size(75, 23);
            this.btnCancelDeleteCategory.TabIndex = 8;
            this.btnCancelDeleteCategory.Text = "Cancel";
            this.btnCancelDeleteCategory.UseVisualStyleBackColor = true;
            this.btnCancelDeleteCategory.Click += new System.EventHandler(this.btnCancelDeleteCategory_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblDeleteCategoryName);
            this.groupBox3.Controls.Add(this.tbxDeleteCategoryDescrption);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(26, 163);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(504, 210);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Category Information";
            // 
            // lblDeleteCategoryName
            // 
            this.lblDeleteCategoryName.AutoSize = true;
            this.lblDeleteCategoryName.Location = new System.Drawing.Point(23, 43);
            this.lblDeleteCategoryName.Name = "lblDeleteCategoryName";
            this.lblDeleteCategoryName.Size = new System.Drawing.Size(155, 15);
            this.lblDeleteCategoryName.TabIndex = 7;
            this.lblDeleteCategoryName.Text = "- Display of Category Name -";
            // 
            // tbxDeleteCategoryDescrption
            // 
            this.tbxDeleteCategoryDescrption.Location = new System.Drawing.Point(26, 85);
            this.tbxDeleteCategoryDescrption.Multiline = true;
            this.tbxDeleteCategoryDescrption.Name = "tbxDeleteCategoryDescrption";
            this.tbxDeleteCategoryDescrption.ReadOnly = true;
            this.tbxDeleteCategoryDescrption.Size = new System.Drawing.Size(311, 107);
            this.tbxDeleteCategoryDescrption.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(23, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 15);
            this.label11.TabIndex = 3;
            this.label11.Text = "Category Name:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 15);
            this.label16.TabIndex = 4;
            this.label16.Text = "Descrption:";
            // 
            // btnDeleteCategory
            // 
            this.btnDeleteCategory.Location = new System.Drawing.Point(364, 379);
            this.btnDeleteCategory.Name = "btnDeleteCategory";
            this.btnDeleteCategory.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteCategory.TabIndex = 7;
            this.btnDeleteCategory.Text = "Delete";
            this.btnDeleteCategory.UseVisualStyleBackColor = true;
            this.btnDeleteCategory.Click += new System.EventHandler(this.btnDeleteCategory_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(109, 60);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(265, 26);
            this.label17.TabIndex = 2;
            this.label17.Text = "Delete Category Information";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(424, 33);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 81);
            this.pictureBox2.TabIndex = 41;
            this.pictureBox2.TabStop = false;
            // 
            // panelDisplayCategory
            // 
            this.panelDisplayCategory.Controls.Add(this.btnDisplayCategory);
            this.panelDisplayCategory.Controls.Add(this.groupBox5);
            this.panelDisplayCategory.Controls.Add(this.label25);
            this.panelDisplayCategory.Controls.Add(this.pictureBox4);
            this.panelDisplayCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDisplayCategory.Location = new System.Drawing.Point(0, 0);
            this.panelDisplayCategory.Name = "panelDisplayCategory";
            this.panelDisplayCategory.Size = new System.Drawing.Size(554, 414);
            this.panelDisplayCategory.TabIndex = 16;
            // 
            // btnDisplayCategory
            // 
            this.btnDisplayCategory.Location = new System.Drawing.Point(455, 379);
            this.btnDisplayCategory.Name = "btnDisplayCategory";
            this.btnDisplayCategory.Size = new System.Drawing.Size(75, 23);
            this.btnDisplayCategory.TabIndex = 8;
            this.btnDisplayCategory.Text = "OK";
            this.btnDisplayCategory.UseVisualStyleBackColor = true;
            this.btnDisplayCategory.Click += new System.EventHandler(this.btnDisplayCategory_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblDisplayCategoryName);
            this.groupBox5.Controls.Add(this.tbxDisplayCategoryDescrption);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(26, 163);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(504, 210);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Category Information";
            // 
            // lblDisplayCategoryName
            // 
            this.lblDisplayCategoryName.AutoSize = true;
            this.lblDisplayCategoryName.Location = new System.Drawing.Point(23, 43);
            this.lblDisplayCategoryName.Name = "lblDisplayCategoryName";
            this.lblDisplayCategoryName.Size = new System.Drawing.Size(155, 15);
            this.lblDisplayCategoryName.TabIndex = 7;
            this.lblDisplayCategoryName.Text = "- Display of Category Name -";
            // 
            // tbxDisplayCategoryDescrption
            // 
            this.tbxDisplayCategoryDescrption.Location = new System.Drawing.Point(26, 85);
            this.tbxDisplayCategoryDescrption.Multiline = true;
            this.tbxDisplayCategoryDescrption.Name = "tbxDisplayCategoryDescrption";
            this.tbxDisplayCategoryDescrption.ReadOnly = true;
            this.tbxDisplayCategoryDescrption.Size = new System.Drawing.Size(311, 107);
            this.tbxDisplayCategoryDescrption.TabIndex = 6;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(23, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(88, 15);
            this.label23.TabIndex = 3;
            this.label23.Text = "Category Name:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(23, 67);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(66, 15);
            this.label24.TabIndex = 4;
            this.label24.Text = "Descrption:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(94, 60);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(270, 26);
            this.label25.TabIndex = 2;
            this.label25.Text = "Display Category Information";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(424, 33);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 81);
            this.pictureBox4.TabIndex = 41;
            this.pictureBox4.TabStop = false;
            // 
            // panelAddNewProduct
            // 
            this.panelAddNewProduct.Controls.Add(this.label26);
            this.panelAddNewProduct.Controls.Add(this.label27);
            this.panelAddNewProduct.Controls.Add(this.tabControlAddNewProduct);
            this.panelAddNewProduct.Controls.Add(this.pictureBox5);
            this.panelAddNewProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAddNewProduct.Location = new System.Drawing.Point(0, 0);
            this.panelAddNewProduct.Name = "panelAddNewProduct";
            this.panelAddNewProduct.Size = new System.Drawing.Size(554, 414);
            this.panelAddNewProduct.TabIndex = 17;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(2, 130);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(196, 17);
            this.label26.TabIndex = 10;
            this.label26.Text = "Please fill in the required fields ..";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(97, 60);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(278, 26);
            this.label27.TabIndex = 2;
            this.label27.Text = "Add New Product Information";
            // 
            // tabControlAddNewProduct
            // 
            this.tabControlAddNewProduct.Controls.Add(this.tabAddNewProductInfo1);
            this.tabControlAddNewProduct.Controls.Add(this.tabAddNewProductInfo2);
            this.tabControlAddNewProduct.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlAddNewProduct.Location = new System.Drawing.Point(0, 149);
            this.tabControlAddNewProduct.Name = "tabControlAddNewProduct";
            this.tabControlAddNewProduct.SelectedIndex = 0;
            this.tabControlAddNewProduct.Size = new System.Drawing.Size(554, 264);
            this.tabControlAddNewProduct.TabIndex = 11;
            // 
            // tabAddNewProductInfo1
            // 
            this.tabAddNewProductInfo1.BackColor = System.Drawing.SystemColors.Control;
            this.tabAddNewProductInfo1.Controls.Add(this.label28);
            this.tabAddNewProductInfo1.Controls.Add(this.label29);
            this.tabAddNewProductInfo1.Controls.Add(this.groupBox6);
            this.tabAddNewProductInfo1.Location = new System.Drawing.Point(4, 24);
            this.tabAddNewProductInfo1.Name = "tabAddNewProductInfo1";
            this.tabAddNewProductInfo1.Padding = new System.Windows.Forms.Padding(3);
            this.tabAddNewProductInfo1.Size = new System.Drawing.Size(546, 236);
            this.tabAddNewProductInfo1.TabIndex = 0;
            this.tabAddNewProductInfo1.Text = "Product Information";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(17, 212);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(405, 15);
            this.label28.TabIndex = 3;
            this.label28.Text = "Please proceed to fill in required fields in the \'Product Price && Image\' section" +
                "";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(17, 8);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(146, 15);
            this.label29.TabIndex = 1;
            this.label29.Text = "* Denotes mandatory fields";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lblAddProductModelNumber);
            this.groupBox6.Controls.Add(this.cbxAddProductCategory);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.tbxAddProductDescription);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this.lblAddProductName);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Location = new System.Drawing.Point(20, 26);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(506, 173);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Product Information";
            // 
            // lblAddProductModelNumber
            // 
            this.lblAddProductModelNumber.AutoSize = true;
            this.lblAddProductModelNumber.Location = new System.Drawing.Point(16, 90);
            this.lblAddProductModelNumber.Name = "lblAddProductModelNumber";
            this.lblAddProductModelNumber.Size = new System.Drawing.Size(168, 15);
            this.lblAddProductModelNumber.TabIndex = 10;
            this.lblAddProductModelNumber.Text = "- Display of Product Model No -";
            // 
            // cbxAddProductCategory
            // 
            this.cbxAddProductCategory.FormattingEnabled = true;
            this.cbxAddProductCategory.Location = new System.Drawing.Point(22, 134);
            this.cbxAddProductCategory.Name = "cbxAddProductCategory";
            this.cbxAddProductCategory.Size = new System.Drawing.Size(142, 23);
            this.cbxAddProductCategory.TabIndex = 9;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(19, 116);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(65, 15);
            this.label31.TabIndex = 6;
            this.label31.Text = "* Category:";
            // 
            // tbxAddProductDescription
            // 
            this.tbxAddProductDescription.Location = new System.Drawing.Point(242, 37);
            this.tbxAddProductDescription.Multiline = true;
            this.tbxAddProductDescription.Name = "tbxAddProductDescription";
            this.tbxAddProductDescription.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.tbxAddProductDescription.Size = new System.Drawing.Size(248, 120);
            this.tbxAddProductDescription.TabIndex = 5;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(239, 19);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(79, 15);
            this.label32.TabIndex = 4;
            this.label32.Text = "* Description:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(19, 65);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(93, 15);
            this.label33.TabIndex = 2;
            this.label33.Text = "* Model Number:";
            // 
            // lblAddProductName
            // 
            this.lblAddProductName.AutoSize = true;
            this.lblAddProductName.Location = new System.Drawing.Point(19, 37);
            this.lblAddProductName.Name = "lblAddProductName";
            this.lblAddProductName.Size = new System.Drawing.Size(148, 15);
            this.lblAddProductName.TabIndex = 1;
            this.lblAddProductName.Text = "- Display of Product Name -";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(19, 19);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(39, 15);
            this.label34.TabIndex = 0;
            this.label34.Text = "Name:";
            // 
            // tabAddNewProductInfo2
            // 
            this.tabAddNewProductInfo2.BackColor = System.Drawing.SystemColors.Control;
            this.tabAddNewProductInfo2.Controls.Add(this.groupBox7);
            this.tabAddNewProductInfo2.Controls.Add(this.btnAddProduct);
            this.tabAddNewProductInfo2.Controls.Add(this.btnCancelAddProduct);
            this.tabAddNewProductInfo2.Controls.Add(this.label65);
            this.tabAddNewProductInfo2.Location = new System.Drawing.Point(4, 24);
            this.tabAddNewProductInfo2.Name = "tabAddNewProductInfo2";
            this.tabAddNewProductInfo2.Padding = new System.Windows.Forms.Padding(3);
            this.tabAddNewProductInfo2.Size = new System.Drawing.Size(546, 236);
            this.tabAddNewProductInfo2.TabIndex = 1;
            this.tabAddNewProductInfo2.Text = "Product Price & Image";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnAddProductEnlargeImage2);
            this.groupBox7.Controls.Add(this.btnAddProductEnlargeImage1);
            this.groupBox7.Controls.Add(this.btnAddProductRemoveImage2);
            this.groupBox7.Controls.Add(this.btnAddProductOpenImage2);
            this.groupBox7.Controls.Add(this.btnAddProductRemoveImage1);
            this.groupBox7.Controls.Add(this.btnAddProductOpenImage1);
            this.groupBox7.Controls.Add(this.label36);
            this.groupBox7.Controls.Add(this.picbxAddProductImage2);
            this.groupBox7.Controls.Add(this.picbxAddProductImage1);
            this.groupBox7.Controls.Add(this.tbxAddProductRetailPrice);
            this.groupBox7.Controls.Add(this.tbxAddProductMSRP);
            this.groupBox7.Controls.Add(this.label37);
            this.groupBox7.Controls.Add(this.label38);
            this.groupBox7.Location = new System.Drawing.Point(20, 26);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(506, 173);
            this.groupBox7.TabIndex = 9;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Product Information";
            // 
            // btnAddProductEnlargeImage2
            // 
            this.btnAddProductEnlargeImage2.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProductEnlargeImage2.Location = new System.Drawing.Point(382, 141);
            this.btnAddProductEnlargeImage2.Name = "btnAddProductEnlargeImage2";
            this.btnAddProductEnlargeImage2.Size = new System.Drawing.Size(47, 23);
            this.btnAddProductEnlargeImage2.TabIndex = 20;
            this.btnAddProductEnlargeImage2.Text = "Enlarge";
            this.btnAddProductEnlargeImage2.UseVisualStyleBackColor = true;
            this.btnAddProductEnlargeImage2.Click += new System.EventHandler(this.btnAddProductEnlargeImage2_Click);
            // 
            // btnAddProductEnlargeImage1
            // 
            this.btnAddProductEnlargeImage1.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProductEnlargeImage1.Location = new System.Drawing.Point(255, 141);
            this.btnAddProductEnlargeImage1.Name = "btnAddProductEnlargeImage1";
            this.btnAddProductEnlargeImage1.Size = new System.Drawing.Size(47, 23);
            this.btnAddProductEnlargeImage1.TabIndex = 19;
            this.btnAddProductEnlargeImage1.Text = "Enlarge";
            this.btnAddProductEnlargeImage1.UseVisualStyleBackColor = true;
            this.btnAddProductEnlargeImage1.Click += new System.EventHandler(this.btnAddProductEnlargeImage1_Click);
            // 
            // btnAddProductRemoveImage2
            // 
            this.btnAddProductRemoveImage2.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProductRemoveImage2.Location = new System.Drawing.Point(433, 115);
            this.btnAddProductRemoveImage2.Name = "btnAddProductRemoveImage2";
            this.btnAddProductRemoveImage2.Size = new System.Drawing.Size(47, 23);
            this.btnAddProductRemoveImage2.TabIndex = 18;
            this.btnAddProductRemoveImage2.Text = "Remove";
            this.btnAddProductRemoveImage2.UseVisualStyleBackColor = true;
            this.btnAddProductRemoveImage2.Click += new System.EventHandler(this.btnAddProductRemoveImage2_Click);
            // 
            // btnAddProductOpenImage2
            // 
            this.btnAddProductOpenImage2.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProductOpenImage2.Location = new System.Drawing.Point(382, 115);
            this.btnAddProductOpenImage2.Name = "btnAddProductOpenImage2";
            this.btnAddProductOpenImage2.Size = new System.Drawing.Size(47, 23);
            this.btnAddProductOpenImage2.TabIndex = 17;
            this.btnAddProductOpenImage2.Text = "Open";
            this.btnAddProductOpenImage2.UseVisualStyleBackColor = true;
            this.btnAddProductOpenImage2.Click += new System.EventHandler(this.btnAddProductOpenImage2_Click);
            // 
            // btnAddProductRemoveImage1
            // 
            this.btnAddProductRemoveImage1.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProductRemoveImage1.Location = new System.Drawing.Point(306, 115);
            this.btnAddProductRemoveImage1.Name = "btnAddProductRemoveImage1";
            this.btnAddProductRemoveImage1.Size = new System.Drawing.Size(47, 23);
            this.btnAddProductRemoveImage1.TabIndex = 16;
            this.btnAddProductRemoveImage1.Text = "Remove";
            this.btnAddProductRemoveImage1.UseVisualStyleBackColor = true;
            this.btnAddProductRemoveImage1.Click += new System.EventHandler(this.btnAddProductRemoveImage1_Click);
            // 
            // btnAddProductOpenImage1
            // 
            this.btnAddProductOpenImage1.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProductOpenImage1.Location = new System.Drawing.Point(255, 115);
            this.btnAddProductOpenImage1.Name = "btnAddProductOpenImage1";
            this.btnAddProductOpenImage1.Size = new System.Drawing.Size(47, 23);
            this.btnAddProductOpenImage1.TabIndex = 14;
            this.btnAddProductOpenImage1.Text = "Open";
            this.btnAddProductOpenImage1.UseVisualStyleBackColor = true;
            this.btnAddProductOpenImage1.Click += new System.EventHandler(this.btnAddProductOpenImage1_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(252, 19);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(47, 15);
            this.label36.TabIndex = 13;
            this.label36.Text = "Images:";
            // 
            // picbxAddProductImage2
            // 
            this.picbxAddProductImage2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picbxAddProductImage2.Location = new System.Drawing.Point(384, 38);
            this.picbxAddProductImage2.Name = "picbxAddProductImage2";
            this.picbxAddProductImage2.Size = new System.Drawing.Size(97, 71);
            this.picbxAddProductImage2.TabIndex = 12;
            this.picbxAddProductImage2.TabStop = false;
            // 
            // picbxAddProductImage1
            // 
            this.picbxAddProductImage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picbxAddProductImage1.Location = new System.Drawing.Point(255, 38);
            this.picbxAddProductImage1.Name = "picbxAddProductImage1";
            this.picbxAddProductImage1.Size = new System.Drawing.Size(97, 71);
            this.picbxAddProductImage1.TabIndex = 11;
            this.picbxAddProductImage1.TabStop = false;
            // 
            // tbxAddProductRetailPrice
            // 
            this.tbxAddProductRetailPrice.Location = new System.Drawing.Point(22, 86);
            this.tbxAddProductRetailPrice.Name = "tbxAddProductRetailPrice";
            this.tbxAddProductRetailPrice.Size = new System.Drawing.Size(142, 23);
            this.tbxAddProductRetailPrice.TabIndex = 10;
            // 
            // tbxAddProductMSRP
            // 
            this.tbxAddProductMSRP.Location = new System.Drawing.Point(22, 38);
            this.tbxAddProductMSRP.Name = "tbxAddProductMSRP";
            this.tbxAddProductMSRP.Size = new System.Drawing.Size(142, 23);
            this.tbxAddProductMSRP.TabIndex = 3;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(19, 65);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(79, 15);
            this.label37.TabIndex = 2;
            this.label37.Text = "* Retail Price:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(19, 19);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(50, 15);
            this.label38.TabIndex = 0;
            this.label38.Text = "* MSRP:";
            // 
            // btnAddProduct
            // 
            this.btnAddProduct.Location = new System.Drawing.Point(360, 208);
            this.btnAddProduct.Name = "btnAddProduct";
            this.btnAddProduct.Size = new System.Drawing.Size(75, 23);
            this.btnAddProduct.TabIndex = 7;
            this.btnAddProduct.Text = "Add";
            this.btnAddProduct.UseVisualStyleBackColor = true;
            this.btnAddProduct.Click += new System.EventHandler(this.btnAddProduct_Click);
            // 
            // btnCancelAddProduct
            // 
            this.btnCancelAddProduct.Location = new System.Drawing.Point(451, 208);
            this.btnCancelAddProduct.Name = "btnCancelAddProduct";
            this.btnCancelAddProduct.Size = new System.Drawing.Size(75, 23);
            this.btnCancelAddProduct.TabIndex = 8;
            this.btnCancelAddProduct.Text = "Cancel";
            this.btnCancelAddProduct.UseVisualStyleBackColor = true;
            this.btnCancelAddProduct.Click += new System.EventHandler(this.btnCancelAddProduct_Click);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(17, 8);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(146, 15);
            this.label65.TabIndex = 11;
            this.label65.Text = "* Denotes mandatory fields";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(424, 33);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(100, 81);
            this.pictureBox5.TabIndex = 41;
            this.pictureBox5.TabStop = false;
            // 
            // panelModifyProduct
            // 
            this.panelModifyProduct.Controls.Add(this.label39);
            this.panelModifyProduct.Controls.Add(this.label40);
            this.panelModifyProduct.Controls.Add(this.tabControlModifyProduct);
            this.panelModifyProduct.Controls.Add(this.pictureBox6);
            this.panelModifyProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelModifyProduct.Location = new System.Drawing.Point(0, 0);
            this.panelModifyProduct.Name = "panelModifyProduct";
            this.panelModifyProduct.Size = new System.Drawing.Size(554, 414);
            this.panelModifyProduct.TabIndex = 18;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(2, 130);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(196, 17);
            this.label39.TabIndex = 10;
            this.label39.Text = "Please fill in the required fields ..";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(100, 60);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(260, 26);
            this.label40.TabIndex = 2;
            this.label40.Text = "Modify Product Information";
            // 
            // tabControlModifyProduct
            // 
            this.tabControlModifyProduct.Controls.Add(this.tabModifyProductInfo1);
            this.tabControlModifyProduct.Controls.Add(this.tabModifyProductInfo2);
            this.tabControlModifyProduct.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlModifyProduct.Location = new System.Drawing.Point(0, 149);
            this.tabControlModifyProduct.Name = "tabControlModifyProduct";
            this.tabControlModifyProduct.SelectedIndex = 0;
            this.tabControlModifyProduct.Size = new System.Drawing.Size(554, 264);
            this.tabControlModifyProduct.TabIndex = 11;
            // 
            // tabModifyProductInfo1
            // 
            this.tabModifyProductInfo1.BackColor = System.Drawing.SystemColors.Control;
            this.tabModifyProductInfo1.Controls.Add(this.label35);
            this.tabModifyProductInfo1.Controls.Add(this.label41);
            this.tabModifyProductInfo1.Controls.Add(this.groupBox8);
            this.tabModifyProductInfo1.Location = new System.Drawing.Point(4, 24);
            this.tabModifyProductInfo1.Name = "tabModifyProductInfo1";
            this.tabModifyProductInfo1.Padding = new System.Windows.Forms.Padding(3);
            this.tabModifyProductInfo1.Size = new System.Drawing.Size(546, 236);
            this.tabModifyProductInfo1.TabIndex = 0;
            this.tabModifyProductInfo1.Text = "Product Information";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(17, 8);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(146, 15);
            this.label35.TabIndex = 3;
            this.label35.Text = "* Denotes mandatory fields";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(17, 212);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(405, 15);
            this.label41.TabIndex = 2;
            this.label41.Text = "Please proceed to fill in required fields in the \'Product Price && Image\' section" +
                "";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lblModifyProductModelNumber);
            this.groupBox8.Controls.Add(this.cbxModifyProductCategory);
            this.groupBox8.Controls.Add(this.label43);
            this.groupBox8.Controls.Add(this.tbxModifyProductDescription);
            this.groupBox8.Controls.Add(this.label44);
            this.groupBox8.Controls.Add(this.label45);
            this.groupBox8.Controls.Add(this.lblModifyProductName);
            this.groupBox8.Controls.Add(this.label46);
            this.groupBox8.Location = new System.Drawing.Point(20, 26);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(506, 173);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Product Information";
            // 
            // lblModifyProductModelNumber
            // 
            this.lblModifyProductModelNumber.AutoSize = true;
            this.lblModifyProductModelNumber.Location = new System.Drawing.Point(19, 88);
            this.lblModifyProductModelNumber.Name = "lblModifyProductModelNumber";
            this.lblModifyProductModelNumber.Size = new System.Drawing.Size(193, 15);
            this.lblModifyProductModelNumber.TabIndex = 10;
            this.lblModifyProductModelNumber.Text = "- Display of Product Model Number -";
            // 
            // cbxModifyProductCategory
            // 
            this.cbxModifyProductCategory.FormattingEnabled = true;
            this.cbxModifyProductCategory.Location = new System.Drawing.Point(22, 134);
            this.cbxModifyProductCategory.Name = "cbxModifyProductCategory";
            this.cbxModifyProductCategory.Size = new System.Drawing.Size(142, 23);
            this.cbxModifyProductCategory.TabIndex = 9;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(19, 116);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(65, 15);
            this.label43.TabIndex = 6;
            this.label43.Text = "* Category:";
            // 
            // tbxModifyProductDescription
            // 
            this.tbxModifyProductDescription.Location = new System.Drawing.Point(242, 37);
            this.tbxModifyProductDescription.Multiline = true;
            this.tbxModifyProductDescription.Name = "tbxModifyProductDescription";
            this.tbxModifyProductDescription.Size = new System.Drawing.Size(248, 120);
            this.tbxModifyProductDescription.TabIndex = 5;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(239, 19);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(79, 15);
            this.label44.TabIndex = 4;
            this.label44.Text = "* Description:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(19, 65);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(84, 15);
            this.label45.TabIndex = 2;
            this.label45.Text = "Model Number:";
            // 
            // lblModifyProductName
            // 
            this.lblModifyProductName.AutoSize = true;
            this.lblModifyProductName.Location = new System.Drawing.Point(19, 37);
            this.lblModifyProductName.Name = "lblModifyProductName";
            this.lblModifyProductName.Size = new System.Drawing.Size(148, 15);
            this.lblModifyProductName.TabIndex = 1;
            this.lblModifyProductName.Text = "- Display of Product Name -";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(19, 19);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(39, 15);
            this.label46.TabIndex = 0;
            this.label46.Text = "Name:";
            // 
            // tabModifyProductInfo2
            // 
            this.tabModifyProductInfo2.BackColor = System.Drawing.SystemColors.Control;
            this.tabModifyProductInfo2.Controls.Add(this.label42);
            this.tabModifyProductInfo2.Controls.Add(this.groupBox9);
            this.tabModifyProductInfo2.Controls.Add(this.btnModifyProduct);
            this.tabModifyProductInfo2.Controls.Add(this.btnCancelModifyProduct);
            this.tabModifyProductInfo2.Location = new System.Drawing.Point(4, 24);
            this.tabModifyProductInfo2.Name = "tabModifyProductInfo2";
            this.tabModifyProductInfo2.Padding = new System.Windows.Forms.Padding(3);
            this.tabModifyProductInfo2.Size = new System.Drawing.Size(546, 236);
            this.tabModifyProductInfo2.TabIndex = 1;
            this.tabModifyProductInfo2.Text = "Product Price & Image";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(17, 8);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(146, 15);
            this.label42.TabIndex = 21;
            this.label42.Text = "* Denotes mandatory fields";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.btnModifyProductEnlargeImage2);
            this.groupBox9.Controls.Add(this.btnModifyProductEnlargeImage1);
            this.groupBox9.Controls.Add(this.btnModifyProductRemoveImage2);
            this.groupBox9.Controls.Add(this.btnModifyProductOpenImage2);
            this.groupBox9.Controls.Add(this.btnModifyProductRemoveImage1);
            this.groupBox9.Controls.Add(this.btnModifyProductOpenImage1);
            this.groupBox9.Controls.Add(this.label48);
            this.groupBox9.Controls.Add(this.picbxModifyProductImage2);
            this.groupBox9.Controls.Add(this.picbxModifyProductImage1);
            this.groupBox9.Controls.Add(this.tbxModifyProductRetailPrice);
            this.groupBox9.Controls.Add(this.tbxModifyProductMSRP);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Location = new System.Drawing.Point(20, 26);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(506, 173);
            this.groupBox9.TabIndex = 9;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Product Information";
            // 
            // btnModifyProductEnlargeImage2
            // 
            this.btnModifyProductEnlargeImage2.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyProductEnlargeImage2.Location = new System.Drawing.Point(382, 141);
            this.btnModifyProductEnlargeImage2.Name = "btnModifyProductEnlargeImage2";
            this.btnModifyProductEnlargeImage2.Size = new System.Drawing.Size(47, 23);
            this.btnModifyProductEnlargeImage2.TabIndex = 42;
            this.btnModifyProductEnlargeImage2.Text = "Enlarge";
            this.btnModifyProductEnlargeImage2.UseVisualStyleBackColor = true;
            this.btnModifyProductEnlargeImage2.Click += new System.EventHandler(this.btnModifyProductEnlargeImage2_Click_1);
            // 
            // btnModifyProductEnlargeImage1
            // 
            this.btnModifyProductEnlargeImage1.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyProductEnlargeImage1.Location = new System.Drawing.Point(255, 141);
            this.btnModifyProductEnlargeImage1.Name = "btnModifyProductEnlargeImage1";
            this.btnModifyProductEnlargeImage1.Size = new System.Drawing.Size(47, 23);
            this.btnModifyProductEnlargeImage1.TabIndex = 21;
            this.btnModifyProductEnlargeImage1.Text = "Enlarge";
            this.btnModifyProductEnlargeImage1.UseVisualStyleBackColor = true;
            this.btnModifyProductEnlargeImage1.Click += new System.EventHandler(this.btnModifyProductEnlargeImage1_Click_1);
            // 
            // btnModifyProductRemoveImage2
            // 
            this.btnModifyProductRemoveImage2.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyProductRemoveImage2.Location = new System.Drawing.Point(433, 115);
            this.btnModifyProductRemoveImage2.Name = "btnModifyProductRemoveImage2";
            this.btnModifyProductRemoveImage2.Size = new System.Drawing.Size(47, 23);
            this.btnModifyProductRemoveImage2.TabIndex = 20;
            this.btnModifyProductRemoveImage2.Text = "Remove";
            this.btnModifyProductRemoveImage2.UseVisualStyleBackColor = true;
            this.btnModifyProductRemoveImage2.Click += new System.EventHandler(this.btnModifyProductRemoveImage2_Click);
            // 
            // btnModifyProductOpenImage2
            // 
            this.btnModifyProductOpenImage2.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyProductOpenImage2.Location = new System.Drawing.Point(382, 115);
            this.btnModifyProductOpenImage2.Name = "btnModifyProductOpenImage2";
            this.btnModifyProductOpenImage2.Size = new System.Drawing.Size(47, 23);
            this.btnModifyProductOpenImage2.TabIndex = 19;
            this.btnModifyProductOpenImage2.Text = "Open";
            this.btnModifyProductOpenImage2.UseVisualStyleBackColor = true;
            this.btnModifyProductOpenImage2.Click += new System.EventHandler(this.btnModifyProductOpenImage2_Click);
            // 
            // btnModifyProductRemoveImage1
            // 
            this.btnModifyProductRemoveImage1.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyProductRemoveImage1.Location = new System.Drawing.Point(306, 115);
            this.btnModifyProductRemoveImage1.Name = "btnModifyProductRemoveImage1";
            this.btnModifyProductRemoveImage1.Size = new System.Drawing.Size(47, 23);
            this.btnModifyProductRemoveImage1.TabIndex = 18;
            this.btnModifyProductRemoveImage1.Text = "Remove";
            this.btnModifyProductRemoveImage1.UseVisualStyleBackColor = true;
            this.btnModifyProductRemoveImage1.Click += new System.EventHandler(this.btnModifyProductRemoveImage1_Click);
            // 
            // btnModifyProductOpenImage1
            // 
            this.btnModifyProductOpenImage1.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyProductOpenImage1.Location = new System.Drawing.Point(255, 115);
            this.btnModifyProductOpenImage1.Name = "btnModifyProductOpenImage1";
            this.btnModifyProductOpenImage1.Size = new System.Drawing.Size(47, 23);
            this.btnModifyProductOpenImage1.TabIndex = 17;
            this.btnModifyProductOpenImage1.Text = "Open";
            this.btnModifyProductOpenImage1.UseVisualStyleBackColor = true;
            this.btnModifyProductOpenImage1.Click += new System.EventHandler(this.btnModifyProductOpenImage1_Click);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(252, 19);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(47, 15);
            this.label48.TabIndex = 13;
            this.label48.Text = "Images:";
            // 
            // picbxModifyProductImage2
            // 
            this.picbxModifyProductImage2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picbxModifyProductImage2.Location = new System.Drawing.Point(384, 38);
            this.picbxModifyProductImage2.Name = "picbxModifyProductImage2";
            this.picbxModifyProductImage2.Size = new System.Drawing.Size(97, 71);
            this.picbxModifyProductImage2.TabIndex = 12;
            this.picbxModifyProductImage2.TabStop = false;
            // 
            // picbxModifyProductImage1
            // 
            this.picbxModifyProductImage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picbxModifyProductImage1.Location = new System.Drawing.Point(255, 38);
            this.picbxModifyProductImage1.Name = "picbxModifyProductImage1";
            this.picbxModifyProductImage1.Size = new System.Drawing.Size(97, 71);
            this.picbxModifyProductImage1.TabIndex = 11;
            this.picbxModifyProductImage1.TabStop = false;
            // 
            // tbxModifyProductRetailPrice
            // 
            this.tbxModifyProductRetailPrice.Location = new System.Drawing.Point(22, 86);
            this.tbxModifyProductRetailPrice.Name = "tbxModifyProductRetailPrice";
            this.tbxModifyProductRetailPrice.Size = new System.Drawing.Size(142, 23);
            this.tbxModifyProductRetailPrice.TabIndex = 10;
            // 
            // tbxModifyProductMSRP
            // 
            this.tbxModifyProductMSRP.Location = new System.Drawing.Point(22, 38);
            this.tbxModifyProductMSRP.Name = "tbxModifyProductMSRP";
            this.tbxModifyProductMSRP.Size = new System.Drawing.Size(142, 23);
            this.tbxModifyProductMSRP.TabIndex = 3;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(19, 65);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(79, 15);
            this.label49.TabIndex = 2;
            this.label49.Text = "* Retail Price:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(19, 19);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(50, 15);
            this.label50.TabIndex = 0;
            this.label50.Text = "* MSRP:";
            // 
            // btnModifyProduct
            // 
            this.btnModifyProduct.Location = new System.Drawing.Point(360, 208);
            this.btnModifyProduct.Name = "btnModifyProduct";
            this.btnModifyProduct.Size = new System.Drawing.Size(75, 23);
            this.btnModifyProduct.TabIndex = 7;
            this.btnModifyProduct.Text = "Update";
            this.btnModifyProduct.UseVisualStyleBackColor = true;
            this.btnModifyProduct.Click += new System.EventHandler(this.btnModifyProduct_Click);
            // 
            // btnCancelModifyProduct
            // 
            this.btnCancelModifyProduct.Location = new System.Drawing.Point(451, 208);
            this.btnCancelModifyProduct.Name = "btnCancelModifyProduct";
            this.btnCancelModifyProduct.Size = new System.Drawing.Size(75, 23);
            this.btnCancelModifyProduct.TabIndex = 8;
            this.btnCancelModifyProduct.Text = "Cancel";
            this.btnCancelModifyProduct.UseVisualStyleBackColor = true;
            this.btnCancelModifyProduct.Click += new System.EventHandler(this.btnCancelModifyProduct_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(424, 33);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(100, 81);
            this.pictureBox6.TabIndex = 41;
            this.pictureBox6.TabStop = false;
            // 
            // panelDeleteProduct
            // 
            this.panelDeleteProduct.Controls.Add(this.label51);
            this.panelDeleteProduct.Controls.Add(this.btnDeleteProduct);
            this.panelDeleteProduct.Controls.Add(this.btnCancelDeleteProduct);
            this.panelDeleteProduct.Controls.Add(this.groupBox10);
            this.panelDeleteProduct.Controls.Add(this.label52);
            this.panelDeleteProduct.Controls.Add(this.pictureBox7);
            this.panelDeleteProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDeleteProduct.Location = new System.Drawing.Point(0, 0);
            this.panelDeleteProduct.Name = "panelDeleteProduct";
            this.panelDeleteProduct.Size = new System.Drawing.Size(554, 414);
            this.panelDeleteProduct.TabIndex = 19;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(21, 129);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(269, 17);
            this.label51.TabIndex = 0;
            this.label51.Text = "Please confirm product information to delete ...";
            // 
            // btnDeleteProduct
            // 
            this.btnDeleteProduct.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteProduct.Location = new System.Drawing.Point(358, 381);
            this.btnDeleteProduct.Name = "btnDeleteProduct";
            this.btnDeleteProduct.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteProduct.TabIndex = 5;
            this.btnDeleteProduct.Text = "Delete";
            this.btnDeleteProduct.UseVisualStyleBackColor = true;
            this.btnDeleteProduct.Click += new System.EventHandler(this.btnDeleteProduct_Click);
            // 
            // btnCancelDeleteProduct
            // 
            this.btnCancelDeleteProduct.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelDeleteProduct.Location = new System.Drawing.Point(449, 381);
            this.btnCancelDeleteProduct.Name = "btnCancelDeleteProduct";
            this.btnCancelDeleteProduct.Size = new System.Drawing.Size(75, 23);
            this.btnCancelDeleteProduct.TabIndex = 4;
            this.btnCancelDeleteProduct.Text = "Cancel";
            this.btnCancelDeleteProduct.UseVisualStyleBackColor = true;
            this.btnCancelDeleteProduct.Click += new System.EventHandler(this.btnCancelDeleteProduct_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.tbxDeleteProductDisplayDescription);
            this.groupBox10.Controls.Add(this.label132);
            this.groupBox10.Controls.Add(this.lblDeleteProductDisplayCategory);
            this.groupBox10.Controls.Add(this.lblDeleteProductDisplayMSRP);
            this.groupBox10.Controls.Add(this.label129);
            this.groupBox10.Controls.Add(this.lblDeleteProductDisplayRetailPrice);
            this.groupBox10.Controls.Add(this.lblDeleteProductDisplayModelNumber);
            this.groupBox10.Controls.Add(this.lblDeleteProductDisplayName);
            this.groupBox10.Controls.Add(this.label125);
            this.groupBox10.Controls.Add(this.label124);
            this.groupBox10.Controls.Add(this.label123);
            this.groupBox10.Controls.Add(this.label30);
            this.groupBox10.Controls.Add(this.btnDeleteProductEnlargeImage2);
            this.groupBox10.Controls.Add(this.btnDeleteProductEnlargeImage1);
            this.groupBox10.Controls.Add(this.picbxDeleteProductImage2);
            this.groupBox10.Controls.Add(this.picbxDeleteProductImage1);
            this.groupBox10.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(24, 149);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(500, 226);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Product Information";
            // 
            // tbxDeleteProductDisplayDescription
            // 
            this.tbxDeleteProductDisplayDescription.Location = new System.Drawing.Point(19, 132);
            this.tbxDeleteProductDisplayDescription.Multiline = true;
            this.tbxDeleteProductDisplayDescription.Name = "tbxDeleteProductDisplayDescription";
            this.tbxDeleteProductDisplayDescription.ReadOnly = true;
            this.tbxDeleteProductDisplayDescription.Size = new System.Drawing.Size(336, 79);
            this.tbxDeleteProductDisplayDescription.TabIndex = 54;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(16, 115);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(70, 15);
            this.label132.TabIndex = 53;
            this.label132.Text = "Description:";
            // 
            // lblDeleteProductDisplayCategory
            // 
            this.lblDeleteProductDisplayCategory.AutoSize = true;
            this.lblDeleteProductDisplayCategory.Location = new System.Drawing.Point(103, 85);
            this.lblDeleteProductDisplayCategory.Name = "lblDeleteProductDisplayCategory";
            this.lblDeleteProductDisplayCategory.Size = new System.Drawing.Size(108, 15);
            this.lblDeleteProductDisplayCategory.TabIndex = 52;
            this.lblDeleteProductDisplayCategory.Text = "- Display Category -";
            // 
            // lblDeleteProductDisplayMSRP
            // 
            this.lblDeleteProductDisplayMSRP.AutoSize = true;
            this.lblDeleteProductDisplayMSRP.Location = new System.Drawing.Point(287, 55);
            this.lblDeleteProductDisplayMSRP.Name = "lblDeleteProductDisplayMSRP";
            this.lblDeleteProductDisplayMSRP.Size = new System.Drawing.Size(93, 15);
            this.lblDeleteProductDisplayMSRP.TabIndex = 51;
            this.lblDeleteProductDisplayMSRP.Text = "- Display MSRP -";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(16, 85);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(56, 15);
            this.label129.TabIndex = 50;
            this.label129.Text = "Category:";
            // 
            // lblDeleteProductDisplayRetailPrice
            // 
            this.lblDeleteProductDisplayRetailPrice.AutoSize = true;
            this.lblDeleteProductDisplayRetailPrice.Location = new System.Drawing.Point(103, 55);
            this.lblDeleteProductDisplayRetailPrice.Name = "lblDeleteProductDisplayRetailPrice";
            this.lblDeleteProductDisplayRetailPrice.Size = new System.Drawing.Size(99, 15);
            this.lblDeleteProductDisplayRetailPrice.TabIndex = 49;
            this.lblDeleteProductDisplayRetailPrice.Text = "- Display R.Price -";
            // 
            // lblDeleteProductDisplayModelNumber
            // 
            this.lblDeleteProductDisplayModelNumber.AutoSize = true;
            this.lblDeleteProductDisplayModelNumber.Location = new System.Drawing.Point(287, 24);
            this.lblDeleteProductDisplayModelNumber.Name = "lblDeleteProductDisplayModelNumber";
            this.lblDeleteProductDisplayModelNumber.Size = new System.Drawing.Size(90, 15);
            this.lblDeleteProductDisplayModelNumber.TabIndex = 48;
            this.lblDeleteProductDisplayModelNumber.Text = "- Display M.No -";
            // 
            // lblDeleteProductDisplayName
            // 
            this.lblDeleteProductDisplayName.AutoSize = true;
            this.lblDeleteProductDisplayName.Location = new System.Drawing.Point(103, 25);
            this.lblDeleteProductDisplayName.Name = "lblDeleteProductDisplayName";
            this.lblDeleteProductDisplayName.Size = new System.Drawing.Size(100, 15);
            this.lblDeleteProductDisplayName.TabIndex = 47;
            this.lblDeleteProductDisplayName.Text = "- Display P.Name -";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(222, 55);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(41, 15);
            this.label125.TabIndex = 46;
            this.label125.Text = "MSRP:";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(16, 55);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(70, 15);
            this.label124.TabIndex = 45;
            this.label124.Text = "Retail Price:";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(223, 25);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(59, 15);
            this.label123.TabIndex = 44;
            this.label123.Text = "Model No:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(16, 25);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(81, 15);
            this.label30.TabIndex = 43;
            this.label30.Text = "Product Name:";
            // 
            // btnDeleteProductEnlargeImage2
            // 
            this.btnDeleteProductEnlargeImage2.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteProductEnlargeImage2.Location = new System.Drawing.Point(434, 191);
            this.btnDeleteProductEnlargeImage2.Name = "btnDeleteProductEnlargeImage2";
            this.btnDeleteProductEnlargeImage2.Size = new System.Drawing.Size(47, 23);
            this.btnDeleteProductEnlargeImage2.TabIndex = 42;
            this.btnDeleteProductEnlargeImage2.Text = "Enlarge";
            this.btnDeleteProductEnlargeImage2.UseVisualStyleBackColor = true;
            this.btnDeleteProductEnlargeImage2.Click += new System.EventHandler(this.btnDeleteProductEnlargeImage2_Click);
            // 
            // btnDeleteProductEnlargeImage1
            // 
            this.btnDeleteProductEnlargeImage1.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteProductEnlargeImage1.Location = new System.Drawing.Point(434, 86);
            this.btnDeleteProductEnlargeImage1.Name = "btnDeleteProductEnlargeImage1";
            this.btnDeleteProductEnlargeImage1.Size = new System.Drawing.Size(47, 23);
            this.btnDeleteProductEnlargeImage1.TabIndex = 42;
            this.btnDeleteProductEnlargeImage1.Text = "Enlarge";
            this.btnDeleteProductEnlargeImage1.UseVisualStyleBackColor = true;
            this.btnDeleteProductEnlargeImage1.Click += new System.EventHandler(this.btnDeleteProductEnlargeImage1_Click);
            // 
            // picbxDeleteProductImage2
            // 
            this.picbxDeleteProductImage2.Location = new System.Drawing.Point(384, 131);
            this.picbxDeleteProductImage2.Name = "picbxDeleteProductImage2";
            this.picbxDeleteProductImage2.Size = new System.Drawing.Size(97, 71);
            this.picbxDeleteProductImage2.TabIndex = 14;
            this.picbxDeleteProductImage2.TabStop = false;
            // 
            // picbxDeleteProductImage1
            // 
            this.picbxDeleteProductImage1.Location = new System.Drawing.Point(384, 27);
            this.picbxDeleteProductImage1.Name = "picbxDeleteProductImage1";
            this.picbxDeleteProductImage1.Size = new System.Drawing.Size(97, 71);
            this.picbxDeleteProductImage1.TabIndex = 13;
            this.picbxDeleteProductImage1.TabStop = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(108, 60);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(255, 26);
            this.label52.TabIndex = 2;
            this.label52.Text = "Delete Product Information";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(424, 33);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(100, 81);
            this.pictureBox7.TabIndex = 41;
            this.pictureBox7.TabStop = false;
            // 
            // panelDisplayProduct
            // 
            this.panelDisplayProduct.Controls.Add(this.pictureBox8);
            this.panelDisplayProduct.Controls.Add(this.btnDisplayProduct);
            this.panelDisplayProduct.Controls.Add(this.groupBox4);
            this.panelDisplayProduct.Controls.Add(this.label18);
            this.panelDisplayProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDisplayProduct.Location = new System.Drawing.Point(0, 0);
            this.panelDisplayProduct.Name = "panelDisplayProduct";
            this.panelDisplayProduct.Size = new System.Drawing.Size(554, 414);
            this.panelDisplayProduct.TabIndex = 20;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(424, 33);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(100, 81);
            this.pictureBox8.TabIndex = 40;
            this.pictureBox8.TabStop = false;
            // 
            // btnDisplayProduct
            // 
            this.btnDisplayProduct.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisplayProduct.Location = new System.Drawing.Point(449, 381);
            this.btnDisplayProduct.Name = "btnDisplayProduct";
            this.btnDisplayProduct.Size = new System.Drawing.Size(75, 23);
            this.btnDisplayProduct.TabIndex = 4;
            this.btnDisplayProduct.Text = "OK";
            this.btnDisplayProduct.UseVisualStyleBackColor = true;
            this.btnDisplayProduct.Click += new System.EventHandler(this.btnDisplayProduct_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbxDisplayProductDisplayDescription);
            this.groupBox4.Controls.Add(this.label126);
            this.groupBox4.Controls.Add(this.lblDisplayProductDisplayCategory);
            this.groupBox4.Controls.Add(this.lblDisplayProductDisplayMSRP);
            this.groupBox4.Controls.Add(this.label130);
            this.groupBox4.Controls.Add(this.lblDisplayProductDisplayRetailPRice);
            this.groupBox4.Controls.Add(this.lblDisplayProductDisplayModelNumber);
            this.groupBox4.Controls.Add(this.lblDisplayProductDisplayName);
            this.groupBox4.Controls.Add(this.label135);
            this.groupBox4.Controls.Add(this.label136);
            this.groupBox4.Controls.Add(this.label137);
            this.groupBox4.Controls.Add(this.label138);
            this.groupBox4.Controls.Add(this.btnDisplayProductEnlargeImage2);
            this.groupBox4.Controls.Add(this.btnDisplayProductEnlargeImage1);
            this.groupBox4.Controls.Add(this.picbxDisplayProductImage2);
            this.groupBox4.Controls.Add(this.picbxDisplayProductImage1);
            this.groupBox4.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(24, 149);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(500, 226);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Product Information";
            // 
            // tbxDisplayProductDisplayDescription
            // 
            this.tbxDisplayProductDisplayDescription.Location = new System.Drawing.Point(19, 132);
            this.tbxDisplayProductDisplayDescription.Multiline = true;
            this.tbxDisplayProductDisplayDescription.Name = "tbxDisplayProductDisplayDescription";
            this.tbxDisplayProductDisplayDescription.ReadOnly = true;
            this.tbxDisplayProductDisplayDescription.Size = new System.Drawing.Size(336, 79);
            this.tbxDisplayProductDisplayDescription.TabIndex = 66;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(16, 115);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(70, 15);
            this.label126.TabIndex = 65;
            this.label126.Text = "Description:";
            // 
            // lblDisplayProductDisplayCategory
            // 
            this.lblDisplayProductDisplayCategory.AutoSize = true;
            this.lblDisplayProductDisplayCategory.Location = new System.Drawing.Point(103, 85);
            this.lblDisplayProductDisplayCategory.Name = "lblDisplayProductDisplayCategory";
            this.lblDisplayProductDisplayCategory.Size = new System.Drawing.Size(108, 15);
            this.lblDisplayProductDisplayCategory.TabIndex = 64;
            this.lblDisplayProductDisplayCategory.Text = "- Display Category -";
            // 
            // lblDisplayProductDisplayMSRP
            // 
            this.lblDisplayProductDisplayMSRP.AutoSize = true;
            this.lblDisplayProductDisplayMSRP.Location = new System.Drawing.Point(287, 55);
            this.lblDisplayProductDisplayMSRP.Name = "lblDisplayProductDisplayMSRP";
            this.lblDisplayProductDisplayMSRP.Size = new System.Drawing.Size(93, 15);
            this.lblDisplayProductDisplayMSRP.TabIndex = 63;
            this.lblDisplayProductDisplayMSRP.Text = "- Display MSRP -";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(16, 85);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(56, 15);
            this.label130.TabIndex = 62;
            this.label130.Text = "Category:";
            // 
            // lblDisplayProductDisplayRetailPRice
            // 
            this.lblDisplayProductDisplayRetailPRice.AutoSize = true;
            this.lblDisplayProductDisplayRetailPRice.Location = new System.Drawing.Point(103, 55);
            this.lblDisplayProductDisplayRetailPRice.Name = "lblDisplayProductDisplayRetailPRice";
            this.lblDisplayProductDisplayRetailPRice.Size = new System.Drawing.Size(99, 15);
            this.lblDisplayProductDisplayRetailPRice.TabIndex = 61;
            this.lblDisplayProductDisplayRetailPRice.Text = "- Display R.Price -";
            // 
            // lblDisplayProductDisplayModelNumber
            // 
            this.lblDisplayProductDisplayModelNumber.AutoSize = true;
            this.lblDisplayProductDisplayModelNumber.Location = new System.Drawing.Point(287, 24);
            this.lblDisplayProductDisplayModelNumber.Name = "lblDisplayProductDisplayModelNumber";
            this.lblDisplayProductDisplayModelNumber.Size = new System.Drawing.Size(90, 15);
            this.lblDisplayProductDisplayModelNumber.TabIndex = 60;
            this.lblDisplayProductDisplayModelNumber.Text = "- Display M.No -";
            // 
            // lblDisplayProductDisplayName
            // 
            this.lblDisplayProductDisplayName.AutoSize = true;
            this.lblDisplayProductDisplayName.Location = new System.Drawing.Point(103, 25);
            this.lblDisplayProductDisplayName.Name = "lblDisplayProductDisplayName";
            this.lblDisplayProductDisplayName.Size = new System.Drawing.Size(100, 15);
            this.lblDisplayProductDisplayName.TabIndex = 59;
            this.lblDisplayProductDisplayName.Text = "- Display P.Name -";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(222, 55);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(41, 15);
            this.label135.TabIndex = 58;
            this.label135.Text = "MSRP:";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(16, 55);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(70, 15);
            this.label136.TabIndex = 57;
            this.label136.Text = "Retail Price:";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(223, 25);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(59, 15);
            this.label137.TabIndex = 56;
            this.label137.Text = "Model No:";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(16, 25);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(81, 15);
            this.label138.TabIndex = 55;
            this.label138.Text = "Product Name:";
            // 
            // btnDisplayProductEnlargeImage2
            // 
            this.btnDisplayProductEnlargeImage2.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisplayProductEnlargeImage2.Location = new System.Drawing.Point(434, 191);
            this.btnDisplayProductEnlargeImage2.Name = "btnDisplayProductEnlargeImage2";
            this.btnDisplayProductEnlargeImage2.Size = new System.Drawing.Size(47, 23);
            this.btnDisplayProductEnlargeImage2.TabIndex = 41;
            this.btnDisplayProductEnlargeImage2.Text = "Enlarge";
            this.btnDisplayProductEnlargeImage2.UseVisualStyleBackColor = true;
            this.btnDisplayProductEnlargeImage2.Click += new System.EventHandler(this.btnDisplayProductEnlargeImage2_Click);
            // 
            // btnDisplayProductEnlargeImage1
            // 
            this.btnDisplayProductEnlargeImage1.Font = new System.Drawing.Font("Comic Sans MS", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisplayProductEnlargeImage1.Location = new System.Drawing.Point(434, 86);
            this.btnDisplayProductEnlargeImage1.Name = "btnDisplayProductEnlargeImage1";
            this.btnDisplayProductEnlargeImage1.Size = new System.Drawing.Size(47, 23);
            this.btnDisplayProductEnlargeImage1.TabIndex = 41;
            this.btnDisplayProductEnlargeImage1.Text = "Enlarge";
            this.btnDisplayProductEnlargeImage1.UseVisualStyleBackColor = true;
            this.btnDisplayProductEnlargeImage1.Click += new System.EventHandler(this.btnDisplayProductEnlargeImage1_Click);
            // 
            // picbxDisplayProductImage2
            // 
            this.picbxDisplayProductImage2.Location = new System.Drawing.Point(384, 131);
            this.picbxDisplayProductImage2.Name = "picbxDisplayProductImage2";
            this.picbxDisplayProductImage2.Size = new System.Drawing.Size(97, 71);
            this.picbxDisplayProductImage2.TabIndex = 14;
            this.picbxDisplayProductImage2.TabStop = false;
            // 
            // picbxDisplayProductImage1
            // 
            this.picbxDisplayProductImage1.Location = new System.Drawing.Point(384, 27);
            this.picbxDisplayProductImage1.Name = "picbxDisplayProductImage1";
            this.picbxDisplayProductImage1.Size = new System.Drawing.Size(97, 71);
            this.picbxDisplayProductImage1.TabIndex = 13;
            this.picbxDisplayProductImage1.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(101, 60);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(260, 26);
            this.label18.TabIndex = 2;
            this.label18.Text = "Display Product Information";
            // 
            // pnlAddStock
            // 
            this.pnlAddStock.Controls.Add(this.label47);
            this.pnlAddStock.Controls.Add(this.groupBox14);
            this.pnlAddStock.Controls.Add(this.label72);
            this.pnlAddStock.Controls.Add(this.label73);
            this.pnlAddStock.Controls.Add(this.pictureBox3);
            this.pnlAddStock.Controls.Add(this.btnAddStock_Cancel);
            this.pnlAddStock.Controls.Add(this.btnAddStock_Add);
            this.pnlAddStock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAddStock.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlAddStock.Location = new System.Drawing.Point(0, 0);
            this.pnlAddStock.Name = "pnlAddStock";
            this.pnlAddStock.Size = new System.Drawing.Size(554, 414);
            this.pnlAddStock.TabIndex = 21;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(32, 155);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(146, 15);
            this.label47.TabIndex = 53;
            this.label47.Text = "* Denotes mandatory fields";
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox14.Controls.Add(this.tbxAddStock_CompanyName);
            this.groupBox14.Controls.Add(this.label66);
            this.groupBox14.Controls.Add(this.tbxAddStock_StockQty);
            this.groupBox14.Controls.Add(this.tbxAddStock_ModelName);
            this.groupBox14.Controls.Add(this.tbxAddStock_ProductName);
            this.groupBox14.Controls.Add(this.label69);
            this.groupBox14.Controls.Add(this.label70);
            this.groupBox14.Controls.Add(this.label71);
            this.groupBox14.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox14.Location = new System.Drawing.Point(29, 173);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(501, 193);
            this.groupBox14.TabIndex = 52;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Product Information";
            // 
            // tbxAddStock_CompanyName
            // 
            this.tbxAddStock_CompanyName.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddStock_CompanyName.Location = new System.Drawing.Point(268, 60);
            this.tbxAddStock_CompanyName.Name = "tbxAddStock_CompanyName";
            this.tbxAddStock_CompanyName.Size = new System.Drawing.Size(184, 23);
            this.tbxAddStock_CompanyName.TabIndex = 62;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(268, 42);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(99, 15);
            this.label66.TabIndex = 61;
            this.label66.Text = "* Company Name :";
            // 
            // tbxAddStock_StockQty
            // 
            this.tbxAddStock_StockQty.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddStock_StockQty.Location = new System.Drawing.Point(268, 123);
            this.tbxAddStock_StockQty.Name = "tbxAddStock_StockQty";
            this.tbxAddStock_StockQty.Size = new System.Drawing.Size(99, 23);
            this.tbxAddStock_StockQty.TabIndex = 60;
            // 
            // tbxAddStock_ModelName
            // 
            this.tbxAddStock_ModelName.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddStock_ModelName.Location = new System.Drawing.Point(41, 120);
            this.tbxAddStock_ModelName.Name = "tbxAddStock_ModelName";
            this.tbxAddStock_ModelName.Size = new System.Drawing.Size(184, 23);
            this.tbxAddStock_ModelName.TabIndex = 59;
            // 
            // tbxAddStock_ProductName
            // 
            this.tbxAddStock_ProductName.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddStock_ProductName.Location = new System.Drawing.Point(41, 60);
            this.tbxAddStock_ProductName.Name = "tbxAddStock_ProductName";
            this.tbxAddStock_ProductName.Size = new System.Drawing.Size(184, 23);
            this.tbxAddStock_ProductName.TabIndex = 58;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(38, 100);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(85, 15);
            this.label69.TabIndex = 57;
            this.label69.Text = "* Model Name :";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(38, 42);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(93, 15);
            this.label70.TabIndex = 56;
            this.label70.Text = "* Product Name :";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(268, 100);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(101, 15);
            this.label71.TabIndex = 55;
            this.label71.Text = "* Stock Quantity :";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(30, 121);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(192, 17);
            this.label72.TabIndex = 51;
            this.label72.Text = "Please fill in the required fields..";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(134, 60);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(216, 26);
            this.label73.TabIndex = 50;
            this.label73.Text = "Add Stock Information";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(430, 37);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 81);
            this.pictureBox3.TabIndex = 49;
            this.pictureBox3.TabStop = false;
            // 
            // btnAddStock_Cancel
            // 
            this.btnAddStock_Cancel.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStock_Cancel.Location = new System.Drawing.Point(452, 379);
            this.btnAddStock_Cancel.Name = "btnAddStock_Cancel";
            this.btnAddStock_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btnAddStock_Cancel.TabIndex = 52;
            this.btnAddStock_Cancel.Text = "Cancel";
            this.btnAddStock_Cancel.UseVisualStyleBackColor = true;
            this.btnAddStock_Cancel.Click += new System.EventHandler(this.btnAddStock_Cancel_Click);
            // 
            // btnAddStock_Add
            // 
            this.btnAddStock_Add.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStock_Add.Location = new System.Drawing.Point(355, 379);
            this.btnAddStock_Add.Name = "btnAddStock_Add";
            this.btnAddStock_Add.Size = new System.Drawing.Size(75, 23);
            this.btnAddStock_Add.TabIndex = 51;
            this.btnAddStock_Add.Text = "Add";
            this.btnAddStock_Add.UseVisualStyleBackColor = true;
            this.btnAddStock_Add.Click += new System.EventHandler(this.btnAddStock_Add_Click);
            // 
            // pnlEditStockInfo
            // 
            this.pnlEditStockInfo.AutoScroll = true;
            this.pnlEditStockInfo.Controls.Add(this.grpEditStockInfo);
            this.pnlEditStockInfo.Controls.Add(this.label78);
            this.pnlEditStockInfo.Controls.Add(this.lblInfoUpdateStock);
            this.pnlEditStockInfo.Controls.Add(this.lblUpdateStock);
            this.pnlEditStockInfo.Controls.Add(this.picBoxLogoUpdateStock);
            this.pnlEditStockInfo.Controls.Add(this.btnCancelUpdateStock);
            this.pnlEditStockInfo.Controls.Add(this.btnUpdateStock);
            this.pnlEditStockInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlEditStockInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlEditStockInfo.Name = "pnlEditStockInfo";
            this.pnlEditStockInfo.Size = new System.Drawing.Size(554, 414);
            this.pnlEditStockInfo.TabIndex = 22;
            // 
            // grpEditStockInfo
            // 
            this.grpEditStockInfo.BackColor = System.Drawing.SystemColors.Control;
            this.grpEditStockInfo.Controls.Add(this.lblEditStock_DisplayCompanyName);
            this.grpEditStockInfo.Controls.Add(this.lblEditStock_DisplayModelName);
            this.grpEditStockInfo.Controls.Add(this.lblEditStock_DisplayProductName);
            this.grpEditStockInfo.Controls.Add(this.label74);
            this.grpEditStockInfo.Controls.Add(this.tbxEditStock_StockQty);
            this.grpEditStockInfo.Controls.Add(this.label75);
            this.grpEditStockInfo.Controls.Add(this.label76);
            this.grpEditStockInfo.Controls.Add(this.label77);
            this.grpEditStockInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpEditStockInfo.Location = new System.Drawing.Point(29, 173);
            this.grpEditStockInfo.Name = "grpEditStockInfo";
            this.grpEditStockInfo.Size = new System.Drawing.Size(501, 193);
            this.grpEditStockInfo.TabIndex = 48;
            this.grpEditStockInfo.TabStop = false;
            this.grpEditStockInfo.Text = "Product Information";
            // 
            // lblEditStock_DisplayCompanyName
            // 
            this.lblEditStock_DisplayCompanyName.AutoSize = true;
            this.lblEditStock_DisplayCompanyName.Location = new System.Drawing.Point(268, 60);
            this.lblEditStock_DisplayCompanyName.Name = "lblEditStock_DisplayCompanyName";
            this.lblEditStock_DisplayCompanyName.Size = new System.Drawing.Size(157, 15);
            this.lblEditStock_DisplayCompanyName.TabIndex = 64;
            this.lblEditStock_DisplayCompanyName.Text = " - Display of Company Name -";
            // 
            // lblEditStock_DisplayModelName
            // 
            this.lblEditStock_DisplayModelName.AutoSize = true;
            this.lblEditStock_DisplayModelName.Location = new System.Drawing.Point(38, 120);
            this.lblEditStock_DisplayModelName.Name = "lblEditStock_DisplayModelName";
            this.lblEditStock_DisplayModelName.Size = new System.Drawing.Size(143, 15);
            this.lblEditStock_DisplayModelName.TabIndex = 63;
            this.lblEditStock_DisplayModelName.Text = " - Display of Model Name -";
            // 
            // lblEditStock_DisplayProductName
            // 
            this.lblEditStock_DisplayProductName.AutoSize = true;
            this.lblEditStock_DisplayProductName.Location = new System.Drawing.Point(38, 60);
            this.lblEditStock_DisplayProductName.Name = "lblEditStock_DisplayProductName";
            this.lblEditStock_DisplayProductName.Size = new System.Drawing.Size(151, 15);
            this.lblEditStock_DisplayProductName.TabIndex = 62;
            this.lblEditStock_DisplayProductName.Text = " - Display of Product Name -";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(268, 42);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(90, 15);
            this.label74.TabIndex = 61;
            this.label74.Text = "Company Name :";
            // 
            // tbxEditStock_StockQty
            // 
            this.tbxEditStock_StockQty.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxEditStock_StockQty.Location = new System.Drawing.Point(271, 118);
            this.tbxEditStock_StockQty.Name = "tbxEditStock_StockQty";
            this.tbxEditStock_StockQty.Size = new System.Drawing.Size(99, 23);
            this.tbxEditStock_StockQty.TabIndex = 60;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(38, 100);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(76, 15);
            this.label75.TabIndex = 57;
            this.label75.Text = "Model Name :";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(38, 42);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(84, 15);
            this.label76.TabIndex = 56;
            this.label76.Text = "Product Name :";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(268, 100);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(101, 15);
            this.label77.TabIndex = 55;
            this.label77.Text = "* Stock Quantity :";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(32, 155);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(146, 15);
            this.label78.TabIndex = 47;
            this.label78.Text = "* Denotes mandatory fields";
            // 
            // lblInfoUpdateStock
            // 
            this.lblInfoUpdateStock.AutoSize = true;
            this.lblInfoUpdateStock.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoUpdateStock.Location = new System.Drawing.Point(30, 121);
            this.lblInfoUpdateStock.Name = "lblInfoUpdateStock";
            this.lblInfoUpdateStock.Size = new System.Drawing.Size(192, 17);
            this.lblInfoUpdateStock.TabIndex = 42;
            this.lblInfoUpdateStock.Text = "Please fill in the required fields..";
            // 
            // lblUpdateStock
            // 
            this.lblUpdateStock.AutoSize = true;
            this.lblUpdateStock.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdateStock.Location = new System.Drawing.Point(169, 60);
            this.lblUpdateStock.Name = "lblUpdateStock";
            this.lblUpdateStock.Size = new System.Drawing.Size(216, 26);
            this.lblUpdateStock.TabIndex = 41;
            this.lblUpdateStock.Text = "Edit Stock Information";
            // 
            // picBoxLogoUpdateStock
            // 
            this.picBoxLogoUpdateStock.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoUpdateStock.Image")));
            this.picBoxLogoUpdateStock.Location = new System.Drawing.Point(430, 37);
            this.picBoxLogoUpdateStock.Name = "picBoxLogoUpdateStock";
            this.picBoxLogoUpdateStock.Size = new System.Drawing.Size(100, 81);
            this.picBoxLogoUpdateStock.TabIndex = 39;
            this.picBoxLogoUpdateStock.TabStop = false;
            // 
            // btnCancelUpdateStock
            // 
            this.btnCancelUpdateStock.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelUpdateStock.Location = new System.Drawing.Point(452, 379);
            this.btnCancelUpdateStock.Name = "btnCancelUpdateStock";
            this.btnCancelUpdateStock.Size = new System.Drawing.Size(75, 23);
            this.btnCancelUpdateStock.TabIndex = 52;
            this.btnCancelUpdateStock.Text = "Cancel";
            this.btnCancelUpdateStock.UseVisualStyleBackColor = true;
            this.btnCancelUpdateStock.Click += new System.EventHandler(this.btnCancelUpdateStock_Click);
            // 
            // btnUpdateStock
            // 
            this.btnUpdateStock.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateStock.Location = new System.Drawing.Point(355, 379);
            this.btnUpdateStock.Name = "btnUpdateStock";
            this.btnUpdateStock.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateStock.TabIndex = 51;
            this.btnUpdateStock.Text = "Update";
            this.btnUpdateStock.UseVisualStyleBackColor = true;
            this.btnUpdateStock.Click += new System.EventHandler(this.btnUpdateStock_Click);
            // 
            // pnlDisplayComputeExcessSHrtfall
            // 
            this.pnlDisplayComputeExcessSHrtfall.Controls.Add(this.lblDeleteExistingSupplierInfo);
            this.pnlDisplayComputeExcessSHrtfall.Controls.Add(this.picBxLogoDeleteSupplierPnl);
            this.pnlDisplayComputeExcessSHrtfall.Controls.Add(this.btnDispComputeExcessSHort_OK);
            this.pnlDisplayComputeExcessSHrtfall.Controls.Add(this.groupBox15);
            this.pnlDisplayComputeExcessSHrtfall.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDisplayComputeExcessSHrtfall.Location = new System.Drawing.Point(0, 0);
            this.pnlDisplayComputeExcessSHrtfall.Name = "pnlDisplayComputeExcessSHrtfall";
            this.pnlDisplayComputeExcessSHrtfall.Size = new System.Drawing.Size(554, 414);
            this.pnlDisplayComputeExcessSHrtfall.TabIndex = 23;
            // 
            // lblDeleteExistingSupplierInfo
            // 
            this.lblDeleteExistingSupplierInfo.AutoSize = true;
            this.lblDeleteExistingSupplierInfo.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteExistingSupplierInfo.Location = new System.Drawing.Point(104, 60);
            this.lblDeleteExistingSupplierInfo.Name = "lblDeleteExistingSupplierInfo";
            this.lblDeleteExistingSupplierInfo.Size = new System.Drawing.Size(257, 26);
            this.lblDeleteExistingSupplierInfo.TabIndex = 53;
            this.lblDeleteExistingSupplierInfo.Text = "Compute Excess / ShortFall";
            // 
            // picBxLogoDeleteSupplierPnl
            // 
            this.picBxLogoDeleteSupplierPnl.Image = ((System.Drawing.Image)(resources.GetObject("picBxLogoDeleteSupplierPnl.Image")));
            this.picBxLogoDeleteSupplierPnl.Location = new System.Drawing.Point(430, 37);
            this.picBxLogoDeleteSupplierPnl.Name = "picBxLogoDeleteSupplierPnl";
            this.picBxLogoDeleteSupplierPnl.Size = new System.Drawing.Size(100, 81);
            this.picBxLogoDeleteSupplierPnl.TabIndex = 52;
            this.picBxLogoDeleteSupplierPnl.TabStop = false;
            // 
            // btnDispComputeExcessSHort_OK
            // 
            this.btnDispComputeExcessSHort_OK.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDispComputeExcessSHort_OK.Location = new System.Drawing.Point(446, 372);
            this.btnDispComputeExcessSHort_OK.Name = "btnDispComputeExcessSHort_OK";
            this.btnDispComputeExcessSHort_OK.Size = new System.Drawing.Size(75, 23);
            this.btnDispComputeExcessSHort_OK.TabIndex = 51;
            this.btnDispComputeExcessSHort_OK.Text = "OK";
            this.btnDispComputeExcessSHort_OK.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label79);
            this.groupBox15.Controls.Add(this.lblProModelName_ComputeExcessShortFallDisplay);
            this.groupBox15.Controls.Add(this.lblTotalOrder_ComputeExcessShortFallDisplay);
            this.groupBox15.Controls.Add(this.lblComputeExcessShortFallDisplay);
            this.groupBox15.Controls.Add(this.lblTotalQty_ComputeExcessShortFallDisplay);
            this.groupBox15.Controls.Add(this.lblProName_ComputeExcessShortFallDisplay);
            this.groupBox15.Controls.Add(this.label93);
            this.groupBox15.Controls.Add(this.label92);
            this.groupBox15.Controls.Add(this.label91);
            this.groupBox15.Controls.Add(this.label89);
            this.groupBox15.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox15.Location = new System.Drawing.Point(33, 135);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(497, 222);
            this.groupBox15.TabIndex = 50;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Display Excess ShortFall";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(44, 91);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(84, 15);
            this.label79.TabIndex = 12;
            this.label79.Text = "Model Number:";
            // 
            // lblProModelName_ComputeExcessShortFallDisplay
            // 
            this.lblProModelName_ComputeExcessShortFallDisplay.AutoSize = true;
            this.lblProModelName_ComputeExcessShortFallDisplay.Location = new System.Drawing.Point(42, 113);
            this.lblProModelName_ComputeExcessShortFallDisplay.Name = "lblProModelName_ComputeExcessShortFallDisplay";
            this.lblProModelName_ComputeExcessShortFallDisplay.Size = new System.Drawing.Size(151, 15);
            this.lblProModelName_ComputeExcessShortFallDisplay.TabIndex = 11;
            this.lblProModelName_ComputeExcessShortFallDisplay.Text = "- Display of Model Number -";
            // 
            // lblTotalOrder_ComputeExcessShortFallDisplay
            // 
            this.lblTotalOrder_ComputeExcessShortFallDisplay.AutoSize = true;
            this.lblTotalOrder_ComputeExcessShortFallDisplay.Location = new System.Drawing.Point(261, 57);
            this.lblTotalOrder_ComputeExcessShortFallDisplay.Name = "lblTotalOrder_ComputeExcessShortFallDisplay";
            this.lblTotalOrder_ComputeExcessShortFallDisplay.Size = new System.Drawing.Size(140, 15);
            this.lblTotalOrder_ComputeExcessShortFallDisplay.TabIndex = 10;
            this.lblTotalOrder_ComputeExcessShortFallDisplay.Text = " - Display of Total Order -";
            // 
            // lblComputeExcessShortFallDisplay
            // 
            this.lblComputeExcessShortFallDisplay.AutoSize = true;
            this.lblComputeExcessShortFallDisplay.Location = new System.Drawing.Point(261, 113);
            this.lblComputeExcessShortFallDisplay.Name = "lblComputeExcessShortFallDisplay";
            this.lblComputeExcessShortFallDisplay.Size = new System.Drawing.Size(155, 15);
            this.lblComputeExcessShortFallDisplay.TabIndex = 9;
            this.lblComputeExcessShortFallDisplay.Text = " - Display of Total Quantity -";
            // 
            // lblTotalQty_ComputeExcessShortFallDisplay
            // 
            this.lblTotalQty_ComputeExcessShortFallDisplay.AutoSize = true;
            this.lblTotalQty_ComputeExcessShortFallDisplay.Location = new System.Drawing.Point(44, 173);
            this.lblTotalQty_ComputeExcessShortFallDisplay.Name = "lblTotalQty_ComputeExcessShortFallDisplay";
            this.lblTotalQty_ComputeExcessShortFallDisplay.Size = new System.Drawing.Size(169, 15);
            this.lblTotalQty_ComputeExcessShortFallDisplay.TabIndex = 8;
            this.lblTotalQty_ComputeExcessShortFallDisplay.Text = " - Display of Excess/Shortage -";
            // 
            // lblProName_ComputeExcessShortFallDisplay
            // 
            this.lblProName_ComputeExcessShortFallDisplay.AutoSize = true;
            this.lblProName_ComputeExcessShortFallDisplay.Location = new System.Drawing.Point(42, 57);
            this.lblProName_ComputeExcessShortFallDisplay.Name = "lblProName_ComputeExcessShortFallDisplay";
            this.lblProName_ComputeExcessShortFallDisplay.Size = new System.Drawing.Size(151, 15);
            this.lblProName_ComputeExcessShortFallDisplay.TabIndex = 7;
            this.lblProName_ComputeExcessShortFallDisplay.Text = " - Display of Product Name -";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(261, 38);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(67, 15);
            this.label93.TabIndex = 6;
            this.label93.Text = "Total Order";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(42, 150);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(142, 15);
            this.label92.TabIndex = 4;
            this.label92.Text = "Excess/ShortFall/Balance";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(261, 91);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(56, 15);
            this.label91.TabIndex = 2;
            this.label91.Text = "Total Qty";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(42, 38);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(81, 15);
            this.label89.TabIndex = 0;
            this.label89.Text = "Product Name:";
            // 
            // pnlComputeExcessShortFall
            // 
            this.pnlComputeExcessShortFall.Controls.Add(this.label80);
            this.pnlComputeExcessShortFall.Controls.Add(this.btnCmputeExcessShortOK);
            this.pnlComputeExcessShortFall.Controls.Add(this.grpBoxlIstOfStocks);
            this.pnlComputeExcessShortFall.Controls.Add(this.lblComputeExcessSHortFall);
            this.pnlComputeExcessShortFall.Controls.Add(this.picBoxLogoPnlComputeExcessShrt);
            this.pnlComputeExcessShortFall.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlComputeExcessShortFall.Location = new System.Drawing.Point(0, 0);
            this.pnlComputeExcessShortFall.Name = "pnlComputeExcessShortFall";
            this.pnlComputeExcessShortFall.Size = new System.Drawing.Size(554, 414);
            this.pnlComputeExcessShortFall.TabIndex = 24;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(35, 111);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(329, 17);
            this.label80.TabIndex = 52;
            this.label80.Text = "Please select the stock to for computing excess/shortfall";
            // 
            // btnCmputeExcessShortOK
            // 
            this.btnCmputeExcessShortOK.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCmputeExcessShortOK.Location = new System.Drawing.Point(449, 339);
            this.btnCmputeExcessShortOK.Name = "btnCmputeExcessShortOK";
            this.btnCmputeExcessShortOK.Size = new System.Drawing.Size(75, 23);
            this.btnCmputeExcessShortOK.TabIndex = 50;
            this.btnCmputeExcessShortOK.Text = "OK";
            this.btnCmputeExcessShortOK.UseVisualStyleBackColor = true;
            // 
            // grpBoxlIstOfStocks
            // 
            this.grpBoxlIstOfStocks.Controls.Add(this.listBxListOfStock);
            this.grpBoxlIstOfStocks.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxlIstOfStocks.Location = new System.Drawing.Point(28, 135);
            this.grpBoxlIstOfStocks.Name = "grpBoxlIstOfStocks";
            this.grpBoxlIstOfStocks.Size = new System.Drawing.Size(500, 180);
            this.grpBoxlIstOfStocks.TabIndex = 49;
            this.grpBoxlIstOfStocks.TabStop = false;
            this.grpBoxlIstOfStocks.Text = "List Of Stocks";
            // 
            // listBxListOfStock
            // 
            this.listBxListOfStock.FormattingEnabled = true;
            this.listBxListOfStock.ItemHeight = 15;
            this.listBxListOfStock.Location = new System.Drawing.Point(12, 26);
            this.listBxListOfStock.Name = "listBxListOfStock";
            this.listBxListOfStock.Size = new System.Drawing.Size(475, 139);
            this.listBxListOfStock.TabIndex = 44;
            // 
            // lblComputeExcessSHortFall
            // 
            this.lblComputeExcessSHortFall.AutoSize = true;
            this.lblComputeExcessSHortFall.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComputeExcessSHortFall.Location = new System.Drawing.Point(120, 60);
            this.lblComputeExcessSHortFall.Name = "lblComputeExcessSHortFall";
            this.lblComputeExcessSHortFall.Size = new System.Drawing.Size(230, 26);
            this.lblComputeExcessSHortFall.TabIndex = 48;
            this.lblComputeExcessSHortFall.Text = "List of Stock to Compute";
            // 
            // picBoxLogoPnlComputeExcessShrt
            // 
            this.picBoxLogoPnlComputeExcessShrt.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoPnlComputeExcessShrt.Image")));
            this.picBoxLogoPnlComputeExcessShrt.Location = new System.Drawing.Point(430, 37);
            this.picBoxLogoPnlComputeExcessShrt.Name = "picBoxLogoPnlComputeExcessShrt";
            this.picBoxLogoPnlComputeExcessShrt.Size = new System.Drawing.Size(100, 81);
            this.picBoxLogoPnlComputeExcessShrt.TabIndex = 47;
            this.picBoxLogoPnlComputeExcessShrt.TabStop = false;
            // 
            // pnlSearchAndDisplayStock
            // 
            this.pnlSearchAndDisplayStock.Controls.Add(this.btnDispStockInfoOK);
            this.pnlSearchAndDisplayStock.Controls.Add(this.grpBoxDispStockInfo);
            this.pnlSearchAndDisplayStock.Controls.Add(this.lblDispExistingStock);
            this.pnlSearchAndDisplayStock.Controls.Add(this.picBoxLogoPnlDispStockInfo);
            this.pnlSearchAndDisplayStock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchAndDisplayStock.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchAndDisplayStock.Name = "pnlSearchAndDisplayStock";
            this.pnlSearchAndDisplayStock.Size = new System.Drawing.Size(554, 414);
            this.pnlSearchAndDisplayStock.TabIndex = 25;
            // 
            // btnDispStockInfoOK
            // 
            this.btnDispStockInfoOK.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDispStockInfoOK.Location = new System.Drawing.Point(449, 339);
            this.btnDispStockInfoOK.Name = "btnDispStockInfoOK";
            this.btnDispStockInfoOK.Size = new System.Drawing.Size(75, 23);
            this.btnDispStockInfoOK.TabIndex = 50;
            this.btnDispStockInfoOK.Text = "OK";
            this.btnDispStockInfoOK.UseVisualStyleBackColor = true;
            // 
            // grpBoxDispStockInfo
            // 
            this.grpBoxDispStockInfo.Controls.Add(this.listBxDispStockInfo);
            this.grpBoxDispStockInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxDispStockInfo.Location = new System.Drawing.Point(28, 135);
            this.grpBoxDispStockInfo.Name = "grpBoxDispStockInfo";
            this.grpBoxDispStockInfo.Size = new System.Drawing.Size(500, 180);
            this.grpBoxDispStockInfo.TabIndex = 49;
            this.grpBoxDispStockInfo.TabStop = false;
            this.grpBoxDispStockInfo.Text = "View Stock Information";
            // 
            // listBxDispStockInfo
            // 
            this.listBxDispStockInfo.FormattingEnabled = true;
            this.listBxDispStockInfo.ItemHeight = 15;
            this.listBxDispStockInfo.Location = new System.Drawing.Point(12, 26);
            this.listBxDispStockInfo.Name = "listBxDispStockInfo";
            this.listBxDispStockInfo.Size = new System.Drawing.Size(475, 139);
            this.listBxDispStockInfo.TabIndex = 44;
            // 
            // lblDispExistingStock
            // 
            this.lblDispExistingStock.AutoSize = true;
            this.lblDispExistingStock.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispExistingStock.Location = new System.Drawing.Point(121, 60);
            this.lblDispExistingStock.Name = "lblDispExistingStock";
            this.lblDispExistingStock.Size = new System.Drawing.Size(243, 26);
            this.lblDispExistingStock.TabIndex = 48;
            this.lblDispExistingStock.Text = "Display Stock Information";
            // 
            // picBoxLogoPnlDispStockInfo
            // 
            this.picBoxLogoPnlDispStockInfo.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoPnlDispStockInfo.Image")));
            this.picBoxLogoPnlDispStockInfo.Location = new System.Drawing.Point(430, 37);
            this.picBoxLogoPnlDispStockInfo.Name = "picBoxLogoPnlDispStockInfo";
            this.picBoxLogoPnlDispStockInfo.Size = new System.Drawing.Size(100, 81);
            this.picBoxLogoPnlDispStockInfo.TabIndex = 47;
            this.picBoxLogoPnlDispStockInfo.TabStop = false;
            // 
            // pnlAddNewSupplierInfo
            // 
            this.pnlAddNewSupplierInfo.AutoScroll = true;
            this.pnlAddNewSupplierInfo.Controls.Add(this.lblInfoRegisterSupplier);
            this.pnlAddNewSupplierInfo.Controls.Add(this.tabControl4);
            this.pnlAddNewSupplierInfo.Controls.Add(this.lblRegisterSupplier);
            this.pnlAddNewSupplierInfo.Controls.Add(this.picBoxLogoRegisterSupplier);
            this.pnlAddNewSupplierInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAddNewSupplierInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlAddNewSupplierInfo.Name = "pnlAddNewSupplierInfo";
            this.pnlAddNewSupplierInfo.Size = new System.Drawing.Size(554, 414);
            this.pnlAddNewSupplierInfo.TabIndex = 26;
            // 
            // lblInfoRegisterSupplier
            // 
            this.lblInfoRegisterSupplier.AutoSize = true;
            this.lblInfoRegisterSupplier.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoRegisterSupplier.Location = new System.Drawing.Point(2, 129);
            this.lblInfoRegisterSupplier.Name = "lblInfoRegisterSupplier";
            this.lblInfoRegisterSupplier.Size = new System.Drawing.Size(192, 17);
            this.lblInfoRegisterSupplier.TabIndex = 50;
            this.lblInfoRegisterSupplier.Text = "Please fill in the required fields..";
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPgAddSupplier_SupplierInfo);
            this.tabControl4.Controls.Add(this.tabPgAddSupplier_ContactInfo);
            this.tabControl4.Location = new System.Drawing.Point(0, 149);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(554, 265);
            this.tabControl4.TabIndex = 49;
            // 
            // tabPgAddSupplier_SupplierInfo
            // 
            this.tabPgAddSupplier_SupplierInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tabPgAddSupplier_SupplierInfo.Controls.Add(this.label81);
            this.tabPgAddSupplier_SupplierInfo.Controls.Add(this.label82);
            this.tabPgAddSupplier_SupplierInfo.Controls.Add(this.groupBox16);
            this.tabPgAddSupplier_SupplierInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPgAddSupplier_SupplierInfo.Name = "tabPgAddSupplier_SupplierInfo";
            this.tabPgAddSupplier_SupplierInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgAddSupplier_SupplierInfo.Size = new System.Drawing.Size(546, 239);
            this.tabPgAddSupplier_SupplierInfo.TabIndex = 0;
            this.tabPgAddSupplier_SupplierInfo.Text = "Supplier Information";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(30, 217);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(349, 13);
            this.label81.TabIndex = 54;
            this.label81.Text = "Please proceed to fill in required fields in the \'Contact Information\' section";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(13, 15);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(146, 15);
            this.label82.TabIndex = 52;
            this.label82.Text = "* Denotes mandatory fields";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.tbxRegisterCompanyName);
            this.groupBox16.Controls.Add(this.lblCompanyName);
            this.groupBox16.Controls.Add(this.tbxRegisterSupplierName);
            this.groupBox16.Controls.Add(this.lblRegisterSupplierName);
            this.groupBox16.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox16.Location = new System.Drawing.Point(18, 47);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(507, 167);
            this.groupBox16.TabIndex = 51;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Particulars of Supplier";
            // 
            // tbxRegisterCompanyName
            // 
            this.tbxRegisterCompanyName.Location = new System.Drawing.Point(27, 108);
            this.tbxRegisterCompanyName.Name = "tbxRegisterCompanyName";
            this.tbxRegisterCompanyName.Size = new System.Drawing.Size(209, 23);
            this.tbxRegisterCompanyName.TabIndex = 7;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.Location = new System.Drawing.Point(24, 89);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(96, 15);
            this.lblCompanyName.TabIndex = 6;
            this.lblCompanyName.Text = "* Company Name:";
            // 
            // tbxRegisterSupplierName
            // 
            this.tbxRegisterSupplierName.Location = new System.Drawing.Point(27, 42);
            this.tbxRegisterSupplierName.Name = "tbxRegisterSupplierName";
            this.tbxRegisterSupplierName.Size = new System.Drawing.Size(209, 23);
            this.tbxRegisterSupplierName.TabIndex = 5;
            // 
            // lblRegisterSupplierName
            // 
            this.lblRegisterSupplierName.AutoSize = true;
            this.lblRegisterSupplierName.Location = new System.Drawing.Point(24, 22);
            this.lblRegisterSupplierName.Name = "lblRegisterSupplierName";
            this.lblRegisterSupplierName.Size = new System.Drawing.Size(110, 15);
            this.lblRegisterSupplierName.TabIndex = 2;
            this.lblRegisterSupplierName.Text = "* Name of Supplier:";
            // 
            // tabPgAddSupplier_ContactInfo
            // 
            this.tabPgAddSupplier_ContactInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tabPgAddSupplier_ContactInfo.Controls.Add(this.label83);
            this.tabPgAddSupplier_ContactInfo.Controls.Add(this.groupBox17);
            this.tabPgAddSupplier_ContactInfo.Controls.Add(this.btnCancelRegisterSupplier);
            this.tabPgAddSupplier_ContactInfo.Controls.Add(this.btnRegisterSupplier);
            this.tabPgAddSupplier_ContactInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPgAddSupplier_ContactInfo.Name = "tabPgAddSupplier_ContactInfo";
            this.tabPgAddSupplier_ContactInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgAddSupplier_ContactInfo.Size = new System.Drawing.Size(546, 239);
            this.tabPgAddSupplier_ContactInfo.TabIndex = 1;
            this.tabPgAddSupplier_ContactInfo.Text = "Contact Information";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(13, 8);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(146, 15);
            this.label83.TabIndex = 58;
            this.label83.Text = "* Denotes mandatory fields";
            // 
            // groupBox17
            // 
            this.groupBox17.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox17.Controls.Add(this.tbxRegisterCityCountry);
            this.groupBox17.Controls.Add(this.lblRegisterCityCountry);
            this.groupBox17.Controls.Add(this.lblRegisterFaxContact);
            this.groupBox17.Controls.Add(this.lblRegisterOfficeNum);
            this.groupBox17.Controls.Add(this.label84);
            this.groupBox17.Controls.Add(this.tbxRegisterFaxNum);
            this.groupBox17.Controls.Add(this.tbxRegisterPhoneOfficeNo);
            this.groupBox17.Controls.Add(this.tbxRegisterPostalCode);
            this.groupBox17.Controls.Add(this.label85);
            this.groupBox17.Controls.Add(this.tbxRegisterAddress_Supplier);
            this.groupBox17.Controls.Add(this.label86);
            this.groupBox17.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox17.Location = new System.Drawing.Point(20, 31);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(506, 170);
            this.groupBox17.TabIndex = 52;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Contact Information of Supplier";
            // 
            // tbxRegisterCityCountry
            // 
            this.tbxRegisterCityCountry.Location = new System.Drawing.Point(18, 132);
            this.tbxRegisterCityCountry.Name = "tbxRegisterCityCountry";
            this.tbxRegisterCityCountry.Size = new System.Drawing.Size(107, 23);
            this.tbxRegisterCityCountry.TabIndex = 54;
            // 
            // lblRegisterCityCountry
            // 
            this.lblRegisterCityCountry.AutoSize = true;
            this.lblRegisterCityCountry.Location = new System.Drawing.Point(22, 114);
            this.lblRegisterCityCountry.Name = "lblRegisterCityCountry";
            this.lblRegisterCityCountry.Size = new System.Drawing.Size(77, 15);
            this.lblRegisterCityCountry.TabIndex = 53;
            this.lblRegisterCityCountry.Text = "City, Country:";
            // 
            // lblRegisterFaxContact
            // 
            this.lblRegisterFaxContact.AutoSize = true;
            this.lblRegisterFaxContact.Location = new System.Drawing.Point(419, 79);
            this.lblRegisterFaxContact.Name = "lblRegisterFaxContact";
            this.lblRegisterFaxContact.Size = new System.Drawing.Size(34, 15);
            this.lblRegisterFaxContact.TabIndex = 50;
            this.lblRegisterFaxContact.Text = "(Fax)";
            // 
            // lblRegisterOfficeNum
            // 
            this.lblRegisterOfficeNum.AutoSize = true;
            this.lblRegisterOfficeNum.Location = new System.Drawing.Point(419, 47);
            this.lblRegisterOfficeNum.Name = "lblRegisterOfficeNum";
            this.lblRegisterOfficeNum.Size = new System.Drawing.Size(45, 15);
            this.lblRegisterOfficeNum.TabIndex = 49;
            this.lblRegisterOfficeNum.Text = "(Phone)";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(287, 25);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(106, 15);
            this.label84.TabIndex = 48;
            this.label84.Text = "* Contact Numbers:";
            // 
            // tbxRegisterFaxNum
            // 
            this.tbxRegisterFaxNum.Location = new System.Drawing.Point(288, 76);
            this.tbxRegisterFaxNum.Name = "tbxRegisterFaxNum";
            this.tbxRegisterFaxNum.Size = new System.Drawing.Size(119, 23);
            this.tbxRegisterFaxNum.TabIndex = 47;
            // 
            // tbxRegisterPhoneOfficeNo
            // 
            this.tbxRegisterPhoneOfficeNo.Location = new System.Drawing.Point(288, 43);
            this.tbxRegisterPhoneOfficeNo.Name = "tbxRegisterPhoneOfficeNo";
            this.tbxRegisterPhoneOfficeNo.Size = new System.Drawing.Size(119, 23);
            this.tbxRegisterPhoneOfficeNo.TabIndex = 46;
            // 
            // tbxRegisterPostalCode
            // 
            this.tbxRegisterPostalCode.Location = new System.Drawing.Point(151, 132);
            this.tbxRegisterPostalCode.Name = "tbxRegisterPostalCode";
            this.tbxRegisterPostalCode.Size = new System.Drawing.Size(75, 23);
            this.tbxRegisterPostalCode.TabIndex = 45;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(148, 114);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(68, 15);
            this.label85.TabIndex = 44;
            this.label85.Text = "Postal Code:";
            // 
            // tbxRegisterAddress_Supplier
            // 
            this.tbxRegisterAddress_Supplier.Location = new System.Drawing.Point(17, 44);
            this.tbxRegisterAddress_Supplier.Multiline = true;
            this.tbxRegisterAddress_Supplier.Name = "tbxRegisterAddress_Supplier";
            this.tbxRegisterAddress_Supplier.Size = new System.Drawing.Size(209, 55);
            this.tbxRegisterAddress_Supplier.TabIndex = 42;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(16, 25);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(60, 15);
            this.label86.TabIndex = 37;
            this.label86.Text = "* Address:";
            // 
            // btnCancelRegisterSupplier
            // 
            this.btnCancelRegisterSupplier.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelRegisterSupplier.Location = new System.Drawing.Point(451, 205);
            this.btnCancelRegisterSupplier.Name = "btnCancelRegisterSupplier";
            this.btnCancelRegisterSupplier.Size = new System.Drawing.Size(75, 23);
            this.btnCancelRegisterSupplier.TabIndex = 52;
            this.btnCancelRegisterSupplier.Text = "Cancel";
            this.btnCancelRegisterSupplier.UseVisualStyleBackColor = true;
            this.btnCancelRegisterSupplier.Click += new System.EventHandler(this.btnCancelRegisterSupplier_Click);
            // 
            // btnRegisterSupplier
            // 
            this.btnRegisterSupplier.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegisterSupplier.Location = new System.Drawing.Point(361, 205);
            this.btnRegisterSupplier.Name = "btnRegisterSupplier";
            this.btnRegisterSupplier.Size = new System.Drawing.Size(75, 23);
            this.btnRegisterSupplier.TabIndex = 51;
            this.btnRegisterSupplier.Text = "Register";
            this.btnRegisterSupplier.UseVisualStyleBackColor = true;
            this.btnRegisterSupplier.Click += new System.EventHandler(this.btnRegisterSupplier_Click);
            // 
            // lblRegisterSupplier
            // 
            this.lblRegisterSupplier.AutoSize = true;
            this.lblRegisterSupplier.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegisterSupplier.Location = new System.Drawing.Point(85, 60);
            this.lblRegisterSupplier.Name = "lblRegisterSupplier";
            this.lblRegisterSupplier.Size = new System.Drawing.Size(281, 26);
            this.lblRegisterSupplier.TabIndex = 41;
            this.lblRegisterSupplier.Text = "Add New Supplier Information";
            // 
            // picBoxLogoRegisterSupplier
            // 
            this.picBoxLogoRegisterSupplier.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoRegisterSupplier.Image")));
            this.picBoxLogoRegisterSupplier.Location = new System.Drawing.Point(430, 37);
            this.picBoxLogoRegisterSupplier.Name = "picBoxLogoRegisterSupplier";
            this.picBoxLogoRegisterSupplier.Size = new System.Drawing.Size(100, 81);
            this.picBoxLogoRegisterSupplier.TabIndex = 39;
            this.picBoxLogoRegisterSupplier.TabStop = false;
            // 
            // pnlModifySupplierInfo
            // 
            this.pnlModifySupplierInfo.AutoScroll = true;
            this.pnlModifySupplierInfo.Controls.Add(this.tabControl5);
            this.pnlModifySupplierInfo.Controls.Add(this.lblModifySupplierInfo);
            this.pnlModifySupplierInfo.Controls.Add(this.lblModifySupplierInformation);
            this.pnlModifySupplierInfo.Controls.Add(this.picBoxLogoModifySupplierInfoPnl);
            this.pnlModifySupplierInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlModifySupplierInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlModifySupplierInfo.Name = "pnlModifySupplierInfo";
            this.pnlModifySupplierInfo.Size = new System.Drawing.Size(554, 414);
            this.pnlModifySupplierInfo.TabIndex = 27;
            // 
            // tabControl5
            // 
            this.tabControl5.Controls.Add(this.tabPgModifySupplier_SupplierInfo);
            this.tabControl5.Controls.Add(this.tabPgModifySupplier_ContactInfo);
            this.tabControl5.Location = new System.Drawing.Point(0, 149);
            this.tabControl5.Name = "tabControl5";
            this.tabControl5.SelectedIndex = 0;
            this.tabControl5.Size = new System.Drawing.Size(554, 265);
            this.tabControl5.TabIndex = 51;
            // 
            // tabPgModifySupplier_SupplierInfo
            // 
            this.tabPgModifySupplier_SupplierInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tabPgModifySupplier_SupplierInfo.Controls.Add(this.label87);
            this.tabPgModifySupplier_SupplierInfo.Controls.Add(this.label88);
            this.tabPgModifySupplier_SupplierInfo.Controls.Add(this.groupBox18);
            this.tabPgModifySupplier_SupplierInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPgModifySupplier_SupplierInfo.Name = "tabPgModifySupplier_SupplierInfo";
            this.tabPgModifySupplier_SupplierInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgModifySupplier_SupplierInfo.Size = new System.Drawing.Size(546, 239);
            this.tabPgModifySupplier_SupplierInfo.TabIndex = 0;
            this.tabPgModifySupplier_SupplierInfo.Text = "Supplier Information";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(30, 217);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(349, 13);
            this.label87.TabIndex = 57;
            this.label87.Text = "Please proceed to fill in required fields in the \'Contact Information\' section";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(13, 15);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(146, 15);
            this.label88.TabIndex = 56;
            this.label88.Text = "* Denotes mandatory fields";
            // 
            // groupBox18
            // 
            this.groupBox18.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox18.Controls.Add(this.lblModifySupplier_DispCompName);
            this.groupBox18.Controls.Add(this.label90);
            this.groupBox18.Controls.Add(this.tbxModifySupplier_Name);
            this.groupBox18.Controls.Add(this.label94);
            this.groupBox18.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox18.Location = new System.Drawing.Point(18, 47);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(507, 169);
            this.groupBox18.TabIndex = 55;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Particulars of Supplier";
            // 
            // lblModifySupplier_DispCompName
            // 
            this.lblModifySupplier_DispCompName.AutoSize = true;
            this.lblModifySupplier_DispCompName.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifySupplier_DispCompName.Location = new System.Drawing.Point(24, 108);
            this.lblModifySupplier_DispCompName.Name = "lblModifySupplier_DispCompName";
            this.lblModifySupplier_DispCompName.Size = new System.Drawing.Size(98, 15);
            this.lblModifySupplier_DispCompName.TabIndex = 11;
            this.lblModifySupplier_DispCompName.Text = "- Company Name -";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(24, 89);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(96, 15);
            this.label90.TabIndex = 10;
            this.label90.Text = "* Company Name:";
            // 
            // tbxModifySupplier_Name
            // 
            this.tbxModifySupplier_Name.Location = new System.Drawing.Point(27, 42);
            this.tbxModifySupplier_Name.Name = "tbxModifySupplier_Name";
            this.tbxModifySupplier_Name.Size = new System.Drawing.Size(209, 23);
            this.tbxModifySupplier_Name.TabIndex = 9;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(24, 22);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(110, 15);
            this.label94.TabIndex = 8;
            this.label94.Text = "* Name of Supplier:";
            // 
            // tabPgModifySupplier_ContactInfo
            // 
            this.tabPgModifySupplier_ContactInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tabPgModifySupplier_ContactInfo.Controls.Add(this.btnMdifySupplier_Cancel);
            this.tabPgModifySupplier_ContactInfo.Controls.Add(this.btnModifySupplier_Update);
            this.tabPgModifySupplier_ContactInfo.Controls.Add(this.groupBox19);
            this.tabPgModifySupplier_ContactInfo.Controls.Add(this.label101);
            this.tabPgModifySupplier_ContactInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPgModifySupplier_ContactInfo.Name = "tabPgModifySupplier_ContactInfo";
            this.tabPgModifySupplier_ContactInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgModifySupplier_ContactInfo.Size = new System.Drawing.Size(546, 239);
            this.tabPgModifySupplier_ContactInfo.TabIndex = 1;
            this.tabPgModifySupplier_ContactInfo.Text = "Contact Information";
            // 
            // btnMdifySupplier_Cancel
            // 
            this.btnMdifySupplier_Cancel.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMdifySupplier_Cancel.Location = new System.Drawing.Point(451, 205);
            this.btnMdifySupplier_Cancel.Name = "btnMdifySupplier_Cancel";
            this.btnMdifySupplier_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btnMdifySupplier_Cancel.TabIndex = 60;
            this.btnMdifySupplier_Cancel.Text = "Cancel";
            this.btnMdifySupplier_Cancel.UseVisualStyleBackColor = true;
            this.btnMdifySupplier_Cancel.Click += new System.EventHandler(this.btnMdifySupplier_Cancel_Click);
            // 
            // btnModifySupplier_Update
            // 
            this.btnModifySupplier_Update.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifySupplier_Update.Location = new System.Drawing.Point(361, 205);
            this.btnModifySupplier_Update.Name = "btnModifySupplier_Update";
            this.btnModifySupplier_Update.Size = new System.Drawing.Size(75, 23);
            this.btnModifySupplier_Update.TabIndex = 59;
            this.btnModifySupplier_Update.Text = "Update";
            this.btnModifySupplier_Update.UseVisualStyleBackColor = true;
            this.btnModifySupplier_Update.Click += new System.EventHandler(this.btnModifySupplier_Update_Click);
            // 
            // groupBox19
            // 
            this.groupBox19.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox19.Controls.Add(this.tbxModifySupplier_CityCountry);
            this.groupBox19.Controls.Add(this.label95);
            this.groupBox19.Controls.Add(this.label96);
            this.groupBox19.Controls.Add(this.label97);
            this.groupBox19.Controls.Add(this.label98);
            this.groupBox19.Controls.Add(this.tbxModifySupplier_Fax);
            this.groupBox19.Controls.Add(this.tbxModifySupplier_Phone);
            this.groupBox19.Controls.Add(this.tbxModifySupplier_PostalCode);
            this.groupBox19.Controls.Add(this.label99);
            this.groupBox19.Controls.Add(this.tbxModifySupplier_Address);
            this.groupBox19.Controls.Add(this.label100);
            this.groupBox19.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox19.Location = new System.Drawing.Point(20, 31);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(506, 168);
            this.groupBox19.TabIndex = 58;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Contact Information of Supplier";
            // 
            // tbxModifySupplier_CityCountry
            // 
            this.tbxModifySupplier_CityCountry.Location = new System.Drawing.Point(19, 132);
            this.tbxModifySupplier_CityCountry.Name = "tbxModifySupplier_CityCountry";
            this.tbxModifySupplier_CityCountry.Size = new System.Drawing.Size(106, 23);
            this.tbxModifySupplier_CityCountry.TabIndex = 56;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(22, 114);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(77, 15);
            this.label95.TabIndex = 55;
            this.label95.Text = "City, Country:";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(419, 79);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(34, 15);
            this.label96.TabIndex = 50;
            this.label96.Text = "(Fax)";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(419, 47);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(45, 15);
            this.label97.TabIndex = 49;
            this.label97.Text = "(Phone)";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(287, 25);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(106, 15);
            this.label98.TabIndex = 48;
            this.label98.Text = "* Contact Numbers:";
            // 
            // tbxModifySupplier_Fax
            // 
            this.tbxModifySupplier_Fax.Location = new System.Drawing.Point(288, 76);
            this.tbxModifySupplier_Fax.Name = "tbxModifySupplier_Fax";
            this.tbxModifySupplier_Fax.Size = new System.Drawing.Size(119, 23);
            this.tbxModifySupplier_Fax.TabIndex = 47;
            // 
            // tbxModifySupplier_Phone
            // 
            this.tbxModifySupplier_Phone.Location = new System.Drawing.Point(288, 43);
            this.tbxModifySupplier_Phone.Name = "tbxModifySupplier_Phone";
            this.tbxModifySupplier_Phone.Size = new System.Drawing.Size(119, 23);
            this.tbxModifySupplier_Phone.TabIndex = 46;
            // 
            // tbxModifySupplier_PostalCode
            // 
            this.tbxModifySupplier_PostalCode.Location = new System.Drawing.Point(151, 132);
            this.tbxModifySupplier_PostalCode.Name = "tbxModifySupplier_PostalCode";
            this.tbxModifySupplier_PostalCode.Size = new System.Drawing.Size(75, 23);
            this.tbxModifySupplier_PostalCode.TabIndex = 45;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(148, 114);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(68, 15);
            this.label99.TabIndex = 44;
            this.label99.Text = "Postal Code:";
            // 
            // tbxModifySupplier_Address
            // 
            this.tbxModifySupplier_Address.Location = new System.Drawing.Point(17, 44);
            this.tbxModifySupplier_Address.Multiline = true;
            this.tbxModifySupplier_Address.Name = "tbxModifySupplier_Address";
            this.tbxModifySupplier_Address.Size = new System.Drawing.Size(209, 55);
            this.tbxModifySupplier_Address.TabIndex = 42;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(16, 25);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(60, 15);
            this.label100.TabIndex = 37;
            this.label100.Text = "* Address:";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(13, 8);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(146, 15);
            this.label101.TabIndex = 57;
            this.label101.Text = "* Denotes mandatory fields";
            // 
            // lblModifySupplierInfo
            // 
            this.lblModifySupplierInfo.AutoSize = true;
            this.lblModifySupplierInfo.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifySupplierInfo.Location = new System.Drawing.Point(2, 129);
            this.lblModifySupplierInfo.Name = "lblModifySupplierInfo";
            this.lblModifySupplierInfo.Size = new System.Drawing.Size(341, 17);
            this.lblModifySupplierInfo.TabIndex = 47;
            this.lblModifySupplierInfo.Text = "Please enter the respective supplier information to modify...";
            // 
            // lblModifySupplierInformation
            // 
            this.lblModifySupplierInformation.AutoSize = true;
            this.lblModifySupplierInformation.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifySupplierInformation.Location = new System.Drawing.Point(120, 60);
            this.lblModifySupplierInformation.Name = "lblModifySupplierInformation";
            this.lblModifySupplierInformation.Size = new System.Drawing.Size(263, 26);
            this.lblModifySupplierInformation.TabIndex = 42;
            this.lblModifySupplierInformation.Text = "Modify Supplier Information";
            // 
            // picBoxLogoModifySupplierInfoPnl
            // 
            this.picBoxLogoModifySupplierInfoPnl.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoModifySupplierInfoPnl.Image")));
            this.picBoxLogoModifySupplierInfoPnl.Location = new System.Drawing.Point(430, 37);
            this.picBoxLogoModifySupplierInfoPnl.Name = "picBoxLogoModifySupplierInfoPnl";
            this.picBoxLogoModifySupplierInfoPnl.Size = new System.Drawing.Size(100, 81);
            this.picBoxLogoModifySupplierInfoPnl.TabIndex = 40;
            this.picBoxLogoModifySupplierInfoPnl.TabStop = false;
            // 
            // pnlDeleteSupplierInfo
            // 
            this.pnlDeleteSupplierInfo.Controls.Add(this.tabControl6);
            this.pnlDeleteSupplierInfo.Controls.Add(this.lblCfmDeleteSupplier);
            this.pnlDeleteSupplierInfo.Controls.Add(this.pictureBox9);
            this.pnlDeleteSupplierInfo.Controls.Add(this.label111);
            this.pnlDeleteSupplierInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDeleteSupplierInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlDeleteSupplierInfo.Name = "pnlDeleteSupplierInfo";
            this.pnlDeleteSupplierInfo.Size = new System.Drawing.Size(554, 414);
            this.pnlDeleteSupplierInfo.TabIndex = 28;
            // 
            // tabControl6
            // 
            this.tabControl6.Controls.Add(this.tabPgDeleteSupplier_SupplierInfo);
            this.tabControl6.Controls.Add(this.tabPgDeleteSupplier_ContactInfo);
            this.tabControl6.Location = new System.Drawing.Point(1, 150);
            this.tabControl6.Name = "tabControl6";
            this.tabControl6.SelectedIndex = 0;
            this.tabControl6.Size = new System.Drawing.Size(554, 265);
            this.tabControl6.TabIndex = 51;
            // 
            // tabPgDeleteSupplier_SupplierInfo
            // 
            this.tabPgDeleteSupplier_SupplierInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tabPgDeleteSupplier_SupplierInfo.Controls.Add(this.label102);
            this.tabPgDeleteSupplier_SupplierInfo.Controls.Add(this.groupBox20);
            this.tabPgDeleteSupplier_SupplierInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPgDeleteSupplier_SupplierInfo.Name = "tabPgDeleteSupplier_SupplierInfo";
            this.tabPgDeleteSupplier_SupplierInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgDeleteSupplier_SupplierInfo.Size = new System.Drawing.Size(546, 239);
            this.tabPgDeleteSupplier_SupplierInfo.TabIndex = 0;
            this.tabPgDeleteSupplier_SupplierInfo.Text = "Supplier Information";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(30, 217);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(272, 13);
            this.label102.TabIndex = 54;
            this.label102.Text = "Please proceed to view the \'Contact Information\' section";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.lblDeleteSupplier_CompanyName);
            this.groupBox20.Controls.Add(this.lblDeleteSupplier_SupplierName);
            this.groupBox20.Controls.Add(this.label103);
            this.groupBox20.Controls.Add(this.label104);
            this.groupBox20.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox20.Location = new System.Drawing.Point(18, 28);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(507, 167);
            this.groupBox20.TabIndex = 51;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Particulars of Supplier";
            // 
            // lblDeleteSupplier_CompanyName
            // 
            this.lblDeleteSupplier_CompanyName.AutoSize = true;
            this.lblDeleteSupplier_CompanyName.Location = new System.Drawing.Point(27, 110);
            this.lblDeleteSupplier_CompanyName.Name = "lblDeleteSupplier_CompanyName";
            this.lblDeleteSupplier_CompanyName.Size = new System.Drawing.Size(98, 15);
            this.lblDeleteSupplier_CompanyName.TabIndex = 9;
            this.lblDeleteSupplier_CompanyName.Text = "- Company Name -";
            // 
            // lblDeleteSupplier_SupplierName
            // 
            this.lblDeleteSupplier_SupplierName.AutoSize = true;
            this.lblDeleteSupplier_SupplierName.Location = new System.Drawing.Point(27, 42);
            this.lblDeleteSupplier_SupplierName.Name = "lblDeleteSupplier_SupplierName";
            this.lblDeleteSupplier_SupplierName.Size = new System.Drawing.Size(97, 15);
            this.lblDeleteSupplier_SupplierName.TabIndex = 8;
            this.lblDeleteSupplier_SupplierName.Text = "- Supplier Name -";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(24, 89);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(87, 15);
            this.label103.TabIndex = 6;
            this.label103.Text = "Company Name:";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(24, 22);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(101, 15);
            this.label104.TabIndex = 2;
            this.label104.Text = "Name of Supplier:";
            // 
            // tabPgDeleteSupplier_ContactInfo
            // 
            this.tabPgDeleteSupplier_ContactInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tabPgDeleteSupplier_ContactInfo.Controls.Add(this.groupBox21);
            this.tabPgDeleteSupplier_ContactInfo.Controls.Add(this.button3);
            this.tabPgDeleteSupplier_ContactInfo.Controls.Add(this.btnDeleteSupplier);
            this.tabPgDeleteSupplier_ContactInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPgDeleteSupplier_ContactInfo.Name = "tabPgDeleteSupplier_ContactInfo";
            this.tabPgDeleteSupplier_ContactInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgDeleteSupplier_ContactInfo.Size = new System.Drawing.Size(546, 239);
            this.tabPgDeleteSupplier_ContactInfo.TabIndex = 1;
            this.tabPgDeleteSupplier_ContactInfo.Text = "Contact Information";
            // 
            // groupBox21
            // 
            this.groupBox21.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox21.Controls.Add(this.lblDeleteSupplier_PostalCode);
            this.groupBox21.Controls.Add(this.lblDeleteSupplier_CityCountry);
            this.groupBox21.Controls.Add(this.lblDeleteSupplier_FaxNum);
            this.groupBox21.Controls.Add(this.lblDeleteSupplier_PhoneNum);
            this.groupBox21.Controls.Add(this.label105);
            this.groupBox21.Controls.Add(this.label106);
            this.groupBox21.Controls.Add(this.label107);
            this.groupBox21.Controls.Add(this.label108);
            this.groupBox21.Controls.Add(this.label109);
            this.groupBox21.Controls.Add(this.tbxDeleteSupplier_Address);
            this.groupBox21.Controls.Add(this.label110);
            this.groupBox21.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox21.Location = new System.Drawing.Point(20, 31);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(506, 170);
            this.groupBox21.TabIndex = 52;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Contact Information of Supplier";
            // 
            // lblDeleteSupplier_PostalCode
            // 
            this.lblDeleteSupplier_PostalCode.AutoSize = true;
            this.lblDeleteSupplier_PostalCode.Location = new System.Drawing.Point(148, 140);
            this.lblDeleteSupplier_PostalCode.Name = "lblDeleteSupplier_PostalCode";
            this.lblDeleteSupplier_PostalCode.Size = new System.Drawing.Size(79, 15);
            this.lblDeleteSupplier_PostalCode.TabIndex = 58;
            this.lblDeleteSupplier_PostalCode.Text = "- Postal Code -";
            // 
            // lblDeleteSupplier_CityCountry
            // 
            this.lblDeleteSupplier_CityCountry.AutoSize = true;
            this.lblDeleteSupplier_CityCountry.Location = new System.Drawing.Point(21, 140);
            this.lblDeleteSupplier_CityCountry.Name = "lblDeleteSupplier_CityCountry";
            this.lblDeleteSupplier_CityCountry.Size = new System.Drawing.Size(88, 15);
            this.lblDeleteSupplier_CityCountry.TabIndex = 57;
            this.lblDeleteSupplier_CityCountry.Text = "- City, Country -";
            // 
            // lblDeleteSupplier_FaxNum
            // 
            this.lblDeleteSupplier_FaxNum.AutoSize = true;
            this.lblDeleteSupplier_FaxNum.Location = new System.Drawing.Point(288, 79);
            this.lblDeleteSupplier_FaxNum.Name = "lblDeleteSupplier_FaxNum";
            this.lblDeleteSupplier_FaxNum.Size = new System.Drawing.Size(72, 15);
            this.lblDeleteSupplier_FaxNum.TabIndex = 56;
            this.lblDeleteSupplier_FaxNum.Text = "- Fax Name -";
            // 
            // lblDeleteSupplier_PhoneNum
            // 
            this.lblDeleteSupplier_PhoneNum.AutoSize = true;
            this.lblDeleteSupplier_PhoneNum.Location = new System.Drawing.Point(288, 46);
            this.lblDeleteSupplier_PhoneNum.Name = "lblDeleteSupplier_PhoneNum";
            this.lblDeleteSupplier_PhoneNum.Size = new System.Drawing.Size(77, 15);
            this.lblDeleteSupplier_PhoneNum.TabIndex = 55;
            this.lblDeleteSupplier_PhoneNum.Text = "- Phone Num -";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(22, 114);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(77, 15);
            this.label105.TabIndex = 53;
            this.label105.Text = "City, Country:";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(419, 79);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(34, 15);
            this.label106.TabIndex = 50;
            this.label106.Text = "(Fax)";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(419, 47);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(45, 15);
            this.label107.TabIndex = 49;
            this.label107.Text = "(Phone)";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(287, 25);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(97, 15);
            this.label108.TabIndex = 48;
            this.label108.Text = "Contact Numbers:";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(148, 114);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(68, 15);
            this.label109.TabIndex = 44;
            this.label109.Text = "Postal Code:";
            // 
            // tbxDeleteSupplier_Address
            // 
            this.tbxDeleteSupplier_Address.Location = new System.Drawing.Point(17, 44);
            this.tbxDeleteSupplier_Address.Multiline = true;
            this.tbxDeleteSupplier_Address.Name = "tbxDeleteSupplier_Address";
            this.tbxDeleteSupplier_Address.ReadOnly = true;
            this.tbxDeleteSupplier_Address.Size = new System.Drawing.Size(209, 55);
            this.tbxDeleteSupplier_Address.TabIndex = 42;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(16, 25);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(51, 15);
            this.label110.TabIndex = 37;
            this.label110.Text = "Address:";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(451, 205);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 48;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnDeleteSupplier
            // 
            this.btnDeleteSupplier.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteSupplier.Location = new System.Drawing.Point(361, 205);
            this.btnDeleteSupplier.Name = "btnDeleteSupplier";
            this.btnDeleteSupplier.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteSupplier.TabIndex = 47;
            this.btnDeleteSupplier.Text = "Delete";
            this.btnDeleteSupplier.UseVisualStyleBackColor = true;
            this.btnDeleteSupplier.Click += new System.EventHandler(this.btnDeleteSupplier_Click);
            // 
            // lblCfmDeleteSupplier
            // 
            this.lblCfmDeleteSupplier.AutoSize = true;
            this.lblCfmDeleteSupplier.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCfmDeleteSupplier.Location = new System.Drawing.Point(13, 110);
            this.lblCfmDeleteSupplier.Name = "lblCfmDeleteSupplier";
            this.lblCfmDeleteSupplier.Size = new System.Drawing.Size(267, 17);
            this.lblCfmDeleteSupplier.TabIndex = 49;
            this.lblCfmDeleteSupplier.Text = "Please confirm supplier information to delete...";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(430, 37);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(100, 81);
            this.pictureBox9.TabIndex = 42;
            this.pictureBox9.TabStop = false;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(85, 60);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(332, 26);
            this.label111.TabIndex = 43;
            this.label111.Text = "Delete Existing Supplier Information";
            // 
            // pnlDisplaySupplierInformation_Details
            // 
            this.pnlDisplaySupplierInformation_Details.Controls.Add(this.tabControl7);
            this.pnlDisplaySupplierInformation_Details.Controls.Add(this.pictureBox10);
            this.pnlDisplaySupplierInformation_Details.Controls.Add(this.label121);
            this.pnlDisplaySupplierInformation_Details.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDisplaySupplierInformation_Details.Location = new System.Drawing.Point(0, 0);
            this.pnlDisplaySupplierInformation_Details.Name = "pnlDisplaySupplierInformation_Details";
            this.pnlDisplaySupplierInformation_Details.Size = new System.Drawing.Size(554, 414);
            this.pnlDisplaySupplierInformation_Details.TabIndex = 29;
            // 
            // tabControl7
            // 
            this.tabControl7.Controls.Add(this.tabPgDisplaySupplier_SupplierInfo);
            this.tabControl7.Controls.Add(this.tabPgDisplaySupplier_ContactInfo);
            this.tabControl7.Location = new System.Drawing.Point(2, 150);
            this.tabControl7.Name = "tabControl7";
            this.tabControl7.SelectedIndex = 0;
            this.tabControl7.Size = new System.Drawing.Size(554, 265);
            this.tabControl7.TabIndex = 55;
            // 
            // tabPgDisplaySupplier_SupplierInfo
            // 
            this.tabPgDisplaySupplier_SupplierInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tabPgDisplaySupplier_SupplierInfo.Controls.Add(this.label112);
            this.tabPgDisplaySupplier_SupplierInfo.Controls.Add(this.groupBox22);
            this.tabPgDisplaySupplier_SupplierInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPgDisplaySupplier_SupplierInfo.Name = "tabPgDisplaySupplier_SupplierInfo";
            this.tabPgDisplaySupplier_SupplierInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgDisplaySupplier_SupplierInfo.Size = new System.Drawing.Size(546, 239);
            this.tabPgDisplaySupplier_SupplierInfo.TabIndex = 0;
            this.tabPgDisplaySupplier_SupplierInfo.Text = "Supplier Information";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(30, 217);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(272, 13);
            this.label112.TabIndex = 54;
            this.label112.Text = "Please proceed to view the \'Contact Information\' section";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.lblDisplaySupplierInformation_Display_CompanyName);
            this.groupBox22.Controls.Add(this.lblDisplaySupplierInformation_Display_SupplierName);
            this.groupBox22.Controls.Add(this.label113);
            this.groupBox22.Controls.Add(this.label114);
            this.groupBox22.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox22.Location = new System.Drawing.Point(18, 28);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(507, 167);
            this.groupBox22.TabIndex = 51;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Particulars of Supplier";
            // 
            // lblDisplaySupplierInformation_Display_CompanyName
            // 
            this.lblDisplaySupplierInformation_Display_CompanyName.AutoSize = true;
            this.lblDisplaySupplierInformation_Display_CompanyName.Location = new System.Drawing.Point(27, 110);
            this.lblDisplaySupplierInformation_Display_CompanyName.Name = "lblDisplaySupplierInformation_Display_CompanyName";
            this.lblDisplaySupplierInformation_Display_CompanyName.Size = new System.Drawing.Size(98, 15);
            this.lblDisplaySupplierInformation_Display_CompanyName.TabIndex = 9;
            this.lblDisplaySupplierInformation_Display_CompanyName.Text = "- Company Name -";
            // 
            // lblDisplaySupplierInformation_Display_SupplierName
            // 
            this.lblDisplaySupplierInformation_Display_SupplierName.AutoSize = true;
            this.lblDisplaySupplierInformation_Display_SupplierName.Location = new System.Drawing.Point(27, 42);
            this.lblDisplaySupplierInformation_Display_SupplierName.Name = "lblDisplaySupplierInformation_Display_SupplierName";
            this.lblDisplaySupplierInformation_Display_SupplierName.Size = new System.Drawing.Size(97, 15);
            this.lblDisplaySupplierInformation_Display_SupplierName.TabIndex = 8;
            this.lblDisplaySupplierInformation_Display_SupplierName.Text = "- Supplier Name -";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(24, 89);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(87, 15);
            this.label113.TabIndex = 6;
            this.label113.Text = "Company Name:";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(24, 22);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(101, 15);
            this.label114.TabIndex = 2;
            this.label114.Text = "Name of Supplier:";
            // 
            // tabPgDisplaySupplier_ContactInfo
            // 
            this.tabPgDisplaySupplier_ContactInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tabPgDisplaySupplier_ContactInfo.Controls.Add(this.groupBox23);
            this.tabPgDisplaySupplier_ContactInfo.Controls.Add(this.btnDisplaySupplierInformation_Details_OK);
            this.tabPgDisplaySupplier_ContactInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPgDisplaySupplier_ContactInfo.Name = "tabPgDisplaySupplier_ContactInfo";
            this.tabPgDisplaySupplier_ContactInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgDisplaySupplier_ContactInfo.Size = new System.Drawing.Size(546, 239);
            this.tabPgDisplaySupplier_ContactInfo.TabIndex = 1;
            this.tabPgDisplaySupplier_ContactInfo.Text = "Contact Information";
            // 
            // groupBox23
            // 
            this.groupBox23.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox23.Controls.Add(this.lblDisplaySupplierInformation_Display_PostalCode);
            this.groupBox23.Controls.Add(this.lblDisplaySupplierInformation_Display_CityCountry);
            this.groupBox23.Controls.Add(this.lblDisplaySupplierInformation_Display_FaxNum);
            this.groupBox23.Controls.Add(this.lblDisplaySupplierInformation_Display_PhoneNum);
            this.groupBox23.Controls.Add(this.label115);
            this.groupBox23.Controls.Add(this.label116);
            this.groupBox23.Controls.Add(this.label117);
            this.groupBox23.Controls.Add(this.label118);
            this.groupBox23.Controls.Add(this.label119);
            this.groupBox23.Controls.Add(this.tbxDisplaySupplierInformation_Display_Address);
            this.groupBox23.Controls.Add(this.label120);
            this.groupBox23.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox23.Location = new System.Drawing.Point(20, 31);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(506, 170);
            this.groupBox23.TabIndex = 52;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Contact Information of Supplier";
            // 
            // lblDisplaySupplierInformation_Display_PostalCode
            // 
            this.lblDisplaySupplierInformation_Display_PostalCode.AutoSize = true;
            this.lblDisplaySupplierInformation_Display_PostalCode.Location = new System.Drawing.Point(148, 140);
            this.lblDisplaySupplierInformation_Display_PostalCode.Name = "lblDisplaySupplierInformation_Display_PostalCode";
            this.lblDisplaySupplierInformation_Display_PostalCode.Size = new System.Drawing.Size(79, 15);
            this.lblDisplaySupplierInformation_Display_PostalCode.TabIndex = 58;
            this.lblDisplaySupplierInformation_Display_PostalCode.Text = "- Postal Code -";
            // 
            // lblDisplaySupplierInformation_Display_CityCountry
            // 
            this.lblDisplaySupplierInformation_Display_CityCountry.AutoSize = true;
            this.lblDisplaySupplierInformation_Display_CityCountry.Location = new System.Drawing.Point(21, 140);
            this.lblDisplaySupplierInformation_Display_CityCountry.Name = "lblDisplaySupplierInformation_Display_CityCountry";
            this.lblDisplaySupplierInformation_Display_CityCountry.Size = new System.Drawing.Size(88, 15);
            this.lblDisplaySupplierInformation_Display_CityCountry.TabIndex = 57;
            this.lblDisplaySupplierInformation_Display_CityCountry.Text = "- City, Country -";
            // 
            // lblDisplaySupplierInformation_Display_FaxNum
            // 
            this.lblDisplaySupplierInformation_Display_FaxNum.AutoSize = true;
            this.lblDisplaySupplierInformation_Display_FaxNum.Location = new System.Drawing.Point(288, 79);
            this.lblDisplaySupplierInformation_Display_FaxNum.Name = "lblDisplaySupplierInformation_Display_FaxNum";
            this.lblDisplaySupplierInformation_Display_FaxNum.Size = new System.Drawing.Size(72, 15);
            this.lblDisplaySupplierInformation_Display_FaxNum.TabIndex = 56;
            this.lblDisplaySupplierInformation_Display_FaxNum.Text = "- Fax Name -";
            // 
            // lblDisplaySupplierInformation_Display_PhoneNum
            // 
            this.lblDisplaySupplierInformation_Display_PhoneNum.AutoSize = true;
            this.lblDisplaySupplierInformation_Display_PhoneNum.Location = new System.Drawing.Point(288, 46);
            this.lblDisplaySupplierInformation_Display_PhoneNum.Name = "lblDisplaySupplierInformation_Display_PhoneNum";
            this.lblDisplaySupplierInformation_Display_PhoneNum.Size = new System.Drawing.Size(77, 15);
            this.lblDisplaySupplierInformation_Display_PhoneNum.TabIndex = 55;
            this.lblDisplaySupplierInformation_Display_PhoneNum.Text = "- Phone Num -";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(22, 114);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(77, 15);
            this.label115.TabIndex = 53;
            this.label115.Text = "City, Country:";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(419, 79);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(34, 15);
            this.label116.TabIndex = 50;
            this.label116.Text = "(Fax)";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(419, 47);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(45, 15);
            this.label117.TabIndex = 49;
            this.label117.Text = "(Phone)";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.Location = new System.Drawing.Point(287, 25);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(97, 15);
            this.label118.TabIndex = 48;
            this.label118.Text = "Contact Numbers:";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(148, 114);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(68, 15);
            this.label119.TabIndex = 44;
            this.label119.Text = "Postal Code:";
            // 
            // tbxDisplaySupplierInformation_Display_Address
            // 
            this.tbxDisplaySupplierInformation_Display_Address.Location = new System.Drawing.Point(17, 44);
            this.tbxDisplaySupplierInformation_Display_Address.Multiline = true;
            this.tbxDisplaySupplierInformation_Display_Address.Name = "tbxDisplaySupplierInformation_Display_Address";
            this.tbxDisplaySupplierInformation_Display_Address.ReadOnly = true;
            this.tbxDisplaySupplierInformation_Display_Address.Size = new System.Drawing.Size(209, 55);
            this.tbxDisplaySupplierInformation_Display_Address.TabIndex = 42;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(16, 25);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(51, 15);
            this.label120.TabIndex = 37;
            this.label120.Text = "Address:";
            // 
            // btnDisplaySupplierInformation_Details_OK
            // 
            this.btnDisplaySupplierInformation_Details_OK.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisplaySupplierInformation_Details_OK.Location = new System.Drawing.Point(451, 205);
            this.btnDisplaySupplierInformation_Details_OK.Name = "btnDisplaySupplierInformation_Details_OK";
            this.btnDisplaySupplierInformation_Details_OK.Size = new System.Drawing.Size(75, 23);
            this.btnDisplaySupplierInformation_Details_OK.TabIndex = 48;
            this.btnDisplaySupplierInformation_Details_OK.Text = "OK";
            this.btnDisplaySupplierInformation_Details_OK.UseVisualStyleBackColor = true;
            this.btnDisplaySupplierInformation_Details_OK.Click += new System.EventHandler(this.btnDisplaySupplierInformation_Details_OK_Click);
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(431, 37);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(100, 81);
            this.pictureBox10.TabIndex = 52;
            this.pictureBox10.TabStop = false;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.Location = new System.Drawing.Point(86, 60);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(263, 26);
            this.label121.TabIndex = 53;
            this.label121.Text = "Display Supplier Information";
            // 
            // pnlDisplaySupplierInfo_ListOfCompanies
            // 
            this.pnlDisplaySupplierInfo_ListOfCompanies.Controls.Add(this.label122);
            this.pnlDisplaySupplierInfo_ListOfCompanies.Controls.Add(this.btnDispSupplierInfoOK);
            this.pnlDisplaySupplierInfo_ListOfCompanies.Controls.Add(this.grpBoxDispSupplier);
            this.pnlDisplaySupplierInfo_ListOfCompanies.Controls.Add(this.lblDispExistingSupplier);
            this.pnlDisplaySupplierInfo_ListOfCompanies.Controls.Add(this.picBoxLogoPnlDispSupplierInfo);
            this.pnlDisplaySupplierInfo_ListOfCompanies.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDisplaySupplierInfo_ListOfCompanies.Location = new System.Drawing.Point(0, 0);
            this.pnlDisplaySupplierInfo_ListOfCompanies.Name = "pnlDisplaySupplierInfo_ListOfCompanies";
            this.pnlDisplaySupplierInfo_ListOfCompanies.Size = new System.Drawing.Size(554, 414);
            this.pnlDisplaySupplierInfo_ListOfCompanies.TabIndex = 30;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.Location = new System.Drawing.Point(16, 110);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(294, 17);
            this.label122.TabIndex = 47;
            this.label122.Text = "Please select one of the company to view its details";
            // 
            // btnDispSupplierInfoOK
            // 
            this.btnDispSupplierInfoOK.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDispSupplierInfoOK.Location = new System.Drawing.Point(446, 334);
            this.btnDispSupplierInfoOK.Name = "btnDispSupplierInfoOK";
            this.btnDispSupplierInfoOK.Size = new System.Drawing.Size(75, 23);
            this.btnDispSupplierInfoOK.TabIndex = 46;
            this.btnDispSupplierInfoOK.Text = "OK";
            this.btnDispSupplierInfoOK.UseVisualStyleBackColor = true;
            this.btnDispSupplierInfoOK.Click += new System.EventHandler(this.btnDispSupplierInfoOK_Click);
            // 
            // grpBoxDispSupplier
            // 
            this.grpBoxDispSupplier.Controls.Add(this.listBxDispSupplierInfo);
            this.grpBoxDispSupplier.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxDispSupplier.Location = new System.Drawing.Point(16, 130);
            this.grpBoxDispSupplier.Name = "grpBoxDispSupplier";
            this.grpBoxDispSupplier.Size = new System.Drawing.Size(508, 180);
            this.grpBoxDispSupplier.TabIndex = 45;
            this.grpBoxDispSupplier.TabStop = false;
            this.grpBoxDispSupplier.Text = "View Company Names";
            // 
            // listBxDispSupplierInfo
            // 
            this.listBxDispSupplierInfo.FormattingEnabled = true;
            this.listBxDispSupplierInfo.ItemHeight = 15;
            this.listBxDispSupplierInfo.Location = new System.Drawing.Point(14, 26);
            this.listBxDispSupplierInfo.Name = "listBxDispSupplierInfo";
            this.listBxDispSupplierInfo.Size = new System.Drawing.Size(479, 139);
            this.listBxDispSupplierInfo.TabIndex = 44;
            // 
            // lblDispExistingSupplier
            // 
            this.lblDispExistingSupplier.AutoSize = true;
            this.lblDispExistingSupplier.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispExistingSupplier.Location = new System.Drawing.Point(120, 60);
            this.lblDispExistingSupplier.Name = "lblDispExistingSupplier";
            this.lblDispExistingSupplier.Size = new System.Drawing.Size(164, 26);
            this.lblDispExistingSupplier.TabIndex = 43;
            this.lblDispExistingSupplier.Text = "List of Companies";
            // 
            // picBoxLogoPnlDispSupplierInfo
            // 
            this.picBoxLogoPnlDispSupplierInfo.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoPnlDispSupplierInfo.Image")));
            this.picBoxLogoPnlDispSupplierInfo.Location = new System.Drawing.Point(430, 37);
            this.picBoxLogoPnlDispSupplierInfo.Name = "picBoxLogoPnlDispSupplierInfo";
            this.picBoxLogoPnlDispSupplierInfo.Size = new System.Drawing.Size(100, 81);
            this.picBoxLogoPnlDispSupplierInfo.TabIndex = 42;
            this.picBoxLogoPnlDispSupplierInfo.TabStop = false;
            // 
            // panelSelectAProductToDisplay
            // 
            this.panelSelectAProductToDisplay.Controls.Add(this.btnSelectAProductToDisplay);
            this.panelSelectAProductToDisplay.Controls.Add(this.pictureBox11);
            this.panelSelectAProductToDisplay.Controls.Add(this.btnCancelSelectAProductToDisplay);
            this.panelSelectAProductToDisplay.Controls.Add(this.groupBox24);
            this.panelSelectAProductToDisplay.Controls.Add(this.label145);
            this.panelSelectAProductToDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSelectAProductToDisplay.Location = new System.Drawing.Point(0, 0);
            this.panelSelectAProductToDisplay.Name = "panelSelectAProductToDisplay";
            this.panelSelectAProductToDisplay.Size = new System.Drawing.Size(554, 414);
            this.panelSelectAProductToDisplay.TabIndex = 31;
            // 
            // btnSelectAProductToDisplay
            // 
            this.btnSelectAProductToDisplay.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectAProductToDisplay.Location = new System.Drawing.Point(357, 381);
            this.btnSelectAProductToDisplay.Name = "btnSelectAProductToDisplay";
            this.btnSelectAProductToDisplay.Size = new System.Drawing.Size(75, 23);
            this.btnSelectAProductToDisplay.TabIndex = 41;
            this.btnSelectAProductToDisplay.Text = "Select";
            this.btnSelectAProductToDisplay.UseVisualStyleBackColor = true;
            this.btnSelectAProductToDisplay.Click += new System.EventHandler(this.btnSelectAProductToDisplay_Click);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(424, 33);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(100, 81);
            this.pictureBox11.TabIndex = 40;
            this.pictureBox11.TabStop = false;
            // 
            // btnCancelSelectAProductToDisplay
            // 
            this.btnCancelSelectAProductToDisplay.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelSelectAProductToDisplay.Location = new System.Drawing.Point(449, 381);
            this.btnCancelSelectAProductToDisplay.Name = "btnCancelSelectAProductToDisplay";
            this.btnCancelSelectAProductToDisplay.Size = new System.Drawing.Size(75, 23);
            this.btnCancelSelectAProductToDisplay.TabIndex = 4;
            this.btnCancelSelectAProductToDisplay.Text = "Cancel";
            this.btnCancelSelectAProductToDisplay.UseVisualStyleBackColor = true;
            this.btnCancelSelectAProductToDisplay.Click += new System.EventHandler(this.btnCancelSelectAProductToDisplay_Click);
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.listbxSelectAProductToDisplay);
            this.groupBox24.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox24.Location = new System.Drawing.Point(24, 149);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(500, 226);
            this.groupBox24.TabIndex = 3;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Product Information";
            // 
            // listbxSelectAProductToDisplay
            // 
            this.listbxSelectAProductToDisplay.FormattingEnabled = true;
            this.listbxSelectAProductToDisplay.ItemHeight = 15;
            this.listbxSelectAProductToDisplay.Location = new System.Drawing.Point(14, 30);
            this.listbxSelectAProductToDisplay.Name = "listbxSelectAProductToDisplay";
            this.listbxSelectAProductToDisplay.Size = new System.Drawing.Size(467, 184);
            this.listbxSelectAProductToDisplay.TabIndex = 0;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.Location = new System.Drawing.Point(101, 60);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(250, 26);
            this.label145.TabIndex = 2;
            this.label145.Text = "Select a Product to Display";
            // 
            // pnlModifyStaffInfo
            // 
            this.pnlModifyStaffInfo.Controls.Add(this.tabControl3);
            this.pnlModifyStaffInfo.Controls.Add(this.lblFill6);
            this.pnlModifyStaffInfo.Controls.Add(this.lblModifyStaffInfo);
            this.pnlModifyStaffInfo.Controls.Add(this.lblModifyStaffInformation);
            this.pnlModifyStaffInfo.Controls.Add(this.picBoxLogoModifyStaffInfoPnl);
            this.pnlModifyStaffInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlModifyStaffInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlModifyStaffInfo.Name = "pnlModifyStaffInfo";
            this.pnlModifyStaffInfo.Size = new System.Drawing.Size(554, 414);
            this.pnlModifyStaffInfo.TabIndex = 32;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage5);
            this.tabControl3.Controls.Add(this.tabPage6);
            this.tabControl3.Controls.Add(this.tabPage7);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl3.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl3.Location = new System.Drawing.Point(0, 142);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(554, 272);
            this.tabControl3.TabIndex = 49;
            // 
            // tabPage5
            // 
            this.tabPage5.AutoScroll = true;
            this.tabPage5.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage5.Controls.Add(this.label19);
            this.tabPage5.Controls.Add(this.label20);
            this.tabPage5.Controls.Add(this.label21);
            this.tabPage5.Controls.Add(this.groupBox11);
            this.tabPage5.Location = new System.Drawing.Point(4, 24);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(546, 244);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Personal Information";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 224);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(356, 15);
            this.label19.TabIndex = 45;
            this.label19.Text = "Please proceed to modify fields in the \'Contact Information\' section";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 208);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(0, 15);
            this.label20.TabIndex = 44;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(14, 4);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(146, 15);
            this.label21.TabIndex = 43;
            this.label21.Text = "* Denotes mandatory fields";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.lblModifyStaffGender);
            this.groupBox11.Controls.Add(this.lblModifyStaffDOB);
            this.groupBox11.Controls.Add(this.lblModifyStaffNRIC);
            this.groupBox11.Controls.Add(this.lblModifyStaffName);
            this.groupBox11.Controls.Add(this.label22);
            this.groupBox11.Controls.Add(this.label53);
            this.groupBox11.Controls.Add(this.label54);
            this.groupBox11.Controls.Add(this.label55);
            this.groupBox11.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(12, 18);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(521, 202);
            this.groupBox11.TabIndex = 42;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Particulars of Registrant";
            // 
            // lblModifyStaffGender
            // 
            this.lblModifyStaffGender.AutoSize = true;
            this.lblModifyStaffGender.Location = new System.Drawing.Point(27, 169);
            this.lblModifyStaffGender.Name = "lblModifyStaffGender";
            this.lblModifyStaffGender.Size = new System.Drawing.Size(147, 15);
            this.lblModifyStaffGender.TabIndex = 20;
            this.lblModifyStaffGender.Text = "- Display of Staff Gender -";
            // 
            // lblModifyStaffDOB
            // 
            this.lblModifyStaffDOB.AutoSize = true;
            this.lblModifyStaffDOB.Location = new System.Drawing.Point(27, 129);
            this.lblModifyStaffDOB.Name = "lblModifyStaffDOB";
            this.lblModifyStaffDOB.Size = new System.Drawing.Size(134, 15);
            this.lblModifyStaffDOB.TabIndex = 19;
            this.lblModifyStaffDOB.Text = "- Display of Staff DOB -";
            // 
            // lblModifyStaffNRIC
            // 
            this.lblModifyStaffNRIC.AutoSize = true;
            this.lblModifyStaffNRIC.Location = new System.Drawing.Point(27, 80);
            this.lblModifyStaffNRIC.Name = "lblModifyStaffNRIC";
            this.lblModifyStaffNRIC.Size = new System.Drawing.Size(139, 15);
            this.lblModifyStaffNRIC.TabIndex = 18;
            this.lblModifyStaffNRIC.Text = "- Display of Staff NRIC -";
            // 
            // lblModifyStaffName
            // 
            this.lblModifyStaffName.AutoSize = true;
            this.lblModifyStaffName.Location = new System.Drawing.Point(27, 35);
            this.lblModifyStaffName.Name = "lblModifyStaffName";
            this.lblModifyStaffName.Size = new System.Drawing.Size(139, 15);
            this.lblModifyStaffName.TabIndex = 17;
            this.lblModifyStaffName.Text = "- Display of Staff Name -\r\n";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(24, 150);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 15);
            this.label22.TabIndex = 12;
            this.label22.Text = "* Gender:";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(24, 105);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(90, 15);
            this.label53.TabIndex = 10;
            this.label53.Text = "* Date Of Birth:";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(24, 61);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(48, 15);
            this.label54.TabIndex = 6;
            this.label54.Text = "* NRIC:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(24, 15);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(96, 15);
            this.label55.TabIndex = 2;
            this.label55.Text = "* Name of Staff:";
            // 
            // tabPage6
            // 
            this.tabPage6.AutoScroll = true;
            this.tabPage6.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage6.Controls.Add(this.label56);
            this.tabPage6.Controls.Add(this.groupBox12);
            this.tabPage6.Controls.Add(this.label62);
            this.tabPage6.Controls.Add(this.label63);
            this.tabPage6.Location = new System.Drawing.Point(4, 24);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(546, 244);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "Contact Information";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(12, 224);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(359, 15);
            this.label56.TabIndex = 47;
            this.label56.Text = "Please proceed to modify fields in the \'Account Information\' section";
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox12.Controls.Add(this.label57);
            this.groupBox12.Controls.Add(this.label58);
            this.groupBox12.Controls.Add(this.label59);
            this.groupBox12.Controls.Add(this.tbxModifyHpNoContact);
            this.groupBox12.Controls.Add(this.tbxModifyHomeNo);
            this.groupBox12.Controls.Add(this.tbxModifyAddress);
            this.groupBox12.Controls.Add(this.label61);
            this.groupBox12.Location = new System.Drawing.Point(15, 34);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(516, 176);
            this.groupBox12.TabIndex = 46;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Contact Information of Registrant";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(236, 72);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(71, 15);
            this.label57.TabIndex = 50;
            this.label57.Text = "(Handphone)";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(236, 46);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(43, 15);
            this.label58.TabIndex = 49;
            this.label58.Text = "(Home)";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(17, 24);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(106, 15);
            this.label59.TabIndex = 48;
            this.label59.Text = "* Contact Numbers:";
            // 
            // tbxModifyHpNoContact
            // 
            this.tbxModifyHpNoContact.Location = new System.Drawing.Point(21, 69);
            this.tbxModifyHpNoContact.Name = "tbxModifyHpNoContact";
            this.tbxModifyHpNoContact.Size = new System.Drawing.Size(209, 23);
            this.tbxModifyHpNoContact.TabIndex = 47;
            // 
            // tbxModifyHomeNo
            // 
            this.tbxModifyHomeNo.Location = new System.Drawing.Point(21, 43);
            this.tbxModifyHomeNo.Name = "tbxModifyHomeNo";
            this.tbxModifyHomeNo.Size = new System.Drawing.Size(209, 23);
            this.tbxModifyHomeNo.TabIndex = 46;
            // 
            // tbxModifyAddress
            // 
            this.tbxModifyAddress.Location = new System.Drawing.Point(20, 112);
            this.tbxModifyAddress.Multiline = true;
            this.tbxModifyAddress.Name = "tbxModifyAddress";
            this.tbxModifyAddress.Size = new System.Drawing.Size(209, 48);
            this.tbxModifyAddress.TabIndex = 42;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(18, 94);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(60, 15);
            this.label61.TabIndex = 37;
            this.label61.Text = "* Address:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 225);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(0, 15);
            this.label62.TabIndex = 45;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(14, 4);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(146, 15);
            this.label63.TabIndex = 44;
            this.label63.Text = "* Denotes mandatory fields";
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage7.Controls.Add(this.btnModifyStaffCancel);
            this.tabPage7.Controls.Add(this.label64);
            this.tabPage7.Controls.Add(this.btnModifyStaffInformation);
            this.tabPage7.Controls.Add(this.groupBox13);
            this.tabPage7.Location = new System.Drawing.Point(4, 24);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(546, 244);
            this.tabPage7.TabIndex = 1;
            this.tabPage7.Text = "Account Information";
            // 
            // btnModifyStaffCancel
            // 
            this.btnModifyStaffCancel.Location = new System.Drawing.Point(455, 133);
            this.btnModifyStaffCancel.Name = "btnModifyStaffCancel";
            this.btnModifyStaffCancel.Size = new System.Drawing.Size(75, 23);
            this.btnModifyStaffCancel.TabIndex = 43;
            this.btnModifyStaffCancel.Text = "Cancel";
            this.btnModifyStaffCancel.UseVisualStyleBackColor = true;
            this.btnModifyStaffCancel.Click += new System.EventHandler(this.btnModifyStaffCancel_Click);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(14, 4);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(146, 15);
            this.label64.TabIndex = 44;
            this.label64.Text = "* Denotes mandatory fields";
            // 
            // btnModifyStaffInformation
            // 
            this.btnModifyStaffInformation.Location = new System.Drawing.Point(358, 133);
            this.btnModifyStaffInformation.Name = "btnModifyStaffInformation";
            this.btnModifyStaffInformation.Size = new System.Drawing.Size(75, 23);
            this.btnModifyStaffInformation.TabIndex = 42;
            this.btnModifyStaffInformation.Text = "Modify";
            this.btnModifyStaffInformation.UseVisualStyleBackColor = true;
            this.btnModifyStaffInformation.Click += new System.EventHandler(this.btnModifyStaffInformation_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.chkBoxModifyInventoryManager);
            this.groupBox13.Controls.Add(this.chkBoxModifyOrderManager);
            this.groupBox13.Controls.Add(this.chkBoxModifyCatalogueManager);
            this.groupBox13.Controls.Add(this.chkBoxModifyAdministrator);
            this.groupBox13.Controls.Add(this.label68);
            this.groupBox13.Location = new System.Drawing.Point(18, 34);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(508, 92);
            this.groupBox13.TabIndex = 42;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Account Details";
            // 
            // chkBoxModifyInventoryManager
            // 
            this.chkBoxModifyInventoryManager.AutoSize = true;
            this.chkBoxModifyInventoryManager.Location = new System.Drawing.Point(140, 60);
            this.chkBoxModifyInventoryManager.Name = "chkBoxModifyInventoryManager";
            this.chkBoxModifyInventoryManager.Size = new System.Drawing.Size(125, 19);
            this.chkBoxModifyInventoryManager.TabIndex = 39;
            this.chkBoxModifyInventoryManager.Text = "Inventory Manager";
            this.chkBoxModifyInventoryManager.UseVisualStyleBackColor = true;
            // 
            // chkBoxModifyOrderManager
            // 
            this.chkBoxModifyOrderManager.AutoSize = true;
            this.chkBoxModifyOrderManager.Location = new System.Drawing.Point(16, 60);
            this.chkBoxModifyOrderManager.Name = "chkBoxModifyOrderManager";
            this.chkBoxModifyOrderManager.Size = new System.Drawing.Size(105, 19);
            this.chkBoxModifyOrderManager.TabIndex = 38;
            this.chkBoxModifyOrderManager.Text = "Order Manager";
            this.chkBoxModifyOrderManager.UseVisualStyleBackColor = true;
            // 
            // chkBoxModifyCatalogueManager
            // 
            this.chkBoxModifyCatalogueManager.AutoSize = true;
            this.chkBoxModifyCatalogueManager.Location = new System.Drawing.Point(140, 38);
            this.chkBoxModifyCatalogueManager.Name = "chkBoxModifyCatalogueManager";
            this.chkBoxModifyCatalogueManager.Size = new System.Drawing.Size(124, 19);
            this.chkBoxModifyCatalogueManager.TabIndex = 37;
            this.chkBoxModifyCatalogueManager.Text = "Catalogue Manager";
            this.chkBoxModifyCatalogueManager.UseVisualStyleBackColor = true;
            // 
            // chkBoxModifyAdministrator
            // 
            this.chkBoxModifyAdministrator.AutoSize = true;
            this.chkBoxModifyAdministrator.Location = new System.Drawing.Point(16, 40);
            this.chkBoxModifyAdministrator.Name = "chkBoxModifyAdministrator";
            this.chkBoxModifyAdministrator.Size = new System.Drawing.Size(97, 19);
            this.chkBoxModifyAdministrator.TabIndex = 36;
            this.chkBoxModifyAdministrator.Text = "Administrator";
            this.chkBoxModifyAdministrator.UseVisualStyleBackColor = true;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(13, 22);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(112, 15);
            this.label68.TabIndex = 23;
            this.label68.Text = "* Role of Registrant:";
            // 
            // lblFill6
            // 
            this.lblFill6.AutoSize = true;
            this.lblFill6.Location = new System.Drawing.Point(9, 248);
            this.lblFill6.Name = "lblFill6";
            this.lblFill6.Size = new System.Drawing.Size(0, 13);
            this.lblFill6.TabIndex = 48;
            // 
            // lblModifyStaffInfo
            // 
            this.lblModifyStaffInfo.AutoSize = true;
            this.lblModifyStaffInfo.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifyStaffInfo.Location = new System.Drawing.Point(4, 122);
            this.lblModifyStaffInfo.Name = "lblModifyStaffInfo";
            this.lblModifyStaffInfo.Size = new System.Drawing.Size(327, 17);
            this.lblModifyStaffInfo.TabIndex = 47;
            this.lblModifyStaffInfo.Text = "Please enter the respective staff information to modify...";
            // 
            // lblModifyStaffInformation
            // 
            this.lblModifyStaffInformation.AutoSize = true;
            this.lblModifyStaffInformation.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifyStaffInformation.Location = new System.Drawing.Point(127, 60);
            this.lblModifyStaffInformation.Name = "lblModifyStaffInformation";
            this.lblModifyStaffInformation.Size = new System.Drawing.Size(243, 26);
            this.lblModifyStaffInformation.TabIndex = 42;
            this.lblModifyStaffInformation.Text = "Modify Staff Information";
            // 
            // picBoxLogoModifyStaffInfoPnl
            // 
            this.picBoxLogoModifyStaffInfoPnl.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoModifyStaffInfoPnl.Image")));
            this.picBoxLogoModifyStaffInfoPnl.Location = new System.Drawing.Point(424, 33);
            this.picBoxLogoModifyStaffInfoPnl.Name = "picBoxLogoModifyStaffInfoPnl";
            this.picBoxLogoModifyStaffInfoPnl.Size = new System.Drawing.Size(100, 81);
            this.picBoxLogoModifyStaffInfoPnl.TabIndex = 40;
            this.picBoxLogoModifyStaffInfoPnl.TabStop = false;
            // 
            // pnlDeleteStaffInfo
            // 
            this.pnlDeleteStaffInfo.Controls.Add(this.lblCfmDeleteStaff);
            this.pnlDeleteStaffInfo.Controls.Add(this.btnCancelDelete);
            this.pnlDeleteStaffInfo.Controls.Add(this.btnDeleteStaff);
            this.pnlDeleteStaffInfo.Controls.Add(this.gpBoxDisplayStaffInfo);
            this.pnlDeleteStaffInfo.Controls.Add(this.lblDeleteExistingStaffInfo);
            this.pnlDeleteStaffInfo.Controls.Add(this.picBxLogoDeleteStaffPnl);
            this.pnlDeleteStaffInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDeleteStaffInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlDeleteStaffInfo.Name = "pnlDeleteStaffInfo";
            this.pnlDeleteStaffInfo.Size = new System.Drawing.Size(554, 414);
            this.pnlDeleteStaffInfo.TabIndex = 33;
            // 
            // lblCfmDeleteStaff
            // 
            this.lblCfmDeleteStaff.AutoSize = true;
            this.lblCfmDeleteStaff.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCfmDeleteStaff.Location = new System.Drawing.Point(19, 127);
            this.lblCfmDeleteStaff.Name = "lblCfmDeleteStaff";
            this.lblCfmDeleteStaff.Size = new System.Drawing.Size(253, 17);
            this.lblCfmDeleteStaff.TabIndex = 49;
            this.lblCfmDeleteStaff.Text = "Please confirm staff information to delete...";
            // 
            // btnCancelDelete
            // 
            this.btnCancelDelete.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelDelete.Location = new System.Drawing.Point(449, 372);
            this.btnCancelDelete.Name = "btnCancelDelete";
            this.btnCancelDelete.Size = new System.Drawing.Size(75, 23);
            this.btnCancelDelete.TabIndex = 48;
            this.btnCancelDelete.Text = "Cancel";
            this.btnCancelDelete.UseVisualStyleBackColor = true;
            this.btnCancelDelete.Click += new System.EventHandler(this.btnCancelDelete_Click);
            // 
            // btnDeleteStaff
            // 
            this.btnDeleteStaff.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteStaff.Location = new System.Drawing.Point(362, 372);
            this.btnDeleteStaff.Name = "btnDeleteStaff";
            this.btnDeleteStaff.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteStaff.TabIndex = 47;
            this.btnDeleteStaff.Text = "Delete";
            this.btnDeleteStaff.UseVisualStyleBackColor = true;
            this.btnDeleteStaff.Click += new System.EventHandler(this.btnDeleteStaff_Click);
            // 
            // gpBoxDisplayStaffInfo
            // 
            this.gpBoxDisplayStaffInfo.Controls.Add(this.lblDispDelStaffAddress);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.lblDispDelStaffRole);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.lblDispDelStaffMobile);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.lblDispDelStaffCintact);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.lblDispDelStaffGender);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.lblDispDelStaffDOB);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.lblDispDelStaffNRIC);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.lblDispDelStaffName);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.label131);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.label60);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.label67);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.label127);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.label128);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.label133);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.label134);
            this.gpBoxDisplayStaffInfo.Controls.Add(this.label139);
            this.gpBoxDisplayStaffInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpBoxDisplayStaffInfo.Location = new System.Drawing.Point(22, 146);
            this.gpBoxDisplayStaffInfo.Name = "gpBoxDisplayStaffInfo";
            this.gpBoxDisplayStaffInfo.Size = new System.Drawing.Size(502, 219);
            this.gpBoxDisplayStaffInfo.TabIndex = 45;
            this.gpBoxDisplayStaffInfo.TabStop = false;
            this.gpBoxDisplayStaffInfo.Text = "Staff Information";
            // 
            // lblDispDelStaffAddress
            // 
            this.lblDispDelStaffAddress.AutoSize = true;
            this.lblDispDelStaffAddress.Location = new System.Drawing.Point(228, 156);
            this.lblDispDelStaffAddress.Name = "lblDispDelStaffAddress";
            this.lblDispDelStaffAddress.Size = new System.Drawing.Size(137, 15);
            this.lblDispDelStaffAddress.TabIndex = 15;
            this.lblDispDelStaffAddress.Text = "- Display Staff address - ";
            // 
            // lblDispDelStaffRole
            // 
            this.lblDispDelStaffRole.AutoSize = true;
            this.lblDispDelStaffRole.BackColor = System.Drawing.SystemColors.Control;
            this.lblDispDelStaffRole.Location = new System.Drawing.Point(228, 139);
            this.lblDispDelStaffRole.Name = "lblDispDelStaffRole";
            this.lblDispDelStaffRole.Size = new System.Drawing.Size(117, 15);
            this.lblDispDelStaffRole.TabIndex = 14;
            this.lblDispDelStaffRole.Text = "- Display Staff Role -";
            // 
            // lblDispDelStaffMobile
            // 
            this.lblDispDelStaffMobile.AutoSize = true;
            this.lblDispDelStaffMobile.Location = new System.Drawing.Point(228, 122);
            this.lblDispDelStaffMobile.Name = "lblDispDelStaffMobile";
            this.lblDispDelStaffMobile.Size = new System.Drawing.Size(133, 15);
            this.lblDispDelStaffMobile.TabIndex = 13;
            this.lblDispDelStaffMobile.Text = "- Display Staff Mobile - ";
            // 
            // lblDispDelStaffCintact
            // 
            this.lblDispDelStaffCintact.AutoSize = true;
            this.lblDispDelStaffCintact.Location = new System.Drawing.Point(228, 105);
            this.lblDispDelStaffCintact.Name = "lblDispDelStaffCintact";
            this.lblDispDelStaffCintact.Size = new System.Drawing.Size(134, 15);
            this.lblDispDelStaffCintact.TabIndex = 12;
            this.lblDispDelStaffCintact.Text = "- Display Staff Contact- ";
            // 
            // lblDispDelStaffGender
            // 
            this.lblDispDelStaffGender.AutoSize = true;
            this.lblDispDelStaffGender.Location = new System.Drawing.Point(228, 89);
            this.lblDispDelStaffGender.Name = "lblDispDelStaffGender";
            this.lblDispDelStaffGender.Size = new System.Drawing.Size(135, 15);
            this.lblDispDelStaffGender.TabIndex = 11;
            this.lblDispDelStaffGender.Text = "- Display Staff Gender - ";
            // 
            // lblDispDelStaffDOB
            // 
            this.lblDispDelStaffDOB.AutoSize = true;
            this.lblDispDelStaffDOB.Location = new System.Drawing.Point(228, 72);
            this.lblDispDelStaffDOB.Name = "lblDispDelStaffDOB";
            this.lblDispDelStaffDOB.Size = new System.Drawing.Size(122, 15);
            this.lblDispDelStaffDOB.TabIndex = 10;
            this.lblDispDelStaffDOB.Text = "- Display Staff DOB - ";
            // 
            // lblDispDelStaffNRIC
            // 
            this.lblDispDelStaffNRIC.AutoSize = true;
            this.lblDispDelStaffNRIC.Location = new System.Drawing.Point(228, 56);
            this.lblDispDelStaffNRIC.Name = "lblDispDelStaffNRIC";
            this.lblDispDelStaffNRIC.Size = new System.Drawing.Size(127, 15);
            this.lblDispDelStaffNRIC.TabIndex = 9;
            this.lblDispDelStaffNRIC.Text = "- Display Staff NRIC - ";
            // 
            // lblDispDelStaffName
            // 
            this.lblDispDelStaffName.AutoSize = true;
            this.lblDispDelStaffName.Location = new System.Drawing.Point(228, 40);
            this.lblDispDelStaffName.Name = "lblDispDelStaffName";
            this.lblDispDelStaffName.Size = new System.Drawing.Size(124, 15);
            this.lblDispDelStaffName.TabIndex = 8;
            this.lblDispDelStaffName.Text = "- Display Staff Name -";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.Location = new System.Drawing.Point(145, 138);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(75, 16);
            this.label131.TabIndex = 7;
            this.label131.Text = "Staff Role:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(121, 152);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(99, 16);
            this.label60.TabIndex = 6;
            this.label60.Text = "Staff Address:";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(106, 121);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(114, 16);
            this.label67.TabIndex = 5;
            this.label67.Text = "Staff Mobile No.:";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.Location = new System.Drawing.Point(99, 105);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(121, 16);
            this.label127.TabIndex = 4;
            this.label127.Text = "Staff Contact No.:";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.Location = new System.Drawing.Point(130, 88);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(92, 16);
            this.label128.TabIndex = 3;
            this.label128.Text = "Staff Gender:";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.Location = new System.Drawing.Point(144, 71);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(78, 16);
            this.label133.TabIndex = 2;
            this.label133.Text = "Staff DOB:";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.Location = new System.Drawing.Point(140, 55);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(83, 16);
            this.label134.TabIndex = 1;
            this.label134.Text = "Staff NRIC:";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.Location = new System.Drawing.Point(138, 38);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(84, 16);
            this.label139.TabIndex = 0;
            this.label139.Text = "Staff Name:";
            // 
            // lblDeleteExistingStaffInfo
            // 
            this.lblDeleteExistingStaffInfo.AutoSize = true;
            this.lblDeleteExistingStaffInfo.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteExistingStaffInfo.Location = new System.Drawing.Point(66, 60);
            this.lblDeleteExistingStaffInfo.Name = "lblDeleteExistingStaffInfo";
            this.lblDeleteExistingStaffInfo.Size = new System.Drawing.Size(312, 26);
            this.lblDeleteExistingStaffInfo.TabIndex = 43;
            this.lblDeleteExistingStaffInfo.Text = "Delete Existing Staff Information";
            // 
            // picBxLogoDeleteStaffPnl
            // 
            this.picBxLogoDeleteStaffPnl.Image = ((System.Drawing.Image)(resources.GetObject("picBxLogoDeleteStaffPnl.Image")));
            this.picBxLogoDeleteStaffPnl.Location = new System.Drawing.Point(424, 33);
            this.picBxLogoDeleteStaffPnl.Name = "picBxLogoDeleteStaffPnl";
            this.picBxLogoDeleteStaffPnl.Size = new System.Drawing.Size(100, 81);
            this.picBxLogoDeleteStaffPnl.TabIndex = 42;
            this.picBxLogoDeleteStaffPnl.TabStop = false;
            // 
            // pnlDisplayStaffInfo
            // 
            this.pnlDisplayStaffInfo.Controls.Add(this.btnDispStaffInfoOK);
            this.pnlDisplayStaffInfo.Controls.Add(this.grpBoxDispStaff);
            this.pnlDisplayStaffInfo.Controls.Add(this.picBoxLogoPnlDispStaffInfo);
            this.pnlDisplayStaffInfo.Controls.Add(this.lblDispExistingStaff);
            this.pnlDisplayStaffInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDisplayStaffInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlDisplayStaffInfo.Name = "pnlDisplayStaffInfo";
            this.pnlDisplayStaffInfo.Size = new System.Drawing.Size(554, 414);
            this.pnlDisplayStaffInfo.TabIndex = 34;
            // 
            // btnDispStaffInfoOK
            // 
            this.btnDispStaffInfoOK.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDispStaffInfoOK.Location = new System.Drawing.Point(449, 372);
            this.btnDispStaffInfoOK.Name = "btnDispStaffInfoOK";
            this.btnDispStaffInfoOK.Size = new System.Drawing.Size(75, 23);
            this.btnDispStaffInfoOK.TabIndex = 46;
            this.btnDispStaffInfoOK.Text = "OK";
            this.btnDispStaffInfoOK.UseVisualStyleBackColor = true;
            this.btnDispStaffInfoOK.Click += new System.EventHandler(this.btnDispStaffInfoOK_Click);
            // 
            // grpBoxDispStaff
            // 
            this.grpBoxDispStaff.Controls.Add(this.label148);
            this.grpBoxDispStaff.Controls.Add(this.label149);
            this.grpBoxDispStaff.Controls.Add(this.label140);
            this.grpBoxDispStaff.Controls.Add(this.label141);
            this.grpBoxDispStaff.Controls.Add(this.label142);
            this.grpBoxDispStaff.Controls.Add(this.label143);
            this.grpBoxDispStaff.Controls.Add(this.label144);
            this.grpBoxDispStaff.Controls.Add(this.label146);
            this.grpBoxDispStaff.Controls.Add(this.label147);
            this.grpBoxDispStaff.Controls.Add(this.label151);
            this.grpBoxDispStaff.Controls.Add(this.label152);
            this.grpBoxDispStaff.Controls.Add(this.label153);
            this.grpBoxDispStaff.Controls.Add(this.label154);
            this.grpBoxDispStaff.Controls.Add(this.label155);
            this.grpBoxDispStaff.Controls.Add(this.label156);
            this.grpBoxDispStaff.Controls.Add(this.label157);
            this.grpBoxDispStaff.Controls.Add(this.label158);
            this.grpBoxDispStaff.Controls.Add(this.label159);
            this.grpBoxDispStaff.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxDispStaff.Location = new System.Drawing.Point(22, 146);
            this.grpBoxDispStaff.Name = "grpBoxDispStaff";
            this.grpBoxDispStaff.Size = new System.Drawing.Size(502, 219);
            this.grpBoxDispStaff.TabIndex = 45;
            this.grpBoxDispStaff.TabStop = false;
            this.grpBoxDispStaff.Text = "View Staff Information";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(244, 177);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(137, 15);
            this.label148.TabIndex = 33;
            this.label148.Text = "- Display Staff address - ";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.Location = new System.Drawing.Point(137, 173);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(99, 16);
            this.label149.TabIndex = 32;
            this.label149.Text = "Staff Address:";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(247, 161);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(145, 15);
            this.label140.TabIndex = 31;
            this.label140.Text = "- Display Staff password - ";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(247, 144);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(120, 15);
            this.label141.TabIndex = 30;
            this.label141.Text = "- Display Staff Role - ";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(247, 127);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(133, 15);
            this.label142.TabIndex = 29;
            this.label142.Text = "- Display Staff Mobile - ";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(247, 110);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(134, 15);
            this.label143.TabIndex = 28;
            this.label143.Text = "- Display Staff Contact- ";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(247, 94);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(135, 15);
            this.label144.TabIndex = 27;
            this.label144.Text = "- Display Staff Gender - ";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(247, 77);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(122, 15);
            this.label146.TabIndex = 26;
            this.label146.Text = "- Display Staff DOB - ";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(247, 61);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(127, 15);
            this.label147.TabIndex = 25;
            this.label147.Text = "- Display Staff NRIC - ";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(247, 45);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(124, 15);
            this.label151.TabIndex = 24;
            this.label151.Text = "- Display Staff Name -";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.Location = new System.Drawing.Point(164, 143);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(75, 16);
            this.label152.TabIndex = 23;
            this.label152.Text = "Staff Role:";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.Location = new System.Drawing.Point(140, 157);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(103, 16);
            this.label153.TabIndex = 22;
            this.label153.Text = "Staff Password:";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.Location = new System.Drawing.Point(125, 126);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(114, 16);
            this.label154.TabIndex = 21;
            this.label154.Text = "Staff Mobile No.:";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.Location = new System.Drawing.Point(118, 110);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(121, 16);
            this.label155.TabIndex = 20;
            this.label155.Text = "Staff Contact No.:";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.Location = new System.Drawing.Point(149, 93);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(92, 16);
            this.label156.TabIndex = 19;
            this.label156.Text = "Staff Gender:";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label157.Location = new System.Drawing.Point(163, 76);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(78, 16);
            this.label157.TabIndex = 18;
            this.label157.Text = "Staff DOB:";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label158.Location = new System.Drawing.Point(159, 60);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(83, 16);
            this.label158.TabIndex = 17;
            this.label158.Text = "Staff NRIC:";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.Location = new System.Drawing.Point(157, 43);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(84, 16);
            this.label159.TabIndex = 16;
            this.label159.Text = "Staff Name:";
            // 
            // picBoxLogoPnlDispStaffInfo
            // 
            this.picBoxLogoPnlDispStaffInfo.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoPnlDispStaffInfo.Image")));
            this.picBoxLogoPnlDispStaffInfo.Location = new System.Drawing.Point(424, 33);
            this.picBoxLogoPnlDispStaffInfo.Name = "picBoxLogoPnlDispStaffInfo";
            this.picBoxLogoPnlDispStaffInfo.Size = new System.Drawing.Size(100, 81);
            this.picBoxLogoPnlDispStaffInfo.TabIndex = 42;
            this.picBoxLogoPnlDispStaffInfo.TabStop = false;
            // 
            // lblDispExistingStaff
            // 
            this.lblDispExistingStaff.AutoSize = true;
            this.lblDispExistingStaff.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispExistingStaff.Location = new System.Drawing.Point(102, 60);
            this.lblDispExistingStaff.Name = "lblDispExistingStaff";
            this.lblDispExistingStaff.Size = new System.Drawing.Size(243, 26);
            this.lblDispExistingStaff.TabIndex = 43;
            this.lblDispExistingStaff.Text = "Display Staff Information";
            // 
            // pnlDeleteOrderInfo
            // 
            this.pnlDeleteOrderInfo.Controls.Add(this.lblDeleteOrderInfo);
            this.pnlDeleteOrderInfo.Controls.Add(this.btnCancelDeleteOrderInfo);
            this.pnlDeleteOrderInfo.Controls.Add(this.btnDeleteOrderInfo);
            this.pnlDeleteOrderInfo.Controls.Add(this.grpDeleteDispOrderInfo);
            this.pnlDeleteOrderInfo.Controls.Add(this.lblDeleteOrderInformation);
            this.pnlDeleteOrderInfo.Controls.Add(this.picBoxDeleteOrderInformation);
            this.pnlDeleteOrderInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDeleteOrderInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlDeleteOrderInfo.Name = "pnlDeleteOrderInfo";
            this.pnlDeleteOrderInfo.Size = new System.Drawing.Size(554, 414);
            this.pnlDeleteOrderInfo.TabIndex = 35;
            // 
            // lblDeleteOrderInfo
            // 
            this.lblDeleteOrderInfo.AutoSize = true;
            this.lblDeleteOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteOrderInfo.Location = new System.Drawing.Point(24, 145);
            this.lblDeleteOrderInfo.Name = "lblDeleteOrderInfo";
            this.lblDeleteOrderInfo.Size = new System.Drawing.Size(253, 17);
            this.lblDeleteOrderInfo.TabIndex = 55;
            this.lblDeleteOrderInfo.Text = "Please confirm order information to delete...";
            // 
            // btnCancelDeleteOrderInfo
            // 
            this.btnCancelDeleteOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelDeleteOrderInfo.Location = new System.Drawing.Point(448, 352);
            this.btnCancelDeleteOrderInfo.Name = "btnCancelDeleteOrderInfo";
            this.btnCancelDeleteOrderInfo.Size = new System.Drawing.Size(75, 23);
            this.btnCancelDeleteOrderInfo.TabIndex = 54;
            this.btnCancelDeleteOrderInfo.Text = "Cancel";
            this.btnCancelDeleteOrderInfo.UseVisualStyleBackColor = true;
            this.btnCancelDeleteOrderInfo.Click += new System.EventHandler(this.btnCancelDeleteOrderInfo_Click);
            // 
            // btnDeleteOrderInfo
            // 
            this.btnDeleteOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteOrderInfo.Location = new System.Drawing.Point(347, 352);
            this.btnDeleteOrderInfo.Name = "btnDeleteOrderInfo";
            this.btnDeleteOrderInfo.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteOrderInfo.TabIndex = 53;
            this.btnDeleteOrderInfo.Text = "Delete";
            this.btnDeleteOrderInfo.UseVisualStyleBackColor = true;
            this.btnDeleteOrderInfo.Click += new System.EventHandler(this.btnDeleteOrderInfo_Click);
            // 
            // grpDeleteDispOrderInfo
            // 
            this.grpDeleteDispOrderInfo.Controls.Add(this.lblDispDelTotalPrice);
            this.grpDeleteDispOrderInfo.Controls.Add(this.lblDispDelOrderDate);
            this.grpDeleteDispOrderInfo.Controls.Add(this.lblDispDelOrderDestAdd);
            this.grpDeleteDispOrderInfo.Controls.Add(this.label160);
            this.grpDeleteDispOrderInfo.Controls.Add(this.label161);
            this.grpDeleteDispOrderInfo.Controls.Add(this.lblDispDelCustName);
            this.grpDeleteDispOrderInfo.Controls.Add(this.lblDispDelORN);
            this.grpDeleteDispOrderInfo.Controls.Add(this.label162);
            this.grpDeleteDispOrderInfo.Controls.Add(this.label163);
            this.grpDeleteDispOrderInfo.Controls.Add(this.label164);
            this.grpDeleteDispOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDeleteDispOrderInfo.Location = new System.Drawing.Point(23, 166);
            this.grpDeleteDispOrderInfo.Name = "grpDeleteDispOrderInfo";
            this.grpDeleteDispOrderInfo.Size = new System.Drawing.Size(500, 180);
            this.grpDeleteDispOrderInfo.TabIndex = 52;
            this.grpDeleteDispOrderInfo.TabStop = false;
            this.grpDeleteDispOrderInfo.Text = "Staff Information";
            // 
            // lblDispDelTotalPrice
            // 
            this.lblDispDelTotalPrice.AutoSize = true;
            this.lblDispDelTotalPrice.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispDelTotalPrice.Location = new System.Drawing.Point(258, 103);
            this.lblDispDelTotalPrice.Name = "lblDispDelTotalPrice";
            this.lblDispDelTotalPrice.Size = new System.Drawing.Size(77, 15);
            this.lblDispDelTotalPrice.TabIndex = 72;
            this.lblDispDelTotalPrice.Text = "- Total Price -";
            // 
            // lblDispDelOrderDate
            // 
            this.lblDispDelOrderDate.AutoSize = true;
            this.lblDispDelOrderDate.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispDelOrderDate.Location = new System.Drawing.Point(258, 84);
            this.lblDispDelOrderDate.Name = "lblDispDelOrderDate";
            this.lblDispDelOrderDate.Size = new System.Drawing.Size(155, 15);
            this.lblDispDelOrderDate.TabIndex = 71;
            this.lblDispDelOrderDate.Text = "- Order Date/Time of entry -";
            // 
            // lblDispDelOrderDestAdd
            // 
            this.lblDispDelOrderDestAdd.AutoSize = true;
            this.lblDispDelOrderDestAdd.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispDelOrderDestAdd.Location = new System.Drawing.Point(258, 120);
            this.lblDispDelOrderDestAdd.Name = "lblDispDelOrderDestAdd";
            this.lblDispDelOrderDestAdd.Size = new System.Drawing.Size(65, 15);
            this.lblDispDelOrderDestAdd.TabIndex = 70;
            this.lblDispDelOrderDestAdd.Text = "- Address - ";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.Location = new System.Drawing.Point(120, 119);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(136, 16);
            this.label160.TabIndex = 69;
            this.label160.Tag = "";
            this.label160.Text = "Destination Address :";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label161.Location = new System.Drawing.Point(175, 102);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(81, 16);
            this.label161.TabIndex = 68;
            this.label161.Tag = "";
            this.label161.Text = "Total Price :";
            // 
            // lblDispDelCustName
            // 
            this.lblDispDelCustName.AutoSize = true;
            this.lblDispDelCustName.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispDelCustName.Location = new System.Drawing.Point(258, 66);
            this.lblDispDelCustName.Name = "lblDispDelCustName";
            this.lblDispDelCustName.Size = new System.Drawing.Size(112, 15);
            this.lblDispDelCustName.TabIndex = 67;
            this.lblDispDelCustName.Text = "- Customer\'s Name  -";
            // 
            // lblDispDelORN
            // 
            this.lblDispDelORN.AutoSize = true;
            this.lblDispDelORN.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDispDelORN.Location = new System.Drawing.Point(258, 45);
            this.lblDispDelORN.Name = "lblDispDelORN";
            this.lblDispDelORN.Size = new System.Drawing.Size(120, 15);
            this.lblDispDelORN.TabIndex = 66;
            this.lblDispDelORN.Text = "- Order Ref Number - ";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label162.Location = new System.Drawing.Point(147, 83);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(109, 16);
            this.label162.TabIndex = 65;
            this.label162.Tag = "";
            this.label162.Text = "Date Of Order :";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label163.Location = new System.Drawing.Point(147, 65);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(109, 16);
            this.label163.TabIndex = 64;
            this.label163.Tag = "";
            this.label163.Text = "Customer Name :";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.Location = new System.Drawing.Point(87, 45);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(169, 16);
            this.label164.TabIndex = 63;
            this.label164.Tag = "";
            this.label164.Text = "Order Reference Number :";
            // 
            // lblDeleteOrderInformation
            // 
            this.lblDeleteOrderInformation.AutoSize = true;
            this.lblDeleteOrderInformation.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteOrderInformation.Location = new System.Drawing.Point(64, 60);
            this.lblDeleteOrderInformation.Name = "lblDeleteOrderInformation";
            this.lblDeleteOrderInformation.Size = new System.Drawing.Size(315, 26);
            this.lblDeleteOrderInformation.TabIndex = 51;
            this.lblDeleteOrderInformation.Text = "Delete Existing Order Information";
            // 
            // picBoxDeleteOrderInformation
            // 
            this.picBoxDeleteOrderInformation.Image = ((System.Drawing.Image)(resources.GetObject("picBoxDeleteOrderInformation.Image")));
            this.picBoxDeleteOrderInformation.Location = new System.Drawing.Point(424, 33);
            this.picBoxDeleteOrderInformation.Name = "picBoxDeleteOrderInformation";
            this.picBoxDeleteOrderInformation.Size = new System.Drawing.Size(100, 81);
            this.picBoxDeleteOrderInformation.TabIndex = 50;
            this.picBoxDeleteOrderInformation.TabStop = false;
            // 
            // pnlSearchDispOrderInfo
            // 
            this.pnlSearchDispOrderInfo.Controls.Add(this.btnOKSearchDispOrderInfo);
            this.pnlSearchDispOrderInfo.Controls.Add(this.grpSearchDispOrderInfo);
            this.pnlSearchDispOrderInfo.Controls.Add(this.lblSearchDispOrderInfo);
            this.pnlSearchDispOrderInfo.Controls.Add(this.picBxLogoPnlSearchDispOrderInfo);
            this.pnlSearchDispOrderInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchDispOrderInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchDispOrderInfo.Name = "pnlSearchDispOrderInfo";
            this.pnlSearchDispOrderInfo.Size = new System.Drawing.Size(554, 414);
            this.pnlSearchDispOrderInfo.TabIndex = 36;
            // 
            // btnOKSearchDispOrderInfo
            // 
            this.btnOKSearchDispOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOKSearchDispOrderInfo.Location = new System.Drawing.Point(448, 352);
            this.btnOKSearchDispOrderInfo.Name = "btnOKSearchDispOrderInfo";
            this.btnOKSearchDispOrderInfo.Size = new System.Drawing.Size(75, 23);
            this.btnOKSearchDispOrderInfo.TabIndex = 50;
            this.btnOKSearchDispOrderInfo.Text = "OK";
            this.btnOKSearchDispOrderInfo.UseVisualStyleBackColor = true;
            this.btnOKSearchDispOrderInfo.Click += new System.EventHandler(this.btnOKSearchDispOrderInfo_Click);
            // 
            // grpSearchDispOrderInfo
            // 
            this.grpSearchDispOrderInfo.Controls.Add(this.label165);
            this.grpSearchDispOrderInfo.Controls.Add(this.label166);
            this.grpSearchDispOrderInfo.Controls.Add(this.label167);
            this.grpSearchDispOrderInfo.Controls.Add(this.label168);
            this.grpSearchDispOrderInfo.Controls.Add(this.label169);
            this.grpSearchDispOrderInfo.Controls.Add(this.label170);
            this.grpSearchDispOrderInfo.Controls.Add(this.label171);
            this.grpSearchDispOrderInfo.Controls.Add(this.label172);
            this.grpSearchDispOrderInfo.Controls.Add(this.label173);
            this.grpSearchDispOrderInfo.Controls.Add(this.label174);
            this.grpSearchDispOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSearchDispOrderInfo.Location = new System.Drawing.Point(23, 166);
            this.grpSearchDispOrderInfo.Name = "grpSearchDispOrderInfo";
            this.grpSearchDispOrderInfo.Size = new System.Drawing.Size(500, 180);
            this.grpSearchDispOrderInfo.TabIndex = 49;
            this.grpSearchDispOrderInfo.TabStop = false;
            this.grpSearchDispOrderInfo.Text = "View Staff Information";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label165.Location = new System.Drawing.Point(258, 103);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(77, 15);
            this.label165.TabIndex = 82;
            this.label165.Text = "- Total Price -";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label166.Location = new System.Drawing.Point(258, 84);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(155, 15);
            this.label166.TabIndex = 81;
            this.label166.Text = "- Order Date/Time of entry -";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label167.Location = new System.Drawing.Point(258, 120);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(65, 15);
            this.label167.TabIndex = 80;
            this.label167.Text = "- Address - ";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label168.Location = new System.Drawing.Point(120, 119);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(136, 16);
            this.label168.TabIndex = 79;
            this.label168.Tag = "";
            this.label168.Text = "Destination Address :";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label169.Location = new System.Drawing.Point(175, 102);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(81, 16);
            this.label169.TabIndex = 78;
            this.label169.Tag = "";
            this.label169.Text = "Total Price :";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label170.Location = new System.Drawing.Point(258, 66);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(112, 15);
            this.label170.TabIndex = 77;
            this.label170.Text = "- Customer\'s Name  -";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.Location = new System.Drawing.Point(258, 45);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(120, 15);
            this.label171.TabIndex = 76;
            this.label171.Text = "- Order Ref Number - ";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.Location = new System.Drawing.Point(147, 83);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(109, 16);
            this.label172.TabIndex = 75;
            this.label172.Tag = "";
            this.label172.Text = "Date Of Order :";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.Location = new System.Drawing.Point(147, 65);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(109, 16);
            this.label173.TabIndex = 74;
            this.label173.Tag = "";
            this.label173.Text = "Customer Name :";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label174.Location = new System.Drawing.Point(87, 45);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(169, 16);
            this.label174.TabIndex = 73;
            this.label174.Tag = "";
            this.label174.Text = "Order Reference Number :";
            // 
            // lblSearchDispOrderInfo
            // 
            this.lblSearchDispOrderInfo.AutoSize = true;
            this.lblSearchDispOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchDispOrderInfo.Location = new System.Drawing.Point(103, 60);
            this.lblSearchDispOrderInfo.Name = "lblSearchDispOrderInfo";
            this.lblSearchDispOrderInfo.Size = new System.Drawing.Size(246, 26);
            this.lblSearchDispOrderInfo.TabIndex = 48;
            this.lblSearchDispOrderInfo.Text = "Display Order Information";
            // 
            // picBxLogoPnlSearchDispOrderInfo
            // 
            this.picBxLogoPnlSearchDispOrderInfo.Image = ((System.Drawing.Image)(resources.GetObject("picBxLogoPnlSearchDispOrderInfo.Image")));
            this.picBxLogoPnlSearchDispOrderInfo.Location = new System.Drawing.Point(424, 33);
            this.picBxLogoPnlSearchDispOrderInfo.Name = "picBxLogoPnlSearchDispOrderInfo";
            this.picBxLogoPnlSearchDispOrderInfo.Size = new System.Drawing.Size(100, 81);
            this.picBxLogoPnlSearchDispOrderInfo.TabIndex = 47;
            this.picBxLogoPnlSearchDispOrderInfo.TabStop = false;
            // 
            // FrmMainWindowStaff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 414);
            this.Controls.Add(this.menuStrpMainWindow);
            this.Controls.Add(this.pnlSearchDispOrderInfo);
            this.Controls.Add(this.panelAddNewCategory);
            this.Controls.Add(this.panelModifyCategory);
            this.Controls.Add(this.panelDeleteCategory);
            this.Controls.Add(this.panelDisplayCategory);
            this.Controls.Add(this.panelAddNewProduct);
            this.Controls.Add(this.panelModifyProduct);
            this.Controls.Add(this.panelDeleteProduct);
            this.Controls.Add(this.panelDisplayProduct);
            this.Controls.Add(this.panelSelectAProductToDisplay);
            this.Controls.Add(this.pnlAddStock);
            this.Controls.Add(this.pnlEditStockInfo);
            this.Controls.Add(this.pnlDisplayComputeExcessSHrtfall);
            this.Controls.Add(this.pnlComputeExcessShortFall);
            this.Controls.Add(this.pnlSearchAndDisplayStock);
            this.Controls.Add(this.pnlAddNewSupplierInfo);
            this.Controls.Add(this.pnlModifySupplierInfo);
            this.Controls.Add(this.pnlDeleteSupplierInfo);
            this.Controls.Add(this.pnlDisplaySupplierInformation_Details);
            this.Controls.Add(this.pnlDisplaySupplierInfo_ListOfCompanies);
            this.Controls.Add(this.pnlWelcomeScreen);
            this.Controls.Add(this.pnlRegisterStaff);
            this.Controls.Add(this.pnlModifyStaffInfo);
            this.Controls.Add(this.pnlDeleteStaffInfo);
            this.Controls.Add(this.pnlDisplayStaffInfo);
            this.Controls.Add(this.pnlResetPassword);
            this.Controls.Add(this.pnlAddOrderInformation);
            this.Controls.Add(this.pnlModifyOrderInfo);
            this.Controls.Add(this.pnlDeleteOrderInfo);
            this.MainMenuStrip = this.menuStrpMainWindow;
            this.Name = "FrmMainWindowStaff";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Welcome!";
            this.menuStrpMainWindow.ResumeLayout(false);
            this.menuStrpMainWindow.PerformLayout();
            this.pnlWelcomeScreen.ResumeLayout(false);
            this.pnlWelcomeScreen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoMainPnl)).EndInit();
            this.pnlRegisterStaff.ResumeLayout(false);
            this.pnlRegisterStaff.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoRegisterStaffPnl)).EndInit();
            this.tabCtrRegisterStaffPnl.ResumeLayout(false);
            this.tabStaffPersonalInformation.ResumeLayout(false);
            this.tabStaffPersonalInformation.PerformLayout();
            this.grpRegisterPersonalParticulars.ResumeLayout(false);
            this.grpRegisterPersonalParticulars.PerformLayout();
            this.tabStaffContactInfo.ResumeLayout(false);
            this.tabStaffContactInfo.PerformLayout();
            this.grpRegisterContactDetails.ResumeLayout(false);
            this.grpRegisterContactDetails.PerformLayout();
            this.tabStaffAccountInformation.ResumeLayout(false);
            this.tabStaffAccountInformation.PerformLayout();
            this.grpRegisterAccntDetails.ResumeLayout(false);
            this.grpRegisterAccntDetails.PerformLayout();
            this.pnlResetPassword.ResumeLayout(false);
            this.pnlResetPassword.PerformLayout();
            this.grpResetPW.ResumeLayout(false);
            this.grpResetPW.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoPnlResetPassword)).EndInit();
            this.pnlAddOrderInformation.ResumeLayout(false);
            this.pnlAddOrderInformation.PerformLayout();
            this.tabCtrOrderInfo.ResumeLayout(false);
            this.tabPgAddOrderCustInfo.ResumeLayout(false);
            this.tabPgAddOrderCustInfo.PerformLayout();
            this.grpBoxAddOrderInfo.ResumeLayout(false);
            this.grpBoxAddOrderInfo.PerformLayout();
            this.tabPgAddOrderInfo.ResumeLayout(false);
            this.tabPgAddOrderInfo.PerformLayout();
            this.grpAddOrderInfo.ResumeLayout(false);
            this.grpAddOrderInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoAddOrderPnl)).EndInit();
            this.pnlModifyOrderInfo.ResumeLayout(false);
            this.pnlModifyOrderInfo.PerformLayout();
            this.grpModifyOrderInfo.ResumeLayout(false);
            this.grpModifyOrderInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoPnlModifyOrderInfo)).EndInit();
            this.panelAddNewCategory.ResumeLayout(false);
            this.panelAddNewCategory.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelModifyCategory.ResumeLayout(false);
            this.panelModifyCategory.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelDeleteCategory.ResumeLayout(false);
            this.panelDeleteCategory.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panelDisplayCategory.ResumeLayout(false);
            this.panelDisplayCategory.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panelAddNewProduct.ResumeLayout(false);
            this.panelAddNewProduct.PerformLayout();
            this.tabControlAddNewProduct.ResumeLayout(false);
            this.tabAddNewProductInfo1.ResumeLayout(false);
            this.tabAddNewProductInfo1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabAddNewProductInfo2.ResumeLayout(false);
            this.tabAddNewProductInfo2.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbxAddProductImage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbxAddProductImage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panelModifyProduct.ResumeLayout(false);
            this.panelModifyProduct.PerformLayout();
            this.tabControlModifyProduct.ResumeLayout(false);
            this.tabModifyProductInfo1.ResumeLayout(false);
            this.tabModifyProductInfo1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabModifyProductInfo2.ResumeLayout(false);
            this.tabModifyProductInfo2.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbxModifyProductImage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbxModifyProductImage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panelDeleteProduct.ResumeLayout(false);
            this.panelDeleteProduct.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbxDeleteProductImage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbxDeleteProductImage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.panelDisplayProduct.ResumeLayout(false);
            this.panelDisplayProduct.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbxDisplayProductImage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbxDisplayProductImage1)).EndInit();
            this.pnlAddStock.ResumeLayout(false);
            this.pnlAddStock.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.pnlEditStockInfo.ResumeLayout(false);
            this.pnlEditStockInfo.PerformLayout();
            this.grpEditStockInfo.ResumeLayout(false);
            this.grpEditStockInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoUpdateStock)).EndInit();
            this.pnlDisplayComputeExcessSHrtfall.ResumeLayout(false);
            this.pnlDisplayComputeExcessSHrtfall.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoDeleteSupplierPnl)).EndInit();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.pnlComputeExcessShortFall.ResumeLayout(false);
            this.pnlComputeExcessShortFall.PerformLayout();
            this.grpBoxlIstOfStocks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoPnlComputeExcessShrt)).EndInit();
            this.pnlSearchAndDisplayStock.ResumeLayout(false);
            this.pnlSearchAndDisplayStock.PerformLayout();
            this.grpBoxDispStockInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoPnlDispStockInfo)).EndInit();
            this.pnlAddNewSupplierInfo.ResumeLayout(false);
            this.pnlAddNewSupplierInfo.PerformLayout();
            this.tabControl4.ResumeLayout(false);
            this.tabPgAddSupplier_SupplierInfo.ResumeLayout(false);
            this.tabPgAddSupplier_SupplierInfo.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.tabPgAddSupplier_ContactInfo.ResumeLayout(false);
            this.tabPgAddSupplier_ContactInfo.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoRegisterSupplier)).EndInit();
            this.pnlModifySupplierInfo.ResumeLayout(false);
            this.pnlModifySupplierInfo.PerformLayout();
            this.tabControl5.ResumeLayout(false);
            this.tabPgModifySupplier_SupplierInfo.ResumeLayout(false);
            this.tabPgModifySupplier_SupplierInfo.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.tabPgModifySupplier_ContactInfo.ResumeLayout(false);
            this.tabPgModifySupplier_ContactInfo.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoModifySupplierInfoPnl)).EndInit();
            this.pnlDeleteSupplierInfo.ResumeLayout(false);
            this.pnlDeleteSupplierInfo.PerformLayout();
            this.tabControl6.ResumeLayout(false);
            this.tabPgDeleteSupplier_SupplierInfo.ResumeLayout(false);
            this.tabPgDeleteSupplier_SupplierInfo.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.tabPgDeleteSupplier_ContactInfo.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.pnlDisplaySupplierInformation_Details.ResumeLayout(false);
            this.pnlDisplaySupplierInformation_Details.PerformLayout();
            this.tabControl7.ResumeLayout(false);
            this.tabPgDisplaySupplier_SupplierInfo.ResumeLayout(false);
            this.tabPgDisplaySupplier_SupplierInfo.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.tabPgDisplaySupplier_ContactInfo.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.pnlDisplaySupplierInfo_ListOfCompanies.ResumeLayout(false);
            this.pnlDisplaySupplierInfo_ListOfCompanies.PerformLayout();
            this.grpBoxDispSupplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoPnlDispSupplierInfo)).EndInit();
            this.panelSelectAProductToDisplay.ResumeLayout(false);
            this.panelSelectAProductToDisplay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.groupBox24.ResumeLayout(false);
            this.pnlModifyStaffInfo.ResumeLayout(false);
            this.pnlModifyStaffInfo.PerformLayout();
            this.tabControl3.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoModifyStaffInfoPnl)).EndInit();
            this.pnlDeleteStaffInfo.ResumeLayout(false);
            this.pnlDeleteStaffInfo.PerformLayout();
            this.gpBoxDisplayStaffInfo.ResumeLayout(false);
            this.gpBoxDisplayStaffInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoDeleteStaffPnl)).EndInit();
            this.pnlDisplayStaffInfo.ResumeLayout(false);
            this.pnlDisplayStaffInfo.PerformLayout();
            this.grpBoxDispStaff.ResumeLayout(false);
            this.grpBoxDispStaff.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoPnlDispStaffInfo)).EndInit();
            this.pnlDeleteOrderInfo.ResumeLayout(false);
            this.pnlDeleteOrderInfo.PerformLayout();
            this.grpDeleteDispOrderInfo.ResumeLayout(false);
            this.grpDeleteDispOrderInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxDeleteOrderInformation)).EndInit();
            this.pnlSearchDispOrderInfo.ResumeLayout(false);
            this.pnlSearchDispOrderInfo.PerformLayout();
            this.grpSearchDispOrderInfo.ResumeLayout(false);
            this.grpSearchDispOrderInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoPnlSearchDispOrderInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrpMainWindow;
        private System.Windows.Forms.ToolStripMenuItem staffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registerNewStaffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteExistingStaffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchForStaffInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetStaffPasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewCategoryInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyCategoryInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteCategoryInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAndDisplayCategoryInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewProductInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyProductInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteExistingProductInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchDisplayProductInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewOrderInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyExistingOrderInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAndDisplayOrderInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem delToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyExistingSupplierInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteExistingSupplierInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAndDisplaySupplierInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editProductQuantityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAndDisplayProductQuantityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem computeExcessShortfallToolStripMenuItem;
        private System.Windows.Forms.Panel pnlWelcomeScreen;
        private System.Windows.Forms.Label lblWelcome2;
        private System.Windows.Forms.Label lblWelcome1;
        private System.Windows.Forms.PictureBox picBoxLogoMainPnl;
        private System.Windows.Forms.Label lblMainWindowInfo;
        private System.Windows.Forms.Panel pnlRegisterStaff;
        private System.Windows.Forms.PictureBox picBoxLogoRegisterStaffPnl;
        private System.Windows.Forms.TabControl tabCtrRegisterStaffPnl;
        private System.Windows.Forms.TabPage tabStaffPersonalInformation;
        private System.Windows.Forms.TabPage tabStaffAccountInformation;
        private System.Windows.Forms.TabPage tabStaffContactInfo;
        private System.Windows.Forms.GroupBox grpRegisterPersonalParticulars;
        private System.Windows.Forms.RadioButton rdBtnFemaleRegister;
        private System.Windows.Forms.RadioButton rdBtnMaleRegister;
        private System.Windows.Forms.DateTimePicker dtpRegisterDateOfBirth;
        private System.Windows.Forms.Label lblRegisterStaffGender;
        private System.Windows.Forms.Label lblRegisterDateOfBirth;
        private System.Windows.Forms.TextBox tbxRegisterNRIC;
        private System.Windows.Forms.Label lblRegisterNRIC;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label lblMandatoryFields1;
        private System.Windows.Forms.Label lblFill1;
        private System.Windows.Forms.Label lblMandatoryFields2;
        private System.Windows.Forms.Label lblFill2;
        private System.Windows.Forms.GroupBox grpRegisterContactDetails;
        private System.Windows.Forms.Label lblRegisterHpContact;
        private System.Windows.Forms.Label lblRegisterHomeContact;
        private System.Windows.Forms.Label lblRegisterContactNo;
        private System.Windows.Forms.TextBox tbxRegisterHpNoContact;
        private System.Windows.Forms.TextBox tbxRegisterHomeNo;
        private System.Windows.Forms.TextBox tbxRegisterAddress;
        private System.Windows.Forms.Label lblRegisterAddress;
        private System.Windows.Forms.Label lblMandatoryFields3;
        private System.Windows.Forms.Label lblRegisterStaff;
        private System.Windows.Forms.Label lblInfoRegisterStaff;
        private System.Windows.Forms.GroupBox grpRegisterAccntDetails;
        private System.Windows.Forms.Button btnCancelRegisterStaff;
        private System.Windows.Forms.Button btnRegisterStaff;
        private System.Windows.Forms.TextBox tbxConfirmRegisterPassword;
        private System.Windows.Forms.Label lblCfmRegisteredPassword;
        private System.Windows.Forms.TextBox tbxRegisterPassword;
        private System.Windows.Forms.Label lblRegisterPassword;
        private System.Windows.Forms.Label lblRegisterStaffRole;
        private System.Windows.Forms.Panel pnlResetPassword;
        private System.Windows.Forms.Label lblResetPassword;
        private System.Windows.Forms.PictureBox picBxLogoPnlResetPassword;
        private System.Windows.Forms.Label lblNRICStaffResetPW;
        private System.Windows.Forms.Label lblNameOfStaffResetPW;
        private System.Windows.Forms.Label lblDOBStaffResetPW;
        private System.Windows.Forms.GroupBox grpResetPW;
        private System.Windows.Forms.Label lblResetPW;
        private System.Windows.Forms.Button lblCancelResetPw;
        private System.Windows.Forms.Button btnResetPw;
        private System.Windows.Forms.TextBox tbxCfmResetPw;
        private System.Windows.Forms.TextBox tbxResetPw;
        private System.Windows.Forms.Label lblCfmNewPw;
        private System.Windows.Forms.Panel pnlAddOrderInformation;
        private System.Windows.Forms.Label lblAddOrder;
        private System.Windows.Forms.PictureBox picBxLogoAddOrderPnl;
        private System.Windows.Forms.Label lblAddOrderInfo;
        private System.Windows.Forms.TabControl tabCtrOrderInfo;
        private System.Windows.Forms.TabPage tabPgAddOrderCustInfo;
        private System.Windows.Forms.TabPage tabPgAddOrderInfo;
        private System.Windows.Forms.GroupBox grpBoxAddOrderInfo;
        private System.Windows.Forms.TextBox tbxAddOrderCustDestinationAddress;
        private System.Windows.Forms.Label lblAddOrderDestinationAddress;
        private System.Windows.Forms.TextBox tbxAddOrderNameOfCust;
        private System.Windows.Forms.Label lblAddOrderNameOfCust;
        private System.Windows.Forms.Label lblMandatoryFields5;
        private System.Windows.Forms.GroupBox grpAddOrderInfo;
        private System.Windows.Forms.Label lblMandatoryFields6;
        private System.Windows.Forms.Button btnCancelAddOrderInfo;
        private System.Windows.Forms.Button btnAddOrderInfo;
        private System.Windows.Forms.Label lblProceedToOrderInfo;
        private System.Windows.Forms.Label lblProceedToContactInformation;
        private System.Windows.Forms.Label lblProceedToAccountInfo;
        private System.Windows.Forms.Panel pnlModifyOrderInfo;
        private System.Windows.Forms.Label lblModifyOrderInformation;
        private System.Windows.Forms.PictureBox picBxLogoPnlModifyOrderInfo;
        private System.Windows.Forms.GroupBox grpModifyOrderInfo;
        private System.Windows.Forms.TextBox tbxModifyDestinationAddress;
        private System.Windows.Forms.Label lblModifyDestinationAddress;
        private System.Windows.Forms.Label lblModifyOrderDate;
        private System.Windows.Forms.Label lblModifyDispCustomerName;
        private System.Windows.Forms.Label lblModifyDispOrderRefNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbxModifyOrderTotalPrice;
        private System.Windows.Forms.Label lblModifyOrderTotalPrice;
        private System.Windows.Forms.Label lblModifyDispDestinationAddress;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblModifyDisplayTotalPrice;
        private System.Windows.Forms.Label lblModifyDisplayDateOfOrder;
        private System.Windows.Forms.Label lblModifyOrderInfo;
        private System.Windows.Forms.Button btnCancelModifyOrderInformation;
        private System.Windows.Forms.Button btnModifyOrderInformation;
        private System.Windows.Forms.Panel panelAddNewCategory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancelAddCategory;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblAddCategoryName;
        private System.Windows.Forms.TextBox tbxAddCategoryDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAddCategory;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panelModifyCategory;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnCancelModifyCategory;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblModifyCategoryName;
        private System.Windows.Forms.TextBox tbxModifyCategoryDescrption;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnModifyCategory;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panelDeleteCategory;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnCancelDeleteCategory;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblDeleteCategoryName;
        private System.Windows.Forms.TextBox tbxDeleteCategoryDescrption;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnDeleteCategory;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panelDisplayCategory;
        private System.Windows.Forms.Button btnDisplayCategory;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblDisplayCategoryName;
        private System.Windows.Forms.TextBox tbxDisplayCategoryDescrption;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panelAddNewProduct;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TabControl tabControlAddNewProduct;
        private System.Windows.Forms.TabPage tabAddNewProductInfo1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox cbxAddProductCategory;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tbxAddProductDescription;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label lblAddProductName;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TabPage tabAddNewProductInfo2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.PictureBox picbxAddProductImage2;
        private System.Windows.Forms.PictureBox picbxAddProductImage1;
        private System.Windows.Forms.TextBox tbxAddProductRetailPrice;
        private System.Windows.Forms.TextBox tbxAddProductMSRP;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button btnAddProduct;
        private System.Windows.Forms.Button btnCancelAddProduct;
        private System.Windows.Forms.Panel panelModifyProduct;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TabControl tabControlModifyProduct;
        private System.Windows.Forms.TabPage tabModifyProductInfo1;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label lblModifyProductModelNumber;
        private System.Windows.Forms.ComboBox cbxModifyProductCategory;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tbxModifyProductDescription;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label lblModifyProductName;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TabPage tabModifyProductInfo2;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.PictureBox picbxModifyProductImage2;
        private System.Windows.Forms.PictureBox picbxModifyProductImage1;
        private System.Windows.Forms.TextBox tbxModifyProductRetailPrice;
        private System.Windows.Forms.TextBox tbxModifyProductMSRP;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Button btnModifyProduct;
        private System.Windows.Forms.Button btnCancelModifyProduct;
        private System.Windows.Forms.Panel panelDeleteProduct;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button btnDeleteProduct;
        private System.Windows.Forms.Button btnCancelDeleteProduct;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.PictureBox picbxDeleteProductImage2;
        private System.Windows.Forms.PictureBox picbxDeleteProductImage1;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Panel panelDisplayProduct;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Button btnDisplayProduct;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox picbxDisplayProductImage2;
        private System.Windows.Forms.PictureBox picbxDisplayProductImage1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnAddProductRemoveImage2;
        private System.Windows.Forms.Button btnAddProductOpenImage2;
        private System.Windows.Forms.Button btnAddProductRemoveImage1;
        private System.Windows.Forms.Button btnAddProductOpenImage1;
        private System.Windows.Forms.Button btnModifyProductRemoveImage2;
        private System.Windows.Forms.Button btnModifyProductOpenImage2;
        private System.Windows.Forms.Button btnModifyProductRemoveImage1;
        private System.Windows.Forms.Button btnModifyProductOpenImage1;
        private System.Windows.Forms.TextBox tbxAddOrderTotalPrice;
        private System.Windows.Forms.Label lblAddOrderTotalPrice;
        private System.Windows.Forms.Label lblAddDateOfOrder;
        private System.Windows.Forms.TextBox tbxAddOrderRefNum;
        private System.Windows.Forms.Label lblAddOrderRefNum;
        private System.Windows.Forms.CheckBox chkBoxAdministrator;
        private System.Windows.Forms.CheckBox chkBoxInventoryManager;
        private System.Windows.Forms.CheckBox chkBoxOrderManager;
        private System.Windows.Forms.CheckBox chkBoxCatalogueManager;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel pnlAddStock;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TextBox tbxAddStock_CompanyName;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox tbxAddStock_StockQty;
        private System.Windows.Forms.TextBox tbxAddStock_ModelName;
        private System.Windows.Forms.TextBox tbxAddStock_ProductName;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnAddStock_Cancel;
        private System.Windows.Forms.Button btnAddStock_Add;
        private System.Windows.Forms.Panel pnlEditStockInfo;
        private System.Windows.Forms.GroupBox grpEditStockInfo;
        private System.Windows.Forms.Label lblEditStock_DisplayCompanyName;
        private System.Windows.Forms.Label lblEditStock_DisplayModelName;
        private System.Windows.Forms.Label lblEditStock_DisplayProductName;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox tbxEditStock_StockQty;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label lblInfoUpdateStock;
        private System.Windows.Forms.Label lblUpdateStock;
        private System.Windows.Forms.PictureBox picBoxLogoUpdateStock;
        private System.Windows.Forms.Button btnCancelUpdateStock;
        private System.Windows.Forms.Button btnUpdateStock;
        private System.Windows.Forms.Panel pnlDisplayComputeExcessSHrtfall;
        private System.Windows.Forms.Label lblDeleteExistingSupplierInfo;
        private System.Windows.Forms.PictureBox picBxLogoDeleteSupplierPnl;
        private System.Windows.Forms.Button btnDispComputeExcessSHort_OK;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label lblProModelName_ComputeExcessShortFallDisplay;
        private System.Windows.Forms.Label lblTotalOrder_ComputeExcessShortFallDisplay;
        private System.Windows.Forms.Label lblComputeExcessShortFallDisplay;
        private System.Windows.Forms.Label lblTotalQty_ComputeExcessShortFallDisplay;
        private System.Windows.Forms.Label lblProName_ComputeExcessShortFallDisplay;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Panel pnlComputeExcessShortFall;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Button btnCmputeExcessShortOK;
        private System.Windows.Forms.GroupBox grpBoxlIstOfStocks;
        private System.Windows.Forms.ListBox listBxListOfStock;
        private System.Windows.Forms.Label lblComputeExcessSHortFall;
        private System.Windows.Forms.PictureBox picBoxLogoPnlComputeExcessShrt;
        private System.Windows.Forms.Panel pnlSearchAndDisplayStock;
        private System.Windows.Forms.Button btnDispStockInfoOK;
        private System.Windows.Forms.GroupBox grpBoxDispStockInfo;
        private System.Windows.Forms.ListBox listBxDispStockInfo;
        private System.Windows.Forms.Label lblDispExistingStock;
        private System.Windows.Forms.PictureBox picBoxLogoPnlDispStockInfo;
        private System.Windows.Forms.Panel pnlAddNewSupplierInfo;
        private System.Windows.Forms.Label lblInfoRegisterSupplier;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPgAddSupplier_SupplierInfo;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.TextBox tbxRegisterCompanyName;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.TextBox tbxRegisterSupplierName;
        private System.Windows.Forms.Label lblRegisterSupplierName;
        private System.Windows.Forms.TabPage tabPgAddSupplier_ContactInfo;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TextBox tbxRegisterCityCountry;
        private System.Windows.Forms.Label lblRegisterCityCountry;
        private System.Windows.Forms.Label lblRegisterFaxContact;
        private System.Windows.Forms.Label lblRegisterOfficeNum;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox tbxRegisterFaxNum;
        private System.Windows.Forms.TextBox tbxRegisterPhoneOfficeNo;
        private System.Windows.Forms.TextBox tbxRegisterPostalCode;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox tbxRegisterAddress_Supplier;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Button btnCancelRegisterSupplier;
        private System.Windows.Forms.Button btnRegisterSupplier;
        private System.Windows.Forms.Label lblRegisterSupplier;
        private System.Windows.Forms.PictureBox picBoxLogoRegisterSupplier;
        private System.Windows.Forms.Panel pnlModifySupplierInfo;
        private System.Windows.Forms.TabControl tabControl5;
        private System.Windows.Forms.TabPage tabPgModifySupplier_SupplierInfo;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label lblModifySupplier_DispCompName;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox tbxModifySupplier_Name;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TabPage tabPgModifySupplier_ContactInfo;
        private System.Windows.Forms.Button btnMdifySupplier_Cancel;
        private System.Windows.Forms.Button btnModifySupplier_Update;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.TextBox tbxModifySupplier_CityCountry;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox tbxModifySupplier_Fax;
        private System.Windows.Forms.TextBox tbxModifySupplier_Phone;
        private System.Windows.Forms.TextBox tbxModifySupplier_PostalCode;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox tbxModifySupplier_Address;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label lblModifySupplierInfo;
        private System.Windows.Forms.Label lblModifySupplierInformation;
        private System.Windows.Forms.PictureBox picBoxLogoModifySupplierInfoPnl;
        private System.Windows.Forms.Panel pnlDeleteSupplierInfo;
        private System.Windows.Forms.TabControl tabControl6;
        private System.Windows.Forms.TabPage tabPgDeleteSupplier_SupplierInfo;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Label lblDeleteSupplier_CompanyName;
        private System.Windows.Forms.Label lblDeleteSupplier_SupplierName;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.TabPage tabPgDeleteSupplier_ContactInfo;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Label lblDeleteSupplier_PostalCode;
        private System.Windows.Forms.Label lblDeleteSupplier_CityCountry;
        private System.Windows.Forms.Label lblDeleteSupplier_FaxNum;
        private System.Windows.Forms.Label lblDeleteSupplier_PhoneNum;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.TextBox tbxDeleteSupplier_Address;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnDeleteSupplier;
        private System.Windows.Forms.Label lblCfmDeleteSupplier;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Panel pnlDisplaySupplierInformation_Details;
        private System.Windows.Forms.TabControl tabControl7;
        private System.Windows.Forms.TabPage tabPgDisplaySupplier_SupplierInfo;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.Label lblDisplaySupplierInformation_Display_CompanyName;
        private System.Windows.Forms.Label lblDisplaySupplierInformation_Display_SupplierName;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.TabPage tabPgDisplaySupplier_ContactInfo;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.Label lblDisplaySupplierInformation_Display_PostalCode;
        private System.Windows.Forms.Label lblDisplaySupplierInformation_Display_CityCountry;
        private System.Windows.Forms.Label lblDisplaySupplierInformation_Display_FaxNum;
        private System.Windows.Forms.Label lblDisplaySupplierInformation_Display_PhoneNum;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.TextBox tbxDisplaySupplierInformation_Display_Address;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Button btnDisplaySupplierInformation_Details_OK;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Panel pnlDisplaySupplierInfo_ListOfCompanies;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Button btnDispSupplierInfoOK;
        private System.Windows.Forms.GroupBox grpBoxDispSupplier;
        private System.Windows.Forms.ListBox listBxDispSupplierInfo;
        private System.Windows.Forms.Label lblDispExistingSupplier;
        private System.Windows.Forms.PictureBox picBoxLogoPnlDispSupplierInfo;
        private System.Windows.Forms.Label lblAddProductModelNumber;
        private System.Windows.Forms.Button btnAddProductEnlargeImage1;
        private System.Windows.Forms.Button btnAddProductEnlargeImage2;
        private System.Windows.Forms.Button btnModifyProductEnlargeImage2;
        private System.Windows.Forms.Button btnModifyProductEnlargeImage1;
        private System.Windows.Forms.Button btnDeleteProductEnlargeImage2;
        private System.Windows.Forms.Button btnDeleteProductEnlargeImage1;
        private System.Windows.Forms.Button btnDisplayProductEnlargeImage2;
        private System.Windows.Forms.Button btnDisplayProductEnlargeImage1;
        private System.Windows.Forms.Label lblDeleteProductDisplayMSRP;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label lblDeleteProductDisplayRetailPrice;
        private System.Windows.Forms.Label lblDeleteProductDisplayModelNumber;
        private System.Windows.Forms.Label lblDeleteProductDisplayName;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tbxDeleteProductDisplayDescription;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label lblDeleteProductDisplayCategory;
        private System.Windows.Forms.TextBox tbxDisplayProductDisplayDescription;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label lblDisplayProductDisplayCategory;
        private System.Windows.Forms.Label lblDisplayProductDisplayMSRP;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label lblDisplayProductDisplayRetailPRice;
        private System.Windows.Forms.Label lblDisplayProductDisplayModelNumber;
        private System.Windows.Forms.Label lblDisplayProductDisplayName;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Panel panelSelectAProductToDisplay;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Button btnCancelSelectAProductToDisplay;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.ListBox listbxSelectAProductToDisplay;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Button btnSelectAProductToDisplay;
        private System.Windows.Forms.Label lblRegisterStaffName;
        private System.Windows.Forms.Label lblDispDOBStaffResetRw;
        private System.Windows.Forms.Label lblDispNRICResetPw;
        private System.Windows.Forms.Label lblDispStaffNameResetPw;
        private System.Windows.Forms.Panel pnlModifyStaffInfo;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label lblModifyStaffGender;
        private System.Windows.Forms.Label lblModifyStaffDOB;
        private System.Windows.Forms.Label lblModifyStaffNRIC;
        private System.Windows.Forms.Label lblModifyStaffName;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox tbxModifyHpNoContact;
        private System.Windows.Forms.TextBox tbxModifyHomeNo;
        private System.Windows.Forms.TextBox tbxModifyAddress;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button btnModifyStaffCancel;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Button btnModifyStaffInformation;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.CheckBox chkBoxModifyInventoryManager;
        private System.Windows.Forms.CheckBox chkBoxModifyOrderManager;
        private System.Windows.Forms.CheckBox chkBoxModifyCatalogueManager;
        private System.Windows.Forms.CheckBox chkBoxModifyAdministrator;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label lblFill6;
        private System.Windows.Forms.Label lblModifyStaffInfo;
        private System.Windows.Forms.Label lblModifyStaffInformation;
        private System.Windows.Forms.PictureBox picBoxLogoModifyStaffInfoPnl;
        private System.Windows.Forms.Panel pnlDeleteStaffInfo;
        private System.Windows.Forms.Label lblCfmDeleteStaff;
        private System.Windows.Forms.Button btnCancelDelete;
        private System.Windows.Forms.Button btnDeleteStaff;
        private System.Windows.Forms.GroupBox gpBoxDisplayStaffInfo;
        private System.Windows.Forms.Label lblDispDelStaffAddress;
        private System.Windows.Forms.Label lblDispDelStaffRole;
        private System.Windows.Forms.Label lblDispDelStaffMobile;
        private System.Windows.Forms.Label lblDispDelStaffCintact;
        private System.Windows.Forms.Label lblDispDelStaffGender;
        private System.Windows.Forms.Label lblDispDelStaffDOB;
        private System.Windows.Forms.Label lblDispDelStaffNRIC;
        private System.Windows.Forms.Label lblDispDelStaffName;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label lblDeleteExistingStaffInfo;
        private System.Windows.Forms.PictureBox picBxLogoDeleteStaffPnl;
        private System.Windows.Forms.Panel pnlDisplayStaffInfo;
        private System.Windows.Forms.Button btnDispStaffInfoOK;
        private System.Windows.Forms.GroupBox grpBoxDispStaff;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.PictureBox picBoxLogoPnlDispStaffInfo;
        private System.Windows.Forms.Label lblDispExistingStaff;
        private System.Windows.Forms.DateTimePicker dtpDateOfAddOrder;
        private System.Windows.Forms.DateTimePicker dtpModifyOrderDate;
        private System.Windows.Forms.Panel pnlDeleteOrderInfo;
        private System.Windows.Forms.Label lblDeleteOrderInfo;
        private System.Windows.Forms.Button btnCancelDeleteOrderInfo;
        private System.Windows.Forms.Button btnDeleteOrderInfo;
        private System.Windows.Forms.GroupBox grpDeleteDispOrderInfo;
        private System.Windows.Forms.Label lblDispDelTotalPrice;
        private System.Windows.Forms.Label lblDispDelOrderDate;
        private System.Windows.Forms.Label lblDispDelOrderDestAdd;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label lblDispDelCustName;
        private System.Windows.Forms.Label lblDispDelORN;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label lblDeleteOrderInformation;
        private System.Windows.Forms.PictureBox picBoxDeleteOrderInformation;
        private System.Windows.Forms.Panel pnlSearchDispOrderInfo;
        private System.Windows.Forms.Button btnOKSearchDispOrderInfo;
        private System.Windows.Forms.GroupBox grpSearchDispOrderInfo;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Label lblSearchDispOrderInfo;
        private System.Windows.Forms.PictureBox picBxLogoPnlSearchDispOrderInfo;
    }
}