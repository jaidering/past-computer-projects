﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Assignment_Phase_2
{
    public class Order
    {
        //attributes
        private string custName;
        private string destAddress;
        private int orderRefNum;
        private string orderDate;
        private double totalPrice;
        private ArrayList listOfProduct;

        //constructor
        public Order(string custName, string destAddress, int orderRefNum, string orderDate,
            double totalPrice)
        {
            this.custName = custName;
            this.destAddress = destAddress;
            this.orderRefNum = orderRefNum;
            this.orderDate = orderDate;
            this.totalPrice = totalPrice;
            listOfProduct = new ArrayList();
        }

        //methods
        public string CustName
        {
            get
            {
                return custName;
            }
            set
            {
                custName = value;
            }
        }

        public string DestAddress
        {
            get
            {
                return destAddress;
            }
            set
            {
                destAddress = value;
            }
        }

        public int OrderRefNum
        {
            get
            {
                return orderRefNum;
            }
            set
            {
                orderRefNum = value;
            }
        }

        public string OrderDate
        {
            get
            {
                return orderDate;
            }
            set
            {
                orderDate = value;
            }
        }

        public double TotalPrice
        {
            get
            {
                return totalPrice;
            }
            set
            {
                totalPrice = value;
            }
        }
    }
}
