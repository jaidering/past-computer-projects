﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assignment_Phase_2
{
    public class Stock
    {
        private string supplier;
        private string productName;
        private string modelNumber;
        private int quantity;

        public Stock(string supplier, string productName, string modelNumber, int quantity)
        {
            this.supplier = supplier;
            this.productName = productName;
            this.modelNumber = modelNumber;
            this.quantity = quantity;
        }

        public string Supplier
        {
            get { return supplier; }
        }

        public string ProductName
        {
            get { return productName; }
        }

        public string ModelNumber
        {
            get { return modelNumber; }
        }

        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
    }
}
