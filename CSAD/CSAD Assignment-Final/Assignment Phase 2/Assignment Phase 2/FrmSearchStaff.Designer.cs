﻿namespace Assignment_Phase_2
{
    partial class FrmSearchStaff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearchStaff));
            this.pnlSearchStaff = new System.Windows.Forms.Panel();
            this.lblSearchForStaff = new System.Windows.Forms.Label();
            this.picBoxLogoSearchPnlSearchtaff = new System.Windows.Forms.PictureBox();
            this.btnSearchStaffCancel = new System.Windows.Forms.Button();
            this.btnSearchStaff = new System.Windows.Forms.Button();
            this.lblSearchStaffInfo = new System.Windows.Forms.Label();
            this.grpBoxSearchStaff = new System.Windows.Forms.GroupBox();
            this.tbxSearchStaff = new System.Windows.Forms.TextBox();
            this.lblSearchName = new System.Windows.Forms.Label();
            this.pnlSearchDispStaff = new System.Windows.Forms.Panel();
            this.lblSearchDispStaff = new System.Windows.Forms.Label();
            this.picBoxLogoSearchPnlSearchDispStaff = new System.Windows.Forms.PictureBox();
            this.btnCancelSearchDispStaff = new System.Windows.Forms.Button();
            this.btnSearchDispStaff = new System.Windows.Forms.Button();
            this.lblSearchandDispInfo = new System.Windows.Forms.Label();
            this.grpBxSearchStaff = new System.Windows.Forms.GroupBox();
            this.comboBoxSearchMode = new System.Windows.Forms.ComboBox();
            this.lblSearchMode = new System.Windows.Forms.Label();
            this.tbxSearchStaffName = new System.Windows.Forms.TextBox();
            this.lblSearchStaffName = new System.Windows.Forms.Label();
            this.pnlSearchStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoSearchPnlSearchtaff)).BeginInit();
            this.grpBoxSearchStaff.SuspendLayout();
            this.pnlSearchDispStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoSearchPnlSearchDispStaff)).BeginInit();
            this.grpBxSearchStaff.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSearchStaff
            // 
            this.pnlSearchStaff.Controls.Add(this.lblSearchForStaff);
            this.pnlSearchStaff.Controls.Add(this.picBoxLogoSearchPnlSearchtaff);
            this.pnlSearchStaff.Controls.Add(this.btnSearchStaffCancel);
            this.pnlSearchStaff.Controls.Add(this.btnSearchStaff);
            this.pnlSearchStaff.Controls.Add(this.lblSearchStaffInfo);
            this.pnlSearchStaff.Controls.Add(this.grpBoxSearchStaff);
            this.pnlSearchStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchStaff.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchStaff.Name = "pnlSearchStaff";
            this.pnlSearchStaff.Size = new System.Drawing.Size(384, 214);
            this.pnlSearchStaff.TabIndex = 44;
            // 
            // lblSearchForStaff
            // 
            this.lblSearchForStaff.AutoSize = true;
            this.lblSearchForStaff.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchForStaff.Location = new System.Drawing.Point(154, 33);
            this.lblSearchForStaff.Name = "lblSearchForStaff";
            this.lblSearchForStaff.Size = new System.Drawing.Size(146, 23);
            this.lblSearchForStaff.TabIndex = 45;
            this.lblSearchForStaff.Text = "Search for Staff";
            // 
            // picBoxLogoSearchPnlSearchtaff
            // 
            this.picBoxLogoSearchPnlSearchtaff.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoSearchPnlSearchtaff.Image")));
            this.picBoxLogoSearchPnlSearchtaff.Location = new System.Drawing.Point(12, 8);
            this.picBoxLogoSearchPnlSearchtaff.Name = "picBoxLogoSearchPnlSearchtaff";
            this.picBoxLogoSearchPnlSearchtaff.Size = new System.Drawing.Size(93, 77);
            this.picBoxLogoSearchPnlSearchtaff.TabIndex = 44;
            this.picBoxLogoSearchPnlSearchtaff.TabStop = false;
            // 
            // btnSearchStaffCancel
            // 
            this.btnSearchStaffCancel.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchStaffCancel.Location = new System.Drawing.Point(297, 181);
            this.btnSearchStaffCancel.Name = "btnSearchStaffCancel";
            this.btnSearchStaffCancel.Size = new System.Drawing.Size(75, 23);
            this.btnSearchStaffCancel.TabIndex = 3;
            this.btnSearchStaffCancel.Text = "Cancel";
            this.btnSearchStaffCancel.UseVisualStyleBackColor = true;
            this.btnSearchStaffCancel.Click += new System.EventHandler(this.btnSearchStaffCancel_Click);
            // 
            // btnSearchStaff
            // 
            this.btnSearchStaff.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchStaff.Location = new System.Drawing.Point(200, 181);
            this.btnSearchStaff.Name = "btnSearchStaff";
            this.btnSearchStaff.Size = new System.Drawing.Size(75, 23);
            this.btnSearchStaff.TabIndex = 2;
            this.btnSearchStaff.Text = "Search";
            this.btnSearchStaff.UseVisualStyleBackColor = true;
            this.btnSearchStaff.Click += new System.EventHandler(this.btnSearchStaff_Click);
            // 
            // lblSearchStaffInfo
            // 
            this.lblSearchStaffInfo.AutoSize = true;
            this.lblSearchStaffInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchStaffInfo.Location = new System.Drawing.Point(9, 97);
            this.lblSearchStaffInfo.Name = "lblSearchStaffInfo";
            this.lblSearchStaffInfo.Size = new System.Drawing.Size(202, 15);
            this.lblSearchStaffInfo.TabIndex = 1;
            this.lblSearchStaffInfo.Text = "Please enter a Staff Name to search...";
            // 
            // grpBoxSearchStaff
            // 
            this.grpBoxSearchStaff.Controls.Add(this.tbxSearchStaff);
            this.grpBoxSearchStaff.Controls.Add(this.lblSearchName);
            this.grpBoxSearchStaff.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxSearchStaff.Location = new System.Drawing.Point(12, 115);
            this.grpBoxSearchStaff.Name = "grpBoxSearchStaff";
            this.grpBoxSearchStaff.Size = new System.Drawing.Size(360, 60);
            this.grpBoxSearchStaff.TabIndex = 0;
            this.grpBoxSearchStaff.TabStop = false;
            this.grpBoxSearchStaff.Text = "Search for Staff";
            // 
            // tbxSearchStaff
            // 
            this.tbxSearchStaff.Location = new System.Drawing.Point(119, 22);
            this.tbxSearchStaff.Name = "tbxSearchStaff";
            this.tbxSearchStaff.Size = new System.Drawing.Size(201, 23);
            this.tbxSearchStaff.TabIndex = 3;
            // 
            // lblSearchName
            // 
            this.lblSearchName.AutoSize = true;
            this.lblSearchName.Location = new System.Drawing.Point(41, 25);
            this.lblSearchName.Name = "lblSearchName";
            this.lblSearchName.Size = new System.Drawing.Size(72, 15);
            this.lblSearchName.TabIndex = 2;
            this.lblSearchName.Text = "Staff Name:";
            // 
            // pnlSearchDispStaff
            // 
            this.pnlSearchDispStaff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlSearchDispStaff.Controls.Add(this.lblSearchDispStaff);
            this.pnlSearchDispStaff.Controls.Add(this.picBoxLogoSearchPnlSearchDispStaff);
            this.pnlSearchDispStaff.Controls.Add(this.btnCancelSearchDispStaff);
            this.pnlSearchDispStaff.Controls.Add(this.btnSearchDispStaff);
            this.pnlSearchDispStaff.Controls.Add(this.lblSearchandDispInfo);
            this.pnlSearchDispStaff.Controls.Add(this.grpBxSearchStaff);
            this.pnlSearchDispStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchDispStaff.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchDispStaff.Name = "pnlSearchDispStaff";
            this.pnlSearchDispStaff.Size = new System.Drawing.Size(384, 214);
            this.pnlSearchDispStaff.TabIndex = 45;
            // 
            // lblSearchDispStaff
            // 
            this.lblSearchDispStaff.AutoSize = true;
            this.lblSearchDispStaff.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchDispStaff.Location = new System.Drawing.Point(154, 33);
            this.lblSearchDispStaff.Name = "lblSearchDispStaff";
            this.lblSearchDispStaff.Size = new System.Drawing.Size(194, 23);
            this.lblSearchDispStaff.TabIndex = 45;
            this.lblSearchDispStaff.Text = "Search && Display Staff";
            // 
            // picBoxLogoSearchPnlSearchDispStaff
            // 
            this.picBoxLogoSearchPnlSearchDispStaff.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoSearchPnlSearchDispStaff.Image")));
            this.picBoxLogoSearchPnlSearchDispStaff.Location = new System.Drawing.Point(12, 8);
            this.picBoxLogoSearchPnlSearchDispStaff.Name = "picBoxLogoSearchPnlSearchDispStaff";
            this.picBoxLogoSearchPnlSearchDispStaff.Size = new System.Drawing.Size(93, 77);
            this.picBoxLogoSearchPnlSearchDispStaff.TabIndex = 44;
            this.picBoxLogoSearchPnlSearchDispStaff.TabStop = false;
            // 
            // btnCancelSearchDispStaff
            // 
            this.btnCancelSearchDispStaff.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelSearchDispStaff.Location = new System.Drawing.Point(299, 187);
            this.btnCancelSearchDispStaff.Name = "btnCancelSearchDispStaff";
            this.btnCancelSearchDispStaff.Size = new System.Drawing.Size(75, 23);
            this.btnCancelSearchDispStaff.TabIndex = 7;
            this.btnCancelSearchDispStaff.Text = "Cancel";
            this.btnCancelSearchDispStaff.UseVisualStyleBackColor = true;
            this.btnCancelSearchDispStaff.Click += new System.EventHandler(this.btnCancelSearchDispStaff_Click);
            // 
            // btnSearchDispStaff
            // 
            this.btnSearchDispStaff.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchDispStaff.Location = new System.Drawing.Point(200, 187);
            this.btnSearchDispStaff.Name = "btnSearchDispStaff";
            this.btnSearchDispStaff.Size = new System.Drawing.Size(75, 23);
            this.btnSearchDispStaff.TabIndex = 6;
            this.btnSearchDispStaff.Text = "Search";
            this.btnSearchDispStaff.UseVisualStyleBackColor = true;
            this.btnSearchDispStaff.Click += new System.EventHandler(this.btnSearchDispStaff_Click);
            // 
            // lblSearchandDispInfo
            // 
            this.lblSearchandDispInfo.AutoSize = true;
            this.lblSearchandDispInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchandDispInfo.Location = new System.Drawing.Point(11, 90);
            this.lblSearchandDispInfo.Name = "lblSearchandDispInfo";
            this.lblSearchandDispInfo.Size = new System.Drawing.Size(176, 15);
            this.lblSearchandDispInfo.TabIndex = 5;
            this.lblSearchandDispInfo.Text = "Please choose a mode of search...";
            // 
            // grpBxSearchStaff
            // 
            this.grpBxSearchStaff.Controls.Add(this.comboBoxSearchMode);
            this.grpBxSearchStaff.Controls.Add(this.lblSearchMode);
            this.grpBxSearchStaff.Controls.Add(this.tbxSearchStaffName);
            this.grpBxSearchStaff.Controls.Add(this.lblSearchStaffName);
            this.grpBxSearchStaff.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBxSearchStaff.Location = new System.Drawing.Point(12, 108);
            this.grpBxSearchStaff.Name = "grpBxSearchStaff";
            this.grpBxSearchStaff.Size = new System.Drawing.Size(360, 74);
            this.grpBxSearchStaff.TabIndex = 4;
            this.grpBxSearchStaff.TabStop = false;
            this.grpBxSearchStaff.Text = "Search for Staff";
            // 
            // comboBoxSearchMode
            // 
            this.comboBoxSearchMode.FormattingEnabled = true;
            this.comboBoxSearchMode.Items.AddRange(new object[] {
            "Search by Staff Name",
            "Search by Staff Type"});
            this.comboBoxSearchMode.Location = new System.Drawing.Point(119, 16);
            this.comboBoxSearchMode.Name = "comboBoxSearchMode";
            this.comboBoxSearchMode.Size = new System.Drawing.Size(201, 23);
            this.comboBoxSearchMode.TabIndex = 5;
            // 
            // lblSearchMode
            // 
            this.lblSearchMode.AutoSize = true;
            this.lblSearchMode.Location = new System.Drawing.Point(35, 19);
            this.lblSearchMode.Name = "lblSearchMode";
            this.lblSearchMode.Size = new System.Drawing.Size(78, 15);
            this.lblSearchMode.TabIndex = 4;
            this.lblSearchMode.Text = "Search Mode:";
            // 
            // tbxSearchStaffName
            // 
            this.tbxSearchStaffName.Location = new System.Drawing.Point(119, 42);
            this.tbxSearchStaffName.Name = "tbxSearchStaffName";
            this.tbxSearchStaffName.Size = new System.Drawing.Size(201, 23);
            this.tbxSearchStaffName.TabIndex = 3;
            // 
            // lblSearchStaffName
            // 
            this.lblSearchStaffName.AutoSize = true;
            this.lblSearchStaffName.Location = new System.Drawing.Point(6, 45);
            this.lblSearchStaffName.Name = "lblSearchStaffName";
            this.lblSearchStaffName.Size = new System.Drawing.Size(107, 15);
            this.lblSearchStaffName.TabIndex = 2;
            this.lblSearchStaffName.Text = "Staff Name/NRIC:";
            // 
            // FrmSearchStaff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 214);
            this.Controls.Add(this.pnlSearchStaff);
            this.Controls.Add(this.pnlSearchDispStaff);
            this.Name = "FrmSearchStaff";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search Staff";
            this.pnlSearchStaff.ResumeLayout(false);
            this.pnlSearchStaff.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoSearchPnlSearchtaff)).EndInit();
            this.grpBoxSearchStaff.ResumeLayout(false);
            this.grpBoxSearchStaff.PerformLayout();
            this.pnlSearchDispStaff.ResumeLayout(false);
            this.pnlSearchDispStaff.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoSearchPnlSearchDispStaff)).EndInit();
            this.grpBxSearchStaff.ResumeLayout(false);
            this.grpBxSearchStaff.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearchStaff;
        private System.Windows.Forms.GroupBox grpBoxSearchStaff;
        private System.Windows.Forms.Button btnSearchStaff;
        private System.Windows.Forms.Label lblSearchStaffInfo;
        private System.Windows.Forms.TextBox tbxSearchStaff;
        private System.Windows.Forms.Label lblSearchName;
        private System.Windows.Forms.Button btnSearchStaffCancel;
        private System.Windows.Forms.Panel pnlSearchDispStaff;
        private System.Windows.Forms.Button btnCancelSearchDispStaff;
        private System.Windows.Forms.Button btnSearchDispStaff;
        private System.Windows.Forms.Label lblSearchandDispInfo;
        private System.Windows.Forms.GroupBox grpBxSearchStaff;
        private System.Windows.Forms.ComboBox comboBoxSearchMode;
        private System.Windows.Forms.Label lblSearchMode;
        private System.Windows.Forms.TextBox tbxSearchStaffName;
        private System.Windows.Forms.Label lblSearchStaffName;
        private System.Windows.Forms.Label lblSearchForStaff;
        private System.Windows.Forms.PictureBox picBoxLogoSearchPnlSearchtaff;
        private System.Windows.Forms.Label lblSearchDispStaff;
        private System.Windows.Forms.PictureBox picBoxLogoSearchPnlSearchDispStaff;
    }
}