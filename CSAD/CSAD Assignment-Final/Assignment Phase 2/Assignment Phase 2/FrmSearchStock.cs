﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_2
{
    public partial class FrmSearchStock : Form
    {
        private AllDataClass allData;
        private bool cancel = false;
        //private int i;

        public FrmSearchStock(AllDataClass adc)
        {
            InitializeComponent();
            allData = adc;
            //this.i = i;

            //getting info and storing in comboBox
            for (int b = 0; b < allData.productList.Count; b++)
            {
                if (allData.productList[b] != null)
                {
                    Product p = (Product)allData.productList[b];
                    //string prodDisplay = "Product Name: " + p.Name + "     " + "ModelNo: " + p.ModelNumber;
                    string prodDisplay = p.Name;
                    string modelDisplay = p.ModelNumber;
                    cmboxSupplier_ProductName.Items.Add(prodDisplay);
                    cboxSearchToEdit_ModelNum.Items.Add(modelDisplay);
                }
            }
            for (int a = 0; a<allData.inventoryList.Count;a++)
            {
                if(allData.inventoryList[a]!=null)
                {
                    Supplier s = (Supplier)allData.inventoryList[a];
                    //string compDisplay = "Company Name: " + s.CompanyName;
                    string compDisplay = s.CompanyName;
                    cmboxSearchToEditAdd_CompanyName.Items.Add(compDisplay);
                }
            }
        }

        public bool Cancel
        {
            get
            {
                return cancel;
            }
            set
            {
                cancel = value;
            }
        }

        private void btnSearchStockEdit_Search_Click(object sender, EventArgs e)
        {
            //int indexOfDot = cmboxSupplier_ProductNameNModelNum.SelectedItem.ToString().IndexOf('.');
           //string productName = cmboxSupplier_ProductName.SelectedItem.ToString();
            
            
            if (checkForExistingStock(cmboxSupplier_ProductName.Text.Trim(), cmboxSearchToEditAdd_CompanyName.Text.Trim(),cboxSearchToEdit_ModelNum.Text.Trim()) == true)
            {
                MessageBox.Show("Stock Found, proceed to edit qty.", "Stock Found");
                for (int i = 0; i < allData.stockList.Count; i++)
                {
                    if (allData.stockList[i] != null)
                    {
                        Stock s = (Stock)allData.stockList[i];
                        if (s.Supplier == cmboxSearchToEditAdd_CompanyName.Text && s.ProductName == cmboxSupplier_ProductName.Text && s.ModelNumber == cboxSearchToEdit_ModelNum.Text)
                        {
                            allData.storeTempStock = s;
                        }
                    }
                }
                allData.findSearch = true;
                cancel = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Stock not found! Proceed to add new stock!","Stock Not Found");
                
                allData.searchStock_ProductName = cmboxSupplier_ProductName.Text.Trim();
                allData.searchStock_ModelNum = cboxSearchToEdit_ModelNum.Text.Trim();
                allData.searchStock_CompanyName = cmboxSearchToEditAdd_CompanyName.Text.Trim();
                allData.findSearch = false;
                cancel = true;
                this.Close();
            }
        }

        public bool checkForExistingStock(string ProductName, string CompanyName, string ModelNum)
        {
            
            bool checkStock = false;
            for (int i = 0; i < allData.stockList.Count; i++)
            {
                Stock st = (Stock)allData.stockList[i];
                if (allData.stockList[i] != null)
                {
                    
                    if (st.ProductName == ProductName && st.Supplier == CompanyName && st.ModelNumber == ModelNum)
                    {
                        checkStock = true;
                    }
                }
            }
            return checkStock;
        }

        private void btnSearchStockEdit_Cancel_Click(object sender, EventArgs e)
        {
            cancel = false;
            this.Close();
        }


    }
}
