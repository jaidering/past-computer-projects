﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_2
{
    public partial class FrmSearchStaff : Form
    {
        private AllDataClass allData;
        private bool cancel;
        private string searchType;

        public FrmSearchStaff(AllDataClass adc, string searchType)
        {
            InitializeComponent();
            allData = adc;
            this.searchType = searchType;

            if (searchType == "Search and Display Staff")
            {
                pnlSearchDispStaff.Visible = true;
                pnlSearchStaff.Visible = false;
            }
            else
            {
                pnlSearchDispStaff.Visible = false;
                pnlSearchStaff.Visible = true;
            }
        }

        public bool Cancel
        {
            get
            {
                return cancel;
            }
            set
            {
                cancel = value;
            }
        }

        private void btnSearchStaff_Click(object sender, EventArgs e)
        {
            if (tbxSearchStaff.Text.Trim() != "")
            {
                if (this.searchType == "Register Staff")
                {
                    if (SearchForExistingStaff(tbxSearchStaff.Text.Trim()) == true)
                    {
                        allData.foundSearchEntry = true;
                        MessageBox.Show("Unable to add staff information. Staff already exist!", "Error!");
                    }
                    else
                    {
                        allData.foundSearchEntry = false;
                        MessageBox.Show("Staff information is not found. Proceed to Register Staff.", "Register Staff");
                        allData.searchStaffName = tbxSearchStaff.Text.Trim();
                        cancel = true;
                        this.Close();
                    }
                }

                if (this.searchType == "Modify Staff")
                {
                    if (SearchForExistingStaff(tbxSearchStaff.Text.Trim()) == true)
                    {
                        allData.foundSearchEntry = true;
                        MessageBox.Show("Existing staff information is found. Proceed to modify staff information.", "Modify Staff Information");
                        cancel = true;
                        this.Close();
                    }
                    else
                    {
                        allData.foundSearchEntry = false;
                        MessageBox.Show("Existing staff information is not found to modify.", "Error!");
                    }
                }

                if (this.searchType == "Delete Staff")
                {
                    if (SearchForExistingStaff(tbxSearchStaff.Text.Trim()) == true)
                    {
                        allData.foundSearchEntry = true;
                        MessageBox.Show("Existing staff information is found. Proceed to delete staff information.", "Delete Staff Information");
                        cancel = true;
                        this.Close();
                    }
                    else
                    {
                        allData.foundSearchEntry = false;
                        MessageBox.Show("Existing staff information is not found to delete.", "Error!");
                    }
                }
                if (this.searchType == "Reset Password")
                {
                    if (SearchForExistingStaff(tbxSearchStaff.Text.Trim()) == true)
                    {
                        allData.foundSearchEntry = true;
                        MessageBox.Show("Existing staff information is found. Proceed to reset staff password.", "Reset Staff password");
                        cancel = true;
                        this.Close();
                    }
                    else
                    {
                        allData.foundSearchEntry = false;
                        MessageBox.Show("Existing staff is not found to reset password.", "Error!");
                    }
                }
            }
            else
            {
                MessageBox.Show("Please enter a staff name to search.", "Error!");
            }

        }

        private void btnSearchStaffCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            cancel = false;
        }

        public bool SearchForExistingStaff(string name)
        {
            bool searchIfExist = false;
            for (int i = 0; i < allData.staffList.Count; i++)
            {
                if (allData.staffList[i] != null)
                {
                    Staff staffInArray = (Staff)allData.staffList[i];
                    if (staffInArray.Name == name)
                    {
                        searchIfExist = true;
                        allData.storeTempStaff = (Staff)allData.staffList[i];
                        break;
                    }
                    else
                    {
                        searchIfExist = false;
                    }
                }
            }
            return searchIfExist;
        }

        private void btnSearchDispStaff_Click(object sender, EventArgs e)
        {
            if (tbxSearchStaffName.Text != "")
            {
                if (comboBoxSearchMode.Text == "Search by Staff Name")
                {
                    if (SearchForExistingStaff(tbxSearchStaffName.Text.Trim()) == true)
                    {
                        allData.foundSearchEntry = true;
                        MessageBox.Show("Existing staff information is found. Proceed to view staff information.", "View Staff Information");
                        cancel = true;
                        this.Close();
                    }
                    else
                    {
                        allData.foundSearchEntry = false;
                        MessageBox.Show("Existing staff information is not found to display.", "Error!");
                    }

                }

                if (comboBoxSearchMode.Text == "Search by Staff NRIC")
                {
                    if (SearchStaffByNRIC(tbxSearchStaffName.Text.Trim()) == true)
                    {
                        allData.foundSearchEntry = true;
                        MessageBox.Show("Existing staff information is found. Proceed to view staff information.", "View Staff Information");
                        cancel = true;
                        this.Close();
                    }
                    else
                    {
                        allData.foundSearchEntry = false;
                        MessageBox.Show("Existing staff information is not found to display.", "Error!");
                    }
                }
            }
            else
            {
                MessageBox.Show("Please enter a search term to search for existing staff information", "Error!");
            }
        }

        public bool SearchStaffByNRIC(string nric)
        {
            bool searchIfExist = false;
            for (int i = 0; i < allData.staffList.Count; i++)
            {
                if (allData.staffList[i] != null)
                {
                    Staff staffInArray = (Staff)allData.staffList[i];
                    if (staffInArray.Nric == nric)
                    {
                        searchIfExist = true;
                        allData.storeTempStaff = (Staff)allData.staffList[i];
                        break;
                    }
                    else
                    {
                        searchIfExist = false;
                    }
                }
            }
            return searchIfExist;
        }

        private void btnCancelSearchDispStaff_Click(object sender, EventArgs e)
        {
            this.Close();
            cancel = false;
        }
    }
}
