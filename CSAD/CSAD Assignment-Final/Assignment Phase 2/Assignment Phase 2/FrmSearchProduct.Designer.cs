﻿namespace Assignment_Phase_2
{
    partial class FrmSearchProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearchProduct));
            this.pnlSearchProduct = new System.Windows.Forms.Panel();
            this.btnCancelSearchProduct = new System.Windows.Forms.Button();
            this.btnSearchProduct = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbxSearchProductModelNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbxSearchProductName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.picBoxLogoSearchPnlSearchtaff = new System.Windows.Forms.PictureBox();
            this.pnlSearchProduct.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoSearchPnlSearchtaff)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSearchProduct
            // 
            this.pnlSearchProduct.Controls.Add(this.btnCancelSearchProduct);
            this.pnlSearchProduct.Controls.Add(this.btnSearchProduct);
            this.pnlSearchProduct.Controls.Add(this.label2);
            this.pnlSearchProduct.Controls.Add(this.groupBox1);
            this.pnlSearchProduct.Controls.Add(this.label1);
            this.pnlSearchProduct.Controls.Add(this.picBoxLogoSearchPnlSearchtaff);
            this.pnlSearchProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchProduct.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchProduct.Name = "pnlSearchProduct";
            this.pnlSearchProduct.Size = new System.Drawing.Size(384, 214);
            this.pnlSearchProduct.TabIndex = 0;
            // 
            // btnCancelSearchProduct
            // 
            this.btnCancelSearchProduct.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelSearchProduct.Location = new System.Drawing.Point(297, 184);
            this.btnCancelSearchProduct.Name = "btnCancelSearchProduct";
            this.btnCancelSearchProduct.Size = new System.Drawing.Size(75, 23);
            this.btnCancelSearchProduct.TabIndex = 17;
            this.btnCancelSearchProduct.Text = "Cancel";
            this.btnCancelSearchProduct.UseVisualStyleBackColor = true;
            this.btnCancelSearchProduct.Click += new System.EventHandler(this.btnCancelSearchProduct_Click);
            // 
            // btnSearchProduct
            // 
            this.btnSearchProduct.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchProduct.Location = new System.Drawing.Point(202, 184);
            this.btnSearchProduct.Name = "btnSearchProduct";
            this.btnSearchProduct.Size = new System.Drawing.Size(75, 23);
            this.btnSearchProduct.TabIndex = 16;
            this.btnSearchProduct.Text = "Search";
            this.btnSearchProduct.UseVisualStyleBackColor = true;
            this.btnSearchProduct.Click += new System.EventHandler(this.btnSearchProduct_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(301, 15);
            this.label2.TabIndex = 15;
            this.label2.Text = "Please enter Product\'s Name && Model Number to search ...";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbxSearchProductModelNumber);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbxSearchProductName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 107);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(360, 71);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search for Product";
            // 
            // tbxSearchProductModelNumber
            // 
            this.tbxSearchProductModelNumber.Location = new System.Drawing.Point(145, 41);
            this.tbxSearchProductModelNumber.Name = "tbxSearchProductModelNumber";
            this.tbxSearchProductModelNumber.Size = new System.Drawing.Size(165, 23);
            this.tbxSearchProductModelNumber.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(45, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "Model Number:";
            // 
            // tbxSearchProductName
            // 
            this.tbxSearchProductName.Location = new System.Drawing.Point(145, 16);
            this.tbxSearchProductName.Name = "tbxSearchProductName";
            this.tbxSearchProductName.Size = new System.Drawing.Size(165, 23);
            this.tbxSearchProductName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(45, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Product Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(154, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 23);
            this.label1.TabIndex = 13;
            this.label1.Text = "Search for Product";
            // 
            // picBoxLogoSearchPnlSearchtaff
            // 
            this.picBoxLogoSearchPnlSearchtaff.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoSearchPnlSearchtaff.Image")));
            this.picBoxLogoSearchPnlSearchtaff.Location = new System.Drawing.Point(12, 7);
            this.picBoxLogoSearchPnlSearchtaff.Name = "picBoxLogoSearchPnlSearchtaff";
            this.picBoxLogoSearchPnlSearchtaff.Size = new System.Drawing.Size(93, 77);
            this.picBoxLogoSearchPnlSearchtaff.TabIndex = 45;
            this.picBoxLogoSearchPnlSearchtaff.TabStop = false;
            // 
            // FrmSearchProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 214);
            this.Controls.Add(this.pnlSearchProduct);
            this.Name = "FrmSearchProduct";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search Product";
            this.pnlSearchProduct.ResumeLayout(false);
            this.pnlSearchProduct.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoSearchPnlSearchtaff)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearchProduct;
        private System.Windows.Forms.Button btnCancelSearchProduct;
        private System.Windows.Forms.Button btnSearchProduct;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbxSearchProductModelNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxSearchProductName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picBoxLogoSearchPnlSearchtaff;
    }
}