﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Assignment_Phase_2
{
    public partial class FrmMainWindowStaff : Form
    {
        private AllDataClass allData;
        private int selectedIndex;

        public FrmMainWindowStaff(AllDataClass adc)
        {
            InitializeComponent();
            allData = adc;
            //Staff
            registerNewStaffToolStripMenuItem.Visible = false;
            modifyToolStripMenuItem.Visible = false;
            deleteExistingStaffToolStripMenuItem.Visible = false;
            resetStaffPasswordToolStripMenuItem.Visible = false;
            //Category
            addNewCategoryInformationToolStripMenuItem.Visible = false;
            modifyCategoryInformationToolStripMenuItem.Visible = false;
            deleteCategoryInformationToolStripMenuItem.Visible = false;
            //Product
            addNewProductInformationToolStripMenuItem.Visible = false;
            modifyProductInformationToolStripMenuItem.Visible = false;
            deleteExistingProductInformationToolStripMenuItem.Visible = false;
            //Order
            addNewOrderInformationToolStripMenuItem.Visible = false;
            modifyExistingOrderInformationToolStripMenuItem.Visible = false;
            delToolStripMenuItem.Visible = false;
            //Inventory
            aToolStripMenuItem.Visible = false;
            modifyExistingSupplierInformationToolStripMenuItem.Visible = false;
            deleteExistingSupplierInformationToolStripMenuItem.Visible = false;
            //Stock
            editProductQuantityToolStripMenuItem.Visible = false;
            computeExcessShortfallToolStripMenuItem.Visible = false;


            if (allData.defaultAdmin == true)
            {
                allData.defaultAdmin = false;
                registerNewStaffToolStripMenuItem.Visible = true;
                modifyToolStripMenuItem.Visible = true;
                deleteExistingStaffToolStripMenuItem.Visible = true;
                resetStaffPasswordToolStripMenuItem.Visible = true;

                addNewCategoryInformationToolStripMenuItem.Visible = true;
                modifyCategoryInformationToolStripMenuItem.Visible = true;
                deleteCategoryInformationToolStripMenuItem.Visible = true;

                addNewProductInformationToolStripMenuItem.Visible = true;
                modifyProductInformationToolStripMenuItem.Visible = true;
                deleteExistingProductInformationToolStripMenuItem.Visible = true;

                aToolStripMenuItem.Visible = true;
                modifyExistingSupplierInformationToolStripMenuItem.Visible = true;
                deleteExistingSupplierInformationToolStripMenuItem.Visible = true;

                addNewOrderInformationToolStripMenuItem.Visible = true;
                modifyExistingOrderInformationToolStripMenuItem.Visible = true;
                delToolStripMenuItem.Visible = true;
                editProductQuantityToolStripMenuItem.Visible = true;
                computeExcessShortfallToolStripMenuItem.Visible = true;
            }
            else
            {
                for (int i = 0; i < allData.roleOfStaffLogIn.Count; i++)
                {
                    if (allData.roleOfStaffLogIn[i] != null)
                    {
                        if ((string)allData.roleOfStaffLogIn[i] == "Administrator")
                        {
                            registerNewStaffToolStripMenuItem.Visible = true;
                            modifyToolStripMenuItem.Visible = true;
                            deleteExistingStaffToolStripMenuItem.Visible = true;
                            searchForStaffInformationToolStripMenuItem.Visible = true;
                            resetStaffPasswordToolStripMenuItem.Visible = true;
                        }
                        else if ((string)allData.roleOfStaffLogIn[i] == "Catalogue Manager")
                        {
                            addNewCategoryInformationToolStripMenuItem.Visible = true;
                            modifyCategoryInformationToolStripMenuItem.Visible = true;
                            deleteCategoryInformationToolStripMenuItem.Visible = true;

                            addNewProductInformationToolStripMenuItem.Visible = true;
                            modifyProductInformationToolStripMenuItem.Visible = true;
                            deleteExistingProductInformationToolStripMenuItem.Visible = true;
                        }
                        else if ((string)allData.roleOfStaffLogIn[i] == "Inventory Manager")
                        {
                            aToolStripMenuItem.Visible = true;
                            modifyExistingSupplierInformationToolStripMenuItem.Visible = true;
                            deleteExistingSupplierInformationToolStripMenuItem.Visible = true;


                        }
                        else if ((string)allData.roleOfStaffLogIn[i] == "Order Manager")
                        {
                            addNewOrderInformationToolStripMenuItem.Visible = true;
                            modifyExistingOrderInformationToolStripMenuItem.Visible = true;
                            delToolStripMenuItem.Visible = true;
                            editProductQuantityToolStripMenuItem.Visible = true;
                            computeExcessShortfallToolStripMenuItem.Visible = true;
                        }

                    }
                }
                allData.roleOfStaffLogIn.Clear();
            }
        }
       //====================================================================
       /* ********   *******    **       **  *       *  **        *  *******
          *          *      *   * *     * *  *       *  * *       *  *      *
          *          *       *  *  *   *  *  *       *  *  *      *  *       * 
          *          *       *  *   * *   *  *       *  *   *     *  *       *
          ********   *       *  *    *    *  *       *  *    *    *  *       *
          *          *       *  *         *  *       *  *     *   *  *       *
          *          *       *  *         *  *       *  *      *  *  *       *
          *          *      *   *         *   *     *   *       * *  *      *
          ********   *******    *         *    *****    *        **  *******
        
        */
        //===================================================================
        
        public void ShowRegisterStaffPnl()
        {
            //if method returns false, pnlRegisterStaff.BringToFront();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            allData.defaultAdmin = false;
            allData.roleOfStaffLogIn.Clear();
            allData.frmlogin.Show();
            this.Hide();
        }

        public string staffGender()
        {
            string gender = "";
            if (rdBtnMaleRegister.Checked == true)
            {
                gender = "Male";
            }
            else if (rdBtnFemaleRegister.Checked == true)
            {
                gender = "Female";
            }
            return gender;
        }
        
        private void registerNewStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
             FrmSearchStaff frmSearchStaff = new FrmSearchStaff(allData, "Register Staff");
             frmSearchStaff.ShowDialog();

             if (frmSearchStaff.Cancel == false)
             {
                 return;
             }

             if (allData.foundSearchEntry == false)
             {
                 this.Text = "Register New Staff";
                 //Edmund
                 pnlWelcomeScreen.Visible = false;
                 pnlRegisterStaff.Visible = true;//True
                 pnlModifyStaffInfo.Visible = false;
                 pnlDeleteStaffInfo.Visible = false;
                 pnlDisplayStaffInfo.Visible = false;
                 pnlResetPassword.Visible = false;
                 pnlAddOrderInformation.Visible = false;
                 pnlModifyOrderInfo.Visible = false;
                 pnlDeleteOrderInfo.Visible = false;
                 pnlSearchDispOrderInfo.Visible = false;
                 //Weiquan
                 panelAddNewCategory.Visible = false;
                 panelModifyCategory.Visible = false;
                 panelDeleteCategory.Visible = false;
                 panelDisplayCategory.Visible = false;
                 panelAddNewProduct.Visible = false;
                 panelModifyProduct.Visible = false;
                 panelDeleteProduct.Visible = false;
                 panelDisplayProduct.Visible = false;
                 panelSelectAProductToDisplay.Visible = false;
                 //Jai
                 pnlAddNewSupplierInfo.Visible = false;
                 pnlModifySupplierInfo.Visible = false;
                 pnlDeleteSupplierInfo.Visible = false;
                 pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                 pnlDisplaySupplierInformation_Details.Visible = false;
                 pnlAddStock.Visible = false;
                 pnlEditStockInfo.Visible = false;
                 pnlDisplayComputeExcessSHrtfall.Visible = false;
                 pnlComputeExcessShortFall.Visible = false;
                 pnlSearchAndDisplayStock.Visible = false;

                 lblRegisterStaffName.Text = allData.searchStaffName;
             }
        }

        private void btnRegisterStaff_Click(object sender, EventArgs e)
        {
            int count = 0;

            DialogResult dr = MessageBox.Show("Do you want to continue registering this Staff?", "Register New Staff", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
            if (chkBoxAdministrator.Checked == true)
            {
                allData.storeTempStaffRole.Add("Administrator");
                count++;
            }
            if (chkBoxCatalogueManager.Checked == true)
            {
                allData.storeTempStaffRole.Add("Catalogue Manager");
                count++;
            }
            if (chkBoxInventoryManager.Checked == true)
            {
                allData.storeTempStaffRole.Add("Inventory Manager");
                count++;
            }
            if (chkBoxOrderManager.Checked == true)
            {
                allData.storeTempStaffRole.Add("Order Manager");
                count++;
            }


                if (tbxRegisterPassword.Text == tbxConfirmRegisterPassword.Text)
                {
                    if (tbxRegisterNRIC.Text != "" && tbxRegisterHomeNo.Text != "" && tbxRegisterHpNoContact.Text != "" && tbxRegisterAddress.Text != "" &&
                        tbxConfirmRegisterPassword.Text != "" && tbxRegisterPassword.Text != "")
                    {
                        if (count == 1 || count == 2)
                        {
                            string password = "";
                            password = tbxConfirmRegisterPassword.Text;

                            Staff registerStaff = new Staff(lblRegisterStaffName.Text, tbxRegisterNRIC.Text, dtpRegisterDateOfBirth.Value.ToShortDateString(),
                                staffGender(), Convert.ToInt32(tbxRegisterHomeNo.Text), Convert.ToInt32(tbxRegisterHpNoContact.Text), tbxRegisterAddress.Text,
                                password);
                            for (int i = 0; i < allData.storeTempStaffRole.Count; i++)
                            {
                                if (i == 0)
                                {
                                    registerStaff.Role1 = (string)allData.storeTempStaffRole[i];
                                }
                                else if (i == 1)
                                {
                                    registerStaff.Role2 = (string)allData.storeTempStaffRole[i];

                                }
                            }

                            allData.staffList.Add(registerStaff);
                            MessageBox.Show("Staff " + registerStaff.Name + " has beeen registered.", "Register Staff Information");

                            //clear fields
                            lblRegisterStaffName.Text = "";
                            tbxRegisterNRIC.Text = "";
                            rdBtnMaleRegister.Checked = false;
                            rdBtnFemaleRegister.Checked = false;
                            tbxRegisterHomeNo.Text = "";
                            tbxRegisterHpNoContact.Text = "";
                            tbxRegisterAddress.Text = "";
                            tbxConfirmRegisterPassword.Text = "";
                            tbxRegisterPassword.Text = "";
                            chkBoxAdministrator.Checked = false;
                            chkBoxCatalogueManager.Checked = false;
                            chkBoxInventoryManager.Checked = false;
                            chkBoxOrderManager.Checked = false;

                            //Edmund
                            pnlWelcomeScreen.Visible = true;//True
                            pnlRegisterStaff.Visible = false;
                            pnlModifyStaffInfo.Visible = false;
                            pnlDeleteStaffInfo.Visible = false;
                            pnlDisplayStaffInfo.Visible = false;
                            pnlResetPassword.Visible = false;
                            pnlAddOrderInformation.Visible = false;
                            pnlModifyOrderInfo.Visible = false;
                            pnlDeleteOrderInfo.Visible = false;
                            pnlSearchDispOrderInfo.Visible = false;
                            //Weiquan
                            panelAddNewCategory.Visible = false;
                            panelModifyCategory.Visible = false;
                            panelDeleteCategory.Visible = false;
                            panelDisplayCategory.Visible = false;
                            panelAddNewProduct.Visible = false;
                            panelModifyProduct.Visible = false;
                            panelDeleteProduct.Visible = false;
                            panelDisplayProduct.Visible = false;
                            panelSelectAProductToDisplay.Visible = false;
                            //Jai
                            pnlAddNewSupplierInfo.Visible = false;
                            pnlModifySupplierInfo.Visible = false;
                            pnlDeleteSupplierInfo.Visible = false;
                            pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                            pnlDisplaySupplierInformation_Details.Visible = false;
                            pnlAddStock.Visible = false;
                            pnlEditStockInfo.Visible = false;
                            pnlDisplayComputeExcessSHrtfall.Visible = false;
                            pnlComputeExcessShortFall.Visible = false;
                            pnlSearchAndDisplayStock.Visible = false;

                            tabCtrRegisterStaffPnl.SelectTab(tabStaffPersonalInformation);
                            allData.storeTempStaffRole.Clear();
                        }
                        else
                        {
                            allData.storeTempStaffRole.Clear();
                            MessageBox.Show("A staff can only have a minimum of one role and a maximum of two roles.", "Error!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("All fields are mandatory!", "Error!");
                    }
                }
                else
                {
                    MessageBox.Show("Passwords do not match!. Please retry.", "Error!");
                }
            }
        }


        private void btnCancelRegisterStaff_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Do you want to cancel registration of staff?", "Cancel Register New Staff", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                this.Text = "Welcome!";
                //Edmund
                pnlWelcomeScreen.Visible = true;//True
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }
        }

        private void modifyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchStaff frmSearchStaff = new FrmSearchStaff(allData, "Modify Staff");
            frmSearchStaff.ShowDialog();

            if (frmSearchStaff.Cancel == false)
            {
                return;
            }

            if (allData.foundSearchEntry == true)
            {
                this.Text = "Modify Existing Staff Information";
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = true;//true
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                lblModifyStaffName.Text = allData.storeTempStaff.Name;
                lblModifyStaffNRIC.Text = allData.storeTempStaff.Nric;
                lblModifyStaffDOB.Text = Convert.ToString(allData.storeTempStaff.DateOfBirth);
                lblModifyStaffGender.Text = allData.storeTempStaff.Gender;
                if (allData.storeTempStaff.Role1 == "Administrator" || allData.storeTempStaff.Role2 == "Administrator")
                {
                    chkBoxModifyAdministrator.Checked = true;
                }
                if (allData.storeTempStaff.Role1 == "Catalogue Manager" || allData.storeTempStaff.Role2 == "Catalogue Manager")
                {
                    chkBoxModifyCatalogueManager.Checked = true;
                }
                if (allData.storeTempStaff.Role1 == "Order Manager" || allData.storeTempStaff.Role2 == "Order Manager")
                {
                    chkBoxModifyOrderManager.Checked = true;
                }
                if (allData.storeTempStaff.Role1 == "Inventory Manager" || allData.storeTempStaff.Role2 == "Inventory Manager")
                {
                    chkBoxModifyInventoryManager.Checked = true;
                }
            }
        }

        private void btnModifyStaffInformation_Click(object sender, EventArgs e)
        {
                DialogResult dr = MessageBox.Show("Do you want to modify staff information?", "Modify Staff Information", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    int count = 0;
                    allData.storeTempStaffRole.Clear();
                    if (chkBoxModifyAdministrator.Checked == true)
                    {
                        allData.storeTempStaffRole.Add("Administrator");
                        count++;
                    }
                    if (chkBoxModifyCatalogueManager.Checked == true)
                    {
                        allData.storeTempStaffRole.Add("Catalogue Manager");
                        count++;
                    }
                    if (chkBoxModifyOrderManager.Checked == true)
                    {
                        allData.storeTempStaffRole.Add("Inventory Manager");
                        count++;
                    }
                    if (chkBoxModifyInventoryManager.Checked == true)
                    {
                        allData.storeTempStaffRole.Add("Order Manager");
                        count++;
                    }

                    if (count == 1 || count == 2)
                    {
                        for (int i = 0; i < allData.staffList.Count; i++)
                        {
                            if (allData.staffList[i] != null)
                            {
                                Staff staffInArray = (Staff)allData.staffList[i];

                                if (lblModifyStaffName.Text == allData.storeTempStaff.Name)
                                {

                                    Staff s = allData.storeTempStaff;
                                    s.HomeTel = Convert.ToInt32(tbxModifyHomeNo.Text.Trim());
                                    s.MobileTel = Convert.ToInt32(tbxModifyHpNoContact.Text.Trim());
                                    s.Address = tbxModifyDestinationAddress.Text.Trim();
                                    for (int j = 0; j < allData.storeTempStaffRole.Count; j++)
                                    {
                                        if (j == 0)
                                        {
                                            MessageBox.Show((string)allData.storeTempStaffRole[0]);
                                            s.Role1 = (string)allData.storeTempStaffRole[j];
                                        }
                                        else if (j == 1)
                                        {
                                            MessageBox.Show((string)allData.storeTempStaffRole[1]);
                                            s.Role2 = (string)allData.storeTempStaffRole[j];

                                        }
                                    }

                                    allData.staffList.RemoveAt(i);
                                    allData.staffList.Add(s);
                                    allData.storeTempStaffRole.Clear();
                                }
                            }
                        }
                        this.Text = "Welcome!";
                        //Edmund
                        pnlWelcomeScreen.Visible = true;//true
                        pnlRegisterStaff.Visible = false;
                        pnlModifyStaffInfo.Visible = false;
                        pnlDeleteStaffInfo.Visible = false;
                        pnlDisplayStaffInfo.Visible = false;
                        pnlResetPassword.Visible = false;
                        pnlAddOrderInformation.Visible = false;
                        pnlModifyOrderInfo.Visible = false;
                        pnlDeleteOrderInfo.Visible = false;
                        pnlSearchDispOrderInfo.Visible = false;
                        //Weiquan
                        panelAddNewCategory.Visible = false;
                        panelModifyCategory.Visible = false;
                        panelDeleteCategory.Visible = false;
                        panelDisplayCategory.Visible = false;
                        panelAddNewProduct.Visible = false;
                        panelModifyProduct.Visible = false;
                        panelDeleteProduct.Visible = false;
                        panelDisplayProduct.Visible = false;
                        panelSelectAProductToDisplay.Visible = false;
                        //Jai
                        pnlAddNewSupplierInfo.Visible = false;
                        pnlModifySupplierInfo.Visible = false;
                        pnlDeleteSupplierInfo.Visible = false;
                        pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                        pnlDisplaySupplierInformation_Details.Visible = false;
                        pnlAddStock.Visible = false;
                        pnlEditStockInfo.Visible = false;
                        pnlDisplayComputeExcessSHrtfall.Visible = false;
                        pnlComputeExcessShortFall.Visible = false;
                        pnlSearchAndDisplayStock.Visible = false;

                        tabControl3.SelectTab(tabPage5);
                    }
                }
                else
                {
                    allData.storeTempStaffRole.Clear();
                    MessageBox.Show("A staff can only have a minimum of one role and a maximum of two roles.", "Error!");
                }
        }

        private void btnModifyStaffCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Do you want to cancel modification of staff information?", "Modify Staff Information", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                this.Text = "Welcome!";
                //Edmund
                pnlWelcomeScreen.Visible = true;//true
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }
        }

        private void deleteExistingStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchStaff frmSearchStaff = new FrmSearchStaff(allData, "Delete Staff");
            frmSearchStaff.ShowDialog();

            if (frmSearchStaff.Cancel == false)
            {
                return;
            }

            if (allData.foundSearchEntry == true)
            {
                this.Text = "Delete Existing Staff Information";
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = true;//true
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                //panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                lblDispDelStaffName.Text = allData.storeTempStaff.Name;
                lblDispDelStaffNRIC.Text = allData.storeTempStaff.Nric;
                lblDispDelStaffDOB.Text = allData.storeTempStaff.DateOfBirth;
                lblDispDelStaffGender.Text = allData.storeTempStaff.Gender;
                lblDispDelStaffMobile.Text = Convert.ToString(allData.storeTempStaff.MobileTel);
                lblDispDelStaffCintact.Text = Convert.ToString(allData.storeTempStaff.HomeTel);
                lblDispDelStaffAddress.Text = allData.storeTempStaff.Address;
                lblDispDelStaffRole.Text = "";

                bool stringContainText = false;
                for (int i = 0; i < allData.storeTempStaffRole.Count; i++)
                {

                    if (allData.storeTempStaffRole[i] != null && stringContainText == false)
                    {
                        lblDispDelStaffRole.Text = lblDispDelStaffRole.Text + Convert.ToString(allData.storeTempStaffRole[i]);
                        stringContainText = true;
                    }
                    else
                    {
                        lblDispDelStaffRole.Text = lblDispDelStaffRole.Text + ", " + Convert.ToString(allData.storeTempStaffRole[i]);
                    }
                }

                lblDispDelStaffRole.Text = allData.storeTempStaff.Role1;
                if (allData.storeTempStaff.Role2 != null)
                {
                    lblDispDelStaffRole.Text = lblDispDelStaffRole.Text + ", " + allData.storeTempStaff.Role2;
                }
            }
        }


          
        //===========================================
         /*  *      *  *  *    *     *     ***     *
             *      *  *  *   *     * *    *  *    *
             *      *  *  *  *     *   *   *   *   *
             *      *  *  * *     *     *  *   *   *
             ********  *  **      *     *  ****    *
             *      *  *  * *     *******  * *     *
             *      *  *  *  *    *     *  *  *    *
             *      *  *  *   *   *     *  *   *   *
             *      *  *  *    *  *     *  *    *  *
         */

         //==========================================

        //Add Category
        private void addNewCategoryInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchCategory f = new FrmSearchCategory(allData,"Add Category");
            allData.searchCategoryName = "";
            f.ShowDialog();

            
            if (f.Cancel == false)
            {
                return;
            }

            if (allData.foundSearch == false)
            {
               
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = true;//True
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                lblAddCategoryName.Text = allData.searchCategoryName;
                this.Text = "Add Category";

            }
            

        }

        private void btnAddCategory_Click(object sender, EventArgs e)
        {

            if (tbxAddCategoryDescription.Text.Trim() != "")
            {
                DialogResult dr = MessageBox.Show("Continue to add category?", "Add New Category", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    Category addCategory = new Category(lblAddCategoryName.Text, tbxAddCategoryDescription.Text.Trim());
                    allData.categoryList.Add(addCategory);
                    MessageBox.Show("Category has been added sucessfully!","Add Sucessful");

                    allData.searchCategoryName = "";
                    tbxAddCategoryDescription.Text = "";

                    //Edmund
                    pnlWelcomeScreen.Visible = true;//True
                    pnlRegisterStaff.Visible = false;
                    pnlModifyStaffInfo.Visible = false;
                    pnlDeleteStaffInfo.Visible = false;
                    pnlDisplayStaffInfo.Visible = false;
                    pnlResetPassword.Visible = false;
                    pnlAddOrderInformation.Visible = false;
                    pnlModifyOrderInfo.Visible = false;
                    pnlDeleteOrderInfo.Visible = false;
                    pnlSearchDispOrderInfo.Visible = false;
                    //Weiquan
                    panelAddNewCategory.Visible = false;
                    panelModifyCategory.Visible = false;
                    panelDeleteCategory.Visible = false;
                    panelDisplayCategory.Visible = false;
                    panelAddNewProduct.Visible = false;
                    panelModifyProduct.Visible = false;
                    panelDeleteProduct.Visible = false;
                    panelDisplayProduct.Visible = false;
                    panelSelectAProductToDisplay.Visible = false;
                    //Jai
                    pnlAddNewSupplierInfo.Visible = false;
                    pnlModifySupplierInfo.Visible = false;
                    pnlDeleteSupplierInfo.Visible = false;
                    pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                    pnlDisplaySupplierInformation_Details.Visible = false;
                    pnlAddStock.Visible = false;
                    pnlEditStockInfo.Visible = false;
                    pnlDisplayComputeExcessSHrtfall.Visible = false;
                    pnlComputeExcessShortFall.Visible = false;
                    pnlSearchAndDisplayStock.Visible = false;

                    this.Text = "Welcome!";
                }
            }
            else
            {
                MessageBox.Show("Please fill in the Category Dsecrption","Error In Adding");
            }

        }

        private void btnCancelAddCategory_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel adding?","Cancel Adding", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                tbxAddCategoryDescription.Text = "";
                //Edmund
                pnlWelcomeScreen.Visible = true;//True
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Welcome!";
            }
        }
        //Modify Category
        private void modifyCategoryInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchCategory f = new FrmSearchCategory(allData, "Modify Category");
            allData.searchCategoryName = "";
            f.ShowDialog();

            if (f.Cancel == false)
            {
                return;
            }

            if (allData.foundSearch == true)
            {
                
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = true;//True
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Modify Existing Category";

                lblModifyCategoryName.Text = allData.searchCategoryName;
                tbxModifyCategoryDescrption.Text = allData.storeTempCategory.Descrption;
            }
        }
        
        private void btnModifyCategory_Click(object sender, EventArgs e)
        {
            if (tbxModifyCategoryDescrption.Text != "")
            {
                DialogResult dr = MessageBox.Show("Continue to update category?", "Modify Existing Category", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    for (int i = 0; i < allData.categoryList.Count; i++)
                    {
                        if (allData.categoryList[i] != null)
                        {
                            Category c = (Category)allData.categoryList[i];
                            if (c.Name == lblModifyCategoryName.Text)
                            {
                                Category modifyCategory = new Category(lblModifyCategoryName.Text, tbxModifyCategoryDescrption.Text.Trim());
                                allData.categoryList.RemoveAt(i);
                                allData.categoryList.Add(modifyCategory);
                                break;
                            }
                        }
                    }
                    MessageBox.Show("Category has been updated sucessfully!","Update Sucessful");
                    tbxAddCategoryDescription.Text = "";
                    //Edmund
                    pnlWelcomeScreen.Visible = true;//True
                    pnlRegisterStaff.Visible = false;
                    pnlModifyStaffInfo.Visible = false;
                    pnlDeleteStaffInfo.Visible = false;
                    pnlDisplayStaffInfo.Visible = false;
                    pnlResetPassword.Visible = false;
                    pnlAddOrderInformation.Visible = false;
                    pnlModifyOrderInfo.Visible = false;
                    pnlDeleteOrderInfo.Visible = false;
                    pnlSearchDispOrderInfo.Visible = false;
                    //Weiquan
                    panelAddNewCategory.Visible = false;
                    panelModifyCategory.Visible = false;
                    panelDeleteCategory.Visible = false;
                    panelDisplayCategory.Visible = false;
                    panelAddNewProduct.Visible = false;
                    panelModifyProduct.Visible = false;
                    panelDeleteProduct.Visible = false;
                    panelDisplayProduct.Visible = false;
                    panelSelectAProductToDisplay.Visible = false;
                    //Jai
                    pnlAddNewSupplierInfo.Visible = false;
                    pnlModifySupplierInfo.Visible = false;
                    pnlDeleteSupplierInfo.Visible = false;
                    pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                    pnlDisplaySupplierInformation_Details.Visible = false;
                    pnlAddStock.Visible = false;
                    pnlEditStockInfo.Visible = false;
                    pnlDisplayComputeExcessSHrtfall.Visible = false;
                    pnlComputeExcessShortFall.Visible = false;
                    pnlSearchAndDisplayStock.Visible = false;
                    tbxModifyCategoryDescrption.Text = "";

                    this.Text = "Welcome!";
                }
            }
            else
            {
                MessageBox.Show("Please fill in the Category Description ", "Error In Modifying");
            }
        }

        private void btnCancelModifyCategory_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel modifying?", "Cancel Modifying", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                tbxModifyCategoryDescrption.Text = "";
                //Edmund
                pnlWelcomeScreen.Visible = true;//True
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Welcome!";
            }
        }
        //Delete Category
        private void deleteCategoryInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchCategory f = new FrmSearchCategory(allData, "Delete Category");
            allData.searchCategoryName = "";
            f.ShowDialog();

            if (f.Cancel == false)
            {
                return;
            }

            if (allData.foundSearch == true)
            {
                
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = true;//True
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
                lblDeleteCategoryName.Text = allData.searchCategoryName;
                tbxDeleteCategoryDescrption.Text = allData.storeTempCategory.Descrption;

                this.Text = "Delete Existing Category";
            }
        }

        private void btnDeleteCategory_Click(object sender, EventArgs e)
        {
            if (tbxDeleteCategoryDescrption.Text != "")
            {
                DialogResult dr = MessageBox.Show("Continue to delete delete category?", "Delete Existing Category", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    for (int i = 0; i < allData.categoryList.Count; i++)
                    {
                        if (allData.categoryList[i] != null)
                        {
                            Category c = (Category)allData.categoryList[i];
                            if (c.Name == lblDeleteCategoryName.Text)
                            {

                                allData.categoryList.RemoveAt(i);
                                MessageBox.Show("Category has been deleted scuessfully!","Delete Sucessful");
                                break;
                            }
                        }
                    }

                    //Edmund
                    pnlWelcomeScreen.Visible = true;//True
                    pnlRegisterStaff.Visible = false;
                    pnlModifyStaffInfo.Visible = false;
                    pnlDeleteStaffInfo.Visible = false;
                    pnlDisplayStaffInfo.Visible = false;
                    pnlResetPassword.Visible = false;
                    pnlAddOrderInformation.Visible = false;
                    pnlModifyOrderInfo.Visible = false;
                    pnlDeleteOrderInfo.Visible = false;
                    pnlSearchDispOrderInfo.Visible = false;
                    //Weiquan
                    panelAddNewCategory.Visible = false;
                    panelModifyCategory.Visible = false;
                    panelDeleteCategory.Visible = false;
                    panelDisplayCategory.Visible = false;
                    panelAddNewProduct.Visible = false;
                    panelModifyProduct.Visible = false;
                    panelDeleteProduct.Visible = false;
                    panelDisplayProduct.Visible = false;
                    panelSelectAProductToDisplay.Visible = false;
                    //Jai
                    pnlAddNewSupplierInfo.Visible = false;
                    pnlModifySupplierInfo.Visible = false;
                    pnlDeleteSupplierInfo.Visible = false;
                    pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                    pnlDisplaySupplierInformation_Details.Visible = false;
                    pnlAddStock.Visible = false;
                    pnlEditStockInfo.Visible = false;
                    pnlDisplayComputeExcessSHrtfall.Visible = false;
                    pnlComputeExcessShortFall.Visible = false;
                    pnlSearchAndDisplayStock.Visible = false;

                    this.Text = "Welcome!";

                }
            }
        }

        private void btnCancelDeleteCategory_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel deleting?", "Cancel Delete", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                //Edmund
                pnlWelcomeScreen.Visible = true;//True
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Welcome!";
            }
        }
        //Display Category
        private void searchAndDisplayCategoryInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchCategory f = new FrmSearchCategory(allData, "Display Category");
            allData.searchCategoryName = "";
            f.ShowDialog();

            if (f.Cancel == false)
            {
                return;
            }
            if (allData.foundSearch == true)
            {
                
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = true;//True
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Display Existing Category Information";
                lblDisplayCategoryName.Text = allData.searchCategoryName;
                tbxDisplayCategoryDescrption.Text = allData.storeTempCategory.Descrption;
            }
        }

        private void btnDisplayCategory_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Pressing the YES button will bring you to the homepage.", "Stop Viewing", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                pnlWelcomeScreen.Visible = true;//True
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Welcome!";
            }
        }
        //Add Product
        private void addNewProductInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchProduct f = new FrmSearchProduct(allData,"Add Product");
            allData.searchProductName = "";
            allData.searchProductModelNumber = "";
            f.ShowDialog();
            

            if (f.Cancel == false)
            {
                return;
            }
            if (allData.foundSearch == false)
            {
                tabControlAddNewProduct.SelectTab(0);
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = true;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                lblAddProductName.Text = allData.searchProductName;
                lblAddProductModelNumber.Text = allData.searchProductModelNumber;
                tbxAddProductDescription.Text = "";
                tbxAddProductMSRP.Text = "";
                tbxAddProductRetailPrice.Text = "";
                cbxAddProductCategory.Text = "";
                picbxAddProductImage1.Image = null;
                picbxAddProductImage2.Image = null;

                
                this.Text = "Add New Product";

                cbxAddProductCategory.Items.Clear();
                for (int i = 0; i < allData.categoryList.Count; i++)
                {
                    if (allData.categoryList[i] != null)
                    {
                        Category c = (Category)allData.categoryList[i];
                        cbxAddProductCategory.Items.Add(c.Name);
                    }
                }
            }

          
        }

        private void btnAddProductOpenImage1_Click(object sender, EventArgs e)
        {
            try
            {

                OpenFileDialog open = new OpenFileDialog();

                open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

                if (open.ShowDialog() == DialogResult.OK)
                {

                    picbxAddProductImage1.Image = new Bitmap(open.FileName);
                    picbxAddProductImage1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage; 
                }

            }

            catch (Exception)
            {

                throw new ApplicationException("Failed loading image");

            }

        }

        private void btnAddProductRemoveImage1_Click(object sender, EventArgs e)
        {
            picbxAddProductImage1.Image = null;
        }

        private void btnAddProductEnlargeImage1_Click(object sender, EventArgs e)
        {
            FrmPictureBox f = new FrmPictureBox((Bitmap)picbxAddProductImage1.Image);
            f.ShowDialog();
        }

        private void btnAddProductOpenImage2_Click(object sender, EventArgs e)
        {
            try
            {

                OpenFileDialog open = new OpenFileDialog();

                open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

                if (open.ShowDialog() == DialogResult.OK)
                {

                    picbxAddProductImage2.Image = new Bitmap(open.FileName);
                    picbxAddProductImage2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                }

            }

            catch (Exception)
            {

                throw new ApplicationException("Failed loading image");

            }
        }

        private void btnAddProductRemoveImage2_Click(object sender, EventArgs e)
        {
            picbxAddProductImage2.Image = null;
        }

        private void btnAddProductEnlargeImage2_Click(object sender, EventArgs e)
        {
            FrmPictureBox f = new FrmPictureBox((Bitmap)picbxAddProductImage2.Image);
            f.ShowDialog();
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxAddProductDescription.Text != "" && tbxAddProductMSRP.Text != "" && tbxAddProductRetailPrice.Text != "" && cbxAddProductCategory.Text != "")
                {
                        bool validCategory = false;

                        for (int i = 0; i < cbxAddProductCategory.Items.Count; i++)
                        {
                            if ((string)cbxAddProductCategory.Items[i] == cbxAddProductCategory.Text)
                            {
                                validCategory = true;
                            }
                        }

                        if (validCategory == true)
                        {

                            DialogResult dr = MessageBox.Show("Continue to add product?", "Add Product", MessageBoxButtons.YesNo);

                            if (dr == DialogResult.Yes)
                            {
                                Product addProduct = new Product(lblAddProductName.Text, tbxAddProductDescription.Text, lblAddProductModelNumber.Text, Convert.ToDouble(tbxAddProductMSRP.Text),
                                                              Convert.ToDouble(tbxAddProductRetailPrice.Text), cbxAddProductCategory.Text);


                                if (picbxAddProductImage1.Image != null)
                                {
                                    addProduct.Image1 = (Bitmap)picbxAddProductImage1.Image;
                                }
                                if (picbxAddProductImage2.Image != null)
                                {
                                    addProduct.Image2 = (Bitmap)picbxAddProductImage2.Image;
                                }

                                allData.productList.Add(addProduct);


                                MessageBox.Show("Product has been added sucessfully!");

                                pnlWelcomeScreen.Visible = true;//True
                                pnlRegisterStaff.Visible = false;
                                pnlModifyStaffInfo.Visible = false;
                                pnlDeleteStaffInfo.Visible = false;
                                pnlDisplayStaffInfo.Visible = false;
                                pnlResetPassword.Visible = false;
                                pnlAddOrderInformation.Visible = false;
                                pnlModifyOrderInfo.Visible = false;
                                pnlDeleteOrderInfo.Visible = false;
                                pnlSearchDispOrderInfo.Visible = false;
                                //Weiquan
                                panelAddNewCategory.Visible = false;
                                panelModifyCategory.Visible = false;
                                panelDeleteCategory.Visible = false;
                                panelDisplayCategory.Visible = false;
                                panelAddNewProduct.Visible = false;
                                panelModifyProduct.Visible = false;
                                panelDeleteProduct.Visible = false;
                                panelDisplayProduct.Visible = false;
                                panelSelectAProductToDisplay.Visible = false;
                                //Jai
                                pnlAddNewSupplierInfo.Visible = false;
                                pnlModifySupplierInfo.Visible = false;
                                pnlDeleteSupplierInfo.Visible = false;
                                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                                pnlDisplaySupplierInformation_Details.Visible = false;
                                pnlAddStock.Visible = false;
                                pnlEditStockInfo.Visible = false;
                                pnlDisplayComputeExcessSHrtfall.Visible = false;
                                pnlComputeExcessShortFall.Visible = false;
                                pnlSearchAndDisplayStock.Visible = false;

                                this.Text = "Welcome!";
                            }

                            

                    }
                    else
                    {
                        MessageBox.Show("Product must be added to existing category!", "Error In Adding");
                        
                    }
                }
                else
                {
                    MessageBox.Show("Please fill in all the fields that marked with a *", "Error In Adding");
                }

            }
            catch (FormatException fe)
            {
                MessageBox.Show("Please enter a number value for MSRP and Retail Price!", "Error In Adding");
            }
        }

        private void btnCancelAddProduct_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel adding?", "Cancel Adding", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                pnlWelcomeScreen.Visible = true;//True
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Welcome!";
            }
        }

        //ModifyProduct
        private void modifyProductInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchProduct f = new FrmSearchProduct(allData, "Modify Product");
            allData.searchProductName = "";
            allData.searchProductModelNumber = "";
            f.ShowDialog();
            tabControlModifyProduct.SelectTab(0);

            if (f.Cancel == false)
            {
                return;
            }

            if (allData.foundSearch == true)
            {
                tabControlModifyProduct.SelectTab(0);

                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = true;//True
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Modify Existing Product";

                lblModifyProductName.Text = allData.searchProductName;
                lblModifyProductModelNumber.Text = allData.searchProductModelNumber;
                tbxModifyProductDescription.Text = allData.storeTempProduct.Descrption;
                tbxModifyProductMSRP.Text = Convert.ToString(allData.storeTempProduct.MSRP);
                tbxModifyProductRetailPrice.Text = Convert.ToString(allData.storeTempProduct.RetailPrice);

                cbxModifyProductCategory.Items.Clear();
                for (int i = 0; i < allData.categoryList.Count; i++)
                {
                    if (allData.categoryList[i] != null)
                    {
                        Category c = (Category)allData.categoryList[i];
                        cbxModifyProductCategory.Items.Add(c.Name);
                        
                    }
                }

                cbxModifyProductCategory.Text = allData.storeTempProduct.Category;

                if(allData.storeTempProduct.Image1 != null)
                {
                    picbxModifyProductImage1.Image = allData.storeTempProduct.Image1;
                    picbxModifyProductImage1.SizeMode = PictureBoxSizeMode.StretchImage;
                }

                if (allData.storeTempProduct.Image2 != null)
                {
                    picbxModifyProductImage2.Image = allData.storeTempProduct.Image2;
                    picbxModifyProductImage2.SizeMode = PictureBoxSizeMode.StretchImage;
                }


            }
        }

        private void btnModifyProductOpenImage1_Click(object sender, EventArgs e)
        {
            try
            {

                OpenFileDialog open = new OpenFileDialog();

                open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

                if (open.ShowDialog() == DialogResult.OK)
                {

                    picbxModifyProductImage1.Image = new Bitmap(open.FileName);
                    picbxModifyProductImage1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                }

            }

            catch (Exception)
            {

                throw new ApplicationException("Failed loading image");

            }
                    
        }

        private void btnModifyProductRemoveImage1_Click(object sender, EventArgs e)
        {
            picbxModifyProductImage1.Image = null;
        }

        private void btnModifyProductOpenImage2_Click(object sender, EventArgs e)
        {
            try
            {

                OpenFileDialog open = new OpenFileDialog();

                open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

                if (open.ShowDialog() == DialogResult.OK)
                {

                    picbxModifyProductImage2.Image = new Bitmap(open.FileName);
                    picbxModifyProductImage2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                }

            }

            catch (Exception)
            {

                throw new ApplicationException("Failed loading image");

            }
        }

        private void btnModifyProductRemoveImage2_Click(object sender, EventArgs e)
        {
            picbxModifyProductImage2.Image = null;
        }

        private void btnModifyProductEnlargeImage1_Click_1(object sender, EventArgs e)
        {
            FrmPictureBox f = new FrmPictureBox((Bitmap)picbxModifyProductImage1.Image);
            f.ShowDialog();
        }

        private void btnModifyProductEnlargeImage2_Click_1(object sender, EventArgs e)
        {
            FrmPictureBox f = new FrmPictureBox((Bitmap)picbxModifyProductImage2.Image);
            f.ShowDialog();
        }

        private void btnModifyProduct_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxModifyProductDescription.Text != "" && tbxModifyProductMSRP.Text != "" && tbxModifyProductRetailPrice.Text != "" && cbxModifyProductCategory.Text != "")
                {
                    bool validCategory = false;

                    for (int i = 0; i < cbxAddProductCategory.Items.Count; i++)
                    {
                        if ((string)cbxModifyProductCategory.Items[i] == cbxModifyProductCategory.Text)
                        {
                            validCategory = true;
                        }
                    }

                    if (validCategory == true)
                    {
                        DialogResult dr = MessageBox.Show("Continue to modify product?", "Modify Product", MessageBoxButtons.YesNo);

                        if (dr == DialogResult.Yes)
                        {
                            for (int i = 0; i < allData.productList.Count; i++)
                            {
                                if (allData.productList[i] != null)
                                {
                                    Product p = (Product)allData.productList[i];
                                    if (p.Name == lblModifyProductName.Text && p.ModelNumber == lblModifyProductModelNumber.Text)
                                    {
                                        Product modifyProduct = new Product(lblModifyProductName.Text, tbxModifyProductDescription.Text.Trim(), lblModifyProductModelNumber.Text,
                                                                              Convert.ToDouble(tbxModifyProductMSRP.Text.Trim()), Convert.ToDouble(tbxModifyProductRetailPrice.Text.Trim()), cbxModifyProductCategory.Text.Trim());
                                        if (picbxModifyProductImage1.Image != null)
                                        {
                                            modifyProduct.Image1 = (Bitmap)picbxModifyProductImage1.Image;
                                        }
                                        if (picbxModifyProductImage2.Image != null)
                                        {
                                            modifyProduct.Image2 = (Bitmap)picbxModifyProductImage2.Image;
                                        }

                                        allData.productList.RemoveAt(i);
                                        allData.productList.Add(modifyProduct);
                                        break;
                                    }
                                }
                            }

                            MessageBox.Show("Product has been updated sucessfully!","Update Sucessful");
                            //Edmund
                            pnlWelcomeScreen.Visible = true;//True
                            pnlRegisterStaff.Visible = false;
                            pnlModifyStaffInfo.Visible = false;
                            pnlDeleteStaffInfo.Visible = false;
                            pnlDisplayStaffInfo.Visible = false;
                            pnlResetPassword.Visible = false;
                            pnlAddOrderInformation.Visible = false;
                            pnlModifyOrderInfo.Visible = false;
                            pnlDeleteOrderInfo.Visible = false;
                            pnlSearchDispOrderInfo.Visible = false;
                            //Weiquan
                            panelAddNewCategory.Visible = false;
                            panelModifyCategory.Visible = false;
                            panelDeleteCategory.Visible = false;
                            panelDisplayCategory.Visible = false;
                            panelAddNewProduct.Visible = false;
                            panelModifyProduct.Visible = false;
                            panelDeleteProduct.Visible = false;
                            panelDisplayProduct.Visible = false;
                            panelSelectAProductToDisplay.Visible = false;
                            //Jai
                            pnlAddNewSupplierInfo.Visible = false;
                            pnlModifySupplierInfo.Visible = false;
                            pnlDeleteSupplierInfo.Visible = false;
                            pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                            pnlDisplaySupplierInformation_Details.Visible = false;
                            pnlAddStock.Visible = false;
                            pnlEditStockInfo.Visible = false;
                            pnlDisplayComputeExcessSHrtfall.Visible = false;
                            pnlComputeExcessShortFall.Visible = false;
                            pnlSearchAndDisplayStock.Visible = false;
                            tbxModifyCategoryDescrption.Text = "";

                            this.Text = "Welcome!";
                        }
                    }
                    else
                    {
                        MessageBox.Show("Product must belong to existing category!", "Error In Modyfing");
                    }
                }

                else
                {
                    MessageBox.Show("Please fill in all the fields that marked with a *", "Error In Adding");
                }
            }
            catch (FormatException fe)
            {
                MessageBox.Show("Please enter a number value for MSRP and Retail Price!", "Error In Modyfing");
            }
        }

        private void btnCancelModifyProduct_Click(object sender, EventArgs e)
        {
             DialogResult dr = MessageBox.Show("Are you sure you want to cancel modifying?","Cancel Modyfing", MessageBoxButtons.YesNo);

             if (dr == DialogResult.Yes)
             {
                 pnlWelcomeScreen.Visible = true;//True
                 pnlRegisterStaff.Visible = false;
                 pnlModifyStaffInfo.Visible = false;
                 pnlDeleteStaffInfo.Visible = false;
                 pnlDisplayStaffInfo.Visible = false;
                 pnlResetPassword.Visible = false;
                 pnlAddOrderInformation.Visible = false;
                 pnlModifyOrderInfo.Visible = false;
                 pnlDeleteOrderInfo.Visible = false;
                 pnlSearchDispOrderInfo.Visible = false;
                 //Weiquan
                 panelAddNewCategory.Visible = false;
                 panelModifyCategory.Visible = false;
                 panelDeleteCategory.Visible = false;
                 panelDisplayCategory.Visible = false;
                 panelAddNewProduct.Visible = false;
                 panelModifyProduct.Visible = false;
                 panelDeleteProduct.Visible = false;
                 panelDisplayProduct.Visible = false;
                 panelSelectAProductToDisplay.Visible = false;
                 //Jai
                 pnlAddNewSupplierInfo.Visible = false;
                 pnlModifySupplierInfo.Visible = false;
                 pnlDeleteSupplierInfo.Visible = false;
                 pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                 pnlDisplaySupplierInformation_Details.Visible = false;
                 pnlAddStock.Visible = false;
                 pnlEditStockInfo.Visible = false;
                 pnlDisplayComputeExcessSHrtfall.Visible = false;
                 pnlComputeExcessShortFall.Visible = false;
                 pnlSearchAndDisplayStock.Visible = false;

                 this.Text = "Welcome!";
             }
        }

        //Delete Product
        private void deleteExistingProductInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchProduct f = new FrmSearchProduct(allData, "Delete Product");
            allData.searchProductName = "";
            allData.searchProductModelNumber = "";
            f.ShowDialog();

            if (f.Cancel == false)
            {
                return;
            }
            if (allData.foundSearch == true)
            {
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = true;//True
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Delete Existing Product";

                btnDeleteProductEnlargeImage1.Visible = false;
                btnDeleteProductEnlargeImage2.Visible = false;

                lblDeleteProductDisplayName.Text = allData.searchProductName;
                lblDeleteProductDisplayModelNumber.Text = allData.searchProductModelNumber;
                lblDeleteProductDisplayMSRP.Text = "$" + Convert.ToString(allData.storeTempProduct.MSRP);
                lblDeleteProductDisplayRetailPrice.Text = "$" + Convert.ToString(allData.storeTempProduct.RetailPrice);
                lblDeleteProductDisplayCategory.Text = allData.storeTempProduct.Category;
                tbxDeleteProductDisplayDescription.Text = allData.storeTempProduct.Descrption;
                picbxDeleteProductImage1.Image = allData.storeTempProduct.Image1;
                picbxDeleteProductImage2.Image = allData.storeTempProduct.Image2;



                if (picbxDeleteProductImage1.Image != null)
                {
                    picbxDeleteProductImage1.SizeMode = PictureBoxSizeMode.StretchImage;
                    btnDeleteProductEnlargeImage1.Visible = true;
                }
                picbxDeleteProductImage2.Image = allData.storeTempProduct.Image2;
                if (picbxDeleteProductImage2.Image != null)
                {
                    picbxDeleteProductImage2.SizeMode = PictureBoxSizeMode.StretchImage;
                    btnDeleteProductEnlargeImage2.Visible = true;
                }
            }
        }

        private void btnDeleteProduct_Click(object sender, EventArgs e)
        {

            DialogResult dr = MessageBox.Show("Continue to delete product?", "Delete Product", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                for (int i = 0; i < allData.productList.Count; i++)
                {
                    if (allData.productList[i] != null)
                    {
                        Product p = (Product)allData.productList[i];
                        if (p.Name == allData.searchProductName && p.ModelNumber == allData.searchProductModelNumber)
                        {

                            allData.productList.RemoveAt(i);
                            MessageBox.Show("Product has been deleted sucessfully!", "Delete Sucessful");
                            break;
                        }
                    }
                }

                //Edmund
                pnlWelcomeScreen.Visible = true;//True
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Welcome";
            }
        }

        private void btnCancelDeleteProduct_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel deleteing?","Cancel Deleting", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                //Edmund
                pnlWelcomeScreen.Visible = true;//True
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
                this.Text = "Welcome";

            }
        }

        private void btnDeleteProductEnlargeImage1_Click(object sender, EventArgs e)
        {
            FrmPictureBox f = new FrmPictureBox((Bitmap)allData.storeTempProduct.Image1);
            f.ShowDialog();
        }

        private void btnDeleteProductEnlargeImage2_Click(object sender, EventArgs e)
        {
            FrmPictureBox f = new FrmPictureBox((Bitmap)allData.storeTempProduct.Image2);
            f.ShowDialog();
        }

        //Display Product
        private void searchDisplayProductInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchDisplayProduct f = new FrmSearchDisplayProduct(allData);
            allData.searchProductName = "";
            allData.searchProductModelNumber = "";
            f.ShowDialog();

            listbxSelectAProductToDisplay.Items.Clear();

            if (f.Cancel == false)
            {
                return;
            }

            if (allData.foundSearch == true)
            {
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = true;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Select Product To Display";

                for (int i = 0; i < allData.storeTempProductList.Count; i++)
                {
                    if (allData.storeTempProductList[i] != null)
                    {
                        Product productInTempProductList = (Product)allData.storeTempProductList[i];
                        listbxSelectAProductToDisplay.Items.Add("Product Name: " + productInTempProductList.Name + " , Model Number: " + productInTempProductList.ModelNumber);

                    }
                }
            }
        }

        private void btnSelectAProductToDisplay_Click(object sender, EventArgs e)
        {

            if (listbxSelectAProductToDisplay.SelectedIndex != -1)
            {
                DialogResult dr = MessageBox.Show("Continue to view selected product?", "View Product", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    selectedIndex = listbxSelectAProductToDisplay.SelectedIndex;

                    for (int i = 0; i < allData.storeTempProductList.Count; i++)
                    {
                        if (allData.storeTempProductList[i] != null)
                        {
                            Product p = (Product)allData.storeTempProductList[i];
                            allData.storeTempProductListForDisplay.Add(p);
                        }
                    }

                    for (int i = 0; i < allData.storeTempProductListForDisplay.Count; i++)
                    {
                        if (allData.storeTempProductListForDisplay[i] != null && i == selectedIndex)
                        {
                            Product p = (Product)allData.storeTempProductListForDisplay[i];

                            lblDisplayProductDisplayName.Text = p.Name;
                            lblDisplayProductDisplayModelNumber.Text = p.ModelNumber;
                            lblDisplayProductDisplayRetailPRice.Text = "$" + Convert.ToString(p.RetailPrice);
                            lblDisplayProductDisplayMSRP.Text = "$" + Convert.ToString(p.MSRP);
                            lblDisplayProductDisplayCategory.Text = p.Category;
                            tbxDisplayProductDisplayDescription.Text = p.Descrption;
                            picbxDisplayProductImage1.Image = p.Image1;

                            btnDisplayProductEnlargeImage1.Visible = false;
                            btnDisplayProductEnlargeImage2.Visible = false;

                            if (picbxDisplayProductImage1.Image != null)
                            {
                                picbxDisplayProductImage1.SizeMode = PictureBoxSizeMode.StretchImage;
                                btnDisplayProductEnlargeImage1.Visible = true;
                            }

                            picbxDisplayProductImage2.Image = p.Image2;

                            if (picbxDisplayProductImage2.Image != null)
                            {
                                picbxDisplayProductImage2.SizeMode = PictureBoxSizeMode.StretchImage;
                                btnDisplayProductEnlargeImage2.Visible = true;
                            }
                        }
                    }
                    pnlWelcomeScreen.Visible = false;
                    pnlRegisterStaff.Visible = false;
                    pnlModifyStaffInfo.Visible = false;
                    pnlDeleteStaffInfo.Visible = false;
                    pnlDisplayStaffInfo.Visible = false;
                    pnlResetPassword.Visible = false;
                    pnlAddOrderInformation.Visible = false;
                    pnlModifyOrderInfo.Visible = false;
                    pnlDeleteOrderInfo.Visible = false;
                    pnlSearchDispOrderInfo.Visible = false;
                    //Weiquan
                    panelAddNewCategory.Visible = false;
                    panelModifyCategory.Visible = false;
                    panelDeleteCategory.Visible = false;
                    panelDisplayCategory.Visible = false;
                    panelAddNewProduct.Visible = false;
                    panelModifyProduct.Visible = false;
                    panelDeleteProduct.Visible = false;
                    panelDisplayProduct.Visible = true;
                    panelSelectAProductToDisplay.Visible = false;
                    //Jai
                    pnlAddNewSupplierInfo.Visible = false;
                    pnlModifySupplierInfo.Visible = false;
                    pnlDeleteSupplierInfo.Visible = false;
                    pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                    pnlDisplaySupplierInformation_Details.Visible = false;
                    pnlAddStock.Visible = false;
                    pnlEditStockInfo.Visible = false;
                    pnlDisplayComputeExcessSHrtfall.Visible = false;
                    pnlComputeExcessShortFall.Visible = false;
                    pnlSearchAndDisplayStock.Visible = false;
                    allData.storeTempProductListForDisplay.Clear();
                    this.Text = "Display Product";
                }

            }
            else
            {
                MessageBox.Show("Please select a product to view","Error In Viewing");
            }
        }

        private void btnCancelSelectAProductToDisplay_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel selecting?", "Cancel Selecting", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                pnlWelcomeScreen.Visible = true;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Welcome!";
            }
        }

        
        private void btnDisplayProduct_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Pressing the YES button will bring you to the homepage.", "Stop Viewing", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                pnlWelcomeScreen.Visible = true;//True
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                this.Text = "Welcome";
            }
        

        }

        private void btnDisplayProductEnlargeImage1_Click(object sender, EventArgs e)
        {
            if (picbxDisplayProductImage1.Image != null)
            {
                FrmPictureBox f = new FrmPictureBox((Bitmap)picbxDisplayProductImage1.Image);
                f.ShowDialog();
            }
        }

        private void btnDisplayProductEnlargeImage2_Click(object sender, EventArgs e)
        {
            if (picbxDisplayProductImage2.Image != null)
            {
                FrmPictureBox f = new FrmPictureBox((Bitmap)picbxDisplayProductImage2.Image);
                f.ShowDialog();
            }
        }




















        //==========================
        /*    *******    *     *
                 *      * *    *
                 *     *   *   *
                 *    *     *  *
                 *    *     *  *
             *   *    *******  *
              *  *    *     *  *
               * *    *     *  *
                 *    *     *  *
            
         */
        //=========================

        private void btnAddStock_Add_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to add stock?", "Add Stock", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {

                if (tbxAddStock_CompanyName.Text == "" || tbxAddStock_ModelName.Text == "" || tbxAddStock_ProductName.Text == "" || tbxAddStock_StockQty.Text == "")
                {
                    MessageBox.Show("Please enter all the fields!", "Missing Fields");
                }
                else
                {
                    Stock stock = new Stock(tbxAddStock_CompanyName.Text.Trim(), tbxAddStock_ProductName.Text.Trim(), tbxAddStock_ModelName.Text.Trim(), Convert.ToInt32(tbxAddStock_StockQty.Text.Trim()));
                    allData.stockList.Add(stock);
                    allData.searchStock_ProductQty = tbxAddStock_StockQty.Text.Trim();
                    MessageBox.Show("Stock Succesfully added!", "Added Succesfully");
                    tbxAddStock_CompanyName.Text = "";
                    tbxAddStock_ModelName.Text = "";
                    tbxAddStock_ProductName.Text = "";
                    tbxAddStock_StockQty.Text = "";
                    backToMain();
                }
            }
        }

        private void btnAddStock_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel adding?", "Add Stock", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {

                backToMain();
                tbxAddStock_StockQty.Text = "";
                tbxAddStock_CompanyName.Text = "";
                tbxAddStock_ModelName.Text = "";
                tbxAddStock_ProductName.Text = "";
            }
        }

        private void btnUpdateStock_Click(object sender, EventArgs e)
        {
           DialogResult dr = MessageBox.Show("Are you sure you want to update this stock quantity?", "Update Stock", MessageBoxButtons.YesNo);
           if (dr == DialogResult.Yes)
           {

               MessageBox.Show("Stock has been updated!", "Updated");
               for (int i = 0; i < allData.stockList.Count; i++)
               {
                   if (allData.stockList[i] != null)
                   {
                       Stock s = (Stock)allData.stockList[i];
                       if (s.ProductName == lblEditStock_DisplayProductName.Text && s.ModelNumber == lblEditStock_DisplayModelName.Text && s.Supplier == lblEditStock_DisplayCompanyName.Text)
                       {
                           Stock modifyStock = new Stock(lblEditStock_DisplayCompanyName.Text, lblEditStock_DisplayProductName.Text, lblEditStock_DisplayModelName.Text, Convert.ToInt32(tbxEditStock_StockQty.Text));
                           allData.stockList.RemoveAt(i);
                           allData.stockList.Add(modifyStock);
                           break;
                       }
                   }
               }
               backToMain();
           }
        }

        private void btnCancelUpdateStock_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel updating stock?", "Add Stock", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {

                backToMain();
                lblEditStock_DisplayCompanyName.Text = "";
                lblEditStock_DisplayModelName.Text = "";
                lblEditStock_DisplayProductName.Text = "";
                tbxEditStock_StockQty.Text = "";
            }
        }

        private void btnRegisterSupplier_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want register supplier?", "Add Supplier", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {

                if (tbxRegisterAddress_Supplier.Text == "" || tbxRegisterCityCountry.Text == "" || tbxRegisterCompanyName.Text == "" || tbxRegisterPhoneOfficeNo.Text == "" || tbxRegisterPostalCode.Text == "" || tbxRegisterSupplierName.Text == "" || tbxRegisterFaxNum.Text == "")
                {
                    MessageBox.Show("Please fill in all the blanks", "Missing Fields");
                }
                else
                {
                    Supplier s = new Supplier(tbxRegisterCompanyName.Text, tbxRegisterSupplierName.Text, tbxRegisterAddress_Supplier.Text, tbxRegisterCityCountry.Text, tbxRegisterPhoneOfficeNo.Text, tbxRegisterFaxNum.Text, tbxRegisterPostalCode.Text);
                    allData.inventoryList.Add(s);
                    MessageBox.Show("Supplier added Successfully.", "Added Successfully");
                    allData.searchCompanyName = "";
                    tbxRegisterCompanyName.Text = "";
                    tbxRegisterSupplierName.Text = "";
                    tbxRegisterPostalCode.Text = "";
                    tbxRegisterPhoneOfficeNo.Text = "";
                    tbxRegisterCityCountry.Text = "";
                    tbxRegisterAddress_Supplier.Text = "";
                    tbxRegisterFaxNum.Text = "";
                    backToMain();



                }
            }
        }

        private void btnCancelRegisterSupplier_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel adding supplier?", "Cancel adding supplier", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {

                tbxRegisterCompanyName.Text = "";
                tbxRegisterSupplierName.Text = "";
                tbxRegisterPostalCode.Text = "";
                tbxRegisterPhoneOfficeNo.Text = "";
                tbxRegisterCityCountry.Text = "";
                tbxRegisterAddress_Supplier.Text = "";
                tbxRegisterFaxNum.Text = "";
                backToMain();
            }
        }

        private void btnModifySupplier_Update_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to modify supplier?", "Modify Supplier", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {

                if (tbxModifySupplier_Address.Text.Trim() == "" || tbxModifySupplier_CityCountry.Text == "" || tbxModifySupplier_Fax.Text == "" || tbxModifySupplier_Name.Text == "" || tbxModifySupplier_Phone.Text == "" || tbxModifySupplier_PostalCode.Text == "")
                {
                    MessageBox.Show("Please enter all the fields", "Missing Fields");
                }
                else
                    for (int i = 0; i < allData.inventoryList.Count; i++)
                    {
                        if (allData.inventoryList[i] != null)
                        {
                            Supplier s = (Supplier)allData.inventoryList[i];
                            if (s.CompanyName == lblModifySupplier_DispCompName.Text)
                            {

                                Supplier modifySupplier = new Supplier(lblModifySupplier_DispCompName.Text, tbxModifySupplier_Name.Text, tbxModifySupplier_Address.Text, tbxModifySupplier_CityCountry.Text, tbxModifySupplier_Phone.Text, tbxModifySupplier_Fax.Text, tbxModifySupplier_PostalCode.Text);
                                allData.inventoryList.RemoveAt(i);
                                allData.inventoryList.Add(modifySupplier);
                                break;
                            }
                        }
                    }
                MessageBox.Show("Update is successfull.");
                tbxModifySupplier_Address.Text = "";
                tbxModifySupplier_CityCountry.Text = "";
                tbxModifySupplier_Fax.Text = "";
                tbxModifySupplier_Name.Text = "";
                tbxModifySupplier_Phone.Text = "";
                tbxModifySupplier_PostalCode.Text = "";
                lblModifySupplier_DispCompName.Text = "";

                backToMain();
            }
        }

        private void btnMdifySupplier_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel modifying supplier", "Cancel Modify Supplier", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {

                tbxModifySupplier_Address.Text = "";
                tbxModifySupplier_CityCountry.Text = "";
                tbxModifySupplier_Fax.Text = "";
                tbxModifySupplier_Name.Text = "";
                tbxModifySupplier_Phone.Text = "";
                tbxModifySupplier_PostalCode.Text = "";
                lblModifySupplier_DispCompName.Text = "";

                backToMain();
            }
        }

        private void btnDeleteSupplier_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to delete supplier?", "Delete Supplier", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {

                for (int i = 0; i < allData.inventoryList.Count; i++)
                {
                    if (allData.inventoryList[i] != null)
                    {
                        Supplier s = (Supplier)allData.inventoryList[i];
                        if (s.CompanyName == lblDeleteSupplier_CompanyName.Text)
                        {
                            {
                                allData.inventoryList.RemoveAt(i);
                                MessageBox.Show("Supplier has been deleted", "Deleted Successfully");
                                break;
                            }
                        }
                    }
                }

                backToMain();
                lblDeleteSupplier_CityCountry.Text = "";
                lblDeleteSupplier_CompanyName.Text = "";
                tbxDeleteSupplier_Address.Text = "";
                lblDeleteSupplier_FaxNum.Text = "";
                lblDeleteSupplier_PhoneNum.Text = "";
                lblDeleteSupplier_PostalCode.Text = "";
                lblDeleteSupplier_SupplierName.Text = "";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel deleting?", "Cancel deleting supplier", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {

                backToMain();
                lblDeleteSupplier_CityCountry.Text = "";
                lblDeleteSupplier_CompanyName.Text = "";
                tbxDeleteSupplier_Address.Text = "";
                lblDeleteSupplier_FaxNum.Text = "";
                lblDeleteSupplier_PhoneNum.Text = "";
                lblDeleteSupplier_PostalCode.Text = "";
                lblDeleteSupplier_SupplierName.Text = "";
            }
        }
        private void btnDisplaySupplierInformation_Details_OK_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("This will bring you to the back main page. Are you sure you want to return?", "Back to main page", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {


                backToMain();
                lblDisplaySupplierInformation_Display_CityCountry.Text = "";
                lblDisplaySupplierInformation_Display_CompanyName.Text = "";
                lblDisplaySupplierInformation_Display_FaxNum.Text = "";
                lblDisplaySupplierInformation_Display_PhoneNum.Text = "";
                lblDisplaySupplierInformation_Display_PostalCode.Text = "";
                lblDisplaySupplierInformation_Display_SupplierName.Text = "";
                tbxDisplaySupplierInformation_Display_Address.Text = "";
            }
        }

        private void btnDispSupplierInfoOK_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to view this companys detail?", "View company details", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
            
            int checkNum;
            if (listBxDispSupplierInfo.SelectedIndex != -1)
            {
                checkNum = listBxDispSupplierInfo.SelectedIndex;
                for (int i = 0; i < allData.tempSupplierList.Count; i++)
                {
                    if (allData.tempSupplierList[i] != null)
                    {
                        Supplier s = (Supplier)allData.tempSupplierList[i];
                        allData.storeTempSupplierListForDisplay.Add(s);
                    }
                }
                for (int i = 0; i < allData.storeTempSupplierListForDisplay.Count; i++)
                {
                    if (allData.storeTempSupplierListForDisplay[i] != null && i == checkNum)
                    {
                        Supplier s = (Supplier)allData.storeTempSupplierListForDisplay[i];

                        lblDisplaySupplierInformation_Display_CityCountry.Text = s.City;
                        lblDisplaySupplierInformation_Display_CompanyName.Text = s.CompanyName;
                        lblDisplaySupplierInformation_Display_FaxNum.Text = s.FaxNumber;
                        lblDisplaySupplierInformation_Display_PhoneNum.Text = s.PhoneNumber;
                        lblDisplaySupplierInformation_Display_PostalCode.Text = s.PostalCode;
                        lblDisplaySupplierInformation_Display_SupplierName.Text = s.ContactName;
                        tbxDisplaySupplierInformation_Display_Address.Text = s.Address;

                        pnlWelcomeScreen.Visible = false;
                        pnlRegisterStaff.Visible = false;
                        pnlModifyStaffInfo.Visible = false;
                        pnlDeleteStaffInfo.Visible = false;
                        pnlDisplayStaffInfo.Visible = false;
                        pnlResetPassword.Visible = false;
                        pnlAddOrderInformation.Visible = false;
                        pnlModifyOrderInfo.Visible = false;
                        pnlDeleteOrderInfo.Visible = false;
                        pnlSearchDispOrderInfo.Visible = false;
                        //Weiquan
                        panelAddNewCategory.Visible = false;
                        panelModifyCategory.Visible = false;
                        panelDeleteCategory.Visible = false;
                        panelDisplayCategory.Visible = false;
                        panelAddNewProduct.Visible = false;
                        panelModifyProduct.Visible = false;
                        panelDeleteProduct.Visible = false;
                        panelDisplayProduct.Visible = false;
                        //panelSelectAProductToDisplay.Visible = false;
                        //Jai
                        pnlAddNewSupplierInfo.Visible = false;
                        pnlModifySupplierInfo.Visible = false;
                        pnlDeleteSupplierInfo.Visible = false;
                        pnlDisplaySupplierInformation_Details.Visible = true;
                        pnlAddStock.Visible = false;
                        pnlEditStockInfo.Visible = false;
                        pnlDisplayComputeExcessSHrtfall.Visible = false;
                        pnlComputeExcessShortFall.Visible = false;
                        pnlSearchAndDisplayStock.Visible = false;
                        pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                        allData.storeTempSupplierListForDisplay.Clear();

                        listBxDispSupplierInfo.Items.Clear();

                        this.Text = "Supplier Informtion";
                    }
                }
                }
            }
        }



        private void aToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchSupplier f = new FrmSearchSupplier(allData, 1);
            allData.searchCompanyName = "";
            f.ShowDialog();

            if (f.Cancel == false)
            {
                return;
            }

            if (allData.findSearch == false)
            {
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = true;//true
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;

                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                tbxRegisterCompanyName.Text = allData.searchCompanyName;
                this.Text = "Add Supplier";
            }
        }

        private void modifyExistingSupplierInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchSupplier f = new FrmSearchSupplier(allData, 2);
            allData.searchCompanyName = "";
            f.ShowDialog();

            if (f.Cancel == false)
            {
                return;
            }

            if (allData.findSearch == true)
            {
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelSelectAProductToDisplay.Visible = false;
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = true;//true
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;

                for (int i = 0; i < allData.inventoryList.Count; i++)
                {
                    if (allData.inventoryList[i] != null)
                    {
                        Supplier s = (Supplier)allData.inventoryList[i];
                        if (s.CompanyName == allData.searchCompanyName)
                        {
                            tbxModifySupplier_CityCountry.Text = s.City;
                            lblModifySupplier_DispCompName.Text = s.CompanyName;
                            tbxModifySupplier_Fax.Text = s.FaxNumber;
                            tbxModifySupplier_Phone.Text = s.PhoneNumber;
                            tbxModifySupplier_PostalCode.Text = s.PostalCode;
                            tbxModifySupplier_Name.Text = s.ContactName;
                            tbxModifySupplier_Address.Text = s.Address;

                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Company not found.");
            }

            this.Text = "Modify Supplier";
        }

        private void deleteExistingSupplierInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchSupplier f = new FrmSearchSupplier(allData, 2);
            allData.searchCompanyName = "";
            f.ShowDialog();

            if (f.Cancel == false)
            {
                return;
            }
            if (allData.findSearch == true)
            {
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelSelectAProductToDisplay.Visible = false;
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = true;//true

                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;

                lblDeleteSupplier_CompanyName.Text = allData.searchCompanyName;
                tbxDeleteSupplier_Address.Text = allData.storeTempSupplier.Address;
                lblDeleteSupplier_CityCountry.Text = allData.storeTempSupplier.City;
                lblDeleteSupplier_FaxNum.Text = allData.storeTempSupplier.FaxNumber;
                lblDeleteSupplier_SupplierName.Text = allData.storeTempSupplier.ContactName;
                lblDeleteSupplier_PhoneNum.Text = allData.storeTempSupplier.PhoneNumber;
                lblDeleteSupplier_PostalCode.Text = allData.storeTempSupplier.PostalCode;
            }
            this.Text = "Delete Supplier";
        }

        private void searchAndDisplaySupplierInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchDisplaySupplier f = new FrmSearchDisplaySupplier(allData);

            allData.searchCompanyName = "";

            f.ShowDialog();

            if (f.Cancel == false)
            {
                return;
            }

            if (allData.findSearch == true)
            {
                //Edmund
                panelSelectAProductToDisplay.Visible = false;
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = true;

                for (int i = 0; i < allData.tempSupplierList.Count; i++)
                {
                    if (allData.inventoryList[i] != null)
                    {
                        Supplier supplierInTempSuppList = (Supplier)allData.tempSupplierList[i];
                        listBxDispSupplierInfo.Items.Add("Company Name: " + supplierInTempSuppList.CompanyName);
                    }
                }
                this.Text = "Display Supplier Information";
            }
        }

        private void editProductQuantityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchStock f = new FrmSearchStock(allData);

            f.ShowDialog();


            if (f.Cancel == false)
            {
                return;
            }

            if (allData.findSearch == true)
            {
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = true;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                lblEditStock_DisplayCompanyName.Text = allData.storeTempStock.Supplier;
                lblEditStock_DisplayModelName.Text = allData.storeTempStock.ModelNumber;
                lblEditStock_DisplayProductName.Text = allData.storeTempStock.ProductName;
                tbxEditStock_StockQty.Text = Convert.ToString(allData.storeTempStock.Quantity);
                this.Text = "Edit Stock";
            }
            else if (allData.findSearch == false)
            {
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = true;//true
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                tbxAddStock_ModelName.Text = allData.searchStock_ModelNum;
                tbxAddStock_ProductName.Text = allData.searchStock_ProductName;
                tbxAddStock_CompanyName.Text = allData.searchStock_CompanyName;

                this.Text = "Add Stock";


            }
        }

        private void searchAndDisplayProductQuantityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchForStockDisplay f = new FrmSearchForStockDisplay(allData);
            allData.searchStockDisplay_CompName = "";
            allData.searchStockDisplay_ModelName = "";
            allData.searchStockDisplay_ProductName = "";
            f.ShowDialog();
            listBxDispStockInfo.Items.Clear();

            if (f.Cancel == false)
            {
                return;
            }

            if (allData.findSearch == true)
            {
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = true;

                this.Text = "Search stock";


                for (int i = 0; i < allData.storeTempStockListForDisplay.Count; i++)
                {
                    if (allData.storeTempStockListForDisplay[i] != null)
                    {
                        Stock stockinTemList = (Stock)allData.storeTempStockListForDisplay[i];
                        listBxDispStockInfo.Items.Add("Product Name: " + stockinTemList.ProductName + " ,Model Num: " + stockinTemList.ModelNumber + " ,Company Name: " + stockinTemList.Supplier);
                    }
                }

            }
        }

        public void backToMain()
        {
            //Edmund
            pnlWelcomeScreen.Visible = true;//True
            pnlRegisterStaff.Visible = false;
            pnlModifyStaffInfo.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlDisplayStaffInfo.Visible = false;
            pnlResetPassword.Visible = false;
            pnlAddOrderInformation.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlDeleteOrderInfo.Visible = false;
            pnlSearchDispOrderInfo.Visible = false;
            //Weiquan
            panelSelectAProductToDisplay.Visible = false;
            panelAddNewCategory.Visible = false;
            panelModifyCategory.Visible = false;
            panelDeleteCategory.Visible = false;
            panelDisplayCategory.Visible = false;
            panelAddNewProduct.Visible = false;
            panelModifyProduct.Visible = false;
            panelDeleteProduct.Visible = false;
            panelDisplayProduct.Visible = false;
            //Jai
            pnlAddNewSupplierInfo.Visible = false;
            pnlModifySupplierInfo.Visible = false;
            pnlDeleteSupplierInfo.Visible = false;

            pnlDisplaySupplierInformation_Details.Visible = false;
            pnlAddStock.Visible = false;
            pnlEditStockInfo.Visible = false;
            pnlDisplayComputeExcessSHrtfall.Visible = false;
            pnlComputeExcessShortFall.Visible = false;
            pnlSearchAndDisplayStock.Visible = false;
            pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;

            this.Text = "Welcome!";
        }

        public void UpdateListBox()
        {
            listBxDispSupplierInfo.Items.Clear();
        }

        private void btnDeleteStaff_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to delete this staff?", "Delete staff information", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                for (int i = 0; i < allData.staffList.Count; i++)
                {
                    if (allData.staffList[i] != null)
                    {
                        Staff personnel = (Staff)allData.staffList[i];
                        if (personnel.Nric == lblDispDelStaffNRIC.Text)
                        {
                            allData.staffList.RemoveAt(i);
                            MessageBox.Show("Staff information has been deleted!", "Delete Staff Information");
                        }
                    }
                }
                this.Text = "Welcome!";
                //Edmund
                pnlWelcomeScreen.Visible = true;//true
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }
        }

        private void btnCancelDelete_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel deleting of stff information?", "Delete staff information", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                this.Text = "Welcome!";
                //Edmund
                pnlWelcomeScreen.Visible = true;//true
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }
        }

        private void searchForStaffInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchStaff frmSearchStaff = new FrmSearchStaff(allData, "Search and Display Staff");
            frmSearchStaff.ShowDialog();

            if (frmSearchStaff.Cancel == false)
            {
                return;
            }

            if (allData.foundSearchEntry == true)
            {
                this.Text = "Search and Display Staff Existing Staff Information";
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = true;//true
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                label139.Text = "";
                label138.Text = "";
                label137.Text = "";
                label136.Text = "";
                label135.Text = "";
                label134.Text = "";
                label148.Text = "";
                label132.Text = "";
                label133.Text = "";

                label139.Text = allData.storeTempStaff.Name;
                label138.Text = allData.storeTempStaff.Nric;
                label137.Text = allData.storeTempStaff.DateOfBirth;
                label136.Text = allData.storeTempStaff.Gender;
                label135.Text = Convert.ToString(allData.storeTempStaff.HomeTel);
                label134.Text = Convert.ToString(allData.storeTempStaff.MobileTel);
                label148.Text = allData.storeTempStaff.Address;
                label132.Text = allData.storeTempStaff.Password;

                label133.Text = allData.storeTempStaff.Role1;
                if (allData.storeTempStaff.Role2 != null)
                {
                    label133.Text = label133.Text + ", " + allData.storeTempStaff.Role2;
                }

            } 
        }

        private void btnDispStaffInfoOK_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to return to the main window?", "Display Staff Information", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                this.Text = "Welcome!";
                //Edmund
                pnlWelcomeScreen.Visible = true;//true
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }
        }

        private void btnResetPw_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to reset password?", "Reset staff password", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                lblDispStaffNameResetPw.Text = "";
                lblDispNRICResetPw.Text = "";
                lblDispDOBStaffResetRw.Text = "";

                ArrayList personnelList = allData.staffList;
                for (int i = 0; i < personnelList.Count; i++)
                {
                    Staff personnel = (Staff)personnelList[i];
                    if (personnel.Name == allData.searchStaffName)
                    {
                        if (tbxResetPw.Text != "" && tbxResetPw.Text == tbxCfmResetPw.Text)
                        {
                            personnel.Password = tbxCfmResetPw.Text;
                            MessageBox.Show("Staff password is reset.", "Reset Staff Password");
                            tbxResetPw.Text = "";
                            tbxCfmResetPw.Text = "";

                            //Edmund
                            pnlWelcomeScreen.Visible = true;//True                                                                                      
                            pnlRegisterStaff.Visible = false;
                            pnlModifyStaffInfo.Visible = false;
                            pnlDeleteStaffInfo.Visible = false;
                            pnlDisplayStaffInfo.Visible = false;
                            pnlResetPassword.Visible = false;
                            pnlAddOrderInformation.Visible = false;
                            pnlModifyOrderInfo.Visible = false;
                            pnlDeleteOrderInfo.Visible = false;
                            pnlSearchDispOrderInfo.Visible = false;
                            //Weiquan
                            panelAddNewCategory.Visible = false;
                            panelModifyCategory.Visible = false;
                            panelDeleteCategory.Visible = false;
                            panelDisplayCategory.Visible = false;
                            panelAddNewProduct.Visible = false;
                            panelModifyProduct.Visible = false;
                            panelDeleteProduct.Visible = false;
                            panelDisplayProduct.Visible = false;
                            panelSelectAProductToDisplay.Visible = false;
                            //Jai
                            pnlAddNewSupplierInfo.Visible = false;
                            pnlModifySupplierInfo.Visible = false;
                            pnlDeleteSupplierInfo.Visible = false;
                            pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                            pnlDisplaySupplierInformation_Details.Visible = false;
                            pnlAddStock.Visible = false;
                            pnlEditStockInfo.Visible = false;
                            pnlDisplayComputeExcessSHrtfall.Visible = false;
                            pnlComputeExcessShortFall.Visible = false;
                            pnlSearchAndDisplayStock.Visible = false;
                        }
                        else
                        {
                            MessageBox.Show("Please set a new password for this staff account.", "Error!");
                        }
                    }
                }
            }
        }

        private void lblCancelResetPw_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel reset of staff password?", "Reset Staff Password", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                this.Text = "Welcome!";
                //Edmund
                pnlWelcomeScreen.Visible = true;//true
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
            }
        }

        private void addNewOrderInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchOrder frmSearchOrder = new FrmSearchOrder(allData, 1);
            frmSearchOrder.ShowDialog();

            if (frmSearchOrder.StopSearchDialog == false)
            {
                return;
            }

            if (allData.foundSearchEntry == false)
            {
                this.Text = "Add Order Information";
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = true;//true
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }
        }

        private void btnAddOrderInfo_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to add order information?", "Add Order Information", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                if (tbxAddOrderNameOfCust.Text != "" && tbxAddOrderRefNum.Text != "" && tbxAddOrderTotalPrice.Text != "" && tbxAddOrderCustDestinationAddress.Text != "")
                {
                    Order createOrder = new Order(tbxAddOrderNameOfCust.Text, tbxAddOrderCustDestinationAddress.Text, Convert.ToInt32(tbxAddOrderRefNum.Text.Trim()),
                       dtpDateOfAddOrder.Value.ToShortDateString(), Convert.ToDouble(tbxAddOrderTotalPrice.Text.Trim()));
                    allData.orderList.Add(createOrder);
                    MessageBox.Show("Order Reference No: " + createOrder.OrderRefNum + " has beeen created.", "Add Order Information");
                }

                //clear textboxes
                tbxAddOrderNameOfCust.Text = "";
                tbxAddOrderRefNum.Text = "";
                tbxAddOrderTotalPrice.Text = "";
                tbxAddOrderCustDestinationAddress.Text = "";

                //Edmund
                pnlWelcomeScreen.Visible = true;//True
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                tabCtrOrderInfo.SelectTab(tabPgAddOrderCustInfo);
            }
        }

        private void btnCancelAddOrderInfo_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel adding orders?", "Adding Order Information", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                this.Text = "Welcome";
                //Edmund
                pnlWelcomeScreen.Visible = true;//True
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }
        }

        private void modifyExistingOrderInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchOrder frmSearchOrder = new FrmSearchOrder(allData, 2);
            frmSearchOrder.ShowDialog();

            if (frmSearchOrder.StopSearchDialog == false)
            {
                return;
            }

            if (allData.foundSearchEntry == true)
            {
                this.Text = "Modify Order Information";
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = true;//true
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                ArrayList purchaseList = allData.orderList;
                for (int i = 0; i < purchaseList.Count; i++)
                {
                    Order purchase = (Order)purchaseList[i];
                    if (purchase.OrderRefNum == allData.orderRef)
                    {
                        lblModifyDispOrderRefNum.Text = Convert.ToString(purchase.OrderRefNum);
                        lblModifyDispDestinationAddress.Text = purchase.DestAddress;
                        lblModifyDisplayDateOfOrder.Text = Convert.ToString(purchase.OrderDate);
                        lblModifyDispCustomerName.Text = purchase.CustName;
                        lblModifyDisplayTotalPrice.Text = Convert.ToString(purchase.TotalPrice);
                    }
                }
            }
        }

        private void searchAndDisplayOrderInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchOrder frmSearchOrder = new FrmSearchOrder(allData, 3);
            frmSearchOrder.ShowDialog();

            if (frmSearchOrder.StopSearchDialog == false)
            {
                return;
            }

            if (allData.foundSearchEntry == true)
            {
                this.Text = "Delete Order Information";
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = true;//true
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }

            ArrayList purchaseList = allData.orderList;
            for (int i = 0; i < purchaseList.Count; i++)
            {
                Order purchase = (Order)purchaseList[i];
                if (purchase.OrderRefNum == allData.orderRef)
                {
                    lblDispDelORN.Text = Convert.ToString(purchase.OrderRefNum);
                    lblDispDelOrderDestAdd.Text = purchase.DestAddress;
                    lblDispDelOrderDate.Text = Convert.ToString(purchase.OrderDate);
                    lblDispDelCustName.Text = purchase.CustName;
                    lblDispDelTotalPrice.Text = Convert.ToString(purchase.TotalPrice);
                }
            }
        }

        private void delToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSearchOrder frmSearchOrder = new FrmSearchOrder(allData, 4);
            frmSearchOrder.ShowDialog();

            if (frmSearchOrder.StopSearchDialog == false)
            {
                return;
            }

            if (allData.foundSearchEntry == true)
            {
                this.Text = "Display Order Information";
                //Edmund
                pnlWelcomeScreen.Visible = false;
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = true;//true
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                label157.Text = Convert.ToString(allData.storeTempOrder.OrderRefNum);
                label156.Text = allData.storeTempOrder.CustName;
                label152.Text = allData.storeTempOrder.OrderDate;
                label153.Text = allData.storeTempOrder.DestAddress;
                label151.Text = Convert.ToString(allData.storeTempOrder.TotalPrice);
            }
        }

        private void btnModifyOrderInformation_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to continue modifying order information?", "Modify Order Information", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                ArrayList purchaseList = allData.orderList;
                for (int i = 0; i < purchaseList.Count; i++)
                {
                    Order purchase = (Order)purchaseList[i];
                    if (purchase.OrderRefNum == allData.orderRef)
                    {
                        break;
                    }
                    if (dtpModifyOrderDate.Value.ToShortDateString() != purchase.OrderDate)
                    {
                        purchase.OrderDate = dtpModifyOrderDate.Value.ToShortDateString();
                    }
                    if (tbxModifyDestinationAddress.Text != "" && tbxModifyDestinationAddress.Text != purchase.DestAddress)
                    {
                        purchase.DestAddress = tbxModifyDestinationAddress.Text;
                    }
                    if (tbxModifyOrderTotalPrice.Text != "" && Convert.ToDouble(tbxModifyOrderTotalPrice.Text) != purchase.TotalPrice)
                    {
                        purchase.TotalPrice = Convert.ToInt32(tbxModifyOrderTotalPrice.Text);
                    }
                }
                MessageBox.Show("Order information is successfully modified.", "Modify Order Information");
                tbxModifyOrderTotalPrice.Text = "";
                tbxModifyDestinationAddress.Text = "";

                this.Text = "Welcome!";
                pnlWelcomeScreen.Visible = true;//true
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }
        }

        private void btnCancelModifyOrderInformation_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel modifying staff information?", "Modify Order Information", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                this.Text = "Welcome!";
                pnlWelcomeScreen.Visible = true;//true
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;
                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }
        }

        private void btnDeleteOrderInfo_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to continue deleting order information?", "Delete Order Information", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                for (int i = 0; i < allData.orderList.Count; i++)
                {
                    if (allData.orderList[i] != null)
                    {
                        Order purchase = (Order)allData.orderList[i];
                        if (Convert.ToString(purchase.OrderRefNum) == lblDispDelORN.Text)
                        {
                            allData.orderList.RemoveAt(i);
                            MessageBox.Show("Order information has been deleted!", "Delete Order");
                        }
                    }
                }

                lblDispDelORN.Text = "";
                lblDispDelOrderDestAdd.Text = "";
                lblDispDelOrderDate.Text = "";
                lblDispDelCustName.Text = "";
                lblDispDelTotalPrice.Text = "";

                this.Text = "Welcome!";
                //Edmund
                pnlWelcomeScreen.Visible = true;//true
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;

                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }
        }

        private void btnCancelDeleteOrderInfo_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to cancel deleting?", "Delete Order Information", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                this.Text = "Welcome!";
                //Edmund
                pnlWelcomeScreen.Visible = true;//true
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;

                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;
            }
        }

        private void btnOKSearchDispOrderInfo_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to return to main window?", "Display Order Information", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                this.Text = "Welcome!";
                //Edmund
                pnlWelcomeScreen.Visible = true;//true
                pnlRegisterStaff.Visible = false;
                pnlModifyStaffInfo.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlDisplayStaffInfo.Visible = false;
                pnlResetPassword.Visible = false;
                pnlAddOrderInformation.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlDeleteOrderInfo.Visible = false;
                pnlSearchDispOrderInfo.Visible = false;
                //Weiquan
                panelAddNewCategory.Visible = false;
                panelModifyCategory.Visible = false;
                panelDeleteCategory.Visible = false;
                panelDisplayCategory.Visible = false;
                panelAddNewProduct.Visible = false;
                panelModifyProduct.Visible = false;
                panelDeleteProduct.Visible = false;
                panelDisplayProduct.Visible = false;
                panelSelectAProductToDisplay.Visible = false;

                //Jai
                pnlAddNewSupplierInfo.Visible = false;
                pnlModifySupplierInfo.Visible = false;
                pnlDeleteSupplierInfo.Visible = false;
                pnlDisplaySupplierInfo_ListOfCompanies.Visible = false;
                pnlDisplaySupplierInformation_Details.Visible = false;
                pnlAddStock.Visible = false;
                pnlEditStockInfo.Visible = false;
                pnlDisplayComputeExcessSHrtfall.Visible = false;
                pnlComputeExcessShortFall.Visible = false;
                pnlSearchAndDisplayStock.Visible = false;

                label157.Text = "";
                label156.Text = "";
                label152.Text = "";
                label153.Text = "";
                label151.Text = "";
            }
        }
    }         
}
