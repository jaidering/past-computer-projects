﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_2
{
    public partial class FrmSearchCategory : Form
    {
        private AllDataClass allData;
        private bool cancel = false;
        private string search;


        public FrmSearchCategory(AllDataClass adc, string search)
        {
            InitializeComponent();
            allData = adc;
            this.search = search;
            if (this.search == "Add Category")
            {
                this.Text = "Add New Category";
            }
            else if (this.search == "Modify Category")
            {
                this.Text = "Modify Existing Category";
            }

            else if (this.search == "Delete Category")
            {
                this.Text = "Delete Existing Category";
            }
            else
            {
                this.Text = "Display Existing Category Information";
            }
        }


        public bool Cancel
        {
            get
            {
                return cancel;
            }
            set
            {
                cancel = value;
            }
        }

        private void btnSearchCategory_Click(object sender, EventArgs e)
        {

            if (tbxSearchCategory.Text.Trim() != "")
            {
                if (search == "Add Category")//Add category
                {
                    if (searchForExistingCategory() == true)
                    {
                        allData.foundSearch = true;
                        MessageBox.Show("Category already exist! Please try again","Category Already Exist");
                    }
                    else
                    {
                        allData.foundSearch = false;
                        MessageBox.Show("Category not found. Proceed to add Category.","Category Not Found");
                        allData.searchCategoryName = tbxSearchCategory.Text.Trim();
                        cancel = true;
                        this.Close();
                    }

                }

                else if (search == "Modify Category" || search == "Delete Category" || search == "Display Category" )//Modify/Delete/Display
                {
                    if (searchForExistingCategory() == true)
                    {
                        allData.foundSearch = true;
                        if (search == "Modify Category")
                        {
                            MessageBox.Show("Category found. Proceed to modify Category", "Category Found");
                        }
                        else if (search == "Delete Category")
                        {
                            MessageBox.Show("Category found. Proceed to delete Category", "Category Found");
                        }
                        else
                        {
                            MessageBox.Show("Category found. Proceed to view Category", "Category Found");
                        }
                        
                        allData.searchCategoryName = tbxSearchCategory.Text.Trim();
                        cancel = true;
                        this.Close();
                    }

                    else
                    {
                        allData.foundSearch = false;
                        MessageBox.Show("Category not found!","Category Not Found");
                    }
                }
            }
            else
            {
                MessageBox.Show("Please enter the Category Name to search","Error In Searching");
            }
        }

        private void btnCancelSearchCategory_Click(object sender, EventArgs e)
        {
            cancel = false;
            this.Close();
        }

        public bool searchForExistingCategory()
        {
            bool checkifCategoryExist = false;//Check if Category exist

            for (int i = 0; i < allData.categoryList.Count; i++)
            {
                if (allData.categoryList[i] != null)
                {
                    Category categoryInArray = (Category)allData.categoryList[i];
                    if (categoryInArray.Name == tbxSearchCategory.Text.Trim())
                    {
                        checkifCategoryExist = true;
                        allData.storeTempCategory = (Category)allData.categoryList[i];
                        break; 
                    }
                    else
                    {
                        checkifCategoryExist = false;
                    }
                }
            }

            return checkifCategoryExist;
        }

    }
}
