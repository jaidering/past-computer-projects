﻿namespace Assignment_Phase_2
{
    partial class FrmSearchCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearchCategory));
            this.pnlSearchCategory = new System.Windows.Forms.Panel();
            this.btnCancelSearchCategory = new System.Windows.Forms.Button();
            this.btnSearchCategory = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbxSearchCategory = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pnlSearchCategory.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSearchCategory
            // 
            this.pnlSearchCategory.Controls.Add(this.btnCancelSearchCategory);
            this.pnlSearchCategory.Controls.Add(this.btnSearchCategory);
            this.pnlSearchCategory.Controls.Add(this.label5);
            this.pnlSearchCategory.Controls.Add(this.groupBox2);
            this.pnlSearchCategory.Controls.Add(this.label7);
            this.pnlSearchCategory.Controls.Add(this.pictureBox2);
            this.pnlSearchCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchCategory.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchCategory.Name = "pnlSearchCategory";
            this.pnlSearchCategory.Size = new System.Drawing.Size(384, 214);
            this.pnlSearchCategory.TabIndex = 2;
            // 
            // btnCancelSearchCategory
            // 
            this.btnCancelSearchCategory.Location = new System.Drawing.Point(297, 184);
            this.btnCancelSearchCategory.Name = "btnCancelSearchCategory";
            this.btnCancelSearchCategory.Size = new System.Drawing.Size(75, 23);
            this.btnCancelSearchCategory.TabIndex = 11;
            this.btnCancelSearchCategory.Text = "Cancel";
            this.btnCancelSearchCategory.UseVisualStyleBackColor = true;
            this.btnCancelSearchCategory.Click += new System.EventHandler(this.btnCancelSearchCategory_Click);
            // 
            // btnSearchCategory
            // 
            this.btnSearchCategory.Location = new System.Drawing.Point(202, 184);
            this.btnSearchCategory.Name = "btnSearchCategory";
            this.btnSearchCategory.Size = new System.Drawing.Size(75, 23);
            this.btnSearchCategory.TabIndex = 10;
            this.btnSearchCategory.Text = "Search";
            this.btnSearchCategory.UseVisualStyleBackColor = true;
            this.btnSearchCategory.Click += new System.EventHandler(this.btnSearchCategory_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(208, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Please enter Category\'s Name to search ...";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbxSearchCategory);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 107);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(360, 71);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search for Category";
            // 
            // tbxSearchCategory
            // 
            this.tbxSearchCategory.Location = new System.Drawing.Point(146, 29);
            this.tbxSearchCategory.Name = "tbxSearchCategory";
            this.tbxSearchCategory.Size = new System.Drawing.Size(165, 23);
            this.tbxSearchCategory.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(55, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Category Name:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(154, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(172, 23);
            this.label7.TabIndex = 7;
            this.label7.Text = "Search for Category";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 7);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(93, 77);
            this.pictureBox2.TabIndex = 46;
            this.pictureBox2.TabStop = false;
            // 
            // FrmSearchCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 214);
            this.Controls.Add(this.pnlSearchCategory);
            this.Name = "FrmSearchCategory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search Category";
            this.pnlSearchCategory.ResumeLayout(false);
            this.pnlSearchCategory.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearchCategory;
        private System.Windows.Forms.Button btnCancelSearchCategory;
        private System.Windows.Forms.Button btnSearchCategory;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbxSearchCategory;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}