﻿namespace Assignment_Phase_2
{
    partial class FrmSearchOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearchOrder));
            this.pnlSearchOrder = new System.Windows.Forms.Panel();
            this.lblSearchOrder = new System.Windows.Forms.Label();
            this.picBoxLogoPnlSearchOrder = new System.Windows.Forms.PictureBox();
            this.btnCancelSearchOrder = new System.Windows.Forms.Button();
            this.btnSearchOrder = new System.Windows.Forms.Button();
            this.lblSearchOrderInfo = new System.Windows.Forms.Label();
            this.grpSearchForOrder = new System.Windows.Forms.GroupBox();
            this.tbxSearchOrderRefNum = new System.Windows.Forms.TextBox();
            this.lblSearchOrderRefNum = new System.Windows.Forms.Label();
            this.pnlSearchDispOrder = new System.Windows.Forms.Panel();
            this.lblSearchAndDispOrder = new System.Windows.Forms.Label();
            this.picBxLogoPnlSearchDispOrder = new System.Windows.Forms.PictureBox();
            this.btnCancelSearchDispOrder = new System.Windows.Forms.Button();
            this.btnSearchDispOrder = new System.Windows.Forms.Button();
            this.lblSearchDispOrderInfo = new System.Windows.Forms.Label();
            this.grpSearchByOrder = new System.Windows.Forms.GroupBox();
            this.comboBxSearchOrderMode = new System.Windows.Forms.ComboBox();
            this.lblSearchOrderMode = new System.Windows.Forms.Label();
            this.tbxSearchByOrderMode = new System.Windows.Forms.TextBox();
            this.lblSearchByOrderMode = new System.Windows.Forms.Label();
            this.pnlSearchOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoPnlSearchOrder)).BeginInit();
            this.grpSearchForOrder.SuspendLayout();
            this.pnlSearchDispOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoPnlSearchDispOrder)).BeginInit();
            this.grpSearchByOrder.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSearchOrder
            // 
            this.pnlSearchOrder.Controls.Add(this.lblSearchOrder);
            this.pnlSearchOrder.Controls.Add(this.picBoxLogoPnlSearchOrder);
            this.pnlSearchOrder.Controls.Add(this.btnCancelSearchOrder);
            this.pnlSearchOrder.Controls.Add(this.btnSearchOrder);
            this.pnlSearchOrder.Controls.Add(this.lblSearchOrderInfo);
            this.pnlSearchOrder.Controls.Add(this.grpSearchForOrder);
            this.pnlSearchOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchOrder.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchOrder.Name = "pnlSearchOrder";
            this.pnlSearchOrder.Size = new System.Drawing.Size(384, 214);
            this.pnlSearchOrder.TabIndex = 46;
            // 
            // lblSearchOrder
            // 
            this.lblSearchOrder.AutoSize = true;
            this.lblSearchOrder.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchOrder.Location = new System.Drawing.Point(154, 33);
            this.lblSearchOrder.Name = "lblSearchOrder";
            this.lblSearchOrder.Size = new System.Drawing.Size(149, 23);
            this.lblSearchOrder.TabIndex = 45;
            this.lblSearchOrder.Text = "Search for Order";
            // 
            // picBoxLogoPnlSearchOrder
            // 
            this.picBoxLogoPnlSearchOrder.Image = ((System.Drawing.Image)(resources.GetObject("picBoxLogoPnlSearchOrder.Image")));
            this.picBoxLogoPnlSearchOrder.Location = new System.Drawing.Point(12, 8);
            this.picBoxLogoPnlSearchOrder.Name = "picBoxLogoPnlSearchOrder";
            this.picBoxLogoPnlSearchOrder.Size = new System.Drawing.Size(93, 77);
            this.picBoxLogoPnlSearchOrder.TabIndex = 44;
            this.picBoxLogoPnlSearchOrder.TabStop = false;
            // 
            // btnCancelSearchOrder
            // 
            this.btnCancelSearchOrder.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelSearchOrder.Location = new System.Drawing.Point(297, 181);
            this.btnCancelSearchOrder.Name = "btnCancelSearchOrder";
            this.btnCancelSearchOrder.Size = new System.Drawing.Size(75, 23);
            this.btnCancelSearchOrder.TabIndex = 3;
            this.btnCancelSearchOrder.Text = "Cancel";
            this.btnCancelSearchOrder.UseVisualStyleBackColor = true;
            this.btnCancelSearchOrder.Click += new System.EventHandler(this.btnCancelSearchOrder_Click);
            // 
            // btnSearchOrder
            // 
            this.btnSearchOrder.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchOrder.Location = new System.Drawing.Point(200, 181);
            this.btnSearchOrder.Name = "btnSearchOrder";
            this.btnSearchOrder.Size = new System.Drawing.Size(75, 23);
            this.btnSearchOrder.TabIndex = 2;
            this.btnSearchOrder.Text = "Search";
            this.btnSearchOrder.UseVisualStyleBackColor = true;
            this.btnSearchOrder.Click += new System.EventHandler(this.btnSearchOrder_Click);
            // 
            // lblSearchOrderInfo
            // 
            this.lblSearchOrderInfo.AutoSize = true;
            this.lblSearchOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchOrderInfo.Location = new System.Drawing.Point(9, 97);
            this.lblSearchOrderInfo.Name = "lblSearchOrderInfo";
            this.lblSearchOrderInfo.Size = new System.Drawing.Size(271, 15);
            this.lblSearchOrderInfo.TabIndex = 1;
            this.lblSearchOrderInfo.Text = "Please enter a Order Reference Number to search...";
            // 
            // grpSearchForOrder
            // 
            this.grpSearchForOrder.Controls.Add(this.tbxSearchOrderRefNum);
            this.grpSearchForOrder.Controls.Add(this.lblSearchOrderRefNum);
            this.grpSearchForOrder.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSearchForOrder.Location = new System.Drawing.Point(12, 115);
            this.grpSearchForOrder.Name = "grpSearchForOrder";
            this.grpSearchForOrder.Size = new System.Drawing.Size(360, 60);
            this.grpSearchForOrder.TabIndex = 0;
            this.grpSearchForOrder.TabStop = false;
            this.grpSearchForOrder.Text = "Search for Order";
            // 
            // tbxSearchOrderRefNum
            // 
            this.tbxSearchOrderRefNum.Location = new System.Drawing.Point(153, 25);
            this.tbxSearchOrderRefNum.Name = "tbxSearchOrderRefNum";
            this.tbxSearchOrderRefNum.Size = new System.Drawing.Size(184, 23);
            this.tbxSearchOrderRefNum.TabIndex = 3;
            // 
            // lblSearchOrderRefNum
            // 
            this.lblSearchOrderRefNum.AutoSize = true;
            this.lblSearchOrderRefNum.Location = new System.Drawing.Point(6, 28);
            this.lblSearchOrderRefNum.Name = "lblSearchOrderRefNum";
            this.lblSearchOrderRefNum.Size = new System.Drawing.Size(141, 15);
            this.lblSearchOrderRefNum.TabIndex = 2;
            this.lblSearchOrderRefNum.Text = "Order Reference Number:";
            // 
            // pnlSearchDispOrder
            // 
            this.pnlSearchDispOrder.Controls.Add(this.lblSearchAndDispOrder);
            this.pnlSearchDispOrder.Controls.Add(this.picBxLogoPnlSearchDispOrder);
            this.pnlSearchDispOrder.Controls.Add(this.btnCancelSearchDispOrder);
            this.pnlSearchDispOrder.Controls.Add(this.btnSearchDispOrder);
            this.pnlSearchDispOrder.Controls.Add(this.lblSearchDispOrderInfo);
            this.pnlSearchDispOrder.Controls.Add(this.grpSearchByOrder);
            this.pnlSearchDispOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchDispOrder.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchDispOrder.Name = "pnlSearchDispOrder";
            this.pnlSearchDispOrder.Size = new System.Drawing.Size(384, 214);
            this.pnlSearchDispOrder.TabIndex = 47;
            // 
            // lblSearchAndDispOrder
            // 
            this.lblSearchAndDispOrder.AutoSize = true;
            this.lblSearchAndDispOrder.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchAndDispOrder.Location = new System.Drawing.Point(154, 33);
            this.lblSearchAndDispOrder.Name = "lblSearchAndDispOrder";
            this.lblSearchAndDispOrder.Size = new System.Drawing.Size(197, 23);
            this.lblSearchAndDispOrder.TabIndex = 45;
            this.lblSearchAndDispOrder.Text = "Search && Display Order";
            // 
            // picBxLogoPnlSearchDispOrder
            // 
            this.picBxLogoPnlSearchDispOrder.Image = ((System.Drawing.Image)(resources.GetObject("picBxLogoPnlSearchDispOrder.Image")));
            this.picBxLogoPnlSearchDispOrder.Location = new System.Drawing.Point(12, 8);
            this.picBxLogoPnlSearchDispOrder.Name = "picBxLogoPnlSearchDispOrder";
            this.picBxLogoPnlSearchDispOrder.Size = new System.Drawing.Size(93, 77);
            this.picBxLogoPnlSearchDispOrder.TabIndex = 44;
            this.picBxLogoPnlSearchDispOrder.TabStop = false;
            // 
            // btnCancelSearchDispOrder
            // 
            this.btnCancelSearchDispOrder.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelSearchDispOrder.Location = new System.Drawing.Point(299, 187);
            this.btnCancelSearchDispOrder.Name = "btnCancelSearchDispOrder";
            this.btnCancelSearchDispOrder.Size = new System.Drawing.Size(75, 23);
            this.btnCancelSearchDispOrder.TabIndex = 7;
            this.btnCancelSearchDispOrder.Text = "Cancel";
            this.btnCancelSearchDispOrder.UseVisualStyleBackColor = true;
            this.btnCancelSearchDispOrder.Click += new System.EventHandler(this.btnCancelSearchDispOrder_Click);
            // 
            // btnSearchDispOrder
            // 
            this.btnSearchDispOrder.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchDispOrder.Location = new System.Drawing.Point(200, 187);
            this.btnSearchDispOrder.Name = "btnSearchDispOrder";
            this.btnSearchDispOrder.Size = new System.Drawing.Size(75, 23);
            this.btnSearchDispOrder.TabIndex = 6;
            this.btnSearchDispOrder.Text = "Search";
            this.btnSearchDispOrder.UseVisualStyleBackColor = true;
            this.btnSearchDispOrder.Click += new System.EventHandler(this.btnSearchDispOrder_Click);
            // 
            // lblSearchDispOrderInfo
            // 
            this.lblSearchDispOrderInfo.AutoSize = true;
            this.lblSearchDispOrderInfo.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchDispOrderInfo.Location = new System.Drawing.Point(11, 90);
            this.lblSearchDispOrderInfo.Name = "lblSearchDispOrderInfo";
            this.lblSearchDispOrderInfo.Size = new System.Drawing.Size(176, 15);
            this.lblSearchDispOrderInfo.TabIndex = 5;
            this.lblSearchDispOrderInfo.Text = "Please choose a mode of search...";
            // 
            // grpSearchByOrder
            // 
            this.grpSearchByOrder.Controls.Add(this.comboBxSearchOrderMode);
            this.grpSearchByOrder.Controls.Add(this.lblSearchOrderMode);
            this.grpSearchByOrder.Controls.Add(this.tbxSearchByOrderMode);
            this.grpSearchByOrder.Controls.Add(this.lblSearchByOrderMode);
            this.grpSearchByOrder.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSearchByOrder.Location = new System.Drawing.Point(12, 108);
            this.grpSearchByOrder.Name = "grpSearchByOrder";
            this.grpSearchByOrder.Size = new System.Drawing.Size(360, 74);
            this.grpSearchByOrder.TabIndex = 4;
            this.grpSearchByOrder.TabStop = false;
            this.grpSearchByOrder.Text = "Search for Order";
            // 
            // comboBxSearchOrderMode
            // 
            this.comboBxSearchOrderMode.FormattingEnabled = true;
            this.comboBxSearchOrderMode.Items.AddRange(new object[] {
            "Search by Order Reference Number only",
            "Search by Customer Name only",
            "Search by Destination City only"});
            this.comboBxSearchOrderMode.Location = new System.Drawing.Point(167, 14);
            this.comboBxSearchOrderMode.Name = "comboBxSearchOrderMode";
            this.comboBxSearchOrderMode.Size = new System.Drawing.Size(187, 23);
            this.comboBxSearchOrderMode.TabIndex = 5;
            // 
            // lblSearchOrderMode
            // 
            this.lblSearchOrderMode.AutoSize = true;
            this.lblSearchOrderMode.Location = new System.Drawing.Point(82, 23);
            this.lblSearchOrderMode.Name = "lblSearchOrderMode";
            this.lblSearchOrderMode.Size = new System.Drawing.Size(78, 15);
            this.lblSearchOrderMode.TabIndex = 4;
            this.lblSearchOrderMode.Text = "Search Mode:";
            // 
            // tbxSearchByOrderMode
            // 
            this.tbxSearchByOrderMode.Location = new System.Drawing.Point(167, 42);
            this.tbxSearchByOrderMode.Name = "tbxSearchByOrderMode";
            this.tbxSearchByOrderMode.Size = new System.Drawing.Size(187, 23);
            this.tbxSearchByOrderMode.TabIndex = 3;
            // 
            // lblSearchByOrderMode
            // 
            this.lblSearchByOrderMode.AutoSize = true;
            this.lblSearchByOrderMode.Location = new System.Drawing.Point(6, 45);
            this.lblSearchByOrderMode.Name = "lblSearchByOrderMode";
            this.lblSearchByOrderMode.Size = new System.Drawing.Size(155, 15);
            this.lblSearchByOrderMode.TabIndex = 2;
            this.lblSearchByOrderMode.Text = "Enter Selected Information :";
            // 
            // FrmSearchOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 214);
            this.Controls.Add(this.pnlSearchOrder);
            this.Controls.Add(this.pnlSearchDispOrder);
            this.Name = "FrmSearchOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search Order";
            this.pnlSearchOrder.ResumeLayout(false);
            this.pnlSearchOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogoPnlSearchOrder)).EndInit();
            this.grpSearchForOrder.ResumeLayout(false);
            this.grpSearchForOrder.PerformLayout();
            this.pnlSearchDispOrder.ResumeLayout(false);
            this.pnlSearchDispOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBxLogoPnlSearchDispOrder)).EndInit();
            this.grpSearchByOrder.ResumeLayout(false);
            this.grpSearchByOrder.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearchOrder;
        private System.Windows.Forms.Label lblSearchOrder;
        private System.Windows.Forms.PictureBox picBoxLogoPnlSearchOrder;
        private System.Windows.Forms.Button btnCancelSearchOrder;
        private System.Windows.Forms.Button btnSearchOrder;
        private System.Windows.Forms.Label lblSearchOrderInfo;
        private System.Windows.Forms.GroupBox grpSearchForOrder;
        private System.Windows.Forms.TextBox tbxSearchOrderRefNum;
        private System.Windows.Forms.Label lblSearchOrderRefNum;
        private System.Windows.Forms.Panel pnlSearchDispOrder;
        private System.Windows.Forms.Label lblSearchAndDispOrder;
        private System.Windows.Forms.PictureBox picBxLogoPnlSearchDispOrder;
        private System.Windows.Forms.Button btnCancelSearchDispOrder;
        private System.Windows.Forms.Button btnSearchDispOrder;
        private System.Windows.Forms.Label lblSearchDispOrderInfo;
        private System.Windows.Forms.GroupBox grpSearchByOrder;
        private System.Windows.Forms.ComboBox comboBxSearchOrderMode;
        private System.Windows.Forms.Label lblSearchOrderMode;
        private System.Windows.Forms.TextBox tbxSearchByOrderMode;
        private System.Windows.Forms.Label lblSearchByOrderMode;
    }
}