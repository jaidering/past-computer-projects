﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Assignment_Phase_2
{
    public class Staff
    {
        //attributes
        private string name;
        private string nric;
        private string dateOfBirth;
        private string gender;
        private int homeTel;
        private int mobileTel;
        private string address;
        private string password;
        private string role1;
        private string role2;

        //constructor
        public Staff(string name, string nric, string dateOfBirth, string gender, int homeTel, int mobileTel,
            string address, string password)
        {

            this.name = name;
            this.nric = nric;
            this.dateOfBirth = dateOfBirth;
            this.gender = gender;
            this.homeTel = homeTel;
            this.mobileTel = mobileTel;
            this.address = address;
            this.password = password;

        }

        //methods
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Nric
        {
            get
            {
                return nric;
            }
            set
            {
                nric = value;
            }
        }

        public string DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }
            set
            {
                dateOfBirth = value;
            }
        }

        public string Gender
        {
            get
            {
                return gender;
            }
            set
            {
                gender = value;
            }
        }

        public int HomeTel
        {
            get
            {
                return homeTel;
            }
            set
            {
                homeTel = value;
            }
        }

        public int MobileTel
        {
            get
            {
                return mobileTel;
            }
            set
            {
                mobileTel = value;
            }
        }

        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }


        public string Role1
        {
            get
            {
                return role1;
            }
            set
            {
                role1 = value;
            }
        }

        public string Role2
        {
            get
            {
                return role2;
            }
            set
            {
                role2 = value;
            }
        }

    }
}
