﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_2
{
    public partial class FrmSearchSupplier : Form
    {
        private AllDataClass allData;
        private int num;
        private bool cancel;

        public FrmSearchSupplier(AllDataClass adc, int i)
        {
            InitializeComponent();
            allData = adc;
            num = i;
        }

        private void btnSearchCompany_Search_Click(object sender, EventArgs e)
        {
            if (tbxSearchCompanyName.Text.Trim() != "")
            {

                if (num == 1)//Add Supplier
                {

                    if (searchForExistingCompany() == true)
                    {
                        allData.findSearch = true;
                        MessageBox.Show("The Company already Exists!", "Company Exists");
                    }
                    else
                    {
                        allData.findSearch = false;
                        allData.searchCompanyName = tbxSearchCompanyName.Text.Trim();
                        cancel = true;
                        this.Close();
                    }
                }
                else if (num == 2)//modify
                {
                    if (searchForExistingCompany())
                    {
                        allData.findSearch = true;
                        allData.searchCompanyName = tbxSearchCompanyName.Text;
                        cancel = true;
                        this.Close();
                    }

                    else
                    {
                        allData.findSearch = false;
                        MessageBox.Show("Supplier not found!");
                    }
                }
                else if (num == 3)//delete
                {
                    if (searchForExistingCompany() == true)
                    {
                        allData.findSearch = true;
                        allData.searchCompanyName = tbxSearchCompanyName.Text;
                        cancel = true;
                        this.Close();
                    }
                    else
                    {
                        allData.findSearch = false;
                        MessageBox.Show("Supplier not found!");
                    }
                }
            }
            else
            {
                MessageBox.Show("Please fill in the company name field.", "Application Error.");
            }
            tbxSearchCompanyName.Text = "";
        }

        public bool searchForExistingCompany()
        {
            bool checkIfCompanyExists = false;//check if categry exist
            for (int i = 0; i < allData.inventoryList.Count; i++)
            {
                if (allData.inventoryList[i] != null)
                {
                    Supplier supplierInArray = (Supplier)allData.inventoryList[i];
                    if (supplierInArray.CompanyName == tbxSearchCompanyName.Text)
                    {
                        checkIfCompanyExists = true;
                        allData.storeTempSupplier = (Supplier)allData.inventoryList[i];
                        break;
                    }

                }
            }

            return checkIfCompanyExists;
        }
        public bool Cancel
        {
            get { return cancel; }
            set { cancel = value; }
        }

        private void btnSearchCompany_Cancel_Click(object sender, EventArgs e)
        {
            cancel = false;
            this.Close();
        }


    }
}
