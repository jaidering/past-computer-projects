﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections;

namespace Assignment_Phase_2
{
    public class Category
    {
        //Attributes
        private string name;
        private string descrption;
        private ArrayList productList;

        //Constructor
        public Category(string name, string descrption)
        {
            this.name = name;
            this.descrption = descrption;
            productList = new ArrayList();
        }

        //Methods
        public string Name
        {
            get { return name; }
        }

        public string Descrption
        {
            get { return descrption; }
            set { descrption = value; }
        }

        public ArrayList ProductList
        {
            get { return productList; }
        }
    }
}
