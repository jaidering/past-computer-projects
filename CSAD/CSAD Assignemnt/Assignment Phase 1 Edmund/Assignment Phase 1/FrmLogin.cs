﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_1
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            FrmMainStaff frmStaff = new FrmMainStaff();
            if (tbxName.Text == "admin" && tbxPassword.Text == "pass123")
            {
                this.Hide();
                frmStaff.Show();
            }
            else if (tbxName.Text == "staff" && tbxPassword.Text == "pass123")
            {
                this.Hide();
                frmStaff.Show();
            }
            else
            {
                MessageBox.Show("Invalid Username or Password");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
