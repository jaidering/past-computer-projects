﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_1
{
    public partial class FrmMainStaff : Form
    {
        public FrmMainStaff()
        {
            InitializeComponent();
        }

        private void loginLogoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmLogin frmlogin = new FrmLogin();
            this.Close();
            frmlogin.Show();
        }

        private void registerNewStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Register New Staff";
            pnlSearchStaff.BringToFront();
            pnlWelcome.Visible = false;
            pnlRegisterStaff.Visible = false;
            pnlSearchStaff.Visible = true;
            pnlModifyStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (tbxStaffName.Text == "")
            {
                MessageBox.Show("Please enter a Staff Name.");
                return;
            }
            else if (tbxStaffName.Text == "admin")
            {
                MessageBox.Show("Staff is Found!");
                tbxStaffName.Text = "";
            }
            else
            {
                MessageBox.Show("Staff is not Found. Proceed to register.");
                pnlWelcome.Visible = false;
                pnlRegisterStaff.Visible = true;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlRegisterStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void btnCancelRegistration_Click(object sender, EventArgs e)
        {
            pnlRegisterStaff.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void modifyNewStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Modify existing Staff Information";
            pnlSearchModifyStaff.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = true;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void btnCancelModify_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void btnCancelSearch_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void btnSearchForStaff_Click(object sender, EventArgs e)
        {
            if (tbxSearchName.Text == "")
            {
                MessageBox.Show("Please enter a Staff Name.");
                return;
            }
            else if (tbxSearchName.Text == "admin")
            {
                MessageBox.Show("Staff Information is found! Proceed to make changes.");
                pnlModifyStaff.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = true;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
            else
            {
                MessageBox.Show("Staff information cannot be found.");
            }
        }

        private void btnCancelDisplay_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void searchForStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Search and Display Staff";
            pnlSearchAndDisplay.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = true;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void btnSearchDeleteStaff_Click(object sender, EventArgs e)
        {
            if (tbxSearchDeleteStaff.Text == "")
            {
                MessageBox.Show("Please enter a Staff Name.");
                return;
            }
            else if (tbxSearchDeleteStaff.Text == "staff")
            {
                MessageBox.Show("Staff Information is found! Proceed to delete existing staff information.");
                pnlDeleteStaffInfo.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlDeleteStaffInfo.Visible = true;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
            else
            {
                MessageBox.Show("Staff information cannot be found.");
            }
        }

        private void deleteExistingStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Delete Existing Staff Information";
            pnlSearchDeleteStaff.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = true;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void btnCancelSearchDelete_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void btnCancelDeleteStaff_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void btnDeleteStaff_Click(object sender, EventArgs e)
        {
            if (tbxDeleteStaff.Text == "")
            {
                MessageBox.Show("Please enter a Staff Name to delete staff information.");
                return;
            }
            else if (tbxDeleteStaff.Text == "staff")
            {
                MessageBox.Show("Deletion of existing Staff Information is successful.");
            }
            else
            {
                MessageBox.Show("Deletion of existing Staff Information was unsuccessful");
            }
        }

        private void btnSearchAddOrder_Click(object sender, EventArgs e)
        {
            if (tbxSearchAddOrder.Text == "PO001")
            {
                MessageBox.Show("Order Found!");
            }
            else if (tbxSearchAddOrder.Text == "")
            {
                MessageBox.Show("Please enter Order Reference Number in this format: 'POXXX'.");
            }
            else
            {
                MessageBox.Show("Order cannot be found. Proceed to Add New Order Information.");
                pnlAddNewOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = true;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
        }

        private void addNewOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Add New Order";
            pnlSearchAddOrder.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlSearchAddOrder.Visible = true;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void btnCancelAddOrder_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void btnCancelAddNewOrder_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (tbxSearchModifyORnum.Text == "PO001" || tbxSearchModifyORnum.Text == "PO002")
            {
                MessageBox.Show("Order Found! Proceed to Modify Order Inforamtion");
                pnlModifyOrderInfo.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = true;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
            else if (tbxSearchModifyORnum.Text == "")
            {
                MessageBox.Show("Please enter Order Reference Number in this format: 'POXXX'.");
            }
            else
            {
                MessageBox.Show("Order cannot be found. Proceed to Add New Order Information.");
                pnlAddNewOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = true;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void modifyOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Modify Order Information";
            pnlModifyOrder.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlSearchAddOrder.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = true;
            pnlDeleteOrder.Visible = false;
        }

        private void searchAndDisplayOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Search and Display Order Information";
            pnlSearchDisplayOrderInfo.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlSearchAddOrder.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = true;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void deleteOrderInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Delete Order Information";
            pnlDeleteSearchOrder.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlSearchAddOrder.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
            pnlDeleteSearchOrder.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (tbxSearchModifyORnum.Text == "PO001" || tbxSearchModifyORnum.Text == "PO002")
            {
                MessageBox.Show("Order Found! Proceed to Delete Order Information");
                pnlDeleteOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = true;
            }
            else if (tbxSearchModifyORnum.Text == "")
            {
                MessageBox.Show("Please enter Order Reference Number in this format: 'POXXX'.");
            }
            else
            {
                MessageBox.Show("Order cannot be found. Proceed to Add New Order Information.");
                pnlAddNewOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = true;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (tbxORNumSearch.Text == "PO001" || tbxORNumSearch.Text == "PO002")
            {
                MessageBox.Show("Order Found! Proceed to Delete Order Information");
                pnlDeleteOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = true;
            }
            else if (tbxSearchModifyORnum.Text == "")
            {
                MessageBox.Show("Please enter Order Reference Number in this format: 'POXXX'.");
            }
            else
            {
                MessageBox.Show("Order cannot be found. Proceed to Add New Order Information.");
                pnlAddNewOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = true;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }
    }
    }
