﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAddSupplier_CheckCompany_Click(object sender, EventArgs e)
        {
            string name = this.tbxAddSupplier_CheckCompanyName.Text;

            if (name.ToLowerInvariant() == "jai")
            {
                MessageBox.Show("Please fill in details");
            }
            else
                if (tbxAddSupplier_CheckCompanyName.Text == "Nano")
                {
                    MessageBox.Show("Company already exists");
                    tbxAddCompanyName.Text = tbxAddSupplier_CheckCompanyName.Text;
                    tbxAddSupplier_CheckCompanyName.Text = "";
                    pnlAddSupplier.BringToFront();
                }


                else
                {
                    tbxAddCompanyName.Text = tbxAddSupplier_CheckCompanyName.Text;
                    tbxAddSupplier_CheckCompanyName.Text = "";
                    pnlAddSupplier.BringToFront();
                }
        
            
        }

        private void SupplierMenu_AddSupplier_Click(object sender, EventArgs e)
        {
            pnlAdd_check.BringToFront();
        }

        private void SupplierMenu_ModifySupplier_Click(object sender, EventArgs e)
        {
            pnlModifySupplier_Check.BringToFront();
        }

        private void SupplierMenu_DeleteSupplier_Click(object sender, EventArgs e)
        {
            pnlDeleteSupplier_Check.BringToFront();
        }

        private void SupplierMenu_SearchSupplier_Click(object sender, EventArgs e)
        {
            pnlSearchSupplier_Check.BringToFront();
        }

        private void btnDeleteSupplier_Check_Search_Click(object sender, EventArgs e)
        {
            pnlDeleteSupplier_Info.BringToFront(); ;
        }

        private void btnModifySupplier_Check_Search_Click(object sender, EventArgs e)
        {
            pnlModifySupplier_Info.BringToFront();
        }

        private void btnSearchSupplier_Check_Search_Click(object sender, EventArgs e)
        {
            pnlSearchSupplier_Info.BringToFront();
        }

        private void menuStock_EditProductQty_Click(object sender, EventArgs e)
        {
            pnlStockSearchToEdit_Chck.BringToFront();
        }

        private void btnCheckProToDeleteExist_Click(object sender, EventArgs e)
        {
            pnlStockEdit_NotFound.BringToFront();
            //still got Stock Found
        }

        private void menuStock_SearchProductQty_Click(object sender, EventArgs e)
        {
            pnlSearchDisplayProductQty_Search.BringToFront();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            pnlStock_DisplaySearchedQty.BringToFront();
        }

        private void menuStock_computeExcessShortFall_Click(object sender, EventArgs e)
        {
            pnlExcessShortFall_Search.BringToFront();
        }

        

        private void btnStock_Found_Update_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Stock has been updated. Thank you.");
        }

        private void btnStock_NotFound_Add_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Stock has been added.");
        }

        private void btnStock_ExcessShortFall_Search_Click(object sender, EventArgs e)
        {
            pnlStockExcessShortFall_Display.BringToFront();
        }

       

        

        

        
    }
}
