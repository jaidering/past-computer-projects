﻿namespace Assignment_Phase_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.staffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_AddSupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_ModifySupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_DeleteSupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_SearchSupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStock_EditProductQty = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStock_SearchProductQty = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStock_computeExcessShortFall = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlAddSupplier = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.grpBoxAddSupplier = new System.Windows.Forms.GroupBox();
            this.btnAddSupplier_InfoCancel = new System.Windows.Forms.Button();
            this.tbxAddCityCountry = new System.Windows.Forms.TextBox();
            this.tbxAddPostalCode = new System.Windows.Forms.TextBox();
            this.tbxAddUnitNum = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbxAddFaxNum = new System.Windows.Forms.TextBox();
            this.tbxAddPhoneNum = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAddSubmit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tbxAddContactName = new System.Windows.Forms.TextBox();
            this.tbxAddStreetName = new System.Windows.Forms.TextBox();
            this.tbxAddHouseNum = new System.Windows.Forms.TextBox();
            this.tbxAddBlockNum = new System.Windows.Forms.TextBox();
            this.tbxAddCompanyName = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.pnlAdd_check = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbxAddSupplier_CheckCompanyName = new System.Windows.Forms.TextBox();
            this.lblCheckProNameToDelete = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAddSupplier_CheckCancel = new System.Windows.Forms.Button();
            this.btnAddSupplier_CheckCompany = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlModifySupplier_Check = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbxModifySupplier_Check_CompanyName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnModifySupplier_Check_Cancel = new System.Windows.Forms.Button();
            this.btnModifySupplier_Check_Search = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pnlDeleteSupplier_Check = new System.Windows.Forms.Panel();
            this.btnDeleteSupplier_Check_Cancel = new System.Windows.Forms.Button();
            this.btnDeleteSupplier_Check_Search = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbDeleteSupplier_Check_CompanyName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pnlSearchSupplier_Check = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbtnSearch_City = new System.Windows.Forms.RadioButton();
            this.rbtnSearch_ContactName = new System.Windows.Forms.RadioButton();
            this.rbtnSearch_CompanyName = new System.Windows.Forms.RadioButton();
            this.tbxSearch_SearchSupplier = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnSearchSupplier_Check_Cancel = new System.Windows.Forms.Button();
            this.btnSearchSupplier_Check_Search = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pnlDeleteSupplier_Info = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.pnlModifySupplier_Info = new System.Windows.Forms.Panel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label49 = new System.Windows.Forms.Label();
            this.btnModifySupplier_Cancel = new System.Windows.Forms.Button();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.btnModifySupplier_Update = new System.Windows.Forms.Button();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.pnlSearchSupplier_Info = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.pnlStockSearchToEdit_Chck = new System.Windows.Forms.Panel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.cbxStockEdit_SearchCompanyName = new System.Windows.Forms.ComboBox();
            this.lblProductName = new System.Windows.Forms.Label();
            this.cbxStockEdit_SearchModelName = new System.Windows.Forms.ComboBox();
            this.label65 = new System.Windows.Forms.Label();
            this.cbxStockEdit_SearchProductName = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.btnCancelProToDeleteExist = new System.Windows.Forms.Button();
            this.btnCheckProToDeleteExist = new System.Windows.Forms.Button();
            this.label64 = new System.Windows.Forms.Label();
            this.pnlStockEdit_NotFound = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label62 = new System.Windows.Forms.Label();
            this.tbxStockEdit_NotFound_CompanyName = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.tbxStockEdit_NotFound_StockQty = new System.Windows.Forms.TextBox();
            this.tbxStockEdit_NotFound_ModelName = new System.Windows.Forms.TextBox();
            this.tbxStockEdit_NotFound_ProductName = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.btnStock_NotFound_Add = new System.Windows.Forms.Button();
            this.pnlStockEdit_Found = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.btnStock_Found_Update = new System.Windows.Forms.Button();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label67 = new System.Windows.Forms.Label();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.pnlSearchDisplayProductQty_Search = new System.Windows.Forms.Panel();
            this.label79 = new System.Windows.Forms.Label();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.label80 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label81 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label73 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label76 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label78 = new System.Windows.Forms.Label();
            this.pnlStock_DisplaySearchedQty = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.pnlStockExcessShortFall_Display = new System.Windows.Forms.Panel();
            this.btnStockExcessShortFall_Cancel = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tbxExcessShortFall_Display_ExcessShortFall = new System.Windows.Forms.TextBox();
            this.tbxExcessShortFall_Display_TotalOrder = new System.Windows.Forms.TextBox();
            this.tbxExcessShortFall_Display_TotalQty = new System.Windows.Forms.TextBox();
            this.tbxExcessShortFall_Display_ProductNameModelName = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pnlExcessShortFall_Search = new System.Windows.Forms.Panel();
            this.btnStockExcessShortFall__SearchCancel = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.cbxStock_ComputeExcessShortFall_Search = new System.Windows.Forms.ComboBox();
            this.label88 = new System.Windows.Forms.Label();
            this.btnStock_ExcessShortFall_Search = new System.Windows.Forms.Button();
            this.label82 = new System.Windows.Forms.Label();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.menuStrip2.SuspendLayout();
            this.pnlAddSupplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.grpBoxAddSupplier.SuspendLayout();
            this.pnlAdd_check.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlModifySupplier_Check.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.pnlDeleteSupplier_Check.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.pnlSearchSupplier_Check.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.pnlDeleteSupplier_Info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.pnlModifySupplier_Info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.pnlSearchSupplier_Info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.pnlStockSearchToEdit_Chck.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.pnlStockEdit_NotFound.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.pnlStockEdit_Found.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.pnlSearchDisplayProductQty_Search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.groupBox11.SuspendLayout();
            this.pnlStock_DisplaySearchedQty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.groupBox12.SuspendLayout();
            this.pnlStockExcessShortFall_Display.SuspendLayout();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.pnlExcessShortFall_Search.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.staffToolStripMenuItem,
            this.catalogueToolStripMenuItem,
            this.orderToolStripMenuItem,
            this.orderToolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem7});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(584, 24);
            this.menuStrip2.TabIndex = 14;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // staffToolStripMenuItem
            // 
            this.staffToolStripMenuItem.Name = "staffToolStripMenuItem";
            this.staffToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.staffToolStripMenuItem.Text = "Staff";
            // 
            // catalogueToolStripMenuItem
            // 
            this.catalogueToolStripMenuItem.Name = "catalogueToolStripMenuItem";
            this.catalogueToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.catalogueToolStripMenuItem.Text = "Catalogue";
            // 
            // orderToolStripMenuItem
            // 
            this.orderToolStripMenuItem.Name = "orderToolStripMenuItem";
            this.orderToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.orderToolStripMenuItem.Text = "Product";
            // 
            // orderToolStripMenuItem1
            // 
            this.orderToolStripMenuItem1.Name = "orderToolStripMenuItem1";
            this.orderToolStripMenuItem1.Size = new System.Drawing.Size(49, 20);
            this.orderToolStripMenuItem1.Text = "Order";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SupplierMenu_AddSupplier,
            this.SupplierMenu_ModifySupplier,
            this.SupplierMenu_DeleteSupplier,
            this.SupplierMenu_SearchSupplier});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(69, 20);
            this.toolStripMenuItem2.Text = "Inventory";
            // 
            // SupplierMenu_AddSupplier
            // 
            this.SupplierMenu_AddSupplier.Name = "SupplierMenu_AddSupplier";
            this.SupplierMenu_AddSupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_AddSupplier.Text = "Add Supplier";
            this.SupplierMenu_AddSupplier.Click += new System.EventHandler(this.SupplierMenu_AddSupplier_Click);
            // 
            // SupplierMenu_ModifySupplier
            // 
            this.SupplierMenu_ModifySupplier.Name = "SupplierMenu_ModifySupplier";
            this.SupplierMenu_ModifySupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_ModifySupplier.Text = "Modify Supplier";
            this.SupplierMenu_ModifySupplier.Click += new System.EventHandler(this.SupplierMenu_ModifySupplier_Click);
            // 
            // SupplierMenu_DeleteSupplier
            // 
            this.SupplierMenu_DeleteSupplier.Name = "SupplierMenu_DeleteSupplier";
            this.SupplierMenu_DeleteSupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_DeleteSupplier.Text = "Delete Supplier";
            this.SupplierMenu_DeleteSupplier.Click += new System.EventHandler(this.SupplierMenu_DeleteSupplier_Click);
            // 
            // SupplierMenu_SearchSupplier
            // 
            this.SupplierMenu_SearchSupplier.Name = "SupplierMenu_SearchSupplier";
            this.SupplierMenu_SearchSupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_SearchSupplier.Text = "Search Supplier";
            this.SupplierMenu_SearchSupplier.Click += new System.EventHandler(this.SupplierMenu_SearchSupplier_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuStock_EditProductQty,
            this.menuStock_SearchProductQty,
            this.menuStock_computeExcessShortFall});
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(48, 20);
            this.toolStripMenuItem7.Text = "Stock";
            // 
            // menuStock_EditProductQty
            // 
            this.menuStock_EditProductQty.Name = "menuStock_EditProductQty";
            this.menuStock_EditProductQty.Size = new System.Drawing.Size(209, 22);
            this.menuStock_EditProductQty.Text = "Edit Product Qty";
            this.menuStock_EditProductQty.Click += new System.EventHandler(this.menuStock_EditProductQty_Click);
            // 
            // menuStock_SearchProductQty
            // 
            this.menuStock_SearchProductQty.Name = "menuStock_SearchProductQty";
            this.menuStock_SearchProductQty.Size = new System.Drawing.Size(209, 22);
            this.menuStock_SearchProductQty.Text = "Search Product Qty";
            this.menuStock_SearchProductQty.Click += new System.EventHandler(this.menuStock_SearchProductQty_Click);
            // 
            // menuStock_computeExcessShortFall
            // 
            this.menuStock_computeExcessShortFall.Name = "menuStock_computeExcessShortFall";
            this.menuStock_computeExcessShortFall.Size = new System.Drawing.Size(209, 22);
            this.menuStock_computeExcessShortFall.Text = "Compute Excess/Shortfall";
            this.menuStock_computeExcessShortFall.Click += new System.EventHandler(this.menuStock_computeExcessShortFall_Click);
            // 
            // pnlAddSupplier
            // 
            this.pnlAddSupplier.AutoScroll = true;
            this.pnlAddSupplier.Controls.Add(this.pictureBox2);
            this.pnlAddSupplier.Controls.Add(this.panel3);
            this.pnlAddSupplier.Controls.Add(this.grpBoxAddSupplier);
            this.pnlAddSupplier.Location = new System.Drawing.Point(0, 27);
            this.pnlAddSupplier.Name = "pnlAddSupplier";
            this.pnlAddSupplier.Size = new System.Drawing.Size(584, 421);
            this.pnlAddSupplier.TabIndex = 15;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(449, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 81);
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(550, 748);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 18);
            this.panel3.TabIndex = 19;
            // 
            // grpBoxAddSupplier
            // 
            this.grpBoxAddSupplier.Controls.Add(this.btnAddSupplier_InfoCancel);
            this.grpBoxAddSupplier.Controls.Add(this.tbxAddCityCountry);
            this.grpBoxAddSupplier.Controls.Add(this.tbxAddPostalCode);
            this.grpBoxAddSupplier.Controls.Add(this.tbxAddUnitNum);
            this.grpBoxAddSupplier.Controls.Add(this.label2);
            this.grpBoxAddSupplier.Controls.Add(this.label3);
            this.grpBoxAddSupplier.Controls.Add(this.label4);
            this.grpBoxAddSupplier.Controls.Add(this.tbxAddFaxNum);
            this.grpBoxAddSupplier.Controls.Add(this.tbxAddPhoneNum);
            this.grpBoxAddSupplier.Controls.Add(this.label5);
            this.grpBoxAddSupplier.Controls.Add(this.label6);
            this.grpBoxAddSupplier.Controls.Add(this.label7);
            this.grpBoxAddSupplier.Controls.Add(this.btnAddSubmit);
            this.grpBoxAddSupplier.Controls.Add(this.label8);
            this.grpBoxAddSupplier.Controls.Add(this.tbxAddContactName);
            this.grpBoxAddSupplier.Controls.Add(this.tbxAddStreetName);
            this.grpBoxAddSupplier.Controls.Add(this.tbxAddHouseNum);
            this.grpBoxAddSupplier.Controls.Add(this.tbxAddBlockNum);
            this.grpBoxAddSupplier.Controls.Add(this.tbxAddCompanyName);
            this.grpBoxAddSupplier.Controls.Add(this.label23);
            this.grpBoxAddSupplier.Controls.Add(this.label24);
            this.grpBoxAddSupplier.Controls.Add(this.label25);
            this.grpBoxAddSupplier.Controls.Add(this.label26);
            this.grpBoxAddSupplier.Controls.Add(this.label27);
            this.grpBoxAddSupplier.Controls.Add(this.label28);
            this.grpBoxAddSupplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxAddSupplier.Location = new System.Drawing.Point(25, 22);
            this.grpBoxAddSupplier.Name = "grpBoxAddSupplier";
            this.grpBoxAddSupplier.Size = new System.Drawing.Size(523, 722);
            this.grpBoxAddSupplier.TabIndex = 18;
            this.grpBoxAddSupplier.TabStop = false;
            this.grpBoxAddSupplier.Text = "Add Supplier";
            // 
            // btnAddSupplier_InfoCancel
            // 
            this.btnAddSupplier_InfoCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSupplier_InfoCancel.Location = new System.Drawing.Point(430, 678);
            this.btnAddSupplier_InfoCancel.Name = "btnAddSupplier_InfoCancel";
            this.btnAddSupplier_InfoCancel.Size = new System.Drawing.Size(75, 23);
            this.btnAddSupplier_InfoCancel.TabIndex = 56;
            this.btnAddSupplier_InfoCancel.Text = "Cancel";
            this.btnAddSupplier_InfoCancel.UseVisualStyleBackColor = true;
            // 
            // tbxAddCityCountry
            // 
            this.tbxAddCityCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddCityCountry.Location = new System.Drawing.Point(80, 508);
            this.tbxAddCityCountry.Name = "tbxAddCityCountry";
            this.tbxAddCityCountry.Size = new System.Drawing.Size(184, 20);
            this.tbxAddCityCountry.TabIndex = 55;
            // 
            // tbxAddPostalCode
            // 
            this.tbxAddPostalCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddPostalCode.Location = new System.Drawing.Point(80, 451);
            this.tbxAddPostalCode.Name = "tbxAddPostalCode";
            this.tbxAddPostalCode.Size = new System.Drawing.Size(184, 20);
            this.tbxAddPostalCode.TabIndex = 54;
            // 
            // tbxAddUnitNum
            // 
            this.tbxAddUnitNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddUnitNum.Location = new System.Drawing.Point(80, 399);
            this.tbxAddUnitNum.Name = "tbxAddUnitNum";
            this.tbxAddUnitNum.Size = new System.Drawing.Size(184, 20);
            this.tbxAddUnitNum.TabIndex = 53;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(77, 487);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 52;
            this.label2.Text = "* City, Country";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(77, 433);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "* Postal Code";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(77, 380);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 50;
            this.label4.Text = "* Unit Number";
            // 
            // tbxAddFaxNum
            // 
            this.tbxAddFaxNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddFaxNum.Location = new System.Drawing.Point(80, 644);
            this.tbxAddFaxNum.Name = "tbxAddFaxNum";
            this.tbxAddFaxNum.Size = new System.Drawing.Size(184, 20);
            this.tbxAddFaxNum.TabIndex = 49;
            // 
            // tbxAddPhoneNum
            // 
            this.tbxAddPhoneNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddPhoneNum.Location = new System.Drawing.Point(80, 587);
            this.tbxAddPhoneNum.Name = "tbxAddPhoneNum";
            this.tbxAddPhoneNum.Size = new System.Drawing.Size(184, 20);
            this.tbxAddPhoneNum.TabIndex = 48;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(77, 623);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "* Fax Number";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(77, 569);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 45;
            this.label6.Text = "* Phone Number";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(63, 539);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 15);
            this.label7.TabIndex = 44;
            this.label7.Text = "Contact";
            // 
            // btnAddSubmit
            // 
            this.btnAddSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSubmit.Location = new System.Drawing.Point(328, 678);
            this.btnAddSubmit.Name = "btnAddSubmit";
            this.btnAddSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnAddSubmit.TabIndex = 43;
            this.btnAddSubmit.Text = "Submit";
            this.btnAddSubmit.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(33, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "* Denotes mandatory fields";
            // 
            // tbxAddContactName
            // 
            this.tbxAddContactName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddContactName.Location = new System.Drawing.Point(80, 158);
            this.tbxAddContactName.Name = "tbxAddContactName";
            this.tbxAddContactName.Size = new System.Drawing.Size(184, 20);
            this.tbxAddContactName.TabIndex = 41;
            // 
            // tbxAddStreetName
            // 
            this.tbxAddStreetName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddStreetName.Location = new System.Drawing.Point(80, 350);
            this.tbxAddStreetName.Name = "tbxAddStreetName";
            this.tbxAddStreetName.Size = new System.Drawing.Size(184, 20);
            this.tbxAddStreetName.TabIndex = 40;
            // 
            // tbxAddHouseNum
            // 
            this.tbxAddHouseNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddHouseNum.Location = new System.Drawing.Point(80, 293);
            this.tbxAddHouseNum.Name = "tbxAddHouseNum";
            this.tbxAddHouseNum.Size = new System.Drawing.Size(184, 20);
            this.tbxAddHouseNum.TabIndex = 39;
            // 
            // tbxAddBlockNum
            // 
            this.tbxAddBlockNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddBlockNum.Location = new System.Drawing.Point(80, 241);
            this.tbxAddBlockNum.Name = "tbxAddBlockNum";
            this.tbxAddBlockNum.Size = new System.Drawing.Size(184, 20);
            this.tbxAddBlockNum.TabIndex = 38;
            // 
            // tbxAddCompanyName
            // 
            this.tbxAddCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddCompanyName.Location = new System.Drawing.Point(80, 106);
            this.tbxAddCompanyName.Name = "tbxAddCompanyName";
            this.tbxAddCompanyName.Size = new System.Drawing.Size(184, 20);
            this.tbxAddCompanyName.TabIndex = 36;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(77, 329);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(73, 13);
            this.label23.TabIndex = 35;
            this.label23.Text = "* Street Name";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(77, 275);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 13);
            this.label24.TabIndex = 34;
            this.label24.Text = "House Number";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(77, 222);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(74, 13);
            this.label25.TabIndex = 33;
            this.label25.Text = "Block Number";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(63, 194);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(58, 15);
            this.label26.TabIndex = 32;
            this.label26.Text = "Address";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(77, 139);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(82, 13);
            this.label27.TabIndex = 31;
            this.label27.Text = "* Contact Name";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(77, 85);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(89, 13);
            this.label28.TabIndex = 30;
            this.label28.Text = "* Company Name";
            // 
            // pnlAdd_check
            // 
            this.pnlAdd_check.Controls.Add(this.groupBox1);
            this.pnlAdd_check.Controls.Add(this.label1);
            this.pnlAdd_check.Controls.Add(this.pictureBox1);
            this.pnlAdd_check.Controls.Add(this.btnAddSupplier_CheckCancel);
            this.pnlAdd_check.Controls.Add(this.btnAddSupplier_CheckCompany);
            this.pnlAdd_check.Controls.Add(this.label9);
            this.pnlAdd_check.Location = new System.Drawing.Point(0, 28);
            this.pnlAdd_check.Name = "pnlAdd_check";
            this.pnlAdd_check.Size = new System.Drawing.Size(584, 419);
            this.pnlAdd_check.TabIndex = 16;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbxAddSupplier_CheckCompanyName);
            this.groupBox1.Controls.Add(this.lblCheckProNameToDelete);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(65, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 151);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Company Name";
            // 
            // tbxAddSupplier_CheckCompanyName
            // 
            this.tbxAddSupplier_CheckCompanyName.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tbxAddSupplier_CheckCompanyName.Location = new System.Drawing.Point(153, 51);
            this.tbxAddSupplier_CheckCompanyName.Name = "tbxAddSupplier_CheckCompanyName";
            this.tbxAddSupplier_CheckCompanyName.Size = new System.Drawing.Size(190, 22);
            this.tbxAddSupplier_CheckCompanyName.TabIndex = 15;
            // 
            // lblCheckProNameToDelete
            // 
            this.lblCheckProNameToDelete.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblCheckProNameToDelete.AutoSize = true;
            this.lblCheckProNameToDelete.Location = new System.Drawing.Point(38, 54);
            this.lblCheckProNameToDelete.Name = "lblCheckProNameToDelete";
            this.lblCheckProNameToDelete.Size = new System.Drawing.Size(112, 16);
            this.lblCheckProNameToDelete.TabIndex = 13;
            this.lblCheckProNameToDelete.Text = "Company Name :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(61, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 25);
            this.label1.TabIndex = 21;
            this.label1.Text = "Add Supplier";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(467, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 81);
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // btnAddSupplier_CheckCancel
            // 
            this.btnAddSupplier_CheckCancel.Location = new System.Drawing.Point(463, 362);
            this.btnAddSupplier_CheckCancel.Name = "btnAddSupplier_CheckCancel";
            this.btnAddSupplier_CheckCancel.Size = new System.Drawing.Size(75, 23);
            this.btnAddSupplier_CheckCancel.TabIndex = 19;
            this.btnAddSupplier_CheckCancel.Text = "Cancel";
            this.btnAddSupplier_CheckCancel.UseVisualStyleBackColor = true;
            // 
            // btnAddSupplier_CheckCompany
            // 
            this.btnAddSupplier_CheckCompany.Location = new System.Drawing.Point(349, 362);
            this.btnAddSupplier_CheckCompany.Name = "btnAddSupplier_CheckCompany";
            this.btnAddSupplier_CheckCompany.Size = new System.Drawing.Size(75, 23);
            this.btnAddSupplier_CheckCompany.TabIndex = 18;
            this.btnAddSupplier_CheckCompany.Text = "Search";
            this.btnAddSupplier_CheckCompany.UseVisualStyleBackColor = true;
            this.btnAddSupplier_CheckCompany.Click += new System.EventHandler(this.btnAddSupplier_CheckCompany_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(61, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(231, 16);
            this.label9.TabIndex = 17;
            this.label9.Text = "Please enter the following information:";
            // 
            // pnlModifySupplier_Check
            // 
            this.pnlModifySupplier_Check.Controls.Add(this.groupBox2);
            this.pnlModifySupplier_Check.Controls.Add(this.btnModifySupplier_Check_Cancel);
            this.pnlModifySupplier_Check.Controls.Add(this.btnModifySupplier_Check_Search);
            this.pnlModifySupplier_Check.Controls.Add(this.label11);
            this.pnlModifySupplier_Check.Controls.Add(this.label10);
            this.pnlModifySupplier_Check.Controls.Add(this.pictureBox3);
            this.pnlModifySupplier_Check.Location = new System.Drawing.Point(0, 23);
            this.pnlModifySupplier_Check.Name = "pnlModifySupplier_Check";
            this.pnlModifySupplier_Check.Size = new System.Drawing.Size(584, 424);
            this.pnlModifySupplier_Check.TabIndex = 17;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbxModifySupplier_Check_CompanyName);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(65, 160);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(398, 151);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search Company Name";
            // 
            // tbxModifySupplier_Check_CompanyName
            // 
            this.tbxModifySupplier_Check_CompanyName.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tbxModifySupplier_Check_CompanyName.Location = new System.Drawing.Point(153, 51);
            this.tbxModifySupplier_Check_CompanyName.Name = "tbxModifySupplier_Check_CompanyName";
            this.tbxModifySupplier_Check_CompanyName.Size = new System.Drawing.Size(190, 22);
            this.tbxModifySupplier_Check_CompanyName.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(38, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 16);
            this.label12.TabIndex = 13;
            this.label12.Text = "Company Name :";
            // 
            // btnModifySupplier_Check_Cancel
            // 
            this.btnModifySupplier_Check_Cancel.Location = new System.Drawing.Point(463, 367);
            this.btnModifySupplier_Check_Cancel.Name = "btnModifySupplier_Check_Cancel";
            this.btnModifySupplier_Check_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btnModifySupplier_Check_Cancel.TabIndex = 19;
            this.btnModifySupplier_Check_Cancel.Text = "Cancel";
            this.btnModifySupplier_Check_Cancel.UseVisualStyleBackColor = true;
            // 
            // btnModifySupplier_Check_Search
            // 
            this.btnModifySupplier_Check_Search.Location = new System.Drawing.Point(349, 367);
            this.btnModifySupplier_Check_Search.Name = "btnModifySupplier_Check_Search";
            this.btnModifySupplier_Check_Search.Size = new System.Drawing.Size(75, 23);
            this.btnModifySupplier_Check_Search.TabIndex = 18;
            this.btnModifySupplier_Check_Search.Text = "Search";
            this.btnModifySupplier_Check_Search.UseVisualStyleBackColor = true;
            this.btnModifySupplier_Check_Search.Click += new System.EventHandler(this.btnModifySupplier_Check_Search_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(61, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(231, 16);
            this.label11.TabIndex = 17;
            this.label11.Text = "Please enter the following information:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(61, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(161, 25);
            this.label10.TabIndex = 16;
            this.label10.Text = "Modify Supplier";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(467, 14);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 81);
            this.pictureBox3.TabIndex = 15;
            this.pictureBox3.TabStop = false;
            // 
            // pnlDeleteSupplier_Check
            // 
            this.pnlDeleteSupplier_Check.Controls.Add(this.btnDeleteSupplier_Check_Cancel);
            this.pnlDeleteSupplier_Check.Controls.Add(this.btnDeleteSupplier_Check_Search);
            this.pnlDeleteSupplier_Check.Controls.Add(this.groupBox3);
            this.pnlDeleteSupplier_Check.Controls.Add(this.label14);
            this.pnlDeleteSupplier_Check.Controls.Add(this.label13);
            this.pnlDeleteSupplier_Check.Controls.Add(this.pictureBox4);
            this.pnlDeleteSupplier_Check.Location = new System.Drawing.Point(0, 24);
            this.pnlDeleteSupplier_Check.Name = "pnlDeleteSupplier_Check";
            this.pnlDeleteSupplier_Check.Size = new System.Drawing.Size(584, 425);
            this.pnlDeleteSupplier_Check.TabIndex = 18;
            // 
            // btnDeleteSupplier_Check_Cancel
            // 
            this.btnDeleteSupplier_Check_Cancel.Location = new System.Drawing.Point(463, 366);
            this.btnDeleteSupplier_Check_Cancel.Name = "btnDeleteSupplier_Check_Cancel";
            this.btnDeleteSupplier_Check_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteSupplier_Check_Cancel.TabIndex = 23;
            this.btnDeleteSupplier_Check_Cancel.Text = "Cancel";
            this.btnDeleteSupplier_Check_Cancel.UseVisualStyleBackColor = true;
            // 
            // btnDeleteSupplier_Check_Search
            // 
            this.btnDeleteSupplier_Check_Search.Location = new System.Drawing.Point(349, 366);
            this.btnDeleteSupplier_Check_Search.Name = "btnDeleteSupplier_Check_Search";
            this.btnDeleteSupplier_Check_Search.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteSupplier_Check_Search.TabIndex = 22;
            this.btnDeleteSupplier_Check_Search.Text = "Search";
            this.btnDeleteSupplier_Check_Search.UseVisualStyleBackColor = true;
            this.btnDeleteSupplier_Check_Search.Click += new System.EventHandler(this.btnDeleteSupplier_Check_Search_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbDeleteSupplier_Check_CompanyName);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(65, 159);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(398, 138);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Search Company Name";
            // 
            // tbDeleteSupplier_Check_CompanyName
            // 
            this.tbDeleteSupplier_Check_CompanyName.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tbDeleteSupplier_Check_CompanyName.Location = new System.Drawing.Point(152, 51);
            this.tbDeleteSupplier_Check_CompanyName.Name = "tbDeleteSupplier_Check_CompanyName";
            this.tbDeleteSupplier_Check_CompanyName.Size = new System.Drawing.Size(190, 22);
            this.tbDeleteSupplier_Check_CompanyName.TabIndex = 15;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(38, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 16);
            this.label15.TabIndex = 13;
            this.label15.Text = "Company Name :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(62, 90);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(231, 16);
            this.label14.TabIndex = 20;
            this.label14.Text = "Please enter the following information:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(61, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(159, 25);
            this.label13.TabIndex = 19;
            this.label13.Text = "Delete Supplier";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(467, 13);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 81);
            this.pictureBox4.TabIndex = 16;
            this.pictureBox4.TabStop = false;
            // 
            // pnlSearchSupplier_Check
            // 
            this.pnlSearchSupplier_Check.Controls.Add(this.groupBox4);
            this.pnlSearchSupplier_Check.Controls.Add(this.btnSearchSupplier_Check_Cancel);
            this.pnlSearchSupplier_Check.Controls.Add(this.btnSearchSupplier_Check_Search);
            this.pnlSearchSupplier_Check.Controls.Add(this.label17);
            this.pnlSearchSupplier_Check.Controls.Add(this.label18);
            this.pnlSearchSupplier_Check.Controls.Add(this.pictureBox5);
            this.pnlSearchSupplier_Check.Location = new System.Drawing.Point(0, 23);
            this.pnlSearchSupplier_Check.Name = "pnlSearchSupplier_Check";
            this.pnlSearchSupplier_Check.Size = new System.Drawing.Size(584, 425);
            this.pnlSearchSupplier_Check.TabIndex = 19;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rbtnSearch_City);
            this.groupBox4.Controls.Add(this.rbtnSearch_ContactName);
            this.groupBox4.Controls.Add(this.rbtnSearch_CompanyName);
            this.groupBox4.Controls.Add(this.tbxSearch_SearchSupplier);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(65, 160);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(398, 151);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Search Company Name";
            // 
            // rbtnSearch_City
            // 
            this.rbtnSearch_City.AutoSize = true;
            this.rbtnSearch_City.Location = new System.Drawing.Point(65, 109);
            this.rbtnSearch_City.Name = "rbtnSearch_City";
            this.rbtnSearch_City.Size = new System.Drawing.Size(48, 20);
            this.rbtnSearch_City.TabIndex = 30;
            this.rbtnSearch_City.TabStop = true;
            this.rbtnSearch_City.Text = "City";
            this.rbtnSearch_City.UseVisualStyleBackColor = true;
            // 
            // rbtnSearch_ContactName
            // 
            this.rbtnSearch_ContactName.AutoSize = true;
            this.rbtnSearch_ContactName.Location = new System.Drawing.Point(65, 89);
            this.rbtnSearch_ContactName.Name = "rbtnSearch_ContactName";
            this.rbtnSearch_ContactName.Size = new System.Drawing.Size(111, 20);
            this.rbtnSearch_ContactName.TabIndex = 29;
            this.rbtnSearch_ContactName.TabStop = true;
            this.rbtnSearch_ContactName.Text = "Contact Name";
            this.rbtnSearch_ContactName.UseVisualStyleBackColor = true;
            // 
            // rbtnSearch_CompanyName
            // 
            this.rbtnSearch_CompanyName.AutoSize = true;
            this.rbtnSearch_CompanyName.Location = new System.Drawing.Point(65, 65);
            this.rbtnSearch_CompanyName.Name = "rbtnSearch_CompanyName";
            this.rbtnSearch_CompanyName.Size = new System.Drawing.Size(124, 20);
            this.rbtnSearch_CompanyName.TabIndex = 28;
            this.rbtnSearch_CompanyName.TabStop = true;
            this.rbtnSearch_CompanyName.Text = "Company Name";
            this.rbtnSearch_CompanyName.UseVisualStyleBackColor = true;
            // 
            // tbxSearch_SearchSupplier
            // 
            this.tbxSearch_SearchSupplier.Location = new System.Drawing.Point(222, 40);
            this.tbxSearch_SearchSupplier.Name = "tbxSearch_SearchSupplier";
            this.tbxSearch_SearchSupplier.Size = new System.Drawing.Size(165, 22);
            this.tbxSearch_SearchSupplier.TabIndex = 27;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 42);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(217, 16);
            this.label16.TabIndex = 26;
            this.label16.Text = "Please enter Only 1 of the following:";
            // 
            // btnSearchSupplier_Check_Cancel
            // 
            this.btnSearchSupplier_Check_Cancel.Location = new System.Drawing.Point(463, 367);
            this.btnSearchSupplier_Check_Cancel.Name = "btnSearchSupplier_Check_Cancel";
            this.btnSearchSupplier_Check_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btnSearchSupplier_Check_Cancel.TabIndex = 27;
            this.btnSearchSupplier_Check_Cancel.Text = "Cancel";
            this.btnSearchSupplier_Check_Cancel.UseVisualStyleBackColor = true;
            // 
            // btnSearchSupplier_Check_Search
            // 
            this.btnSearchSupplier_Check_Search.Location = new System.Drawing.Point(349, 367);
            this.btnSearchSupplier_Check_Search.Name = "btnSearchSupplier_Check_Search";
            this.btnSearchSupplier_Check_Search.Size = new System.Drawing.Size(75, 23);
            this.btnSearchSupplier_Check_Search.TabIndex = 26;
            this.btnSearchSupplier_Check_Search.Text = "Search";
            this.btnSearchSupplier_Check_Search.UseVisualStyleBackColor = true;
            this.btnSearchSupplier_Check_Search.Click += new System.EventHandler(this.btnSearchSupplier_Check_Search_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(61, 91);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(231, 16);
            this.label17.TabIndex = 25;
            this.label17.Text = "Please enter the following information:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(61, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(165, 25);
            this.label18.TabIndex = 24;
            this.label18.Text = "Search Supplier";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(467, 14);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(100, 81);
            this.pictureBox5.TabIndex = 17;
            this.pictureBox5.TabStop = false;
            // 
            // pnlDeleteSupplier_Info
            // 
            this.pnlDeleteSupplier_Info.AutoScroll = true;
            this.pnlDeleteSupplier_Info.Controls.Add(this.pictureBox6);
            this.pnlDeleteSupplier_Info.Controls.Add(this.groupBox5);
            this.pnlDeleteSupplier_Info.Location = new System.Drawing.Point(0, 22);
            this.pnlDeleteSupplier_Info.Name = "pnlDeleteSupplier_Info";
            this.pnlDeleteSupplier_Info.Size = new System.Drawing.Size(584, 424);
            this.pnlDeleteSupplier_Info.TabIndex = 24;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(448, 11);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(100, 81);
            this.pictureBox6.TabIndex = 23;
            this.pictureBox6.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(this.textBox3);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.textBox4);
            this.groupBox5.Controls.Add(this.textBox5);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Controls.Add(this.textBox6);
            this.groupBox5.Controls.Add(this.textBox7);
            this.groupBox5.Controls.Add(this.textBox8);
            this.groupBox5.Controls.Add(this.textBox9);
            this.groupBox5.Controls.Add(this.textBox10);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(24, 23);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(523, 722);
            this.groupBox5.TabIndex = 22;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Delete Supplier";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(406, 664);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 56;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(56, 494);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(184, 20);
            this.textBox1.TabIndex = 55;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(56, 437);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(184, 20);
            this.textBox2.TabIndex = 54;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(56, 385);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(184, 20);
            this.textBox3.TabIndex = 53;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(53, 473);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 13);
            this.label19.TabIndex = 52;
            this.label19.Text = "City, Country";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(53, 419);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 13);
            this.label20.TabIndex = 51;
            this.label20.Text = "Postal Code";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(53, 366);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(66, 13);
            this.label21.TabIndex = 50;
            this.label21.Text = "Unit Number";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(56, 630);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(184, 20);
            this.textBox4.TabIndex = 49;
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(56, 573);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(184, 20);
            this.textBox5.TabIndex = 48;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(53, 609);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(64, 13);
            this.label22.TabIndex = 46;
            this.label22.Text = "Fax Number";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(53, 555);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(78, 13);
            this.label29.TabIndex = 45;
            this.label29.Text = "Phone Number";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(39, 525);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(55, 15);
            this.label30.TabIndex = 44;
            this.label30.Text = "Contact";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(304, 664);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 43;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(56, 144);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(184, 20);
            this.textBox6.TabIndex = 41;
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(56, 336);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(184, 20);
            this.textBox7.TabIndex = 40;
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(56, 279);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(184, 20);
            this.textBox8.TabIndex = 39;
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.Location = new System.Drawing.Point(56, 227);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(184, 20);
            this.textBox9.TabIndex = 38;
            // 
            // textBox10
            // 
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.Location = new System.Drawing.Point(56, 92);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(184, 20);
            this.textBox10.TabIndex = 36;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(53, 315);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(66, 13);
            this.label32.TabIndex = 35;
            this.label32.Text = "Street Name";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(53, 261);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(78, 13);
            this.label33.TabIndex = 34;
            this.label33.Text = "House Number";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(53, 208);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(74, 13);
            this.label34.TabIndex = 33;
            this.label34.Text = "Block Number";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(39, 180);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(58, 15);
            this.label35.TabIndex = 32;
            this.label35.Text = "Address";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(53, 125);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(75, 13);
            this.label36.TabIndex = 31;
            this.label36.Text = "Contact Name";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(53, 71);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(82, 13);
            this.label37.TabIndex = 30;
            this.label37.Text = "Company Name";
            // 
            // pnlModifySupplier_Info
            // 
            this.pnlModifySupplier_Info.AutoScroll = true;
            this.pnlModifySupplier_Info.Controls.Add(this.pictureBox7);
            this.pnlModifySupplier_Info.Controls.Add(this.groupBox6);
            this.pnlModifySupplier_Info.Location = new System.Drawing.Point(0, 24);
            this.pnlModifySupplier_Info.Name = "pnlModifySupplier_Info";
            this.pnlModifySupplier_Info.Size = new System.Drawing.Size(584, 423);
            this.pnlModifySupplier_Info.TabIndex = 25;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(453, 11);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(100, 81);
            this.pictureBox7.TabIndex = 25;
            this.pictureBox7.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label49);
            this.groupBox6.Controls.Add(this.btnModifySupplier_Cancel);
            this.groupBox6.Controls.Add(this.textBox11);
            this.groupBox6.Controls.Add(this.textBox12);
            this.groupBox6.Controls.Add(this.textBox13);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this.label39);
            this.groupBox6.Controls.Add(this.textBox14);
            this.groupBox6.Controls.Add(this.textBox15);
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.label41);
            this.groupBox6.Controls.Add(this.label42);
            this.groupBox6.Controls.Add(this.btnModifySupplier_Update);
            this.groupBox6.Controls.Add(this.textBox16);
            this.groupBox6.Controls.Add(this.textBox17);
            this.groupBox6.Controls.Add(this.textBox18);
            this.groupBox6.Controls.Add(this.textBox19);
            this.groupBox6.Controls.Add(this.textBox20);
            this.groupBox6.Controls.Add(this.label43);
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this.label45);
            this.groupBox6.Controls.Add(this.label46);
            this.groupBox6.Controls.Add(this.label47);
            this.groupBox6.Controls.Add(this.label48);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(29, 23);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(523, 722);
            this.groupBox6.TabIndex = 24;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Modify Supplier";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(44, 45);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(137, 13);
            this.label49.TabIndex = 57;
            this.label49.Text = "* Denotes Mandatory Fields";
            // 
            // btnModifySupplier_Cancel
            // 
            this.btnModifySupplier_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifySupplier_Cancel.Location = new System.Drawing.Point(424, 669);
            this.btnModifySupplier_Cancel.Name = "btnModifySupplier_Cancel";
            this.btnModifySupplier_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btnModifySupplier_Cancel.TabIndex = 56;
            this.btnModifySupplier_Cancel.Text = "Cancel";
            this.btnModifySupplier_Cancel.UseVisualStyleBackColor = true;
            // 
            // textBox11
            // 
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.Location = new System.Drawing.Point(74, 499);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(184, 20);
            this.textBox11.TabIndex = 55;
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.Location = new System.Drawing.Point(74, 442);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(184, 20);
            this.textBox12.TabIndex = 54;
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.Location = new System.Drawing.Point(74, 390);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(184, 20);
            this.textBox13.TabIndex = 53;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(71, 478);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(73, 13);
            this.label31.TabIndex = 52;
            this.label31.Text = "* City, Country";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(71, 424);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(71, 13);
            this.label38.TabIndex = 51;
            this.label38.Text = "* Postal Code";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(71, 371);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(73, 13);
            this.label39.TabIndex = 50;
            this.label39.Text = "* Unit Number";
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.Location = new System.Drawing.Point(74, 635);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(184, 20);
            this.textBox14.TabIndex = 49;
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.Location = new System.Drawing.Point(74, 578);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(184, 20);
            this.textBox15.TabIndex = 48;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(71, 614);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(71, 13);
            this.label40.TabIndex = 46;
            this.label40.Text = "* Fax Number";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(71, 560);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(85, 13);
            this.label41.TabIndex = 45;
            this.label41.Text = "* Phone Number";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(57, 530);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(55, 15);
            this.label42.TabIndex = 44;
            this.label42.Text = "Contact";
            // 
            // btnModifySupplier_Update
            // 
            this.btnModifySupplier_Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifySupplier_Update.Location = new System.Drawing.Point(322, 669);
            this.btnModifySupplier_Update.Name = "btnModifySupplier_Update";
            this.btnModifySupplier_Update.Size = new System.Drawing.Size(75, 23);
            this.btnModifySupplier_Update.TabIndex = 43;
            this.btnModifySupplier_Update.Text = "Update";
            this.btnModifySupplier_Update.UseVisualStyleBackColor = true;
            // 
            // textBox16
            // 
            this.textBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.Location = new System.Drawing.Point(74, 149);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(184, 20);
            this.textBox16.TabIndex = 41;
            // 
            // textBox17
            // 
            this.textBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox17.Location = new System.Drawing.Point(74, 341);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(184, 20);
            this.textBox17.TabIndex = 40;
            // 
            // textBox18
            // 
            this.textBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.Location = new System.Drawing.Point(74, 284);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(184, 20);
            this.textBox18.TabIndex = 39;
            // 
            // textBox19
            // 
            this.textBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox19.Location = new System.Drawing.Point(74, 232);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(184, 20);
            this.textBox19.TabIndex = 38;
            // 
            // textBox20
            // 
            this.textBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox20.Location = new System.Drawing.Point(74, 97);
            this.textBox20.Name = "textBox20";
            this.textBox20.ReadOnly = true;
            this.textBox20.Size = new System.Drawing.Size(184, 20);
            this.textBox20.TabIndex = 36;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(71, 320);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(73, 13);
            this.label43.TabIndex = 35;
            this.label43.Text = "* Street Name";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(71, 266);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(78, 13);
            this.label44.TabIndex = 34;
            this.label44.Text = "House Number";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(71, 213);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(74, 13);
            this.label45.TabIndex = 33;
            this.label45.Text = "Block Number";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(57, 185);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(58, 15);
            this.label46.TabIndex = 32;
            this.label46.Text = "Address";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(71, 130);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(82, 13);
            this.label47.TabIndex = 31;
            this.label47.Text = "* Contact Name";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(71, 76);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(82, 13);
            this.label48.TabIndex = 30;
            this.label48.Text = "Company Name";
            // 
            // pnlSearchSupplier_Info
            // 
            this.pnlSearchSupplier_Info.AutoScroll = true;
            this.pnlSearchSupplier_Info.Controls.Add(this.pictureBox8);
            this.pnlSearchSupplier_Info.Controls.Add(this.groupBox7);
            this.pnlSearchSupplier_Info.Location = new System.Drawing.Point(0, 21);
            this.pnlSearchSupplier_Info.Name = "pnlSearchSupplier_Info";
            this.pnlSearchSupplier_Info.Size = new System.Drawing.Size(584, 424);
            this.pnlSearchSupplier_Info.TabIndex = 26;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(447, 13);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(100, 81);
            this.pictureBox8.TabIndex = 25;
            this.pictureBox8.TabStop = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button3);
            this.groupBox7.Controls.Add(this.textBox21);
            this.groupBox7.Controls.Add(this.textBox22);
            this.groupBox7.Controls.Add(this.textBox23);
            this.groupBox7.Controls.Add(this.label50);
            this.groupBox7.Controls.Add(this.label51);
            this.groupBox7.Controls.Add(this.label52);
            this.groupBox7.Controls.Add(this.textBox24);
            this.groupBox7.Controls.Add(this.textBox25);
            this.groupBox7.Controls.Add(this.label53);
            this.groupBox7.Controls.Add(this.label54);
            this.groupBox7.Controls.Add(this.label55);
            this.groupBox7.Controls.Add(this.textBox26);
            this.groupBox7.Controls.Add(this.textBox27);
            this.groupBox7.Controls.Add(this.textBox28);
            this.groupBox7.Controls.Add(this.textBox29);
            this.groupBox7.Controls.Add(this.textBox30);
            this.groupBox7.Controls.Add(this.label56);
            this.groupBox7.Controls.Add(this.label57);
            this.groupBox7.Controls.Add(this.label58);
            this.groupBox7.Controls.Add(this.label59);
            this.groupBox7.Controls.Add(this.label60);
            this.groupBox7.Controls.Add(this.label61);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(23, 25);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(523, 699);
            this.groupBox7.TabIndex = 24;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Searched Supplier";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(406, 658);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 56;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox21
            // 
            this.textBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox21.Location = new System.Drawing.Point(56, 479);
            this.textBox21.Name = "textBox21";
            this.textBox21.ReadOnly = true;
            this.textBox21.Size = new System.Drawing.Size(184, 20);
            this.textBox21.TabIndex = 55;
            // 
            // textBox22
            // 
            this.textBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox22.Location = new System.Drawing.Point(56, 422);
            this.textBox22.Name = "textBox22";
            this.textBox22.ReadOnly = true;
            this.textBox22.Size = new System.Drawing.Size(184, 20);
            this.textBox22.TabIndex = 54;
            // 
            // textBox23
            // 
            this.textBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox23.Location = new System.Drawing.Point(56, 370);
            this.textBox23.Name = "textBox23";
            this.textBox23.ReadOnly = true;
            this.textBox23.Size = new System.Drawing.Size(184, 20);
            this.textBox23.TabIndex = 53;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(53, 458);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(66, 13);
            this.label50.TabIndex = 52;
            this.label50.Text = "City, Country";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(53, 404);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(64, 13);
            this.label51.TabIndex = 51;
            this.label51.Text = "Postal Code";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(53, 351);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(66, 13);
            this.label52.TabIndex = 50;
            this.label52.Text = "Unit Number";
            // 
            // textBox24
            // 
            this.textBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox24.Location = new System.Drawing.Point(56, 615);
            this.textBox24.Name = "textBox24";
            this.textBox24.ReadOnly = true;
            this.textBox24.Size = new System.Drawing.Size(184, 20);
            this.textBox24.TabIndex = 49;
            // 
            // textBox25
            // 
            this.textBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox25.Location = new System.Drawing.Point(56, 558);
            this.textBox25.Name = "textBox25";
            this.textBox25.ReadOnly = true;
            this.textBox25.Size = new System.Drawing.Size(184, 20);
            this.textBox25.TabIndex = 48;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(53, 594);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(64, 13);
            this.label53.TabIndex = 46;
            this.label53.Text = "Fax Number";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(53, 540);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(78, 13);
            this.label54.TabIndex = 45;
            this.label54.Text = "Phone Number";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(39, 510);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(55, 15);
            this.label55.TabIndex = 44;
            this.label55.Text = "Contact";
            // 
            // textBox26
            // 
            this.textBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox26.Location = new System.Drawing.Point(56, 129);
            this.textBox26.Name = "textBox26";
            this.textBox26.ReadOnly = true;
            this.textBox26.Size = new System.Drawing.Size(184, 20);
            this.textBox26.TabIndex = 41;
            // 
            // textBox27
            // 
            this.textBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox27.Location = new System.Drawing.Point(56, 321);
            this.textBox27.Name = "textBox27";
            this.textBox27.ReadOnly = true;
            this.textBox27.Size = new System.Drawing.Size(184, 20);
            this.textBox27.TabIndex = 40;
            // 
            // textBox28
            // 
            this.textBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox28.Location = new System.Drawing.Point(56, 264);
            this.textBox28.Name = "textBox28";
            this.textBox28.ReadOnly = true;
            this.textBox28.Size = new System.Drawing.Size(184, 20);
            this.textBox28.TabIndex = 39;
            // 
            // textBox29
            // 
            this.textBox29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox29.Location = new System.Drawing.Point(56, 212);
            this.textBox29.Name = "textBox29";
            this.textBox29.ReadOnly = true;
            this.textBox29.Size = new System.Drawing.Size(184, 20);
            this.textBox29.TabIndex = 38;
            // 
            // textBox30
            // 
            this.textBox30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox30.Location = new System.Drawing.Point(56, 77);
            this.textBox30.Name = "textBox30";
            this.textBox30.ReadOnly = true;
            this.textBox30.Size = new System.Drawing.Size(184, 20);
            this.textBox30.TabIndex = 36;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(53, 300);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(66, 13);
            this.label56.TabIndex = 35;
            this.label56.Text = "Street Name";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(53, 246);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(78, 13);
            this.label57.TabIndex = 34;
            this.label57.Text = "House Number";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(53, 193);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(74, 13);
            this.label58.TabIndex = 33;
            this.label58.Text = "Block Number";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(39, 165);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(58, 15);
            this.label59.TabIndex = 32;
            this.label59.Text = "Address";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(53, 110);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(75, 13);
            this.label60.TabIndex = 31;
            this.label60.Text = "Contact Name";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(53, 56);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(82, 13);
            this.label61.TabIndex = 30;
            this.label61.Text = "Company Name";
            // 
            // pnlStockSearchToEdit_Chck
            // 
            this.pnlStockSearchToEdit_Chck.Controls.Add(this.groupBox8);
            this.pnlStockSearchToEdit_Chck.Controls.Add(this.label63);
            this.pnlStockSearchToEdit_Chck.Controls.Add(this.pictureBox9);
            this.pnlStockSearchToEdit_Chck.Controls.Add(this.btnCancelProToDeleteExist);
            this.pnlStockSearchToEdit_Chck.Controls.Add(this.btnCheckProToDeleteExist);
            this.pnlStockSearchToEdit_Chck.Controls.Add(this.label64);
            this.pnlStockSearchToEdit_Chck.Location = new System.Drawing.Point(0, 24);
            this.pnlStockSearchToEdit_Chck.Name = "pnlStockSearchToEdit_Chck";
            this.pnlStockSearchToEdit_Chck.Size = new System.Drawing.Size(584, 425);
            this.pnlStockSearchToEdit_Chck.TabIndex = 27;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.cbxStockEdit_SearchCompanyName);
            this.groupBox8.Controls.Add(this.lblProductName);
            this.groupBox8.Controls.Add(this.cbxStockEdit_SearchModelName);
            this.groupBox8.Controls.Add(this.label65);
            this.groupBox8.Controls.Add(this.cbxStockEdit_SearchProductName);
            this.groupBox8.Controls.Add(this.label66);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(65, 143);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(398, 188);
            this.groupBox8.TabIndex = 22;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Search Product";
            // 
            // cbxStockEdit_SearchCompanyName
            // 
            this.cbxStockEdit_SearchCompanyName.FormattingEnabled = true;
            this.cbxStockEdit_SearchCompanyName.Items.AddRange(new object[] {
            "Nano"});
            this.cbxStockEdit_SearchCompanyName.Location = new System.Drawing.Point(213, 115);
            this.cbxStockEdit_SearchCompanyName.Name = "cbxStockEdit_SearchCompanyName";
            this.cbxStockEdit_SearchCompanyName.Size = new System.Drawing.Size(165, 24);
            this.cbxStockEdit_SearchCompanyName.TabIndex = 48;
            // 
            // lblProductName
            // 
            this.lblProductName.AutoSize = true;
            this.lblProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductName.Location = new System.Drawing.Point(77, 61);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(100, 16);
            this.lblProductName.TabIndex = 43;
            this.lblProductName.Text = "Product Name :";
            // 
            // cbxStockEdit_SearchModelName
            // 
            this.cbxStockEdit_SearchModelName.FormattingEnabled = true;
            this.cbxStockEdit_SearchModelName.Items.AddRange(new object[] {
            "Sony-123"});
            this.cbxStockEdit_SearchModelName.Location = new System.Drawing.Point(213, 85);
            this.cbxStockEdit_SearchModelName.Name = "cbxStockEdit_SearchModelName";
            this.cbxStockEdit_SearchModelName.Size = new System.Drawing.Size(166, 24);
            this.cbxStockEdit_SearchModelName.TabIndex = 47;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(84, 90);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(92, 16);
            this.label65.TabIndex = 44;
            this.label65.Text = "Model Name :";
            // 
            // cbxStockEdit_SearchProductName
            // 
            this.cbxStockEdit_SearchProductName.FormattingEnabled = true;
            this.cbxStockEdit_SearchProductName.Items.AddRange(new object[] {
            "Sony MP3"});
            this.cbxStockEdit_SearchProductName.Location = new System.Drawing.Point(212, 56);
            this.cbxStockEdit_SearchProductName.Name = "cbxStockEdit_SearchProductName";
            this.cbxStockEdit_SearchProductName.Size = new System.Drawing.Size(166, 24);
            this.cbxStockEdit_SearchProductName.TabIndex = 46;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(11, 117);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(165, 16);
            this.label66.TabIndex = 45;
            this.label66.Text = "Supplier Company Name :";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(62, 22);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(160, 25);
            this.label63.TabIndex = 21;
            this.label63.Text = "Search Product";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(468, 13);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(100, 81);
            this.pictureBox9.TabIndex = 20;
            this.pictureBox9.TabStop = false;
            // 
            // btnCancelProToDeleteExist
            // 
            this.btnCancelProToDeleteExist.Location = new System.Drawing.Point(474, 362);
            this.btnCancelProToDeleteExist.Name = "btnCancelProToDeleteExist";
            this.btnCancelProToDeleteExist.Size = new System.Drawing.Size(75, 23);
            this.btnCancelProToDeleteExist.TabIndex = 19;
            this.btnCancelProToDeleteExist.Text = "Cancel";
            this.btnCancelProToDeleteExist.UseVisualStyleBackColor = true;
            // 
            // btnCheckProToDeleteExist
            // 
            this.btnCheckProToDeleteExist.Location = new System.Drawing.Point(360, 362);
            this.btnCheckProToDeleteExist.Name = "btnCheckProToDeleteExist";
            this.btnCheckProToDeleteExist.Size = new System.Drawing.Size(75, 23);
            this.btnCheckProToDeleteExist.TabIndex = 18;
            this.btnCheckProToDeleteExist.Text = "Check";
            this.btnCheckProToDeleteExist.UseVisualStyleBackColor = true;
            this.btnCheckProToDeleteExist.Click += new System.EventHandler(this.btnCheckProToDeleteExist_Click);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(62, 90);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(231, 16);
            this.label64.TabIndex = 17;
            this.label64.Text = "Please enter the following information:";
            // 
            // pnlStockEdit_NotFound
            // 
            this.pnlStockEdit_NotFound.AutoScroll = true;
            this.pnlStockEdit_NotFound.Controls.Add(this.button5);
            this.pnlStockEdit_NotFound.Controls.Add(this.pictureBox10);
            this.pnlStockEdit_NotFound.Controls.Add(this.groupBox9);
            this.pnlStockEdit_NotFound.Controls.Add(this.btnStock_NotFound_Add);
            this.pnlStockEdit_NotFound.Location = new System.Drawing.Point(0, 22);
            this.pnlStockEdit_NotFound.Name = "pnlStockEdit_NotFound";
            this.pnlStockEdit_NotFound.Size = new System.Drawing.Size(584, 425);
            this.pnlStockEdit_NotFound.TabIndex = 28;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(490, 381);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 56;
            this.button5.Text = "Cancel";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(457, 10);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(100, 81);
            this.pictureBox10.TabIndex = 27;
            this.pictureBox10.TabStop = false;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Controls.Add(this.tbxStockEdit_NotFound_CompanyName);
            this.groupBox9.Controls.Add(this.label68);
            this.groupBox9.Controls.Add(this.tbxStockEdit_NotFound_StockQty);
            this.groupBox9.Controls.Add(this.tbxStockEdit_NotFound_ModelName);
            this.groupBox9.Controls.Add(this.tbxStockEdit_NotFound_ProductName);
            this.groupBox9.Controls.Add(this.label74);
            this.groupBox9.Controls.Add(this.label75);
            this.groupBox9.Controls.Add(this.label77);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(33, 22);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(523, 315);
            this.groupBox9.TabIndex = 26;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Stock Not Found";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(43, 72);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(169, 13);
            this.label62.TabIndex = 55;
            this.label62.Text = "Please fill up the information below";
            // 
            // tbxStockEdit_NotFound_CompanyName
            // 
            this.tbxStockEdit_NotFound_CompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxStockEdit_NotFound_CompanyName.Location = new System.Drawing.Point(201, 185);
            this.tbxStockEdit_NotFound_CompanyName.Name = "tbxStockEdit_NotFound_CompanyName";
            this.tbxStockEdit_NotFound_CompanyName.Size = new System.Drawing.Size(184, 20);
            this.tbxStockEdit_NotFound_CompanyName.TabIndex = 54;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(69, 187);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(88, 13);
            this.label68.TabIndex = 51;
            this.label68.Text = "Company Name :";
            // 
            // tbxStockEdit_NotFound_StockQty
            // 
            this.tbxStockEdit_NotFound_StockQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxStockEdit_NotFound_StockQty.Location = new System.Drawing.Point(202, 219);
            this.tbxStockEdit_NotFound_StockQty.Name = "tbxStockEdit_NotFound_StockQty";
            this.tbxStockEdit_NotFound_StockQty.Size = new System.Drawing.Size(184, 20);
            this.tbxStockEdit_NotFound_StockQty.TabIndex = 41;
            // 
            // tbxStockEdit_NotFound_ModelName
            // 
            this.tbxStockEdit_NotFound_ModelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxStockEdit_NotFound_ModelName.Location = new System.Drawing.Point(202, 157);
            this.tbxStockEdit_NotFound_ModelName.Name = "tbxStockEdit_NotFound_ModelName";
            this.tbxStockEdit_NotFound_ModelName.Size = new System.Drawing.Size(184, 20);
            this.tbxStockEdit_NotFound_ModelName.TabIndex = 39;
            // 
            // tbxStockEdit_NotFound_ProductName
            // 
            this.tbxStockEdit_NotFound_ProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxStockEdit_NotFound_ProductName.Location = new System.Drawing.Point(202, 124);
            this.tbxStockEdit_NotFound_ProductName.Name = "tbxStockEdit_NotFound_ProductName";
            this.tbxStockEdit_NotFound_ProductName.Size = new System.Drawing.Size(184, 20);
            this.tbxStockEdit_NotFound_ProductName.TabIndex = 38;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(83, 157);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(73, 13);
            this.label74.TabIndex = 34;
            this.label74.Text = "Model Name :";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(75, 127);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(81, 13);
            this.label75.TabIndex = 33;
            this.label75.Text = "Product Name :";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(74, 219);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(82, 13);
            this.label77.TabIndex = 31;
            this.label77.Text = "Add Stock Qty :";
            // 
            // btnStock_NotFound_Add
            // 
            this.btnStock_NotFound_Add.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStock_NotFound_Add.Location = new System.Drawing.Point(388, 381);
            this.btnStock_NotFound_Add.Name = "btnStock_NotFound_Add";
            this.btnStock_NotFound_Add.Size = new System.Drawing.Size(75, 23);
            this.btnStock_NotFound_Add.TabIndex = 43;
            this.btnStock_NotFound_Add.Text = "Add";
            this.btnStock_NotFound_Add.UseVisualStyleBackColor = true;
            this.btnStock_NotFound_Add.Click += new System.EventHandler(this.btnStock_NotFound_Add_Click);
            // 
            // pnlStockEdit_Found
            // 
            this.pnlStockEdit_Found.AutoScroll = true;
            this.pnlStockEdit_Found.Controls.Add(this.button7);
            this.pnlStockEdit_Found.Controls.Add(this.btnStock_Found_Update);
            this.pnlStockEdit_Found.Controls.Add(this.pictureBox11);
            this.pnlStockEdit_Found.Controls.Add(this.groupBox10);
            this.pnlStockEdit_Found.Location = new System.Drawing.Point(0, 25);
            this.pnlStockEdit_Found.Name = "pnlStockEdit_Found";
            this.pnlStockEdit_Found.Size = new System.Drawing.Size(584, 419);
            this.pnlStockEdit_Found.TabIndex = 29;
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(490, 378);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 58;
            this.button7.Text = "Cancel";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // btnStock_Found_Update
            // 
            this.btnStock_Found_Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStock_Found_Update.Location = new System.Drawing.Point(388, 378);
            this.btnStock_Found_Update.Name = "btnStock_Found_Update";
            this.btnStock_Found_Update.Size = new System.Drawing.Size(75, 23);
            this.btnStock_Found_Update.TabIndex = 57;
            this.btnStock_Found_Update.Text = "Update";
            this.btnStock_Found_Update.UseVisualStyleBackColor = true;
            this.btnStock_Found_Update.Click += new System.EventHandler(this.btnStock_Found_Update_Click);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(469, 12);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(100, 81);
            this.pictureBox11.TabIndex = 29;
            this.pictureBox11.TabStop = false;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label67);
            this.groupBox10.Controls.Add(this.textBox31);
            this.groupBox10.Controls.Add(this.label69);
            this.groupBox10.Controls.Add(this.textBox32);
            this.groupBox10.Controls.Add(this.textBox33);
            this.groupBox10.Controls.Add(this.textBox34);
            this.groupBox10.Controls.Add(this.label70);
            this.groupBox10.Controls.Add(this.label71);
            this.groupBox10.Controls.Add(this.label72);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(45, 24);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(523, 315);
            this.groupBox10.TabIndex = 28;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Stock Found";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(43, 72);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(169, 13);
            this.label67.TabIndex = 55;
            this.label67.Text = "Please fill up the information below";
            // 
            // textBox31
            // 
            this.textBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox31.Location = new System.Drawing.Point(201, 185);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(184, 20);
            this.textBox31.TabIndex = 54;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(69, 187);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(88, 13);
            this.label69.TabIndex = 51;
            this.label69.Text = "Company Name :";
            // 
            // textBox32
            // 
            this.textBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox32.Location = new System.Drawing.Point(202, 219);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(184, 20);
            this.textBox32.TabIndex = 41;
            // 
            // textBox33
            // 
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox33.Location = new System.Drawing.Point(202, 157);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(184, 20);
            this.textBox33.TabIndex = 39;
            // 
            // textBox34
            // 
            this.textBox34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox34.Location = new System.Drawing.Point(202, 124);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(184, 20);
            this.textBox34.TabIndex = 38;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(83, 157);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(73, 13);
            this.label70.TabIndex = 34;
            this.label70.Text = "Model Name :";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(75, 127);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(81, 13);
            this.label71.TabIndex = 33;
            this.label71.Text = "Product Name :";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(97, 217);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(60, 13);
            this.label72.TabIndex = 31;
            this.label72.Text = "Stock Qty :";
            // 
            // pnlSearchDisplayProductQty_Search
            // 
            this.pnlSearchDisplayProductQty_Search.Controls.Add(this.label79);
            this.pnlSearchDisplayProductQty_Search.Controls.Add(this.pictureBox12);
            this.pnlSearchDisplayProductQty_Search.Controls.Add(this.button9);
            this.pnlSearchDisplayProductQty_Search.Controls.Add(this.button10);
            this.pnlSearchDisplayProductQty_Search.Controls.Add(this.label80);
            this.pnlSearchDisplayProductQty_Search.Controls.Add(this.groupBox11);
            this.pnlSearchDisplayProductQty_Search.Location = new System.Drawing.Point(0, 23);
            this.pnlSearchDisplayProductQty_Search.Name = "pnlSearchDisplayProductQty_Search";
            this.pnlSearchDisplayProductQty_Search.Size = new System.Drawing.Size(584, 425);
            this.pnlSearchDisplayProductQty_Search.TabIndex = 30;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(68, 23);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(160, 25);
            this.label79.TabIndex = 27;
            this.label79.Text = "Search Product";
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(474, 14);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(100, 81);
            this.pictureBox12.TabIndex = 26;
            this.pictureBox12.TabStop = false;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(480, 363);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 25;
            this.button9.Text = "Cancel";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(366, 363);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 24;
            this.button10.Text = "Search";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(68, 91);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(231, 16);
            this.label80.TabIndex = 23;
            this.label80.Text = "Please enter the following information:";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label81);
            this.groupBox11.Controls.Add(this.comboBox1);
            this.groupBox11.Controls.Add(this.label73);
            this.groupBox11.Controls.Add(this.comboBox2);
            this.groupBox11.Controls.Add(this.label76);
            this.groupBox11.Controls.Add(this.comboBox3);
            this.groupBox11.Controls.Add(this.label78);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(71, 144);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(398, 188);
            this.groupBox11.TabIndex = 28;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Search Product";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(120, 96);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(30, 16);
            this.label81.TabIndex = 49;
            this.label81.Text = "OR";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Nano"});
            this.comboBox1.Location = new System.Drawing.Point(213, 121);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(165, 24);
            this.comboBox1.TabIndex = 48;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(77, 38);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(100, 16);
            this.label73.TabIndex = 43;
            this.label73.Text = "Product Name :";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Sony-123"});
            this.comboBox2.Location = new System.Drawing.Point(213, 62);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(166, 24);
            this.comboBox2.TabIndex = 47;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(84, 67);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(92, 16);
            this.label76.TabIndex = 44;
            this.label76.Text = "Model Name :";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Sony MP3"});
            this.comboBox3.Location = new System.Drawing.Point(212, 33);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(166, 24);
            this.comboBox3.TabIndex = 46;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(11, 123);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(165, 16);
            this.label78.TabIndex = 45;
            this.label78.Text = "Supplier Company Name :";
            // 
            // pnlStock_DisplaySearchedQty
            // 
            this.pnlStock_DisplaySearchedQty.Controls.Add(this.button11);
            this.pnlStock_DisplaySearchedQty.Controls.Add(this.pictureBox13);
            this.pnlStock_DisplaySearchedQty.Controls.Add(this.groupBox12);
            this.pnlStock_DisplaySearchedQty.Location = new System.Drawing.Point(0, 25);
            this.pnlStock_DisplaySearchedQty.Name = "pnlStock_DisplaySearchedQty";
            this.pnlStock_DisplaySearchedQty.Size = new System.Drawing.Size(584, 422);
            this.pnlStock_DisplaySearchedQty.TabIndex = 31;
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(490, 375);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 62;
            this.button11.Text = "Cancel";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(469, 9);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(100, 81);
            this.pictureBox13.TabIndex = 60;
            this.pictureBox13.TabStop = false;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.textBox35);
            this.groupBox12.Controls.Add(this.label83);
            this.groupBox12.Controls.Add(this.textBox36);
            this.groupBox12.Controls.Add(this.textBox37);
            this.groupBox12.Controls.Add(this.textBox38);
            this.groupBox12.Controls.Add(this.label84);
            this.groupBox12.Controls.Add(this.label85);
            this.groupBox12.Controls.Add(this.label86);
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.Location = new System.Drawing.Point(45, 21);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(523, 315);
            this.groupBox12.TabIndex = 59;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Searched Stock";
            // 
            // textBox35
            // 
            this.textBox35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox35.Location = new System.Drawing.Point(205, 168);
            this.textBox35.Name = "textBox35";
            this.textBox35.ReadOnly = true;
            this.textBox35.Size = new System.Drawing.Size(184, 20);
            this.textBox35.TabIndex = 54;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(73, 170);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(88, 13);
            this.label83.TabIndex = 51;
            this.label83.Text = "Company Name :";
            // 
            // textBox36
            // 
            this.textBox36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox36.Location = new System.Drawing.Point(206, 202);
            this.textBox36.Name = "textBox36";
            this.textBox36.ReadOnly = true;
            this.textBox36.Size = new System.Drawing.Size(184, 20);
            this.textBox36.TabIndex = 41;
            // 
            // textBox37
            // 
            this.textBox37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox37.Location = new System.Drawing.Point(206, 140);
            this.textBox37.Name = "textBox37";
            this.textBox37.ReadOnly = true;
            this.textBox37.Size = new System.Drawing.Size(184, 20);
            this.textBox37.TabIndex = 39;
            // 
            // textBox38
            // 
            this.textBox38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox38.Location = new System.Drawing.Point(206, 107);
            this.textBox38.Name = "textBox38";
            this.textBox38.ReadOnly = true;
            this.textBox38.Size = new System.Drawing.Size(184, 20);
            this.textBox38.TabIndex = 38;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(87, 140);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(73, 13);
            this.label84.TabIndex = 34;
            this.label84.Text = "Model Name :";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(79, 110);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(81, 13);
            this.label85.TabIndex = 33;
            this.label85.Text = "Product Name :";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(101, 200);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(60, 13);
            this.label86.TabIndex = 31;
            this.label86.Text = "Stock Qty :";
            // 
            // pnlStockExcessShortFall_Display
            // 
            this.pnlStockExcessShortFall_Display.Controls.Add(this.btnStockExcessShortFall_Cancel);
            this.pnlStockExcessShortFall_Display.Controls.Add(this.groupBox14);
            this.pnlStockExcessShortFall_Display.Controls.Add(this.label90);
            this.pnlStockExcessShortFall_Display.Controls.Add(this.pictureBox15);
            this.pnlStockExcessShortFall_Display.Location = new System.Drawing.Point(0, 22);
            this.pnlStockExcessShortFall_Display.Name = "pnlStockExcessShortFall_Display";
            this.pnlStockExcessShortFall_Display.Size = new System.Drawing.Size(584, 424);
            this.pnlStockExcessShortFall_Display.TabIndex = 34;
            // 
            // btnStockExcessShortFall_Cancel
            // 
            this.btnStockExcessShortFall_Cancel.Location = new System.Drawing.Point(498, 368);
            this.btnStockExcessShortFall_Cancel.Name = "btnStockExcessShortFall_Cancel";
            this.btnStockExcessShortFall_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btnStockExcessShortFall_Cancel.TabIndex = 35;
            this.btnStockExcessShortFall_Cancel.Text = "Cancel";
            this.btnStockExcessShortFall_Cancel.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tbxExcessShortFall_Display_ExcessShortFall);
            this.groupBox14.Controls.Add(this.tbxExcessShortFall_Display_TotalOrder);
            this.groupBox14.Controls.Add(this.tbxExcessShortFall_Display_TotalQty);
            this.groupBox14.Controls.Add(this.tbxExcessShortFall_Display_ProductNameModelName);
            this.groupBox14.Controls.Add(this.label93);
            this.groupBox14.Controls.Add(this.label92);
            this.groupBox14.Controls.Add(this.label91);
            this.groupBox14.Controls.Add(this.label89);
            this.groupBox14.Location = new System.Drawing.Point(37, 96);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(423, 236);
            this.groupBox14.TabIndex = 34;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Display Excess ShortFall";
            // 
            // tbxExcessShortFall_Display_ExcessShortFall
            // 
            this.tbxExcessShortFall_Display_ExcessShortFall.Location = new System.Drawing.Point(197, 165);
            this.tbxExcessShortFall_Display_ExcessShortFall.Name = "tbxExcessShortFall_Display_ExcessShortFall";
            this.tbxExcessShortFall_Display_ExcessShortFall.ReadOnly = true;
            this.tbxExcessShortFall_Display_ExcessShortFall.Size = new System.Drawing.Size(149, 20);
            this.tbxExcessShortFall_Display_ExcessShortFall.TabIndex = 10;
            // 
            // tbxExcessShortFall_Display_TotalOrder
            // 
            this.tbxExcessShortFall_Display_TotalOrder.Location = new System.Drawing.Point(197, 135);
            this.tbxExcessShortFall_Display_TotalOrder.Name = "tbxExcessShortFall_Display_TotalOrder";
            this.tbxExcessShortFall_Display_TotalOrder.ReadOnly = true;
            this.tbxExcessShortFall_Display_TotalOrder.Size = new System.Drawing.Size(149, 20);
            this.tbxExcessShortFall_Display_TotalOrder.TabIndex = 9;
            // 
            // tbxExcessShortFall_Display_TotalQty
            // 
            this.tbxExcessShortFall_Display_TotalQty.Location = new System.Drawing.Point(198, 102);
            this.tbxExcessShortFall_Display_TotalQty.Name = "tbxExcessShortFall_Display_TotalQty";
            this.tbxExcessShortFall_Display_TotalQty.ReadOnly = true;
            this.tbxExcessShortFall_Display_TotalQty.Size = new System.Drawing.Size(149, 20);
            this.tbxExcessShortFall_Display_TotalQty.TabIndex = 8;
            // 
            // tbxExcessShortFall_Display_ProductNameModelName
            // 
            this.tbxExcessShortFall_Display_ProductNameModelName.Location = new System.Drawing.Point(198, 68);
            this.tbxExcessShortFall_Display_ProductNameModelName.Name = "tbxExcessShortFall_Display_ProductNameModelName";
            this.tbxExcessShortFall_Display_ProductNameModelName.ReadOnly = true;
            this.tbxExcessShortFall_Display_ProductNameModelName.Size = new System.Drawing.Size(149, 20);
            this.tbxExcessShortFall_Display_ProductNameModelName.TabIndex = 7;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(42, 138);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(60, 13);
            this.label93.TabIndex = 6;
            this.label93.Text = "Total Order";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(44, 169);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(131, 13);
            this.label92.TabIndex = 4;
            this.label92.Text = "Excess/ShortFall/Balance";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(42, 106);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(50, 13);
            this.label91.TabIndex = 2;
            this.label91.Text = "Total Qty";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(41, 71);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(147, 13);
            this.label89.TabIndex = 0;
            this.label89.Text = "Product Name && Model Name";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(58, 24);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(167, 25);
            this.label90.TabIndex = 33;
            this.label90.Text = "Excess Shortfall";
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(469, 15);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(100, 81);
            this.pictureBox15.TabIndex = 32;
            this.pictureBox15.TabStop = false;
            // 
            // pnlExcessShortFall_Search
            // 
            this.pnlExcessShortFall_Search.Controls.Add(this.btnStockExcessShortFall__SearchCancel);
            this.pnlExcessShortFall_Search.Controls.Add(this.groupBox13);
            this.pnlExcessShortFall_Search.Controls.Add(this.btnStock_ExcessShortFall_Search);
            this.pnlExcessShortFall_Search.Controls.Add(this.label82);
            this.pnlExcessShortFall_Search.Controls.Add(this.pictureBox14);
            this.pnlExcessShortFall_Search.Location = new System.Drawing.Point(0, 22);
            this.pnlExcessShortFall_Search.Name = "pnlExcessShortFall_Search";
            this.pnlExcessShortFall_Search.Size = new System.Drawing.Size(584, 425);
            this.pnlExcessShortFall_Search.TabIndex = 35;
            // 
            // btnStockExcessShortFall__SearchCancel
            // 
            this.btnStockExcessShortFall__SearchCancel.Location = new System.Drawing.Point(493, 366);
            this.btnStockExcessShortFall__SearchCancel.Name = "btnStockExcessShortFall__SearchCancel";
            this.btnStockExcessShortFall__SearchCancel.Size = new System.Drawing.Size(75, 23);
            this.btnStockExcessShortFall__SearchCancel.TabIndex = 37;
            this.btnStockExcessShortFall__SearchCancel.Text = "Cancel";
            this.btnStockExcessShortFall__SearchCancel.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.cbxStock_ComputeExcessShortFall_Search);
            this.groupBox13.Controls.Add(this.label88);
            this.groupBox13.Location = new System.Drawing.Point(56, 97);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(423, 236);
            this.groupBox13.TabIndex = 35;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Search For Excess Shortfall";
            // 
            // cbxStock_ComputeExcessShortFall_Search
            // 
            this.cbxStock_ComputeExcessShortFall_Search.FormattingEnabled = true;
            this.cbxStock_ComputeExcessShortFall_Search.Location = new System.Drawing.Point(205, 117);
            this.cbxStock_ComputeExcessShortFall_Search.Name = "cbxStock_ComputeExcessShortFall_Search";
            this.cbxStock_ComputeExcessShortFall_Search.Size = new System.Drawing.Size(161, 21);
            this.cbxStock_ComputeExcessShortFall_Search.TabIndex = 1;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(41, 121);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(147, 13);
            this.label88.TabIndex = 0;
            this.label88.Text = "Product Name && Model Name";
            // 
            // btnStock_ExcessShortFall_Search
            // 
            this.btnStock_ExcessShortFall_Search.Location = new System.Drawing.Point(394, 366);
            this.btnStock_ExcessShortFall_Search.Name = "btnStock_ExcessShortFall_Search";
            this.btnStock_ExcessShortFall_Search.Size = new System.Drawing.Size(75, 23);
            this.btnStock_ExcessShortFall_Search.TabIndex = 36;
            this.btnStock_ExcessShortFall_Search.Text = "Search";
            this.btnStock_ExcessShortFall_Search.UseVisualStyleBackColor = true;
            this.btnStock_ExcessShortFall_Search.Click += new System.EventHandler(this.btnStock_ExcessShortFall_Search_Click);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(58, 18);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(167, 25);
            this.label82.TabIndex = 34;
            this.label82.Text = "Excess Shortfall";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(469, 9);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(100, 81);
            this.pictureBox14.TabIndex = 33;
            this.pictureBox14.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 444);
            this.Controls.Add(this.menuStrip2);
            this.Controls.Add(this.pnlAddSupplier);
            this.Controls.Add(this.pnlAdd_check);
            this.Controls.Add(this.pnlSearchSupplier_Info);
            this.Controls.Add(this.pnlStock_DisplaySearchedQty);
            this.Controls.Add(this.pnlStockEdit_Found);
            this.Controls.Add(this.pnlStockEdit_NotFound);
            this.Controls.Add(this.pnlStockExcessShortFall_Display);
            this.Controls.Add(this.pnlExcessShortFall_Search);
            this.Controls.Add(this.pnlSearchDisplayProductQty_Search);
            this.Controls.Add(this.pnlStockSearchToEdit_Chck);
            this.Controls.Add(this.pnlSearchSupplier_Check);
            this.Controls.Add(this.pnlDeleteSupplier_Check);
            this.Controls.Add(this.pnlModifySupplier_Info);
            this.Controls.Add(this.pnlDeleteSupplier_Info);
            this.Controls.Add(this.pnlModifySupplier_Check);
            this.Name = "Form1";
            this.Text = "Inventory";
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.pnlAddSupplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.grpBoxAddSupplier.ResumeLayout(false);
            this.grpBoxAddSupplier.PerformLayout();
            this.pnlAdd_check.ResumeLayout(false);
            this.pnlAdd_check.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlModifySupplier_Check.ResumeLayout(false);
            this.pnlModifySupplier_Check.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.pnlDeleteSupplier_Check.ResumeLayout(false);
            this.pnlDeleteSupplier_Check.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.pnlSearchSupplier_Check.ResumeLayout(false);
            this.pnlSearchSupplier_Check.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.pnlDeleteSupplier_Info.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.pnlModifySupplier_Info.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.pnlSearchSupplier_Info.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.pnlStockSearchToEdit_Chck.ResumeLayout(false);
            this.pnlStockSearchToEdit_Chck.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.pnlStockEdit_NotFound.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.pnlStockEdit_Found.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.pnlSearchDisplayProductQty_Search.ResumeLayout(false);
            this.pnlSearchDisplayProductQty_Search.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.pnlStock_DisplaySearchedQty.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.pnlStockExcessShortFall_Display.ResumeLayout(false);
            this.pnlStockExcessShortFall_Display.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.pnlExcessShortFall_Search.ResumeLayout(false);
            this.pnlExcessShortFall_Search.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem staffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem catalogueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_AddSupplier;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_ModifySupplier;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_DeleteSupplier;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_SearchSupplier;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem menuStock_EditProductQty;
        private System.Windows.Forms.ToolStripMenuItem menuStock_SearchProductQty;
        private System.Windows.Forms.ToolStripMenuItem menuStock_computeExcessShortFall;
        private System.Windows.Forms.Panel pnlAddSupplier;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox grpBoxAddSupplier;
        private System.Windows.Forms.TextBox tbxAddCityCountry;
        private System.Windows.Forms.TextBox tbxAddPostalCode;
        private System.Windows.Forms.TextBox tbxAddUnitNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxAddFaxNum;
        private System.Windows.Forms.TextBox tbxAddPhoneNum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAddSubmit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbxAddContactName;
        private System.Windows.Forms.TextBox tbxAddStreetName;
        private System.Windows.Forms.TextBox tbxAddHouseNum;
        private System.Windows.Forms.TextBox tbxAddBlockNum;
        private System.Windows.Forms.TextBox tbxAddCompanyName;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel pnlAdd_check;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbxAddSupplier_CheckCompanyName;
        private System.Windows.Forms.Label lblCheckProNameToDelete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAddSupplier_CheckCancel;
        private System.Windows.Forms.Button btnAddSupplier_CheckCompany;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnAddSupplier_InfoCancel;
        private System.Windows.Forms.Panel pnlModifySupplier_Check;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnModifySupplier_Check_Cancel;
        private System.Windows.Forms.Button btnModifySupplier_Check_Search;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbxModifySupplier_Check_CompanyName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel pnlDeleteSupplier_Check;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbDeleteSupplier_Check_CompanyName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnDeleteSupplier_Check_Cancel;
        private System.Windows.Forms.Button btnDeleteSupplier_Check_Search;
        private System.Windows.Forms.Panel pnlSearchSupplier_Check;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnSearchSupplier_Check_Cancel;
        private System.Windows.Forms.Button btnSearchSupplier_Check_Search;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel pnlDeleteSupplier_Info;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel pnlModifySupplier_Info;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnModifySupplier_Cancel;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button btnModifySupplier_Update;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.RadioButton rbtnSearch_City;
        private System.Windows.Forms.RadioButton rbtnSearch_ContactName;
        private System.Windows.Forms.RadioButton rbtnSearch_CompanyName;
        private System.Windows.Forms.TextBox tbxSearch_SearchSupplier;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel pnlSearchSupplier_Info;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Panel pnlStockSearchToEdit_Chck;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Button btnCancelProToDeleteExist;
        private System.Windows.Forms.Button btnCheckProToDeleteExist;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.ComboBox cbxStockEdit_SearchCompanyName;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.ComboBox cbxStockEdit_SearchModelName;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox cbxStockEdit_SearchProductName;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel pnlStockEdit_NotFound;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox tbxStockEdit_NotFound_CompanyName;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Button btnStock_NotFound_Add;
        private System.Windows.Forms.TextBox tbxStockEdit_NotFound_StockQty;
        private System.Windows.Forms.TextBox tbxStockEdit_NotFound_ModelName;
        private System.Windows.Forms.TextBox tbxStockEdit_NotFound_ProductName;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Panel pnlStockEdit_Found;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btnStock_Found_Update;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Panel pnlSearchDisplayProductQty_Search;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Panel pnlStock_DisplaySearchedQty;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Panel pnlStockExcessShortFall_Display;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Button btnStockExcessShortFall_Cancel;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox tbxExcessShortFall_Display_ExcessShortFall;
        private System.Windows.Forms.TextBox tbxExcessShortFall_Display_TotalOrder;
        private System.Windows.Forms.TextBox tbxExcessShortFall_Display_TotalQty;
        private System.Windows.Forms.TextBox tbxExcessShortFall_Display_ProductNameModelName;
        private System.Windows.Forms.Panel pnlExcessShortFall_Search;
        private System.Windows.Forms.Button btnStockExcessShortFall__SearchCancel;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.ComboBox cbxStock_ComputeExcessShortFall_Search;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Button btnStock_ExcessShortFall_Search;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.PictureBox pictureBox14;
    }
}

