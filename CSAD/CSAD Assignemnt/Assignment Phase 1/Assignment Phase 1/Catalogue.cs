using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_1
{
    public partial class frmCatalogue : Form
    {
        private AllDataClass allData;
        public frmCatalogue(AllDataClass adc)
        {
            InitializeComponent();
            allData = adc;
        }

        public void btnCheckCatExist_Click(object sender, EventArgs e)
        {
            lblCatNameToAdd.Text = tbxCheckCatNameToAdd.Text;
            tbxCheckCatNameToAdd.Text = "";
            MessageBox.Show("Catalogue not found. Proceed to enter Catalogue's Details");
            pnlCheckIfCatToAddExist.Visible = false;
            pnlEnterCatDetail.Visible = true;
            pnlCheckIfCatToModifyExist.Visible = false;
            pnlModifyCatDetail.Visible = false;
            pnlCheckIfCatToDeleteExist.Visible = false;
            pnlDetailOfCategoryToDelete.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlCheckIfProToAddExist.Visible = false;
            pnlCheckIfProToModifyExist.Visible = false;
            pnlCheckIfProToDeleteExist.Visible = false;
            pnlAddProduct.Visible = false;
            pnlDeleteProduct.Visible = false;
            pnlModifyProduct.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlSearchAndDisplayProInfo.Visible = false;
            
            
        }

        public void addNewCatalogueInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnlCheckIfCatToAddExist.Visible = true;
            pnlEnterCatDetail.Visible = false;
            pnlCheckIfCatToModifyExist.Visible = false;
            pnlModifyCatDetail.Visible = false;
            pnlCheckIfCatToDeleteExist.Visible = false;
            pnlDetailOfCategoryToDelete.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlCheckIfProToAddExist.Visible = false;
            pnlCheckIfProToModifyExist.Visible = false;
            pnlCheckIfProToDeleteExist.Visible = false;
            pnlAddProduct.Visible = false;
            pnlDeleteProduct.Visible = false;
            pnlModifyProduct.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlSearchAndDisplayProInfo.Visible = false;
            
        }

        public void modifyCatalogueInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnlCheckIfCatToAddExist.Visible = false;
            pnlEnterCatDetail.Visible = false;
            pnlCheckIfCatToModifyExist.Visible = true;
            pnlModifyCatDetail.Visible = false;
            pnlCheckIfCatToDeleteExist.Visible = false;
            pnlDetailOfCategoryToDelete.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlCheckIfProToAddExist.Visible = false;
            pnlCheckIfProToModifyExist.Visible = false;
            pnlCheckIfProToDeleteExist.Visible = false;
            pnlAddProduct.Visible = false;
            pnlDeleteProduct.Visible = false;
            pnlModifyProduct.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlSearchAndDisplayProInfo.Visible = false;
            
        }



        public void btnConfirmCatDetail_Click(object sender, EventArgs e)
        {
            if (tbxEnterCatDescNameToAdd.Text == "" )
            {
                MessageBox.Show("Fill in Missing Fields");
            }
            else
            {

                lblCatNameToAdd.Text = "";
                tbxEnterCatDescNameToAdd.Text = "";
                MessageBox.Show("Add Sucessfully");
            }
        }

        public void btnUpdateCatDetail_Click(object sender, EventArgs e)
        {
            if (tbxEnterCatDescNameToModify.Text == "")
            {
                MessageBox.Show("Please fill in the Missing Field(s)");
            }
            else
            {
                MessageBox.Show("Update Sucessfully");
            }
        }

        public void btnCheckCatToModifyExist_Click(object sender, EventArgs e)
        {
            if (tbxCheckCatNameToModify.Text == "MP4")
            {
                lblNameOfCategoryToModify.Text = tbxCheckCatNameToModify.Text;
                tbxCheckCatNameToModify.Text = "";              
                MessageBox.Show("Category Found");
                pnlCheckIfCatToAddExist.Visible = false;
                pnlEnterCatDetail.Visible = false;
                pnlCheckIfCatToModifyExist.Visible = false;
                pnlModifyCatDetail.Visible = true;
                pnlCheckIfCatToDeleteExist.Visible = false;
                pnlDetailOfCategoryToDelete.Visible = false;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlCheckIfProToAddExist.Visible = false;
                pnlCheckIfProToModifyExist.Visible = false;
                pnlCheckIfProToDeleteExist.Visible = false;
                pnlAddProduct.Visible = false;
                pnlDeleteProduct.Visible = false;
                pnlModifyProduct.Visible = false;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlSearchAndDisplayProInfo.Visible = false;
                
            }

            else
            {
                MessageBox.Show("Category not Found");
            }
        }

        public void deleteCatalogueInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {

                pnlCheckIfCatToAddExist.Visible = false;
                pnlEnterCatDetail.Visible = false;
                pnlCheckIfCatToModifyExist.Visible = false;
                pnlModifyCatDetail.Visible = false;
                pnlCheckIfCatToDeleteExist.Visible = true;
                pnlDetailOfCategoryToDelete.Visible = false;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlCheckIfProToAddExist.Visible = false;
                pnlCheckIfProToModifyExist.Visible = false;
                pnlCheckIfProToDeleteExist.Visible = false;
                pnlAddProduct.Visible = false;
                pnlDeleteProduct.Visible = false;
                pnlModifyProduct.Visible = false;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlSearchAndDisplayProInfo.Visible = false;


        }

        public void btnCheckCatToDeleteExist_Click(object sender, EventArgs e)
        {
            if (tbxCheckCatNameToDelete.Text == "MP4")
            {
                MessageBox.Show("Category Found");
                pnlCheckIfCatToAddExist.Visible = false;
                pnlEnterCatDetail.Visible = false;
                pnlCheckIfCatToModifyExist.Visible = false;
                pnlModifyCatDetail.Visible = false;
                pnlCheckIfCatToDeleteExist.Visible = false;
                pnlDetailOfCategoryToDelete.Visible = true;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlCheckIfProToAddExist.Visible = false;
                pnlCheckIfProToModifyExist.Visible = false;
                pnlCheckIfProToDeleteExist.Visible = false;
                pnlAddProduct.Visible = false;
                pnlDeleteProduct.Visible = false;
                pnlModifyProduct.Visible = false;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlSearchAndDisplayProInfo.Visible = false;

            }

            else
            {
                MessageBox.Show("Category not Found");
            }
        }

        public void btnCategoryToDelete_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Category deleted");
        }

        public void searchAndDisplayCatalogueInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnlCheckIfCatToAddExist.Visible = false;
            pnlEnterCatDetail.Visible = false;
            pnlCheckIfCatToModifyExist.Visible = false;
            pnlModifyCatDetail.Visible = false;
            pnlCheckIfCatToDeleteExist.Visible = false;
            pnlDetailOfCategoryToDelete.Visible = false;
            //pnlSearchAndDisplayCatInfo.Visible = true;
            pnlCheckIfProToAddExist.Visible = false;
            pnlCheckIfProToModifyExist.Visible = false;
            pnlCheckIfProToDeleteExist.Visible = false;
            pnlAddProduct.Visible = false;
            pnlDeleteProduct.Visible = false;
            pnlModifyProduct.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = true;
            pnlSearchAndDisplayProInfo.Visible = false;
        }

        public void btnDisplayCatDetail_Click(object sender, EventArgs e)
        {
            if (tbxSearchCategoryName.Text == "MP4")
            {
                tbxDisplayCatDescription.Text = "Nothing to display";
            }
        }

        public void addNewProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnlCheckIfCatToAddExist.Visible = false;
            pnlEnterCatDetail.Visible = false;
            pnlCheckIfCatToModifyExist.Visible = false;
            pnlModifyCatDetail.Visible = false;
            pnlCheckIfCatToDeleteExist.Visible = false;
            pnlDetailOfCategoryToDelete.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlCheckIfProToAddExist.Visible = true;
            pnlCheckIfProToModifyExist.Visible = false;
            pnlCheckIfProToDeleteExist.Visible = false;
            pnlAddProduct.Visible = false;
            pnlDeleteProduct.Visible = false;
            pnlModifyProduct.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlSearchAndDisplayProInfo.Visible = false;
        }

        public void modifyProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnlCheckIfCatToAddExist.Visible = false;
            pnlEnterCatDetail.Visible = false;
            pnlCheckIfCatToModifyExist.Visible = false;
            pnlModifyCatDetail.Visible = false;
            pnlCheckIfCatToDeleteExist.Visible = false;
            pnlDetailOfCategoryToDelete.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlCheckIfProToAddExist.Visible = false;
            pnlCheckIfProToModifyExist.Visible = true;
            pnlCheckIfProToDeleteExist.Visible = false;
            pnlAddProduct.Visible = false;
            pnlDeleteProduct.Visible = false;
            pnlModifyProduct.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlSearchAndDisplayProInfo.Visible = false;
        }

        public void deleteProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnlCheckIfCatToAddExist.Visible = false;
            pnlEnterCatDetail.Visible = false;
            pnlCheckIfCatToModifyExist.Visible = false;
            pnlModifyCatDetail.Visible = false;
            pnlCheckIfCatToDeleteExist.Visible = false;
            pnlDetailOfCategoryToDelete.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlCheckIfProToAddExist.Visible = false;
            pnlCheckIfProToModifyExist.Visible = false;
            pnlCheckIfProToDeleteExist.Visible = true;
            pnlAddProduct.Visible = false;
            pnlDeleteProduct.Visible = false;
            pnlModifyProduct.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlSearchAndDisplayProInfo.Visible = false;
        }

        public void button6_Click(object sender, EventArgs e)
        {
            if (tbxSearchCategoryName.Text == "MP4")
            {
                lblDisplayCategoryName.Text = tbxSearchCategoryName.Text;
                tbxSearchCategoryName.Text = "";
                tbxDisplayCatDescription.Text = "MP4 Player is a portable media player that support MPEG-4";
            }
        }

        public void btnAddProToCategory_Click(object sender, EventArgs e)
        {
            
        }

        public void btnCheckProToAddExist_Click(object sender, EventArgs e)
        {
            if ((tbxCheckProNameToAdd.Text == "MP4-Super") || (tbxCheckModelNumToAdd.Text == "1234567A")||(tbxCheckModelNumToAdd.Text.Length != 8))
            {
                MessageBox.Show("1.Product Name and/or Model Number already exist! \n\n2.Make sure that Model Number is of length 8!");
                tbxCheckProNameToAdd.Text = "";
                tbxCheckModelNumToAdd.Text = "";

            }
            else
            {
                MessageBox.Show("Product not found");
                lblProNameToBeAdded.Text = tbxCheckProNameToAdd.Text;
                lblProModelNumToBeAdded.Text = tbxCheckModelNumToAdd.Text;
                tbxCheckProNameToAdd.Text = "";
                tbxCheckModelNumToAdd.Text = "";
                pnlCheckIfCatToAddExist.Visible = false;
                pnlEnterCatDetail.Visible = false;
                pnlCheckIfCatToModifyExist.Visible = false;
                pnlModifyCatDetail.Visible = false;
                pnlCheckIfCatToDeleteExist.Visible = false;
                pnlDetailOfCategoryToDelete.Visible = false;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlCheckIfProToAddExist.Visible = false;
                pnlCheckIfProToModifyExist.Visible = false;
                pnlCheckIfProToDeleteExist.Visible = false;
                pnlAddProduct.Visible = true;
                pnlDeleteProduct.Visible = false;
                pnlModifyProduct.Visible = false;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlSearchAndDisplayProInfo.Visible = false;
            }
            

        }

        public void btnCheckProToDeleteExist_Click(object sender, EventArgs e)
        {
            if ((tbxCheckProNameToDelete.Text != "MP4-Super") || (tbxCheckModelNumToDelete.Text != "1234567A"))
            {
                MessageBox.Show("Product does not exist");
                tbxCheckProNameToDelete.Text = "";
                tbxCheckModelNumToDelete.Text = "";

            }
            else
            {
                MessageBox.Show("Product found");
                lblProNameToDelete.Text = tbxCheckProNameToDelete.Text;
                lblProModelNumToDelete.Text = tbxCheckModelNumToDelete.Text;
                tbxCheckProNameToDelete.Text = "";
                tbxCheckModelNumToDelete.Text = "";
                pnlCheckIfCatToAddExist.Visible = false;
                pnlEnterCatDetail.Visible = false;
                pnlCheckIfCatToModifyExist.Visible = false;
                pnlModifyCatDetail.Visible = false;
                pnlCheckIfCatToDeleteExist.Visible = false;
                pnlDetailOfCategoryToDelete.Visible = false;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlCheckIfProToAddExist.Visible = false;
                pnlCheckIfProToModifyExist.Visible = false;
                pnlCheckIfProToDeleteExist.Visible = false;
                pnlAddProduct.Visible = false;
                pnlDeleteProduct.Visible = true;
                pnlModifyProduct.Visible = false;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlSearchAndDisplayProInfo.Visible = false;
                
            }
        }

        public void btnUpdateCatDetail_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Update Sucessfully");
            tbxEnterCatDescNameToModify.Text = "";
        }

        public void btnCheckProToModifyExist_Click(object sender, EventArgs e)
        {
            if ((tbxCheckProNameToModify.Text != "MP4-Super") || (tbxCheckModelNumToModify.Text != "1234567A"))
            {
                MessageBox.Show("Product does not exist");
                tbxCheckProNameToDelete.Text = "";
                tbxCheckModelNumToDelete.Text = "";

            }
            else
            {
                MessageBox.Show("Product found");
                lblProNameToModify.Text = tbxCheckProNameToModify.Text;
                lblProModelNumToModify.Text = tbxCheckModelNumToModify.Text;
                tbxCheckProNameToDelete.Text = "";
                tbxCheckModelNumToDelete.Text = "";
                pnlCheckIfCatToAddExist.Visible = false;
                pnlEnterCatDetail.Visible = false;
                pnlCheckIfCatToModifyExist.Visible = false;
                pnlModifyCatDetail.Visible = false;
                pnlCheckIfCatToDeleteExist.Visible = false;
                pnlDetailOfCategoryToDelete.Visible = false;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlCheckIfProToAddExist.Visible = false;
                pnlCheckIfProToModifyExist.Visible = false;
                pnlCheckIfProToDeleteExist.Visible = false;
                pnlAddProduct.Visible = false;
                pnlDeleteProduct.Visible = false;
                pnlModifyProduct.Visible = true;
                pnlSearchAndDisplayCatInfo.Visible = false;
                pnlSearchAndDisplayProInfo.Visible = false;

            }
        }

        public void btnDisplayProInfo_Click(object sender, EventArgs e)
        {
            if ((tbxSearchProductName.Text == "MP4-Super") && (tbxSearchProModelNum.Text == "1234567A"))
            {
                lblDisplayProName.Text = "MP4-Super";
                lblDisplayProModelNum.Text = "1234567A";
                lblDisplayMSRP.Text = "$50";
                lblDisplayRetailPrice.Text = "$65";
                lblDisplayCategory.Text = "MP4";
                tbxDisplayProDes.Text = "MP4-Super is the cheapest MP4 player";
            }
        }

        public void searchAndDisplayProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnlCheckIfCatToAddExist.Visible = false;
            pnlEnterCatDetail.Visible = false;
            pnlCheckIfCatToModifyExist.Visible = false;
            pnlModifyCatDetail.Visible = false;
            pnlCheckIfCatToDeleteExist.Visible = false;
            pnlDetailOfCategoryToDelete.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlCheckIfProToAddExist.Visible = false;
            pnlCheckIfProToModifyExist.Visible = false;
            pnlCheckIfProToDeleteExist.Visible = false;
            pnlAddProduct.Visible = false;
            pnlDeleteProduct.Visible = false;
            pnlModifyProduct.Visible = false;
            pnlSearchAndDisplayCatInfo.Visible = false;
            pnlSearchAndDisplayProInfo.Visible = true;
        }

        public void registerNewStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.registerNewStaffToolStripMenuItem_Click(this, e);
        }

        private void loginLogOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.loginLogoutToolStripMenuItem_Click(this, e);
        }

        private void modifyStaffInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.modifyNewStaffToolStripMenuItem_Click(this, e);
        }

        private void searchAndDisplayStaffInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.searchForStaffToolStripMenuItem_Click(this, e);
        }

        private void deleteExistingStaffInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.deleteExistingStaffToolStripMenuItem_Click(this, e);
        }

        private void SupplierMenu_AddSupplier_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.SupplierMenu_AddSupplier_Click(this,e);
        }

        private void SupplierMenu_ModifySupplier_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.SupplierMenu_ModifySupplier_Click(this, e);
        }

        private void SupplierMenu_DeleteSupplier_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.SupplierMenu_DeleteSupplier_Click(this,e);
        }

        private void SupplierMenu_SearchSupplier_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.SupplierMenu_SearchSupplier_Click(this, e);
        }

        private void menuStock_EditProductQty_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.menuStock_EditProductQty_Click(this, e);
        }

        private void menuStock_SearchProductQty_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.menuStock_SearchProductQty_Click(this, e);
        }

        private void menuStock_computeExcessShortFall_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.menuStock_computeExcessShortFall_Click(this, e);
        }

        private void addNewOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.addNewOrderToolStripMenuItem_Click(this, e);
        }

        private void modifyOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.modifyOrderToolStripMenuItem_Click(this, e);
        }

        private void searchAndDisplayOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.searchAndDisplayOrderToolStripMenuItem_Click(this, e);
        }

        private void deleteOrderInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.deleteOrderInformationToolStripMenuItem_Click(this, e);
        }





    }
}