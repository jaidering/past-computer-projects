﻿namespace Assignment_Phase_1
{
    partial class FrmMainStaff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMainStaff));
            this.mnuStrpMain = new System.Windows.Forms.MenuStrip();
            this.staffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginLogoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registerNewStaffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyNewStaffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchForStaffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteExistingStaffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewCategoryInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCategoryInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAndDisplayCategoryInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewProductInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyProductInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteProductInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAndDisplayProductInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAndDisplayOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteOrderInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_AddSupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_ModifySupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_DeleteSupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_SearchSupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStock_EditProductQty = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStock_SearchProductQty = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStock_computeExcessShortFall = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlSearchStaff = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblInformation = new System.Windows.Forms.Label();
            this.lblSearchStaff = new System.Windows.Forms.Label();
            this.grpBoxSearchStaff = new System.Windows.Forms.GroupBox();
            this.tbxStaffName = new System.Windows.Forms.TextBox();
            this.lblStaffName = new System.Windows.Forms.Label();
            this.pnlWelcome = new System.Windows.Forms.Panel();
            this.lblWelcome2 = new System.Windows.Forms.Label();
            this.lblWelcome1 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblInfo = new System.Windows.Forms.Label();
            this.pnlRegisterStaff = new System.Windows.Forms.Panel();
            this.btnCancelRegistration = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSubmitRegistration = new System.Windows.Forms.Button();
            this.grpAccntDetails = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.lblCfmPassword = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblRegisterPassword = new System.Windows.Forms.Label();
            this.rdBtnStaff = new System.Windows.Forms.RadioButton();
            this.rdBtnAdministrator = new System.Windows.Forms.RadioButton();
            this.lblRole = new System.Windows.Forms.Label();
            this.tbxEmail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grpPersonalParticulars = new System.Windows.Forms.GroupBox();
            this.lblHp = new System.Windows.Forms.Label();
            this.lblHome = new System.Windows.Forms.Label();
            this.lblContactNo = new System.Windows.Forms.Label();
            this.tbxHpNo = new System.Windows.Forms.TextBox();
            this.tbxHomeNo = new System.Windows.Forms.TextBox();
            this.tbxPostCode = new System.Windows.Forms.TextBox();
            this.lblPostalCode = new System.Windows.Forms.Label();
            this.lblStreet = new System.Windows.Forms.Label();
            this.tbxAddress = new System.Windows.Forms.TextBox();
            this.lblUnitNo = new System.Windows.Forms.Label();
            this.tbxUnitNo = new System.Windows.Forms.TextBox();
            this.lblBlock = new System.Windows.Forms.Label();
            this.tbxBlockNo = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.rdBtnFemale = new System.Windows.Forms.RadioButton();
            this.rdBtnMale = new System.Windows.Forms.RadioButton();
            this.dtpDateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblDateOfBirth = new System.Windows.Forms.Label();
            this.tbxNRIC = new System.Windows.Forms.TextBox();
            this.lblNRIC = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblInformation1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblRegisterStaff = new System.Windows.Forms.Label();
            this.pnlModifyStaff = new System.Windows.Forms.Panel();
            this.btnCancelModify = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblModifyStaff = new System.Windows.Forms.Label();
            this.btnSubmit1 = new System.Windows.Forms.Button();
            this.grpAccntDetails1 = new System.Windows.Forms.GroupBox();
            this.rdBtnStaff1 = new System.Windows.Forms.RadioButton();
            this.rdBtnAdministrator1 = new System.Windows.Forms.RadioButton();
            this.lblRole1 = new System.Windows.Forms.Label();
            this.tbxEmail1 = new System.Windows.Forms.TextBox();
            this.lblEmail1 = new System.Windows.Forms.Label();
            this.grpPersonalParticular = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblContactNo1 = new System.Windows.Forms.Label();
            this.tbxHpNo1 = new System.Windows.Forms.TextBox();
            this.tbxHomeNo1 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblStreet1 = new System.Windows.Forms.Label();
            this.tbxStreet1 = new System.Windows.Forms.TextBox();
            this.lblUnitNo1 = new System.Windows.Forms.Label();
            this.tbxUnitNo1 = new System.Windows.Forms.TextBox();
            this.lblBlock1 = new System.Windows.Forms.Label();
            this.tbxBlockNo1 = new System.Windows.Forms.TextBox();
            this.lblAddress1 = new System.Windows.Forms.Label();
            this.tbxNRIC1 = new System.Windows.Forms.TextBox();
            this.lblNRIC1 = new System.Windows.Forms.Label();
            this.pnlSearchModifyStaff = new System.Windows.Forms.Panel();
            this.btnCancelSearch = new System.Windows.Forms.Button();
            this.btnSearchForStaff = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.lblSearchModifyStaffInfo = new System.Windows.Forms.Label();
            this.lblSearchModifyStaff = new System.Windows.Forms.Label();
            this.grpBoxSearchModifyStaff = new System.Windows.Forms.GroupBox();
            this.tbxSearchName = new System.Windows.Forms.TextBox();
            this.lblSearchName = new System.Windows.Forms.Label();
            this.pnlSearchAndDisplay = new System.Windows.Forms.Panel();
            this.btnCancelDisplay = new System.Windows.Forms.Button();
            this.btnDisplayStaffInfo = new System.Windows.Forms.Button();
            this.grpBoxSearchDisplayStaffInfo = new System.Windows.Forms.GroupBox();
            this.tbxDisplayStaffInfo = new System.Windows.Forms.TextBox();
            this.lblStaffInformation = new System.Windows.Forms.Label();
            this.tbxSelectStaffInfo = new System.Windows.Forms.TextBox();
            this.lblEnterInformation = new System.Windows.Forms.Label();
            this.lblTypeOfSearch = new System.Windows.Forms.Label();
            this.cmBoxTypeofSearch = new System.Windows.Forms.ComboBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.lblSearchDisplayInfo = new System.Windows.Forms.Label();
            this.lblSearchDisp = new System.Windows.Forms.Label();
            this.pnlSearchDeleteStaff = new System.Windows.Forms.Panel();
            this.btnCancelSearchDelete = new System.Windows.Forms.Button();
            this.btnSearchDeleteStaff = new System.Windows.Forms.Button();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.lblSearchDeleteInfo = new System.Windows.Forms.Label();
            this.lblSearchDeleteStaff = new System.Windows.Forms.Label();
            this.grpBoxSearchDeleteStaff = new System.Windows.Forms.GroupBox();
            this.tbxSearchDeleteStaff = new System.Windows.Forms.TextBox();
            this.lblSearchDeleteStaffName = new System.Windows.Forms.Label();
            this.pnlDeleteStaffInfo = new System.Windows.Forms.Panel();
            this.btnCancelDeleteStaff = new System.Windows.Forms.Button();
            this.btnDeleteStaff = new System.Windows.Forms.Button();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.lblDeleteInfo = new System.Windows.Forms.Label();
            this.lblDeleteStaffInformation = new System.Windows.Forms.Label();
            this.grpBoxDelStaff = new System.Windows.Forms.GroupBox();
            this.tbxDeleteStaff = new System.Windows.Forms.TextBox();
            this.lblDeleteStaffName = new System.Windows.Forms.Label();
            this.pnlAddNewOrder = new System.Windows.Forms.Panel();
            this.btnCancelAddNewOrder = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.grpOrderItemInfo = new System.Windows.Forms.GroupBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.lblAddOrderQty2 = new System.Windows.Forms.Label();
            this.tbxQuantityAddOrder2 = new System.Windows.Forms.TextBox();
            this.tbxTitleAddOrder2 = new System.Windows.Forms.TextBox();
            this.lblItemAddOrder2 = new System.Windows.Forms.Label();
            this.lblItemAddOrder1 = new System.Windows.Forms.Label();
            this.lblAddOrderInfo = new System.Windows.Forms.Label();
            this.tbxNRIC3 = new System.Windows.Forms.TextBox();
            this.lblAddItemQty1 = new System.Windows.Forms.Label();
            this.grpBoxAddOrderInfo = new System.Windows.Forms.GroupBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.lblAddPostCode = new System.Windows.Forms.Label();
            this.lblAddStreet = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.lblAddUnitNum = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.lblAddBlk = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.lblAddAddress = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lblAddDateOfOrder = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.lblAddNameOfCust = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.lblAddORNum = new System.Windows.Forms.Label();
            this.btnAddOrder = new System.Windows.Forms.Button();
            this.lblAddNewOrderInfo = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.lblAddNewOrder = new System.Windows.Forms.Label();
            this.pnlSearchAddOrder = new System.Windows.Forms.Panel();
            this.btnCancelAddOrder = new System.Windows.Forms.Button();
            this.btnSearchAddOrder = new System.Windows.Forms.Button();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.lblODRnum = new System.Windows.Forms.Label();
            this.lblSearchAddOrder = new System.Windows.Forms.Label();
            this.grpBoxSearchAddOrder = new System.Windows.Forms.GroupBox();
            this.tbxSearchAddOrder = new System.Windows.Forms.TextBox();
            this.lblOrderRefNum = new System.Windows.Forms.Label();
            this.pnlSearchDisplayOrderInfo = new System.Windows.Forms.Panel();
            this.btnCancelDisplayOrderInfo = new System.Windows.Forms.Button();
            this.btnDisplayOrderInformation = new System.Windows.Forms.Button();
            this.grpBoxSearchDisplayOrderInfo = new System.Windows.Forms.GroupBox();
            this.tbxOrderDisplay = new System.Windows.Forms.TextBox();
            this.lblOrderDisplay = new System.Windows.Forms.Label();
            this.tbxOrderInformation = new System.Windows.Forms.TextBox();
            this.lblOrderSelectInfo = new System.Windows.Forms.Label();
            this.lblSelectSearchMode = new System.Windows.Forms.Label();
            this.cmBoxOrderModeOfSearch = new System.Windows.Forms.ComboBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lblSearchDisplayOrder = new System.Windows.Forms.Label();
            this.pnlModifyOrder = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.lblSearchModifyORNum = new System.Windows.Forms.Label();
            this.lblSearchModifyOrder = new System.Windows.Forms.Label();
            this.grpboxSearchExisting = new System.Windows.Forms.GroupBox();
            this.tbxSearchModifyORnum = new System.Windows.Forms.TextBox();
            this.lblSearchMofidyOrderRefNUm = new System.Windows.Forms.Label();
            this.pnlModifyOrderInfo = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.label25 = new System.Windows.Forms.Label();
            this.pnlDeleteOrder = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbxORNum = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.pnlDeleteSearchOrder = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbxORNumSearch = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.mnuStrpMain.SuspendLayout();
            this.pnlSearchStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grpBoxSearchStaff.SuspendLayout();
            this.pnlWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.pnlRegisterStaff.SuspendLayout();
            this.grpAccntDetails.SuspendLayout();
            this.grpPersonalParticulars.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pnlModifyStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.grpAccntDetails1.SuspendLayout();
            this.grpPersonalParticular.SuspendLayout();
            this.pnlSearchModifyStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.grpBoxSearchModifyStaff.SuspendLayout();
            this.pnlSearchAndDisplay.SuspendLayout();
            this.grpBoxSearchDisplayStaffInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.pnlSearchDeleteStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.grpBoxSearchDeleteStaff.SuspendLayout();
            this.pnlDeleteStaffInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.grpBoxDelStaff.SuspendLayout();
            this.pnlAddNewOrder.SuspendLayout();
            this.grpOrderItemInfo.SuspendLayout();
            this.grpBoxAddOrderInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.pnlSearchAddOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.grpBoxSearchAddOrder.SuspendLayout();
            this.pnlSearchDisplayOrderInfo.SuspendLayout();
            this.grpBoxSearchDisplayOrderInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.pnlModifyOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.grpboxSearchExisting.SuspendLayout();
            this.pnlModifyOrderInfo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.pnlDeleteOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.pnlDeleteSearchOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuStrpMain
            // 
            this.mnuStrpMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.staffToolStripMenuItem,
            this.catalogueToolStripMenuItem,
            this.toolStripMenuItem1,
            this.orderToolStripMenuItem,
            this.toolStripMenuItem2,
            this.toolStripMenuItem7});
            this.mnuStrpMain.Location = new System.Drawing.Point(0, 0);
            this.mnuStrpMain.Name = "mnuStrpMain";
            this.mnuStrpMain.Size = new System.Drawing.Size(584, 24);
            this.mnuStrpMain.TabIndex = 41;
            this.mnuStrpMain.Text = "menuStrip1";
            // 
            // staffToolStripMenuItem
            // 
            this.staffToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginLogoutToolStripMenuItem,
            this.registerNewStaffToolStripMenuItem,
            this.modifyNewStaffToolStripMenuItem,
            this.searchForStaffToolStripMenuItem,
            this.deleteExistingStaffToolStripMenuItem});
            this.staffToolStripMenuItem.Name = "staffToolStripMenuItem";
            this.staffToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.staffToolStripMenuItem.Text = "Staff";
            // 
            // loginLogoutToolStripMenuItem
            // 
            this.loginLogoutToolStripMenuItem.Name = "loginLogoutToolStripMenuItem";
            this.loginLogoutToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.loginLogoutToolStripMenuItem.Text = "Login/Logout";
            this.loginLogoutToolStripMenuItem.Click += new System.EventHandler(this.loginLogoutToolStripMenuItem_Click);
            // 
            // registerNewStaffToolStripMenuItem
            // 
            this.registerNewStaffToolStripMenuItem.Name = "registerNewStaffToolStripMenuItem";
            this.registerNewStaffToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.registerNewStaffToolStripMenuItem.Text = "Register New Staff";
            this.registerNewStaffToolStripMenuItem.Click += new System.EventHandler(this.registerNewStaffToolStripMenuItem_Click);
            // 
            // modifyNewStaffToolStripMenuItem
            // 
            this.modifyNewStaffToolStripMenuItem.Name = "modifyNewStaffToolStripMenuItem";
            this.modifyNewStaffToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.modifyNewStaffToolStripMenuItem.Text = "Modify Staff Information";
            this.modifyNewStaffToolStripMenuItem.Click += new System.EventHandler(this.modifyNewStaffToolStripMenuItem_Click);
            // 
            // searchForStaffToolStripMenuItem
            // 
            this.searchForStaffToolStripMenuItem.Name = "searchForStaffToolStripMenuItem";
            this.searchForStaffToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.searchForStaffToolStripMenuItem.Text = "Search and Display Staff Information";
            this.searchForStaffToolStripMenuItem.Click += new System.EventHandler(this.searchForStaffToolStripMenuItem_Click);
            // 
            // deleteExistingStaffToolStripMenuItem
            // 
            this.deleteExistingStaffToolStripMenuItem.Name = "deleteExistingStaffToolStripMenuItem";
            this.deleteExistingStaffToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.deleteExistingStaffToolStripMenuItem.Text = "Delete Existing Staff Information";
            this.deleteExistingStaffToolStripMenuItem.Click += new System.EventHandler(this.deleteExistingStaffToolStripMenuItem_Click);
            // 
            // catalogueToolStripMenuItem
            // 
            this.catalogueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewCategoryInfoToolStripMenuItem,
            this.modifyCategoryToolStripMenuItem,
            this.deleteCategoryInfoToolStripMenuItem,
            this.searchAndDisplayCategoryInfoToolStripMenuItem});
            this.catalogueToolStripMenuItem.Name = "catalogueToolStripMenuItem";
            this.catalogueToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.catalogueToolStripMenuItem.Text = "Category";
            // 
            // addNewCategoryInfoToolStripMenuItem
            // 
            this.addNewCategoryInfoToolStripMenuItem.Name = "addNewCategoryInfoToolStripMenuItem";
            this.addNewCategoryInfoToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.addNewCategoryInfoToolStripMenuItem.Text = "Add New Category Info";
            this.addNewCategoryInfoToolStripMenuItem.Click += new System.EventHandler(this.addNewCategoryInfoToolStripMenuItem_Click);
            // 
            // modifyCategoryToolStripMenuItem
            // 
            this.modifyCategoryToolStripMenuItem.Name = "modifyCategoryToolStripMenuItem";
            this.modifyCategoryToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.modifyCategoryToolStripMenuItem.Text = "Modify Category Info";
            this.modifyCategoryToolStripMenuItem.Click += new System.EventHandler(this.modifyCategoryToolStripMenuItem_Click);
            // 
            // deleteCategoryInfoToolStripMenuItem
            // 
            this.deleteCategoryInfoToolStripMenuItem.Name = "deleteCategoryInfoToolStripMenuItem";
            this.deleteCategoryInfoToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.deleteCategoryInfoToolStripMenuItem.Text = "Delete Category Info";
            this.deleteCategoryInfoToolStripMenuItem.Click += new System.EventHandler(this.deleteCategoryInfoToolStripMenuItem_Click);
            // 
            // searchAndDisplayCategoryInfoToolStripMenuItem
            // 
            this.searchAndDisplayCategoryInfoToolStripMenuItem.Name = "searchAndDisplayCategoryInfoToolStripMenuItem";
            this.searchAndDisplayCategoryInfoToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.searchAndDisplayCategoryInfoToolStripMenuItem.Text = "Search and Display Category Info";
            this.searchAndDisplayCategoryInfoToolStripMenuItem.Click += new System.EventHandler(this.searchAndDisplayCategoryInfoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewProductInfoToolStripMenuItem,
            this.modifyProductInfoToolStripMenuItem,
            this.deleteProductInfoToolStripMenuItem,
            this.searchAndDisplayProductInfoToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem1.Text = "Product";
            // 
            // addNewProductInfoToolStripMenuItem
            // 
            this.addNewProductInfoToolStripMenuItem.Name = "addNewProductInfoToolStripMenuItem";
            this.addNewProductInfoToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.addNewProductInfoToolStripMenuItem.Text = "Add New Product Info";
            this.addNewProductInfoToolStripMenuItem.Click += new System.EventHandler(this.addNewProductInfoToolStripMenuItem_Click);
            // 
            // modifyProductInfoToolStripMenuItem
            // 
            this.modifyProductInfoToolStripMenuItem.Name = "modifyProductInfoToolStripMenuItem";
            this.modifyProductInfoToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.modifyProductInfoToolStripMenuItem.Text = "Modify Product Info";
            this.modifyProductInfoToolStripMenuItem.Click += new System.EventHandler(this.modifyProductInfoToolStripMenuItem_Click);
            // 
            // deleteProductInfoToolStripMenuItem
            // 
            this.deleteProductInfoToolStripMenuItem.Name = "deleteProductInfoToolStripMenuItem";
            this.deleteProductInfoToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.deleteProductInfoToolStripMenuItem.Text = "Delete Product Info";
            this.deleteProductInfoToolStripMenuItem.Click += new System.EventHandler(this.deleteProductInfoToolStripMenuItem_Click);
            // 
            // searchAndDisplayProductInfoToolStripMenuItem
            // 
            this.searchAndDisplayProductInfoToolStripMenuItem.Name = "searchAndDisplayProductInfoToolStripMenuItem";
            this.searchAndDisplayProductInfoToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.searchAndDisplayProductInfoToolStripMenuItem.Text = "Search and Display Product Info";
            this.searchAndDisplayProductInfoToolStripMenuItem.Click += new System.EventHandler(this.searchAndDisplayProductInfoToolStripMenuItem_Click);
            // 
            // orderToolStripMenuItem
            // 
            this.orderToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewOrderToolStripMenuItem,
            this.modifyOrderToolStripMenuItem,
            this.searchAndDisplayOrderToolStripMenuItem,
            this.deleteOrderInformationToolStripMenuItem});
            this.orderToolStripMenuItem.Name = "orderToolStripMenuItem";
            this.orderToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.orderToolStripMenuItem.Text = "Order";
            // 
            // addNewOrderToolStripMenuItem
            // 
            this.addNewOrderToolStripMenuItem.Name = "addNewOrderToolStripMenuItem";
            this.addNewOrderToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.addNewOrderToolStripMenuItem.Text = "Add New Order Information";
            this.addNewOrderToolStripMenuItem.Click += new System.EventHandler(this.addNewOrderToolStripMenuItem_Click);
            // 
            // modifyOrderToolStripMenuItem
            // 
            this.modifyOrderToolStripMenuItem.Name = "modifyOrderToolStripMenuItem";
            this.modifyOrderToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.modifyOrderToolStripMenuItem.Text = "Modify Existing Order Information";
            this.modifyOrderToolStripMenuItem.Click += new System.EventHandler(this.modifyOrderToolStripMenuItem_Click);
            // 
            // searchAndDisplayOrderToolStripMenuItem
            // 
            this.searchAndDisplayOrderToolStripMenuItem.Name = "searchAndDisplayOrderToolStripMenuItem";
            this.searchAndDisplayOrderToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.searchAndDisplayOrderToolStripMenuItem.Text = "Search and Display Order Information";
            this.searchAndDisplayOrderToolStripMenuItem.Click += new System.EventHandler(this.searchAndDisplayOrderToolStripMenuItem_Click);
            // 
            // deleteOrderInformationToolStripMenuItem
            // 
            this.deleteOrderInformationToolStripMenuItem.Name = "deleteOrderInformationToolStripMenuItem";
            this.deleteOrderInformationToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.deleteOrderInformationToolStripMenuItem.Text = "Delete Order Information";
            this.deleteOrderInformationToolStripMenuItem.Click += new System.EventHandler(this.deleteOrderInformationToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SupplierMenu_AddSupplier,
            this.SupplierMenu_ModifySupplier,
            this.SupplierMenu_DeleteSupplier,
            this.SupplierMenu_SearchSupplier});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(69, 20);
            this.toolStripMenuItem2.Text = "Inventory";
            // 
            // SupplierMenu_AddSupplier
            // 
            this.SupplierMenu_AddSupplier.Name = "SupplierMenu_AddSupplier";
            this.SupplierMenu_AddSupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_AddSupplier.Text = "Add Supplier";
            this.SupplierMenu_AddSupplier.Click += new System.EventHandler(this.SupplierMenu_AddSupplier_Click);
            // 
            // SupplierMenu_ModifySupplier
            // 
            this.SupplierMenu_ModifySupplier.Name = "SupplierMenu_ModifySupplier";
            this.SupplierMenu_ModifySupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_ModifySupplier.Text = "Modify Supplier";
            this.SupplierMenu_ModifySupplier.Click += new System.EventHandler(this.SupplierMenu_ModifySupplier_Click);
            // 
            // SupplierMenu_DeleteSupplier
            // 
            this.SupplierMenu_DeleteSupplier.Name = "SupplierMenu_DeleteSupplier";
            this.SupplierMenu_DeleteSupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_DeleteSupplier.Text = "Delete Supplier";
            this.SupplierMenu_DeleteSupplier.Click += new System.EventHandler(this.SupplierMenu_DeleteSupplier_Click);
            // 
            // SupplierMenu_SearchSupplier
            // 
            this.SupplierMenu_SearchSupplier.Name = "SupplierMenu_SearchSupplier";
            this.SupplierMenu_SearchSupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_SearchSupplier.Text = "Search Supplier";
            this.SupplierMenu_SearchSupplier.Click += new System.EventHandler(this.SupplierMenu_SearchSupplier_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuStock_EditProductQty,
            this.menuStock_SearchProductQty,
            this.menuStock_computeExcessShortFall});
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(48, 20);
            this.toolStripMenuItem7.Text = "Stock";
            // 
            // menuStock_EditProductQty
            // 
            this.menuStock_EditProductQty.Name = "menuStock_EditProductQty";
            this.menuStock_EditProductQty.Size = new System.Drawing.Size(209, 22);
            this.menuStock_EditProductQty.Text = "Edit Product Qty";
            this.menuStock_EditProductQty.Click += new System.EventHandler(this.menuStock_EditProductQty_Click);
            // 
            // menuStock_SearchProductQty
            // 
            this.menuStock_SearchProductQty.Name = "menuStock_SearchProductQty";
            this.menuStock_SearchProductQty.Size = new System.Drawing.Size(209, 22);
            this.menuStock_SearchProductQty.Text = "Search Product Qty";
            this.menuStock_SearchProductQty.Click += new System.EventHandler(this.menuStock_SearchProductQty_Click);
            // 
            // menuStock_computeExcessShortFall
            // 
            this.menuStock_computeExcessShortFall.Name = "menuStock_computeExcessShortFall";
            this.menuStock_computeExcessShortFall.Size = new System.Drawing.Size(209, 22);
            this.menuStock_computeExcessShortFall.Text = "Compute Excess/Shortfall";
            this.menuStock_computeExcessShortFall.Click += new System.EventHandler(this.menuStock_computeExcessShortFall_Click);
            // 
            // pnlSearchStaff
            // 
            this.pnlSearchStaff.AutoScroll = true;
            this.pnlSearchStaff.Controls.Add(this.btnCancel);
            this.pnlSearchStaff.Controls.Add(this.btnSearch);
            this.pnlSearchStaff.Controls.Add(this.pictureBox1);
            this.pnlSearchStaff.Controls.Add(this.lblInformation);
            this.pnlSearchStaff.Controls.Add(this.lblSearchStaff);
            this.pnlSearchStaff.Controls.Add(this.grpBoxSearchStaff);
            this.pnlSearchStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchStaff.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchStaff.Name = "pnlSearchStaff";
            this.pnlSearchStaff.Size = new System.Drawing.Size(584, 444);
            this.pnlSearchStaff.TabIndex = 45;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(415, 350);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 39;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(312, 350);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 38;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(452, 47);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 81);
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            // 
            // lblInformation
            // 
            this.lblInformation.AutoSize = true;
            this.lblInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInformation.Location = new System.Drawing.Point(60, 120);
            this.lblInformation.Name = "lblInformation";
            this.lblInformation.Size = new System.Drawing.Size(231, 16);
            this.lblInformation.TabIndex = 0;
            this.lblInformation.Text = "Please enter a Staff Name to search...";
            // 
            // lblSearchStaff
            // 
            this.lblSearchStaff.AutoSize = true;
            this.lblSearchStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchStaff.Location = new System.Drawing.Point(58, 49);
            this.lblSearchStaff.Name = "lblSearchStaff";
            this.lblSearchStaff.Size = new System.Drawing.Size(142, 25);
            this.lblSearchStaff.TabIndex = 36;
            this.lblSearchStaff.Text = "Register Staff";
            this.lblSearchStaff.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpBoxSearchStaff
            // 
            this.grpBoxSearchStaff.Controls.Add(this.tbxStaffName);
            this.grpBoxSearchStaff.Controls.Add(this.lblStaffName);
            this.grpBoxSearchStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxSearchStaff.Location = new System.Drawing.Point(63, 160);
            this.grpBoxSearchStaff.Name = "grpBoxSearchStaff";
            this.grpBoxSearchStaff.Size = new System.Drawing.Size(401, 173);
            this.grpBoxSearchStaff.TabIndex = 35;
            this.grpBoxSearchStaff.TabStop = false;
            this.grpBoxSearchStaff.Text = "Search Staff";
            // 
            // tbxStaffName
            // 
            this.tbxStaffName.Location = new System.Drawing.Point(36, 80);
            this.tbxStaffName.Name = "tbxStaffName";
            this.tbxStaffName.Size = new System.Drawing.Size(256, 22);
            this.tbxStaffName.TabIndex = 1;
            // 
            // lblStaffName
            // 
            this.lblStaffName.AutoSize = true;
            this.lblStaffName.Location = new System.Drawing.Point(33, 52);
            this.lblStaffName.Name = "lblStaffName";
            this.lblStaffName.Size = new System.Drawing.Size(74, 16);
            this.lblStaffName.TabIndex = 0;
            this.lblStaffName.Text = "Staff name:";
            // 
            // pnlWelcome
            // 
            this.pnlWelcome.Controls.Add(this.lblWelcome2);
            this.pnlWelcome.Controls.Add(this.lblWelcome1);
            this.pnlWelcome.Controls.Add(this.pictureBox6);
            this.pnlWelcome.Controls.Add(this.lblInfo);
            this.pnlWelcome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlWelcome.Location = new System.Drawing.Point(0, 0);
            this.pnlWelcome.Name = "pnlWelcome";
            this.pnlWelcome.Size = new System.Drawing.Size(584, 444);
            this.pnlWelcome.TabIndex = 46;
            // 
            // lblWelcome2
            // 
            this.lblWelcome2.AutoSize = true;
            this.lblWelcome2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblWelcome2.Location = new System.Drawing.Point(193, 330);
            this.lblWelcome2.Name = "lblWelcome2";
            this.lblWelcome2.Size = new System.Drawing.Size(194, 24);
            this.lblWelcome2.TabIndex = 10;
            this.lblWelcome2.Text = " E-Business System";
            // 
            // lblWelcome1
            // 
            this.lblWelcome1.AutoSize = true;
            this.lblWelcome1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblWelcome1.Location = new System.Drawing.Point(95, 63);
            this.lblWelcome1.Name = "lblWelcome1";
            this.lblWelcome1.Size = new System.Drawing.Size(397, 24);
            this.lblWelcome1.TabIndex = 9;
            this.lblWelcome1.Text = "Welcome to the Electronic Gadget Online";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(134, 59);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(309, 280);
            this.pictureBox6.TabIndex = 12;
            this.pictureBox6.TabStop = false;
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(6, 396);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(250, 13);
            this.lblInfo.TabIndex = 11;
            this.lblInfo.Text = "Select an option from the above menu to proceed...";
            // 
            // pnlRegisterStaff
            // 
            this.pnlRegisterStaff.AutoScroll = true;
            this.pnlRegisterStaff.Controls.Add(this.btnCancelRegistration);
            this.pnlRegisterStaff.Controls.Add(this.label2);
            this.pnlRegisterStaff.Controls.Add(this.btnSubmitRegistration);
            this.pnlRegisterStaff.Controls.Add(this.grpAccntDetails);
            this.pnlRegisterStaff.Controls.Add(this.grpPersonalParticulars);
            this.pnlRegisterStaff.Controls.Add(this.lblInformation1);
            this.pnlRegisterStaff.Controls.Add(this.pictureBox2);
            this.pnlRegisterStaff.Controls.Add(this.lblRegisterStaff);
            this.pnlRegisterStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRegisterStaff.Location = new System.Drawing.Point(0, 0);
            this.pnlRegisterStaff.Name = "pnlRegisterStaff";
            this.pnlRegisterStaff.Size = new System.Drawing.Size(584, 444);
            this.pnlRegisterStaff.TabIndex = 47;
            // 
            // btnCancelRegistration
            // 
            this.btnCancelRegistration.Location = new System.Drawing.Point(467, 963);
            this.btnCancelRegistration.Name = "btnCancelRegistration";
            this.btnCancelRegistration.Size = new System.Drawing.Size(75, 23);
            this.btnCancelRegistration.TabIndex = 44;
            this.btnCancelRegistration.Text = "Cancel";
            this.btnCancelRegistration.UseVisualStyleBackColor = true;
            this.btnCancelRegistration.Click += new System.EventHandler(this.btnCancelRegistration_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 985);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "* Denotes Mandatory field";
            // 
            // btnSubmitRegistration
            // 
            this.btnSubmitRegistration.Location = new System.Drawing.Point(361, 963);
            this.btnSubmitRegistration.Name = "btnSubmitRegistration";
            this.btnSubmitRegistration.Size = new System.Drawing.Size(75, 23);
            this.btnSubmitRegistration.TabIndex = 42;
            this.btnSubmitRegistration.Text = "Submit";
            this.btnSubmitRegistration.UseVisualStyleBackColor = true;
            // 
            // grpAccntDetails
            // 
            this.grpAccntDetails.Controls.Add(this.textBox3);
            this.grpAccntDetails.Controls.Add(this.lblCfmPassword);
            this.grpAccntDetails.Controls.Add(this.textBox2);
            this.grpAccntDetails.Controls.Add(this.lblRegisterPassword);
            this.grpAccntDetails.Controls.Add(this.rdBtnStaff);
            this.grpAccntDetails.Controls.Add(this.rdBtnAdministrator);
            this.grpAccntDetails.Controls.Add(this.lblRole);
            this.grpAccntDetails.Controls.Add(this.tbxEmail);
            this.grpAccntDetails.Controls.Add(this.label3);
            this.grpAccntDetails.Location = new System.Drawing.Point(50, 711);
            this.grpAccntDetails.Name = "grpAccntDetails";
            this.grpAccntDetails.Size = new System.Drawing.Size(393, 243);
            this.grpAccntDetails.TabIndex = 41;
            this.grpAccntDetails.TabStop = false;
            this.grpAccntDetails.Text = "Account Details";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(23, 90);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(209, 20);
            this.textBox3.TabIndex = 29;
            // 
            // lblCfmPassword
            // 
            this.lblCfmPassword.AutoSize = true;
            this.lblCfmPassword.Location = new System.Drawing.Point(23, 71);
            this.lblCfmPassword.Name = "lblCfmPassword";
            this.lblCfmPassword.Size = new System.Drawing.Size(94, 13);
            this.lblCfmPassword.TabIndex = 28;
            this.lblCfmPassword.Text = "Confirm Password:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(24, 43);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(209, 20);
            this.textBox2.TabIndex = 27;
            // 
            // lblRegisterPassword
            // 
            this.lblRegisterPassword.AutoSize = true;
            this.lblRegisterPassword.Location = new System.Drawing.Point(24, 24);
            this.lblRegisterPassword.Name = "lblRegisterPassword";
            this.lblRegisterPassword.Size = new System.Drawing.Size(56, 13);
            this.lblRegisterPassword.TabIndex = 26;
            this.lblRegisterPassword.Text = "Password:";
            // 
            // rdBtnStaff
            // 
            this.rdBtnStaff.AutoSize = true;
            this.rdBtnStaff.Location = new System.Drawing.Point(27, 187);
            this.rdBtnStaff.Name = "rdBtnStaff";
            this.rdBtnStaff.Size = new System.Drawing.Size(47, 17);
            this.rdBtnStaff.TabIndex = 25;
            this.rdBtnStaff.TabStop = true;
            this.rdBtnStaff.Text = "Staff";
            this.rdBtnStaff.UseVisualStyleBackColor = true;
            // 
            // rdBtnAdministrator
            // 
            this.rdBtnAdministrator.AutoSize = true;
            this.rdBtnAdministrator.Location = new System.Drawing.Point(27, 210);
            this.rdBtnAdministrator.Name = "rdBtnAdministrator";
            this.rdBtnAdministrator.Size = new System.Drawing.Size(85, 17);
            this.rdBtnAdministrator.TabIndex = 24;
            this.rdBtnAdministrator.TabStop = true;
            this.rdBtnAdministrator.Text = "Administrator";
            this.rdBtnAdministrator.UseVisualStyleBackColor = true;
            // 
            // lblRole
            // 
            this.lblRole.AutoSize = true;
            this.lblRole.Location = new System.Drawing.Point(23, 168);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(102, 13);
            this.lblRole.TabIndex = 23;
            this.lblRole.Text = "* Role of Registrant:";
            // 
            // tbxEmail
            // 
            this.tbxEmail.Location = new System.Drawing.Point(25, 138);
            this.tbxEmail.Name = "tbxEmail";
            this.tbxEmail.Size = new System.Drawing.Size(209, 20);
            this.tbxEmail.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "E-mail Address:";
            // 
            // grpPersonalParticulars
            // 
            this.grpPersonalParticulars.Controls.Add(this.lblHp);
            this.grpPersonalParticulars.Controls.Add(this.lblHome);
            this.grpPersonalParticulars.Controls.Add(this.lblContactNo);
            this.grpPersonalParticulars.Controls.Add(this.tbxHpNo);
            this.grpPersonalParticulars.Controls.Add(this.tbxHomeNo);
            this.grpPersonalParticulars.Controls.Add(this.tbxPostCode);
            this.grpPersonalParticulars.Controls.Add(this.lblPostalCode);
            this.grpPersonalParticulars.Controls.Add(this.lblStreet);
            this.grpPersonalParticulars.Controls.Add(this.tbxAddress);
            this.grpPersonalParticulars.Controls.Add(this.lblUnitNo);
            this.grpPersonalParticulars.Controls.Add(this.tbxUnitNo);
            this.grpPersonalParticulars.Controls.Add(this.lblBlock);
            this.grpPersonalParticulars.Controls.Add(this.tbxBlockNo);
            this.grpPersonalParticulars.Controls.Add(this.lblAddress);
            this.grpPersonalParticulars.Controls.Add(this.rdBtnFemale);
            this.grpPersonalParticulars.Controls.Add(this.rdBtnMale);
            this.grpPersonalParticulars.Controls.Add(this.dtpDateOfBirth);
            this.grpPersonalParticulars.Controls.Add(this.lblGender);
            this.grpPersonalParticulars.Controls.Add(this.lblDateOfBirth);
            this.grpPersonalParticulars.Controls.Add(this.tbxNRIC);
            this.grpPersonalParticulars.Controls.Add(this.lblNRIC);
            this.grpPersonalParticulars.Controls.Add(this.textBox1);
            this.grpPersonalParticulars.Controls.Add(this.label4);
            this.grpPersonalParticulars.Location = new System.Drawing.Point(50, 171);
            this.grpPersonalParticulars.Name = "grpPersonalParticulars";
            this.grpPersonalParticulars.Size = new System.Drawing.Size(393, 531);
            this.grpPersonalParticulars.TabIndex = 40;
            this.grpPersonalParticulars.TabStop = false;
            this.grpPersonalParticulars.Text = "Personal Particulars";
            // 
            // lblHp
            // 
            this.lblHp.AutoSize = true;
            this.lblHp.Location = new System.Drawing.Point(242, 306);
            this.lblHp.Name = "lblHp";
            this.lblHp.Size = new System.Drawing.Size(69, 13);
            this.lblHp.TabIndex = 36;
            this.lblHp.Text = "(Handphone)";
            // 
            // lblHome
            // 
            this.lblHome.AutoSize = true;
            this.lblHome.Location = new System.Drawing.Point(242, 280);
            this.lblHome.Name = "lblHome";
            this.lblHome.Size = new System.Drawing.Size(41, 13);
            this.lblHome.TabIndex = 35;
            this.lblHome.Text = "(Home)";
            // 
            // lblContactNo
            // 
            this.lblContactNo.AutoSize = true;
            this.lblContactNo.Location = new System.Drawing.Point(26, 252);
            this.lblContactNo.Name = "lblContactNo";
            this.lblContactNo.Size = new System.Drawing.Size(99, 13);
            this.lblContactNo.TabIndex = 34;
            this.lblContactNo.Text = "* Contact Numbers:";
            // 
            // tbxHpNo
            // 
            this.tbxHpNo.Location = new System.Drawing.Point(27, 303);
            this.tbxHpNo.Name = "tbxHpNo";
            this.tbxHpNo.Size = new System.Drawing.Size(209, 20);
            this.tbxHpNo.TabIndex = 33;
            // 
            // tbxHomeNo
            // 
            this.tbxHomeNo.Location = new System.Drawing.Point(27, 277);
            this.tbxHomeNo.Name = "tbxHomeNo";
            this.tbxHomeNo.Size = new System.Drawing.Size(209, 20);
            this.tbxHomeNo.TabIndex = 32;
            // 
            // tbxPostCode
            // 
            this.tbxPostCode.Location = new System.Drawing.Point(25, 498);
            this.tbxPostCode.Name = "tbxPostCode";
            this.tbxPostCode.Size = new System.Drawing.Size(75, 20);
            this.tbxPostCode.TabIndex = 30;
            // 
            // lblPostalCode
            // 
            this.lblPostalCode.AutoSize = true;
            this.lblPostalCode.Location = new System.Drawing.Point(22, 477);
            this.lblPostalCode.Name = "lblPostalCode";
            this.lblPostalCode.Size = new System.Drawing.Size(67, 13);
            this.lblPostalCode.TabIndex = 29;
            this.lblPostalCode.Text = "Postal Code:";
            // 
            // lblStreet
            // 
            this.lblStreet.AutoSize = true;
            this.lblStreet.Location = new System.Drawing.Point(24, 394);
            this.lblStreet.Name = "lblStreet";
            this.lblStreet.Size = new System.Drawing.Size(38, 13);
            this.lblStreet.TabIndex = 28;
            this.lblStreet.Text = "Street:";
            // 
            // tbxAddress
            // 
            this.tbxAddress.Location = new System.Drawing.Point(25, 419);
            this.tbxAddress.Multiline = true;
            this.tbxAddress.Name = "tbxAddress";
            this.tbxAddress.Size = new System.Drawing.Size(209, 48);
            this.tbxAddress.TabIndex = 26;
            // 
            // lblUnitNo
            // 
            this.lblUnitNo.AutoSize = true;
            this.lblUnitNo.Location = new System.Drawing.Point(106, 365);
            this.lblUnitNo.Name = "lblUnitNo";
            this.lblUnitNo.Size = new System.Drawing.Size(49, 13);
            this.lblUnitNo.TabIndex = 21;
            this.lblUnitNo.Text = "Unit No.:";
            // 
            // tbxUnitNo
            // 
            this.tbxUnitNo.Location = new System.Drawing.Point(155, 362);
            this.tbxUnitNo.Name = "tbxUnitNo";
            this.tbxUnitNo.Size = new System.Drawing.Size(79, 20);
            this.tbxUnitNo.TabIndex = 20;
            // 
            // lblBlock
            // 
            this.lblBlock.AutoSize = true;
            this.lblBlock.Location = new System.Drawing.Point(24, 365);
            this.lblBlock.Name = "lblBlock";
            this.lblBlock.Size = new System.Drawing.Size(35, 13);
            this.lblBlock.TabIndex = 19;
            this.lblBlock.Text = "Blk: #";
            // 
            // tbxBlockNo
            // 
            this.tbxBlockNo.Location = new System.Drawing.Point(60, 362);
            this.tbxBlockNo.Name = "tbxBlockNo";
            this.tbxBlockNo.Size = new System.Drawing.Size(40, 20);
            this.tbxBlockNo.TabIndex = 18;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(24, 337);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(55, 13);
            this.lblAddress.TabIndex = 17;
            this.lblAddress.Text = "* Address:";
            // 
            // rdBtnFemale
            // 
            this.rdBtnFemale.AutoSize = true;
            this.rdBtnFemale.Location = new System.Drawing.Point(111, 218);
            this.rdBtnFemale.Name = "rdBtnFemale";
            this.rdBtnFemale.Size = new System.Drawing.Size(59, 17);
            this.rdBtnFemale.TabIndex = 16;
            this.rdBtnFemale.TabStop = true;
            this.rdBtnFemale.Text = "Female";
            this.rdBtnFemale.UseVisualStyleBackColor = true;
            // 
            // rdBtnMale
            // 
            this.rdBtnMale.AutoSize = true;
            this.rdBtnMale.Location = new System.Drawing.Point(29, 218);
            this.rdBtnMale.Name = "rdBtnMale";
            this.rdBtnMale.Size = new System.Drawing.Size(48, 17);
            this.rdBtnMale.TabIndex = 15;
            this.rdBtnMale.TabStop = true;
            this.rdBtnMale.Text = "Male";
            this.rdBtnMale.UseVisualStyleBackColor = true;
            // 
            // dtpDateOfBirth
            // 
            this.dtpDateOfBirth.Location = new System.Drawing.Point(27, 160);
            this.dtpDateOfBirth.Name = "dtpDateOfBirth";
            this.dtpDateOfBirth.Size = new System.Drawing.Size(209, 20);
            this.dtpDateOfBirth.TabIndex = 14;
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(24, 197);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(52, 13);
            this.lblGender.TabIndex = 12;
            this.lblGender.Text = "* Gender:";
            // 
            // lblDateOfBirth
            // 
            this.lblDateOfBirth.AutoSize = true;
            this.lblDateOfBirth.Location = new System.Drawing.Point(24, 135);
            this.lblDateOfBirth.Name = "lblDateOfBirth";
            this.lblDateOfBirth.Size = new System.Drawing.Size(78, 13);
            this.lblDateOfBirth.TabIndex = 10;
            this.lblDateOfBirth.Text = "* Date Of Birth:";
            // 
            // tbxNRIC
            // 
            this.tbxNRIC.Location = new System.Drawing.Point(27, 107);
            this.tbxNRIC.Name = "tbxNRIC";
            this.tbxNRIC.Size = new System.Drawing.Size(209, 20);
            this.tbxNRIC.TabIndex = 7;
            // 
            // lblNRIC
            // 
            this.lblNRIC.AutoSize = true;
            this.lblNRIC.Location = new System.Drawing.Point(24, 80);
            this.lblNRIC.Name = "lblNRIC";
            this.lblNRIC.Size = new System.Drawing.Size(43, 13);
            this.lblNRIC.TabIndex = 6;
            this.lblNRIC.Text = "* NRIC:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(27, 48);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(209, 20);
            this.textBox1.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "* Name of Staff:";
            // 
            // lblInformation1
            // 
            this.lblInformation1.AutoSize = true;
            this.lblInformation1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInformation1.Location = new System.Drawing.Point(47, 120);
            this.lblInformation1.Name = "lblInformation1";
            this.lblInformation1.Size = new System.Drawing.Size(231, 16);
            this.lblInformation1.TabIndex = 39;
            this.lblInformation1.Text = "Please enter the following information:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(452, 47);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 81);
            this.pictureBox2.TabIndex = 38;
            this.pictureBox2.TabStop = false;
            // 
            // lblRegisterStaff
            // 
            this.lblRegisterStaff.AutoSize = true;
            this.lblRegisterStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegisterStaff.Location = new System.Drawing.Point(45, 48);
            this.lblRegisterStaff.Name = "lblRegisterStaff";
            this.lblRegisterStaff.Size = new System.Drawing.Size(190, 25);
            this.lblRegisterStaff.TabIndex = 0;
            this.lblRegisterStaff.Text = "Register New Staff";
            // 
            // pnlModifyStaff
            // 
            this.pnlModifyStaff.AutoScroll = true;
            this.pnlModifyStaff.Controls.Add(this.btnCancelModify);
            this.pnlModifyStaff.Controls.Add(this.label7);
            this.pnlModifyStaff.Controls.Add(this.pictureBox3);
            this.pnlModifyStaff.Controls.Add(this.lblModifyStaff);
            this.pnlModifyStaff.Controls.Add(this.btnSubmit1);
            this.pnlModifyStaff.Controls.Add(this.grpAccntDetails1);
            this.pnlModifyStaff.Controls.Add(this.grpPersonalParticular);
            this.pnlModifyStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlModifyStaff.Location = new System.Drawing.Point(0, 0);
            this.pnlModifyStaff.Name = "pnlModifyStaff";
            this.pnlModifyStaff.Size = new System.Drawing.Size(584, 444);
            this.pnlModifyStaff.TabIndex = 48;
            // 
            // btnCancelModify
            // 
            this.btnCancelModify.Location = new System.Drawing.Point(467, 723);
            this.btnCancelModify.Name = "btnCancelModify";
            this.btnCancelModify.Size = new System.Drawing.Size(75, 23);
            this.btnCancelModify.TabIndex = 43;
            this.btnCancelModify.Text = "Cancel";
            this.btnCancelModify.UseVisualStyleBackColor = true;
            this.btnCancelModify.Click += new System.EventHandler(this.btnCancelModify_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(47, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(231, 16);
            this.label7.TabIndex = 42;
            this.label7.Text = "Please enter the following information:";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(452, 47);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 81);
            this.pictureBox3.TabIndex = 41;
            this.pictureBox3.TabStop = false;
            // 
            // lblModifyStaff
            // 
            this.lblModifyStaff.AutoSize = true;
            this.lblModifyStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifyStaff.Location = new System.Drawing.Point(45, 50);
            this.lblModifyStaff.Name = "lblModifyStaff";
            this.lblModifyStaff.Size = new System.Drawing.Size(238, 25);
            this.lblModifyStaff.TabIndex = 40;
            this.lblModifyStaff.Text = "Modify Staff Information";
            // 
            // btnSubmit1
            // 
            this.btnSubmit1.Location = new System.Drawing.Point(364, 723);
            this.btnSubmit1.Name = "btnSubmit1";
            this.btnSubmit1.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit1.TabIndex = 36;
            this.btnSubmit1.Text = "Submit";
            this.btnSubmit1.UseVisualStyleBackColor = true;
            // 
            // grpAccntDetails1
            // 
            this.grpAccntDetails1.Controls.Add(this.rdBtnStaff1);
            this.grpAccntDetails1.Controls.Add(this.rdBtnAdministrator1);
            this.grpAccntDetails1.Controls.Add(this.lblRole1);
            this.grpAccntDetails1.Controls.Add(this.tbxEmail1);
            this.grpAccntDetails1.Controls.Add(this.lblEmail1);
            this.grpAccntDetails1.Location = new System.Drawing.Point(50, 539);
            this.grpAccntDetails1.Name = "grpAccntDetails1";
            this.grpAccntDetails1.Size = new System.Drawing.Size(393, 171);
            this.grpAccntDetails1.TabIndex = 35;
            this.grpAccntDetails1.TabStop = false;
            this.grpAccntDetails1.Text = "Account Details";
            // 
            // rdBtnStaff1
            // 
            this.rdBtnStaff1.AutoSize = true;
            this.rdBtnStaff1.Location = new System.Drawing.Point(28, 112);
            this.rdBtnStaff1.Name = "rdBtnStaff1";
            this.rdBtnStaff1.Size = new System.Drawing.Size(47, 17);
            this.rdBtnStaff1.TabIndex = 25;
            this.rdBtnStaff1.TabStop = true;
            this.rdBtnStaff1.Text = "Staff";
            this.rdBtnStaff1.UseVisualStyleBackColor = true;
            // 
            // rdBtnAdministrator1
            // 
            this.rdBtnAdministrator1.AutoSize = true;
            this.rdBtnAdministrator1.Location = new System.Drawing.Point(28, 135);
            this.rdBtnAdministrator1.Name = "rdBtnAdministrator1";
            this.rdBtnAdministrator1.Size = new System.Drawing.Size(85, 17);
            this.rdBtnAdministrator1.TabIndex = 24;
            this.rdBtnAdministrator1.TabStop = true;
            this.rdBtnAdministrator1.Text = "Administrator";
            this.rdBtnAdministrator1.UseVisualStyleBackColor = true;
            // 
            // lblRole1
            // 
            this.lblRole1.AutoSize = true;
            this.lblRole1.Location = new System.Drawing.Point(25, 87);
            this.lblRole1.Name = "lblRole1";
            this.lblRole1.Size = new System.Drawing.Size(102, 13);
            this.lblRole1.TabIndex = 23;
            this.lblRole1.Text = "* Role of Registrant:";
            // 
            // tbxEmail1
            // 
            this.tbxEmail1.Location = new System.Drawing.Point(29, 46);
            this.tbxEmail1.Name = "tbxEmail1";
            this.tbxEmail1.Size = new System.Drawing.Size(209, 20);
            this.tbxEmail1.TabIndex = 21;
            // 
            // lblEmail1
            // 
            this.lblEmail1.AutoSize = true;
            this.lblEmail1.Location = new System.Drawing.Point(25, 30);
            this.lblEmail1.Name = "lblEmail1";
            this.lblEmail1.Size = new System.Drawing.Size(79, 13);
            this.lblEmail1.TabIndex = 20;
            this.lblEmail1.Text = "E-mail Address:";
            // 
            // grpPersonalParticular
            // 
            this.grpPersonalParticular.Controls.Add(this.label1);
            this.grpPersonalParticular.Controls.Add(this.label5);
            this.grpPersonalParticular.Controls.Add(this.lblContactNo1);
            this.grpPersonalParticular.Controls.Add(this.tbxHpNo1);
            this.grpPersonalParticular.Controls.Add(this.tbxHomeNo1);
            this.grpPersonalParticular.Controls.Add(this.textBox4);
            this.grpPersonalParticular.Controls.Add(this.label6);
            this.grpPersonalParticular.Controls.Add(this.lblStreet1);
            this.grpPersonalParticular.Controls.Add(this.tbxStreet1);
            this.grpPersonalParticular.Controls.Add(this.lblUnitNo1);
            this.grpPersonalParticular.Controls.Add(this.tbxUnitNo1);
            this.grpPersonalParticular.Controls.Add(this.lblBlock1);
            this.grpPersonalParticular.Controls.Add(this.tbxBlockNo1);
            this.grpPersonalParticular.Controls.Add(this.lblAddress1);
            this.grpPersonalParticular.Controls.Add(this.tbxNRIC1);
            this.grpPersonalParticular.Controls.Add(this.lblNRIC1);
            this.grpPersonalParticular.Location = new System.Drawing.Point(50, 158);
            this.grpPersonalParticular.Name = "grpPersonalParticular";
            this.grpPersonalParticular.Size = new System.Drawing.Size(393, 375);
            this.grpPersonalParticular.TabIndex = 34;
            this.grpPersonalParticular.TabStop = false;
            this.grpPersonalParticular.Text = "Personal Particulars";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(255, 145);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "(Handphone)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(255, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "(Home)";
            // 
            // lblContactNo1
            // 
            this.lblContactNo1.AutoSize = true;
            this.lblContactNo1.Location = new System.Drawing.Point(24, 91);
            this.lblContactNo1.Name = "lblContactNo1";
            this.lblContactNo1.Size = new System.Drawing.Size(92, 13);
            this.lblContactNo1.TabIndex = 34;
            this.lblContactNo1.Text = "Contact Numbers:";
            // 
            // tbxHpNo1
            // 
            this.tbxHpNo1.Location = new System.Drawing.Point(25, 142);
            this.tbxHpNo1.Name = "tbxHpNo1";
            this.tbxHpNo1.Size = new System.Drawing.Size(209, 20);
            this.tbxHpNo1.TabIndex = 33;
            // 
            // tbxHomeNo1
            // 
            this.tbxHomeNo1.Location = new System.Drawing.Point(25, 116);
            this.tbxHomeNo1.Name = "tbxHomeNo1";
            this.tbxHomeNo1.Size = new System.Drawing.Size(209, 20);
            this.tbxHomeNo1.TabIndex = 32;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(25, 340);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(75, 20);
            this.textBox4.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 319);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Postal Code:";
            // 
            // lblStreet1
            // 
            this.lblStreet1.AutoSize = true;
            this.lblStreet1.Location = new System.Drawing.Point(24, 236);
            this.lblStreet1.Name = "lblStreet1";
            this.lblStreet1.Size = new System.Drawing.Size(38, 13);
            this.lblStreet1.TabIndex = 28;
            this.lblStreet1.Text = "Street:";
            // 
            // tbxStreet1
            // 
            this.tbxStreet1.Location = new System.Drawing.Point(25, 261);
            this.tbxStreet1.Multiline = true;
            this.tbxStreet1.Name = "tbxStreet1";
            this.tbxStreet1.Size = new System.Drawing.Size(209, 48);
            this.tbxStreet1.TabIndex = 26;
            // 
            // lblUnitNo1
            // 
            this.lblUnitNo1.AutoSize = true;
            this.lblUnitNo1.Location = new System.Drawing.Point(106, 207);
            this.lblUnitNo1.Name = "lblUnitNo1";
            this.lblUnitNo1.Size = new System.Drawing.Size(49, 13);
            this.lblUnitNo1.TabIndex = 21;
            this.lblUnitNo1.Text = "Unit No.:";
            // 
            // tbxUnitNo1
            // 
            this.tbxUnitNo1.Location = new System.Drawing.Point(155, 204);
            this.tbxUnitNo1.Name = "tbxUnitNo1";
            this.tbxUnitNo1.Size = new System.Drawing.Size(79, 20);
            this.tbxUnitNo1.TabIndex = 20;
            // 
            // lblBlock1
            // 
            this.lblBlock1.AutoSize = true;
            this.lblBlock1.Location = new System.Drawing.Point(24, 207);
            this.lblBlock1.Name = "lblBlock1";
            this.lblBlock1.Size = new System.Drawing.Size(35, 13);
            this.lblBlock1.TabIndex = 19;
            this.lblBlock1.Text = "Blk: #";
            // 
            // tbxBlockNo1
            // 
            this.tbxBlockNo1.Location = new System.Drawing.Point(60, 204);
            this.tbxBlockNo1.Name = "tbxBlockNo1";
            this.tbxBlockNo1.Size = new System.Drawing.Size(40, 20);
            this.tbxBlockNo1.TabIndex = 18;
            // 
            // lblAddress1
            // 
            this.lblAddress1.AutoSize = true;
            this.lblAddress1.Location = new System.Drawing.Point(24, 179);
            this.lblAddress1.Name = "lblAddress1";
            this.lblAddress1.Size = new System.Drawing.Size(48, 13);
            this.lblAddress1.TabIndex = 17;
            this.lblAddress1.Text = "Address:";
            // 
            // tbxNRIC1
            // 
            this.tbxNRIC1.Location = new System.Drawing.Point(27, 57);
            this.tbxNRIC1.Name = "tbxNRIC1";
            this.tbxNRIC1.Size = new System.Drawing.Size(209, 20);
            this.tbxNRIC1.TabIndex = 7;
            // 
            // lblNRIC1
            // 
            this.lblNRIC1.AutoSize = true;
            this.lblNRIC1.Location = new System.Drawing.Point(24, 30);
            this.lblNRIC1.Name = "lblNRIC1";
            this.lblNRIC1.Size = new System.Drawing.Size(36, 13);
            this.lblNRIC1.TabIndex = 6;
            this.lblNRIC1.Text = "NRIC:";
            // 
            // pnlSearchModifyStaff
            // 
            this.pnlSearchModifyStaff.AutoScroll = true;
            this.pnlSearchModifyStaff.Controls.Add(this.btnCancelSearch);
            this.pnlSearchModifyStaff.Controls.Add(this.btnSearchForStaff);
            this.pnlSearchModifyStaff.Controls.Add(this.pictureBox4);
            this.pnlSearchModifyStaff.Controls.Add(this.lblSearchModifyStaffInfo);
            this.pnlSearchModifyStaff.Controls.Add(this.lblSearchModifyStaff);
            this.pnlSearchModifyStaff.Controls.Add(this.grpBoxSearchModifyStaff);
            this.pnlSearchModifyStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchModifyStaff.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchModifyStaff.Name = "pnlSearchModifyStaff";
            this.pnlSearchModifyStaff.Size = new System.Drawing.Size(584, 444);
            this.pnlSearchModifyStaff.TabIndex = 49;
            // 
            // btnCancelSearch
            // 
            this.btnCancelSearch.Location = new System.Drawing.Point(415, 350);
            this.btnCancelSearch.Name = "btnCancelSearch";
            this.btnCancelSearch.Size = new System.Drawing.Size(75, 23);
            this.btnCancelSearch.TabIndex = 39;
            this.btnCancelSearch.Text = "Cancel";
            this.btnCancelSearch.UseVisualStyleBackColor = true;
            this.btnCancelSearch.Click += new System.EventHandler(this.btnCancelSearch_Click);
            // 
            // btnSearchForStaff
            // 
            this.btnSearchForStaff.Location = new System.Drawing.Point(312, 350);
            this.btnSearchForStaff.Name = "btnSearchForStaff";
            this.btnSearchForStaff.Size = new System.Drawing.Size(75, 23);
            this.btnSearchForStaff.TabIndex = 38;
            this.btnSearchForStaff.Text = "Search";
            this.btnSearchForStaff.UseVisualStyleBackColor = true;
            this.btnSearchForStaff.Click += new System.EventHandler(this.btnSearchForStaff_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(452, 47);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 81);
            this.pictureBox4.TabIndex = 37;
            this.pictureBox4.TabStop = false;
            // 
            // lblSearchModifyStaffInfo
            // 
            this.lblSearchModifyStaffInfo.AutoSize = true;
            this.lblSearchModifyStaffInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchModifyStaffInfo.Location = new System.Drawing.Point(60, 120);
            this.lblSearchModifyStaffInfo.Name = "lblSearchModifyStaffInfo";
            this.lblSearchModifyStaffInfo.Size = new System.Drawing.Size(231, 16);
            this.lblSearchModifyStaffInfo.TabIndex = 0;
            this.lblSearchModifyStaffInfo.Text = "Please enter a Staff Name to search...";
            // 
            // lblSearchModifyStaff
            // 
            this.lblSearchModifyStaff.AutoSize = true;
            this.lblSearchModifyStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchModifyStaff.Location = new System.Drawing.Point(58, 50);
            this.lblSearchModifyStaff.Name = "lblSearchModifyStaff";
            this.lblSearchModifyStaff.Size = new System.Drawing.Size(238, 25);
            this.lblSearchModifyStaff.TabIndex = 36;
            this.lblSearchModifyStaff.Text = "Modify Staff Information";
            this.lblSearchModifyStaff.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpBoxSearchModifyStaff
            // 
            this.grpBoxSearchModifyStaff.Controls.Add(this.tbxSearchName);
            this.grpBoxSearchModifyStaff.Controls.Add(this.lblSearchName);
            this.grpBoxSearchModifyStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxSearchModifyStaff.Location = new System.Drawing.Point(63, 160);
            this.grpBoxSearchModifyStaff.Name = "grpBoxSearchModifyStaff";
            this.grpBoxSearchModifyStaff.Size = new System.Drawing.Size(401, 173);
            this.grpBoxSearchModifyStaff.TabIndex = 35;
            this.grpBoxSearchModifyStaff.TabStop = false;
            this.grpBoxSearchModifyStaff.Text = "Search Staff";
            // 
            // tbxSearchName
            // 
            this.tbxSearchName.Location = new System.Drawing.Point(36, 80);
            this.tbxSearchName.Name = "tbxSearchName";
            this.tbxSearchName.Size = new System.Drawing.Size(256, 22);
            this.tbxSearchName.TabIndex = 1;
            // 
            // lblSearchName
            // 
            this.lblSearchName.AutoSize = true;
            this.lblSearchName.Location = new System.Drawing.Point(33, 52);
            this.lblSearchName.Name = "lblSearchName";
            this.lblSearchName.Size = new System.Drawing.Size(74, 16);
            this.lblSearchName.TabIndex = 0;
            this.lblSearchName.Text = "Staff name:";
            // 
            // pnlSearchAndDisplay
            // 
            this.pnlSearchAndDisplay.AutoScroll = true;
            this.pnlSearchAndDisplay.Controls.Add(this.btnCancelDisplay);
            this.pnlSearchAndDisplay.Controls.Add(this.btnDisplayStaffInfo);
            this.pnlSearchAndDisplay.Controls.Add(this.grpBoxSearchDisplayStaffInfo);
            this.pnlSearchAndDisplay.Controls.Add(this.pictureBox5);
            this.pnlSearchAndDisplay.Controls.Add(this.lblSearchDisplayInfo);
            this.pnlSearchAndDisplay.Controls.Add(this.lblSearchDisp);
            this.pnlSearchAndDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchAndDisplay.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchAndDisplay.Name = "pnlSearchAndDisplay";
            this.pnlSearchAndDisplay.Size = new System.Drawing.Size(584, 444);
            this.pnlSearchAndDisplay.TabIndex = 50;
            // 
            // btnCancelDisplay
            // 
            this.btnCancelDisplay.Location = new System.Drawing.Point(383, 488);
            this.btnCancelDisplay.Name = "btnCancelDisplay";
            this.btnCancelDisplay.Size = new System.Drawing.Size(75, 23);
            this.btnCancelDisplay.TabIndex = 44;
            this.btnCancelDisplay.Text = "Cancel";
            this.btnCancelDisplay.UseVisualStyleBackColor = true;
            this.btnCancelDisplay.Click += new System.EventHandler(this.btnCancelDisplay_Click);
            // 
            // btnDisplayStaffInfo
            // 
            this.btnDisplayStaffInfo.Location = new System.Drawing.Point(280, 488);
            this.btnDisplayStaffInfo.Name = "btnDisplayStaffInfo";
            this.btnDisplayStaffInfo.Size = new System.Drawing.Size(75, 23);
            this.btnDisplayStaffInfo.TabIndex = 43;
            this.btnDisplayStaffInfo.Text = "Display";
            this.btnDisplayStaffInfo.UseVisualStyleBackColor = true;
            // 
            // grpBoxSearchDisplayStaffInfo
            // 
            this.grpBoxSearchDisplayStaffInfo.Controls.Add(this.tbxDisplayStaffInfo);
            this.grpBoxSearchDisplayStaffInfo.Controls.Add(this.lblStaffInformation);
            this.grpBoxSearchDisplayStaffInfo.Controls.Add(this.tbxSelectStaffInfo);
            this.grpBoxSearchDisplayStaffInfo.Controls.Add(this.lblEnterInformation);
            this.grpBoxSearchDisplayStaffInfo.Controls.Add(this.lblTypeOfSearch);
            this.grpBoxSearchDisplayStaffInfo.Controls.Add(this.cmBoxTypeofSearch);
            this.grpBoxSearchDisplayStaffInfo.Location = new System.Drawing.Point(48, 177);
            this.grpBoxSearchDisplayStaffInfo.Name = "grpBoxSearchDisplayStaffInfo";
            this.grpBoxSearchDisplayStaffInfo.Size = new System.Drawing.Size(410, 300);
            this.grpBoxSearchDisplayStaffInfo.TabIndex = 41;
            this.grpBoxSearchDisplayStaffInfo.TabStop = false;
            this.grpBoxSearchDisplayStaffInfo.Text = "Search && Display Existing Information";
            // 
            // tbxDisplayStaffInfo
            // 
            this.tbxDisplayStaffInfo.Location = new System.Drawing.Point(25, 154);
            this.tbxDisplayStaffInfo.Multiline = true;
            this.tbxDisplayStaffInfo.Name = "tbxDisplayStaffInfo";
            this.tbxDisplayStaffInfo.Size = new System.Drawing.Size(343, 118);
            this.tbxDisplayStaffInfo.TabIndex = 5;
            // 
            // lblStaffInformation
            // 
            this.lblStaffInformation.AutoSize = true;
            this.lblStaffInformation.Location = new System.Drawing.Point(22, 130);
            this.lblStaffInformation.Name = "lblStaffInformation";
            this.lblStaffInformation.Size = new System.Drawing.Size(87, 13);
            this.lblStaffInformation.TabIndex = 4;
            this.lblStaffInformation.Text = "Staff Information:";
            // 
            // tbxSelectStaffInfo
            // 
            this.tbxSelectStaffInfo.Location = new System.Drawing.Point(25, 96);
            this.tbxSelectStaffInfo.Name = "tbxSelectStaffInfo";
            this.tbxSelectStaffInfo.Size = new System.Drawing.Size(343, 20);
            this.tbxSelectStaffInfo.TabIndex = 3;
            // 
            // lblEnterInformation
            // 
            this.lblEnterInformation.AutoSize = true;
            this.lblEnterInformation.Location = new System.Drawing.Point(22, 75);
            this.lblEnterInformation.Name = "lblEnterInformation";
            this.lblEnterInformation.Size = new System.Drawing.Size(132, 13);
            this.lblEnterInformation.TabIndex = 2;
            this.lblEnterInformation.Text = "Enter selected information:";
            // 
            // lblTypeOfSearch
            // 
            this.lblTypeOfSearch.AutoSize = true;
            this.lblTypeOfSearch.Location = new System.Drawing.Point(21, 41);
            this.lblTypeOfSearch.Name = "lblTypeOfSearch";
            this.lblTypeOfSearch.Size = new System.Drawing.Size(125, 13);
            this.lblTypeOfSearch.TabIndex = 1;
            this.lblTypeOfSearch.Text = "Select a mode of search:";
            // 
            // cmBoxTypeofSearch
            // 
            this.cmBoxTypeofSearch.FormattingEnabled = true;
            this.cmBoxTypeofSearch.Items.AddRange(new object[] {
            "Name",
            "NRIC",
            "Name & NRIC"});
            this.cmBoxTypeofSearch.Location = new System.Drawing.Point(172, 42);
            this.cmBoxTypeofSearch.Name = "cmBoxTypeofSearch";
            this.cmBoxTypeofSearch.Size = new System.Drawing.Size(196, 21);
            this.cmBoxTypeofSearch.TabIndex = 0;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(452, 47);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(100, 81);
            this.pictureBox5.TabIndex = 40;
            this.pictureBox5.TabStop = false;
            // 
            // lblSearchDisplayInfo
            // 
            this.lblSearchDisplayInfo.AutoSize = true;
            this.lblSearchDisplayInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchDisplayInfo.Location = new System.Drawing.Point(45, 123);
            this.lblSearchDisplayInfo.Name = "lblSearchDisplayInfo";
            this.lblSearchDisplayInfo.Size = new System.Drawing.Size(231, 16);
            this.lblSearchDisplayInfo.TabIndex = 38;
            this.lblSearchDisplayInfo.Text = "Please enter the following information:";
            // 
            // lblSearchDisp
            // 
            this.lblSearchDisp.AutoSize = true;
            this.lblSearchDisp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchDisp.Location = new System.Drawing.Point(43, 50);
            this.lblSearchDisp.Name = "lblSearchDisp";
            this.lblSearchDisp.Size = new System.Drawing.Size(339, 25);
            this.lblSearchDisp.TabIndex = 39;
            this.lblSearchDisp.Text = "Search && Display Staff Information";
            this.lblSearchDisp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlSearchDeleteStaff
            // 
            this.pnlSearchDeleteStaff.AutoScroll = true;
            this.pnlSearchDeleteStaff.Controls.Add(this.btnCancelSearchDelete);
            this.pnlSearchDeleteStaff.Controls.Add(this.btnSearchDeleteStaff);
            this.pnlSearchDeleteStaff.Controls.Add(this.pictureBox7);
            this.pnlSearchDeleteStaff.Controls.Add(this.lblSearchDeleteInfo);
            this.pnlSearchDeleteStaff.Controls.Add(this.lblSearchDeleteStaff);
            this.pnlSearchDeleteStaff.Controls.Add(this.grpBoxSearchDeleteStaff);
            this.pnlSearchDeleteStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchDeleteStaff.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchDeleteStaff.Name = "pnlSearchDeleteStaff";
            this.pnlSearchDeleteStaff.Size = new System.Drawing.Size(584, 444);
            this.pnlSearchDeleteStaff.TabIndex = 51;
            // 
            // btnCancelSearchDelete
            // 
            this.btnCancelSearchDelete.Location = new System.Drawing.Point(415, 350);
            this.btnCancelSearchDelete.Name = "btnCancelSearchDelete";
            this.btnCancelSearchDelete.Size = new System.Drawing.Size(75, 23);
            this.btnCancelSearchDelete.TabIndex = 39;
            this.btnCancelSearchDelete.Text = "Cancel";
            this.btnCancelSearchDelete.UseVisualStyleBackColor = true;
            this.btnCancelSearchDelete.Click += new System.EventHandler(this.btnCancelSearchDelete_Click);
            // 
            // btnSearchDeleteStaff
            // 
            this.btnSearchDeleteStaff.Location = new System.Drawing.Point(312, 350);
            this.btnSearchDeleteStaff.Name = "btnSearchDeleteStaff";
            this.btnSearchDeleteStaff.Size = new System.Drawing.Size(75, 23);
            this.btnSearchDeleteStaff.TabIndex = 38;
            this.btnSearchDeleteStaff.Text = "Search";
            this.btnSearchDeleteStaff.UseVisualStyleBackColor = true;
            this.btnSearchDeleteStaff.Click += new System.EventHandler(this.btnSearchDeleteStaff_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(452, 47);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(100, 81);
            this.pictureBox7.TabIndex = 37;
            this.pictureBox7.TabStop = false;
            // 
            // lblSearchDeleteInfo
            // 
            this.lblSearchDeleteInfo.AutoSize = true;
            this.lblSearchDeleteInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchDeleteInfo.Location = new System.Drawing.Point(60, 120);
            this.lblSearchDeleteInfo.Name = "lblSearchDeleteInfo";
            this.lblSearchDeleteInfo.Size = new System.Drawing.Size(231, 16);
            this.lblSearchDeleteInfo.TabIndex = 0;
            this.lblSearchDeleteInfo.Text = "Please enter a Staff Name to search...";
            // 
            // lblSearchDeleteStaff
            // 
            this.lblSearchDeleteStaff.AutoSize = true;
            this.lblSearchDeleteStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchDeleteStaff.Location = new System.Drawing.Point(58, 50);
            this.lblSearchDeleteStaff.Name = "lblSearchDeleteStaff";
            this.lblSearchDeleteStaff.Size = new System.Drawing.Size(236, 25);
            this.lblSearchDeleteStaff.TabIndex = 36;
            this.lblSearchDeleteStaff.Text = "Delete Staff Information";
            this.lblSearchDeleteStaff.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpBoxSearchDeleteStaff
            // 
            this.grpBoxSearchDeleteStaff.Controls.Add(this.tbxSearchDeleteStaff);
            this.grpBoxSearchDeleteStaff.Controls.Add(this.lblSearchDeleteStaffName);
            this.grpBoxSearchDeleteStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxSearchDeleteStaff.Location = new System.Drawing.Point(63, 160);
            this.grpBoxSearchDeleteStaff.Name = "grpBoxSearchDeleteStaff";
            this.grpBoxSearchDeleteStaff.Size = new System.Drawing.Size(401, 173);
            this.grpBoxSearchDeleteStaff.TabIndex = 35;
            this.grpBoxSearchDeleteStaff.TabStop = false;
            this.grpBoxSearchDeleteStaff.Text = "Search Staff";
            // 
            // tbxSearchDeleteStaff
            // 
            this.tbxSearchDeleteStaff.Location = new System.Drawing.Point(36, 80);
            this.tbxSearchDeleteStaff.Name = "tbxSearchDeleteStaff";
            this.tbxSearchDeleteStaff.Size = new System.Drawing.Size(256, 22);
            this.tbxSearchDeleteStaff.TabIndex = 1;
            // 
            // lblSearchDeleteStaffName
            // 
            this.lblSearchDeleteStaffName.AutoSize = true;
            this.lblSearchDeleteStaffName.Location = new System.Drawing.Point(33, 52);
            this.lblSearchDeleteStaffName.Name = "lblSearchDeleteStaffName";
            this.lblSearchDeleteStaffName.Size = new System.Drawing.Size(74, 16);
            this.lblSearchDeleteStaffName.TabIndex = 0;
            this.lblSearchDeleteStaffName.Text = "Staff name:";
            // 
            // pnlDeleteStaffInfo
            // 
            this.pnlDeleteStaffInfo.AutoScroll = true;
            this.pnlDeleteStaffInfo.Controls.Add(this.btnCancelDeleteStaff);
            this.pnlDeleteStaffInfo.Controls.Add(this.btnDeleteStaff);
            this.pnlDeleteStaffInfo.Controls.Add(this.pictureBox8);
            this.pnlDeleteStaffInfo.Controls.Add(this.lblDeleteInfo);
            this.pnlDeleteStaffInfo.Controls.Add(this.lblDeleteStaffInformation);
            this.pnlDeleteStaffInfo.Controls.Add(this.grpBoxDelStaff);
            this.pnlDeleteStaffInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDeleteStaffInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlDeleteStaffInfo.Name = "pnlDeleteStaffInfo";
            this.pnlDeleteStaffInfo.Size = new System.Drawing.Size(584, 444);
            this.pnlDeleteStaffInfo.TabIndex = 52;
            // 
            // btnCancelDeleteStaff
            // 
            this.btnCancelDeleteStaff.Location = new System.Drawing.Point(415, 350);
            this.btnCancelDeleteStaff.Name = "btnCancelDeleteStaff";
            this.btnCancelDeleteStaff.Size = new System.Drawing.Size(75, 23);
            this.btnCancelDeleteStaff.TabIndex = 39;
            this.btnCancelDeleteStaff.Text = "Cancel";
            this.btnCancelDeleteStaff.UseVisualStyleBackColor = true;
            this.btnCancelDeleteStaff.Click += new System.EventHandler(this.btnCancelDeleteStaff_Click);
            // 
            // btnDeleteStaff
            // 
            this.btnDeleteStaff.Location = new System.Drawing.Point(312, 350);
            this.btnDeleteStaff.Name = "btnDeleteStaff";
            this.btnDeleteStaff.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteStaff.TabIndex = 38;
            this.btnDeleteStaff.Text = "Delete";
            this.btnDeleteStaff.UseVisualStyleBackColor = true;
            this.btnDeleteStaff.Click += new System.EventHandler(this.btnDeleteStaff_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(452, 47);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(100, 81);
            this.pictureBox8.TabIndex = 37;
            this.pictureBox8.TabStop = false;
            // 
            // lblDeleteInfo
            // 
            this.lblDeleteInfo.AutoSize = true;
            this.lblDeleteInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteInfo.Location = new System.Drawing.Point(60, 120);
            this.lblDeleteInfo.Name = "lblDeleteInfo";
            this.lblDeleteInfo.Size = new System.Drawing.Size(228, 16);
            this.lblDeleteInfo.TabIndex = 0;
            this.lblDeleteInfo.Text = "Please enter a Staff Name to delete...";
            // 
            // lblDeleteStaffInformation
            // 
            this.lblDeleteStaffInformation.AutoSize = true;
            this.lblDeleteStaffInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteStaffInformation.Location = new System.Drawing.Point(58, 50);
            this.lblDeleteStaffInformation.Name = "lblDeleteStaffInformation";
            this.lblDeleteStaffInformation.Size = new System.Drawing.Size(236, 25);
            this.lblDeleteStaffInformation.TabIndex = 36;
            this.lblDeleteStaffInformation.Text = "Delete Staff Information";
            this.lblDeleteStaffInformation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpBoxDelStaff
            // 
            this.grpBoxDelStaff.Controls.Add(this.tbxDeleteStaff);
            this.grpBoxDelStaff.Controls.Add(this.lblDeleteStaffName);
            this.grpBoxDelStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxDelStaff.Location = new System.Drawing.Point(63, 160);
            this.grpBoxDelStaff.Name = "grpBoxDelStaff";
            this.grpBoxDelStaff.Size = new System.Drawing.Size(401, 173);
            this.grpBoxDelStaff.TabIndex = 35;
            this.grpBoxDelStaff.TabStop = false;
            this.grpBoxDelStaff.Text = "Delete Staff Information";
            // 
            // tbxDeleteStaff
            // 
            this.tbxDeleteStaff.Location = new System.Drawing.Point(36, 80);
            this.tbxDeleteStaff.Name = "tbxDeleteStaff";
            this.tbxDeleteStaff.Size = new System.Drawing.Size(256, 22);
            this.tbxDeleteStaff.TabIndex = 1;
            // 
            // lblDeleteStaffName
            // 
            this.lblDeleteStaffName.AutoSize = true;
            this.lblDeleteStaffName.Location = new System.Drawing.Point(33, 52);
            this.lblDeleteStaffName.Name = "lblDeleteStaffName";
            this.lblDeleteStaffName.Size = new System.Drawing.Size(74, 16);
            this.lblDeleteStaffName.TabIndex = 0;
            this.lblDeleteStaffName.Text = "Staff name:";
            // 
            // pnlAddNewOrder
            // 
            this.pnlAddNewOrder.AutoScroll = true;
            this.pnlAddNewOrder.Controls.Add(this.btnCancelAddNewOrder);
            this.pnlAddNewOrder.Controls.Add(this.label8);
            this.pnlAddNewOrder.Controls.Add(this.grpOrderItemInfo);
            this.pnlAddNewOrder.Controls.Add(this.grpBoxAddOrderInfo);
            this.pnlAddNewOrder.Controls.Add(this.btnAddOrder);
            this.pnlAddNewOrder.Controls.Add(this.lblAddNewOrderInfo);
            this.pnlAddNewOrder.Controls.Add(this.pictureBox9);
            this.pnlAddNewOrder.Controls.Add(this.lblAddNewOrder);
            this.pnlAddNewOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAddNewOrder.Location = new System.Drawing.Point(0, 0);
            this.pnlAddNewOrder.Name = "pnlAddNewOrder";
            this.pnlAddNewOrder.Size = new System.Drawing.Size(584, 444);
            this.pnlAddNewOrder.TabIndex = 53;
            // 
            // btnCancelAddNewOrder
            // 
            this.btnCancelAddNewOrder.Location = new System.Drawing.Point(415, 712);
            this.btnCancelAddNewOrder.Name = "btnCancelAddNewOrder";
            this.btnCancelAddNewOrder.Size = new System.Drawing.Size(75, 23);
            this.btnCancelAddNewOrder.TabIndex = 49;
            this.btnCancelAddNewOrder.Text = "Cancel";
            this.btnCancelAddNewOrder.UseVisualStyleBackColor = true;
            this.btnCancelAddNewOrder.Click += new System.EventHandler(this.btnCancelAddNewOrder_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 747);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 13);
            this.label8.TabIndex = 48;
            this.label8.Text = "* Denotes Mandatory field";
            // 
            // grpOrderItemInfo
            // 
            this.grpOrderItemInfo.Controls.Add(this.textBox7);
            this.grpOrderItemInfo.Controls.Add(this.lblAddOrderQty2);
            this.grpOrderItemInfo.Controls.Add(this.tbxQuantityAddOrder2);
            this.grpOrderItemInfo.Controls.Add(this.tbxTitleAddOrder2);
            this.grpOrderItemInfo.Controls.Add(this.lblItemAddOrder2);
            this.grpOrderItemInfo.Controls.Add(this.lblItemAddOrder1);
            this.grpOrderItemInfo.Controls.Add(this.lblAddOrderInfo);
            this.grpOrderItemInfo.Controls.Add(this.tbxNRIC3);
            this.grpOrderItemInfo.Controls.Add(this.lblAddItemQty1);
            this.grpOrderItemInfo.Location = new System.Drawing.Point(34, 561);
            this.grpOrderItemInfo.Name = "grpOrderItemInfo";
            this.grpOrderItemInfo.Size = new System.Drawing.Size(418, 144);
            this.grpOrderItemInfo.TabIndex = 47;
            this.grpOrderItemInfo.TabStop = false;
            this.grpOrderItemInfo.Text = "Item Information";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(339, 102);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(39, 20);
            this.textBox7.TabIndex = 47;
            // 
            // lblAddOrderQty2
            // 
            this.lblAddOrderQty2.AutoSize = true;
            this.lblAddOrderQty2.Location = new System.Drawing.Point(307, 105);
            this.lblAddOrderQty2.Name = "lblAddOrderQty2";
            this.lblAddOrderQty2.Size = new System.Drawing.Size(26, 13);
            this.lblAddOrderQty2.TabIndex = 46;
            this.lblAddOrderQty2.Text = "Qty:";
            // 
            // tbxQuantityAddOrder2
            // 
            this.tbxQuantityAddOrder2.Location = new System.Drawing.Point(69, 105);
            this.tbxQuantityAddOrder2.Name = "tbxQuantityAddOrder2";
            this.tbxQuantityAddOrder2.Size = new System.Drawing.Size(222, 20);
            this.tbxQuantityAddOrder2.TabIndex = 45;
            // 
            // tbxTitleAddOrder2
            // 
            this.tbxTitleAddOrder2.Location = new System.Drawing.Point(69, 68);
            this.tbxTitleAddOrder2.Name = "tbxTitleAddOrder2";
            this.tbxTitleAddOrder2.Size = new System.Drawing.Size(222, 20);
            this.tbxTitleAddOrder2.TabIndex = 44;
            // 
            // lblItemAddOrder2
            // 
            this.lblItemAddOrder2.AutoSize = true;
            this.lblItemAddOrder2.Location = new System.Drawing.Point(24, 71);
            this.lblItemAddOrder2.Name = "lblItemAddOrder2";
            this.lblItemAddOrder2.Size = new System.Drawing.Size(46, 13);
            this.lblItemAddOrder2.TabIndex = 43;
            this.lblItemAddOrder2.Text = "* Item 1:";
            // 
            // lblItemAddOrder1
            // 
            this.lblItemAddOrder1.AutoSize = true;
            this.lblItemAddOrder1.Location = new System.Drawing.Point(31, 105);
            this.lblItemAddOrder1.Name = "lblItemAddOrder1";
            this.lblItemAddOrder1.Size = new System.Drawing.Size(39, 13);
            this.lblItemAddOrder1.TabIndex = 42;
            this.lblItemAddOrder1.Text = "Item 2:";
            // 
            // lblAddOrderInfo
            // 
            this.lblAddOrderInfo.AutoSize = true;
            this.lblAddOrderInfo.Location = new System.Drawing.Point(26, 31);
            this.lblAddOrderInfo.Name = "lblAddOrderInfo";
            this.lblAddOrderInfo.Size = new System.Drawing.Size(178, 13);
            this.lblAddOrderInfo.TabIndex = 32;
            this.lblAddOrderInfo.Text = "Enter Item and Quantity of Purchase";
            // 
            // tbxNRIC3
            // 
            this.tbxNRIC3.Location = new System.Drawing.Point(339, 68);
            this.tbxNRIC3.Name = "tbxNRIC3";
            this.tbxNRIC3.Size = new System.Drawing.Size(39, 20);
            this.tbxNRIC3.TabIndex = 7;
            // 
            // lblAddItemQty1
            // 
            this.lblAddItemQty1.AutoSize = true;
            this.lblAddItemQty1.Location = new System.Drawing.Point(307, 71);
            this.lblAddItemQty1.Name = "lblAddItemQty1";
            this.lblAddItemQty1.Size = new System.Drawing.Size(26, 13);
            this.lblAddItemQty1.TabIndex = 6;
            this.lblAddItemQty1.Text = "Qty:";
            // 
            // grpBoxAddOrderInfo
            // 
            this.grpBoxAddOrderInfo.Controls.Add(this.textBox8);
            this.grpBoxAddOrderInfo.Controls.Add(this.lblAddPostCode);
            this.grpBoxAddOrderInfo.Controls.Add(this.lblAddStreet);
            this.grpBoxAddOrderInfo.Controls.Add(this.textBox9);
            this.grpBoxAddOrderInfo.Controls.Add(this.lblAddUnitNum);
            this.grpBoxAddOrderInfo.Controls.Add(this.textBox10);
            this.grpBoxAddOrderInfo.Controls.Add(this.lblAddBlk);
            this.grpBoxAddOrderInfo.Controls.Add(this.textBox11);
            this.grpBoxAddOrderInfo.Controls.Add(this.lblAddAddress);
            this.grpBoxAddOrderInfo.Controls.Add(this.dateTimePicker1);
            this.grpBoxAddOrderInfo.Controls.Add(this.lblAddDateOfOrder);
            this.grpBoxAddOrderInfo.Controls.Add(this.textBox12);
            this.grpBoxAddOrderInfo.Controls.Add(this.lblAddNameOfCust);
            this.grpBoxAddOrderInfo.Controls.Add(this.textBox13);
            this.grpBoxAddOrderInfo.Controls.Add(this.lblAddORNum);
            this.grpBoxAddOrderInfo.Location = new System.Drawing.Point(48, 158);
            this.grpBoxAddOrderInfo.Name = "grpBoxAddOrderInfo";
            this.grpBoxAddOrderInfo.Size = new System.Drawing.Size(404, 385);
            this.grpBoxAddOrderInfo.TabIndex = 46;
            this.grpBoxAddOrderInfo.TabStop = false;
            this.grpBoxAddOrderInfo.Text = "Order Information";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(25, 353);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(75, 20);
            this.textBox8.TabIndex = 30;
            // 
            // lblAddPostCode
            // 
            this.lblAddPostCode.AutoSize = true;
            this.lblAddPostCode.Location = new System.Drawing.Point(22, 332);
            this.lblAddPostCode.Name = "lblAddPostCode";
            this.lblAddPostCode.Size = new System.Drawing.Size(67, 13);
            this.lblAddPostCode.TabIndex = 29;
            this.lblAddPostCode.Text = "Postal Code:";
            // 
            // lblAddStreet
            // 
            this.lblAddStreet.AutoSize = true;
            this.lblAddStreet.Location = new System.Drawing.Point(24, 249);
            this.lblAddStreet.Name = "lblAddStreet";
            this.lblAddStreet.Size = new System.Drawing.Size(38, 13);
            this.lblAddStreet.TabIndex = 28;
            this.lblAddStreet.Text = "Street:";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(25, 274);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(209, 48);
            this.textBox9.TabIndex = 26;
            // 
            // lblAddUnitNum
            // 
            this.lblAddUnitNum.AutoSize = true;
            this.lblAddUnitNum.Location = new System.Drawing.Point(106, 220);
            this.lblAddUnitNum.Name = "lblAddUnitNum";
            this.lblAddUnitNum.Size = new System.Drawing.Size(49, 13);
            this.lblAddUnitNum.TabIndex = 21;
            this.lblAddUnitNum.Text = "Unit No.:";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(155, 217);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(79, 20);
            this.textBox10.TabIndex = 20;
            // 
            // lblAddBlk
            // 
            this.lblAddBlk.AutoSize = true;
            this.lblAddBlk.Location = new System.Drawing.Point(24, 220);
            this.lblAddBlk.Name = "lblAddBlk";
            this.lblAddBlk.Size = new System.Drawing.Size(35, 13);
            this.lblAddBlk.TabIndex = 19;
            this.lblAddBlk.Text = "Blk: #";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(60, 217);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(40, 20);
            this.textBox11.TabIndex = 18;
            // 
            // lblAddAddress
            // 
            this.lblAddAddress.AutoSize = true;
            this.lblAddAddress.Location = new System.Drawing.Point(24, 192);
            this.lblAddAddress.Name = "lblAddAddress";
            this.lblAddAddress.Size = new System.Drawing.Size(111, 13);
            this.lblAddAddress.TabIndex = 17;
            this.lblAddAddress.Text = "* Destination Address:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(27, 160);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(209, 20);
            this.dateTimePicker1.TabIndex = 14;
            // 
            // lblAddDateOfOrder
            // 
            this.lblAddDateOfOrder.AutoSize = true;
            this.lblAddDateOfOrder.Location = new System.Drawing.Point(24, 135);
            this.lblAddDateOfOrder.Name = "lblAddDateOfOrder";
            this.lblAddDateOfOrder.Size = new System.Drawing.Size(83, 13);
            this.lblAddDateOfOrder.TabIndex = 10;
            this.lblAddDateOfOrder.Text = "* Date Of Order:";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(27, 107);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(209, 20);
            this.textBox12.TabIndex = 7;
            // 
            // lblAddNameOfCust
            // 
            this.lblAddNameOfCust.AutoSize = true;
            this.lblAddNameOfCust.Location = new System.Drawing.Point(24, 80);
            this.lblAddNameOfCust.Name = "lblAddNameOfCust";
            this.lblAddNameOfCust.Size = new System.Drawing.Size(104, 13);
            this.lblAddNameOfCust.TabIndex = 6;
            this.lblAddNameOfCust.Text = "* Name of Customer:";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(27, 48);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(209, 20);
            this.textBox13.TabIndex = 5;
            // 
            // lblAddORNum
            // 
            this.lblAddORNum.AutoSize = true;
            this.lblAddORNum.Location = new System.Drawing.Point(24, 22);
            this.lblAddORNum.Name = "lblAddORNum";
            this.lblAddORNum.Size = new System.Drawing.Size(136, 13);
            this.lblAddORNum.TabIndex = 2;
            this.lblAddORNum.Text = "* Order Reference Number:";
            // 
            // btnAddOrder
            // 
            this.btnAddOrder.Location = new System.Drawing.Point(312, 712);
            this.btnAddOrder.Name = "btnAddOrder";
            this.btnAddOrder.Size = new System.Drawing.Size(75, 23);
            this.btnAddOrder.TabIndex = 45;
            this.btnAddOrder.Text = "Add";
            this.btnAddOrder.UseVisualStyleBackColor = true;
            // 
            // lblAddNewOrderInfo
            // 
            this.lblAddNewOrderInfo.AutoSize = true;
            this.lblAddNewOrderInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddNewOrderInfo.Location = new System.Drawing.Point(47, 122);
            this.lblAddNewOrderInfo.Name = "lblAddNewOrderInfo";
            this.lblAddNewOrderInfo.Size = new System.Drawing.Size(231, 16);
            this.lblAddNewOrderInfo.TabIndex = 42;
            this.lblAddNewOrderInfo.Text = "Please enter the following information:";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(452, 47);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(100, 81);
            this.pictureBox9.TabIndex = 41;
            this.pictureBox9.TabStop = false;
            // 
            // lblAddNewOrder
            // 
            this.lblAddNewOrder.AutoSize = true;
            this.lblAddNewOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddNewOrder.Location = new System.Drawing.Point(45, 50);
            this.lblAddNewOrder.Name = "lblAddNewOrder";
            this.lblAddNewOrder.Size = new System.Drawing.Size(270, 25);
            this.lblAddNewOrder.TabIndex = 40;
            this.lblAddNewOrder.Text = "Add New Order Information";
            // 
            // pnlSearchAddOrder
            // 
            this.pnlSearchAddOrder.AutoScroll = true;
            this.pnlSearchAddOrder.Controls.Add(this.btnCancelAddOrder);
            this.pnlSearchAddOrder.Controls.Add(this.btnSearchAddOrder);
            this.pnlSearchAddOrder.Controls.Add(this.pictureBox10);
            this.pnlSearchAddOrder.Controls.Add(this.lblODRnum);
            this.pnlSearchAddOrder.Controls.Add(this.lblSearchAddOrder);
            this.pnlSearchAddOrder.Controls.Add(this.grpBoxSearchAddOrder);
            this.pnlSearchAddOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchAddOrder.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchAddOrder.Name = "pnlSearchAddOrder";
            this.pnlSearchAddOrder.Size = new System.Drawing.Size(584, 444);
            this.pnlSearchAddOrder.TabIndex = 54;
            // 
            // btnCancelAddOrder
            // 
            this.btnCancelAddOrder.Location = new System.Drawing.Point(415, 350);
            this.btnCancelAddOrder.Name = "btnCancelAddOrder";
            this.btnCancelAddOrder.Size = new System.Drawing.Size(75, 23);
            this.btnCancelAddOrder.TabIndex = 39;
            this.btnCancelAddOrder.Text = "Cancel";
            this.btnCancelAddOrder.UseVisualStyleBackColor = true;
            this.btnCancelAddOrder.Click += new System.EventHandler(this.btnCancelAddOrder_Click);
            // 
            // btnSearchAddOrder
            // 
            this.btnSearchAddOrder.Location = new System.Drawing.Point(312, 350);
            this.btnSearchAddOrder.Name = "btnSearchAddOrder";
            this.btnSearchAddOrder.Size = new System.Drawing.Size(75, 23);
            this.btnSearchAddOrder.TabIndex = 38;
            this.btnSearchAddOrder.Text = "Search";
            this.btnSearchAddOrder.UseVisualStyleBackColor = true;
            this.btnSearchAddOrder.Click += new System.EventHandler(this.btnSearchAddOrder_Click);
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(452, 47);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(100, 81);
            this.pictureBox10.TabIndex = 37;
            this.pictureBox10.TabStop = false;
            // 
            // lblODRnum
            // 
            this.lblODRnum.AutoSize = true;
            this.lblODRnum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblODRnum.Location = new System.Drawing.Point(60, 120);
            this.lblODRnum.Name = "lblODRnum";
            this.lblODRnum.Size = new System.Drawing.Size(325, 16);
            this.lblODRnum.TabIndex = 0;
            this.lblODRnum.Text = "Please enter an Order Reference Number to Search...";
            // 
            // lblSearchAddOrder
            // 
            this.lblSearchAddOrder.AutoSize = true;
            this.lblSearchAddOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchAddOrder.Location = new System.Drawing.Point(58, 50);
            this.lblSearchAddOrder.Name = "lblSearchAddOrder";
            this.lblSearchAddOrder.Size = new System.Drawing.Size(158, 25);
            this.lblSearchAddOrder.TabIndex = 36;
            this.lblSearchAddOrder.Text = "Add New Order";
            this.lblSearchAddOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpBoxSearchAddOrder
            // 
            this.grpBoxSearchAddOrder.Controls.Add(this.tbxSearchAddOrder);
            this.grpBoxSearchAddOrder.Controls.Add(this.lblOrderRefNum);
            this.grpBoxSearchAddOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxSearchAddOrder.Location = new System.Drawing.Point(63, 160);
            this.grpBoxSearchAddOrder.Name = "grpBoxSearchAddOrder";
            this.grpBoxSearchAddOrder.Size = new System.Drawing.Size(401, 173);
            this.grpBoxSearchAddOrder.TabIndex = 35;
            this.grpBoxSearchAddOrder.TabStop = false;
            this.grpBoxSearchAddOrder.Text = "Search for existing Order";
            // 
            // tbxSearchAddOrder
            // 
            this.tbxSearchAddOrder.Location = new System.Drawing.Point(36, 80);
            this.tbxSearchAddOrder.Name = "tbxSearchAddOrder";
            this.tbxSearchAddOrder.Size = new System.Drawing.Size(256, 22);
            this.tbxSearchAddOrder.TabIndex = 1;
            // 
            // lblOrderRefNum
            // 
            this.lblOrderRefNum.AutoSize = true;
            this.lblOrderRefNum.Location = new System.Drawing.Point(33, 52);
            this.lblOrderRefNum.Name = "lblOrderRefNum";
            this.lblOrderRefNum.Size = new System.Drawing.Size(162, 16);
            this.lblOrderRefNum.TabIndex = 0;
            this.lblOrderRefNum.Text = "Order Reference Number:";
            // 
            // pnlSearchDisplayOrderInfo
            // 
            this.pnlSearchDisplayOrderInfo.AutoScroll = true;
            this.pnlSearchDisplayOrderInfo.Controls.Add(this.btnCancelDisplayOrderInfo);
            this.pnlSearchDisplayOrderInfo.Controls.Add(this.btnDisplayOrderInformation);
            this.pnlSearchDisplayOrderInfo.Controls.Add(this.grpBoxSearchDisplayOrderInfo);
            this.pnlSearchDisplayOrderInfo.Controls.Add(this.pictureBox11);
            this.pnlSearchDisplayOrderInfo.Controls.Add(this.label12);
            this.pnlSearchDisplayOrderInfo.Controls.Add(this.lblSearchDisplayOrder);
            this.pnlSearchDisplayOrderInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchDisplayOrderInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchDisplayOrderInfo.Name = "pnlSearchDisplayOrderInfo";
            this.pnlSearchDisplayOrderInfo.Size = new System.Drawing.Size(584, 444);
            this.pnlSearchDisplayOrderInfo.TabIndex = 55;
            // 
            // btnCancelDisplayOrderInfo
            // 
            this.btnCancelDisplayOrderInfo.Location = new System.Drawing.Point(383, 488);
            this.btnCancelDisplayOrderInfo.Name = "btnCancelDisplayOrderInfo";
            this.btnCancelDisplayOrderInfo.Size = new System.Drawing.Size(75, 23);
            this.btnCancelDisplayOrderInfo.TabIndex = 44;
            this.btnCancelDisplayOrderInfo.Text = "Cancel";
            this.btnCancelDisplayOrderInfo.UseVisualStyleBackColor = true;
            this.btnCancelDisplayOrderInfo.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnDisplayOrderInformation
            // 
            this.btnDisplayOrderInformation.Location = new System.Drawing.Point(280, 488);
            this.btnDisplayOrderInformation.Name = "btnDisplayOrderInformation";
            this.btnDisplayOrderInformation.Size = new System.Drawing.Size(75, 23);
            this.btnDisplayOrderInformation.TabIndex = 43;
            this.btnDisplayOrderInformation.Text = "Display";
            this.btnDisplayOrderInformation.UseVisualStyleBackColor = true;
            // 
            // grpBoxSearchDisplayOrderInfo
            // 
            this.grpBoxSearchDisplayOrderInfo.Controls.Add(this.tbxOrderDisplay);
            this.grpBoxSearchDisplayOrderInfo.Controls.Add(this.lblOrderDisplay);
            this.grpBoxSearchDisplayOrderInfo.Controls.Add(this.tbxOrderInformation);
            this.grpBoxSearchDisplayOrderInfo.Controls.Add(this.lblOrderSelectInfo);
            this.grpBoxSearchDisplayOrderInfo.Controls.Add(this.lblSelectSearchMode);
            this.grpBoxSearchDisplayOrderInfo.Controls.Add(this.cmBoxOrderModeOfSearch);
            this.grpBoxSearchDisplayOrderInfo.Location = new System.Drawing.Point(48, 177);
            this.grpBoxSearchDisplayOrderInfo.Name = "grpBoxSearchDisplayOrderInfo";
            this.grpBoxSearchDisplayOrderInfo.Size = new System.Drawing.Size(410, 300);
            this.grpBoxSearchDisplayOrderInfo.TabIndex = 41;
            this.grpBoxSearchDisplayOrderInfo.TabStop = false;
            this.grpBoxSearchDisplayOrderInfo.Text = "Search && Display Existing Information";
            // 
            // tbxOrderDisplay
            // 
            this.tbxOrderDisplay.Location = new System.Drawing.Point(25, 154);
            this.tbxOrderDisplay.Multiline = true;
            this.tbxOrderDisplay.Name = "tbxOrderDisplay";
            this.tbxOrderDisplay.Size = new System.Drawing.Size(343, 118);
            this.tbxOrderDisplay.TabIndex = 5;
            // 
            // lblOrderDisplay
            // 
            this.lblOrderDisplay.AutoSize = true;
            this.lblOrderDisplay.Location = new System.Drawing.Point(22, 130);
            this.lblOrderDisplay.Name = "lblOrderDisplay";
            this.lblOrderDisplay.Size = new System.Drawing.Size(91, 13);
            this.lblOrderDisplay.TabIndex = 4;
            this.lblOrderDisplay.Text = "Order Information:";
            // 
            // tbxOrderInformation
            // 
            this.tbxOrderInformation.Location = new System.Drawing.Point(25, 96);
            this.tbxOrderInformation.Name = "tbxOrderInformation";
            this.tbxOrderInformation.Size = new System.Drawing.Size(343, 20);
            this.tbxOrderInformation.TabIndex = 3;
            // 
            // lblOrderSelectInfo
            // 
            this.lblOrderSelectInfo.AutoSize = true;
            this.lblOrderSelectInfo.Location = new System.Drawing.Point(22, 75);
            this.lblOrderSelectInfo.Name = "lblOrderSelectInfo";
            this.lblOrderSelectInfo.Size = new System.Drawing.Size(132, 13);
            this.lblOrderSelectInfo.TabIndex = 2;
            this.lblOrderSelectInfo.Text = "Enter selected information:";
            // 
            // lblSelectSearchMode
            // 
            this.lblSelectSearchMode.AutoSize = true;
            this.lblSelectSearchMode.Location = new System.Drawing.Point(21, 41);
            this.lblSelectSearchMode.Name = "lblSelectSearchMode";
            this.lblSelectSearchMode.Size = new System.Drawing.Size(125, 13);
            this.lblSelectSearchMode.TabIndex = 1;
            this.lblSelectSearchMode.Text = "Select a mode of search:";
            // 
            // cmBoxOrderModeOfSearch
            // 
            this.cmBoxOrderModeOfSearch.FormattingEnabled = true;
            this.cmBoxOrderModeOfSearch.Items.AddRange(new object[] {
            "Reference Order",
            "Customer Name",
            "Order Date"});
            this.cmBoxOrderModeOfSearch.Location = new System.Drawing.Point(172, 42);
            this.cmBoxOrderModeOfSearch.Name = "cmBoxOrderModeOfSearch";
            this.cmBoxOrderModeOfSearch.Size = new System.Drawing.Size(196, 21);
            this.cmBoxOrderModeOfSearch.TabIndex = 0;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(452, 47);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(100, 81);
            this.pictureBox11.TabIndex = 40;
            this.pictureBox11.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(45, 123);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(231, 16);
            this.label12.TabIndex = 38;
            this.label12.Text = "Please enter the following information:";
            // 
            // lblSearchDisplayOrder
            // 
            this.lblSearchDisplayOrder.AutoSize = true;
            this.lblSearchDisplayOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchDisplayOrder.Location = new System.Drawing.Point(43, 50);
            this.lblSearchDisplayOrder.Name = "lblSearchDisplayOrder";
            this.lblSearchDisplayOrder.Size = new System.Drawing.Size(349, 25);
            this.lblSearchDisplayOrder.TabIndex = 39;
            this.lblSearchDisplayOrder.Text = "Search && Display Order Information";
            this.lblSearchDisplayOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlModifyOrder
            // 
            this.pnlModifyOrder.AutoScroll = true;
            this.pnlModifyOrder.Controls.Add(this.button1);
            this.pnlModifyOrder.Controls.Add(this.button2);
            this.pnlModifyOrder.Controls.Add(this.pictureBox12);
            this.pnlModifyOrder.Controls.Add(this.lblSearchModifyORNum);
            this.pnlModifyOrder.Controls.Add(this.lblSearchModifyOrder);
            this.pnlModifyOrder.Controls.Add(this.grpboxSearchExisting);
            this.pnlModifyOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlModifyOrder.Location = new System.Drawing.Point(0, 0);
            this.pnlModifyOrder.Name = "pnlModifyOrder";
            this.pnlModifyOrder.Size = new System.Drawing.Size(584, 444);
            this.pnlModifyOrder.TabIndex = 56;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(415, 350);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 39;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(312, 350);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 38;
            this.button2.Text = "Search";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(452, 47);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(100, 81);
            this.pictureBox12.TabIndex = 37;
            this.pictureBox12.TabStop = false;
            // 
            // lblSearchModifyORNum
            // 
            this.lblSearchModifyORNum.AutoSize = true;
            this.lblSearchModifyORNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchModifyORNum.Location = new System.Drawing.Point(60, 120);
            this.lblSearchModifyORNum.Name = "lblSearchModifyORNum";
            this.lblSearchModifyORNum.Size = new System.Drawing.Size(325, 16);
            this.lblSearchModifyORNum.TabIndex = 0;
            this.lblSearchModifyORNum.Text = "Please enter an Order Reference Number to Search...";
            // 
            // lblSearchModifyOrder
            // 
            this.lblSearchModifyOrder.AutoSize = true;
            this.lblSearchModifyOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchModifyOrder.Location = new System.Drawing.Point(58, 50);
            this.lblSearchModifyOrder.Name = "lblSearchModifyOrder";
            this.lblSearchModifyOrder.Size = new System.Drawing.Size(184, 25);
            this.lblSearchModifyOrder.TabIndex = 36;
            this.lblSearchModifyOrder.Text = "Modify New Order";
            this.lblSearchModifyOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpboxSearchExisting
            // 
            this.grpboxSearchExisting.Controls.Add(this.tbxSearchModifyORnum);
            this.grpboxSearchExisting.Controls.Add(this.lblSearchMofidyOrderRefNUm);
            this.grpboxSearchExisting.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpboxSearchExisting.Location = new System.Drawing.Point(63, 160);
            this.grpboxSearchExisting.Name = "grpboxSearchExisting";
            this.grpboxSearchExisting.Size = new System.Drawing.Size(401, 173);
            this.grpboxSearchExisting.TabIndex = 35;
            this.grpboxSearchExisting.TabStop = false;
            this.grpboxSearchExisting.Text = "Search for existing Order";
            // 
            // tbxSearchModifyORnum
            // 
            this.tbxSearchModifyORnum.Location = new System.Drawing.Point(36, 80);
            this.tbxSearchModifyORnum.Name = "tbxSearchModifyORnum";
            this.tbxSearchModifyORnum.Size = new System.Drawing.Size(256, 22);
            this.tbxSearchModifyORnum.TabIndex = 1;
            // 
            // lblSearchMofidyOrderRefNUm
            // 
            this.lblSearchMofidyOrderRefNUm.AutoSize = true;
            this.lblSearchMofidyOrderRefNUm.Location = new System.Drawing.Point(33, 52);
            this.lblSearchMofidyOrderRefNUm.Name = "lblSearchMofidyOrderRefNUm";
            this.lblSearchMofidyOrderRefNUm.Size = new System.Drawing.Size(162, 16);
            this.lblSearchMofidyOrderRefNUm.TabIndex = 0;
            this.lblSearchMofidyOrderRefNUm.Text = "Order Reference Number:";
            // 
            // pnlModifyOrderInfo
            // 
            this.pnlModifyOrderInfo.AutoScroll = true;
            this.pnlModifyOrderInfo.Controls.Add(this.button3);
            this.pnlModifyOrderInfo.Controls.Add(this.label9);
            this.pnlModifyOrderInfo.Controls.Add(this.groupBox1);
            this.pnlModifyOrderInfo.Controls.Add(this.groupBox2);
            this.pnlModifyOrderInfo.Controls.Add(this.button4);
            this.pnlModifyOrderInfo.Controls.Add(this.label24);
            this.pnlModifyOrderInfo.Controls.Add(this.pictureBox13);
            this.pnlModifyOrderInfo.Controls.Add(this.label25);
            this.pnlModifyOrderInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlModifyOrderInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlModifyOrderInfo.Name = "pnlModifyOrderInfo";
            this.pnlModifyOrderInfo.Size = new System.Drawing.Size(584, 444);
            this.pnlModifyOrderInfo.TabIndex = 57;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(429, 626);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(67, 23);
            this.button3.TabIndex = 49;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(37, 661);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(129, 13);
            this.label9.TabIndex = 48;
            this.label9.Text = "* Denotes Mandatory field";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.textBox14);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.textBox15);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Location = new System.Drawing.Point(48, 475);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(410, 144);
            this.groupBox1.TabIndex = 47;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Item Information";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(339, 102);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(39, 20);
            this.textBox5.TabIndex = 47;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(307, 105);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 46;
            this.label10.Text = "Qty:";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(69, 105);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(222, 20);
            this.textBox6.TabIndex = 45;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(69, 68);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(222, 20);
            this.textBox14.TabIndex = 44;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(24, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 43;
            this.label11.Text = "* Item 1:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(31, 105);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 13);
            this.label13.TabIndex = 42;
            this.label13.Text = "Item 2:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(26, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(178, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Enter Item and Quantity of Purchase";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(339, 68);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(39, 20);
            this.textBox15.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(307, 71);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(26, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Qty:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox16);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.textBox17);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.textBox18);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.textBox19);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.dateTimePicker2);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Location = new System.Drawing.Point(48, 158);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(404, 296);
            this.groupBox2.TabIndex = 46;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Order Information";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(20, 251);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(75, 20);
            this.textBox16.TabIndex = 30;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 230);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Postal Code:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(19, 147);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 13);
            this.label17.TabIndex = 28;
            this.label17.Text = "Street:";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(20, 172);
            this.textBox17.Multiline = true;
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(209, 48);
            this.textBox17.TabIndex = 26;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(101, 118);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 13);
            this.label18.TabIndex = 21;
            this.label18.Text = "Unit No.:";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(150, 115);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(79, 20);
            this.textBox18.TabIndex = 20;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(19, 118);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 19;
            this.label19.Text = "Blk: #";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(55, 115);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(40, 20);
            this.textBox19.TabIndex = 18;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(19, 90);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(111, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "* Destination Address:";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(22, 58);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(209, 20);
            this.dateTimePicker2.TabIndex = 14;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(19, 33);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(83, 13);
            this.label21.TabIndex = 10;
            this.label21.Text = "* Date Of Order:";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(326, 626);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(67, 23);
            this.button4.TabIndex = 45;
            this.button4.Text = "Add";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(47, 122);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(231, 16);
            this.label24.TabIndex = 42;
            this.label24.Text = "Please enter the following information:";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(452, 47);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(100, 81);
            this.pictureBox13.TabIndex = 41;
            this.pictureBox13.TabStop = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(45, 50);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(248, 25);
            this.label25.TabIndex = 40;
            this.label25.Text = "Modify Order Information";
            // 
            // pnlDeleteOrder
            // 
            this.pnlDeleteOrder.AutoScroll = true;
            this.pnlDeleteOrder.Controls.Add(this.button5);
            this.pnlDeleteOrder.Controls.Add(this.button6);
            this.pnlDeleteOrder.Controls.Add(this.pictureBox14);
            this.pnlDeleteOrder.Controls.Add(this.label22);
            this.pnlDeleteOrder.Controls.Add(this.label23);
            this.pnlDeleteOrder.Controls.Add(this.groupBox3);
            this.pnlDeleteOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDeleteOrder.Location = new System.Drawing.Point(0, 0);
            this.pnlDeleteOrder.Name = "pnlDeleteOrder";
            this.pnlDeleteOrder.Size = new System.Drawing.Size(584, 444);
            this.pnlDeleteOrder.TabIndex = 58;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(415, 350);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 39;
            this.button5.Text = "Cancel";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(312, 350);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 38;
            this.button6.Text = "Delete";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(452, 47);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(100, 81);
            this.pictureBox14.TabIndex = 37;
            this.pictureBox14.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(60, 120);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(320, 16);
            this.label22.TabIndex = 0;
            this.label22.Text = "Please enter an Order Reference Number to delete...";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(58, 50);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(246, 25);
            this.label23.TabIndex = 36;
            this.label23.Text = "Delete Order Information";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbxORNum);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(63, 160);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(401, 173);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Delete Order";
            // 
            // tbxORNum
            // 
            this.tbxORNum.Location = new System.Drawing.Point(36, 80);
            this.tbxORNum.Name = "tbxORNum";
            this.tbxORNum.Size = new System.Drawing.Size(256, 22);
            this.tbxORNum.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(33, 52);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(162, 16);
            this.label26.TabIndex = 0;
            this.label26.Text = "Order Reference Number:";
            // 
            // pnlDeleteSearchOrder
            // 
            this.pnlDeleteSearchOrder.AutoScroll = true;
            this.pnlDeleteSearchOrder.Controls.Add(this.button7);
            this.pnlDeleteSearchOrder.Controls.Add(this.button8);
            this.pnlDeleteSearchOrder.Controls.Add(this.pictureBox15);
            this.pnlDeleteSearchOrder.Controls.Add(this.label27);
            this.pnlDeleteSearchOrder.Controls.Add(this.label28);
            this.pnlDeleteSearchOrder.Controls.Add(this.groupBox4);
            this.pnlDeleteSearchOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDeleteSearchOrder.Location = new System.Drawing.Point(0, 0);
            this.pnlDeleteSearchOrder.Name = "pnlDeleteSearchOrder";
            this.pnlDeleteSearchOrder.Size = new System.Drawing.Size(584, 444);
            this.pnlDeleteSearchOrder.TabIndex = 59;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(415, 350);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 39;
            this.button7.Text = "Cancel";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(312, 350);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 38;
            this.button8.Text = "Search";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(452, 47);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(100, 81);
            this.pictureBox15.TabIndex = 37;
            this.pictureBox15.TabStop = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(60, 120);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(323, 16);
            this.label27.TabIndex = 0;
            this.label27.Text = "Please enter an Order Reference Number to search...";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(58, 50);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(246, 25);
            this.label28.TabIndex = 36;
            this.label28.Text = "Delete Order Information";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbxORNumSearch);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(63, 160);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(401, 173);
            this.groupBox4.TabIndex = 35;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Search Order";
            // 
            // tbxORNumSearch
            // 
            this.tbxORNumSearch.Location = new System.Drawing.Point(36, 80);
            this.tbxORNumSearch.Name = "tbxORNumSearch";
            this.tbxORNumSearch.Size = new System.Drawing.Size(256, 22);
            this.tbxORNumSearch.TabIndex = 1;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(33, 52);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(162, 16);
            this.label29.TabIndex = 0;
            this.label29.Text = "Order Reference Number:";
            // 
            // FrmMainStaff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 444);
            this.Controls.Add(this.mnuStrpMain);
            this.Controls.Add(this.pnlWelcome);
            this.Controls.Add(this.pnlSearchDisplayOrderInfo);
            this.Controls.Add(this.pnlSearchAndDisplay);
            this.Controls.Add(this.pnlModifyOrder);
            this.Controls.Add(this.pnlSearchAddOrder);
            this.Controls.Add(this.pnlDeleteOrder);
            this.Controls.Add(this.pnlDeleteStaffInfo);
            this.Controls.Add(this.pnlDeleteSearchOrder);
            this.Controls.Add(this.pnlSearchDeleteStaff);
            this.Controls.Add(this.pnlSearchModifyStaff);
            this.Controls.Add(this.pnlSearchStaff);
            this.Controls.Add(this.pnlModifyOrderInfo);
            this.Controls.Add(this.pnlAddNewOrder);
            this.Controls.Add(this.pnlModifyStaff);
            this.Controls.Add(this.pnlRegisterStaff);
            this.Name = "FrmMainStaff";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Welcome";
            this.mnuStrpMain.ResumeLayout(false);
            this.mnuStrpMain.PerformLayout();
            this.pnlSearchStaff.ResumeLayout(false);
            this.pnlSearchStaff.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grpBoxSearchStaff.ResumeLayout(false);
            this.grpBoxSearchStaff.PerformLayout();
            this.pnlWelcome.ResumeLayout(false);
            this.pnlWelcome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.pnlRegisterStaff.ResumeLayout(false);
            this.pnlRegisterStaff.PerformLayout();
            this.grpAccntDetails.ResumeLayout(false);
            this.grpAccntDetails.PerformLayout();
            this.grpPersonalParticulars.ResumeLayout(false);
            this.grpPersonalParticulars.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pnlModifyStaff.ResumeLayout(false);
            this.pnlModifyStaff.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.grpAccntDetails1.ResumeLayout(false);
            this.grpAccntDetails1.PerformLayout();
            this.grpPersonalParticular.ResumeLayout(false);
            this.grpPersonalParticular.PerformLayout();
            this.pnlSearchModifyStaff.ResumeLayout(false);
            this.pnlSearchModifyStaff.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.grpBoxSearchModifyStaff.ResumeLayout(false);
            this.grpBoxSearchModifyStaff.PerformLayout();
            this.pnlSearchAndDisplay.ResumeLayout(false);
            this.pnlSearchAndDisplay.PerformLayout();
            this.grpBoxSearchDisplayStaffInfo.ResumeLayout(false);
            this.grpBoxSearchDisplayStaffInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.pnlSearchDeleteStaff.ResumeLayout(false);
            this.pnlSearchDeleteStaff.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.grpBoxSearchDeleteStaff.ResumeLayout(false);
            this.grpBoxSearchDeleteStaff.PerformLayout();
            this.pnlDeleteStaffInfo.ResumeLayout(false);
            this.pnlDeleteStaffInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.grpBoxDelStaff.ResumeLayout(false);
            this.grpBoxDelStaff.PerformLayout();
            this.pnlAddNewOrder.ResumeLayout(false);
            this.pnlAddNewOrder.PerformLayout();
            this.grpOrderItemInfo.ResumeLayout(false);
            this.grpOrderItemInfo.PerformLayout();
            this.grpBoxAddOrderInfo.ResumeLayout(false);
            this.grpBoxAddOrderInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.pnlSearchAddOrder.ResumeLayout(false);
            this.pnlSearchAddOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.grpBoxSearchAddOrder.ResumeLayout(false);
            this.grpBoxSearchAddOrder.PerformLayout();
            this.pnlSearchDisplayOrderInfo.ResumeLayout(false);
            this.pnlSearchDisplayOrderInfo.PerformLayout();
            this.grpBoxSearchDisplayOrderInfo.ResumeLayout(false);
            this.grpBoxSearchDisplayOrderInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.pnlModifyOrder.ResumeLayout(false);
            this.pnlModifyOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.grpboxSearchExisting.ResumeLayout(false);
            this.grpboxSearchExisting.PerformLayout();
            this.pnlModifyOrderInfo.ResumeLayout(false);
            this.pnlModifyOrderInfo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.pnlDeleteOrder.ResumeLayout(false);
            this.pnlDeleteOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.pnlDeleteSearchOrder.ResumeLayout(false);
            this.pnlDeleteSearchOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuStrpMain;
        private System.Windows.Forms.ToolStripMenuItem staffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginLogoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registerNewStaffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyNewStaffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchForStaffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteExistingStaffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem catalogueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAndDisplayOrderToolStripMenuItem;
        private System.Windows.Forms.Panel pnlSearchStaff;
        private System.Windows.Forms.GroupBox grpBoxSearchStaff;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblInformation;
        private System.Windows.Forms.Label lblSearchStaff;
        private System.Windows.Forms.Label lblStaffName;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbxStaffName;
        private System.Windows.Forms.Panel pnlWelcome;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Label lblWelcome2;
        private System.Windows.Forms.Label lblWelcome1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlRegisterStaff;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblRegisterStaff;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSubmitRegistration;
        private System.Windows.Forms.GroupBox grpAccntDetails;
        private System.Windows.Forms.RadioButton rdBtnStaff;
        private System.Windows.Forms.RadioButton rdBtnAdministrator;
        private System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.TextBox tbxEmail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox grpPersonalParticulars;
        private System.Windows.Forms.Label lblHp;
        private System.Windows.Forms.Label lblHome;
        private System.Windows.Forms.Label lblContactNo;
        private System.Windows.Forms.TextBox tbxHpNo;
        private System.Windows.Forms.TextBox tbxHomeNo;
        private System.Windows.Forms.TextBox tbxPostCode;
        private System.Windows.Forms.Label lblPostalCode;
        private System.Windows.Forms.Label lblStreet;
        private System.Windows.Forms.TextBox tbxAddress;
        private System.Windows.Forms.Label lblUnitNo;
        private System.Windows.Forms.TextBox tbxUnitNo;
        private System.Windows.Forms.Label lblBlock;
        private System.Windows.Forms.TextBox tbxBlockNo;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.RadioButton rdBtnFemale;
        private System.Windows.Forms.RadioButton rdBtnMale;
        private System.Windows.Forms.DateTimePicker dtpDateOfBirth;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblDateOfBirth;
        private System.Windows.Forms.TextBox tbxNRIC;
        private System.Windows.Forms.Label lblNRIC;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblInformation1;
        private System.Windows.Forms.Button btnCancelRegistration;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label lblRegisterPassword;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label lblCfmPassword;
        private System.Windows.Forms.Panel pnlModifyStaff;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lblModifyStaff;
        private System.Windows.Forms.Button btnSubmit1;
        private System.Windows.Forms.GroupBox grpAccntDetails1;
        private System.Windows.Forms.RadioButton rdBtnStaff1;
        private System.Windows.Forms.RadioButton rdBtnAdministrator1;
        private System.Windows.Forms.Label lblRole1;
        private System.Windows.Forms.TextBox tbxEmail1;
        private System.Windows.Forms.Label lblEmail1;
        private System.Windows.Forms.GroupBox grpPersonalParticular;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblContactNo1;
        private System.Windows.Forms.TextBox tbxHpNo1;
        private System.Windows.Forms.TextBox tbxHomeNo1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblStreet1;
        private System.Windows.Forms.TextBox tbxStreet1;
        private System.Windows.Forms.Label lblUnitNo1;
        private System.Windows.Forms.TextBox tbxUnitNo1;
        private System.Windows.Forms.Label lblBlock1;
        private System.Windows.Forms.TextBox tbxBlockNo1;
        private System.Windows.Forms.Label lblAddress1;
        private System.Windows.Forms.TextBox tbxNRIC1;
        private System.Windows.Forms.Label lblNRIC1;
        private System.Windows.Forms.Button btnCancelModify;
        private System.Windows.Forms.Panel pnlSearchModifyStaff;
        private System.Windows.Forms.Button btnCancelSearch;
        private System.Windows.Forms.Button btnSearchForStaff;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label lblSearchModifyStaffInfo;
        private System.Windows.Forms.Label lblSearchModifyStaff;
        private System.Windows.Forms.GroupBox grpBoxSearchModifyStaff;
        private System.Windows.Forms.TextBox tbxSearchName;
        private System.Windows.Forms.Label lblSearchName;
        private System.Windows.Forms.Panel pnlSearchAndDisplay;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label lblSearchDisplayInfo;
        private System.Windows.Forms.Label lblSearchDisp;
        private System.Windows.Forms.GroupBox grpBoxSearchDisplayStaffInfo;
        private System.Windows.Forms.ComboBox cmBoxTypeofSearch;
        private System.Windows.Forms.Label lblEnterInformation;
        private System.Windows.Forms.Label lblTypeOfSearch;
        private System.Windows.Forms.TextBox tbxSelectStaffInfo;
        private System.Windows.Forms.Button btnDisplayStaffInfo;
        private System.Windows.Forms.TextBox tbxDisplayStaffInfo;
        private System.Windows.Forms.Label lblStaffInformation;
        private System.Windows.Forms.Button btnCancelDisplay;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel pnlSearchDeleteStaff;
        private System.Windows.Forms.Button btnCancelSearchDelete;
        private System.Windows.Forms.Button btnSearchDeleteStaff;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label lblSearchDeleteInfo;
        private System.Windows.Forms.Label lblSearchDeleteStaff;
        private System.Windows.Forms.GroupBox grpBoxSearchDeleteStaff;
        private System.Windows.Forms.TextBox tbxSearchDeleteStaff;
        private System.Windows.Forms.Label lblSearchDeleteStaffName;
        private System.Windows.Forms.Panel pnlDeleteStaffInfo;
        private System.Windows.Forms.Button btnCancelDeleteStaff;
        private System.Windows.Forms.Button btnDeleteStaff;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label lblDeleteInfo;
        private System.Windows.Forms.Label lblDeleteStaffInformation;
        private System.Windows.Forms.GroupBox grpBoxDelStaff;
        private System.Windows.Forms.TextBox tbxDeleteStaff;
        private System.Windows.Forms.Label lblDeleteStaffName;
        private System.Windows.Forms.Panel pnlAddNewOrder;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox grpOrderItemInfo;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label lblAddOrderQty2;
        private System.Windows.Forms.TextBox tbxQuantityAddOrder2;
        private System.Windows.Forms.TextBox tbxTitleAddOrder2;
        private System.Windows.Forms.Label lblItemAddOrder2;
        private System.Windows.Forms.Label lblItemAddOrder1;
        private System.Windows.Forms.Label lblAddOrderInfo;
        private System.Windows.Forms.TextBox tbxNRIC3;
        private System.Windows.Forms.Label lblAddItemQty1;
        private System.Windows.Forms.GroupBox grpBoxAddOrderInfo;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label lblAddPostCode;
        private System.Windows.Forms.Label lblAddStreet;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label lblAddUnitNum;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label lblAddBlk;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label lblAddAddress;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lblAddDateOfOrder;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label lblAddNameOfCust;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label lblAddORNum;
        private System.Windows.Forms.Button btnAddOrder;
        private System.Windows.Forms.Label lblAddNewOrderInfo;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label lblAddNewOrder;
        private System.Windows.Forms.Panel pnlSearchAddOrder;
        private System.Windows.Forms.Button btnCancelAddOrder;
        private System.Windows.Forms.Button btnSearchAddOrder;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Label lblODRnum;
        private System.Windows.Forms.Label lblSearchAddOrder;
        private System.Windows.Forms.GroupBox grpBoxSearchAddOrder;
        private System.Windows.Forms.TextBox tbxSearchAddOrder;
        private System.Windows.Forms.Label lblOrderRefNum;
        private System.Windows.Forms.Button btnCancelAddNewOrder;
        private System.Windows.Forms.Panel pnlSearchDisplayOrderInfo;
        private System.Windows.Forms.Button btnCancelDisplayOrderInfo;
        private System.Windows.Forms.Button btnDisplayOrderInformation;
        private System.Windows.Forms.GroupBox grpBoxSearchDisplayOrderInfo;
        private System.Windows.Forms.TextBox tbxOrderDisplay;
        private System.Windows.Forms.Label lblOrderDisplay;
        private System.Windows.Forms.TextBox tbxOrderInformation;
        private System.Windows.Forms.Label lblOrderSelectInfo;
        private System.Windows.Forms.Label lblSelectSearchMode;
        private System.Windows.Forms.ComboBox cmBoxOrderModeOfSearch;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblSearchDisplayOrder;
        private System.Windows.Forms.Panel pnlModifyOrder;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Label lblSearchModifyORNum;
        private System.Windows.Forms.Label lblSearchModifyOrder;
        private System.Windows.Forms.GroupBox grpboxSearchExisting;
        private System.Windows.Forms.TextBox tbxSearchModifyORnum;
        private System.Windows.Forms.Label lblSearchMofidyOrderRefNUm;
        private System.Windows.Forms.Panel pnlModifyOrderInfo;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ToolStripMenuItem deleteOrderInformationToolStripMenuItem;
        private System.Windows.Forms.Panel pnlDeleteOrder;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbxORNum;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel pnlDeleteSearchOrder;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbxORNumSearch;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ToolStripMenuItem addNewCategoryInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteCategoryInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAndDisplayCategoryInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addNewProductInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyProductInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteProductInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAndDisplayProductInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_AddSupplier;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_ModifySupplier;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_DeleteSupplier;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_SearchSupplier;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem menuStock_EditProductQty;
        private System.Windows.Forms.ToolStripMenuItem menuStock_SearchProductQty;
        private System.Windows.Forms.ToolStripMenuItem menuStock_computeExcessShortFall;
    }
}