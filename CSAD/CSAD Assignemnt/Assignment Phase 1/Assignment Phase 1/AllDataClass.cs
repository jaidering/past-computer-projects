﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_Phase_1
{
    public class AllDataClass
    {
        
        public frmCatalogue frmCata;
        public FrmMainStaff frmMain;
        public FrmLogin frmLog;
        public Form1 frmInventory;

        //constructor
        public AllDataClass(FrmLogin _mainWindowForm)
        {
            frmLog= _mainWindowForm;
            frmCata = new frmCatalogue(this);
            frmMain = new FrmMainStaff(this);
            frmInventory = new Form1(this);
           

            

        }
    }
}
