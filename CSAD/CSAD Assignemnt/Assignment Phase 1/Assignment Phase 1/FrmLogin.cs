﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_1
{
    public partial class FrmLogin : Form
    {
        private AllDataClass allData;
        public FrmLogin()
        {
            InitializeComponent();
            allData = new AllDataClass(this);
        }

        public void btnLogin_Click(object sender, EventArgs e)
        {
            
            if (tbxName.Text == "admin" && tbxPassword.Text == "pass123")
            {
                this.Hide();
                allData.frmMain.Show();
            }
            else if (tbxName.Text == "staff" && tbxPassword.Text == "pass123")
            {
                this.Hide();
                allData.frmMain.Show();
            }
            else
            {
                MessageBox.Show("Invalid Username or Password");
            }
        }

        public void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }
    }
}
