﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_1
{
    public partial class Form1 : Form
    {
        private AllDataClass allData;
        public Form1(AllDataClass adc)
        {
            InitializeComponent();
            allData = adc;
        }

        public void btnAddSupplier_CheckCompany_Click(object sender, EventArgs e)
        {


            if (tbxAddSupplier_CheckCompanyName.Text == "")
            {
                MessageBox.Show("Please fill in details");
            }
            else
                if (tbxAddSupplier_CheckCompanyName.Text == "Nano")
                {
                    MessageBox.Show("Company already exists");
                    tbxAddCompanyName.Text = tbxAddSupplier_CheckCompanyName.Text;
                    tbxAddSupplier_CheckCompanyName.Text = "";
                    pnlAddSupplier.BringToFront();
                }


                else
                {
                    tbxAddCompanyName.Text = tbxAddSupplier_CheckCompanyName.Text;
                    tbxAddSupplier_CheckCompanyName.Text = "";
                    pnlAddSupplier.BringToFront();
                }
        
            
        }

        public void SupplierMenu_AddSupplier_Click(object sender, EventArgs e)
        {
            pnlAdd_check.BringToFront();
        }

        public void SupplierMenu_ModifySupplier_Click(object sender, EventArgs e)
        {
            pnlModifySupplier_Check.BringToFront();
        }

        public void SupplierMenu_DeleteSupplier_Click(object sender, EventArgs e)
        {
            pnlDeleteSupplier_Check.BringToFront();
        }

        public void SupplierMenu_SearchSupplier_Click(object sender, EventArgs e)
        {
            pnlSearchSupplier_Check.BringToFront();
        }

        public void btnDeleteSupplier_Check_Search_Click(object sender, EventArgs e)
        {
            pnlDeleteSupplier_Info.BringToFront(); ;
        }

        public void btnModifySupplier_Check_Search_Click(object sender, EventArgs e)
        {
            pnlModifySupplier_Info.BringToFront();
        }

        public void btnSearchSupplier_Check_Search_Click(object sender, EventArgs e)
        {
            pnlSearchSupplier_Info.BringToFront();
        }

        public void menuStock_EditProductQty_Click(object sender, EventArgs e)
        {
            pnlStockSearchToEdit_Chck.BringToFront();
        }

        public void btnCheckProToDeleteExist_Click(object sender, EventArgs e)
        {
            pnlStockEdit_NotFound.BringToFront();
            //still got Stock Found
        }

        public void menuStock_SearchProductQty_Click(object sender, EventArgs e)
        {
            pnlSearchDisplayProductQty_Search.BringToFront();
        }

        public void button10_Click(object sender, EventArgs e)
        {
            pnlStock_DisplaySearchedQty.BringToFront();
        }

        public void menuStock_computeExcessShortFall_Click(object sender, EventArgs e)
        {
            pnlExcessShortFall_Search.BringToFront();
        }

        

        public void btnStock_Found_Update_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Stock has been updated. Thank you.");
        }

        public void btnStock_NotFound_Add_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Stock has been added.");
        }

        public void btnStock_ExcessShortFall_Search_Click(object sender, EventArgs e)
        {
            pnlStockExcessShortFall_Display.BringToFront();
        }

        private void loginLogoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.loginLogoutToolStripMenuItem_Click(this, e);

        }

        private void registerNewStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.registerNewStaffToolStripMenuItem_Click(this, e);
        }

        private void modifyNewStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.modifyNewStaffToolStripMenuItem_Click(this,e);
        }

        private void searchForStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.searchForStaffToolStripMenuItem_Click(this, e);
        }

        private void deleteExistingStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.deleteExistingStaffToolStripMenuItem_Click(this, e);
        }

        private void addNewCatalogueInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.addNewCatalogueInfoToolStripMenuItem_Click(this,e);
        }

        private void modifyCatalogueInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.modifyCatalogueInfoToolStripMenuItem_Click(this, e);
        }

        private void deleteCatalogueInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.deleteCatalogueInfoToolStripMenuItem_Click(this, e);
        }

        private void searchAndDisplayCatalogueInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.searchAndDisplayCatalogueInfoToolStripMenuItem_Click(this, e);
        }

        private void addNewProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.addNewProductInfoToolStripMenuItem_Click(this, e);
        }

        private void modifyProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.modifyProductInfoToolStripMenuItem_Click(this, e);
        }

        private void deleteProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.deleteProductInfoToolStripMenuItem_Click(this, e);
        }

        private void searchAndDisplayProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.searchAndDisplayProductInfoToolStripMenuItem_Click(this,e);
        }

        private void addNewOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.addNewOrderToolStripMenuItem_Click(this, e);
        }

        private void modifyOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.modifyOrderToolStripMenuItem_Click(this, e);
        }

        private void searchAndDisplayOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.searchAndDisplayOrderToolStripMenuItem_Click(this, e);
        }

        private void deleteOrderInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmMain.Show();
            allData.frmMain.deleteOrderInformationToolStripMenuItem_Click(this, e);
        }





        

       

        

        

        
    }
}
