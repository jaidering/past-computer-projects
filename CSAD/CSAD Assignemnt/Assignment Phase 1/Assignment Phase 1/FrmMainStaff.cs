﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace Assignment_Phase_1
{
    public partial class FrmMainStaff : Form
    {
        private AllDataClass allData;
        public FrmMainStaff(AllDataClass adc)
        {
            InitializeComponent();
            allData = adc;
        }

        public void loginLogoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmLogin frmlogin = new FrmLogin();
            this.Close();
            frmlogin.Show();
        }

        public void registerNewStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Register New Staff";
            pnlSearchStaff.BringToFront();
            pnlWelcome.Visible = false;
            pnlRegisterStaff.Visible = false;
            pnlSearchStaff.Visible = true;
            pnlModifyStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            if (tbxStaffName.Text == "")
            {
                MessageBox.Show("Please enter a Staff Name.");
                return;
            }
            else if (tbxStaffName.Text == "admin")
            {
                MessageBox.Show("Staff is Found!");
                tbxStaffName.Text = "";
            }
            else
            {
                MessageBox.Show("Staff is not Found. Proceed to register.");
                pnlWelcome.Visible = false;
                pnlRegisterStaff.Visible = true;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
        }

        public void btnCancel_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlRegisterStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void btnCancelRegistration_Click(object sender, EventArgs e)
        {
            pnlRegisterStaff.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void modifyNewStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Modify existing Staff Information";
            pnlSearchModifyStaff.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = true;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void btnCancelModify_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void btnCancelSearch_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void btnSearchForStaff_Click(object sender, EventArgs e)
        {
            if (tbxSearchName.Text == "")
            {
                MessageBox.Show("Please enter a Staff Name.");
                return;
            }
            else if (tbxSearchName.Text == "admin")
            {
                MessageBox.Show("Staff Information is found! Proceed to make changes.");
                pnlModifyStaff.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = true;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
            else
            {
                MessageBox.Show("Staff information cannot be found.");
            }
        }

        public void btnCancelDisplay_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void searchForStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Search and Display Staff";
            pnlSearchAndDisplay.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = true;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void btnSearchDeleteStaff_Click(object sender, EventArgs e)
        {
            if (tbxSearchDeleteStaff.Text == "")
            {
                MessageBox.Show("Please enter a Staff Name.");
                return;
            }
            else if (tbxSearchDeleteStaff.Text == "staff")
            {
                MessageBox.Show("Staff Information is found! Proceed to delete existing staff information.");
                pnlDeleteStaffInfo.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlDeleteStaffInfo.Visible = true;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
            else
            {
                MessageBox.Show("Staff information cannot be found.");
            }
        }

        public void deleteExistingStaffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Delete Existing Staff Information";
            pnlSearchDeleteStaff.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = true;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void btnCancelSearchDelete_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void btnCancelDeleteStaff_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void btnDeleteStaff_Click(object sender, EventArgs e)
        {
            if (tbxDeleteStaff.Text == "")
            {
                MessageBox.Show("Please enter a Staff Name to delete staff information.");
                return;
            }
            else if (tbxDeleteStaff.Text == "staff")
            {
                MessageBox.Show("Deletion of existing Staff Information is successful.");
            }
            else
            {
                MessageBox.Show("Deletion of existing Staff Information was unsuccessful");
            }
        }

        public void btnSearchAddOrder_Click(object sender, EventArgs e)
        {
            if (tbxSearchAddOrder.Text == "PO001")
            {
                MessageBox.Show("Order Found!");
            }
            else if (tbxSearchAddOrder.Text == "")
            {
                MessageBox.Show("Please enter Order Reference Number in this format: 'POXXX'.");
            }
            else
            {
                MessageBox.Show("Order cannot be found. Proceed to Add New Order Information.");
                pnlAddNewOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = true;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
        }

        public void addNewOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Add New Order";
            pnlSearchAddOrder.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlSearchAddOrder.Visible = true;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void btnCancelAddOrder_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void btnCancelAddNewOrder_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void button1_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void button1_Click_1(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void button2_Click(object sender, EventArgs e)
        {
            if (tbxSearchModifyORnum.Text == "PO001" || tbxSearchModifyORnum.Text == "PO002")
            {
                MessageBox.Show("Order Found! Proceed to Modify Order Inforamtion");
                pnlModifyOrderInfo.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = true;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
            else if (tbxSearchModifyORnum.Text == "")
            {
                MessageBox.Show("Please enter Order Reference Number in this format: 'POXXX'.");
            }
            else
            {
                MessageBox.Show("Order cannot be found. Proceed to Add New Order Information.");
                pnlAddNewOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = true;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
        }

        public void button3_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void modifyOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Modify Order Information";
            pnlModifyOrder.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlSearchAddOrder.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = true;
            pnlDeleteOrder.Visible = false;
        }

        public void searchAndDisplayOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Search and Display Order Information";
            pnlSearchDisplayOrderInfo.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlSearchAddOrder.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = true;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void deleteOrderInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Delete Order Information";
            pnlDeleteSearchOrder.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = false;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlSearchAddOrder.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
            pnlDeleteSearchOrder.Visible = true;
        }

        public void button5_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void button6_Click(object sender, EventArgs e)
        {
            if (tbxSearchModifyORnum.Text == "PO001" || tbxSearchModifyORnum.Text == "PO002")
            {
                MessageBox.Show("Order Found! Proceed to Delete Order Information");
                pnlDeleteOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = true;
            }
            else if (tbxSearchModifyORnum.Text == "")
            {
                MessageBox.Show("Please enter Order Reference Number in this format: 'POXXX'.");
            }
            else
            {
                MessageBox.Show("Order cannot be found. Proceed to Add New Order Information.");
                pnlAddNewOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = true;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
        }

        public void button8_Click(object sender, EventArgs e)
        {
            if (tbxORNumSearch.Text == "PO001" || tbxORNumSearch.Text == "PO002")
            {
                MessageBox.Show("Order Found! Proceed to Delete Order Information");
                pnlDeleteOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = false;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = true;
            }
            else if (tbxSearchModifyORnum.Text == "")
            {
                MessageBox.Show("Please enter Order Reference Number in this format: 'POXXX'.");
            }
            else
            {
                MessageBox.Show("Order cannot be found. Proceed to Add New Order Information.");
                pnlAddNewOrder.BringToFront();
                pnlRegisterStaff.Visible = false;
                pnlWelcome.Visible = false;
                pnlSearchStaff.Visible = false;
                pnlModifyStaff.Visible = false;
                pnlSearchModifyStaff.Visible = false;
                pnlSearchAndDisplay.Visible = false;
                pnlSearchDeleteStaff.Visible = false;
                pnlDeleteStaffInfo.Visible = false;
                pnlSearchAddOrder.Visible = false;
                pnlAddNewOrder.Visible = true;
                pnlModifyOrderInfo.Visible = false;
                pnlSearchDisplayOrderInfo.Visible = false;
                pnlModifyOrder.Visible = false;
                pnlDeleteOrder.Visible = false;
            }
        }

        public void button7_Click(object sender, EventArgs e)
        {
            pnlWelcome.BringToFront();
            pnlRegisterStaff.Visible = false;
            pnlWelcome.Visible = true;
            pnlSearchStaff.Visible = false;
            pnlModifyStaff.Visible = false;
            pnlSearchModifyStaff.Visible = false;
            pnlSearchAndDisplay.Visible = false;
            pnlSearchDeleteStaff.Visible = false;
            pnlDeleteStaffInfo.Visible = false;
            pnlAddNewOrder.Visible = false;
            pnlModifyOrderInfo.Visible = false;
            pnlSearchDisplayOrderInfo.Visible = false;
            pnlModifyOrder.Visible = false;
            pnlDeleteOrder.Visible = false;
        }

        public void addNewCategoryInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.addNewCatalogueInfoToolStripMenuItem_Click(this,e);
        }

        public void modifyCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.modifyCatalogueInfoToolStripMenuItem_Click(this, e);
        }

        public void deleteCategoryInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.deleteCatalogueInfoToolStripMenuItem_Click(this, e);
        }

        public void searchAndDisplayCategoryInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.searchAndDisplayCatalogueInfoToolStripMenuItem_Click(this, e);
        }

        public void addNewProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.addNewProductInfoToolStripMenuItem_Click(this, e);
        }

        public void modifyProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.modifyProductInfoToolStripMenuItem_Click(this, e);
        }

        public void deleteProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.deleteProductInfoToolStripMenuItem_Click(this, e);
        }

        public void searchAndDisplayProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmCata.Show();
            allData.frmCata.searchAndDisplayProductInfoToolStripMenuItem_Click(this, e);
        }

        public void SupplierMenu_AddSupplier_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.SupplierMenu_AddSupplier_Click(this, e);
        }

        public void SupplierMenu_ModifySupplier_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.SupplierMenu_ModifySupplier_Click(this, e);
        }

        private void SupplierMenu_DeleteSupplier_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.SupplierMenu_DeleteSupplier_Click(this, e);
        }

        private void SupplierMenu_SearchSupplier_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.SupplierMenu_SearchSupplier_Click(this, e);
        }

        private void menuStock_EditProductQty_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.menuStock_EditProductQty_Click(this, e);
        }

        private void menuStock_SearchProductQty_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.menuStock_SearchProductQty_Click(this, e);
        }

        private void menuStock_computeExcessShortFall_Click(object sender, EventArgs e)
        {
            this.Hide();
            allData.frmInventory.Show();
            allData.frmInventory.menuStock_computeExcessShortFall_Click(this, e);
        }


        

        







    }
    }
