namespace Assignment_Phase_1
{
    partial class frmCatalogue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCatalogue));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.staffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginLogOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registerNewStaffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyStaffInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAndDisplayStaffInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteExistingStaffInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catalogueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewCatalogueInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyCatalogueInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCatalogueInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAndDisplayCatalogueInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewProductInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyProductInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteProductInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAndDisplayProductInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAndDisplayOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteOrderInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_AddSupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_ModifySupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_DeleteSupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierMenu_SearchSupplier = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStock_EditProductQty = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStock_SearchProductQty = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStock_computeExcessShortFall = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlCheckIfCatToAddExist = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblCheckCatNameToAdd = new System.Windows.Forms.Label();
            this.tbxCheckCatNameToAdd = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.btnExitCheckCatName = new System.Windows.Forms.Button();
            this.btnCheckCatExist = new System.Windows.Forms.Button();
            this.lblEnterCatName = new System.Windows.Forms.Label();
            this.pnlEnterCatDetail = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lblCatNameToAdd = new System.Windows.Forms.Label();
            this.tbxEnterCatDescNameToAdd = new System.Windows.Forms.TextBox();
            this.lblEnterCatDescToAdd = new System.Windows.Forms.Label();
            this.lblEnterCatNameToAdd = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.btnExitEnterCatDetail = new System.Windows.Forms.Button();
            this.btnConfirmCatDetail = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.pnlCheckIfCatToModifyExist = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tbxCheckCatNameToModify = new System.Windows.Forms.TextBox();
            this.lblCheckCatNameToModify = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.btnCancelCatToModifyExist = new System.Windows.Forms.Button();
            this.btnCheckCatToModifyExist = new System.Windows.Forms.Button();
            this.lblEnterCatName2 = new System.Windows.Forms.Label();
            this.pnlModifyCatDetail = new System.Windows.Forms.Panel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lblNameOfCategoryToModify = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tbxEnterCatDescNameToModify = new System.Windows.Forms.TextBox();
            this.lblEnterCatDescToModify = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnUpdateCatDetail = new System.Windows.Forms.Button();
            this.btnCancelUpdateDetail = new System.Windows.Forms.Button();
            this.pnlDetailOfCategoryToDelete = new System.Windows.Forms.Panel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.lblNameOfCategoryToDelete = new System.Windows.Forms.Label();
            this.lblDesOfCategoryToDelete = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnCategoryToDelete = new System.Windows.Forms.Button();
            this.pnlCheckIfProToModifyExist = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbxCheckProNameToModify = new System.Windows.Forms.TextBox();
            this.tbxCheckModelNumToModify = new System.Windows.Forms.TextBox();
            this.lblCheckProNameToModify = new System.Windows.Forms.Label();
            this.lblCheckModelNumToModify = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnCancelProToModifyExist = new System.Windows.Forms.Button();
            this.btnCheckProToModifyExist = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlCheckIfProToDeleteExist = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCancelProToDeleteExist = new System.Windows.Forms.Button();
            this.btnCheckProToDeleteExist = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbxCheckProNameToDelete = new System.Windows.Forms.TextBox();
            this.tbxCheckModelNumToDelete = new System.Windows.Forms.TextBox();
            this.lblCheckProNameToDelete = new System.Windows.Forms.Label();
            this.lblCheckModelNumToDelete = new System.Windows.Forms.Label();
            this.pnlCheckIfProToAddExist = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblCheckProNameToAdd = new System.Windows.Forms.Label();
            this.lblCheckModelNumToAdd = new System.Windows.Forms.Label();
            this.tbxCheckModelNumToAdd = new System.Windows.Forms.TextBox();
            this.tbxCheckProNameToAdd = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnCancelProToAddExist = new System.Windows.Forms.Button();
            this.btnCheckProToAddExist = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCheckCatToDeleteExist = new System.Windows.Forms.Button();
            this.btnCancelCatToDeleteExist = new System.Windows.Forms.Button();
            this.lblEnterCatName3 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlCheckIfCatToDeleteExist = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblCheckCatNameToDelete = new System.Windows.Forms.Label();
            this.tbxCheckCatNameToDelete = new System.Windows.Forms.TextBox();
            this.pnlAddProduct = new System.Windows.Forms.Panel();
            this.btnCancelAddProToCategory = new System.Windows.Forms.Button();
            this.btnAddProToCategory = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.lblProModelNumToBeAdded = new System.Windows.Forms.Label();
            this.lblProNameToBeAdded = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.cbxProCategoryToBeAdded = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbxProRetailPriceToBeAdded = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tbxProMSRPToBeAdded = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label24 = new System.Windows.Forms.Label();
            this.pnlModifyProduct = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.btnModifyProduct = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.lblProModelNumToModify = new System.Windows.Forms.Label();
            this.lblProNameToModify = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.label40 = new System.Windows.Forms.Label();
            this.pnlDeleteProduct = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.btnDeleteProduct = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.label51 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.lblProDescToDelete = new System.Windows.Forms.TextBox();
            this.lblProCatToDelete = new System.Windows.Forms.Label();
            this.lblProRetailPriceToDelete = new System.Windows.Forms.Label();
            this.lblProMSRPToDelete = new System.Windows.Forms.Label();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.lblProModelNumToDelete = new System.Windows.Forms.Label();
            this.lblProNameToDelete = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.pnlSearchAndDisplayCatInfo = new System.Windows.Forms.Panel();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.tbxDisplayCatDescription = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.lblDisplayCategoryName = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.tbxSearchCategoryName = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.pnlSearchAndDisplayProInfo = new System.Windows.Forms.Panel();
            this.tbxSearchProModelNum = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.tbxSearchProductName = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnDisplayProInfo = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.lblDisplayCategory = new System.Windows.Forms.Label();
            this.lblDisplayRetailPrice = new System.Windows.Forms.Label();
            this.lblDisplayMSRP = new System.Windows.Forms.Label();
            this.lblDisplayProModelNum = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.tbxDisplayProDes = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lblDisplayProName = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.pnlCheckIfCatToAddExist.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.pnlEnterCatDetail.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.pnlCheckIfCatToModifyExist.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.pnlModifyCatDetail.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.pnlDetailOfCategoryToDelete.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.pnlCheckIfProToModifyExist.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pnlCheckIfProToDeleteExist.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.pnlCheckIfProToAddExist.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.pnlCheckIfCatToDeleteExist.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.pnlAddProduct.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.pnlModifyProduct.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.pnlDeleteProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            this.pnlSearchAndDisplayCatInfo.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            this.pnlSearchAndDisplayProInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.groupBox14.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.staffToolStripMenuItem,
            this.catalogueToolStripMenuItem,
            this.productToolStripMenuItem,
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem7});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(584, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // staffToolStripMenuItem
            // 
            this.staffToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginLogOutToolStripMenuItem,
            this.registerNewStaffToolStripMenuItem,
            this.modifyStaffInformationToolStripMenuItem,
            this.searchAndDisplayStaffInformationToolStripMenuItem,
            this.deleteExistingStaffInformationToolStripMenuItem});
            this.staffToolStripMenuItem.Name = "staffToolStripMenuItem";
            this.staffToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.staffToolStripMenuItem.Text = "Staff";
            // 
            // loginLogOutToolStripMenuItem
            // 
            this.loginLogOutToolStripMenuItem.Name = "loginLogOutToolStripMenuItem";
            this.loginLogOutToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.loginLogOutToolStripMenuItem.Text = "Login/Logout";
            this.loginLogOutToolStripMenuItem.Click += new System.EventHandler(this.loginLogOutToolStripMenuItem_Click);
            // 
            // registerNewStaffToolStripMenuItem
            // 
            this.registerNewStaffToolStripMenuItem.Name = "registerNewStaffToolStripMenuItem";
            this.registerNewStaffToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.registerNewStaffToolStripMenuItem.Text = "Register New Staff";
            this.registerNewStaffToolStripMenuItem.Click += new System.EventHandler(this.registerNewStaffToolStripMenuItem_Click);
            // 
            // modifyStaffInformationToolStripMenuItem
            // 
            this.modifyStaffInformationToolStripMenuItem.Name = "modifyStaffInformationToolStripMenuItem";
            this.modifyStaffInformationToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.modifyStaffInformationToolStripMenuItem.Text = "Modify Staff Information";
            this.modifyStaffInformationToolStripMenuItem.Click += new System.EventHandler(this.modifyStaffInformationToolStripMenuItem_Click);
            // 
            // searchAndDisplayStaffInformationToolStripMenuItem
            // 
            this.searchAndDisplayStaffInformationToolStripMenuItem.Name = "searchAndDisplayStaffInformationToolStripMenuItem";
            this.searchAndDisplayStaffInformationToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.searchAndDisplayStaffInformationToolStripMenuItem.Text = "Search and Display Staff Information";
            this.searchAndDisplayStaffInformationToolStripMenuItem.Click += new System.EventHandler(this.searchAndDisplayStaffInformationToolStripMenuItem_Click);
            // 
            // deleteExistingStaffInformationToolStripMenuItem
            // 
            this.deleteExistingStaffInformationToolStripMenuItem.Name = "deleteExistingStaffInformationToolStripMenuItem";
            this.deleteExistingStaffInformationToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.deleteExistingStaffInformationToolStripMenuItem.Text = "Delete Existing Staff Information";
            this.deleteExistingStaffInformationToolStripMenuItem.Click += new System.EventHandler(this.deleteExistingStaffInformationToolStripMenuItem_Click);
            // 
            // catalogueToolStripMenuItem
            // 
            this.catalogueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewCatalogueInfoToolStripMenuItem,
            this.modifyCatalogueInfoToolStripMenuItem,
            this.deleteCatalogueInfoToolStripMenuItem,
            this.searchAndDisplayCatalogueInfoToolStripMenuItem});
            this.catalogueToolStripMenuItem.Name = "catalogueToolStripMenuItem";
            this.catalogueToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.catalogueToolStripMenuItem.Text = "Category";
            // 
            // addNewCatalogueInfoToolStripMenuItem
            // 
            this.addNewCatalogueInfoToolStripMenuItem.Name = "addNewCatalogueInfoToolStripMenuItem";
            this.addNewCatalogueInfoToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.addNewCatalogueInfoToolStripMenuItem.Text = "Add New Category Info";
            this.addNewCatalogueInfoToolStripMenuItem.Click += new System.EventHandler(this.addNewCatalogueInfoToolStripMenuItem_Click);
            // 
            // modifyCatalogueInfoToolStripMenuItem
            // 
            this.modifyCatalogueInfoToolStripMenuItem.Name = "modifyCatalogueInfoToolStripMenuItem";
            this.modifyCatalogueInfoToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.modifyCatalogueInfoToolStripMenuItem.Text = "Modify Category Info";
            this.modifyCatalogueInfoToolStripMenuItem.Click += new System.EventHandler(this.modifyCatalogueInfoToolStripMenuItem_Click);
            // 
            // deleteCatalogueInfoToolStripMenuItem
            // 
            this.deleteCatalogueInfoToolStripMenuItem.Name = "deleteCatalogueInfoToolStripMenuItem";
            this.deleteCatalogueInfoToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.deleteCatalogueInfoToolStripMenuItem.Text = "Delete Category Info";
            this.deleteCatalogueInfoToolStripMenuItem.Click += new System.EventHandler(this.deleteCatalogueInfoToolStripMenuItem_Click);
            // 
            // searchAndDisplayCatalogueInfoToolStripMenuItem
            // 
            this.searchAndDisplayCatalogueInfoToolStripMenuItem.Name = "searchAndDisplayCatalogueInfoToolStripMenuItem";
            this.searchAndDisplayCatalogueInfoToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.searchAndDisplayCatalogueInfoToolStripMenuItem.Text = "Search and Display Category Info";
            this.searchAndDisplayCatalogueInfoToolStripMenuItem.Click += new System.EventHandler(this.searchAndDisplayCatalogueInfoToolStripMenuItem_Click);
            // 
            // productToolStripMenuItem
            // 
            this.productToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewProductInfoToolStripMenuItem,
            this.modifyProductInfoToolStripMenuItem,
            this.deleteProductInfoToolStripMenuItem,
            this.searchAndDisplayProductInfoToolStripMenuItem});
            this.productToolStripMenuItem.Name = "productToolStripMenuItem";
            this.productToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.productToolStripMenuItem.Text = "Product";
            // 
            // addNewProductInfoToolStripMenuItem
            // 
            this.addNewProductInfoToolStripMenuItem.Name = "addNewProductInfoToolStripMenuItem";
            this.addNewProductInfoToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.addNewProductInfoToolStripMenuItem.Text = "Add New Product Info";
            this.addNewProductInfoToolStripMenuItem.Click += new System.EventHandler(this.addNewProductInfoToolStripMenuItem_Click);
            // 
            // modifyProductInfoToolStripMenuItem
            // 
            this.modifyProductInfoToolStripMenuItem.Name = "modifyProductInfoToolStripMenuItem";
            this.modifyProductInfoToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.modifyProductInfoToolStripMenuItem.Text = "Modify Product Info";
            this.modifyProductInfoToolStripMenuItem.Click += new System.EventHandler(this.modifyProductInfoToolStripMenuItem_Click);
            // 
            // deleteProductInfoToolStripMenuItem
            // 
            this.deleteProductInfoToolStripMenuItem.Name = "deleteProductInfoToolStripMenuItem";
            this.deleteProductInfoToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.deleteProductInfoToolStripMenuItem.Text = "Delete Product Info";
            this.deleteProductInfoToolStripMenuItem.Click += new System.EventHandler(this.deleteProductInfoToolStripMenuItem_Click);
            // 
            // searchAndDisplayProductInfoToolStripMenuItem
            // 
            this.searchAndDisplayProductInfoToolStripMenuItem.Name = "searchAndDisplayProductInfoToolStripMenuItem";
            this.searchAndDisplayProductInfoToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.searchAndDisplayProductInfoToolStripMenuItem.Text = "Search and Display Product Info";
            this.searchAndDisplayProductInfoToolStripMenuItem.Click += new System.EventHandler(this.searchAndDisplayProductInfoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewOrderToolStripMenuItem,
            this.modifyOrderToolStripMenuItem,
            this.searchAndDisplayOrderToolStripMenuItem,
            this.deleteOrderInformationToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(49, 20);
            this.toolStripMenuItem1.Text = "Order";
            // 
            // addNewOrderToolStripMenuItem
            // 
            this.addNewOrderToolStripMenuItem.Name = "addNewOrderToolStripMenuItem";
            this.addNewOrderToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.addNewOrderToolStripMenuItem.Text = "Add New Order Information";
            this.addNewOrderToolStripMenuItem.Click += new System.EventHandler(this.addNewOrderToolStripMenuItem_Click);
            // 
            // modifyOrderToolStripMenuItem
            // 
            this.modifyOrderToolStripMenuItem.Name = "modifyOrderToolStripMenuItem";
            this.modifyOrderToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.modifyOrderToolStripMenuItem.Text = "Modify Existing Order Information";
            this.modifyOrderToolStripMenuItem.Click += new System.EventHandler(this.modifyOrderToolStripMenuItem_Click);
            // 
            // searchAndDisplayOrderToolStripMenuItem
            // 
            this.searchAndDisplayOrderToolStripMenuItem.Name = "searchAndDisplayOrderToolStripMenuItem";
            this.searchAndDisplayOrderToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.searchAndDisplayOrderToolStripMenuItem.Text = "Search and Display Order Information";
            this.searchAndDisplayOrderToolStripMenuItem.Click += new System.EventHandler(this.searchAndDisplayOrderToolStripMenuItem_Click);
            // 
            // deleteOrderInformationToolStripMenuItem
            // 
            this.deleteOrderInformationToolStripMenuItem.Name = "deleteOrderInformationToolStripMenuItem";
            this.deleteOrderInformationToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.deleteOrderInformationToolStripMenuItem.Text = "Delete Order Information";
            this.deleteOrderInformationToolStripMenuItem.Click += new System.EventHandler(this.deleteOrderInformationToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SupplierMenu_AddSupplier,
            this.SupplierMenu_ModifySupplier,
            this.SupplierMenu_DeleteSupplier,
            this.SupplierMenu_SearchSupplier});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(69, 20);
            this.toolStripMenuItem2.Text = "Inventory";
            // 
            // SupplierMenu_AddSupplier
            // 
            this.SupplierMenu_AddSupplier.Name = "SupplierMenu_AddSupplier";
            this.SupplierMenu_AddSupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_AddSupplier.Text = "Add Supplier";
            this.SupplierMenu_AddSupplier.Click += new System.EventHandler(this.SupplierMenu_AddSupplier_Click);
            // 
            // SupplierMenu_ModifySupplier
            // 
            this.SupplierMenu_ModifySupplier.Name = "SupplierMenu_ModifySupplier";
            this.SupplierMenu_ModifySupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_ModifySupplier.Text = "Modify Supplier";
            this.SupplierMenu_ModifySupplier.Click += new System.EventHandler(this.SupplierMenu_ModifySupplier_Click);
            // 
            // SupplierMenu_DeleteSupplier
            // 
            this.SupplierMenu_DeleteSupplier.Name = "SupplierMenu_DeleteSupplier";
            this.SupplierMenu_DeleteSupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_DeleteSupplier.Text = "Delete Supplier";
            this.SupplierMenu_DeleteSupplier.Click += new System.EventHandler(this.SupplierMenu_DeleteSupplier_Click);
            // 
            // SupplierMenu_SearchSupplier
            // 
            this.SupplierMenu_SearchSupplier.Name = "SupplierMenu_SearchSupplier";
            this.SupplierMenu_SearchSupplier.Size = new System.Drawing.Size(158, 22);
            this.SupplierMenu_SearchSupplier.Text = "Search Supplier";
            this.SupplierMenu_SearchSupplier.Click += new System.EventHandler(this.SupplierMenu_SearchSupplier_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuStock_EditProductQty,
            this.menuStock_SearchProductQty,
            this.menuStock_computeExcessShortFall});
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(48, 20);
            this.toolStripMenuItem7.Text = "Stock";
            // 
            // menuStock_EditProductQty
            // 
            this.menuStock_EditProductQty.Name = "menuStock_EditProductQty";
            this.menuStock_EditProductQty.Size = new System.Drawing.Size(209, 22);
            this.menuStock_EditProductQty.Text = "Edit Product Qty";
            this.menuStock_EditProductQty.Click += new System.EventHandler(this.menuStock_EditProductQty_Click);
            // 
            // menuStock_SearchProductQty
            // 
            this.menuStock_SearchProductQty.Name = "menuStock_SearchProductQty";
            this.menuStock_SearchProductQty.Size = new System.Drawing.Size(209, 22);
            this.menuStock_SearchProductQty.Text = "Search Product Qty";
            this.menuStock_SearchProductQty.Click += new System.EventHandler(this.menuStock_SearchProductQty_Click);
            // 
            // menuStock_computeExcessShortFall
            // 
            this.menuStock_computeExcessShortFall.Name = "menuStock_computeExcessShortFall";
            this.menuStock_computeExcessShortFall.Size = new System.Drawing.Size(209, 22);
            this.menuStock_computeExcessShortFall.Text = "Compute Excess/Shortfall";
            this.menuStock_computeExcessShortFall.Click += new System.EventHandler(this.menuStock_computeExcessShortFall_Click);
            // 
            // pnlCheckIfCatToAddExist
            // 
            this.pnlCheckIfCatToAddExist.Controls.Add(this.groupBox6);
            this.pnlCheckIfCatToAddExist.Controls.Add(this.label9);
            this.pnlCheckIfCatToAddExist.Controls.Add(this.pictureBox6);
            this.pnlCheckIfCatToAddExist.Controls.Add(this.btnExitCheckCatName);
            this.pnlCheckIfCatToAddExist.Controls.Add(this.btnCheckCatExist);
            this.pnlCheckIfCatToAddExist.Controls.Add(this.lblEnterCatName);
            this.pnlCheckIfCatToAddExist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCheckIfCatToAddExist.Location = new System.Drawing.Point(0, 0);
            this.pnlCheckIfCatToAddExist.Name = "pnlCheckIfCatToAddExist";
            this.pnlCheckIfCatToAddExist.Size = new System.Drawing.Size(584, 444);
            this.pnlCheckIfCatToAddExist.TabIndex = 1;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lblCheckCatNameToAdd);
            this.groupBox6.Controls.Add(this.tbxCheckCatNameToAdd);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(50, 164);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(400, 200);
            this.groupBox6.TabIndex = 23;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Search Category";
            // 
            // lblCheckCatNameToAdd
            // 
            this.lblCheckCatNameToAdd.AutoSize = true;
            this.lblCheckCatNameToAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckCatNameToAdd.Location = new System.Drawing.Point(27, 88);
            this.lblCheckCatNameToAdd.Name = "lblCheckCatNameToAdd";
            this.lblCheckCatNameToAdd.Size = new System.Drawing.Size(106, 16);
            this.lblCheckCatNameToAdd.TabIndex = 4;
            this.lblCheckCatNameToAdd.Text = "Category Name:";
            // 
            // tbxCheckCatNameToAdd
            // 
            this.tbxCheckCatNameToAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxCheckCatNameToAdd.Location = new System.Drawing.Point(153, 85);
            this.tbxCheckCatNameToAdd.Name = "tbxCheckCatNameToAdd";
            this.tbxCheckCatNameToAdd.Size = new System.Drawing.Size(190, 22);
            this.tbxCheckCatNameToAdd.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(45, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 25);
            this.label9.TabIndex = 22;
            this.label9.Text = "Add Category";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(453, 34);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(100, 81);
            this.pictureBox6.TabIndex = 21;
            this.pictureBox6.TabStop = false;
            // 
            // btnExitCheckCatName
            // 
            this.btnExitCheckCatName.Location = new System.Drawing.Point(477, 386);
            this.btnExitCheckCatName.Name = "btnExitCheckCatName";
            this.btnExitCheckCatName.Size = new System.Drawing.Size(75, 23);
            this.btnExitCheckCatName.TabIndex = 4;
            this.btnExitCheckCatName.Text = "Cancel";
            this.btnExitCheckCatName.UseVisualStyleBackColor = true;
            // 
            // btnCheckCatExist
            // 
            this.btnCheckCatExist.Location = new System.Drawing.Point(375, 386);
            this.btnCheckCatExist.Name = "btnCheckCatExist";
            this.btnCheckCatExist.Size = new System.Drawing.Size(75, 23);
            this.btnCheckCatExist.TabIndex = 3;
            this.btnCheckCatExist.Text = "Check";
            this.btnCheckCatExist.UseVisualStyleBackColor = true;
            this.btnCheckCatExist.Click += new System.EventHandler(this.btnCheckCatExist_Click);
            // 
            // lblEnterCatName
            // 
            this.lblEnterCatName.AutoSize = true;
            this.lblEnterCatName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnterCatName.Location = new System.Drawing.Point(47, 111);
            this.lblEnterCatName.Name = "lblEnterCatName";
            this.lblEnterCatName.Size = new System.Drawing.Size(231, 16);
            this.lblEnterCatName.TabIndex = 0;
            this.lblEnterCatName.Text = "Please enter the following information:";
            // 
            // pnlEnterCatDetail
            // 
            this.pnlEnterCatDetail.Controls.Add(this.groupBox7);
            this.pnlEnterCatDetail.Controls.Add(this.label10);
            this.pnlEnterCatDetail.Controls.Add(this.pictureBox7);
            this.pnlEnterCatDetail.Controls.Add(this.btnExitEnterCatDetail);
            this.pnlEnterCatDetail.Controls.Add(this.btnConfirmCatDetail);
            this.pnlEnterCatDetail.Controls.Add(this.label12);
            this.pnlEnterCatDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlEnterCatDetail.Location = new System.Drawing.Point(0, 0);
            this.pnlEnterCatDetail.Name = "pnlEnterCatDetail";
            this.pnlEnterCatDetail.Size = new System.Drawing.Size(584, 444);
            this.pnlEnterCatDetail.TabIndex = 5;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lblCatNameToAdd);
            this.groupBox7.Controls.Add(this.tbxEnterCatDescNameToAdd);
            this.groupBox7.Controls.Add(this.lblEnterCatDescToAdd);
            this.groupBox7.Controls.Add(this.lblEnterCatNameToAdd);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(50, 164);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(400, 200);
            this.groupBox7.TabIndex = 21;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Category\'s Details";
            // 
            // lblCatNameToAdd
            // 
            this.lblCatNameToAdd.AutoSize = true;
            this.lblCatNameToAdd.Location = new System.Drawing.Point(140, 43);
            this.lblCatNameToAdd.Name = "lblCatNameToAdd";
            this.lblCatNameToAdd.Size = new System.Drawing.Size(0, 16);
            this.lblCatNameToAdd.TabIndex = 9;
            // 
            // tbxEnterCatDescNameToAdd
            // 
            this.tbxEnterCatDescNameToAdd.Location = new System.Drawing.Point(140, 93);
            this.tbxEnterCatDescNameToAdd.Multiline = true;
            this.tbxEnterCatDescNameToAdd.Name = "tbxEnterCatDescNameToAdd";
            this.tbxEnterCatDescNameToAdd.Size = new System.Drawing.Size(190, 95);
            this.tbxEnterCatDescNameToAdd.TabIndex = 8;
            // 
            // lblEnterCatDescToAdd
            // 
            this.lblEnterCatDescToAdd.AutoSize = true;
            this.lblEnterCatDescToAdd.Location = new System.Drawing.Point(41, 93);
            this.lblEnterCatDescToAdd.Name = "lblEnterCatDescToAdd";
            this.lblEnterCatDescToAdd.Size = new System.Drawing.Size(79, 16);
            this.lblEnterCatDescToAdd.TabIndex = 7;
            this.lblEnterCatDescToAdd.Text = "Description:";
            // 
            // lblEnterCatNameToAdd
            // 
            this.lblEnterCatNameToAdd.AutoSize = true;
            this.lblEnterCatNameToAdd.Location = new System.Drawing.Point(41, 43);
            this.lblEnterCatNameToAdd.Name = "lblEnterCatNameToAdd";
            this.lblEnterCatNameToAdd.Size = new System.Drawing.Size(48, 16);
            this.lblEnterCatNameToAdd.TabIndex = 5;
            this.lblEnterCatNameToAdd.Text = "Name:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(45, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(143, 25);
            this.label10.TabIndex = 20;
            this.label10.Text = "Add Category";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(453, 34);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(100, 81);
            this.pictureBox7.TabIndex = 19;
            this.pictureBox7.TabStop = false;
            // 
            // btnExitEnterCatDetail
            // 
            this.btnExitEnterCatDetail.Location = new System.Drawing.Point(477, 386);
            this.btnExitEnterCatDetail.Name = "btnExitEnterCatDetail";
            this.btnExitEnterCatDetail.Size = new System.Drawing.Size(75, 23);
            this.btnExitEnterCatDetail.TabIndex = 6;
            this.btnExitEnterCatDetail.Text = "Cancel";
            this.btnExitEnterCatDetail.UseVisualStyleBackColor = true;
            // 
            // btnConfirmCatDetail
            // 
            this.btnConfirmCatDetail.Location = new System.Drawing.Point(375, 386);
            this.btnConfirmCatDetail.Name = "btnConfirmCatDetail";
            this.btnConfirmCatDetail.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmCatDetail.TabIndex = 5;
            this.btnConfirmCatDetail.Text = "Add";
            this.btnConfirmCatDetail.UseVisualStyleBackColor = true;
            this.btnConfirmCatDetail.Click += new System.EventHandler(this.btnConfirmCatDetail_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(47, 111);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(231, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "Please enter the following information:";
            // 
            // pnlCheckIfCatToModifyExist
            // 
            this.pnlCheckIfCatToModifyExist.Controls.Add(this.groupBox5);
            this.pnlCheckIfCatToModifyExist.Controls.Add(this.label8);
            this.pnlCheckIfCatToModifyExist.Controls.Add(this.pictureBox5);
            this.pnlCheckIfCatToModifyExist.Controls.Add(this.btnCancelCatToModifyExist);
            this.pnlCheckIfCatToModifyExist.Controls.Add(this.btnCheckCatToModifyExist);
            this.pnlCheckIfCatToModifyExist.Controls.Add(this.lblEnterCatName2);
            this.pnlCheckIfCatToModifyExist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCheckIfCatToModifyExist.Location = new System.Drawing.Point(0, 0);
            this.pnlCheckIfCatToModifyExist.Name = "pnlCheckIfCatToModifyExist";
            this.pnlCheckIfCatToModifyExist.Size = new System.Drawing.Size(584, 444);
            this.pnlCheckIfCatToModifyExist.TabIndex = 7;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tbxCheckCatNameToModify);
            this.groupBox5.Controls.Add(this.lblCheckCatNameToModify);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(50, 164);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(400, 200);
            this.groupBox5.TabIndex = 21;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Search Category";
            // 
            // tbxCheckCatNameToModify
            // 
            this.tbxCheckCatNameToModify.Location = new System.Drawing.Point(153, 85);
            this.tbxCheckCatNameToModify.Name = "tbxCheckCatNameToModify";
            this.tbxCheckCatNameToModify.Size = new System.Drawing.Size(190, 22);
            this.tbxCheckCatNameToModify.TabIndex = 10;
            // 
            // lblCheckCatNameToModify
            // 
            this.lblCheckCatNameToModify.AutoSize = true;
            this.lblCheckCatNameToModify.Location = new System.Drawing.Point(27, 88);
            this.lblCheckCatNameToModify.Name = "lblCheckCatNameToModify";
            this.lblCheckCatNameToModify.Size = new System.Drawing.Size(106, 16);
            this.lblCheckCatNameToModify.TabIndex = 9;
            this.lblCheckCatNameToModify.Text = "Category Name:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(45, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(169, 25);
            this.label8.TabIndex = 20;
            this.label8.Text = "Modify Category";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(453, 34);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(100, 81);
            this.pictureBox5.TabIndex = 19;
            this.pictureBox5.TabStop = false;
            // 
            // btnCancelCatToModifyExist
            // 
            this.btnCancelCatToModifyExist.Location = new System.Drawing.Point(477, 386);
            this.btnCancelCatToModifyExist.Name = "btnCancelCatToModifyExist";
            this.btnCancelCatToModifyExist.Size = new System.Drawing.Size(75, 23);
            this.btnCancelCatToModifyExist.TabIndex = 9;
            this.btnCancelCatToModifyExist.Text = "Cancel";
            this.btnCancelCatToModifyExist.UseVisualStyleBackColor = true;
            // 
            // btnCheckCatToModifyExist
            // 
            this.btnCheckCatToModifyExist.Location = new System.Drawing.Point(375, 386);
            this.btnCheckCatToModifyExist.Name = "btnCheckCatToModifyExist";
            this.btnCheckCatToModifyExist.Size = new System.Drawing.Size(75, 23);
            this.btnCheckCatToModifyExist.TabIndex = 8;
            this.btnCheckCatToModifyExist.Text = "Check";
            this.btnCheckCatToModifyExist.UseVisualStyleBackColor = true;
            this.btnCheckCatToModifyExist.Click += new System.EventHandler(this.btnCheckCatToModifyExist_Click);
            // 
            // lblEnterCatName2
            // 
            this.lblEnterCatName2.AutoSize = true;
            this.lblEnterCatName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnterCatName2.Location = new System.Drawing.Point(47, 111);
            this.lblEnterCatName2.Name = "lblEnterCatName2";
            this.lblEnterCatName2.Size = new System.Drawing.Size(231, 16);
            this.lblEnterCatName2.TabIndex = 8;
            this.lblEnterCatName2.Text = "Please enter the following information:";
            // 
            // pnlModifyCatDetail
            // 
            this.pnlModifyCatDetail.Controls.Add(this.groupBox8);
            this.pnlModifyCatDetail.Controls.Add(this.label11);
            this.pnlModifyCatDetail.Controls.Add(this.pictureBox8);
            this.pnlModifyCatDetail.Controls.Add(this.label13);
            this.pnlModifyCatDetail.Controls.Add(this.btnUpdateCatDetail);
            this.pnlModifyCatDetail.Controls.Add(this.btnCancelUpdateDetail);
            this.pnlModifyCatDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlModifyCatDetail.Location = new System.Drawing.Point(0, 0);
            this.pnlModifyCatDetail.Name = "pnlModifyCatDetail";
            this.pnlModifyCatDetail.Size = new System.Drawing.Size(584, 444);
            this.pnlModifyCatDetail.TabIndex = 11;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lblNameOfCategoryToModify);
            this.groupBox8.Controls.Add(this.label14);
            this.groupBox8.Controls.Add(this.tbxEnterCatDescNameToModify);
            this.groupBox8.Controls.Add(this.lblEnterCatDescToModify);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(50, 164);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(400, 200);
            this.groupBox8.TabIndex = 26;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Category\'s Details";
            // 
            // lblNameOfCategoryToModify
            // 
            this.lblNameOfCategoryToModify.AutoSize = true;
            this.lblNameOfCategoryToModify.Location = new System.Drawing.Point(137, 43);
            this.lblNameOfCategoryToModify.Name = "lblNameOfCategoryToModify";
            this.lblNameOfCategoryToModify.Size = new System.Drawing.Size(0, 16);
            this.lblNameOfCategoryToModify.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(41, 43);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 16);
            this.label14.TabIndex = 12;
            this.label14.Text = "Name:";
            // 
            // tbxEnterCatDescNameToModify
            // 
            this.tbxEnterCatDescNameToModify.Location = new System.Drawing.Point(140, 93);
            this.tbxEnterCatDescNameToModify.Multiline = true;
            this.tbxEnterCatDescNameToModify.Name = "tbxEnterCatDescNameToModify";
            this.tbxEnterCatDescNameToModify.Size = new System.Drawing.Size(190, 95);
            this.tbxEnterCatDescNameToModify.TabIndex = 11;
            // 
            // lblEnterCatDescToModify
            // 
            this.lblEnterCatDescToModify.AutoSize = true;
            this.lblEnterCatDescToModify.Location = new System.Drawing.Point(41, 93);
            this.lblEnterCatDescToModify.Name = "lblEnterCatDescToModify";
            this.lblEnterCatDescToModify.Size = new System.Drawing.Size(79, 16);
            this.lblEnterCatDescToModify.TabIndex = 10;
            this.lblEnterCatDescToModify.Text = "Description:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(45, 43);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(169, 25);
            this.label11.TabIndex = 25;
            this.label11.Text = "Modify Category";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(453, 34);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(100, 81);
            this.pictureBox8.TabIndex = 24;
            this.pictureBox8.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(47, 111);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(231, 16);
            this.label13.TabIndex = 23;
            this.label13.Text = "Please enter the following information:";
            // 
            // btnUpdateCatDetail
            // 
            this.btnUpdateCatDetail.Location = new System.Drawing.Point(375, 386);
            this.btnUpdateCatDetail.Name = "btnUpdateCatDetail";
            this.btnUpdateCatDetail.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateCatDetail.TabIndex = 10;
            this.btnUpdateCatDetail.Text = "Update";
            this.btnUpdateCatDetail.UseVisualStyleBackColor = true;
            this.btnUpdateCatDetail.Click += new System.EventHandler(this.btnUpdateCatDetail_Click_1);
            // 
            // btnCancelUpdateDetail
            // 
            this.btnCancelUpdateDetail.Location = new System.Drawing.Point(477, 386);
            this.btnCancelUpdateDetail.Name = "btnCancelUpdateDetail";
            this.btnCancelUpdateDetail.Size = new System.Drawing.Size(75, 23);
            this.btnCancelUpdateDetail.TabIndex = 8;
            this.btnCancelUpdateDetail.Text = "Cancel";
            this.btnCancelUpdateDetail.UseVisualStyleBackColor = true;
            // 
            // pnlDetailOfCategoryToDelete
            // 
            this.pnlDetailOfCategoryToDelete.Controls.Add(this.groupBox9);
            this.pnlDetailOfCategoryToDelete.Controls.Add(this.label15);
            this.pnlDetailOfCategoryToDelete.Controls.Add(this.pictureBox9);
            this.pnlDetailOfCategoryToDelete.Controls.Add(this.label16);
            this.pnlDetailOfCategoryToDelete.Controls.Add(this.button1);
            this.pnlDetailOfCategoryToDelete.Controls.Add(this.btnCategoryToDelete);
            this.pnlDetailOfCategoryToDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDetailOfCategoryToDelete.Location = new System.Drawing.Point(0, 0);
            this.pnlDetailOfCategoryToDelete.Name = "pnlDetailOfCategoryToDelete";
            this.pnlDetailOfCategoryToDelete.Size = new System.Drawing.Size(584, 444);
            this.pnlDetailOfCategoryToDelete.TabIndex = 12;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.textBox5);
            this.groupBox9.Controls.Add(this.lblNameOfCategoryToDelete);
            this.groupBox9.Controls.Add(this.lblDesOfCategoryToDelete);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(50, 164);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(400, 200);
            this.groupBox9.TabIndex = 29;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Category\'s Details";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(138, 92);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(192, 96);
            this.textBox5.TabIndex = 9;
            this.textBox5.Text = "MP4 Player is a portable media player that support MPEG-4";
            // 
            // lblNameOfCategoryToDelete
            // 
            this.lblNameOfCategoryToDelete.AutoSize = true;
            this.lblNameOfCategoryToDelete.Location = new System.Drawing.Point(140, 43);
            this.lblNameOfCategoryToDelete.Name = "lblNameOfCategoryToDelete";
            this.lblNameOfCategoryToDelete.Size = new System.Drawing.Size(35, 16);
            this.lblNameOfCategoryToDelete.TabIndex = 8;
            this.lblNameOfCategoryToDelete.Text = "MP4";
            // 
            // lblDesOfCategoryToDelete
            // 
            this.lblDesOfCategoryToDelete.AutoSize = true;
            this.lblDesOfCategoryToDelete.Location = new System.Drawing.Point(41, 93);
            this.lblDesOfCategoryToDelete.Name = "lblDesOfCategoryToDelete";
            this.lblDesOfCategoryToDelete.Size = new System.Drawing.Size(79, 16);
            this.lblDesOfCategoryToDelete.TabIndex = 7;
            this.lblDesOfCategoryToDelete.Text = "Description:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(41, 43);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 16);
            this.label18.TabIndex = 5;
            this.label18.Text = "Name:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(45, 43);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(167, 25);
            this.label15.TabIndex = 28;
            this.label15.Text = "Delete Category";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(453, 34);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(100, 81);
            this.pictureBox9.TabIndex = 27;
            this.pictureBox9.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(47, 111);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(231, 16);
            this.label16.TabIndex = 26;
            this.label16.Text = "Please enter the following information:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(477, 386);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnCategoryToDelete
            // 
            this.btnCategoryToDelete.Location = new System.Drawing.Point(375, 386);
            this.btnCategoryToDelete.Name = "btnCategoryToDelete";
            this.btnCategoryToDelete.Size = new System.Drawing.Size(75, 23);
            this.btnCategoryToDelete.TabIndex = 5;
            this.btnCategoryToDelete.Text = "Delete";
            this.btnCategoryToDelete.UseVisualStyleBackColor = true;
            this.btnCategoryToDelete.Click += new System.EventHandler(this.btnCategoryToDelete_Click);
            // 
            // pnlCheckIfProToModifyExist
            // 
            this.pnlCheckIfProToModifyExist.Controls.Add(this.groupBox2);
            this.pnlCheckIfProToModifyExist.Controls.Add(this.label3);
            this.pnlCheckIfProToModifyExist.Controls.Add(this.pictureBox2);
            this.pnlCheckIfProToModifyExist.Controls.Add(this.btnCancelProToModifyExist);
            this.pnlCheckIfProToModifyExist.Controls.Add(this.btnCheckProToModifyExist);
            this.pnlCheckIfProToModifyExist.Controls.Add(this.label4);
            this.pnlCheckIfProToModifyExist.Location = new System.Drawing.Point(0, 0);
            this.pnlCheckIfProToModifyExist.Name = "pnlCheckIfProToModifyExist";
            this.pnlCheckIfProToModifyExist.Size = new System.Drawing.Size(584, 444);
            this.pnlCheckIfProToModifyExist.TabIndex = 16;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbxCheckProNameToModify);
            this.groupBox2.Controls.Add(this.tbxCheckModelNumToModify);
            this.groupBox2.Controls.Add(this.lblCheckProNameToModify);
            this.groupBox2.Controls.Add(this.lblCheckModelNumToModify);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(50, 164);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(400, 200);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search Product";
            // 
            // tbxCheckProNameToModify
            // 
            this.tbxCheckProNameToModify.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxCheckProNameToModify.Location = new System.Drawing.Point(138, 56);
            this.tbxCheckProNameToModify.Name = "tbxCheckProNameToModify";
            this.tbxCheckProNameToModify.Size = new System.Drawing.Size(190, 22);
            this.tbxCheckProNameToModify.TabIndex = 15;
            // 
            // tbxCheckModelNumToModify
            // 
            this.tbxCheckModelNumToModify.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxCheckModelNumToModify.Location = new System.Drawing.Point(138, 103);
            this.tbxCheckModelNumToModify.Name = "tbxCheckModelNumToModify";
            this.tbxCheckModelNumToModify.Size = new System.Drawing.Size(190, 22);
            this.tbxCheckModelNumToModify.TabIndex = 14;
            // 
            // lblCheckProNameToModify
            // 
            this.lblCheckProNameToModify.AutoSize = true;
            this.lblCheckProNameToModify.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckProNameToModify.Location = new System.Drawing.Point(27, 59);
            this.lblCheckProNameToModify.Name = "lblCheckProNameToModify";
            this.lblCheckProNameToModify.Size = new System.Drawing.Size(97, 16);
            this.lblCheckProNameToModify.TabIndex = 13;
            this.lblCheckProNameToModify.Text = "Product Name:";
            // 
            // lblCheckModelNumToModify
            // 
            this.lblCheckModelNumToModify.AutoSize = true;
            this.lblCheckModelNumToModify.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckModelNumToModify.Location = new System.Drawing.Point(27, 106);
            this.lblCheckModelNumToModify.Name = "lblCheckModelNumToModify";
            this.lblCheckModelNumToModify.Size = new System.Drawing.Size(100, 16);
            this.lblCheckModelNumToModify.TabIndex = 12;
            this.lblCheckModelNumToModify.Text = "Model Number:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(45, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 25);
            this.label3.TabIndex = 16;
            this.label3.Text = "Modify Product";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(453, 34);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 81);
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // btnCancelProToModifyExist
            // 
            this.btnCancelProToModifyExist.Location = new System.Drawing.Point(477, 386);
            this.btnCancelProToModifyExist.Name = "btnCancelProToModifyExist";
            this.btnCancelProToModifyExist.Size = new System.Drawing.Size(75, 23);
            this.btnCancelProToModifyExist.TabIndex = 13;
            this.btnCancelProToModifyExist.Text = "Cancel";
            this.btnCancelProToModifyExist.UseVisualStyleBackColor = true;
            // 
            // btnCheckProToModifyExist
            // 
            this.btnCheckProToModifyExist.Location = new System.Drawing.Point(375, 386);
            this.btnCheckProToModifyExist.Name = "btnCheckProToModifyExist";
            this.btnCheckProToModifyExist.Size = new System.Drawing.Size(75, 23);
            this.btnCheckProToModifyExist.TabIndex = 12;
            this.btnCheckProToModifyExist.Text = "Check";
            this.btnCheckProToModifyExist.UseVisualStyleBackColor = true;
            this.btnCheckProToModifyExist.Click += new System.EventHandler(this.btnCheckProToModifyExist_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(47, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(231, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Please enter the following information:";
            // 
            // pnlCheckIfProToDeleteExist
            // 
            this.pnlCheckIfProToDeleteExist.Controls.Add(this.label2);
            this.pnlCheckIfProToDeleteExist.Controls.Add(this.pictureBox1);
            this.pnlCheckIfProToDeleteExist.Controls.Add(this.btnCancelProToDeleteExist);
            this.pnlCheckIfProToDeleteExist.Controls.Add(this.btnCheckProToDeleteExist);
            this.pnlCheckIfProToDeleteExist.Controls.Add(this.label5);
            this.pnlCheckIfProToDeleteExist.Controls.Add(this.groupBox1);
            this.pnlCheckIfProToDeleteExist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCheckIfProToDeleteExist.Location = new System.Drawing.Point(0, 0);
            this.pnlCheckIfProToDeleteExist.Name = "pnlCheckIfProToDeleteExist";
            this.pnlCheckIfProToDeleteExist.Size = new System.Drawing.Size(584, 444);
            this.pnlCheckIfProToDeleteExist.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(47, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 25);
            this.label2.TabIndex = 15;
            this.label2.Text = "Delete Product";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(453, 34);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 81);
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // btnCancelProToDeleteExist
            // 
            this.btnCancelProToDeleteExist.Location = new System.Drawing.Point(477, 386);
            this.btnCancelProToDeleteExist.Name = "btnCancelProToDeleteExist";
            this.btnCancelProToDeleteExist.Size = new System.Drawing.Size(75, 23);
            this.btnCancelProToDeleteExist.TabIndex = 13;
            this.btnCancelProToDeleteExist.Text = "Cancel";
            this.btnCancelProToDeleteExist.UseVisualStyleBackColor = true;
            // 
            // btnCheckProToDeleteExist
            // 
            this.btnCheckProToDeleteExist.Location = new System.Drawing.Point(375, 386);
            this.btnCheckProToDeleteExist.Name = "btnCheckProToDeleteExist";
            this.btnCheckProToDeleteExist.Size = new System.Drawing.Size(75, 23);
            this.btnCheckProToDeleteExist.TabIndex = 12;
            this.btnCheckProToDeleteExist.Text = "Check";
            this.btnCheckProToDeleteExist.UseVisualStyleBackColor = true;
            this.btnCheckProToDeleteExist.Click += new System.EventHandler(this.btnCheckProToDeleteExist_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(47, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(231, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Please enter the following information:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbxCheckProNameToDelete);
            this.groupBox1.Controls.Add(this.tbxCheckModelNumToDelete);
            this.groupBox1.Controls.Add(this.lblCheckProNameToDelete);
            this.groupBox1.Controls.Add(this.lblCheckModelNumToDelete);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(50, 164);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 200);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Product";
            // 
            // tbxCheckProNameToDelete
            // 
            this.tbxCheckProNameToDelete.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tbxCheckProNameToDelete.Location = new System.Drawing.Point(138, 56);
            this.tbxCheckProNameToDelete.Name = "tbxCheckProNameToDelete";
            this.tbxCheckProNameToDelete.Size = new System.Drawing.Size(190, 22);
            this.tbxCheckProNameToDelete.TabIndex = 15;
            // 
            // tbxCheckModelNumToDelete
            // 
            this.tbxCheckModelNumToDelete.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tbxCheckModelNumToDelete.Location = new System.Drawing.Point(138, 103);
            this.tbxCheckModelNumToDelete.Name = "tbxCheckModelNumToDelete";
            this.tbxCheckModelNumToDelete.Size = new System.Drawing.Size(190, 22);
            this.tbxCheckModelNumToDelete.TabIndex = 14;
            // 
            // lblCheckProNameToDelete
            // 
            this.lblCheckProNameToDelete.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblCheckProNameToDelete.AutoSize = true;
            this.lblCheckProNameToDelete.Location = new System.Drawing.Point(27, 59);
            this.lblCheckProNameToDelete.Name = "lblCheckProNameToDelete";
            this.lblCheckProNameToDelete.Size = new System.Drawing.Size(97, 16);
            this.lblCheckProNameToDelete.TabIndex = 13;
            this.lblCheckProNameToDelete.Text = "Product Name:";
            // 
            // lblCheckModelNumToDelete
            // 
            this.lblCheckModelNumToDelete.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblCheckModelNumToDelete.AutoSize = true;
            this.lblCheckModelNumToDelete.Location = new System.Drawing.Point(27, 106);
            this.lblCheckModelNumToDelete.Name = "lblCheckModelNumToDelete";
            this.lblCheckModelNumToDelete.Size = new System.Drawing.Size(100, 16);
            this.lblCheckModelNumToDelete.TabIndex = 12;
            this.lblCheckModelNumToDelete.Text = "Model Number:";
            // 
            // pnlCheckIfProToAddExist
            // 
            this.pnlCheckIfProToAddExist.Controls.Add(this.groupBox3);
            this.pnlCheckIfProToAddExist.Controls.Add(this.label6);
            this.pnlCheckIfProToAddExist.Controls.Add(this.pictureBox3);
            this.pnlCheckIfProToAddExist.Controls.Add(this.btnCancelProToAddExist);
            this.pnlCheckIfProToAddExist.Controls.Add(this.btnCheckProToAddExist);
            this.pnlCheckIfProToAddExist.Controls.Add(this.label1);
            this.pnlCheckIfProToAddExist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCheckIfProToAddExist.Location = new System.Drawing.Point(0, 0);
            this.pnlCheckIfProToAddExist.Name = "pnlCheckIfProToAddExist";
            this.pnlCheckIfProToAddExist.Size = new System.Drawing.Size(584, 444);
            this.pnlCheckIfProToAddExist.TabIndex = 19;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblCheckProNameToAdd);
            this.groupBox3.Controls.Add(this.lblCheckModelNumToAdd);
            this.groupBox3.Controls.Add(this.tbxCheckModelNumToAdd);
            this.groupBox3.Controls.Add(this.tbxCheckProNameToAdd);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(50, 164);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(400, 200);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Search Product";
            // 
            // lblCheckProNameToAdd
            // 
            this.lblCheckProNameToAdd.AutoSize = true;
            this.lblCheckProNameToAdd.Location = new System.Drawing.Point(27, 59);
            this.lblCheckProNameToAdd.Name = "lblCheckProNameToAdd";
            this.lblCheckProNameToAdd.Size = new System.Drawing.Size(97, 16);
            this.lblCheckProNameToAdd.TabIndex = 2;
            this.lblCheckProNameToAdd.Text = "Product Name:";
            // 
            // lblCheckModelNumToAdd
            // 
            this.lblCheckModelNumToAdd.AutoSize = true;
            this.lblCheckModelNumToAdd.Location = new System.Drawing.Point(27, 106);
            this.lblCheckModelNumToAdd.Name = "lblCheckModelNumToAdd";
            this.lblCheckModelNumToAdd.Size = new System.Drawing.Size(100, 16);
            this.lblCheckModelNumToAdd.TabIndex = 1;
            this.lblCheckModelNumToAdd.Text = "Model Number:";
            // 
            // tbxCheckModelNumToAdd
            // 
            this.tbxCheckModelNumToAdd.Location = new System.Drawing.Point(138, 103);
            this.tbxCheckModelNumToAdd.Name = "tbxCheckModelNumToAdd";
            this.tbxCheckModelNumToAdd.Size = new System.Drawing.Size(190, 22);
            this.tbxCheckModelNumToAdd.TabIndex = 3;
            // 
            // tbxCheckProNameToAdd
            // 
            this.tbxCheckProNameToAdd.Location = new System.Drawing.Point(138, 56);
            this.tbxCheckProNameToAdd.Name = "tbxCheckProNameToAdd";
            this.tbxCheckProNameToAdd.Size = new System.Drawing.Size(190, 22);
            this.tbxCheckProNameToAdd.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(45, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 25);
            this.label6.TabIndex = 18;
            this.label6.Text = "Add Product";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(453, 34);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 81);
            this.pictureBox3.TabIndex = 17;
            this.pictureBox3.TabStop = false;
            // 
            // btnCancelProToAddExist
            // 
            this.btnCancelProToAddExist.Location = new System.Drawing.Point(477, 386);
            this.btnCancelProToAddExist.Name = "btnCancelProToAddExist";
            this.btnCancelProToAddExist.Size = new System.Drawing.Size(75, 23);
            this.btnCancelProToAddExist.TabIndex = 6;
            this.btnCancelProToAddExist.Text = "Cancel";
            this.btnCancelProToAddExist.UseVisualStyleBackColor = true;
            // 
            // btnCheckProToAddExist
            // 
            this.btnCheckProToAddExist.Location = new System.Drawing.Point(375, 386);
            this.btnCheckProToAddExist.Name = "btnCheckProToAddExist";
            this.btnCheckProToAddExist.Size = new System.Drawing.Size(75, 23);
            this.btnCheckProToAddExist.TabIndex = 5;
            this.btnCheckProToAddExist.Text = "Check";
            this.btnCheckProToAddExist.UseVisualStyleBackColor = true;
            this.btnCheckProToAddExist.Click += new System.EventHandler(this.btnCheckProToAddExist_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(231, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please enter the following information:";
            // 
            // btnCheckCatToDeleteExist
            // 
            this.btnCheckCatToDeleteExist.Location = new System.Drawing.Point(375, 386);
            this.btnCheckCatToDeleteExist.Name = "btnCheckCatToDeleteExist";
            this.btnCheckCatToDeleteExist.Size = new System.Drawing.Size(75, 23);
            this.btnCheckCatToDeleteExist.TabIndex = 0;
            this.btnCheckCatToDeleteExist.Text = "Check";
            this.btnCheckCatToDeleteExist.UseVisualStyleBackColor = true;
            this.btnCheckCatToDeleteExist.Click += new System.EventHandler(this.btnCheckCatToDeleteExist_Click);
            // 
            // btnCancelCatToDeleteExist
            // 
            this.btnCancelCatToDeleteExist.Location = new System.Drawing.Point(477, 386);
            this.btnCancelCatToDeleteExist.Name = "btnCancelCatToDeleteExist";
            this.btnCancelCatToDeleteExist.Size = new System.Drawing.Size(75, 23);
            this.btnCancelCatToDeleteExist.TabIndex = 1;
            this.btnCancelCatToDeleteExist.Text = "Cancel";
            this.btnCancelCatToDeleteExist.UseVisualStyleBackColor = true;
            // 
            // lblEnterCatName3
            // 
            this.lblEnterCatName3.AutoSize = true;
            this.lblEnterCatName3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnterCatName3.Location = new System.Drawing.Point(47, 111);
            this.lblEnterCatName3.Name = "lblEnterCatName3";
            this.lblEnterCatName3.Size = new System.Drawing.Size(231, 16);
            this.lblEnterCatName3.TabIndex = 2;
            this.lblEnterCatName3.Text = "Please enter the following information:";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(453, 34);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 81);
            this.pictureBox4.TabIndex = 17;
            this.pictureBox4.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(45, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(167, 25);
            this.label7.TabIndex = 18;
            this.label7.Text = "Delete Category";
            // 
            // pnlCheckIfCatToDeleteExist
            // 
            this.pnlCheckIfCatToDeleteExist.Controls.Add(this.groupBox4);
            this.pnlCheckIfCatToDeleteExist.Controls.Add(this.label7);
            this.pnlCheckIfCatToDeleteExist.Controls.Add(this.pictureBox4);
            this.pnlCheckIfCatToDeleteExist.Controls.Add(this.lblEnterCatName3);
            this.pnlCheckIfCatToDeleteExist.Controls.Add(this.btnCancelCatToDeleteExist);
            this.pnlCheckIfCatToDeleteExist.Controls.Add(this.btnCheckCatToDeleteExist);
            this.pnlCheckIfCatToDeleteExist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCheckIfCatToDeleteExist.Location = new System.Drawing.Point(0, 0);
            this.pnlCheckIfCatToDeleteExist.Name = "pnlCheckIfCatToDeleteExist";
            this.pnlCheckIfCatToDeleteExist.Size = new System.Drawing.Size(584, 444);
            this.pnlCheckIfCatToDeleteExist.TabIndex = 10;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblCheckCatNameToDelete);
            this.groupBox4.Controls.Add(this.tbxCheckCatNameToDelete);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(50, 164);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(400, 200);
            this.groupBox4.TabIndex = 19;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Search Category";
            // 
            // lblCheckCatNameToDelete
            // 
            this.lblCheckCatNameToDelete.AutoSize = true;
            this.lblCheckCatNameToDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckCatNameToDelete.Location = new System.Drawing.Point(27, 88);
            this.lblCheckCatNameToDelete.Name = "lblCheckCatNameToDelete";
            this.lblCheckCatNameToDelete.Size = new System.Drawing.Size(106, 16);
            this.lblCheckCatNameToDelete.TabIndex = 6;
            this.lblCheckCatNameToDelete.Text = "Category Name:";
            // 
            // tbxCheckCatNameToDelete
            // 
            this.tbxCheckCatNameToDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxCheckCatNameToDelete.Location = new System.Drawing.Point(153, 85);
            this.tbxCheckCatNameToDelete.Name = "tbxCheckCatNameToDelete";
            this.tbxCheckCatNameToDelete.Size = new System.Drawing.Size(190, 22);
            this.tbxCheckCatNameToDelete.TabIndex = 5;
            // 
            // pnlAddProduct
            // 
            this.pnlAddProduct.AutoScroll = true;
            this.pnlAddProduct.Controls.Add(this.btnCancelAddProToCategory);
            this.pnlAddProduct.Controls.Add(this.btnAddProToCategory);
            this.pnlAddProduct.Controls.Add(this.groupBox10);
            this.pnlAddProduct.Controls.Add(this.label23);
            this.pnlAddProduct.Controls.Add(this.pictureBox11);
            this.pnlAddProduct.Controls.Add(this.label24);
            this.pnlAddProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAddProduct.Location = new System.Drawing.Point(0, 0);
            this.pnlAddProduct.Name = "pnlAddProduct";
            this.pnlAddProduct.Size = new System.Drawing.Size(584, 444);
            this.pnlAddProduct.TabIndex = 20;
            // 
            // btnCancelAddProToCategory
            // 
            this.btnCancelAddProToCategory.Location = new System.Drawing.Point(478, 658);
            this.btnCancelAddProToCategory.Name = "btnCancelAddProToCategory";
            this.btnCancelAddProToCategory.Size = new System.Drawing.Size(75, 23);
            this.btnCancelAddProToCategory.TabIndex = 15;
            this.btnCancelAddProToCategory.Text = "Cancel";
            this.btnCancelAddProToCategory.UseVisualStyleBackColor = true;
            // 
            // btnAddProToCategory
            // 
            this.btnAddProToCategory.Location = new System.Drawing.Point(375, 658);
            this.btnAddProToCategory.Name = "btnAddProToCategory";
            this.btnAddProToCategory.Size = new System.Drawing.Size(75, 23);
            this.btnAddProToCategory.TabIndex = 24;
            this.btnAddProToCategory.Text = "Add";
            this.btnAddProToCategory.UseVisualStyleBackColor = true;
            this.btnAddProToCategory.Click += new System.EventHandler(this.btnAddProToCategory_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.pictureBox13);
            this.groupBox10.Controls.Add(this.pictureBox12);
            this.groupBox10.Controls.Add(this.label29);
            this.groupBox10.Controls.Add(this.textBox1);
            this.groupBox10.Controls.Add(this.label28);
            this.groupBox10.Controls.Add(this.lblProModelNumToBeAdded);
            this.groupBox10.Controls.Add(this.lblProNameToBeAdded);
            this.groupBox10.Controls.Add(this.label27);
            this.groupBox10.Controls.Add(this.cbxProCategoryToBeAdded);
            this.groupBox10.Controls.Add(this.label26);
            this.groupBox10.Controls.Add(this.tbxProRetailPriceToBeAdded);
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Controls.Add(this.tbxProMSRPToBeAdded);
            this.groupBox10.Controls.Add(this.label21);
            this.groupBox10.Controls.Add(this.label22);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(50, 164);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(400, 486);
            this.groupBox10.TabIndex = 23;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Product Details";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Location = new System.Drawing.Point(244, 386);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(100, 66);
            this.pictureBox13.TabIndex = 17;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Location = new System.Drawing.Point(138, 386);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(100, 66);
            this.pictureBox12.TabIndex = 16;
            this.pictureBox12.TabStop = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(28, 386);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(56, 16);
            this.label29.TabIndex = 15;
            this.label29.Text = "Images:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(138, 271);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(190, 95);
            this.textBox1.TabIndex = 14;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(27, 269);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(79, 16);
            this.label28.TabIndex = 13;
            this.label28.Text = "Description:";
            // 
            // lblProModelNumToBeAdded
            // 
            this.lblProModelNumToBeAdded.AutoSize = true;
            this.lblProModelNumToBeAdded.Location = new System.Drawing.Point(135, 85);
            this.lblProModelNumToBeAdded.Name = "lblProModelNumToBeAdded";
            this.lblProModelNumToBeAdded.Size = new System.Drawing.Size(0, 16);
            this.lblProModelNumToBeAdded.TabIndex = 12;
            // 
            // lblProNameToBeAdded
            // 
            this.lblProNameToBeAdded.AutoSize = true;
            this.lblProNameToBeAdded.Location = new System.Drawing.Point(135, 39);
            this.lblProNameToBeAdded.Name = "lblProNameToBeAdded";
            this.lblProNameToBeAdded.Size = new System.Drawing.Size(0, 16);
            this.lblProNameToBeAdded.TabIndex = 11;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(27, 227);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(66, 16);
            this.label27.TabIndex = 10;
            this.label27.Text = "Category:";
            // 
            // cbxProCategoryToBeAdded
            // 
            this.cbxProCategoryToBeAdded.FormattingEnabled = true;
            this.cbxProCategoryToBeAdded.Location = new System.Drawing.Point(138, 224);
            this.cbxProCategoryToBeAdded.Name = "cbxProCategoryToBeAdded";
            this.cbxProCategoryToBeAdded.Size = new System.Drawing.Size(190, 24);
            this.cbxProCategoryToBeAdded.TabIndex = 9;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(27, 180);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(80, 16);
            this.label26.TabIndex = 8;
            this.label26.Text = "Retail Price:";
            // 
            // tbxProRetailPriceToBeAdded
            // 
            this.tbxProRetailPriceToBeAdded.Location = new System.Drawing.Point(138, 177);
            this.tbxProRetailPriceToBeAdded.Name = "tbxProRetailPriceToBeAdded";
            this.tbxProRetailPriceToBeAdded.Size = new System.Drawing.Size(190, 22);
            this.tbxProRetailPriceToBeAdded.TabIndex = 7;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(27, 133);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(50, 16);
            this.label25.TabIndex = 6;
            this.label25.Text = "MSRP:";
            // 
            // tbxProMSRPToBeAdded
            // 
            this.tbxProMSRPToBeAdded.Location = new System.Drawing.Point(138, 130);
            this.tbxProMSRPToBeAdded.Name = "tbxProMSRPToBeAdded";
            this.tbxProMSRPToBeAdded.Size = new System.Drawing.Size(190, 22);
            this.tbxProMSRPToBeAdded.TabIndex = 5;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(27, 39);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(97, 16);
            this.label21.TabIndex = 2;
            this.label21.Text = "Product Name:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(27, 86);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 16);
            this.label22.TabIndex = 1;
            this.label22.Text = "Model Number:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(45, 43);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(130, 25);
            this.label23.TabIndex = 22;
            this.label23.Text = "Add Product";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(453, 34);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(100, 81);
            this.pictureBox11.TabIndex = 21;
            this.pictureBox11.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(47, 111);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(231, 16);
            this.label24.TabIndex = 20;
            this.label24.Text = "Please enter the following information:";
            // 
            // pnlModifyProduct
            // 
            this.pnlModifyProduct.AutoScroll = true;
            this.pnlModifyProduct.Controls.Add(this.button3);
            this.pnlModifyProduct.Controls.Add(this.btnModifyProduct);
            this.pnlModifyProduct.Controls.Add(this.groupBox11);
            this.pnlModifyProduct.Controls.Add(this.label39);
            this.pnlModifyProduct.Controls.Add(this.pictureBox16);
            this.pnlModifyProduct.Controls.Add(this.label40);
            this.pnlModifyProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlModifyProduct.Location = new System.Drawing.Point(0, 0);
            this.pnlModifyProduct.Name = "pnlModifyProduct";
            this.pnlModifyProduct.Size = new System.Drawing.Size(584, 444);
            this.pnlModifyProduct.TabIndex = 21;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(478, 658);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 15;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnModifyProduct
            // 
            this.btnModifyProduct.Location = new System.Drawing.Point(375, 658);
            this.btnModifyProduct.Name = "btnModifyProduct";
            this.btnModifyProduct.Size = new System.Drawing.Size(75, 23);
            this.btnModifyProduct.TabIndex = 24;
            this.btnModifyProduct.Text = "Update";
            this.btnModifyProduct.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.pictureBox14);
            this.groupBox11.Controls.Add(this.pictureBox15);
            this.groupBox11.Controls.Add(this.label30);
            this.groupBox11.Controls.Add(this.textBox2);
            this.groupBox11.Controls.Add(this.label31);
            this.groupBox11.Controls.Add(this.lblProModelNumToModify);
            this.groupBox11.Controls.Add(this.lblProNameToModify);
            this.groupBox11.Controls.Add(this.label34);
            this.groupBox11.Controls.Add(this.comboBox1);
            this.groupBox11.Controls.Add(this.label35);
            this.groupBox11.Controls.Add(this.textBox3);
            this.groupBox11.Controls.Add(this.label36);
            this.groupBox11.Controls.Add(this.textBox4);
            this.groupBox11.Controls.Add(this.label37);
            this.groupBox11.Controls.Add(this.label38);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(50, 164);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(400, 486);
            this.groupBox11.TabIndex = 23;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Product Details";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Location = new System.Drawing.Point(244, 386);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(100, 66);
            this.pictureBox14.TabIndex = 17;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Location = new System.Drawing.Point(138, 386);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(100, 66);
            this.pictureBox15.TabIndex = 16;
            this.pictureBox15.TabStop = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(28, 386);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(56, 16);
            this.label30.TabIndex = 15;
            this.label30.Text = "Images:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(138, 271);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(190, 95);
            this.textBox2.TabIndex = 14;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(27, 269);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(79, 16);
            this.label31.TabIndex = 13;
            this.label31.Text = "Description:";
            // 
            // lblProModelNumToModify
            // 
            this.lblProModelNumToModify.AutoSize = true;
            this.lblProModelNumToModify.Location = new System.Drawing.Point(135, 85);
            this.lblProModelNumToModify.Name = "lblProModelNumToModify";
            this.lblProModelNumToModify.Size = new System.Drawing.Size(66, 16);
            this.lblProModelNumToModify.TabIndex = 12;
            this.lblProModelNumToModify.Text = "1234567A";
            // 
            // lblProNameToModify
            // 
            this.lblProNameToModify.AutoSize = true;
            this.lblProNameToModify.Location = new System.Drawing.Point(135, 39);
            this.lblProNameToModify.Name = "lblProNameToModify";
            this.lblProNameToModify.Size = new System.Drawing.Size(75, 16);
            this.lblProNameToModify.TabIndex = 11;
            this.lblProNameToModify.Text = "MP4-Super";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(27, 227);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(66, 16);
            this.label34.TabIndex = 10;
            this.label34.Text = "Category:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(138, 224);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(190, 24);
            this.comboBox1.TabIndex = 9;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(27, 180);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(80, 16);
            this.label35.TabIndex = 8;
            this.label35.Text = "Retail Price:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(138, 177);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(190, 22);
            this.textBox3.TabIndex = 7;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(27, 133);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(50, 16);
            this.label36.TabIndex = 6;
            this.label36.Text = "MSRP:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(138, 130);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(190, 22);
            this.textBox4.TabIndex = 5;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(27, 39);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(97, 16);
            this.label37.TabIndex = 2;
            this.label37.Text = "Product Name:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(27, 86);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(100, 16);
            this.label38.TabIndex = 1;
            this.label38.Text = "Model Number:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(45, 43);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(156, 25);
            this.label39.TabIndex = 22;
            this.label39.Text = "Modify Product";
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox16.Image")));
            this.pictureBox16.Location = new System.Drawing.Point(453, 34);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(100, 81);
            this.pictureBox16.TabIndex = 21;
            this.pictureBox16.TabStop = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(47, 111);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(231, 16);
            this.label40.TabIndex = 20;
            this.label40.Text = "Please enter the following information:";
            // 
            // pnlDeleteProduct
            // 
            this.pnlDeleteProduct.AutoScroll = true;
            this.pnlDeleteProduct.Controls.Add(this.button5);
            this.pnlDeleteProduct.Controls.Add(this.btnDeleteProduct);
            this.pnlDeleteProduct.Controls.Add(this.label50);
            this.pnlDeleteProduct.Controls.Add(this.pictureBox19);
            this.pnlDeleteProduct.Controls.Add(this.label51);
            this.pnlDeleteProduct.Controls.Add(this.groupBox12);
            this.pnlDeleteProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDeleteProduct.Location = new System.Drawing.Point(0, 0);
            this.pnlDeleteProduct.Name = "pnlDeleteProduct";
            this.pnlDeleteProduct.Size = new System.Drawing.Size(584, 444);
            this.pnlDeleteProduct.TabIndex = 22;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(478, 658);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 15;
            this.button5.Text = "Cancel";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // btnDeleteProduct
            // 
            this.btnDeleteProduct.Location = new System.Drawing.Point(375, 658);
            this.btnDeleteProduct.Name = "btnDeleteProduct";
            this.btnDeleteProduct.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteProduct.TabIndex = 24;
            this.btnDeleteProduct.Text = "Delete";
            this.btnDeleteProduct.UseVisualStyleBackColor = true;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(45, 43);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(154, 25);
            this.label50.TabIndex = 22;
            this.label50.Text = "Delete Product";
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox19.Image")));
            this.pictureBox19.Location = new System.Drawing.Point(453, 34);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(100, 81);
            this.pictureBox19.TabIndex = 21;
            this.pictureBox19.TabStop = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(47, 111);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(231, 16);
            this.label51.TabIndex = 20;
            this.label51.Text = "Please enter the following information:";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.lblProDescToDelete);
            this.groupBox12.Controls.Add(this.lblProCatToDelete);
            this.groupBox12.Controls.Add(this.lblProRetailPriceToDelete);
            this.groupBox12.Controls.Add(this.lblProMSRPToDelete);
            this.groupBox12.Controls.Add(this.pictureBox17);
            this.groupBox12.Controls.Add(this.pictureBox18);
            this.groupBox12.Controls.Add(this.label41);
            this.groupBox12.Controls.Add(this.label42);
            this.groupBox12.Controls.Add(this.lblProModelNumToDelete);
            this.groupBox12.Controls.Add(this.lblProNameToDelete);
            this.groupBox12.Controls.Add(this.label45);
            this.groupBox12.Controls.Add(this.label46);
            this.groupBox12.Controls.Add(this.label47);
            this.groupBox12.Controls.Add(this.label48);
            this.groupBox12.Controls.Add(this.label49);
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.Location = new System.Drawing.Point(50, 164);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(400, 486);
            this.groupBox12.TabIndex = 23;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Product Details";
            // 
            // lblProDescToDelete
            // 
            this.lblProDescToDelete.Location = new System.Drawing.Point(138, 266);
            this.lblProDescToDelete.Multiline = true;
            this.lblProDescToDelete.Name = "lblProDescToDelete";
            this.lblProDescToDelete.ReadOnly = true;
            this.lblProDescToDelete.Size = new System.Drawing.Size(220, 102);
            this.lblProDescToDelete.TabIndex = 21;
            this.lblProDescToDelete.Text = "MP4-Super is the cheapest MP4 player.";
            // 
            // lblProCatToDelete
            // 
            this.lblProCatToDelete.AutoSize = true;
            this.lblProCatToDelete.Location = new System.Drawing.Point(135, 227);
            this.lblProCatToDelete.Name = "lblProCatToDelete";
            this.lblProCatToDelete.Size = new System.Drawing.Size(35, 16);
            this.lblProCatToDelete.TabIndex = 20;
            this.lblProCatToDelete.Text = "MP4";
            // 
            // lblProRetailPriceToDelete
            // 
            this.lblProRetailPriceToDelete.AutoSize = true;
            this.lblProRetailPriceToDelete.Location = new System.Drawing.Point(135, 179);
            this.lblProRetailPriceToDelete.Name = "lblProRetailPriceToDelete";
            this.lblProRetailPriceToDelete.Size = new System.Drawing.Size(29, 16);
            this.lblProRetailPriceToDelete.TabIndex = 19;
            this.lblProRetailPriceToDelete.Text = "$65";
            // 
            // lblProMSRPToDelete
            // 
            this.lblProMSRPToDelete.AutoSize = true;
            this.lblProMSRPToDelete.Location = new System.Drawing.Point(135, 133);
            this.lblProMSRPToDelete.Name = "lblProMSRPToDelete";
            this.lblProMSRPToDelete.Size = new System.Drawing.Size(29, 16);
            this.lblProMSRPToDelete.TabIndex = 18;
            this.lblProMSRPToDelete.Text = "$50";
            // 
            // pictureBox17
            // 
            this.pictureBox17.Location = new System.Drawing.Point(258, 386);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(100, 66);
            this.pictureBox17.TabIndex = 17;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Location = new System.Drawing.Point(138, 386);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(100, 66);
            this.pictureBox18.TabIndex = 16;
            this.pictureBox18.TabStop = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(28, 386);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(56, 16);
            this.label41.TabIndex = 15;
            this.label41.Text = "Images:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(27, 269);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(79, 16);
            this.label42.TabIndex = 13;
            this.label42.Text = "Description:";
            // 
            // lblProModelNumToDelete
            // 
            this.lblProModelNumToDelete.AutoSize = true;
            this.lblProModelNumToDelete.Location = new System.Drawing.Point(135, 86);
            this.lblProModelNumToDelete.Name = "lblProModelNumToDelete";
            this.lblProModelNumToDelete.Size = new System.Drawing.Size(66, 16);
            this.lblProModelNumToDelete.TabIndex = 12;
            this.lblProModelNumToDelete.Text = "1234567A";
            // 
            // lblProNameToDelete
            // 
            this.lblProNameToDelete.AutoSize = true;
            this.lblProNameToDelete.Location = new System.Drawing.Point(135, 39);
            this.lblProNameToDelete.Name = "lblProNameToDelete";
            this.lblProNameToDelete.Size = new System.Drawing.Size(75, 16);
            this.lblProNameToDelete.TabIndex = 11;
            this.lblProNameToDelete.Text = "MP4-Super";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(27, 227);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(66, 16);
            this.label45.TabIndex = 10;
            this.label45.Text = "Category:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(27, 180);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(80, 16);
            this.label46.TabIndex = 8;
            this.label46.Text = "Retail Price:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(27, 133);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(50, 16);
            this.label47.TabIndex = 6;
            this.label47.Text = "MSRP:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(27, 39);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(97, 16);
            this.label48.TabIndex = 2;
            this.label48.Text = "Product Name:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(27, 86);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(100, 16);
            this.label49.TabIndex = 1;
            this.label49.Text = "Model Number:";
            // 
            // pnlSearchAndDisplayCatInfo
            // 
            this.pnlSearchAndDisplayCatInfo.Controls.Add(this.groupBox13);
            this.pnlSearchAndDisplayCatInfo.Controls.Add(this.label56);
            this.pnlSearchAndDisplayCatInfo.Controls.Add(this.pictureBox20);
            this.pnlSearchAndDisplayCatInfo.Controls.Add(this.label57);
            this.pnlSearchAndDisplayCatInfo.Controls.Add(this.label59);
            this.pnlSearchAndDisplayCatInfo.Controls.Add(this.tbxSearchCategoryName);
            this.pnlSearchAndDisplayCatInfo.Controls.Add(this.button4);
            this.pnlSearchAndDisplayCatInfo.Controls.Add(this.button6);
            this.pnlSearchAndDisplayCatInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchAndDisplayCatInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchAndDisplayCatInfo.Name = "pnlSearchAndDisplayCatInfo";
            this.pnlSearchAndDisplayCatInfo.Size = new System.Drawing.Size(584, 444);
            this.pnlSearchAndDisplayCatInfo.TabIndex = 23;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.tbxDisplayCatDescription);
            this.groupBox13.Controls.Add(this.label61);
            this.groupBox13.Controls.Add(this.lblDisplayCategoryName);
            this.groupBox13.Controls.Add(this.label58);
            this.groupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.Location = new System.Drawing.Point(94, 203);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(356, 204);
            this.groupBox13.TabIndex = 24;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Search Result";
            // 
            // tbxDisplayCatDescription
            // 
            this.tbxDisplayCatDescription.Location = new System.Drawing.Point(123, 78);
            this.tbxDisplayCatDescription.Multiline = true;
            this.tbxDisplayCatDescription.Name = "tbxDisplayCatDescription";
            this.tbxDisplayCatDescription.ReadOnly = true;
            this.tbxDisplayCatDescription.Size = new System.Drawing.Size(212, 107);
            this.tbxDisplayCatDescription.TabIndex = 13;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(14, 78);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(79, 16);
            this.label61.TabIndex = 12;
            this.label61.Text = "Description:";
            // 
            // lblDisplayCategoryName
            // 
            this.lblDisplayCategoryName.AutoSize = true;
            this.lblDisplayCategoryName.Location = new System.Drawing.Point(126, 41);
            this.lblDisplayCategoryName.Name = "lblDisplayCategoryName";
            this.lblDisplayCategoryName.Size = new System.Drawing.Size(0, 16);
            this.lblDisplayCategoryName.TabIndex = 11;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(14, 41);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(106, 16);
            this.label58.TabIndex = 10;
            this.label58.Text = "Category Name:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(45, 43);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(333, 25);
            this.label56.TabIndex = 23;
            this.label56.Text = "Search and Display Category Info";
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox20.Image")));
            this.pictureBox20.Location = new System.Drawing.Point(453, 34);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(100, 81);
            this.pictureBox20.TabIndex = 22;
            this.pictureBox20.TabStop = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(47, 111);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(231, 16);
            this.label57.TabIndex = 21;
            this.label57.Text = "Please enter the following information:";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(91, 160);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(106, 16);
            this.label59.TabIndex = 9;
            this.label59.Text = "Category Name:";
            // 
            // tbxSearchCategoryName
            // 
            this.tbxSearchCategoryName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxSearchCategoryName.Location = new System.Drawing.Point(217, 157);
            this.tbxSearchCategoryName.Name = "tbxSearchCategoryName";
            this.tbxSearchCategoryName.Size = new System.Drawing.Size(212, 22);
            this.tbxSearchCategoryName.TabIndex = 8;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(477, 413);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 6;
            this.button4.Text = "Cancel";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(375, 413);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 5;
            this.button6.Text = "Display";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // pnlSearchAndDisplayProInfo
            // 
            this.pnlSearchAndDisplayProInfo.AutoScroll = true;
            this.pnlSearchAndDisplayProInfo.Controls.Add(this.tbxSearchProModelNum);
            this.pnlSearchAndDisplayProInfo.Controls.Add(this.label44);
            this.pnlSearchAndDisplayProInfo.Controls.Add(this.label32);
            this.pnlSearchAndDisplayProInfo.Controls.Add(this.pictureBox10);
            this.pnlSearchAndDisplayProInfo.Controls.Add(this.label33);
            this.pnlSearchAndDisplayProInfo.Controls.Add(this.label43);
            this.pnlSearchAndDisplayProInfo.Controls.Add(this.tbxSearchProductName);
            this.pnlSearchAndDisplayProInfo.Controls.Add(this.button2);
            this.pnlSearchAndDisplayProInfo.Controls.Add(this.btnDisplayProInfo);
            this.pnlSearchAndDisplayProInfo.Controls.Add(this.groupBox14);
            this.pnlSearchAndDisplayProInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchAndDisplayProInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchAndDisplayProInfo.Name = "pnlSearchAndDisplayProInfo";
            this.pnlSearchAndDisplayProInfo.Size = new System.Drawing.Size(584, 444);
            this.pnlSearchAndDisplayProInfo.TabIndex = 24;
            // 
            // tbxSearchProModelNum
            // 
            this.tbxSearchProModelNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxSearchProModelNum.Location = new System.Drawing.Point(217, 192);
            this.tbxSearchProModelNum.Name = "tbxSearchProModelNum";
            this.tbxSearchProModelNum.Size = new System.Drawing.Size(212, 22);
            this.tbxSearchProModelNum.TabIndex = 26;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(91, 195);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(100, 16);
            this.label44.TabIndex = 25;
            this.label44.Text = "Model Number:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(45, 43);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(320, 25);
            this.label32.TabIndex = 23;
            this.label32.Text = "Search and Display Product Info";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(453, 34);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(100, 81);
            this.pictureBox10.TabIndex = 22;
            this.pictureBox10.TabStop = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(47, 111);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(231, 16);
            this.label33.TabIndex = 21;
            this.label33.Text = "Please enter the following information:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(91, 160);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(97, 16);
            this.label43.TabIndex = 9;
            this.label43.Text = "Product Name:";
            // 
            // tbxSearchProductName
            // 
            this.tbxSearchProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxSearchProductName.Location = new System.Drawing.Point(217, 157);
            this.tbxSearchProductName.Name = "tbxSearchProductName";
            this.tbxSearchProductName.Size = new System.Drawing.Size(212, 22);
            this.tbxSearchProductName.TabIndex = 8;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(477, 671);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btnDisplayProInfo
            // 
            this.btnDisplayProInfo.Location = new System.Drawing.Point(375, 671);
            this.btnDisplayProInfo.Name = "btnDisplayProInfo";
            this.btnDisplayProInfo.Size = new System.Drawing.Size(75, 23);
            this.btnDisplayProInfo.TabIndex = 5;
            this.btnDisplayProInfo.Text = "Display";
            this.btnDisplayProInfo.UseVisualStyleBackColor = true;
            this.btnDisplayProInfo.Click += new System.EventHandler(this.btnDisplayProInfo_Click);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.lblDisplayCategory);
            this.groupBox14.Controls.Add(this.lblDisplayRetailPrice);
            this.groupBox14.Controls.Add(this.lblDisplayMSRP);
            this.groupBox14.Controls.Add(this.lblDisplayProModelNum);
            this.groupBox14.Controls.Add(this.label55);
            this.groupBox14.Controls.Add(this.label60);
            this.groupBox14.Controls.Add(this.label62);
            this.groupBox14.Controls.Add(this.label63);
            this.groupBox14.Controls.Add(this.tbxDisplayProDes);
            this.groupBox14.Controls.Add(this.label17);
            this.groupBox14.Controls.Add(this.lblDisplayProName);
            this.groupBox14.Controls.Add(this.label20);
            this.groupBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox14.Location = new System.Drawing.Point(94, 245);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(356, 393);
            this.groupBox14.TabIndex = 24;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Search Result";
            // 
            // lblDisplayCategory
            // 
            this.lblDisplayCategory.AutoSize = true;
            this.lblDisplayCategory.Location = new System.Drawing.Point(122, 219);
            this.lblDisplayCategory.Name = "lblDisplayCategory";
            this.lblDisplayCategory.Size = new System.Drawing.Size(0, 16);
            this.lblDisplayCategory.TabIndex = 28;
            // 
            // lblDisplayRetailPrice
            // 
            this.lblDisplayRetailPrice.AutoSize = true;
            this.lblDisplayRetailPrice.Location = new System.Drawing.Point(122, 171);
            this.lblDisplayRetailPrice.Name = "lblDisplayRetailPrice";
            this.lblDisplayRetailPrice.Size = new System.Drawing.Size(0, 16);
            this.lblDisplayRetailPrice.TabIndex = 27;
            // 
            // lblDisplayMSRP
            // 
            this.lblDisplayMSRP.AutoSize = true;
            this.lblDisplayMSRP.Location = new System.Drawing.Point(122, 125);
            this.lblDisplayMSRP.Name = "lblDisplayMSRP";
            this.lblDisplayMSRP.Size = new System.Drawing.Size(0, 16);
            this.lblDisplayMSRP.TabIndex = 26;
            // 
            // lblDisplayProModelNum
            // 
            this.lblDisplayProModelNum.AutoSize = true;
            this.lblDisplayProModelNum.Location = new System.Drawing.Point(122, 78);
            this.lblDisplayProModelNum.Name = "lblDisplayProModelNum";
            this.lblDisplayProModelNum.Size = new System.Drawing.Size(0, 16);
            this.lblDisplayProModelNum.TabIndex = 25;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(14, 219);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(66, 16);
            this.label55.TabIndex = 24;
            this.label55.Text = "Category:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(14, 172);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(80, 16);
            this.label60.TabIndex = 23;
            this.label60.Text = "Retail Price:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(14, 125);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(50, 16);
            this.label62.TabIndex = 22;
            this.label62.Text = "MSRP:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(14, 78);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(100, 16);
            this.label63.TabIndex = 21;
            this.label63.Text = "Model Number:";
            // 
            // tbxDisplayProDes
            // 
            this.tbxDisplayProDes.Location = new System.Drawing.Point(113, 263);
            this.tbxDisplayProDes.Multiline = true;
            this.tbxDisplayProDes.Name = "tbxDisplayProDes";
            this.tbxDisplayProDes.ReadOnly = true;
            this.tbxDisplayProDes.Size = new System.Drawing.Size(212, 107);
            this.tbxDisplayProDes.TabIndex = 13;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 262);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 16);
            this.label17.TabIndex = 12;
            this.label17.Text = "Description:";
            // 
            // lblDisplayProName
            // 
            this.lblDisplayProName.AutoSize = true;
            this.lblDisplayProName.Location = new System.Drawing.Point(120, 41);
            this.lblDisplayProName.Name = "lblDisplayProName";
            this.lblDisplayProName.Size = new System.Drawing.Size(0, 16);
            this.lblDisplayProName.TabIndex = 11;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(14, 41);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 16);
            this.label20.TabIndex = 10;
            this.label20.Text = "Product Name:";
            // 
            // frmCatalogue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 444);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pnlModifyProduct);
            this.Controls.Add(this.pnlAddProduct);
            this.Controls.Add(this.pnlCheckIfProToAddExist);
            this.Controls.Add(this.pnlCheckIfProToDeleteExist);
            this.Controls.Add(this.pnlCheckIfProToModifyExist);
            this.Controls.Add(this.pnlCheckIfCatToDeleteExist);
            this.Controls.Add(this.pnlCheckIfCatToModifyExist);
            this.Controls.Add(this.pnlCheckIfCatToAddExist);
            this.Controls.Add(this.pnlModifyCatDetail);
            this.Controls.Add(this.pnlDetailOfCategoryToDelete);
            this.Controls.Add(this.pnlEnterCatDetail);
            this.Controls.Add(this.pnlSearchAndDisplayProInfo);
            this.Controls.Add(this.pnlSearchAndDisplayCatInfo);
            this.Controls.Add(this.pnlDeleteProduct);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmCatalogue";
            this.Text = "Catalogue";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlCheckIfCatToAddExist.ResumeLayout(false);
            this.pnlCheckIfCatToAddExist.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.pnlEnterCatDetail.ResumeLayout(false);
            this.pnlEnterCatDetail.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.pnlCheckIfCatToModifyExist.ResumeLayout(false);
            this.pnlCheckIfCatToModifyExist.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.pnlModifyCatDetail.ResumeLayout(false);
            this.pnlModifyCatDetail.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.pnlDetailOfCategoryToDelete.ResumeLayout(false);
            this.pnlDetailOfCategoryToDelete.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.pnlCheckIfProToModifyExist.ResumeLayout(false);
            this.pnlCheckIfProToModifyExist.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pnlCheckIfProToDeleteExist.ResumeLayout(false);
            this.pnlCheckIfProToDeleteExist.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlCheckIfProToAddExist.ResumeLayout(false);
            this.pnlCheckIfProToAddExist.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.pnlCheckIfCatToDeleteExist.ResumeLayout(false);
            this.pnlCheckIfCatToDeleteExist.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.pnlAddProduct.ResumeLayout(false);
            this.pnlAddProduct.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.pnlModifyProduct.ResumeLayout(false);
            this.pnlModifyProduct.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.pnlDeleteProduct.ResumeLayout(false);
            this.pnlDeleteProduct.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            this.pnlSearchAndDisplayCatInfo.ResumeLayout(false);
            this.pnlSearchAndDisplayCatInfo.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            this.pnlSearchAndDisplayProInfo.ResumeLayout(false);
            this.pnlSearchAndDisplayProInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem catalogueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewCatalogueInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyCatalogueInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteCatalogueInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAndDisplayCatalogueInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productToolStripMenuItem;
        private System.Windows.Forms.Panel pnlCheckIfCatToAddExist;
        private System.Windows.Forms.Label lblEnterCatName;
        private System.Windows.Forms.Button btnExitCheckCatName;
        private System.Windows.Forms.Button btnCheckCatExist;
        private System.Windows.Forms.Panel pnlEnterCatDetail;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnExitEnterCatDetail;
        private System.Windows.Forms.Button btnConfirmCatDetail;
        private System.Windows.Forms.Panel pnlCheckIfCatToModifyExist;
        private System.Windows.Forms.Label lblEnterCatName2;
        private System.Windows.Forms.Button btnCheckCatToModifyExist;
        private System.Windows.Forms.Button btnCancelCatToModifyExist;
        private System.Windows.Forms.ToolStripMenuItem staffToolStripMenuItem;
        private System.Windows.Forms.Panel pnlModifyCatDetail;
        private System.Windows.Forms.Button btnUpdateCatDetail;
        private System.Windows.Forms.Button btnCancelUpdateDetail;
        private System.Windows.Forms.Panel pnlDetailOfCategoryToDelete;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnCategoryToDelete;
        private System.Windows.Forms.ToolStripMenuItem addNewProductInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyProductInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteProductInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAndDisplayProductInfoToolStripMenuItem;
        private System.Windows.Forms.Panel pnlCheckIfProToModifyExist;
        private System.Windows.Forms.Button btnCancelProToModifyExist;
        private System.Windows.Forms.Button btnCheckProToModifyExist;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnlCheckIfProToDeleteExist;
        private System.Windows.Forms.Button btnCancelProToDeleteExist;
        private System.Windows.Forms.Button btnCheckProToDeleteExist;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox tbxCheckProNameToDelete;
        private System.Windows.Forms.TextBox tbxCheckModelNumToDelete;
        private System.Windows.Forms.Label lblCheckProNameToDelete;
        private System.Windows.Forms.Label lblCheckModelNumToDelete;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbxCheckProNameToModify;
        private System.Windows.Forms.TextBox tbxCheckModelNumToModify;
        private System.Windows.Forms.Label lblCheckProNameToModify;
        private System.Windows.Forms.Label lblCheckModelNumToModify;
        private System.Windows.Forms.Panel pnlCheckIfProToAddExist;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnCancelProToAddExist;
        private System.Windows.Forms.Button btnCheckProToAddExist;
        private System.Windows.Forms.TextBox tbxCheckProNameToAdd;
        private System.Windows.Forms.TextBox tbxCheckModelNumToAdd;
        private System.Windows.Forms.Label lblCheckProNameToAdd;
        private System.Windows.Forms.Label lblCheckModelNumToAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnCheckCatToDeleteExist;
        private System.Windows.Forms.Button btnCancelCatToDeleteExist;
        private System.Windows.Forms.Label lblEnterCatName3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel pnlCheckIfCatToDeleteExist;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblCheckCatNameToDelete;
        private System.Windows.Forms.TextBox tbxCheckCatNameToDelete;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tbxCheckCatNameToModify;
        private System.Windows.Forms.Label lblCheckCatNameToModify;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label lblCheckCatNameToAdd;
        private System.Windows.Forms.TextBox tbxCheckCatNameToAdd;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox tbxEnterCatDescNameToAdd;
        private System.Windows.Forms.Label lblEnterCatDescToAdd;
        private System.Windows.Forms.Label lblEnterCatNameToAdd;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox tbxEnterCatDescNameToModify;
        private System.Windows.Forms.Label lblEnterCatDescToModify;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblNameOfCategoryToModify;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label lblDesOfCategoryToDelete;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblNameOfCategoryToDelete;
        private System.Windows.Forms.Panel pnlAddProduct;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbxProMSRPToBeAdded;
        private System.Windows.Forms.ComboBox cbxProCategoryToBeAdded;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbxProRetailPriceToBeAdded;
        private System.Windows.Forms.Label lblProModelNumToBeAdded;
        private System.Windows.Forms.Label lblProNameToBeAdded;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnCancelAddProToCategory;
        private System.Windows.Forms.Button btnAddProToCategory;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel pnlModifyProduct;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnModifyProduct;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label lblProModelNumToModify;
        private System.Windows.Forms.Label lblProNameToModify;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Panel pnlDeleteProduct;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnDeleteProduct;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label lblProCatToDelete;
        private System.Windows.Forms.Label lblProRetailPriceToDelete;
        private System.Windows.Forms.Label lblProMSRPToDelete;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label lblProModelNumToDelete;
        private System.Windows.Forms.Label lblProNameToDelete;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Panel pnlSearchAndDisplayCatInfo;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox tbxSearchCategoryName;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox tbxDisplayCatDescription;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label lblDisplayCategoryName;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox lblProDescToDelete;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Panel pnlSearchAndDisplayProInfo;
        private System.Windows.Forms.TextBox tbxSearchProModelNum;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TextBox tbxDisplayProDes;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblDisplayProName;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tbxSearchProductName;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnDisplayProInfo;
        private System.Windows.Forms.Label lblDisplayCategory;
        private System.Windows.Forms.Label lblDisplayRetailPrice;
        private System.Windows.Forms.Label lblDisplayMSRP;
        private System.Windows.Forms.Label lblDisplayProModelNum;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label lblCatNameToAdd;
        private System.Windows.Forms.ToolStripMenuItem loginLogOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registerNewStaffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyStaffInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAndDisplayStaffInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteExistingStaffInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addNewOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifyOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAndDisplayOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteOrderInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_AddSupplier;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_ModifySupplier;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_DeleteSupplier;
        private System.Windows.Forms.ToolStripMenuItem SupplierMenu_SearchSupplier;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem menuStock_EditProductQty;
        private System.Windows.Forms.ToolStripMenuItem menuStock_SearchProductQty;
        private System.Windows.Forms.ToolStripMenuItem menuStock_computeExcessShortFall;
    }
}

