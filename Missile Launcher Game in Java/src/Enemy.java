
import java.awt.Color;

public class Enemy extends GameEntity {
    //attributes
    private int speed;
  

    //constructor
    public Enemy() {
        super(0, 10, 30, 10, Color.BLUE);
        
        speed = 30;
      

    }

    //methods
    public void checkBoundary() {

        if (getXPos() > 270) {
            reset();

        }
    }

    public void reset() {
        delete();
        setXPos(0);
              
    }

    
    public void run() {
        while (true) {
            moveX(1);
            drawRect();
            delay(speed);
            delete();
            checkBoundary();
            
        }
    }

    public void setSpeed(int sped) {
        speed = speed - sped;
    }
}
