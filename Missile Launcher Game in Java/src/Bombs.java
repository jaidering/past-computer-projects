/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jai
 */
import java.awt.Color;

public class Bombs extends GameEntity {
    //attributes
    private int speed;


    //constructor
    public Bombs() {
        super(10, 10, 10, 10, Color.MAGENTA);
        speed = 30;
    }

    public void checkBoundary() {

        if (getYPos() > 190) {
            reset();
        }
    }

    public void reset() {
        delete();
        setYPos(0);
        setXPos(0);

    }

    public void run() {
        while (true) {
            moveX(1);
            moveY(1);


            drawSquare();
            delay(speed);
            delete();
            checkBoundary();

        }

    }

    public void setSpeed(int bombSpeed) {
        speed = speed - bombSpeed;
    }
}
