
import java.awt.Color;
import java.util.ArrayList;

public class GameManager extends Thread {
//attributes
    private Missile missile;
    private Enemy enemy;
    private Bombs bombing;
    private Time time;
    private Shots numOfShots;
    private int shots;
    private int badGuy;
    private ArrayList<Missile> mis;


//constructor
    public GameManager() {

        mis = new ArrayList<Missile>();
        shots = 0;
        badGuy = 0;
        this.numOfShots = numOfShots;
        mis.add(missile);
        time = new Time();


    }
    //methods
    public void fire() {
        //shooting from diff places alternately
        
        if (shots % 2 == 0) {
            missile = new Missile(100, 180, 5, 15, Color.RED, enemy, bombing);
        } else {
            missile = new Missile(170, 180, 5, 15, Color.GREEN, enemy, bombing);
        }

        missile.start();

        numOfShots = new Shots();
        numOfShots.decreaseNumOfShots(1);

           shots++;
        if (GameApp.timeText.getText().equals("0") || GameApp.shotText.getText().equals("0")) {
            time.stop();
            enemy.stop();
            bombing.stop();
            missile.stop();
        }

     

    }

    public void start() {

        while (enemy == null) {
           
           
            enemy = new Enemy();
            enemy.start();
            bombing = new Bombs();
            bombing.start();
            

 if (GameApp.timeText.getText().equals("0") || GameApp.shotText.getText().equals("0")) {
                time.stop();
                enemy.stop();
                bombing.stop();
                missile.stop();
            } else {
                time.start();
            }


        }
    }

    public void quit() {
        System.exit(0);
    }

    public void setSpeed(int speeds) {
        enemy.setSpeed(speeds);
    }

    public void setTime(int times) {
        time.setTime(times);
    }

    public void setScore(int scre) {
        scre = 0;

    }
}//end of class

