
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GameApp extends JFrame implements ActionListener {
    // Constants
    public static final Color bgColor = Color.WHITE;
    public static final Color fgColor = Color.BLUE;
    public static final int W = 300;
    public static final int H = 200;
    // Static variables
    public static JLabel scoreText,  totalScoreText,  levelText,  timeText,  shotText;
    public static JPanel drawPanel;
    private JButton startBtn,  quitBtn,  fireBtn;
    private JLabel scoreLabel,  totalScoreLabel,  levelLabel,  timeLabel,  shotsLabel;
    private GameManager gm;

    public static void main(String[] args) {
        GameApp frame = new GameApp();
        frame.setSize(340, 330); // define frame size
        frame.createGameGUI(); // create the Game GUI
        frame.setVisible(true);
        frame.setGm(new GameManager());


    }

    // Draw Panel, buttons and labels in the frame
    // Please DO NOT change any codes here in this method
    public void createGameGUI() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        Container window = getContentPane();
        window.setLayout(new FlowLayout());

        // draw panel with white background
        drawPanel = new JPanel();
        drawPanel.setPreferredSize(new Dimension(W, H));
        drawPanel.setBackground(Color.YELLOW);
        window.add(drawPanel);

        // Create Start button
        startBtn = new JButton("start");
        window.add(startBtn);
        startBtn.addActionListener(this);
        // Create Fire button
        fireBtn = new JButton("Fire");
        window.add(fireBtn);
        fireBtn.addActionListener(this);

        // Create Quit button
        quitBtn = new JButton("Quit");
        window.add(quitBtn);
        quitBtn.addActionListener(this);

        //Create Level Label
        levelLabel = new JLabel("Level: ");
        window.add(levelLabel);
        //initialise level to 1
        levelText = new JLabel("1");
        window.add(levelText);

        // Create Score label with the label "Score : "
        scoreLabel = new JLabel("Score : ");
        window.add(scoreLabel);

        // Initial score label value with 0 point
        scoreText = new JLabel("0");
        window.add(scoreText);

        //create Total Score label with the lable "Total Score: "
        totalScoreLabel = new JLabel("Total Score: ");
        window.add(totalScoreLabel);
        // Initial score label value with 0 point
        totalScoreText = new JLabel("0");
        window.add(totalScoreText);

        // Create Time label with the label "Score : "
        timeLabel = new JLabel("Time : ");
        window.add(timeLabel);

        // Initial time label value with 40
        timeText = new JLabel("40");
        window.add(timeText);

        //create Shot left label
        shotsLabel = new JLabel("Nuber of Shots left: ");
        window.add(shotsLabel);
        //Initialise shots Label
        shotText = new JLabel("22");
        window.add(shotText);

    }

    public void actionPerformed(ActionEvent event) {

        if (event.getSource() == fireBtn) {
            fireButtonActionPerform(event);
        }

        if (event.getSource() == startBtn) {
            startButtonActionPerform(event);
        }

        if (event.getSource() == quitBtn) {
            quitButtonActionPerform(event);
        }
    }

    public void fireButtonActionPerform(ActionEvent event) {
        // if Fire button is pressed, create a new thread to fire missile
        // YOUR CODES HERE
        gm.fire();

    }

    public void startButtonActionPerform(ActionEvent event) {
        // if Start button is clicked, create a new thread to start enemy moving
        // target
        // YOUR CODES HERE
        gm.start();
    }

    public void quitButtonActionPerform(ActionEvent event) {
        gm.quit();
    }

    public void setGm(GameManager gm) {
        this.gm = gm;
    }

    public static void updateScore() {
        scoreText.setText(Integer.toString(Integer.parseInt(scoreText.getText()) + 1));
    }

    public static void updateTotalScore() {
        totalScoreText.setText(Integer.toString(Integer.parseInt(totalScoreText.getText()) + 1));
    }

    public static boolean levelup() {
        boolean check = false;
        if (scoreText.getText().equals("5")) {
            scoreText.setText("0");
            levelText.setText(Integer.toString(Integer.parseInt(levelText.getText()) + 1));
            check = true;
        }
        return check;
    }

    public static void decreaseTime() {
        if (timeText.getText().equals("0")) {

        } else {
            timeText.setText(Integer.toString(Integer.parseInt(timeText.getText()) - 1));
        }
    }

    public static void decreaseShots() {
        if (shotText.getText().equals("0")) {

        } else {
            shotText.setText(Integer.toString(Integer.parseInt(shotText.getText()) - 1));
        }

    }
    }

   


  
