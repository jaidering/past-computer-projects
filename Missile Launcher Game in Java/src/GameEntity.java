
import java.awt.Color;
import java.awt.Graphics;

/*
 * 
 * @author jai
 */
public abstract class GameEntity extends Thread {
    //attributes
    public int xpos;
    public int ypos;
    private int width;
    private int height;
    private Color color;

    //constructor
    public GameEntity() {
        xpos = 0;
        ypos = 0;

    }
    ///overloaded constructor
    public GameEntity(int xpos, int ypos, int width, int height, Color color) {
        this.xpos = xpos;
        this.ypos = ypos;
        this.width = width;
        this.height = height;
        this.color = color;
    }
    //methods
    public int getXPos() {
        return xpos;
    }

    public int getYPos() {
        return ypos;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void moveX(int dx) {
        xpos += dx;
    }

    public void moveY(int dy) {
        ypos += dy;
    }

    public void drawRect() {
        Graphics graphics = GameApp.drawPanel.getGraphics();
        graphics.setColor(color);
        graphics.fillRect(xpos, ypos, width, height);
    }

    public void drawSquare() {
        Graphics graphics = GameApp.drawPanel.getGraphics();
        graphics.setColor(color);
        graphics.fillRect(xpos, ypos, width, height);

    }

    public void delete() {
        Graphics graphics = GameApp.drawPanel.getGraphics();
        graphics.setColor(Color.YELLOW);
        graphics.fillRect(xpos, ypos, width, height);
    }

    public void delay(int ms) {

        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            System.out.println("The thread is busy.");

        }
    }

    public void setXPos(int XP) {
        this.xpos = XP;
    }

    public void setYPos(int YP) {
        this.ypos = YP;
    }
}
