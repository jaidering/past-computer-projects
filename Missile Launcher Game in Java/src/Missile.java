
import java.awt.Color;

public class Missile extends GameEntity {
    //attributes
    private Enemy movingTgt;
    private Shots numShot;
    private Bombs bomb;
    private Time time;
    

    //constructor
    public Missile() {
        super(110, 180, 5, 15, Color.RED);
        this.movingTgt = movingTgt;
        this.bomb = bomb;
        numShot = new Shots();
        bomb = new Bombs();
        time = new Time();
    }

    //overloaded constructor
    public Missile(int xpos, int ypos, int width, int height, Color color, Enemy tgt, Bombs bombz) {
        super(xpos, ypos, width, height, color);
        this.movingTgt = tgt;
        numShot = new Shots();
        this.bomb = bombz;
    }

    public void run() {
        while (getYPos() >= 0) {
            moveY(-1);
            drawRect();
            delay(10);
            delete();
            checkCollisionEnemy();
            checkCollisionBomb();
        }

    }

    public void updateScore() {
        GameApp.updateScore();
        GameApp.updateTotalScore();
        if (GameApp.levelup()) {
            movingTgt.setSpeed(2);
            bomb.setSpeed(1);
            numShot.LevelUp();
            
        }
    }

    public boolean checkCollisionEnemy() {
        boolean status = false;
        if ((Math.abs(super.getXPos() - movingTgt.getXPos()) < 22 && Math.abs(super.getYPos() - movingTgt.getYPos()) < 10)) {

            movingTgt.reset();
            status = true;
            updateScore();

        }
        return status;
    }

    public boolean checkCollisionBomb() {
        boolean check = false;
        if ((Math.abs(super.getXPos() - bomb.getXPos()) < 10 && Math.abs(super.getYPos() - bomb.getYPos()) < 10)) {

            bomb.reset();
            check = true;
            updateScore();

        }
        return check;
    }
    }
