import java.io.*; 
 
public class WriteFile { 
  public static void main(String[] args) { 
    try {
       File outputFile = new File("output.txt");   
       FileWriter out = new FileWriter(outputFile); 
       out.write("Writing to output file"); 
       out.close(); 
			}
    catch (IOException ie) {
 	  System.out.println("Error writing to file!");
       System.exit(0);
    }
  } 
} 
