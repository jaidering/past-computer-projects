import java.io.*;

public class TestFileOutput
{
	public static void main(String[] args)
	{			
	try{
		File outputFile =new File("out.txt");
		FileWriter writer = new FileWriter(outputFile);
		PrintWriter pw = new PrintWriter(writer);
		
		pw.print("Hello! \nI am a Java student.");
      pw.close();
      writer.close();

		}
		catch(IOException ie){
		System.out.println("Fail to print");
		}
	}
}
