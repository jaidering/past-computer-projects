import java.io.*;
import java.util.Scanner;
 
public class Scan {
  public static void main(String[] args) {
    try {
      File file = new File("day.txt");
      Scanner sc = new Scanner(file);    
        
      while(sc.hasNextLine()) {
        String line = sc.nextLine();
        System.out.println("Day: " + line);
      }
      sc.close();
    }
    catch (FileNotFoundException e) {
      System.out.println("Error opening file!");
    }  
  }
}
