import java.io.*; 
public class CopyFileContent { 
  public static void main(String[] args) { 
    try {
       
       File inputFile = new File("orig.txt"); 
       File outputFile = new File("new.txt");   
      
	   FileReader in = new FileReader(inputFile);   
       FileWriter out = new FileWriter(outputFile); 
     
       
       int c; 
       while ((c = in.read()) != -1) 
         out.write(c); 
 
     
       in.close(); 
       out.close(); 
    }
    catch (IOException ie) {
 	   System.out.println("Error copying files!");
       System.exit(0);
    }
  } 
} 
