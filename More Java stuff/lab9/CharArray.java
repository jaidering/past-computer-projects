import java.util.Scanner;  
public class CharArray{
  public static void main(String [] args){
   try{  
	  Scanner sc = new Scanner (System.in);

     char[] cArray = {'a', 'b', 'c'};
   
     System.out.print("Enter an array index number: ");   
     int index = sc.nextInt(); sc.nextLine();

     System.out.println(cArray[index]);
	  }
	  catch(ArrayIndexOutOfBoundsException e){
	  System.out.println("The index value should be 0 - 2");
	  }
  }
}
