import java.io.*;
public class LineNUmber {
  public static void main(String[] args) {			
    try {
      // this opens the file passed on the command line
LineNumberReader inFile = new LineNumberReader(new FileReader(args[0]));
// now read the rest of the file
String line;
while ( (line = inFile.readLine()) != null){  
  System.out.println(inFile.getLineNumber()+
 ": " + line);
         }
inFile.close();
    }
      catch (FileNotFoundException e) {
         //If the file cannot be found
}
catch (IOException e) {
         //If the File I/O has problems
      }
}
}