import java.util.Scanner;

public class Console {
private Scanner sc;

  public Console() {
    sc = new Scanner(System.in);
}

  public int readInt( ) {
    int toReturn = 0;
// read in an integer value from the user
try{
	toReturn=sc.nextInt();sc.nextLine();
	}
// remember include the try�catch� block to handle the exception
catch(Exception e){
System.out.println("Enter an Integer");
}    
    return toReturn;
  }
}