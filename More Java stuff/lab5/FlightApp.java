   import java.util.Scanner;
    public class FlightApp{
   
       public static void main(String[] args){
      
         Scanner sc=new Scanner(System.in);
      
         Flight f=new Flight("SQ006","H007",100);
      
         String name=f.getAirlineName();
         String flightNum=f.getFlightNum();
         double fare=f.getAirfare();
      
         System.out.println("Airline name is : "+name);
         System.out.println("Flight number is: "+flightNum);
         System.out.println("Air fare is: $"+fare);
      }
   }