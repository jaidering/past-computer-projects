    public class Flight{
   
      private String airlineName;
      private String flightNum;
      private double airFare;
   
       public Flight(String airlineName,String flightNum){
         this.airlineName=airlineName;
         this.flightNum=flightNum;
      }
       public Flight(String airlineName,String flightNum,double airFare){
         this.airlineName=airlineName;
         this.flightNum=flightNum;
         this.airFare=airFare;
      }
   
       public String getAirlineName(){
         return airlineName;
      }
       public void setAirlineName(String name){
         airlineName=name;
      }
       public String getFlightNum(){
         return flightNum;
      }
       public void setFlightNum(String num){
         flightNum=num;
      }
       public double getAirfare(){
         return airFare;
      }
       public void setAirfare(double fare){
         fare=airFare;
      }
   }