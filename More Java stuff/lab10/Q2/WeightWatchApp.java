 import java.util.Scanner;
 import java.io.*;  
    public class WeightWatchApp {
   
       public static void main(String[] args) {
         Character c1 = new Character("Chicken Little", 0.8, 5);
         Character c2 = new Character("Mr Incredible", 2.1, 105);
         Character c3 = new Character("Tarzan", 1.8, 76);
         Character c4 = new Character("Mulan", 1.6, 48);
      
      // ********************************************************
      // 1. Create the array of People object references
      //    and assign the above to the array elements.
      // ********************************************************
         Character[] cArray = new Character[10];
         cArray[0] = c1;
         cArray[1] = c2;
         cArray[2] = c3;
         cArray[3] = c4;
      
      // ********************************************************
      // 2. Create the CartoonStudio object and pass in
      //    an array of Character objects, name as "Disney"
      // ********************************************************
         CartoonStudio cStudio = new CartoonStudio(cArray, "Disney");
      
      
      // ********************************************************
      // 3. Remove the Character with the name "Tarzan" from 
      //    the CartoonStudio object 
      // ********************************************************
         cStudio.removeCharacter("Tarzan");
      
      // ********************************************************
      // 4. Create a new Character object with "Brother Bear" as 
      //    the name, 3.2 as the height, 250 as the weight and
      //    add it to the CartoonStudio object
      // ********************************************************	
         Character cTemp = new Character("Brother Bear", 3.2, 250);
      
         cStudio.addCharacter(cTemp);
      
      // ********************************************************
      // 5. Retrieve the array from CartoonStudio and
      //    assign it to the disneyChars array
      // ********************************************************		
         Character[] disneyChars = cStudio.getCharacters();
        
         System.out.println("Let's display the final array ");
      // ********************************************************
      // 6. Traverse the array to display the current array
      //    after adding the Character object with "Brother Bear"
      //    as the name, height as 3.2 and weight as 250kg
      // ********************************************************
         if (disneyChars!=null) {
            for (int i = 0; i < disneyChars.length; i++) {
               if (disneyChars[i]!=null) {
                  System.out.println("Name: " + disneyChars[i].getName());
                  System.out.println("BMI: " + disneyChars[i].getBMI());
                  System.out.println();
               }  // end if (checking null for array elements)
            } // end for
         }  // end if (checking null for disneyChars)
      
   	   try{   
      	cStudio.addCharacters("characters.txt");
			String newinfo=cStudio.toString();
			System.out.println(newinfo);
			}
			catch(FileNotFoundException e){
			System.out.println("File not found");
			}
   }
   
   }