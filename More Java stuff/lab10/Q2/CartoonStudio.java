 import java.util.Scanner;
 import java.io.*;   
	 public class CartoonStudio {
   
      private Character[] characters;
      private String name;

   		
		 public	CartoonStudio(String filename)throws FileNotFoundException{
			File f = new File (filename);

		 } 
       public CartoonStudio(Character[] characters, String name) {
         this.characters = characters;
         this.name = name;
      }
      
       public String getName() {
         return name;
      }
   
       public Character[] getCharacters() {
         return characters;
      }
   
   //*****************************************
   // Q2b (i)
   // Implement removeCharacter() method
   //*****************************************
       public boolean removeCharacter(String name) {
         boolean success = false; // informs whether the character has been removed
         for (int i = 0; i < characters.length; i++) {
            if (characters[i]!=null && characters[i].getName().equals(name)) {
               characters[i] = null;  // assign null to the array element
               success = true;
            }
         }
      
      
         return success;
      }
   
   //*****************************************
   // Q2b (ii) 
   // Implement addCharacter() method
   //*****************************************
       public boolean addCharacter(Character c) {
         boolean success = false;  // informs whether the character has been added.
      
         for (int i = 0; i < characters.length; i++) {
            if (characters[i]==null) {
               characters[i] = c;
               success = true;  // change success to true if there is a space
               break;
            }
         }
      		
         return success;
      }
		//New question add from file characters
			public boolean addCharacters(String fileName)throws FileNotFoundException{
   		boolean check=false; 		
			
      			int i=0;
        			 File f = new File (fileName);
         		 Scanner readFileSC = new Scanner(f);
        			 while ((readFileSC.hasNextLine())){
          		  String name = readFileSC.next();
         
          		  double height = readFileSC.nextDouble(); 
           			 double weight = readFileSC.nextDouble();
          			characters[i] = new Character (name, height, weight);
          			 i++;
						  check=true;
        				 }
        				 return check;
      }
			
   
   
   // Q3.
       public Character[] getOverWeightCharacters() {
         int count = 0;
         Character[] tempCharacters = null;  // initialise to null
         if (characters!=null) {
            for (int i = 0; i < characters.length; i++) {
               if (characters[i]!=null) {
                  if (characters[i].isOverWeight()==true) {
                     count++;
                  }
               }  // end if (check null for array elements)
            } // end for
         } // end if (check null for characters)
      
         int index = 0;  // index for the new array
         if (count > 0) {
            tempCharacters = new Character[count];
            for (int i = 0; i < characters.length; i++) {
               if (characters[i]!=null) {
                  if (characters[i].isOverWeight()==true) {
                     if (index < tempCharacters.length) {
                        tempCharacters[index] = characters[i];
                        index++;
                     }
                  }  // end if (check for Overweight characters)
               }  // end if (check for null for characters)
            }  // end for
         } // end if (check for count > 0)
      
         return tempCharacters;  // this may be null				
      }
      
   ///**********toString method**********//	
       public String toString(){
         String info = "Cartoon Studio: "+ name+ "\n";
      
         if (characters!=null) {
            for (int i = 0; i < characters.length; i++) {
               if (characters[i]!=null) {
                  info += "Name: " + characters[i].getName()+"\n";
                  info += "BMI: " + characters[i].getBMI() + "\n\n" ;
                  System.out.println();
               }  // end if (checking null for array elements)
            } // end for
         }  // end if (checking null for disneyChars)
         return info;
      
      }
   	
      
   }