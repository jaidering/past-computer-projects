   import java.util.Scanner;
   import java.io.*;
    public class ReadCharatersApp{
       public static Character[] readCharacterFromFile(String fileName) throws FileNotFoundException{
         Character[] ch = new Character[10];
      
         File f = new File (fileName);
         Scanner readFileSC = new Scanner(f);
         int i =0;
         while ((readFileSC.hasNextLine()) &&(i<9)){
            String name = readFileSC.next();
         
            double height = readFileSC.nextDouble(); 
            double weight = readFileSC.nextDouble();
            ch[i] = new Character (name, height, weight);
            i++;
         }
         return ch;
      }
   
       public static void main(String[] args){
       
         Scanner sc = new Scanner (System.in);
         System.out.print("Please enter the file name:");
         String fileName = sc.nextLine();
         try{
            Character[] characters = readCharacterFromFile(fileName);
            for (int i = 0; i < characters.length; i++){
               if (characters[i] != null){
                  System.out.println(characters[i].getName());
               }
            }
         } 
             catch(FileNotFoundException e){
               System.out.print("File "+ fileName +" is not found");
            }
      } 
   }
