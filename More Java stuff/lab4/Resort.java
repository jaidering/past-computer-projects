    public class Resort{
      private String name;
      private int numOfRooms;
      private boolean isFull;
      private double chargePerNight;
   
       public Resort(String name,int numOfRooms,boolean isFull,double chargePerNight){
         this.name=name;
         this.numOfRooms=numOfRooms;
         this.isFull=isFull;
         this.chargePerNight=chargePerNight;  
      }
       public Resort(String name,int numOfRooms){
         this.name=name;
         this.numOfRooms=numOfRooms;
         chargePerNight=100;
         isFull=false;
      }	
   
       public String getName(){
         return name;
      }
       public int getNumOfRooms(){
         return numOfRooms;
      }
       public boolean checkAvailability(){
         return isFull;
      }
       public double getChargePerNight(){
         return chargePerNight;
      }
       public void setChargePerNight(double price){
         chargePerNight=price;
      }
       public String toString(){
         String info = "Name: " + name + "\nNumber Of Rooms: " + numOfRooms + "\nRoom Availability: "+ isFull+"\nCharges per night: " +chargePerNight;
         return info;
      }
       public String toString(String language){
         String info = "Name: " + name + "\nNumber Of Rooms: " + numOfRooms + "\nRoom Availability: "+ isFull+"\nCharges per night: " +chargePerNight+"\nLanguage is: "+language;
         return info;
      }
   }