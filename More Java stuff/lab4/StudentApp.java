    public class StudentApp{
   
       public static void main(String[] args){
      
         Student a=new Student("Joe Lim","Creative Writing",'A');
         a.computeGrade(67.5);
      
         String allString=a.toString();
         System.out.println(allString);
      
         Student b=new Student("Kelly Pek","C# programming",'A');
         b.computeGrade("Pass");
      
         allString=b.toString();
         System.out.println(allString);
      
      }
   }