    public class Student{
      private String name;
      private String subject;
      private char grade;
   
       public Student(String name,String subject,char grade){
         this.name=name;
         this.subject=subject;
         this.grade=grade;
      }
       public void computeGrade(double marks){
         if(marks>=80){
            grade='A';
         }
         if(marks>=70){
            grade='B';
         }
         if(marks>=60){
            grade='C';
         }
         if(marks>=50){
            grade='D';
         }
         if(marks<50){
            grade='F';
         }
      }
       public void computeGrade(String result){
         if(result.equals("Pass"))
            grade='P';
         else 
            grade='F';
      }
       public String toString(){
         String info = "Name: " + name + "\nSubject: " + subject+"\nGrade: "+grade;
         return info;
      }
   }