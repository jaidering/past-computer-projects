//Part 1 : Solve the compile errors
    public class SolveErr3 {
       public static void main(String[] args) {
      
         int testscore = 76;
         char grade = 'C';
      
         if (testscore >= 90) {
            grade = 'A';
         } 
         else if (testscore >= 80) {
            grade = 'B';
         } 
         else if (testscore >= 70) {
            grade = 'C';
         } 
         else if (testscore >= 60) {
            grade = 'D';
         } 
         else if (testscore >= 50) {
            grade = 'E';
         }
			else
			   grade = 'F';
         
      	//Part 2: Amend the condition such that Grade is F if testscore 
      	//        is less than 50 only
         System.out.println("Grade = " + grade);
      }
   }
