import java.util.Scanner;
public class Sequence{
	public static void main(String[] args){
         Scanner sc = new Scanner(System.in);
         System.out.println("Enter a list of nonnegative scores.");
         System.out.println("Mark the end with a negative number.");
         System.out.println("I will compute the number of odd,even and the total amount of numbers entered");
         int next, numOfEven = 0, numOfOdd=0, numTotal=0; 
				do{
					System.out.println("Enter your number:");
					next=sc.nextInt();sc.nextLine();
						if(next>=0){
							if(next%2>0){
								numOfOdd++;
								numTotal++;
							}
							else{
								numOfEven++;
								numTotal++;
							}
						}	
			}while(next!=-1);
				System.out.println("The number of odd numbers is: "+numOfOdd);
				System.out.println("The number of even numbers is: "+numOfEven);
				System.out.println("The total amount of numbers entered is: "+numTotal);  
	}
}

 
