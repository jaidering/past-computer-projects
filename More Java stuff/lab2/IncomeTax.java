
import java.util.Scanner;
public class IncomeTax{
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        double netIncome, fivePercentTax, tenPercentTax;
		  double tax =0.0;
        System.out.println("Enter net income.\n"
                      + "Do not include a dollar sign or any commas.");
        netIncome = keyboard.nextDouble( );
		  
		  if (netIncome>=15000 && netIncome<30000){
		  	fivePercentTax=netIncome/100*5;
		  	tax=tax+fivePercentTax;
		  }
		  
		  if (netIncome>=30000){
			tenPercentTax=netIncome-30000;
			tenPercentTax=tenPercentTax/100*10;
			fivePercentTax=29999/100*5;
			tax=tenPercentTax+fivePercentTax;
		  }  
        System.out.printf("Tax due = $%.2f", tax);
    }
}





