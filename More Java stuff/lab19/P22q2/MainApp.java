public class MainApp {
	
	public static void main(String[] args) {
		Vehicle v1 = new Vehicle();
		Vehicle v2 = new Car();
		Vehicle v3 = new Train();
		Vehicle v4 = new SportsCar();
		Vehicle v5 = new BulletTrain();
		
		/* 
			1. Create a Vehicle reference to store a BulletTrain object
				
			2. Put the above objects into an array.
			
			3. Traverse the array and call the startEngine method for each object in the array		
		*/
		
			Vehicle[] arrayVehicle= new Vehicle[5];
			arrayVehicle[0]=v1;
			arrayVehicle[1]=v2;
			arrayVehicle[2]=v3;
			arrayVehicle[3]=v4;
			arrayVehicle[4]=v5;
			
			for(int i=0;i<arrayVehicle.length;i++){
			 arrayVehicle[i].startEngine();
				
			
			
			
			}
		
	}
}