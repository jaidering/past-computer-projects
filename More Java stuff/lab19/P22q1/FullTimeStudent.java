public class FullTimeStudent extends Student {
	
	private String careGrp;
	
	public FullTimeStudent(String name, int age, String adminNo, String careGrp) {
		super(name, age, adminNo);
		this.careGrp = careGrp;
	}

	
	public String getCareGrp() {
		return careGrp;
	}
	
	public String toString() {
		String info = super.toString()
						  + "\nCare Group: " + careGrp;
		return info;
	}
	
	public int getCount(FullTimeStudent s) {
		System.out.println("FullTimeStudent!");
		return 1;
	}

}