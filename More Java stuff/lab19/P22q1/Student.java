public class Student extends Person {
	
	private String adminNo;
	
	public Student(String name, int age, String adminNo) {
		super(name, age);
		this.adminNo = adminNo;
	}
	
	public String getAdminNo() {
		return adminNo;
	}
	
	
	public void setAdminNo(String a) {
		adminNo = a;
	}

	public String toString() {
		String info = super.toString()		
						  + "\nAdmin No: " + adminNo;
		return info;
	}	
}