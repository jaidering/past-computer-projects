public class PartTimeStudent extends Student {
	
	private String courseType;  // can be diploma course or just a short course
	
	public PartTimeStudent(String name, int age, String adminNo, String courseType) {
		super(name, age, adminNo);
		this.courseType = courseType;
	}
	
	public String courseType() {
		return courseType;
	}

	public String toString() {
		String info = super.toString()
						  + "\nCourse Type: " + courseType;
		return info;
	}	
}