import java.util.Scanner;

public class SchoolApp {
	
	public static void main(String args[]) {
		Student stud = new Student("Fiona", 21, "S5566");
		
		FullTimeStudent fullStud = new FullTimeStudent("James Ng", 18, "S0011", "C001");
		
		PartTimeStudent partStud = new PartTimeStudent("Farhana", 19, "P2233", "short course");
		
		Person p = stud;  // eg. Upcasting a student to a Person object
		// i. Create Student object called tempS1 and upcast fullStud to Student type	
		
		Student tempS1 = new Student ("Wong",16,"S12421");
		tempS1=fullStud;
		// ii. Create a FullTimeStudent called fts1 and downcast tempS1 to a FullTimeStudent type
		FullTimeStudent fts1= new FullTimeStudent(" Ng", 16, "S0021", "C002");
		fts1=(FullTimeStudent)tempS1;
		// iii. Create a PartTimeStudent called partStud and upcast a partStud to a Student type
		tempS1=partStud;
		
		System.out.println(fullStud.toString());
		System.out.println(partStud.toString());
		System.out.println(tempS1.toString());
		System.out.println(fts1.toString());
	
	}
	
}