public class School {

	private Student[] studentsArray;
	private String name;
	
	public School(String name, Student[] studentsArray) {
		this.name = name;
		this.studentsArray = studentsArray;
	}
	
	public boolean addStudent(Student student) {
		boolean flag = false;
		
		if (studentsArray!=null) {
			for (int i = 0; i < studentsArray.length; i++) {
				if(studentsArray[i]==null) {
					studentsArray[i] = student;
					flag = true;
					break;
				}
			}
			
		}
		
		return flag;
	}
	
	public Student[] getAllStudents() {
		return studentsArray;
	}
}