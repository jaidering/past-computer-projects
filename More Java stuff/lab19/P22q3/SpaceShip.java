public class SpaceShip {

	private String name;
	
	public SpaceShip()
	{
		name = "Odyssey 113";
	}
	
	public SpaceShip(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
}