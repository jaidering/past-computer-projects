public class GalaxyApp {

	public static void main(String[] args)
	{
		SpaceShip s1 = new WingFighter("WingFighter", 900);
		SpaceShip s2 = new DropShip("DropShip", "elephant");
		SpaceShip s3 = new FlagShip("FlagShip", "white");
		
		
		//1. Explicitly cast s1 to an appropriate object type and call the shoot() method
		   WingFighter wf;
			wf=(WingFighter)s1;
			wf.shoot();
		//2. Explicitly cast s2 to an appropriate object type and call the drop() method
			
			DropShip ds;
			ds=(DropShip)s2;
			ds.drop();
		//3. Explicitly cast s3 to an appropriate object type and call the flag() method
			
			FlagShip fs;
			fs=(FlagShip)s3;
			fs.flag();
	}
}