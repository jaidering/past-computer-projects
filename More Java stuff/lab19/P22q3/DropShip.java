public class DropShip extends SpaceShip {
	
	private String item;
	
	public DropShip (String name, String item)
	{
		super(name);
		this.item = item;
	}
	
	public void drop() {
		System.out.println(getName() + " have dropped " + item);
	}
}