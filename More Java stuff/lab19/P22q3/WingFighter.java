public class WingFighter extends SpaceShip {
	
	private int rounds;
	
	public WingFighter(String name, int rounds)
	{
		super(name);
		this.rounds = rounds;
	}
	
	public void shoot()
	{
		System.out.println(getName() + " has shot " + rounds + " rounds.");
	}
}