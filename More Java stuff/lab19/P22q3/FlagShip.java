public class FlagShip extends SpaceShip {
	
	private String flagColor;
	
	public FlagShip(String name, String flagColor)
	{
		super(name);
		this.flagColor = flagColor;
	}
	
	public void flag()
	{
		System.out.println(getName() + "'s flag is " + flagColor);
	}
}