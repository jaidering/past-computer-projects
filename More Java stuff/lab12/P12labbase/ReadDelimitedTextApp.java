   import java.io.*;
   import java.util.Scanner;
    public class ReadDelimitedTextApp{
       public static void main(String[] args){
      
         Scanner sc = new Scanner(System.in);
      
         System.out.print("Enter a delimited text string : ");
         String text = sc.nextLine();
      
      	//Create scanner object to scan the text string
         //...YOUR CODES HERE	
         
      	
      	//Inform scanner to use comma delimiter
         //...YOUR CODES HERE
         
      	
         //Read first string data
         //...YOUR CODES HERE 
         
      	
         //Read next number data
         //...YOUR CODES HERE 
      
      
         //Read next boolean data
         //...YOUR CODES HERE
         
         System.out.println();
         
      	//output data read using println() method
         //...YOUR CODES HERE
      
      
      }
   }