public class RatingApp{
	public static void main(String[] args){
	
	Movie mv=new Movie(5);
	Aircon ac=new Aircon(5);
	
	int mvRating=mv.computeRating();
	int acRating=ac.computeRating();
	
	System.out.println("Rating of Movie is: "+mvRating);
	System.out.println("Rating of Aircon is: "+acRating);
	
	}
}	