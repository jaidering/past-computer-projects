public class TrafficOffence extends Offence implements Payable{
	private String vehicleNo;
	private double fine;
	private int factor;
	
	public TrafficOffence(String date,String vehicleNo,double fine,int factor){
	super(date);
	this.vehicleNo=vehicleNo;
	this.fine=fine;
	this.factor=factor;
	}
	
	public String getVehicleNo(){
	return vehicleNo;
	}
	
	public void setFine(double newFine){
	fine=newFine;
	}
	
	public double computePayment(){
	double payment=fine*(1+(factor/10.0));
	return payment;
	}
}	