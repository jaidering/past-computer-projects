public class Aircon implements Rankable,Comfortable{
	private String brand;
	private int rating;
	
	public Aircon(int rating){
	this.rating=rating;
	}
	
	public int computeRating(){
	int newRating=rating*30;
	return newRating;
	}
	
	public int adjustComfortLevel(){
	return 10;
	
	}
}	