public class Movie implements Rankable{
	private String title;
	private int rating;
	
	public Movie(int rating){
	this.rating=rating;
	}
	
	public String getTitle(){
	return title;
	}
	
	public int computeRating(){
	int newRating=rating*3;
	return newRating;
	}
}	