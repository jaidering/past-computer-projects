public interface Payable{
	public double computePayment();
	}