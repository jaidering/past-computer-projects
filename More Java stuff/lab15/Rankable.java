public interface Rankable{

	public int computeRating();
	
	}