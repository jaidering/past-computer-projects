   import java.awt.*;
   import java.awt.event.*;
   import javax.swing.*;
   import java.util.*;

    public class ShoppingApp extends JPanel implements ActionListener {
   
      private JTextField textField1, textField2, textField3;
      private JButton addBtn, remvBtn, searchBtn, replaceBtn;
      private JTextArea textArea;
      private JLabel inputLabel, replaceLabel, statusLabel;
      
    	//Declare an arrayList of String variable - shopList  
   	//...YOUR CODES HERE
   	ArrayList<String> shopList;
   
       public ShoppingApp() {
         setPreferredSize (new Dimension(330, 220));
         setBackground (Color.cyan);
         
         inputLabel = new JLabel("Search Item : ");
         add(inputLabel);
         
         textField1 = new JTextField(20);
         add(textField1);
                  
         replaceLabel = new JLabel("Replace Item : ");
         add(replaceLabel);
         
         textField2 = new JTextField(20);
         add(textField2);
      
         addBtn = new JButton("Add");
         add(addBtn);
         addBtn.addActionListener(this);
      
      
         searchBtn = new JButton("Search");
         add(searchBtn);
         searchBtn.addActionListener(this);
      
         
         replaceBtn = new JButton("Replace");
         add(replaceBtn);
         replaceBtn.addActionListener(this);
         
      
         remvBtn = new JButton("Remove");
         add(remvBtn);
         remvBtn.addActionListener(this);
         
         statusLabel = new JLabel("Status :                                   ");
         add(statusLabel);
         
         textArea = new JTextArea(5, 25);
         add(textArea);
         
      	//Create arrayList of String variable and assigned to shopList variable
      	//...YOUR CODES HERE
      	shopList=new ArrayList<String>();
      }
   
       public void actionPerformed(ActionEvent event) {
         String textString = "";
         String replaceTextString = "";
         boolean itemFound = false;
         
         if (event.getSource() == searchBtn)	 {
         	//get text from search text field input
            textString = textField1.getText();
            
         	//invoke the searchItem() method with textString as argument
         	//If the item is found, display status as �Item found!�,
         	//otherwise display status as �Item not found!�.
         	//...YOUR CODES HERE
         	boolean success=searchItem(textString);
				if(success==true){
				statusLabel.setText("Status : Item found");
				}else{
				statusLabel.setText("Status : Item not found");
				
				}
         	displayShopList();
         	
         }
         else if (event.getSource() == addBtn)	 {
         	//get text from search text field input
            textString = textField1.getText();
            
         	//invoke the searchItem() method with textString as argument
         	//If item is found, display status �Item already exists, cannot be added�,
         	//If item does not exist, add the item into shopList and display 
         	//status �Item successfully added!�.
         	//...YOUR CODES HERE
   			boolean success=searchItem(textString);
				if(success==true){
				statusLabel.setText("Status : Item already exists, cannot be added");
				displayShopList();
				}else{
				shopList.add(textString);
				statusLabel.setText("Status : Item successfully added");
				displayShopList();
				
				}      
			
			
         }
         
         else if (event.getSource() == replaceBtn)	 {
         	//get text from search and replace text fields input
            textString = textField1.getText();
            replaceTextString = textField2.getText();
            
         	//traverse the shopList to search for item that matches the textString.  
           	//If item is found, update the item with the replaceTextString 
           	//using the set() method in ArrayList class and display 
           	//status �Item successfully replaced�.  
         	//Otherwise display status as �Item not found!�.  
           	//...YOUR CODES HERE
				
         	for(int i=0;i<shopList.size();i++){
					if(shopList.get(i).equalsIgnoreCase(textString)){
						shopList.set(i,replaceTextString);
						System.out.println("Item successfully replaced");
						}
					else{
						System.out.println("Item not found!");
					}
				}
				displayShopList();
                  
         }
         
         else if (event.getSource() == remvBtn)	 {
         	//get text from search text field input
            textString = textField1.getText();
            
         	//invoke the searchItem() method and pass textString as argument
         	//If the item is found, remove the item from shopList and 
         	//display status �Item successfully removed!�,
         	//otherwise display status  �Item not found!�.  
				//...YOUR CODES HERE
				boolean success=searchItem(textString);
					for(int i=0;i<shopList.size();i++){
					if(shopList.get(i).equalsIgnoreCase(textString)){
					shopList.remove(textString);
	
					}
	
				}

				
				if(success==true){
					System.out.println("Item successfully removed!");
				}
				else{
					System.out.println("Item not found!");
					}
				displayShopList();
				
         }      
      }
      
       public void displayShopList() 	 {
         String newLine = "\r\n";
         String str = "";
         textArea.setText("");
         
      	//Traverse shopList arraylist and retrieve each item in arraylist
      	//Assign it to variable str
      	//...YOUR CODES HERE
      	for(int i=0;i<shopList.size();i++){
				str=shopList.get(i);
				textArea.append(str + newLine); 
			
			}
          
      }
         
   	//Write searchItem() method - The method will take in a String parameter
   	//and traverse the shopList to search for matching item (ignoring case).
   	//If the item is found, the search stops and the method returns true,
   	//otherwise method returns false.
   	//...YOUR CODES HERE
   	public boolean searchItem(String text){
			boolean check=false;
				for(int i=0;i<shopList.size();i++){
					if(shopList.get(i).equalsIgnoreCase(text)){
					
					check=true;
					}
	
				}
			return check;
		}
   
   }
		