   import java.awt.*;
   import java.awt.event.*;
   import javax.swing.*;
   import java.util.*;

    public class BookApp extends JPanel implements ActionListener 
   {
      private JTextField textField1, textField2, textField3;
      private JButton addBtn, remvBtn, searchBtn, updateBtn, thickBtn;
      private JTextArea textArea;
      private JLabel statusLabel, label1, label2, label3, label4, label5;    
      
   
      private ArrayList<Book> bookList;
   	
       public BookApp()
      {
      
         setPreferredSize (new Dimension(330, 300));
         setBackground (Color.cyan);
         
         label1 = new JLabel("Title :        ");
         add(label1);
         textField1 = new JTextField(20);
         add(textField1);
         label2 = new JLabel("Author :        ");
         add(label2);
         textField2 = new JTextField(20);
         add(textField2);
         label3 = new JLabel("Pages :        ");
         add(label3);
         textField3 = new JTextField(20);
         add(textField3);            
               
         addBtn = new JButton("Add");
         add(addBtn);
         addBtn.addActionListener(this);
      
      
         searchBtn = new JButton("Search");
         add(searchBtn);
         searchBtn.addActionListener(this);
      
         
         updateBtn = new JButton("Update");
         add(updateBtn);
         updateBtn.addActionListener(this);
               
         remvBtn = new JButton("Remove");
         add(remvBtn);
         remvBtn.addActionListener(this);
         
         
         statusLabel = new JLabel("Message :                                         ");
         add(statusLabel);
      
      
         label5 = new JLabel("Title              Author              Pages                         ");
         add(label5);
      
         textArea = new JTextArea(5, 25);
         add(textArea);
      
         thickBtn = new JButton("*Find Thickest Book");
         add(thickBtn);
         thickBtn.addActionListener(this);
               
         bookList = new ArrayList<Book>();
      	
      }
       public void actionPerformed(ActionEvent event) {
      
       
         if (event.getSource() == addBtn)	 {
            String t1 = textField1.getText();	//get title from text field
            String t2 = textField2.getText();	//get author from text field
            String t3 = textField3.getText();	//get numOfPages from text field
            int numPages = Integer.parseInt(t3);	//convert numOfPages to integer
            
            //Invoke searchBook() method with t1 and t2 as argument
         	//If book does not exist (result is null), create a book object 
         	//based on the text field input (t1, t2, numPages)
            //and add the Book object into bookList arraylist 
         	//display status "Book successfully added" and display latest bookList in text area
         	//Otherwise display status �Book already exists�.  
            //...YOUR CODES HERE
         	Book bookExist=searchBook(t1,t2);
         	if(bookExist==null){
					Book b=new Book(t1,t2,numPages);
					bookList.add(b);
					
					statusLabel.setText("Message : Book successfully added");
				}
				else{
					
					statusLabel.setText("Message : Book already exists");
				}
         	displayBookList();
         }
         else if (event.getSource() == searchBtn)	 {	//Search button pressed
            
            String newLine = "\r\n";
            textArea.setText("");			
            String str = "";
            
            String t1 = textField1.getText();	//get title from text field
            String t2 = textField2.getText();	//get author from text field
            
            //Invoke searchBook() method with t1 and t2 as argument
         	//If book is found (result not null), display title, author and numOfPages
         	//separated by tabspace in text area and display status Book Found
         	//Otherwise display status �Book not found�.  
            //...YOUR CODES HERE
				
         Book bookExist=searchBook(t1,t2);
			if(bookExist==null){
			statusLabel.setText("Message : Book not Found");
		
			}else{
			String info="Title: "+bookExist.getTitle()+"\tAuthor: "+bookExist.getAuthor()+"\tNumber of Pages: "+bookExist.getNoOfPages();
         	textArea.append(info);
   		}      
         
            
         		
         displayBookList();
         }
         else if (event.getSource() == remvBtn)	 {	//Remove button pressed
            String t1 = textField1.getText();	//get title from text field
            String t2 = textField2.getText();	//get author from text field
         
          	//Invoke searchBook() method with t1 and t2 as argument
         	//If book is found (result not null), remove the book from bookList 
         	//Display status "Remove successfully" and display latest bookList in text area
         	//Otherwise display status "Book not found"
            //...YOUR CODES HERE
          Book bookExist=searchBook(t1,t2);
			 if(bookExist==null){
			  statusLabel.setText("Message : Book not found");

			 
         }else{
         	for(int i=0;i<bookList.size();i++){
     				if(bookList.get(i).getTitle().equalsIgnoreCase(t1)){
					 bookList.remove(i);
					    	statusLabel.setText("Message : Remove successfully");
					}
				}
         }
			displayBookList();
         }
         else if (event.getSource() == updateBtn)	 {	//Display button pressed
         
            String t1 = textField1.getText();	//get title from text field
            String t2 = textField2.getText();	//get author from text field
            String t3 = textField3.getText();	//get numOfPages from text field
            int numPages = Integer.parseInt(t3);	//convert numOfPages to integer
         
         	//Traverse bookList and get each Book object	
            for (int i = 0; i < bookList.size(); i ++) {
            
              	//Search for matching title and author
            	//If book is found, update the book with new numPages enter by user
            	//Display status "Book pages updated" and display latest bookList in text area
            	//Otherwise display status "Book not found"
               //...YOUR CODES HERE
            	if(bookList.get(i).getTitle().equalsIgnoreCase(t1) && bookList.get(i).getAuthor().equalsIgnoreCase(t2)){
      				bookList.get(i).setNoOfPages(numPages);      	
						statusLabel.setText("Message : Book pages updated");
					
					}else{
						statusLabel.setText("Message : Book not found");
					}
            }
				displayBookList();
         }
         else if (event.getSource() == thickBtn)	 {	//Thickest button pressed
            
            String newLine = "\r\n";
            textArea.setText("");			
            String str = "";
            
            String t1 = textField1.getText();	//get title from text field
            String t2 = textField2.getText();	//get author from text field
            
            //invoke the searchThickestBook () method to search for the thickest book 
         	//in bookList.  If found, display book title, author and noOfPages in text area
         	//(separated by tabspace).  Otherwise display status �Book not found� 
         	//if there are no books in bookList.              
         	//...YOUR CODES HERE
         
          Book bookExist= searchThickestBook();
			 if(bookExist!=null){
         	String info="Title: "+bookExist.getTitle()+"\tAuthor: "+bookExist.getAuthor()+"\tNumber of Pages: "+bookExist.getNoOfPages();
         	textArea.append(info);

         }
            statusLabel.setText("Message : Book Found");
         		
         
         }
      
         //Clear all text fields
         textField1.setText("");
         textField2.setText("");
         textField3.setText("");
      
      }
               
       public void displayBookList() {
         String newLine = "\r\n";
         textArea.setText("");		//clear text area
         String str = "";
         String title ="";
         String author="";
         int numPage = 0;
      	
      	//Traverse bookList and get each Book object	
         for (int i = 0; i < bookList.size(); i ++) {
           	//Retrieve title and author and noOfPages from the Book object
         	//Assign book title to the variable title
         	//Assign book author to the variable author
         	//Assign book noOfPages to the variable numPage
            //...YOUR CODES HERE
            
         	         	
         	
            String numPageStr = Integer.toString(numPage);  //convert to string 	
            str = title + '\t' + author + '\t' + numPageStr;			//display format
            textArea.append(str + newLine);
         }
      }
      
   	//Write a searchBook() method that returns a Book object
   	//The method will take in two String parameters (title and author) and 
   	//traverse the bookList to search for matching book title and author (ignoring case)
   	//If found, the method will return the Book object, otherwise it will return null.
   	//...YOUR CODES HERE
   	public Book searchBook(String title, String author){
			Book b=null;
			for(int i =0;i<bookList.size();i++){
				if(bookList.get(i).getTitle().equalsIgnoreCase(title) && bookList.get(i).getAuthor().equalsIgnoreCase(author)){
					b=bookList.get(i);
   			}
   		}
			return b;
   	}
		
   	//Write a searchThickestBook() method that returns a Book object that has the 
   	//most number of pages.  The method take in no parameter and traverse the bookList 
   	//to search for the book with the most number of pages.  
   	//If found, the method will return the Book object, otherwise it will return null.
   	//...YOUR CODES HERE
   		public Book searchThickestBook(){
   	 		Book b=null;
				int pages=0; 
		  		for(int i=0;i<bookList.size();i++){
		  			if(bookList.get(i).getNoOfPages()>pages){
		  				pages=bookList.get(i).getNoOfPages();
		  				b=bookList.get(i);
		  			}
		  		}
				return b;
		  }	
   }