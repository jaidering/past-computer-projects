   import javax.swing.JFrame;

    public class Q2App
   {
   //-----------------------------------------------------------------
   //  Creates the main program frame.
   //-----------------------------------------------------------------
       public static void main (String[] args)
      {
         JFrame frame = new JFrame ("BookApp");
         frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
      
         frame.getContentPane().add(new BookApp());
      
         frame.pack();
         frame.setVisible(true);
      }
   }
