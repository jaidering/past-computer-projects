   import java.awt.*;
   import java.awt.event.*;
   import javax.swing.*;
   import java.util.*;

    public class ShoppingApp extends JPanel implements ActionListener {
   
      private JTextField textField1, textField2, textField3;
      private JButton addBtn, remvBtn, insertBtn, dispBtn;
      private JTextArea textArea;
      private JLabel statusLabel;
      
      //Declare ArrayList variable named shopList that can hold String objects
      //...YOUR CODES HERE
		
   	ArrayList<String> shopList;
		
       public ShoppingApp() {
         setPreferredSize (new Dimension(300, 250));
         setBackground (Color.cyan);
         
         textField1 = new JTextField(23);
         add(textField1);
                  
         addBtn = new JButton("Add    ");
         add(addBtn);
         addBtn.addActionListener(this);
      
      
         insertBtn = new JButton("Insert First");
         add(insertBtn);
         insertBtn.addActionListener(this);
      
      
         remvBtn = new JButton("Remove");
         add(remvBtn);
         remvBtn.addActionListener(this);
         
         
         statusLabel = new JLabel("Status :                                   ");
         add(statusLabel);
         
         textArea = new JTextArea(5, 18);
         add(textArea);
         
         dispBtn = new JButton("Display");
         add(dispBtn);
         dispBtn.addActionListener(this);
         
      	//Create an arraylist of String objects and assigned 
      	//it to shopList variable
      	//...YOUR CODES HERE
			shopList=new ArrayList<String>();
      }
   
       public void actionPerformed(ActionEvent event) {
         String textString = "";
      	
      	//When Add button is pressed
         if (event.getSource() == addBtn)	 {
            textString = textField1.getText();	//get user input from textfield
            
         	//Add the textString into the shopList arraylist
            //...YOUR CODES HERE
            shopList.add(textString);
				display();
            textField1.setText("");
            statusLabel.setText("Status : Add Successful");
         }
         else if (event.getSource() == insertBtn)	 {	//Insert button pressed
            textString = textField1.getText();
         	//Insert the textString as FIRST item in the shopList arraylist
            //...YOUR CODES HERE
				shopList.add(0,textString);
         	display();
            statusLabel.setText("Status : " + textString + " inserted as first item");
                    
         }
         else if (event.getSource() == remvBtn)	 {	//Remove button pressed
         
         	//Remove the FIRST item in the shopList arraylist
            //...YOUR CODES HERE
         	shopList.remove(0);
				display();
            statusLabel.setText("Status : Remove successful");
				
         
         }
			}
        public void display(){
		   String newLine = "\r\n";
            String str = "";
            textArea.setText("");
            
         	//Traverse the shopList arraylist and obtain each string value
         	//...YOUR CODES HERE
         	for(int i=0;i<shopList.size();i++){
				str=shopList.get(i);
				
				//Put str into the text area
         	//include this line in your <for> loop
            textArea.append(str + newLine);
				
				}
				}
         
         	              
         }
      	

   
   

