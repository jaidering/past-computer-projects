   import java.util.*;
    public class Book
   {
      private String title;
      private String author;
      private int noOfPages;
   
   
       public Book(String title, String author, int noOfPages) {
         this.title = title;
         this.author = author;
         this.noOfPages = noOfPages;
      	
      }
       public String getTitle( ) {
         return title;
      }
       public String getAuthor( ) {
         return author;
      }
       public int getNoOfPages ( ) {
         return noOfPages;
      }
   
   
   }