   import java.awt.*;
   import java.awt.event.*;
   import javax.swing.*;
   import java.util.*;

    public class BookApp extends JPanel implements ActionListener 
   {
      private JTextField textField1, textField2, textField3;
      private JButton addBtn, remvBtn, insertBtn, dispBtn;
      private JTextArea textArea;
      private JLabel statusLabel, label1, label2, label3, label4, label5;    
      
   	//Declare ArrayList variable named bookList that can hold Book objects
      //...YOUR CODES HERE
   	ArrayList<Book> bookList;
		
       public BookApp()
      {
      
         setPreferredSize (new Dimension(300, 300));
         setBackground (Color.cyan);
         
         label1 = new JLabel("Title :        ");
         add(label1);
         textField1 = new JTextField(15);
         add(textField1);
         label2 = new JLabel("Author :        ");
         add(label2);
         textField2 = new JTextField(15);
         add(textField2);
         label3 = new JLabel("Pages :        ");
         add(label3);
         textField3 = new JTextField(15);
         add(textField3);            
               
         addBtn = new JButton("Add Book");
         add(addBtn);
         addBtn.addActionListener(this);
      
      
         insertBtn = new JButton("Insert First");
         add(insertBtn);
         insertBtn.addActionListener(this);
      
      
         remvBtn = new JButton("Remove");
         add(remvBtn);
         remvBtn.addActionListener(this);
         
         
         statusLabel = new JLabel("Message :                                      ");
         add(statusLabel);
      
         label4 = new JLabel("                                              ");
         add(label4);
         label5 = new JLabel("Display Book List :                              ");
         add(label5);
      
         textArea = new JTextArea(5, 18);
         add(textArea);
         
         dispBtn = new JButton("Display");
         add(dispBtn);
         dispBtn.addActionListener(this);
             
        	//Create an arraylist of Book objects and assigned 
      	//it to bookList variable
      	//...YOUR CODES HERE      	
      	bookList=new ArrayList<Book>();
      	
      }
       public void actionPerformed(ActionEvent event) {
      
       
         if (event.getSource() == addBtn)	 {
            String t1 = textField1.getText();
            String t2 = textField2.getText();
            String t3 = textField3.getText();
            int numPages = Integer.parseInt(t3);
            //Create a book object based on the text field input (t1, t2, numPages)
            //and add the Book object into bookList arraylist
            //...YOUR CODES HERE
         Book b=new Book(t1,t2,numPages);
			bookList.add(b);
         
         
            statusLabel.setText("Message : Add Book Successful");
         }
         else if (event.getSource() == insertBtn)	 {	//Insert button pressed
            String t1 = textField1.getText();
            String t2 = textField2.getText();
            String t3 = textField3.getText();
            int numPages = Integer.parseInt(t3);
            //Create a book object based on the text field input (t1, t2, numPages)
            //and insert the Book object as FIRST item in bookList arraylist
            //...YOUR CODES HERE
         	Book b=new Book(t1,t2,numPages);
				bookList.add(0,b);
            statusLabel.setText("Message : Book inserted as first item");
                    
         }
         else if (event.getSource() == remvBtn)	 {	//Remove button pressed
         
         	//Remove the FIRST item in the bookList arraylist
            //...YOUR CODES HERE
         	bookList.remove(0);
            statusLabel.setText("Message : Remove successful");
         
         }
         else if (event.getSource() == dispBtn)	 {	//Display button pressed
           
			   String newLine = "\r\n";
            String str = "";
            textArea.setText("");
     			for(int i=0;i<bookList.size();i++){       
         	//Traverse the bookList arraylist and concatenate the 
         	//book object title and author (separated by tab space using '\t')
         	//and assign the concatenated string to the variable 'str'
         	//...YOUR CODES HERE
     			str=bookList.get(i).getTitle();
				String author=bookList.get(i).getAuthor();
				int pages=bookList.get(i).getNoOfPages();

         	//Put str into the text area
         	//include this line in your <for> loop
            textArea.append(str + newLine + author + newLine + pages);
				}              
         }
      
         //Clear all text fields
         textField1.setText("");
         textField2.setText("");
         textField3.setText("");
      
      }
   }