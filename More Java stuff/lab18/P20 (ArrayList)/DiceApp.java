  import java.util.*; 
public class DiceApp {
    public static void main (String arg[])  {
	//(a) Create an ArrayList name diceList which can hold Dice 
   //     object reference
	ArrayList<Dice> diceList = new ArrayList<Dice>();


   //(b) Create create three Dice objects d1, d2 and d3 and add them into 
   //     diceList;
     Dice d1= new Dice();
     Dice d2= new Dice(); 
     Dice d3= new Dice();

     diceList.add(d1);
     diceList.add(d2);
     diceList.add(d3);



   //(c) Write codes to print the number of dice objects in 
   //     diceList

    System.out.println("Number of Dice : "+ diceList.size());



   //(d) Write codes to create a new dice object d4 and insert it as 
   //     the 2nd item in diceList.
     Dice d4= new Dice();
     diceList.add(1,d4);



   //(f) Write codes to remove the FIRST dice object from diceList.  

         diceList.remove(1);



   //(h) Write codes to roll and display the face value of ALL the dices
   //     in diceList.

         for(int i=0;i<diceList.size();i++){
                Dice d= diceList.get(i);
                d.roll();
                System.out.println("Dice"+i+"\tFace Value: "+                 
                diceList.get(i).getFaceValue());

			}

    } 	
}
