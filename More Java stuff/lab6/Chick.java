public class Chick{

	private String name;
	private Hen parent;

	public Chick(String name,Hen parent){
	this.name=name;
	this.parent=parent;
	}
	public String getName(){
	return name;
	}
	public String getParentName(){
	String parentName=parent.getName();
	return parentName;
	}
}