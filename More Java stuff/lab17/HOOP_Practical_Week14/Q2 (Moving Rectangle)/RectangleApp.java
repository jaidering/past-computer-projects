   import java.awt.Color;
   import javax.swing.JFrame;
   import javax.swing.JPanel;

    public class RectangleApp
   {
      public static JPanel panel;
      public static final int W = 300;
      public static final int H = 200;
      public static final Color bgColor = Color.WHITE;
   // execute application
       public static void main( String args[] )
      {
      // create frame 
         JFrame frame = new JFrame( "Rectangle Application" );
         frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
         panel = new JPanel();
         panel.setBackground(bgColor);
         frame.add(panel); // add panel to frame
         frame.setBackground(bgColor); // set frame background color
         frame.setSize(W, H); // set frame size
         frame.setVisible(true); // display frame
         RectangleManager rm = new RectangleManager();        
         rm.demo();  
      } 
   } 
