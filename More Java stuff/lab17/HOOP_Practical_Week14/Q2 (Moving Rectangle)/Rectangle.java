   import java.awt.Color;
   import java.awt.Graphics;
    public abstract class Rectangle extends Thread{
      private int xpos;
      private int ypos;
      private int width;
      private int height;
      private Color color;
      
       public Rectangle(int xpos, int ypos, int width, int height, Color color){
         this.xpos = xpos;
         this.ypos = ypos;
         this.width = width;
         this.height = height;
         this.color = color;
      }
       public int getXpos(){
         return xpos;
      }
   
       public int getYpos(){
         return ypos;
      }
       public Color getColor()  {
         return color;
      }
      
       public void draw(){
         Graphics graphics = RectangleApp.panel.getGraphics();
         graphics.setColor(color);
         graphics.fillRect(xpos, ypos, width, height);
      }
      
       public void delete(){
         Graphics graphics = RectangleApp.panel.getGraphics();
         graphics.setColor(RectangleApp.bgColor);
         graphics.fillRect(xpos, ypos, width, height);	
      }
   	
       public void delay(int speed){
         try {
            Thread.sleep(100/speed);
         }
             catch (InterruptedException e)
            {}
      }
       public void moveX(int dx){
         xpos += dx;
      }
       public void moveY(int dy){
         ypos += dy;
      }
   }