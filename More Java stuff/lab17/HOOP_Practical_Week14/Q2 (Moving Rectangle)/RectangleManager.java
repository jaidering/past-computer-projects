    public class RectangleManager{
      public MovingRectangle mr;
		public StationaryRectangle sr;
       public RectangleManager(){
         
			sr = new StationaryRectangle();
      	mr = new MovingRectangle(sr);
      }
       public void demo(){
         mr.start();
			sr.start();
      }
   }