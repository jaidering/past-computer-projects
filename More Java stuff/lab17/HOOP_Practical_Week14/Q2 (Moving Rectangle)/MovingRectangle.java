   import java.awt.Color;
   import java.awt.Graphics;


   import javax.swing.JPanel;

    public class MovingRectangle extends Rectangle{
   
   	//Initial rectangle position
      private static final int SPEED = 5;
      private int dx;
		private StationaryRectangle sr;
       	
   	
    	//Constructor
       public MovingRectangle(StationaryRectangle str) {
         super(5,40, 40, 20, Color.BLUE);
         this.dx = 1;
			sr=str;
              
      }
   	
       public void run() {
         while(true){
            draw();
            delay(SPEED);
            delete();
            moveX(dx);
				checkCollision();
            checkWallBounce();
         }
      }
      
       public void checkWallBounce() {
      
         if ((getXpos() >260) || (getXpos() < 0)){
            dx = -dx;
         }
      }
		
			public void checkCollision(){
			if(sr!=null){
				if(Math.abs(super.getXpos() - sr.getXpos()) < 40){
					dx=-dx;
					sr.hit();
				}
				}
			}

   } 
