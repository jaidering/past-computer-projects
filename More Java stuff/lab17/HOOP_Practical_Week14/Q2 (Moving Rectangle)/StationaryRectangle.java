   import java.awt.Color;
   import java.awt.Graphics;
   import javax.swing.JPanel;

    public class StationaryRectangle extends Rectangle{
   
   	//Initial rectangle position
		private static final int SPEED = 5;
      private int dx;
    	//Constructor
   
       public StationaryRectangle() {
         super(100,40, 20, 40, Color.RED);
      	dx=0;
      }      
   	
		public int getXpos(){
		return super.getXpos();
		}
		public void run() {
         while(true){
            draw();
            delay(SPEED);
            delete();
            moveX(dx);
            checkWallBounce();

         }
      }
		
		public void checkWallBounce() {
      
         if ((getXpos() >260) || (getXpos() < 0)){
            dx = -dx;

         }
      }
		
		public void hit(){
			dx=1;
			}
         
   }