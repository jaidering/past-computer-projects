   import java.awt.*;
   import java.awt.event.*;
   import javax.swing.*;

    public class BallApp extends JFrame implements ActionListener {
   
      private JButton throw1, stop1;
      public static JPanel panel1;
      private BallManager bm;
		private int count=0;
   	
      
       public static void main(String[] args) {
         BallApp frame = new BallApp();
         frame.setSize(200,220);
         frame.createGUI();
         frame.setVisible(true);
      }
      
       public void createGUI() {
         setDefaultCloseOperation(EXIT_ON_CLOSE);
         Container window = getContentPane();
         window.setLayout(new FlowLayout());
      
         throw1 = new JButton("Throw");
         window.add(throw1);
         throw1.addActionListener(this);
      
       
         stop1 = new JButton("Stop");
         window.add(stop1);
         stop1.addActionListener(this);
         
         panel1 = new JPanel();
         panel1.setPreferredSize(new Dimension(150, 100));
         panel1.setBackground(Color.white);
         window.add(panel1);
         this.bm = new BallManager();
      
      }
   
       public void actionPerformed(ActionEvent event) {
    
         if (event.getSource() == throw1) {
		  		 bm.throwBall();	
         }
      
         if (event.getSource() == stop1) {
            bm.stopBall();
         }
      
      }
   }

