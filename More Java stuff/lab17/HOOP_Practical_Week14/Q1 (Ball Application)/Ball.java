   import java.awt.*;
   import javax.swing.*;
	import java.util.Random;
    public class Ball extends Thread {
      private int x = 7, xChange = 3;
      private int y = 0, yChange = 2;
      private final int diameter = 10;
      private final int width = 140, height = 90;
      boolean keepGoing;
		
		
		public Ball(int sequence){
		if(sequence==0){
			this.x=7;
			this.y=0;
		
		}else{
				this.x = 138;
				this.y = 0;
			}
		}
		
   
   
       public void run() {
         keepGoing = true;
         while (keepGoing) {
            move();
            bounce();
            draw(); 
            delay();
            delete(); 
         }
      }
		
		
   
       private void move() {
         x = x + xChange;
         y = y + yChange;
      }
   
       private void bounce() {
         if (x <= 0 || x  >= width) {
            xChange = -xChange;
         }
         if (y <= 0 || y >= height) {
            yChange = -yChange;
         }
      }
   
       private void delay() {
         try {
            Thread.sleep(50);
         }
             catch (InterruptedException e) {
               return;
            }
      }
   
       private void draw() {
         Graphics paper = BallApp.panel1.getGraphics();
			Random r= new Random();
			Color c= new Color( r.nextInt(256), r.nextInt(256),r.nextInt(256));
			paper.setColor(c);
         paper.fillOval(x, y, diameter, diameter);
      }
		
	
   
		
		
       private void delete() {
         Graphics paper = BallApp.panel1.getGraphics();
         paper.setColor(Color.white);
         paper.fillOval (x, y, diameter, diameter);
      }
   
       public void pleaseStop() {
         keepGoing = false;
      }
   }
