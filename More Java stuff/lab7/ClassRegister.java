    public class ClassRegister{
   
      private String[] studentNames;
      private String tutGrp;
   
       public ClassRegister(String[] studentNames,String tutGrp){
         this.studentNames=studentNames;
         this.tutGrp=tutGrp;
      }
   
       public boolean searchStudent(String studentName){
         boolean check=false;
         for(int i=0;i<studentNames.length;i++){
            if(studentNames[i].equals(studentName))
            {
               check=true;
               break;
            }
         }
         return check;
      }
   
       public String getTutGrp(){
         return tutGrp;
      }
   
   }	
