   import java.awt.*;
   import javax.swing.JPanel;

    public class DrawLines extends JPanel 
   {
       public void paintComponent(Graphics g )
      {
      
         //Step 1: ADD YOUR CODE HERE
         g.setColor(Color.RED);
      	g.drawLine(40,100,40,300);
         g.drawLine(80,100,80,300);
         g.drawLine(40,200,80,200);
      	
      	//Step 2: ADD YOUR CODE HERE
      	g.setColor(Color.RED);
      	g.drawLine(120,100,120,300);
         g.drawLine(120,300,160,300);
         g.drawLine(160,300,160,100);
			g.drawLine(160,100,120,100);
      	
      	
      	
      	//Step 3: ADD YOUR CODE HERE
      	
      	g.setColor(Color.RED);
      	g.drawLine(200,100,200,300);
         g.drawLine(200,300,240,300);
         g.drawLine(240,300,240,100);
			g.drawLine(240,100,200,100);
			

      	
      	//Step 4: ADD YOUR CODE HERE
      	
      	g.setColor(Color.RED);
      	g.drawLine(280,100,280,300);
         g.drawLine(280,100,320,100);
         g.drawLine(320,100,320,200);
			g.drawLine(320,200,280,200);
			
      	
      	
      } 
      
   } 
