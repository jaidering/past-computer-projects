

   import java.awt.Color;
   import java.awt.Graphics;
   import java.util.Iterator;
   import javax.swing.JPanel;
	import java.util.Random;

    public class MovingRocket extends Thread{
      private JPanel panel;
   	
   	//Initial rectangle position
      private int xpos =-60;
      private  int ypos = 0;
      private int rect_width =20;
      private int rect_height =50;
		
		private int xpos1 =-40;
      private  int ypos1 = 13;
      private int rect_width1 =60;
      private int rect_height1 =20;
		
		private int xpos2 =20;
      private  int ypos2 = 17;
      private int rect_width2 =5;
      private int rect_height2 =10;
		
	
    	//Constructor
       public MovingRocket(JPanel panel) {
         this.panel = panel;
      }
   	
       public void run(){
         while(true){
			
         	//invoke the drawRectangle() method and pass in the variables,
         	//xpos, ypos, rect_width, rect_height
            //ADD YOUR CODE HERE
         	drawRocket(xpos,ypos,rect_width,rect_height);
				drawRocket(xpos1,ypos1,rect_width1,rect_height1);
				drawRocket(xpos2,ypos2,rect_width2,rect_height2);
			
          
         	//invoke the delay method and pass the argument value 1
            //ADD YOUR CODE HERE
			//	Random r=new Random();
			//	int randomNo =r.nextInt(30);
         	delay(10);
			
            
         	//invoke the deleteRectangle() method and pass in the variables,
         	//xpos, ypos, rect_width, rect_height
            //ADD YOUR CODE HERE
         	deleteRectangle(xpos,ypos,rect_width,rect_height);
				deleteRectangle(xpos1,ypos1,rect_width1,rect_height1);
				deleteRectangle(xpos2,ypos2,rect_width2,rect_height2);
            
         	//Increment xpos by 1 to start rectangle at new horizontal position
            //ADD YOUR CODE HERE
         	xpos=xpos+1;
			   xpos1=xpos1+1;
				xpos2=xpos2+1;
				
				
				if(xpos==400){
				xpos=-60;
				}
					
				if(xpos1==400){
				xpos1=-60;
				}
					
				if(xpos2==400){
				xpos2=-60;
				}
                     
         }
      }
   
       public void drawRocket(int x, int y,int width, int height){
         Graphics g = panel.getGraphics();
        
      	//Set color to BLUE (Refer to DrawRectangles.java for working example)
      	//ADD YOUR CODE HERE
         g.setColor(Color.BLUE);
			
        
      	//Draw the rectangle using drawRect() method and passed in the arguments above
      	//ADD YOUR CODE HERE
         g.fillRect(x,y,width,height);
      
      }
       public void deleteRectangle(int x, int y,int width, int height){
         Graphics g = panel.getGraphics();
        
      	//Set color to WHITE (Refer to DrawRectangles.java for working example)
      	//ADD YOUR CODE HERE
         g.setColor(Color.WHITE);
        
      	//Draw the rectangle using drawRect() method and passed in the arguments above
      	//ADD YOUR CODE HERE
         g.fillRect(x,y,width,height);
			
      }
       public void delay(int n){
         for (long i = 0; i < n*1000000; i++) {
            //do nothing, let it loop to simulate short pause
         }
      }
		
		
   } 
