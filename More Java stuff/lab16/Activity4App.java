import java.awt.Color;
   import javax.swing.JFrame;
import javax.swing.JPanel;
 
    public class Activity4App
   {
   // execute application
       public static void main( String args[] )
      {
      // create frame 
         JFrame frame = new JFrame(" MovingRocket " );
         frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
         JPanel panel = new JPanel();
         panel.setBackground(Color.WHITE);
			Thread t = new MovingRocket(panel);
       //  MovingRocket movingRocket = new MovingRocket(panel); 
         frame.add(panel); // add panel to frame
         frame.setBackground( Color.WHITE ); // set frame background color
         frame.setSize( 400, 200 ); // set frame size
         frame.setVisible( true ); // display frame
         
       t.start();
		       } // end main
   } 
