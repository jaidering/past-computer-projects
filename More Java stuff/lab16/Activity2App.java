   import java.awt.Color;
   import javax.swing.JFrame;

    public class Activity2App
   {
   // execute application
       public static void main( String args[] )
      {
      // create frame 
         JFrame frame = new JFrame( "Drawing Lines" );
         frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      
         DrawRectangles drawRectanglesJPanel = new DrawRectangles(); 
         frame.add(drawRectanglesJPanel ); // add panel into frame
         frame.setBackground( Color.WHITE ); // set frame background color
         frame.setSize( 400, 400 ); // set frame size
         frame.setVisible( true ); // display frame
      } // end main
   }

