   import java.awt.*;
   import javax.swing.JPanel;

    public class DrawRectangles extends JPanel 
   {
       public void paintComponent(Graphics g )
      {
      	//Letter 'H' provided
         g.setColor(Color.RED);
         g.drawLine(40,100,40,300);
         g.drawLine(80,100,80,300);
         g.drawLine(40,200,80,200);
      	
         //Step 1: ADD YOUR CODE HERE TO DRAW 1ST "O"
      	g.setColor(Color.BLUE);
   		g.drawRect(120,100,40,200);   	

         
         //Step 2: ADD YOUR CODE HERE TO DRAW 2ND "O"
      	g.setColor(Color.BLUE);
			g.drawRect(200,100,40,200);


      
         //Letter 'P' provided  
      	g.setColor(Color.RED);
         g.drawRect(280,100,40,100);
         g.drawLine(280,200,280,300);
      	
      } 
      
   } 
