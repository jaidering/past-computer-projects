/*
 * Activity3App.java
 *
 * Created on 05 January 2007, 09:18
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author tscung
 */
   import java.awt.Color;
   import javax.swing.JFrame;
   import javax.swing.JPanel;

    public class Activity3App
   {
   // execute application
       public static void main( String args[] )
      {
      // create frame 
         JFrame frame = new JFrame( "Moving Rectangle" );
         frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
         JPanel panel = new JPanel();
         panel.setBackground(Color.WHITE);
         MovingRectangle movingRectangle = new MovingRectangle(panel); 
         frame.add(panel); // add panel to frame
         frame.setBackground( Color.WHITE ); // set frame background color
         frame.setSize( 400, 200 ); // set frame size
         frame.setVisible( true ); // display frame
         
         movingRectangle.demo();
      } // end main
   } 
