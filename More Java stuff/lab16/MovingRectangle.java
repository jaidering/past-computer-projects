/*
 * MovingRactangle.java
 *
 * Created on 05 January 2007, 09:08
 *
 */


   import java.awt.Color;
   import java.awt.Graphics;
   import java.util.Iterator;
   import javax.swing.JPanel;
	import java.util.Random;

/**
 *
 * @author tscung
 */
    public class MovingRectangle {
      private JPanel panel;
   	
   	//Initial rectangle position
      private int xpos =0;
      private  int ypos = 80;
      private int rect_width =40;
      private int rect_height =20;
    
    	//Constructor
       public MovingRectangle(JPanel panel) {
         this.panel = panel;
      }
   	
       public void demo(){
         while(true){
			
         	//invoke the drawRectangle() method and pass in the variables,
         	//xpos, ypos, rect_width, rect_height
            //ADD YOUR CODE HERE
         	drawRectangle(xpos,ypos,rect_width,rect_height);
          
         	//invoke the delay method and pass the argument value 1
            //ADD YOUR CODE HERE
				Random r=new Random();
				int randomNo =r.nextInt(30);
         	delay(randomNo);
			
            
         	//invoke the deleteRectangle() method and pass in the variables,
         	//xpos, ypos, rect_width, rect_height
            //ADD YOUR CODE HERE
         	deleteRectangle(xpos,ypos,rect_width,rect_height);
            
         	//Increment xpos by 1 to start rectangle at new horizontal position
            //ADD YOUR CODE HERE
         	xpos=xpos+1;
				
				if(xpos==400){
				xpos=0;
				}
                     
         }
      }
   
       public void drawRectangle(int x, int y,int width, int height){
         Graphics g = panel.getGraphics();
        
      	//Set color to BLUE (Refer to DrawRectangles.java for working example)
      	//ADD YOUR CODE HERE
         g.setColor(Color.BLUE);
			
        
      	//Draw the rectangle using drawRect() method and passed in the arguments above
      	//ADD YOUR CODE HERE
         g.drawRect(x,y,width,height);
      
      }
       public void deleteRectangle(int x, int y,int width, int height){
         Graphics g = panel.getGraphics();
        
      	//Set color to WHITE (Refer to DrawRectangles.java for working example)
      	//ADD YOUR CODE HERE
         g.setColor(Color.WHITE);
        
      	//Draw the rectangle using drawRect() method and passed in the arguments above
      	//ADD YOUR CODE HERE
         g.drawRect(x,y,width,height);
			
      }
       public void delay(int n){
         for (long i = 0; i < n*1000000; i++) {
            //do nothing, let it loop to simulate short pause
         }
      }
   } 
