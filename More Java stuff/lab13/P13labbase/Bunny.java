public class Bunny extends Animal{
	private String furColor;
	
	public Bunny(String name,char gender,String furColor){
	super(name,gender);
	this.furColor=furColor;
	}
	
	public String getFurColor(){
	return furColor;
	}
	
	public void setFurColor(String color){
	furColor=color;
	}
}