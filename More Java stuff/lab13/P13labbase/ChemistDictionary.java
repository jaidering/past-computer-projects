public class ChemistDictionary extends Dictionary
{

	private int elements = 40;
	
	public void setNumElements(int numElements)
	{
		elements = numElements;
	}
	
	public int getElements()
	{
		return elements;
	}	
}