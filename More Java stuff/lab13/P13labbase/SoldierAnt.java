public class SoldierAnt extends Ant{
	private int attackPower;
	
	public SoldierAnt(String name,int strength,String colour,int attackPower){
	super(name,strength,colour);
	this.attackPower=attackPower;
	}
	
	public int getAttackPower(){
	return attackPower;
	}

}