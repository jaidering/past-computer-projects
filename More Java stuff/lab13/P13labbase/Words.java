public class Words
{
	public static void main(String[] args)
	{
		Book myBook = new Book();
		
		System.out.println("Number of pages: " + myBook.getPages());
		
		CookBook cb= new CookBook(3500,"Tyson");
		
		System.out.println("The ratio is: " + cb.computeRatio());
	}
}