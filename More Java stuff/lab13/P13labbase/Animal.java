public class Animal{
	private String name;
	private char gender;
	
	public Animal (String name,char gender){
	this.name=name;
	this.gender=gender;
	
	}
	
	public String getName(){
	return name;
	}
	
	public void setName(String newName){
	name=newName;
	}
	
	public char getGender(){
	return gender;
	}
}