public class Encyclopedia extends Book
{
	private int articles = 150000;
	
	public double computeRatio()
	{
		return articles/getPages();
	}		
	
	public void setArticles (int numArticles)
	{
		articles = numArticles;
	}
	
	public int getArticles()
	{
		return articles;
	}	
}