public class Ant{
	private String name;
	private int strength;
	private String colour;
	
	public Ant(String name,int strength,String colour){
	this.name=name;
	this.strength=strength;
	this.colour=colour;

	}
	
	public void setName(String n){
	name=n;
	}
	
	public String getName(){
	return name;
	}
	
	public void setStrength(int s){
	strength=s;
	
	}
	
	public void setColour(String c){
	colour=c;
	}

}