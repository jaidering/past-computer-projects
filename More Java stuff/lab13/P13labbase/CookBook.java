public class CookBook extends Book
{
	private int numRecipes;
	private String photographer;

	public CookBook(int numRecipes,String photographer){
	super();
	this.numRecipes=numRecipes;
	this.photographer=photographer;
	}
	
	public double computeRatio(){
		return numRecipes / getPages();
	}
	
	}