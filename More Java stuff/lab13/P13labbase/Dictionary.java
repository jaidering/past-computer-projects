public class Dictionary extends Book
{
	private int definitions = 52500;
	private String name;
	
	public Dictionary()
	{
	}
	
	public double computeRatio()
	{
		return definitions/getPages();
	}
	
	public void setDefinitions (int numDefinitions)
	{
		definitions = numDefinitions;
	}
	
	public int getDefinitions()
	{
		return definitions;
	}
}