public class Ape extends Animal{
	private double armLength;
	
	public Ape(String name,char gender,double armLength){
	super(name,gender);
	this.armLength=armLength;
	}
	
	public double getArmLength(){
	return armLength;
	}
	
	public void setArmLength(double length){
	armLength=length;
	}
}	
