    public class WeightWatchApp {
   
       public static void main(String[] args) {
         Character c1 = new Character("Chicken Little", 0.8, 5);
         Character c2 = new Character("Mr Incredible", 2.1, 105);
         Character c3 = new Character("Tarzan", 1.8, 76);
         Character c4 = new Character("Mulan", 1.6, 48);
      // ********************************************************
      // bi. Create the CartoonStudio object and pass in
      //     an array of Character objects, name as "Disney"
      // ********************************************************
      
         Character[] characters={c1,c2,c3,c4};
         CartoonStudio cs=new CartoonStudio(characters,"Disney");
      
      // ********************************************************
      // ii. Remove the Character with the name "Tarzan" from 
      //     the CartoonStudio object 
      // ********************************************************
      
         boolean removeTarzan=cs.removeCharacter("Tarzan");
         if(removeTarzan){
            System.out.println("Tarzan is removed");
         }
         else{
            System.out.println("Tarzan is not found");
         }
      
      // ********************************************************
      // iii. Create a new Character object with "Brother Bear" as 
      //      the name, 3.2 as the height, 250 as the weight and
      //    add it to the CartoonStudio object
      // ********************************************************	
         Character c5 = new Character("Brother Bear", 3.2, 250);
         boolean check=cs.addCharacter(c5);
      
      
      // ********************************************************
      // iv. Retrieve the array from CartoonStudio and
      //     assign it to the disneyChars array
      // ********************************************************		
         Character[] disneyChars=cs.getCharacters();
      
      
         System.out.println("Let's display the final array ");
      // ********************************************************
      // iv. Traverse the array to display the current array
      //     after adding the Character object with "Brother Bear"
      //     as the name, height as 3.2 and weight as 250kg
      // ********************************************************
         for(int i=0;i<disneyChars.length;i++){
            String name=disneyChars[i].getName();
            System.out.println(name);
         }
      
         Character[] overWeight=cs.getOverWeightCharacters();
         for(int i=0;i<overWeight.length;i++){
            if (overWeight[i] != null){
               String name=overWeight[i].getName();
               System.out.println(name);
            }
         }
      }
   }