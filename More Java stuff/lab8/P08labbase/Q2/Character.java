public class Character {
	private String name;
	private double height;
	private double weight;
	
	public Character(String name, double height, double weight) {
		this.name = name;
		this.height = height;
		this.weight = weight;
	}
	
	public double getBMI() {
		return weight / (height * height);
	}
	
	public boolean isOverWeight() {
		if (getBMI() <= 23) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public String getName() {
		return name;
	}
}