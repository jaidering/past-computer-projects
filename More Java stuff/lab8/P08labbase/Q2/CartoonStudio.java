    public class CartoonStudio {
   
      private Character[] characters;
      private String name;
   
       public CartoonStudio(Character[] characters, String name) {
         this.characters = characters;
         this.name = name;
      }
   
       public String getName() {
         return name;
      }
   
       public Character[] getCharacters() {
         return characters;
      }
   
   //*****************************************
   // Q2b (i)
   // Implement removeCharacter() method
   //*****************************************
       public boolean removeCharacter(String name){
         boolean check=false;
         for(int i=0;i<characters.length;i++){
            if(name.equals(characters[i].getName())){
               characters[i]=null;
               check=true;
               break;
            }
            else
            {
            }
         }
         return check;
      }
   //*****************************************
   // Q2b (ii) 
   // Implement addCharacter() method
   //*****************************************
       public boolean addCharacter(Character character){
         boolean check=false;
         for(int i=0;i<characters.length;i++){
            if(characters[i]==null){
               characters[i]=character;
            }
         }
         return check;
      }
      
       public Character[] getOverWeightCharacters(){
      
         Character[] overWeight=new Character[characters.length];
         for(int i=0;i<characters.length;i++){
            boolean check=characters[i].isOverWeight();
            if(check)
               overWeight[i]=characters[i];
         }
         return overWeight;
      }
   }