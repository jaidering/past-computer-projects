public class NewClassRegister{

private Student[] studentArray;
private String tutGrp;

	public NewClassRegister(Student[] studentArray,String tutGrp){
	this.studentArray=studentArray;
	this.tutGrp=tutGrp;
	}
	
	public Student[] getStudentArray(){
	return studentArray;
	}
	
	public String getTutGrp(){
	return tutGrp;
	}
	
	public boolean searchStudent(String name){
	boolean check=false;
	for(int i=0;i<studentArray.length;i++){
	if(studentArray[i] != null){
		if(studentArray[i].getName().equals(name)){
			check=true;
			break;
			}
			}
	}
	return check;
	}
	
	public void printStudents(){
	for(int i=0;i<studentArray.length;i++){
	if(studentArray[i] !=null){
	System.out.print("  "+studentArray[i]);
	}
	}
	}
}	