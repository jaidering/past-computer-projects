import java.util.Scanner;
public class NewClassRegisterApp{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		
		//create studentArray and add in students objects
		Student[] studentArray = new Student[10];
		
		studentArray[0] = new Student("Ron", "0605123D", 17);
		studentArray[1] = new Student("Mandy", "0605678A", 18);
		studentArray[2] = new Student("Joshua", "0605666J", 18);
		
		//create NewClassRegister object
		NewClassRegister cr = new NewClassRegister(studentArray,"T06");
		
		//search for student
		System.out.print("Enter the student name: ");
		String name = sc.nextLine();
		
		boolean found = cr.searchStudent(name);
		
		if (found)
			System.out.println("Student found");
		else
			System.out.println("Student not found");
			
		//print all students
		cr.printStudents();
	}
}