public class Student {
	private String name;
	private String adminNo;
	private int age;
	
	public Student(String name, String adminNo, int age) {
		this.name = name;
		this.adminNo = adminNo;
		this.age = age;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAdminNo() {
		return adminNo;
	}
	
	public void setAdminNo() {
		this.adminNo = adminNo;
	}

	public String toString() {
		String info = "Name: " + name
						+ "\nAdmin No: " + adminNo
						+ "\nAge: " + age;
		return info;
	}

}