public class NavyDiver implements Swimmer{
	
	private String rank;
	
	public NavyDiver(){
	}
	
	public String speak(){
		String info = "Hello!";
		return info;
	}
	
	public void swim(){
		System.out.println ("Diving down 10m, 20m, ..50m, Urgh, I forgot my oxygen tank!");
	}
	
}	