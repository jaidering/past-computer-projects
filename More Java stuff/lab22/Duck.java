public class Duck extends Animal implements Swimmer{
	
	public Duck(){
	}
	
	public String sound(){
		String info = "Quack Quack!";
		return info;
	}
	
	public void swim(){
	System.out.println ("Down by the river I would go, wibble wobble, to and fro");
	}
}