public class TestSwimApp
{
	public static void main(String[] args)	
	{
		//Create Duck object named myDuck
		
		Duck myDuck = new Duck();
		
		//Create Whale object named myWhale
		
		Whale myWhale = new Whale();
		
		//Create NavyDiver object named myNavyDiver
		
		NavyDiver myNavyDiver = new NavyDiver();
		
		//Declare an array of 30 Swimmer interface reference called swimArray
		
		Swimmer[] swimArray = new Swimmer[30];
		
		//Assign the Duck, Whale and NavyDiver objects into the interface reference array elements
		
		swimArray[0] = myDuck;
		swimArray[1] = myWhale;
		swimArray[2] = myNavyDiver;
	
		//Traverse the array of interface reference and invoke the swim() method  
		
		for ( int i = 0; i < swimArray.length; i++){
			if ( swimArray[i] != null){
				swimArray[i].swim();
			}	
		}
		
	}
}
