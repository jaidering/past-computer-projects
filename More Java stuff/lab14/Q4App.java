   import javax.swing.JFrame;

    public class Q4App
   {
   //-----------------------------------------------------------------
   //  Creates the main program frame.
   //-----------------------------------------------------------------
       public static void main (String[] args)
      {
         JFrame frame = new JFrame ("Number Formatting");
         frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
      
         frame.getContentPane().add(new LoanCalculator());
      
         frame.pack();
         frame.setVisible(true);
      }
   }
