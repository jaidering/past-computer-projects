   import java.awt.*;
   import java.awt.event.*;
   import javax.swing.*;
   import java.text.*;

    public class Circumference extends JPanel implements ActionListener
   {
      private JPanel panel;
      private JTextField loanAmtField, yearsField;
      private JLabel label1, label3, resultLabel;
      private JButton fmtButton, goButton; 
		NumberFormat currencyFmt = NumberFormat.getCurrencyInstance ();
    
    //This method will place the GUI objects on the pane	 
       public Circumference() {
      	
         setPreferredSize (new Dimension(250, 250));
         setBackground (Color.cyan);
      
        //Add the GUI objects on the panel
         label1 = new JLabel("Radius: ");
         add(label1);
      
         loanAmtField = new JTextField(10);
         add(loanAmtField);
      	
         
         label3 = new JLabel("Circumference:    ");
         add(label3);
         
         resultLabel = new JLabel("---");
         add(resultLabel);	
         
         fmtButton = new JButton("Compute circumference");
         add(fmtButton);
         fmtButton.addActionListener(this); 
      }
   
       public void actionPerformed(ActionEvent event) {
       
       	//Part 1
         String result ="";
         if (event.getSource() == fmtButton) {
         	
         	//This code segment will extract the number entered by user, 
         	//convert it to double data type 
            String s1 = loanAmtField.getText();
            double amt = Double.parseDouble(s1);           
         	
         	//Compute Interest payable
				double circumference = 2 * Math.PI * amt;
         	String s3= ""+circumference;
         	//format the value and present the interest payable in the result label
         	//... YOUR CODES HERE
        
         resultLabel.setText(s3);
         
         }  
      
      }
   }
