   import java.awt.*;
   import java.awt.event.*;
   import javax.swing.*;

   public class StringTemplate extends JPanel implements ActionListener
   {
      private JPanel panel;
      private JTextField string1Field, string2Field, resultField;
      private JLabel string1Label, string2Label, resultLabel;
      private JButton btn; 
    
    //This method will place the GUI objects on the pane	 
       public StringTemplate() {
      	
         setPreferredSize (new Dimension(250, 250));
         setBackground (Color.cyan);
      
        //Add the GUI objects on the panel
         string1Label = new JLabel("Type string1 ");
         add(string1Label);
      
         string1Field = new JTextField(20);
         add(string1Field);
      
         string2Label = new JLabel("Type string2: ");
         add(string2Label);
      
         string2Field = new JTextField(20);
         add(string2Field);
      
         resultLabel = new JLabel("result is:    ");
         add(resultLabel);
      
         resultField = new JTextField(20);    
         add(resultField);
      
         btn = new JButton("Compare");
         add(btn);
         btn.addActionListener(this); 
      }
   
       public void actionPerformed(ActionEvent event) {
         String result ="";
			
			//if Compare button is clicked
         if (event.getSource() == btn) {
			
         	//Retrieve text input from text fields using getText() method
            String s1 = string1Field.getText().trim();
            String s2 = string2Field.getText().trim();
         
            //Compare the 2 strings above
         	if(s1.equalsIgnoreCase(s2)){
				result="They are equal and has"+s1.length()+" letters";
				}else{
				result="They are not equal";
				}
         	
            //... FILLED IN YOUR CODES HERE
           
         
			
				//display result in result textfield
            resultField.setText(result);
         }   
      }
   }


