   import java.awt.*;
   import java.awt.event.*;
   import javax.swing.*;
   import java.text.*;

    public class FormatClass extends JPanel implements ActionListener
   {
      private JPanel panel;
      private JTextField loanAmtField, yearsField, resultField;
      private JLabel label1, label2, label3;
      private JButton fmtButton, goButton; 
    
    //This method will place the GUI objects on the pane	 
       public FormatClass() {
      	
         setPreferredSize (new Dimension(250, 250));
         setBackground (Color.cyan);
      
        //Add the GUI objects on the panel
         label1 = new JLabel("Loan Amount: ");
         add(label1);
      
         loanAmtField = new JTextField(10);
         add(loanAmtField);
      
         label2 = new JLabel("Number of Years:    ");
         add(label2);
      	
         yearsField = new JTextField(10);
         add(yearsField);
         
         label3 = new JLabel("Interest:    ");
         add(label3);
         resultField = new JTextField(10);    
         add(resultField);
      	
         fmtButton = new JButton("Compute Interest");
         add(fmtButton);
         fmtButton.addActionListener(this); 
      	
      	/*
         string2Label = new JLabel("Type string2: ");
         add(string2Label);
      
         string2Field = new JTextField(20);
         add(string2Field);
      
      
         goButton = new JButton("Compare");
         add(goButton);
         goButton.addActionListener(this); */
      }
   
       public void actionPerformed(ActionEvent event) {
       
       	//Part 1
         String result ="";
         if (event.getSource() == fmtButton) {
         	
         	//This code segment will extract the number entered by user, 
         	//convert it to double data type 
            String s1 = loanAmtField.getText();
            double amt = Double.parseDouble(s1);
            String s2 = yearsField.getText();
            double yrs = Double.parseDouble(s2);            
         	
				//Compute Interest payable
				
				
         	//format the value and present it in currency f
            NumberFormat fmat1 = NumberFormat.getCurrencyInstance();
            result = fmat1.format(amt);
         	
            resultField.setText(result);
         }  

      }
   }


