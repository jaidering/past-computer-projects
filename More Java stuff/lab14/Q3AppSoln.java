   import java.util.*;
   import java.text.*;

    public class Q3AppSoln	{
       public static void main(String arg[]) {
      
         double TAX_RATE = 0.05;  // 5% GST 
         int quantity = 10;
         double subtotal, tax, totalPrice;
         double unitPrice = 11.90;
      
         Scanner sc = new Scanner(System.in);
      
      //Part 1 : Prompt user to enter Unit Price and Quantity
      	
      	
      //Part 2 : Calculate subtotal, tax and totalCost		
         subtotal = quantity * unitPrice;
         tax = subtotal * TAX_RATE;
         totalPrice = subtotal + tax;
      
      //Display results before formatting      
         System.out.println("Before formatting ...");
         System.out.println ("Subtotal: " + subtotal);
         System.out.println ("GST Tax: " + tax + " at " +TAX_RATE);
         System.out.println ("Total: " + totalPrice);
      		
      
      //Part 3 : Create NumberFormat Currency and Percentage objects
         System.out.println("\nAfter formatting ...");
         NumberFormat currencyFmt = NumberFormat.getCurrencyInstance (Locale.UK);
         NumberFormat percentFmt = NumberFormat.getPercentInstance ();
      
       //Part 4 : Format output with appropriate currency and percentage formats
         System.out.println ("Subtotal: " + currencyFmt.format(subtotal));
         System.out.println ("GST Tax: " + currencyFmt.format(tax) + " at " + percentFmt.format(TAX_RATE));
         System.out.println ("Total: " + currencyFmt.format(totalPrice));
      
         System.out.println(currencyFmt.getCurrency().getSymbol());
      
      }
   }