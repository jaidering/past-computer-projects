   import java.util.*;
   import java.text.*;

    public class Q5App	{
       public static void main(String arg[]) {
      
         double area = 0;
         double radius = 0;
      
      
         Scanner sc = new Scanner(System.in);
      
      //Part 1 : Prompt user to enter radius
      System.out.println("Enter radius:");
		radius=sc.nextDouble();sc.nextLine();      
      //Part 2 : Calculate area of circle		
               area = Math.PI* radius*radius;
      
      //Display results before formatting      
         System.out.println("Before formatting ...");
         System.out.println("Area = " + area);  
      		
      
      //Part 3 : Create DecimalFormat object
         System.out.println("\nAfter formatting ...");
      String pattern = "0.###";	
      DecimalFormat dfmt = new DecimalFormat(pattern);
     
      System.out.println("Area = " + dfmt.format(area));
      
      
      }
   }