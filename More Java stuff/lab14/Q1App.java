public class Q1App	{
	public static void main(String arg[]) {
	
		//Part 1
		int x = 123;
		int y = 321;
		System.out.println(x+y+" is the total of x+y");
		//...your codes here...
		System.out.println("x+y = " +(x+y));
		
		System.out.println("--------------- End of Part 1 --------------");
		
		//Part 2
		String s1 = "TO BE or not TO BE";
		//Replace the string s2 to show the result "TO EAT or not TO EAT"
		//...your codes here...
		String r2 = s1.replace("BE", "EAT");
      System.out.println(r2);
		
		
		System.out.println("--------------- End of Part 2 --------------");

		//Part 3
		String s2 = "Mississippi";
		//To display "Mississippi" as "MISSISSIPPI"
		//...your codes here...
		String r3 = s2.toUpperCase();
		System.out.println(r3);
		
		System.out.println("--------------- End of Part 3 --------------");
		
		//Part 4
		//Use the substring method to extract the word "SIP" from MISSISSIPPI
		//...your codes here...
		String r4 = r3.substring(0,4);
		System.out.println(r4);
		
		String r5 = r3.substring(6,9);
		System.out.println(r5);
		
		System.out.println("--------------- End of Part 4 --------------");

	}
}