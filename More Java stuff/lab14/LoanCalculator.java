   import java.awt.*;
   import java.awt.event.*;
   import javax.swing.*;
   import java.text.*;

    public class LoanCalculator extends JPanel implements ActionListener
   {
      private JPanel panel;
      private JTextField loanAmtField, yearsField;
      private JLabel label1, label2, label3, resultLabel;
      private JButton fmtButton, goButton; 
		NumberFormat currencyFmt = NumberFormat.getCurrencyInstance ();
    
    //This method will place the GUI objects on the pane	 
       public LoanCalculator() {
      	
         setPreferredSize (new Dimension(250, 250));
         setBackground (Color.cyan);
      
        //Add the GUI objects on the panel
         label1 = new JLabel("Loan Amount: ");
         add(label1);
      
         loanAmtField = new JTextField(10);
         add(loanAmtField);
      
         label2 = new JLabel("Number of Years:    ");
         add(label2);
      	
         yearsField = new JTextField(10);
         add(yearsField);
         
         label3 = new JLabel("Interest:    ");
         add(label3);
         
         resultLabel = new JLabel("---");
         add(resultLabel);	
         
         fmtButton = new JButton("Compute Interest");
         add(fmtButton);
         fmtButton.addActionListener(this); 
      }
   
       public void actionPerformed(ActionEvent event) {
       
       	//Part 1
         String result ="";
         if (event.getSource() == fmtButton) {
         	
         	//This code segment will extract the number entered by user, 
         	//convert it to double data type 
            String s1 = loanAmtField.getText();
            double amt = Double.parseDouble(s1);
            String s2 = yearsField.getText();
            double yrs = Double.parseDouble(s2);            
         	
         	//Compute Interest payable
         	double interest = (amt*0.0199) / yrs ;
				String s3= ""+interest;

         	
         	//format the value and present the interest payable in the result label
         	//... YOUR CODES HERE
        
         resultLabel.setText(s3);
         
         }  
      
      }
   }


