   import java.util.*;
	import java.text.*;
   

    public class Q3App	{
       public static void main(String arg[]) {
      
         double TAX_RATE = 0.05;  // 5% GST 
         int quantity = 0;
         double subtotal, tax, totalPrice;
         double unitPrice = 0;
			
			NumberFormat currencyFmt = NumberFormat.getCurrencyInstance (Locale.UK);
  			  NumberFormat percentFmt = NumberFormat.getPercentInstance ();
      
      
         Scanner sc = new Scanner(System.in);
      
      //Part 1 : Prompt user to enter Unit Price and Quantity
		System.out.print("Enter unit price :$");
      unitPrice=sc.nextDouble();sc.nextLine();
		System.out.print("Enter the quantity :");
		quantity=sc.nextInt();sc.nextLine();
      
      //Part 2 : Calculate subtotal, tax and totalCost		
      subtotal = quantity * unitPrice;
      tax = subtotal * TAX_RATE;
      totalPrice = subtotal + tax;
      
      //Display results before formatting      
         System.out.println("Before formatting ...");
         System.out.println ("Subtotal: " + subtotal);
         System.out.println ("GST Tax: " + tax + " at " +TAX_RATE);
         System.out.println ("Total: " + totalPrice);
			
			
			
		
      		
      
      //Part 3 : Create NumberFormat Currency and Percentage objects
         System.out.println("\nAfter formatting ...");
      
      
       //Part 4 : Format output with appropriate currency and percentage formats
      		System.out.println ("Subtotal: " + currencyFmt.format(subtotal));
    System.out.println ("GST Tax: " + tax + " at " + percentFmt.format(TAX_RATE));
    System.out.println ("Total: " + currencyFmt.format(totalPrice));      
      }
   }