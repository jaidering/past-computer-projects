   import java.util.*;
   import java.text.*;

    public class Q5AppSoln	{
       public static void main(String arg[]) {
      
         double area = 120.5523;
         double radius = 100;
      
      
         Scanner sc = new Scanner(System.in);
      
      //Part 1 : Prompt user to enter radius
      
      
      //Part 2 : Calculate area of circle		
         area = Math.PI* radius*radius;
      
      //Display results before formatting      
         System.out.println("Before formatting ...");
         System.out.println("Area = " + area);  
      		
      
      //Part 3 : Create DecimalFormat object
         System.out.println("\nAfter formatting ...");
         String pattern = "#,###.00";
      
       //Part 4 : Display formatted result
         DecimalFormat dfmt = new DecimalFormat(pattern);
         System.out.println("Area = " + dfmt.format(area));	      
      
      }
   }