public class Building {
	//a. Declare attributes here
private String name;
private String location;
private int    numStorey;
	//b. Declare constructor here
public Building(String nam,String loc,int numS){
name=nam;
location=loc;
numStorey=numS;
}
	//Methods
	public String getName(){
		return name;
	}

	public void setName(String n){
		name = n;
	}

	public String getLocation() {
		return location;
	}
	
	public void setLocation(String l) {
		location = l;
	}

	public int getNumStorey() {
		return numStorey;
	}
	
	public void setNumStorey(int s) {
		numStorey = s;
	}

	public String toString(){
		String info = "Name: " + name
							+ "\nLocation: " + location
							+ "\nNumber of storeys: " + numStorey;
		return info;	
	}
}