public class EzyLinkCard{
	//a. Declare attributes here
	private String id;
	private double balance;
	//b. Declare constructor here
	public EzyLinkCard(String iden,double bal){
		id=iden;
		balance=bal;
	}
	//Methods	
	public double getBalance(){
		return balance;
	}
	public void print(){
		System.out.println("Balance: "+balance);
	}
}