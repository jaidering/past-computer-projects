public class BuildingApp {
	public static void main(String[] args) {
		//Create a new Building object
		Building build = new Building("Tangs", "Orchard", 32);
		
		//Print details of building
		String details = build.toString();
		System.out.println("Object information \n" +details);
	}
}