public class Circle{
	//a. Declare attributes here
private double radius;
	//b. Declare constructor here
public Circle(double rad){
radius=rad;
}
	//Methods	
	public double getRadius(){
		return radius;
	}
	
	public void setRadius(double r){
		radius = r;
	}
	
	public double area(){
		return 3.14*radius*radius;
	}
	
	public void print(){
		System.out.println("Radius: "+radius);
	}
}