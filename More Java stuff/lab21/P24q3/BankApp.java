public class BankApp {
	public static void main(String[] arg)
	{
		Bank tpBank = new Bank("Bank of TP");
		
		SavingAccount acc1 = new SavingAccount("001",1000, 500);
		SavingAccount acc2 = new SavingAccount("002",2000, 1000);
		CurrentAccount acc3 = new CurrentAccount("003",3000, 1500);
		JuniorAccount acc4 = new JuniorAccount("004",4000, "Andrews");

		//(i)Add the above new accounts into Bank of TP
	
		tpBank.addAccount(acc1);
		tpBank.addAccount(acc2);
		tpBank.addAccount(acc3);
		tpBank.addAccount(acc4);
				
		//(ii)Call the appropriate method to print the details of the bank accounts			
		
		tpBank.printAllStatements();
		
		//(iii)Display the total interest accumulated from all accounts in the Bank of TP
		
		System.out.println("Total interest is: " + tpBank.computeTotalInterest());
		
	}
}