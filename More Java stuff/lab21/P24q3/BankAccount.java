public abstract class BankAccount {
	private String accountNo;
	private double balance;
	
	public BankAccount(String acNo, double bal)
	{
		accountNo = acNo;
		balance = bal;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void setBalance(double bal) {
		balance = bal;
	}
	
	public void deposit(double amt) {
		balance += amt;
	}
	
	public boolean withdraw(double amt) {
		boolean suc = false;
		if (balance > amt) {
			balance -= amt;
			suc = true;
		}
		else
			suc = false;
		
		return suc;
	}
		
	public abstract double computeInterest();		
	
	public String toString()
	{
		String info = "Account No : " + accountNo + "\n" +
						  "Balance : " + balance + "\n";
		return info;
	}
}