public class Bank  {
	private String bankName;
	private BankAccount[] accounts;
	
	public Bank(String name)
	{
		this.bankName = name;
		//Create array of 100 BankAccounts
		accounts = new BankAccount[100];
		
	}
	
	//Where does object casting take place in this addAccount method?
	public boolean addAccount(BankAccount a)
	{
		boolean suc = false;
		for (int i=0; i<accounts.length; i++)  {
			if (accounts[i] == null) {
				accounts[i] = a;
				suc = true;
				break;
			}
			else
				suc = false;
			
		}
		return suc;
	}
	
	public double computeTotalInterest()
	{
		double totalInterest = 0;
		//Traverse accounts array to get the sum of interest earn for each account
			for ( int i = 0; i < accounts.length; i++ ){
				if ( accounts[i] != null){
					//Polymorphism occurs here*
					double interest=accounts[i].computeInterest();
					totalInterest =+ interest;
				}
			}
		//Identify and place comment at the line where you think polymorphism occurs
		
		return totalInterest;
	}
	
	public void printAllStatements()
	{
		//Traverse accounts array to print the details of each account
			for ( int i = 0; i < accounts.length; i++ ){
				if ( accounts[i] != null){
					//Polymorphism occurs here*
					String info = accounts[i].toString();
					System.out.println(info);
				}
			}
		//Identify and place comment at the line where you think polymorphism occurs
		
	}
}