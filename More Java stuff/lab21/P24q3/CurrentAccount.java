public class CurrentAccount extends BankAccount {
	private double overdraftLimits;
	
	public CurrentAccount(String accNo, double bal, double overdraftLimits)
	{
		super(accNo, bal);
		this.overdraftLimits = overdraftLimits;
	}
	
	public boolean withdraw(double amt)
	{
		boolean suc = false;
		if (amt < overdraftLimits && amt < getBalance() )
		{
			double bal = getBalance() - amt;
			setBalance(bal);
			suc = true;
		}
		else
			suc = false;
		return suc;
	}
	
	public double computeInterest()
	{
		return getBalance()*0.01;	//1% only
	}
	
	public String toString()
	{
		String info = super.toString() + 
						  "Overdraft Limit : " + overdraftLimits + "\n";
		return info;
	}
}