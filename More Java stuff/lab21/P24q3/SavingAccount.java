public class SavingAccount extends BankAccount {
	private double withdrawalLimits;
	
	public SavingAccount(String accNo, double bal, double withdrawalLimits)
	{
		super(accNo, bal);
		this.withdrawalLimits = withdrawalLimits;
	}
	
	public boolean withdraw(double amt)
	{
		boolean suc = false;
		if (amt < withdrawalLimits && amt < getBalance() )
		{
			double bal = getBalance() - amt;
			setBalance(bal);
			suc = true;
		}
		else
			suc = false;
		return suc;
	}
	
	public double computeInterest()
	{
		return getBalance()*0.02;
	}
	
	public String toString()
	{
		String info = super.toString() + 
						  "Withdrawal Limit : " + withdrawalLimits + "\n";
		return info;
	}
}