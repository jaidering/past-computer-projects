public class JuniorAccount extends BankAccount {
	private String guardianName;
	
	public JuniorAccount(String accNo, double bal, String guardianName)
	{
		super(accNo, bal);
		this.guardianName = guardianName;
	}
	
		
	public double computeInterest()
	{
		return getBalance()*0.03;
	}
	
	public String toString()
	{
		String info = super.toString() + 
						  "Guardian Name : " + guardianName + "\n";
		return info;
	}
}