public class Employee extends StaffMember {

	private double payRate;
	private int hoursWorked;

	public Employee(String n, String ph, double payRate, int hoursWorked) {
		super(n, ph);
		this.payRate = payRate;
		this.hoursWorked = hoursWorked;
	}

	public double getPay() {
		double pay = payRate*hoursWorked;
		return pay;
	}


}