public class StaffMember {

	private String name;
	private String phone;
	
	public StaffMember(String n, String ph) {
		name = n;
		phone = ph;
	}
	
	public String getName() {
		return name;
	}	
	
	public String getPhone() {
		return phone;
	}

	public double getPay() {
		return 1000;
	}

}