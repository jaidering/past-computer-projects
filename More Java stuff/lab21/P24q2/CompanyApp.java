public class CompanyApp {
	
	public static void main(String[] args) {
		
		// a. Create an array of 10 StaffMember objects named staffArray
		
				StaffMember[] staffArray = new StaffMember[10];
		
		// b. Create Volunteer object vol1 with name "John Williams" and phone number "001-2030-2020"
		
				Volunteer vol1 = new Volunteer("John Williams" , "001-2030-2020" );
		
		// c. Create Employee object emp1 with name "Lily Smith", phone number "030-202-0303",
		//    pay rate of 12.0 per hour and 40 hours worked.
		
				Employee emp1 = new Employee("Lily Smith" , "030-202-0303", 12.0, 40);
		
		
		// d. Store the vol1 and emp1 objects into the staffArray[0] and staffArray[3] respectively
		
				staffArray[0] = vol1;
				staffArray[3] = emp1;
		
		// e. Traverse the array and print out the pay for each employee	
		
				for (int i = 0; i < staffArray.length; i++){
					if (staffArray[i] != null){
						System.out.println("Pay:$ " + staffArray[i].getPay());
					}
				}	

	}
}