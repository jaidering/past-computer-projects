class DynamicShape {  
  public static void main(String[] args)
  {
	//An array of shapes objects with 10 elements
	TwoDShape[] shapeArray = new TwoDShape[10];
       	
	
    // Given Triangle and Rectangle objects
	Triangle t1 = new Triangle("Right Angled", 3, 4);
	Triangle t2 = new Triangle("Different sides", 8, 5);
	
	Rectangle r1 = new Rectangle(3, 8);
	Rectangle r2 = new Rectangle(6, 9);

	//*********************************************************************	
	// 1. Create a Parabola object called para with width=5, height=8
	//*********************************************************************

	Parabola para = new Parabola(5,8);


	//*********************************************************************
	// 2. Store the above shapes into the shapeArray
	//*********************************************************************

	shapeArray[0] = t1;
	shapeArray[1] = t2;
	shapeArray[2] = r1;
	shapeArray[3] = r2;
	shapeArray[4] = para;
	
	//*********************************************************************
	// 3. Traverse the for loop through the shapes array
	//    and display the name and area of each shape stored in the array
	//*********************************************************************
	
	for (int i = 0;i < shapeArray.length; i++){
		if (shapeArray[i] != null){
			System.out.println("Shape name: " + shapeArray[i].getName());
			System.out.println("Shape area: " + shapeArray[i].getArea());
		}
	}
  }   
} 
