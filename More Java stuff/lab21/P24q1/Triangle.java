public class Triangle extends TwoDShape {  
  private String style;

  // Triangle constructor
  public Triangle(String s, double w, double h){
    super(w, h, "triangle"); 
    style = s;
  }
   
  //Override area() method  
  public double getArea(){   
    	return getWidth() * getHeight() * 0.5; 
  }   
} 

