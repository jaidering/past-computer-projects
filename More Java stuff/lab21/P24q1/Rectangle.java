public class Rectangle extends TwoDShape {  

  // Rectangle constructor
  public Rectangle(double w, double h) {
    super(w, h, "rectangle"); 
  }
   
  //Override area() method  
  public double getArea() {   
    return getWidth() * getHeight(); 
  }   
} 