public class Parabola extends TwoDShape {  

  // Parabola constructor
  public Parabola (double w, double h) {
    super(w, h, "parabola"); 	 
  }
  //Override area() method  
  public double getArea(){   
    return getWidth() * getHeight() * 2 / 3; 
  }   
}

