public class TwoDShape {  
  private double width;  
  private double height;  
  private String name;  
  public TwoDShape(double w, double h, String n) {  
    width = w;  
    height = h;  
    name = n;  
  }   
  public double getWidth() { return width;  }
  public double getHeight() { return height;  }
  public String getName() { return name; }  
  public double getArea(){   
    	double area = width*height; 
    	return area;  
  }   
} 
