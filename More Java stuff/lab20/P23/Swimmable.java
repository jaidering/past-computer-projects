public interface Swimmable extends Movable, Floatable {
	public boolean canSwim();
	public boolean canFloat();
	public boolean canMove();


	}