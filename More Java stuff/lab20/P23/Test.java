//---- DO NOT CHANGE THESE LINES -----
class Animal {
	public double getWeight() {
		return 0;
	}
}

class Fish extends Animal {
		public double getWeight() {
		return 100.0;
	}
}

class Whale extends Fish {
	public double getWeight() {
		return 1000;
	}
}

class Insects extends Animal {
	public double getWeight() {
		return 0.1;
	}
}
//---------------------------------------

public class Test{
	public static void main(String[] arg)
	{
          Animal c1 = new Animal();
          Animal c2 = new Fish();
          Animal mosquito = new Insects();
          Fish f = new Whale();
       
          Fish dolphy = (Fish)c2;

          Whale willy =  (Whale)f;
          
          Fish sardine = (Fish)c1;
          Whale wimmy = (Whale)c2;
			 
			//complete the codes to get the expected output
			
			System.out.println("Weight of c1 is "+c1.getWeight());
			System.out.println("Weight of c2 is "+c2.getWeight());
			System.out.println("Weight of mosquito is "+mosquito.getWeight());
			System.out.println("Weight of f is "+f.getWeight());

	}
}