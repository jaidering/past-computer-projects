public class GoldFish extends Fish{

	private double weight;
	private String color;
	
	public GoldFish(String type, double weight){
		super(type);
		this.weight=weight;
	}
	
	public double getWeight(){
		return weight;
	}
	
	public String getColor(){
		return color;
	}
}