public abstract class Fish{
		private String type;
	
		public Fish(String type){
			this.type=type;
		}
		
		public String getType(){
			return type;
		}
		
		public void getType(String newType){
			type=newType;
		}
		
		public abstract String getColor();
		
}