public abstract class Whale extends Animal {

	public Whale(double w) {
		super(w);
	}
	
   public String makeSound() {
		return "Hmmmm";
	}

}
