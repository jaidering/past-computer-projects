public abstract class Bird extends Animal {

	public Bird(double w) {
		super(w);
	}
	
   public String makeSound() {
		return "Chip";
	}

}
