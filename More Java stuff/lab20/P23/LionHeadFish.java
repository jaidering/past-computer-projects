public class LionHeadFish extends GoldFish{

	private int age;
	private String color;
	
	public LionHeadFish(String type, double weight, int age){
	super(type,weight);
	this.age=age;
	}

	public int getAge(){
		return age;
	}
	
	public String getColor(){
		return color;
	}
}