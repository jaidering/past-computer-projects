public abstract class Animal {

   private double weight;
	
	public Animal(double w) {
		weight = w;
	}
	public double getWeight() {
		return weight;
	}
   public String makeSound() {
		return "Hoo";
	}
   public abstract String getHabitat();

}
