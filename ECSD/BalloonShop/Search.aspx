﻿<%@ Page Language="C#" MasterPageFile="~/BalloonShop.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" Title="Untitled Page" %>

<%@ Register src="UserControls/ProductsList.ascx" tagname="ProductsList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
    <p>
        <asp:Label ID="titleLabel" runat="server" CssClass="CatalogTitle"></asp:Label>
        <br />
        <asp:Label ID="descriptionLabel" runat="server" CssClass="CatalogDescription"></asp:Label>
    </p>
    <p>
        <uc1:ProductsList ID="ProductsList1" runat="server" />
    </p>
</asp:Content>

