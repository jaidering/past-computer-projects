﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

/// <summary>
/// A further wrapper around ProfileWrapper, exposing data
/// in a form usable by ObjectDataSource.
/// </summary>
public class ProfileDataSource
{
    public ProfileDataSource()
    {
    }

    public List<ProfileWrapper> GetData()
    {
        List<ProfileWrapper> data = new List<ProfileWrapper>();
        data.Add(new ProfileWrapper());
        return data;
    }

    public void UpdateData(ProfileWrapper newData)
    {
        newData.UpdateProfile();
    }
}

