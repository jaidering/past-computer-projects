﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DepartmentsAdmin.ascx.cs" Inherits="UserControls_DepartmentsAdmin" %>
<asp:Label ID="statusLabel" runat="server" Text="Department Loaded"></asp:Label>
<p>
    &nbsp;</p>
<asp:Label ID="locationLabel" runat="server" Text="These are your department:"></asp:Label>
<p>
    &nbsp;</p>
<asp:GridView ID="grid" runat="server" DataKeyNames="DepartmentID" Width="100%" 
    AutoGenerateColumns="False" onrowcancelingedit="grid_RowCancelingEdit" 
    onrowdeleting="grid_RowDeleting" onrowediting="grid_RowEditing" 
    onrowupdating="grid_RowUpdating">
    <Columns>
        <asp:BoundField DataField="Name" HeaderText="Departmant Name" 
            SortExpression="Name" />
        <asp:TemplateField HeaderText="Department Description" 
            SortExpression="Description">
            <EditItemTemplate>
                <asp:TextBox ID="descriptionTextBox" runat="server" 
                    Text='<%# Bind("Description") %>' Height="70px" TextMode="MultiLine" 
                    Width="350px"></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:HyperLinkField DataNavigateUrlFields="DepartmentID" 
            DataNavigateUrlFormatString="../CatalogAdmin.aspx?DepartmentID={0}" 
            Text="View Categories" />
        <asp:CommandField ShowEditButton="True" />
        <asp:ButtonField CommandName="Delete" Text="Delete " />
    </Columns>
</asp:GridView>
<span class="AdminPageText">
<br />
Create a new department:</span>
<table class="AdminPageText" cellspacing="0">
  <tr>
    <td valign="top" width="100">Name:
    </td>
    <td>
      <asp:TextBox cssClass="AdminPageText" ID="newName" Runat="server"
Width="400px" />
    </td>
  </tr>
  <tr>
    <td valign="top" width="100">Description:
    </td>
    <td>
      <asp:TextBox cssClass="AdminPageText" ID="newDescription"
Runat="server" Width="400px" Height="70px" TextMode="MultiLine"/>
    </td>
  </tr>
</table>
<asp:Button ID="createDepartment" Text="Create Department" Runat="server"
CssClass="AdminButtonText" onclick="createDepartment_Click" />