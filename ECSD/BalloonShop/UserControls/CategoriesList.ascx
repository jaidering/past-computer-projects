﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CategoriesList.ascx.cs" Inherits="UserControls_CategoriesList" %>
<asp:DataList ID="list" runat="server" CssClass="CategoryListContent" 
    Width="200px">
    <HeaderTemplate>
        Choose a Category
    </HeaderTemplate>
    <HeaderStyle CssClass="CategoryListHead" />
    <ItemTemplate>
    &nbsp;&raquo;
    <asp:HyperLink
      ID="HyperLink1"
      Runat="server"
      NavigateUrl='<%# "../Catalog.aspx?DepartmentID=" +
Request.QueryString["DepartmentID"] +
"&CategoryID=" + Eval("CategoryID") %>'
      Text='<%# Eval("Name") %>'
      ToolTip='<%# Eval("Description") %>'
      CssClass='<%# Eval("CategoryID").ToString() ==
Request.QueryString["CategoryID"] ?
"CategorySelected" : "CategoryUnselected" %>'>>
    </asp:HyperLink>
    &nbsp;&laquo;
  </ItemTemplate>
</asp:DataList>
<asp:Label ID="brLabel" runat="server" Text="" /></asp:Label>
