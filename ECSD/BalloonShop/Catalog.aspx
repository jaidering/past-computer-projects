﻿<%@ Page Language="C#" MasterPageFile="~/BalloonShop.master" AutoEventWireup="true" CodeFile="Catalog.aspx.cs" Inherits="Catalog" Title="BalloonShop - The Product Catalog" %>

<%@ Register src="UserControls/ProductsList.ascx" tagname="ProductsList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolder" Runat="Server">
 <asp:Label ID="catalogTitleLabel" CssClass="CatalogTitle"
Runat="server" />
  <br />
  <asp:Label ID="catalogDescriptionLabel" CssClass="CatalogDescription"
 Runat="server" />
  <br />
    <uc1:ProductsList ID="ProductsList1" runat="server" />
&nbsp;
</asp:Content>

