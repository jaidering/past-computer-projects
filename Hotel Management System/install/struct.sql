-- phpMyAdmin SQL Dump
-- version 2.6.4-pl3
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 03, 2007 at 01:10 AM
-- Server version: 4.1.16
-- PHP Version: 5.0.5
--
-- Database: `hotel_management`
--


CREATE TABLE crm_vendor (
  vendor_id int(10) NOT NULL primary key auto_increment,
  created int(11) NOT NULL default '0',
  modify int(11) NOT NULL default '0',
  vendorname varchar(30) default NULL,
  phone varchar(30) default NULL,
  email varchar(30) default NULL,
  website varchar(30) default NULL,
  glacct varchar(30) default NULL,
  category varchar(30) default NULL,
  treet text,
  city varchar(30) default NULL,
  state varchar(30) default NULL,
  postalcode varchar(30) default NULL,
  country varchar(30) default NULL,
  des text
);

CREATE TABLE tbl_account (
  account_id int not null primary key auto_increment,
  account_name varchar(150) NOT NULL default '0',
  group_id int(10) NOT NULL default '0',
  op_bal double(10,2) NOT NULL default '0.00',
  cur_bal double(10,2) NOT NULL default '0.00'
);

--
-- Dumping data for table `tbl_account`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_advance`
--

CREATE TABLE `tbl_advance` (
  `reciept_no` int(10) NOT NULL default '0',
  `cust_id` varchar(10) NOT NULL default '',
  `payment_date` date NOT NULL default '0000-00-00',
  `payment_mode` varchar(100) NOT NULL default '',
  `chk_card_no` varchar(10) NOT NULL default '',
  `bank_name` varchar(10) NOT NULL default '',
  `amount` double(10,2) NOT NULL default '0.00'
) ;

--
-- Dumping data for table `tbl_advance`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bar_bill`
--

CREATE TABLE `tbl_bar_bill` (
  `bill_no` int(10) NOT NULL default '0',
  `table_no` int(10) NOT NULL default '0',
  `rest_date` date NOT NULL default '0000-00-00',
  `rest_time` time NOT NULL default '00:00:00',
  `cust_id` int(10) NOT NULL default '0',
  `room_id` int(10) NOT NULL default '0',
  `tax_scheme_id` int(10) NOT NULL default '0',
  `total` double(10,2) NOT NULL default '0.00',
  `discount` double(10,2) NOT NULL default '0.00',
  `net_total` double(10,2) NOT NULL default '0.00',
  `rs_in_words` varchar(100) NOT NULL default '',
  `trans_status` varchar(5) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_bar_bill`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_bar_order_detail`
--

CREATE TABLE `tbl_bar_order_detail` (
  `sr_no` int(10) NOT NULL default '0',
  `bill_no` varchar(10) NOT NULL default '',
  `menu_id` int(10) NOT NULL default '0',
  `quantity` int(10) NOT NULL default '0',
  `amount` double(10,2) NOT NULL default '0.00',
  PRIMARY KEY  (`sr_no`)
) ;

--
-- Dumping data for table `tbl_bar_order_detail`
--



-- --------------------------------------------------------

--
-- Table structure for table `tbl_bill_receipt`
--

CREATE TABLE `tbl_bill_receipt` (
  `recpt_id` int(10) NOT NULL default '0',
  `bill_no` int(10) NOT NULL default '0',
  `receipt_date` date NOT NULL default '0000-00-00',
  `payment_mode` varchar(40) NOT NULL default '',
  `chq_no` varchar(20) NOT NULL default '',
  `bank_name` varchar(80) NOT NULL default '',
  `amount` double(10,2) NOT NULL default '0.00',
  `rest_bar` varchar(10) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_bill_receipt`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_checkout`
--

CREATE TABLE `tbl_checkout` (
  `checkout_code` int(10) NOT NULL default '0',
  `checkin_code` int(10) NOT NULL default '0',
  `cust_id` int(10) NOT NULL default '0',
  `room_id` int(10) NOT NULL default '0',
  `checkout_date` date NOT NULL default '0000-00-00',
  `checkout_time` time NOT NULL default '00:00:00'
) ;

--
-- Dumping data for table `tbl_checkout`
--



-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE `tbl_country` (
  `country_id` int(20) NOT NULL,
  `countrynm` varchar(150) NOT NULL default '',
  PRIMARY KEY  (`country_id`)
)  AUTO_INCREMENT=137 ;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` VALUES (1, 'Afghanistan');
INSERT INTO `tbl_country` VALUES (2, 'Albania');
INSERT INTO `tbl_country` VALUES (3, 'Algeria');
INSERT INTO `tbl_country` VALUES (4, 'Angola');
INSERT INTO `tbl_country` VALUES (5, 'Argentina');
INSERT INTO `tbl_country` VALUES (6, 'Armenia');
INSERT INTO `tbl_country` VALUES (7, 'Australia');
INSERT INTO `tbl_country` VALUES (8, 'Austria');
INSERT INTO `tbl_country` VALUES (9, 'Bahamas');
INSERT INTO `tbl_country` VALUES (10, 'Bahrain');
INSERT INTO `tbl_country` VALUES (11, 'Bangladesh');
INSERT INTO `tbl_country` VALUES (12, 'Belgium');
INSERT INTO `tbl_country` VALUES (13, 'Bermuda');
INSERT INTO `tbl_country` VALUES (14, 'Bhutan');
INSERT INTO `tbl_country` VALUES (15, 'Bolivia');
INSERT INTO `tbl_country` VALUES (16, 'Bosnia-Herzegovina');
INSERT INTO `tbl_country` VALUES (17, 'Brazil');
INSERT INTO `tbl_country` VALUES (18, 'Brunei');
INSERT INTO `tbl_country` VALUES (19, 'Bulgaria');
INSERT INTO `tbl_country` VALUES (20, 'Cambodia');
INSERT INTO `tbl_country` VALUES (21, 'Cameroon');
INSERT INTO `tbl_country` VALUES (22, 'Canada');
INSERT INTO `tbl_country` VALUES (23, 'Canary Islands');
INSERT INTO `tbl_country` VALUES (24, 'Chile');
INSERT INTO `tbl_country` VALUES (25, 'China');
INSERT INTO `tbl_country` VALUES (26, 'Colombia');
INSERT INTO `tbl_country` VALUES (27, 'Congo');
INSERT INTO `tbl_country` VALUES (28, 'Costa Rica');
INSERT INTO `tbl_country` VALUES (29, 'Croatia');
INSERT INTO `tbl_country` VALUES (30, 'Cuba');
INSERT INTO `tbl_country` VALUES (31, 'Cyprus');
INSERT INTO `tbl_country` VALUES (32, 'Czech Republic');
INSERT INTO `tbl_country` VALUES (33, 'Denmark');
INSERT INTO `tbl_country` VALUES (34, 'Ecuador');
INSERT INTO `tbl_country` VALUES (35, 'Egypt');
INSERT INTO `tbl_country` VALUES (36, 'El Salvador');
INSERT INTO `tbl_country` VALUES (37, 'Falkland Islands');
INSERT INTO `tbl_country` VALUES (38, 'Fiji');
INSERT INTO `tbl_country` VALUES (39, 'Finland');
INSERT INTO `tbl_country` VALUES (40, 'France');
INSERT INTO `tbl_country` VALUES (41, 'Georgia');
INSERT INTO `tbl_country` VALUES (42, 'Germany');
INSERT INTO `tbl_country` VALUES (43, 'Ghana');
INSERT INTO `tbl_country` VALUES (44, 'Gibraltar');
INSERT INTO `tbl_country` VALUES (45, 'Greece');
INSERT INTO `tbl_country` VALUES (46, 'Haiti');
INSERT INTO `tbl_country` VALUES (47, 'Holland');
INSERT INTO `tbl_country` VALUES (48, 'Hong Kong');
INSERT INTO `tbl_country` VALUES (49, 'Hungary');
INSERT INTO `tbl_country` VALUES (50, 'Iceland');
INSERT INTO `tbl_country` VALUES (51, 'India');
INSERT INTO `tbl_country` VALUES (52, 'Indonesia');
INSERT INTO `tbl_country` VALUES (53, 'Iran');
INSERT INTO `tbl_country` VALUES (54, 'Iraq');
INSERT INTO `tbl_country` VALUES (55, 'Ireland');
INSERT INTO `tbl_country` VALUES (56, 'Israel');
INSERT INTO `tbl_country` VALUES (57, 'Italy');
INSERT INTO `tbl_country` VALUES (58, 'Jamaica');
INSERT INTO `tbl_country` VALUES (59, 'Japan');
INSERT INTO `tbl_country` VALUES (60, 'Jordan');
INSERT INTO `tbl_country` VALUES (61, 'Kazakhstan');
INSERT INTO `tbl_country` VALUES (62, 'Kenya');
INSERT INTO `tbl_country` VALUES (63, 'Kuwait');
INSERT INTO `tbl_country` VALUES (64, 'Kyrgyzstan');
INSERT INTO `tbl_country` VALUES (65, 'Latvia');
INSERT INTO `tbl_country` VALUES (66, 'Lebanon');
INSERT INTO `tbl_country` VALUES (67, 'Libya');
INSERT INTO `tbl_country` VALUES (68, 'Lithuania');
INSERT INTO `tbl_country` VALUES (69, 'Luxembourg');
INSERT INTO `tbl_country` VALUES (70, 'Malaysia');
INSERT INTO `tbl_country` VALUES (71, 'Maldives');
INSERT INTO `tbl_country` VALUES (72, 'Mali');
INSERT INTO `tbl_country` VALUES (73, 'Malta');
INSERT INTO `tbl_country` VALUES (74, 'Mauritius');
INSERT INTO `tbl_country` VALUES (75, 'Mongolia');
INSERT INTO `tbl_country` VALUES (76, 'Mexico');
INSERT INTO `tbl_country` VALUES (77, 'Morocco');
INSERT INTO `tbl_country` VALUES (78, 'Mynamar');
INSERT INTO `tbl_country` VALUES (79, 'Namibia');
INSERT INTO `tbl_country` VALUES (80, 'Nepal');
INSERT INTO `tbl_country` VALUES (81, 'Netherlands');
INSERT INTO `tbl_country` VALUES (82, 'New Zealand');
INSERT INTO `tbl_country` VALUES (83, 'Nicaragua');
INSERT INTO `tbl_country` VALUES (84, 'Nigeria');
INSERT INTO `tbl_country` VALUES (85, 'North Korea');
INSERT INTO `tbl_country` VALUES (86, 'Norway');
INSERT INTO `tbl_country` VALUES (87, 'Oman');
INSERT INTO `tbl_country` VALUES (88, 'Pakistan');
INSERT INTO `tbl_country` VALUES (89, 'Pakistan');
INSERT INTO `tbl_country` VALUES (90, 'Paraguay');
INSERT INTO `tbl_country` VALUES (91, 'Peru');
INSERT INTO `tbl_country` VALUES (92, 'Philippines');
INSERT INTO `tbl_country` VALUES (93, 'Poland');
INSERT INTO `tbl_country` VALUES (94, 'Portugal');
INSERT INTO `tbl_country` VALUES (95, 'Puerto Rico');
INSERT INTO `tbl_country` VALUES (96, 'Qatar');
INSERT INTO `tbl_country` VALUES (97, 'Romania');
INSERT INTO `tbl_country` VALUES (98, 'Russia');
INSERT INTO `tbl_country` VALUES (99, 'Saudi Arabia');
INSERT INTO `tbl_country` VALUES (100, 'Senegal');
INSERT INTO `tbl_country` VALUES (101, 'Seychelles');
INSERT INTO `tbl_country` VALUES (102, 'Sierra Leone');
INSERT INTO `tbl_country` VALUES (103, 'Singapore');
INSERT INTO `tbl_country` VALUES (104, 'Slovakia');
INSERT INTO `tbl_country` VALUES (105, 'Slovenia');
INSERT INTO `tbl_country` VALUES (106, 'Somalia');
INSERT INTO `tbl_country` VALUES (107, 'South Africa');
INSERT INTO `tbl_country` VALUES (108, 'South Korea');
INSERT INTO `tbl_country` VALUES (109, 'Spain');
INSERT INTO `tbl_country` VALUES (110, 'Sri Lanka');
INSERT INTO `tbl_country` VALUES (111, 'Sudan');
INSERT INTO `tbl_country` VALUES (112, 'Sweden');
INSERT INTO `tbl_country` VALUES (113, 'Switzerland');
INSERT INTO `tbl_country` VALUES (114, 'Syria');
INSERT INTO `tbl_country` VALUES (115, 'Tahiti');
INSERT INTO `tbl_country` VALUES (116, 'Taiwan');
INSERT INTO `tbl_country` VALUES (117, 'Tajikistan');
INSERT INTO `tbl_country` VALUES (118, 'Tanzania');
INSERT INTO `tbl_country` VALUES (119, 'Thailand');
INSERT INTO `tbl_country` VALUES (120, 'Tunisia');
INSERT INTO `tbl_country` VALUES (121, 'Turkey');
INSERT INTO `tbl_country` VALUES (122, 'Turkmenistan');
INSERT INTO `tbl_country` VALUES (123, 'Uganda');
INSERT INTO `tbl_country` VALUES (124, 'Ukraine');
INSERT INTO `tbl_country` VALUES (125, 'United Arab Emirates');
INSERT INTO `tbl_country` VALUES (126, 'United Kingdom');
INSERT INTO `tbl_country` VALUES (127, 'Uruguay');
INSERT INTO `tbl_country` VALUES (128, 'USA');
INSERT INTO `tbl_country` VALUES (129, 'Uzbekistan');
INSERT INTO `tbl_country` VALUES (130, 'Venezuela');
INSERT INTO `tbl_country` VALUES (131, 'Vietnam');
INSERT INTO `tbl_country` VALUES (132, 'Yemen');
INSERT INTO `tbl_country` VALUES (133, 'Yugoslavia');
INSERT INTO `tbl_country` VALUES (134, 'Zambia');
INSERT INTO `tbl_country` VALUES (135, 'Zimbabwe');
INSERT INTO `tbl_country` VALUES (136, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `cust_id` int(10) NOT NULL default '0',
  `cust_name` varchar(100) NOT NULL default '',
  `cust_address` varchar(100) NOT NULL default '',
  `city` varchar(100) NOT NULL default '',
  `zip` varchar(10) NOT NULL default '',
  `state` varchar(100) NOT NULL default '',
  `country` varchar(70) NOT NULL default '',
  `cust_phone` varchar(100) NOT NULL default '',
  `cust_mobile` varchar(26) NOT NULL default '',
  `adults` varchar(10) NOT NULL default '',
  `child` varchar(10) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_customer`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_dept`
--

CREATE TABLE `tbl_dept` (
  `dept_id` int(10) NOT NULL default '0',
  `dept_name` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`dept_id`)
) ;

--
-- Dumping data for table `tbl_dept`
--

INSERT INTO `tbl_dept` VALUES (1, 'Restaurant');
INSERT INTO `tbl_dept` VALUES (2, 'House Keeping');
INSERT INTO `tbl_dept` VALUES (3, 'Room Services');
INSERT INTO `tbl_dept` VALUES (4, 'Security');
INSERT INTO `tbl_dept` VALUES (5, 'Front Desk');
INSERT INTO `tbl_dept` VALUES (6, 'Accounts');
INSERT INTO `tbl_dept` VALUES (7, 'Miscelaneuous');
INSERT INTO `tbl_dept` VALUES (8, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_facility`
--

CREATE TABLE `tbl_facility` (
  `facility_id` int(10) NOT NULL default '0',
  `facility_name` varchar(100) NOT NULL default '',
  `facility_charges` double(10,2) NOT NULL default '0.00',
  PRIMARY KEY  (`facility_id`)
) ;

--
-- Dumping data for table `tbl_facility`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_final_bill`
--

CREATE TABLE `tbl_final_bill` (
  `bill_no` varchar(10) NOT NULL default '0',
  `bill_date` date NOT NULL default '0000-00-00',
  `time` time NOT NULL default '00:00:00',
  `cust_id` varchar(50) NOT NULL default '',
  `room_no` varchar(10) NOT NULL default '0',
  `total` double(10,2) NOT NULL default '0.00',
  `discount` double(10,2) NOT NULL default '0.00',
  `net_total` double(10,2) NOT NULL default '0.00',
  `rs_in_words` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`bill_no`)
) ;

--
-- Dumping data for table `tbl_final_bill`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_floor`
--

CREATE TABLE `tbl_floor` (
  `floor_id` int(10) NOT NULL default '0',
  `floor_name` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_floor`
--

INSERT INTO `tbl_floor` VALUES (1, 'Ground Floor');



-- --------------------------------------------------------

--
-- Table structure for table `tbl_group`
--

CREATE TABLE `tbl_group` (
  `group_id` int(10) NOT NULL default '0',
  `account_group` varchar(150) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_group`
--

INSERT INTO `tbl_group` VALUES (1, 'Bank A/C');
INSERT INTO `tbl_group` VALUES (2, 'Cash-In-Hand');
INSERT INTO `tbl_group` VALUES (3, 'Purchase A/C');
INSERT INTO `tbl_group` VALUES (4, 'Sales A/C');
INSERT INTO `tbl_group` VALUES (5, 'Sundry Creditors');
INSERT INTO `tbl_group` VALUES (6, 'Sundry Debtors');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hall_book`
--

CREATE TABLE `tbl_hall_book` (
  `cust_id` int(10) NOT NULL default '0',
  `occasion_id` int(10) NOT NULL default '0',
  `hall_charges` double(10,2) NOT NULL default '0.00',
  `from_date` date NOT NULL default '0000-00-00',
  `to_date` date NOT NULL default '0000-00-00',
  `from_time` time NOT NULL default '00:00:00',
  `to_time` time NOT NULL default '00:00:00',
  `no_of_people` int(10) NOT NULL default '0',
  `final_total` double(10,2) NOT NULL default '0.00'
) ;

--
-- Dumping data for table `tbl_hall_book`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_hall_order`
--

CREATE TABLE `tbl_hall_order` (
  `cust_id` int(10) NOT NULL default '0',
  `menu_id` int(10) NOT NULL default '0',
  `charge` double(10,2) NOT NULL default '0.00'
) ;

--
-- Dumping data for table `tbl_hall_order`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_hotel_info`
--

CREATE TABLE `tbl_hotel_info` (
  `hotel_id` int(10) NOT NULL default '0',
  `hotel_name` varchar(100) NOT NULL default '',
  `hotel_address` varchar(100) NOT NULL default '',
  `city` varchar(50) NOT NULL default '',
  `state` varchar(50) NOT NULL default '',
  `zip` varchar(20) NOT NULL default '0',
  `country_id` varchar(50) NOT NULL default '',
  `phone` varchar(50) NOT NULL default '',
  `fax` varchar(50) default NULL,
  `email` varchar(50) NOT NULL default '',
  `url` varchar(50) NOT NULL default '',
  `path` varchar(50) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_hotel_info`
--

INSERT INTO `tbl_hotel_info` VALUES (0, 'Sample', 'Sample', 'Sample', 'Sample', 0, 'sample', 'sample', '', 'nobody@nowhere.com', 'www.somesite.com', '_');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_house_keeping`
--

CREATE TABLE `tbl_house_keeping` (
  `hk_id` int(10) NOT NULL default '0',
  `room_no` varchar(10) NOT NULL default '0',
  `work_desc` varchar(100) NOT NULL default '',
  `date` date NOT NULL default '0000-00-00',
  `time` time NOT NULL default '00:00:00',
  `status` varchar(10) NOT NULL default '',
  `exp_date` date NOT NULL default '0000-00-00',
  `exp_time` time NOT NULL default '00:00:00',
  `compli_date` date NOT NULL default '0000-00-00',
  `compli_time` time NOT NULL default '00:00:00'
) ;

--
-- Dumping data for table `tbl_house_keeping`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_inward`
--

CREATE TABLE `tbl_inward` (
  `id` int(10) NOT NULL default '0',
  `inward_no` varchar(10) NOT NULL default '',
  `inward_date` date NOT NULL default '0000-00-00',
  `po_number` varchar(10) NOT NULL default '',
  `vendor_id` int(10) NOT NULL default '0',
  `tax` double(10,2) NOT NULL default '0.00',
  `total` double(10,2) NOT NULL default '0.00',
  `discount` double(10,2) NOT NULL default '0.00',
  `net_total` double(10,2) NOT NULL default '0.00',
  `rs_in_words` varchar(10) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_inward`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inward_detail`
--

CREATE TABLE `tbl_inward_detail` (
  `id` int(10) NOT NULL default '0',
  `inward_no` varchar(10) NOT NULL default '',
  `item_id` int(10) NOT NULL default '0',
  `quantity` varchar(10) NOT NULL default '',
  `rate` double(10,2) NOT NULL default '0.00',
  `amount` double(10,2) NOT NULL default '0.00'
) ;

--
-- Dumping data for table `tbl_inward_detail`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_item`
--

CREATE TABLE `tbl_item` (
  `item_id` int(10) NOT NULL default '0',
  `item_type_id` int(10) NOT NULL default '0',
  `item_name` varchar(100) NOT NULL default '',
  `unit_id` int(10) NOT NULL default '0',
  `opening_stock` varchar(100) NOT NULL default '',
  `current_bal` int(10) NOT NULL default '0',
  `max_qty` varchar(100) NOT NULL default '',
  `min_qty` varchar(100) NOT NULL default '',
  `standard_qty` varchar(100) NOT NULL default '',
  `standard_rate` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`item_id`)
) ;

--
-- Dumping data for table `tbl_item`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_receipt`
--

CREATE TABLE `tbl_item_receipt` (
  `id` int(10) NOT NULL default '0',
  `receipt_no` int(10) NOT NULL default '0',
  `outward_no` int(10) NOT NULL default '0',
  `receipt_date` date NOT NULL default '0000-00-00',
  `time` time NOT NULL default '00:00:00',
  `dept_id` int(10) NOT NULL default '0'
) ;

--
-- Dumping data for table `tbl_item_receipt`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_receipt_detail`
--

CREATE TABLE `tbl_item_receipt_detail` (
  `id` int(10) NOT NULL default '0',
  `receipt_no` int(10) NOT NULL default '0',
  `item_id` int(10) NOT NULL default '0',
  `quantity` varchar(10) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_item_receipt_detail`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_type`
--

CREATE TABLE `tbl_item_type` (
  `item_type_id` int(10) NOT NULL default '0',
  `item_type_name` varchar(100) NOT NULL default '',
  `igredient` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`item_type_id`)
) ;

--
-- Dumping data for table `tbl_item_type`
--

INSERT INTO `tbl_item_type` VALUES (1, 'Kitchen Item', '1');
INSERT INTO `tbl_item_type` VALUES (2, 'Room Item', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_master_account`
--

CREATE TABLE `tbl_master_account` (
  `master_account_id` int(50) NOT NULL default '0',
  `master_account_name` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`master_account_id`)
) ;

--
-- Dumping data for table `tbl_master_account`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_material_issue`
--

CREATE TABLE `tbl_material_issue` (
  `outward_number` varchar(10) NOT NULL default '',
  `outward_date` date NOT NULL default '0000-00-00',
  `dept_id` int(10) NOT NULL default '0',
  `sr_no` int(10) NOT NULL default '0'
) ;

--
-- Dumping data for table `tbl_material_issue`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu_card_create`
--

CREATE TABLE `tbl_menu_card_create` (
  `menu_item_id` int(10) NOT NULL default '0',
  `menu_name` varchar(100) NOT NULL default '',
  `menu_type_id` int(100) NOT NULL default '0'
) ;

--
-- Dumping data for table `tbl_menu_card_create`
--



-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu_item_ingredient`
--

CREATE TABLE `tbl_menu_item_ingredient` (
  `menu_item_id` int(10) NOT NULL default '0',
  `ingredient_nm` varchar(100) NOT NULL default '',
  `ingredient_type` varchar(100) NOT NULL default '',
  `quantity` double(10,2) NOT NULL default '0.00',
  `unit` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_menu_item_ingredient`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu_type`
--

CREATE TABLE `tbl_menu_type` (
  `menu_type_id` int(10) NOT NULL default '0',
  `menu_type_name` varchar(100) NOT NULL default '',
  `taxable` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`menu_type_id`)
) ;

--
-- Dumping data for table `tbl_menu_type`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu_type_tax`
--

CREATE TABLE `tbl_menu_type_tax` (
  `menu_tax_id` int(10) NOT NULL default '0',
  `menu_type_id` varchar(100) NOT NULL default '',
  `tax_scheme_id` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_menu_type_tax`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_menucard_name`
--

CREATE TABLE `tbl_menucard_name` (
  `card_nm_id` int(10) NOT NULL default '0',
  `card_name` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`card_nm_id`)
) ;

--
-- Dumping data for table `tbl_menucard_name`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_menucard_name_detail`
--

CREATE TABLE `tbl_menucard_name_detail` (
  `card_nm_id` int(10) NOT NULL default '0',
  `card_item_id` int(10) NOT NULL default '0',
  `card_item_qty` int(10) NOT NULL default '0',
  `units` varchar(10) NOT NULL default '',
  `card_item_price` double(10,2) NOT NULL default '0.00'
) ;

--
-- Dumping data for table `tbl_menucard_name_detail`
--



-- --------------------------------------------------------

--
-- Table structure for table `tbl_occassion`
--

CREATE TABLE `tbl_occassion` (
  `occasion_id` int(10) NOT NULL default '0',
  `occassion_name` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_occassion`
--



-- --------------------------------------------------------

--
-- Table structure for table `tbl_outward`
--

CREATE TABLE `tbl_outward` (
  `id` int(10) NOT NULL default '0',
  `outward_no` varchar(10) NOT NULL default '',
  `req_no` int(10) NOT NULL default '0',
  `outward_date` date NOT NULL default '0000-00-00',
  `dept_id` int(10) NOT NULL default '0'
) ;

--
-- Dumping data for table `tbl_outward`
--



-- --------------------------------------------------------

--
-- Table structure for table `tbl_outward_detail`
--

CREATE TABLE `tbl_outward_detail` (
  `id` int(10) NOT NULL default '0',
  `outward_no` int(10) NOT NULL default '0',
  `item_id` int(10) NOT NULL default '0',
  `item_qty` int(10) NOT NULL default '0'
) ;

--
-- Dumping data for table `tbl_outward_detail`
--



-- --------------------------------------------------------

--
-- Table structure for table `tbl_parameter`
--

CREATE TABLE `tbl_parameter` (
  `parameter_id` int(10) NOT NULL default '0',
  `parameter_name` varchar(100) NOT NULL default '',
  `parameter_value` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`parameter_id`)
) ;

--
-- Dumping data for table `tbl_parameter`
--

INSERT INTO `tbl_parameter` VALUES (1, 'room_type_charges', '3');
INSERT INTO `tbl_parameter` VALUES (2, 'currency', '$');
INSERT INTO `tbl_parameter` VALUES (3, 'restaurent_menucard', '1');
INSERT INTO `tbl_parameter` VALUES (4, 'bar_menucard', '3');
INSERT INTO `tbl_parameter` VALUES (5, 'restaurent_menucard_room', '2');
INSERT INTO `tbl_parameter` VALUES (6, 'bar_menucard_room', '2');
INSERT INTO `tbl_parameter` VALUES (7, 'year_type', 'Yearly');
INSERT INTO `tbl_parameter` VALUES (8, 'from_day', '1');
INSERT INTO `tbl_parameter` VALUES (9, 'from_month', '12');
INSERT INTO `tbl_parameter` VALUES (10, 'to_day', '29');
INSERT INTO `tbl_parameter` VALUES (11, 'to_month', '11');
INSERT INTO `tbl_parameter` VALUES (12, 'start_year', '2006');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `payment_id` int(10) NOT NULL default '0',
  `transaction_no` int(10) NOT NULL default '0',
  `payment_date` date NOT NULL default '0000-00-00',
  `debit` varchar(100) NOT NULL default '',
  `credit` varchar(100) NOT NULL default '',
  `amount` double(10,2) NOT NULL default '0.00',
  `narration` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_payment`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment_mode`
--

CREATE TABLE `tbl_payment_mode` (
  `mode_id` int(10) NOT NULL default '0',
  `mode_name` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`mode_id`)
) ;

--
-- Dumping data for table `tbl_payment_mode`
--

INSERT INTO `tbl_payment_mode` VALUES (1, 'Cash');
INSERT INTO `tbl_payment_mode` VALUES (2, 'Credit Card');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_po_detail`
--

CREATE TABLE `tbl_po_detail` (
  `id` int(10) NOT NULL default '0',
  `po_number` int(10) NOT NULL default '0',
  `item_id` int(10) NOT NULL default '0',
  `quantity` int(10) NOT NULL default '0',
  `rate` double(10,2) NOT NULL default '0.00',
  `amount` double(10,2) NOT NULL default '0.00'
) ;

--
-- Dumping data for table `tbl_po_detail`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_po_order`
--

CREATE TABLE `tbl_po_order` (
  `id` int(10) NOT NULL default '0',
  `po_number` int(10) NOT NULL default '0',
  `po_date` date NOT NULL default '0000-00-00',
  `vendor_id` int(10) NOT NULL default '0',
  `tax_id` int(10) NOT NULL default '0',
  `total` double(10,2) NOT NULL default '0.00',
  `bank` varchar(100) NOT NULL default '',
  `chk_dd_no` int(10) NOT NULL default '0',
  `chk_dd_date` date NOT NULL default '0000-00-00',
  `net_amount` double(10,2) NOT NULL default '0.00',
  `amount` double(10,2) NOT NULL default '0.00',
  `rs_in_words` varchar(100) NOT NULL default '',
  `status` varchar(20) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_po_order`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_reciept`
--

CREATE TABLE `tbl_reciept` (
  `receipt_id` int(10) NOT NULL default '0',
  `transaction_no` int(10) NOT NULL default '0',
  `reciept_date` date NOT NULL default '0000-00-00',
  `credit` varchar(100) NOT NULL default '',
  `debit` varchar(100) NOT NULL default '',
  `amount` double(10,2) NOT NULL default '0.00',
  `narration` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`receipt_id`)
) ;

--
-- Dumping data for table `tbl_reciept`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_req`
--

CREATE TABLE `tbl_req` (
  `id` int(10) NOT NULL default '0',
  `req_no` varchar(10) NOT NULL default '',
  `req_date` date NOT NULL default '0000-00-00',
  `lead_date` date NOT NULL default '0000-00-00',
  `req_time` time NOT NULL default '00:00:00',
  `dept_id` int(10) NOT NULL default '0',
  `status` varchar(50) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_req`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_req_detail`
--

CREATE TABLE `tbl_req_detail` (
  `id` int(10) NOT NULL default '0',
  `req_no` varchar(100) NOT NULL default '',
  `item_id` int(10) NOT NULL default '0',
  `quantity` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_req_detail`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_rest_order_detail`
--

CREATE TABLE `tbl_rest_order_detail` (
  `sr_no` int(10) NOT NULL primary key auto_increment,
  `bill_no` varchar(10) NOT NULL default '',
  `menu_id` int(10) NOT NULL default '0',
  `quantity` int(10) NOT NULL default '0',
  `amount` double(10,2) NOT NULL default '0.00'
) ;

--
-- Dumping data for table `tbl_rest_order_detail`
--



-- --------------------------------------------------------

--
-- Table structure for table `tbl_restaurant_bill`
--

CREATE TABLE `tbl_restaurant_bill` (
  `bill_no` int(10) NOT NULL primary key auto_increment,
  `table_no` int(10) NOT NULL default '0',
  `rest_date` date NOT NULL default '0000-00-00',
  `rest_time` time NOT NULL default '00:00:00',
  `cust_id` int(10) NOT NULL default '0',
  `room_id` int(10) NOT NULL default '0',
  `tax_scheme_id` int(10) NOT NULL default '0',
  `total` double(10,2) NOT NULL default '0.00',
  `discount` double(10,2) NOT NULL default '0.00',
  `net_total` double(10,2) NOT NULL default '0.00',
  `rs_in_words` varchar(100) NOT NULL default '',
  `trans_status` varchar(5) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_restaurant_bill`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_room`
--

CREATE TABLE `tbl_room` (
  `room_id` int(10) NOT NULL default '0',
  `room_no` varchar(100) NOT NULL default '0',
  `floor_id` int(10) NOT NULL default '0',
  `room_type_id` int(10) NOT NULL default '0',
  `room_adult` int(10) NOT NULL default '0',
  `room_child` int(10) NOT NULL default '0',
  `allowed_smoking` int(10) NOT NULL default '0',
  `room_charges` double(100,2) NOT NULL default '0.00',
  `room_status` varchar(10) NOT NULL default '',
  `from_date` date NOT NULL default '0000-00-00',
  `to_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`room_id`)
) ;

--
-- Dumping data for table `tbl_room`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_booked`
--

CREATE TABLE `tbl_room_booked` (
  `booking_code` int(10) NOT NULL default '0',
  `cust_id` int(10) NOT NULL default '0',
  `room_type_id` int(10) NOT NULL default '0',
  `room_id` varchar(10) NOT NULL default '0',
  `room_charges` double(10,2) NOT NULL default '0.00',
  `from_date` date NOT NULL default '0000-00-00',
  `to_date` date NOT NULL default '0000-00-00',
  `booking_date` date NOT NULL default '0000-00-00'
) ;

--
-- Dumping data for table `tbl_room_booked`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_checkin`
--

CREATE TABLE `tbl_room_checkin` (
  `checkin_code` int(10) NOT NULL default '0',
  `booking_code` int(10) NOT NULL default '0',
  `cust_id` int(10) NOT NULL default '0',
  `room_type_id` int(10) NOT NULL default '0',
  `room_id` int(10) NOT NULL default '0',
  `room_charges` double(10,2) NOT NULL default '0.00',
  `from_date` date NOT NULL default '0000-00-00',
  `to_date` date NOT NULL default '0000-00-00',
  `arrival_time` time NOT NULL default '00:00:00',
  `come_from` varchar(100) NOT NULL default '',
  `going_to` varchar(100) NOT NULL default '',
  `adult` int(10) NOT NULL default '0',
  `child` int(10) NOT NULL default '0'
) ;

--
-- Dumping data for table `tbl_room_checkin`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_service`
--

CREATE TABLE `tbl_room_service` (
  `service_id` int(10) NOT NULL default '0',
  `service_date` date NOT NULL default '0000-00-00',
  `service_time` time NOT NULL default '00:00:00',
  `customer_id` int(10) NOT NULL default '0',
  `room_number` int(10) NOT NULL default '0',
  `dept_no` int(10) NOT NULL default '0',
  `service_no` int(10) NOT NULL default '0',
  `service_desc` varchar(200) NOT NULL default '',
  `other` varchar(100) NOT NULL default '',
  `final_total` double(10,2) NOT NULL default '0.00',
  `status` varchar(10) NOT NULL default '',
  `exp_date` date NOT NULL default '0000-00-00',
  `exp_time` time NOT NULL default '00:00:00',
  `compli_date` date NOT NULL default '0000-00-00',
  `compli_time` time NOT NULL default '00:00:00'
) ;

--
-- Dumping data for table `tbl_room_service`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_type`
--

CREATE TABLE `tbl_room_type` (
  `room_type_id` int(10) NOT NULL default '0',
  `room_type` varchar(100) NOT NULL default '',
  `room_type_charges` double(10,2) NOT NULL default '0.00',
  `bedsize` varchar(50) NOT NULL default '',
  `handicap` int(2) NOT NULL default '0',
  `Note` varchar(200) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_room_type`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_room_type_facility`
--

CREATE TABLE `tbl_room_type_facility` (
  `facility_no` int(10) NOT NULL default '0',
  `room_type_id` varchar(100) NOT NULL default '',
  `facility_id` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_room_type_facility`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_selected_year`
--

CREATE TABLE `tbl_selected_year` (
  `year_id` int(10) NOT NULL default '0',
  `from_date` date NOT NULL default '0000-00-00',
  `to_date` date NOT NULL default '0000-00-00'
) ;

--
-- Dumping data for table `tbl_selected_year`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_service_entry`
--

CREATE TABLE `tbl_service_entry` (
  `service_id` int(10) NOT NULL default '0',
  `service_name` varchar(100) NOT NULL default '',
  `service_charges` double(10,2) NOT NULL default '0.00',
  `dept_id` int(10) NOT NULL default '0',
  `taxable` varchar(10) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_service_entry`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_service_rest_order`
--

CREATE TABLE `tbl_service_rest_order` (
  `sr_no` int(10) NOT NULL default '0',
  `service_id` varchar(10) NOT NULL default '',
  `menu_id` int(10) NOT NULL default '0',
  `quantity` int(10) NOT NULL default '0',
  `amount` double(10,2) NOT NULL default '0.00',
  PRIMARY KEY  (`sr_no`)
) ;

--
-- Dumping data for table `tbl_service_rest_order`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `status_id` int(10) NOT NULL default '0',
  `status_name` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`status_id`)
) ;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` VALUES (1, 'Complete');
INSERT INTO `tbl_status` VALUES (2, 'In Process');
INSERT INTO `tbl_status` VALUES (3, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_table`
--

CREATE TABLE `tbl_table` (
  `table_id` int(10) NOT NULL default '0',
  `table_no` varchar(10) NOT NULL default '0',
  `table_chairs` int(10) NOT NULL default '0',
  `table_type_id` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`table_id`)
) ;

--
-- Dumping data for table `tbl_table`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_table_type`
--

CREATE TABLE `tbl_table_type` (
  `table_type_id` int(10) NOT NULL default '0',
  `table_type_nm` varchar(50) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_table_type`
--

INSERT INTO `tbl_table_type` VALUES (1, 'Restaurant Table');
INSERT INTO `tbl_table_type` VALUES (2, 'Bar Table');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tax`
--

CREATE TABLE `tbl_tax` (
  `tax_id` int(10) NOT NULL default '0',
  `tax_name` varchar(100) NOT NULL default '',
  `tax_percent` double(10,2) NOT NULL default '0.00',
  `live_status` int(2) NOT NULL default '0',
  PRIMARY KEY  (`tax_id`)
) ;

--
-- Dumping data for table `tbl_tax`
--

INSERT INTO `tbl_tax` VALUES (1, 'VAT', 12.50, 1);
INSERT INTO `tbl_tax` VALUES (2, 'Food Tax', 4.50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tax_scheme`
--

CREATE TABLE `tbl_tax_scheme` (
  `scheme_id` int(10) NOT NULL default '0',
  `sheme_name` varchar(100) NOT NULL default '',
  `from_date` date NOT NULL default '0000-00-00',
  `live_status` tinyint(1) not null default 0,
  PRIMARY KEY  (`scheme_id`)
) ;

--
-- Dumping data for table `tbl_tax_scheme`
--

INSERT INTO `tbl_tax_scheme` VALUES (1, 'Tax Scheme', '2006-08-01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tax_scheme_detail`
--

CREATE TABLE `tbl_tax_scheme_detail` (
  `sr_no` int(10) NOT NULL default '0',
  `scheme_id` varchar(100) NOT NULL default '0',
  `tax_id` varchar(100) NOT NULL default '0',
  PRIMARY KEY  (`sr_no`)
) ;

--
-- Dumping data for table `tbl_tax_scheme_detail`
--

INSERT INTO `tbl_tax_scheme_detail` VALUES (1, '1', '1');
INSERT INTO `tbl_tax_scheme_detail` VALUES (2, '1', '2');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temp_inword_detail`
--

CREATE TABLE `tbl_temp_inword_detail` (
  `temp_id` int(10) NOT NULL default '0',
  `inword_number` varchar(10) NOT NULL default '',
  `item_id` int(10) NOT NULL default '0',
  `qty` int(10) NOT NULL default '0',
  `rate` double(10,2) NOT NULL default '0.00',
  `amount` double(10,2) NOT NULL default '0.00',
  `session_id` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_temp_inword_detail`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temp_item_receipt_detail`
--

CREATE TABLE `tbl_temp_item_receipt_detail` (
  `temp_id` int(10) NOT NULL default '0',
  `receipt_no` int(10) NOT NULL default '0',
  `item_id` int(10) NOT NULL default '0',
  `qty` varchar(10) NOT NULL default '',
  `session_id` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_temp_item_receipt_detail`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_temp_outward_detail`
--

CREATE TABLE `tbl_temp_outward_detail` (
  `temp_id` int(10) NOT NULL default '0',
  `outward_number` int(10) NOT NULL default '0',
  `item_id` int(10) NOT NULL default '0',
  `item_qty` int(10) NOT NULL default '0',
  `session_id` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_temp_outward_detail`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temp_po_detail`
--

CREATE TABLE `tbl_temp_po_detail` (
  `temp_id` int(10) NOT NULL default '0',
  `po_number` varchar(10) NOT NULL default '',
  `item_id` int(10) NOT NULL default '0',
  `qty` int(10) NOT NULL default '0',
  `rate` double(10,2) NOT NULL default '0.00',
  `amount` double(10,2) NOT NULL default '0.00',
  `session_id` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_temp_po_detail`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temp_req_detail`
--

CREATE TABLE `tbl_temp_req_detail` (
  `temp_id` int(10) NOT NULL default '0',
  `req_no` varchar(100) NOT NULL default '',
  `item_id` int(10) NOT NULL default '0',
  `qty` varchar(100) NOT NULL default '',
  `session_id` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_temp_req_detail`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_unit`
--

CREATE TABLE `tbl_unit` (
  `unit_id` int(10) NOT NULL default '0',
  `unit_name` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`unit_id`)
) ;

--
-- Dumping data for table `tbl_unit`
--

INSERT INTO `tbl_unit` VALUES (1, 'Kg');
INSERT INTO `tbl_unit` VALUES (2, 'pcs');
INSERT INTO `tbl_unit` VALUES (3, 'plate');
INSERT INTO `tbl_unit` VALUES (4, 'pieces');
INSERT INTO `tbl_unit` VALUES (5, 'peck');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_info`
--

CREATE TABLE `tbl_user_info` (
  `user_id` int(6) NOT NULL,
  `created` int(11) NOT NULL default '0',
  `modify` int(11) NOT NULL default '0',
  `first_name` varchar(60) default NULL,
  `username` varchar(60) default NULL,
  `last_name` varchar(60) default NULL,
  `password` varchar(10) default NULL,
  `phone_work` varchar(60) default NULL,
  `department` varchar(60) default NULL,
  `email1` varchar(60) default NULL,
  `description` text,
  `role` varchar(60) default NULL,
  `address_street` text,
  `address_city` varchar(60) default NULL,
  `address_state` varchar(60) default NULL,
  `address_postalcode` varchar(60) default NULL,
  `address_country` varchar(60) default NULL,
  PRIMARY KEY  (`user_id`)
);

--
-- Dumping data for table `tbl_user_info`
--

INSERT INTO `tbl_user_info` VALUES (1, 0, 1163470277, '', 'admin', 'admin', 'admin', '', 'Administration', '', '', '1', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_role`
--

CREATE TABLE `tbl_user_role` (
  `role_id` int(10) NOT NULL default '0',
  `user_role` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_user_role`
--

INSERT INTO `tbl_user_role` VALUES (1, 'administrator');
INSERT INTO `tbl_user_role` VALUES (2, 'frontdesk');
INSERT INTO `tbl_user_role` VALUES (3, 'restaurant');
INSERT INTO `tbl_user_role` VALUES (4, 'housekeeping');
INSERT INTO `tbl_user_role` VALUES (5, 'roomservices');
INSERT INTO `tbl_user_role` VALUES (6, 'stores');
INSERT INTO `tbl_user_role` VALUES (7, 'account');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vendor_info`
--

CREATE TABLE `tbl_vendor_info` (
  `vendor_id` int(10) NOT NULL default '0',
  `vendor_name` varchar(100) NOT NULL default '',
  `vendor_address` varchar(100) NOT NULL default '',
  `vendor_city` varchar(100) NOT NULL default '',
  `vendor_state` varchar(100) NOT NULL default '',
  `country_id` int(10) NOT NULL default '0',
  `vendor_zip` varchar(100) NOT NULL default '',
  `vendor_phone` varchar(100) NOT NULL default '',
  `vendor_mobile` varchar(100) NOT NULL default '',
  `vendor_contact_person` varchar(100) NOT NULL default ''
) ;

--
-- Dumping data for table `tbl_vendor_info`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vendor_item`
--

CREATE TABLE `tbl_vendor_item` (
  `sr_no` int(10) NOT NULL default '0',
  `vendor_id` int(10) NOT NULL default '0',
  `item_id` int(10) NOT NULL default '0',
  PRIMARY KEY  (`sr_no`)
) ;

--
-- Dumping data for table `tbl_vendor_item`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_year`
--

CREATE TABLE `tbl_year` (
  `year_id` int(10) NOT NULL default '0',
  `from_date` date NOT NULL default '0000-00-00',
  `to_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`year_id`)
) ;

--
-- Dumping data for table `tbl_year`
--


-- --------------------------------------------------------

--
-- Table structure for table `temp_facility`
--

CREATE TABLE `temp_facility` (
  `sr_no` int(10) NOT NULL default '0',
  `facility_id` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`sr_no`)
) ;

--
-- Dumping data for table `temp_facility`
--


-- --------------------------------------------------------

--
-- Table structure for table `temp_rest_order_detail`
--

CREATE TABLE `temp_rest_order_detail` (
  `sr_no` int(10) NOT NULL default '0',
  `bill_no` int(10) NOT NULL default '0',
  `menu_id` int(10) NOT NULL default '0',
  `quantity` int(10) NOT NULL default '0',
  `amount` double(10,2) NOT NULL default '0.00',
  PRIMARY KEY  (`sr_no`)
) ;

--
-- Dumping data for table `temp_rest_order_detail`
--

-- --------------------------------------------------------

--
-- Table structure for table `temp_room_book`
--

CREATE TABLE `temp_room_book` (
  `sr_no` int(10) NOT NULL default '0',
  `room_no` varchar(100) NOT NULL default '',
  `room_type_id` varchar(100) NOT NULL default '',
  `floor_no` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`sr_no`)
) ;

--
-- Dumping data for table `temp_room_book`
--


-- --------------------------------------------------------

--
-- Table structure for table `temp_room_book_info`
--

CREATE TABLE `temp_room_book_info` (
  `room_id` int(10) NOT NULL default '0',
  `status` varchar(20) NOT NULL default ''
) ;

--
-- Dumping data for table `temp_room_book_info`
--

INSERT INTO `temp_room_book_info` VALUES (4, 'F');

-- --------------------------------------------------------

--
-- Table structure for table `temp_room_free_book`
--

CREATE TABLE `temp_room_free_book` (
  `sr_no` int(10) NOT NULL default '0',
  `room_no` varchar(10) NOT NULL default '',
  `room_type_id` varchar(10) NOT NULL default '',
  `floor_no` varchar(10) NOT NULL default '',
  `status` varchar(10) NOT NULL default '',
  `from_date` date NOT NULL default '0000-00-00',
  `to_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`sr_no`)
) ;

--
-- Dumping data for table `temp_room_free_book`
--


-- --------------------------------------------------------

--
-- Table structure for table `temp_room_type`
--

CREATE TABLE `temp_room_type` (
  `sr_no` int(10) NOT NULL default '0',
  `type_id` int(10) NOT NULL default '0',
  `counter` int(10) NOT NULL default '0',
  PRIMARY KEY  (`sr_no`)
) ;

--
-- Dumping data for table `temp_room_type`
--

