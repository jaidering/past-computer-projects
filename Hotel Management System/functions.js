  function isPhone(val)
  {
    var allowed = "1234567890-()";
    var len = val.length;

    var ok = true;

    for (var i = 0; i < len; ++i)
    {
      var cLetter = val.charAt(i);
      if (allowed.indexOf(cLetter) == -1)
      {
        ok = false;
        break;
      }
    } // for

    if (!ok)
      return false;
    else
      return true;

  } // isPhone

  function isZip(val)
  {
    var ok = true;
    var len = val.length;

    if (len == 0 || len > 14)
    {
      ok=false;
    }

    var allowed = "1234567890-";
    for (var i = 0; i < len; ++i)
    {
      var cLetter = val.charAt(i);
      if (allowed.indexOf(cLetter) == -1)
      {
        ok = false;
        break;
      }
    }

    if (ok)
      return true;
    else
      return false;
    endif;
  } // isZip
  
  function clearSelect(elm)
  {
		var ecount = elm.options.length;
		
		for (var iter = ecount-1; iter >= 0; --iter)
		{
			elm.options[iter] = null;
		} // endfor	
  } // clearSelect

