  var xmlhttp = null;
  var xmlhttp2 = null;

  try {
    xmlhttp = new XMLHttpRequest();
    xmlhttp2 = new XMLHttpRequest();
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
      xmlhttp2 = new ActiveXObject("Msxml3.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
      xmlhttp2 = false;
    }
  }

  function createRequest()
  {
    var output = null;
    try {
      output = new XMLHttpRequest();
    } catch (e) {
      try {
        output = new ActiveXObject("Msxml2.XMLHTTP");

      } catch (E) {
        output = false;

      }
    }
    return output;
  } // createrequest